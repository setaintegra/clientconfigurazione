VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPrezzo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idPrezzoTitoloProdotto As Long
Public importoBase As Long
Public stringaImportoBase As String
Public importoServizioPrevendita As Long
Public stringaImportoServizioPrevendita As String
Public idTariffa As Long
Public idArea As Long
Public idProdotto As Long
Public idPeriodoCommerciale As Long
Public idTipoTariffaSIAE As Long

