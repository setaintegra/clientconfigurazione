VERSION 5.00
Begin VB.Form frmStrutturaPiantaCapienzaIllimitata 
   Caption         =   "Struttura pianta"
   ClientHeight    =   6255
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11700
   LinkTopic       =   "Form1"
   ScaleHeight     =   6255
   ScaleWidth      =   11700
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtStampaIngresso 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6105
      MaxLength       =   10
      TabIndex        =   21
      Top             =   3420
      Width           =   2475
   End
   Begin VB.CommandButton cmdEliminaSuperarea 
      Caption         =   "Elimina"
      Height          =   375
      Left            =   8040
      TabIndex        =   15
      Top             =   3900
      Width           =   1575
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   615
      Left            =   9960
      TabIndex        =   17
      Top             =   4860
      Width           =   1575
   End
   Begin VB.CommandButton cmdAnnullaSuperarea 
      Caption         =   "Annulla"
      Height          =   375
      Left            =   9960
      TabIndex        =   16
      Top             =   3900
      Width           =   1575
   End
   Begin VB.CommandButton cmdConfermaSuperarea 
      Caption         =   "Conferma"
      Height          =   375
      Left            =   6120
      TabIndex        =   14
      Top             =   3900
      Width           =   1575
   End
   Begin VB.CommandButton cmdNuovaSuperarea 
      Caption         =   "Nuova superarea"
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   3360
      Width           =   4095
   End
   Begin VB.TextBox txtDescrizioneSuperarea 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   6100
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Top             =   2640
      Width           =   5475
   End
   Begin VB.TextBox txtDescrizionePOSSuperarea 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6100
      MaxLength       =   21
      TabIndex        =   12
      Top             =   2280
      Width           =   2475
   End
   Begin VB.TextBox txtNomeSuperarea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6100
      MaxLength       =   30
      TabIndex        =   9
      Top             =   1200
      Width           =   3315
   End
   Begin VB.TextBox txtCodiceSuperarea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6100
      MaxLength       =   2
      TabIndex        =   10
      Top             =   1560
      Width           =   435
   End
   Begin VB.TextBox txtDescrizioneAlternativaSuperarea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6100
      MaxLength       =   30
      TabIndex        =   11
      Top             =   1920
      Width           =   3315
   End
   Begin VB.ListBox lstSuperaree 
      Height          =   2010
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   4095
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8340
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   240
      Width           =   3255
   End
   Begin VB.Label lblStampaIngresso 
      Alignment       =   1  'Right Justify
      Caption         =   "Stampa ingresso (area)"
      Height          =   255
      Left            =   4200
      TabIndex        =   22
      Top             =   3420
      Width           =   1800
   End
   Begin VB.Label Label1 
      Caption         =   "Ogni superarea conterr� una sola area non numerata avente lo stesso nome della superarea."
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   4860
      Width           =   7575
   End
   Begin VB.Label Label6 
      Caption         =   "Superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   840
      Width           =   4095
   End
   Begin VB.Label lblDescrizioneSuperarea 
      Alignment       =   1  'Right Justify
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   4200
      TabIndex        =   18
      Top             =   2640
      Width           =   1800
   End
   Begin VB.Label lblDescrizionePOSSuperarea 
      Alignment       =   1  'Right Justify
      Caption         =   "Descrizione POS"
      Height          =   255
      Left            =   4200
      TabIndex        =   4
      Top             =   2280
      Width           =   1800
   End
   Begin VB.Label lblNomeSuperarea 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome"
      Height          =   255
      Left            =   4200
      TabIndex        =   0
      Top             =   1200
      Width           =   1800
   End
   Begin VB.Label lblDescrizioneAlternativaSuperarea 
      Alignment       =   1  'Right Justify
      Caption         =   "Descrizione alternativa"
      Height          =   255
      Left            =   4200
      TabIndex        =   1
      Top             =   1920
      Width           =   1800
   End
   Begin VB.Label lblCodiceSuperarea 
      Alignment       =   1  'Right Justify
      Caption         =   "Codice"
      Height          =   255
      Left            =   4200
      TabIndex        =   2
      Top             =   1560
      Width           =   1800
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione della pianta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8340
      TabIndex        =   6
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmStrutturaPiantaCapienzaIllimitata"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idSuperareaSelezionata As Long
Private idPiantaSelezionata As Long
Private descrizioneAlternativaRecordSelezionato As String
Private descrizionePOS As String
Private codiceRecordSelezionato As String
Private descrizioneStampaIngresso As String
Private nomeRecordSelezionato As String
Private nomePiantaSelezionata As String
Private numeroVersioneArea As Long
Private isAttributiAreaModificati As Boolean

Private gestioneExitCode As ExitCodeEnum
Private gestioneSuperarea As AzioneSuGrigliaEnum
Private gestioneArea As AzioneSuGrigliaEnum
Private tipoArea As TipoAreaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pianta"
    txtInfo1.Text = nomePiantaSelezionata
    txtInfo1.Enabled = False
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetNomePiantaSelezionata(nome As String)
    nomePiantaSelezionata = nome
End Sub

Public Sub Init()
    isAttributiAreaModificati = False
    idSuperareaSelezionata = idNessunElementoSelezionato
    Call InitListaSuperaree
    Call DisabilitaSuperarea
    gestioneSuperarea = ASG_NON_SPECIFICATO
    gestioneArea = ASG_NON_SPECIFICATO
    
    Call AggiornaAbilitazioneControlli

    Call Me.Show(vbModal)
End Sub

Public Sub InitListaSuperaree()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long

    lstSuperaree.Clear
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT IDAREA AS ""ID"", NOME, CODICE FROM AREA" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA = " & TA_SUPERAREA_NON_NUMERATA & _
        " ORDER BY NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstSuperaree.AddItem "(" & rec("ID") & ") " & rec("CODICE") & " - " & rec("NOME")
            lstSuperaree.ItemData(i) = rec("ID")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub cmdAnnullaSuperarea_Click()
    Call AnnullaSuperarea
End Sub

Private Sub cmdConfermaSuperarea_Click()
    ConfermaSuperarea
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub cmdNuovaSuperarea_Click()
    Call NuovaSuperarea
End Sub

Private Sub cmdEliminaSuperarea_Click()
    Call EliminaSuperarea
End Sub

Private Sub lstSuperaree_Click()
    idSuperareaSelezionata = lstSuperaree.ItemData(lstSuperaree.ListIndex)
    gestioneSuperarea = ASG_MODIFICA
    Call CaricaSuperarea
    Call AbilitaSuperarea
End Sub

Private Sub AbilitaSuperarea()
    
    lblNomeSuperarea.Enabled = True
    txtNomeSuperarea.Enabled = True
    lblCodiceSuperarea.Enabled = True
    txtCodiceSuperarea.Enabled = True
    lblDescrizioneSuperarea.Enabled = True
    txtDescrizioneSuperarea.Enabled = True
    lblDescrizioneAlternativaSuperarea.Enabled = True
    txtDescrizioneAlternativaSuperarea.Enabled = True
    lblDescrizionePOSSuperarea.Enabled = True
    txtDescrizionePOSSuperarea.Enabled = True
    lblStampaIngresso.Enabled = True
    txtStampaIngresso.Enabled = True
    
    cmdConfermaSuperarea.Enabled = True
    cmdEliminaSuperarea.Enabled = True
    cmdAnnullaSuperarea.Enabled = True
    
End Sub

Private Sub DisabilitaSuperarea()
    
    lblNomeSuperarea.Enabled = False
    txtNomeSuperarea.Enabled = False
    lblCodiceSuperarea.Enabled = False
    txtCodiceSuperarea.Enabled = False
    lblDescrizioneSuperarea.Enabled = False
    txtDescrizioneSuperarea.Enabled = False
    lblDescrizioneAlternativaSuperarea.Enabled = False
    txtDescrizioneAlternativaSuperarea.Enabled = False
    lblDescrizionePOSSuperarea.Enabled = False
    txtDescrizionePOSSuperarea.Enabled = False
    lblStampaIngresso.Enabled = False
    txtStampaIngresso.Enabled = False

    cmdConfermaSuperarea.Enabled = False
    cmdEliminaSuperarea.Enabled = False
    cmdAnnullaSuperarea.Enabled = False

End Sub
Private Sub PulisciSuperarea()
    
    txtNomeSuperarea.Text = ""
    txtCodiceSuperarea.Text = ""
    txtDescrizioneSuperarea.Text = ""
    txtDescrizioneAlternativaSuperarea.Text = ""
    txtDescrizionePOSSuperarea.Text = ""
    txtStampaIngresso.Text = ""

End Sub

Private Sub InizializzaSuperarea()
    
    txtNomeSuperarea.Text = "AREA UNICA"
    txtCodiceSuperarea.Text = "01"
    txtDescrizioneSuperarea.Text = "AREA UNICA"
    txtDescrizioneAlternativaSuperarea.Text = "AREA UNICA"
    txtDescrizionePOSSuperarea.Text = "AREA UNICA"
    txtStampaIngresso.Text = "AREA UNICA"

End Sub

Private Sub CaricaSuperarea()
    Dim sql As String
    Dim rec As OraDynaset
    
    Call PulisciSuperarea
    If idSuperareaSelezionata <> idNessunElementoSelezionato Then
        Call ApriConnessioneBD_ORA
        
        sql = "SELECT SA.NOME, SA.CODICE, SA.DESCRIZIONE, SA.DESCRIZIONEALTERNATIVA, SA.DESCRIZIONEPOS, A.DESCRIZIONESTAMPAINGRESSO" & _
            " FROM AREA SA, AREA A" & _
            " WHERE SA.IDAREA = " & idSuperareaSelezionata & _
            " AND SA.IDAREA = A.IDAREA_PADRE"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            txtNomeSuperarea.Text = rec("NOME")
            txtCodiceSuperarea.Text = rec("CODICE")
            If Not IsNull(rec("DESCRIZIONE")) Then
                txtDescrizioneSuperarea.Text = rec("DESCRIZIONE")
            End If
            If Not IsNull(rec("DESCRIZIONEALTERNATIVA")) Then
                txtDescrizioneAlternativaSuperarea.Text = rec("DESCRIZIONEALTERNATIVA")
            End If
            If Not IsNull(rec("DESCRIZIONEPOS")) Then
                txtDescrizionePOSSuperarea.Text = rec("DESCRIZIONEPOS")
            End If
            If Not IsNull(rec("DESCRIZIONESTAMPAINGRESSO")) Then
                txtStampaIngresso.Text = rec("DESCRIZIONESTAMPAINGRESSO")
            End If
        End If
        rec.Close
        
        Call ChiudiConnessioneBD_ORA
    End If
End Sub

Private Sub NuovaSuperarea()
    gestioneSuperarea = ASG_INSERISCI_NUOVO
    gestioneArea = ASG_NON_SPECIFICATO
    idSuperareaSelezionata = idNessunElementoSelezionato
    Call InizializzaSuperarea
    Call AbilitaSuperarea
End Sub

Private Sub ConfermaSuperarea()
    
    Select Case gestioneSuperarea
        Case ASG_INSERISCI_NUOVO
            If idSuperareaSelezionata = idNessunElementoSelezionato Then
                Call InserisciSuperareaNellaBaseDati
                Call InitListaSuperaree
            End If
        Case ASG_MODIFICA
            If idSuperareaSelezionata <> idNessunElementoSelezionato Then
                Call ModificaSuperareaNellaBaseDati
                Call InitListaSuperaree
            End If
        Case Else
    End Select
    
    Call DisabilitaSuperarea
End Sub

Private Sub EliminaSuperarea()
    Select Case gestioneSuperarea
        Case ASG_MODIFICA
            If idSuperareaSelezionata <> idNessunElementoSelezionato Then
                Call EliminaSuperareaNellaBaseDati
                Call InitListaSuperaree
            End If
        Case Else
    End Select
    
    Call DisabilitaSuperarea
End Sub

Private Sub AnnullaSuperarea()
    Call PulisciSuperarea
    gestioneSuperarea = ASG_NON_SPECIFICATO
    gestioneArea = ASG_NON_SPECIFICATO
    
    Call DisabilitaSuperarea
End Sub

Private Sub InserisciSuperareaNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idNuovaSuperarea As Long
    
On Error GoTo InserisciSuperareaNellaBaseDati_errore
    idNuovaSuperarea = OttieniIdentificatoreDaSequenza("SQ_AREA")
    
    Call ApriConnessioneBD_ORA
    
    ORADB.BeginTrans

    ' superarea
    sql = "INSERT INTO AREA (IDAREA, IDTIPOAREA, IDPIANTA, NOME, CODICE, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, DESCRIZIONESTAMPAINGRESSO, CAPIENZA, NUMEROVERSIONE, NUMERATAAPOSTOUNICO)" & _
        " VALUES (" & idNuovaSuperarea & _
        ", " & TA_SUPERAREA_NON_NUMERATA & _
        ", " & idPiantaSelezionata & _
        ", '" & txtNomeSuperarea.Text & "'" & _
        ", '" & txtCodiceSuperarea.Text & "'" & _
        ", '" & txtDescrizioneSuperarea.Text & "'" & _
        ", '" & txtDescrizioneAlternativaSuperarea.Text & "'" & _
        ", '" & txtDescrizionePOSSuperarea.Text & "'" & _
        ", ''" & _
        ", NULL" & _
        ", " & VB_VERO & _
        ", " & VB_FALSO & _
        ")"
    n = ORADB.ExecuteSQL(sql) ' da verificare l'unicit� del codice e se ci sono altri campi
    
    ' unica area
    sql = "INSERT INTO AREA (IDAREA, IDTIPOAREA, IDPIANTA, IDAREA_PADRE, NOME, CODICE, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, DESCRIZIONESTAMPAINGRESSO, CAPIENZA, NUMEROVERSIONE, NUMERATAAPOSTOUNICO)" & _
        " VALUES (SQ_AREA.NEXTVAL" & _
        ", " & TA_AREA_NON_NUMERATA & _
        ", " & idPiantaSelezionata & _
        ", " & idNuovaSuperarea & _
        ", '" & Left(("AREA " & txtNomeSuperarea.Text), 30) & "'" & _
        ", '" & txtCodiceSuperarea.Text & "0'" & _
        ", '" & Left(("AREA " & txtDescrizioneSuperarea.Text), 255) & "'" & _
        ", '" & Left(("AREA " & txtDescrizioneAlternativaSuperarea.Text), 30) & "'" & _
        ", '" & Left(("AREA " & txtDescrizionePOSSuperarea.Text), 21) & "'" & _
        ", '" & txtStampaIngresso.Text & "'" & _
        ", NULL" & _
        ", " & VB_VERO & _
        ", " & VB_FALSO & _
        ")"
    n = ORADB.ExecuteSQL(sql) ' da verificare l'unicit� del codice e se ci sono altri campi
    
    ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA
    Exit Sub
    
InserisciSuperareaNellaBaseDati_errore:
    MsgBox "Si � verificato un errore (probabile campo duplicato): operazione non eseguita."
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub ModificaSuperareaNellaBaseDati()
    Dim sql As String
    Dim n As Long
    
On Error GoTo ModificaSuperareaNellaBaseDati_errore
    Call ApriConnessioneBD_ORA
    
    ORADB.BeginTrans
    
    ' superarea
    sql = "UPDATE AREA SET NOME='" & txtNomeSuperarea.Text & "'" & _
        ", CODICE='" & txtCodiceSuperarea.Text & "'" & _
        ", DESCRIZIONE='" & txtDescrizioneSuperarea.Text & "'" & _
        ", DESCRIZIONEALTERNATIVA='" & txtDescrizioneAlternativaSuperarea.Text & "'" & _
        ", DESCRIZIONEPOS='" & txtDescrizionePOSSuperarea.Text & "'" & _
        " WHERE IDAREA = " & idSuperareaSelezionata
    n = ORADB.ExecuteSQL(sql)
    
    ' unica area
    sql = "UPDATE AREA SET NOME='" & Left(("AREA " & txtNomeSuperarea.Text), 30) & "'" & _
        ", CODICE='" & txtCodiceSuperarea.Text & "0'" & _
        ", DESCRIZIONE='" & Left(("AREA " & txtDescrizioneSuperarea.Text), 255) & "'" & _
        ", DESCRIZIONEALTERNATIVA='" & Left(("AREA " & txtDescrizioneAlternativaSuperarea.Text), 30) & "'" & _
        ", DESCRIZIONEPOS='" & Left(("AREA " & txtDescrizionePOSSuperarea.Text), 21) & "'" & _
        ", DESCRIZIONESTAMPAINGRESSO='" & txtStampaIngresso.Text & "'" & _
        " WHERE IDAREA_PADRE = " & idSuperareaSelezionata
    n = ORADB.ExecuteSQL(sql)

    ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    Exit Sub
    
ModificaSuperareaNellaBaseDati_errore:
    MsgBox "Si � verificato un errore (probabile campo duplicato): operazione non eseguita."
    Call ChiudiConnessioneBD_ORA
End Sub

'Private Sub ModificaAreaNellaBaseDati()
'    Dim sql As String
'    Dim n As Long
'
'On Error GoTo ModificaAreaNellaBaseDati_errore
'    Call ApriConnessioneBD_ORA
'
'    sql = "UPDATE AREA SET NOME='" & txtNomeArea.Text & "'" & _
'        ", CODICE='" & txtCodiceArea.Text & "'" & _
'        ", DESCRIZIONE='" & txtDescrizioneArea.Text & "'" & _
'        ", DESCRIZIONEALTERNATIVA='" & txtDescrizioneAlternativaArea.Text & "'" & _
'        ", DESCRIZIONEPOS='" & txtDescrizionePOSArea.Text & "'" & _
'        ", DESCRIZIONESTAMPAINGRESSO='" & txtDescrizioneStampaIngressoArea.Text & "'" & _
'        " WHERE IDAREA = " & idAreaSelezionata
'    n = ORADB.ExecuteSQL(sql)
'
'    Call ChiudiConnessioneBD_ORA
'    Exit Sub
'
'ModificaAreaNellaBaseDati_errore:
'    MsgBox "Si � verificato un errore (probabile campo duplicato): operazione non eseguita."
'    Call ChiudiConnessioneBD_ORA
'End Sub

'Private Sub InserisciAreaNellaBaseDati()
'    Dim sql As String
'    Dim n As Long
'
'On Error GoTo InserisciAreaNellaBaseDati_errore
'
'    Call ApriConnessioneBD_ORA
'
'    sql = "INSERT INTO AREA (IDAREA, IDTIPOAREA, IDPIANTA, IDAREA_PADRE, NOME, CODICE, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, DESCRIZIONESTAMPAINGRESSO, CAPIENZA, NUMEROVERSIONE, NUMERATAAPOSTOUNICO)" & _
'        " VALUES (SQ_AREA.NEXTVAL" & _
'        ", " & TA_AREA_NON_NUMERATA & _
'        ", " & idPiantaSelezionata & _
'        ", " & idSuperareaSelezionata & _
'        ", '" & txtNomeArea.Text & "'" & _
'        ", '" & txtCodiceArea.Text & "'" & _
'        ", '" & txtDescrizioneArea.Text & "'" & _
'        ", '" & txtDescrizioneAlternativaArea.Text & "'" & _
'        ", '" & txtDescrizionePOSArea.Text & "'" & _
'        ", '" & txtDescrizioneStampaIngressoArea.Text & "'" & _
'        ", NULL" & _
'        ", " & VB_VERO & _
'        ", " & VB_FALSO & _
'        ")"
'    n = ORADB.ExecuteSQL(sql) ' da verificare l'unicit� del codice e se ci sono altri campi
'
'    Call ChiudiConnessioneBD_ORA
'    Exit Sub
'
'InserisciAreaNellaBaseDati_errore:
'    MsgBox "Si � verificato un errore (probabile campo duplicato): operazione non eseguita."
'    Call ChiudiConnessioneBD_ORA
'End Sub

Private Sub EliminaSuperareaNellaBaseDati()
    Dim sql As String
    Dim rec As OraDynaset
    Dim n As Long
    Dim risposta As VbMsgBoxResult
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM AREA A, TITOLO T" & _
        " WHERE IDAREA_PADRE = " & idSuperareaSelezionata & _
        " AND A.IDAREA = T.IDAREA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") > 0 Then
            MsgBox "Impossibile cancellare la superarea, esistono titoli emessi"
        Else
            risposta = MsgBox("Sicuro di voler cancellare la superarea e tutte le sue aree? Il processo potrebbe richiedere molto tempo.", vbYesNo)
            If risposta = vbYes Then
                ORADB.BeginTrans
                
                sql = "DELETE FROM AREA WHERE IDAREA_PADRE = " & idSuperareaSelezionata
                n = ORADB.ExecuteSQL(sql) ' da verificare l'unicit� del codice e se ci sono altri campi
                sql = "DELETE FROM AREA WHERE IDAREA = " & idSuperareaSelezionata
                n = ORADB.ExecuteSQL(sql)
                
                ORADB.CommitTrans
            End If
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD_ORA
End Sub

