VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfigurazioneProdottoAbilitazionePuntiVendita 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   11145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13335
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11145
   ScaleWidth      =   13335
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmAggiornamento 
      Caption         =   "Aggiornamento"
      Height          =   6975
      Left            =   120
      TabIndex        =   15
      Top             =   3240
      Width           =   13095
      Begin VB.ComboBox cmbClassiSuperaree 
         Height          =   315
         Left            =   240
         TabIndex        =   35
         Text            =   "Combo1"
         Top             =   3360
         Width           =   1935
      End
      Begin VB.CommandButton cmdSelezionaFile 
         Caption         =   "Importa PV da ..."
         Height          =   375
         Left            =   8040
         TabIndex        =   33
         Top             =   3000
         Width           =   1395
      End
      Begin VB.CommandButton cmdConfermaAggiornamento 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   120
         TabIndex        =   28
         Top             =   6360
         Width           =   1035
      End
      Begin VB.CommandButton cmdAnnullaAggiornamento 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1200
         TabIndex        =   27
         Top             =   6360
         Width           =   1035
      End
      Begin VB.ListBox lstClassiPVDaModificare 
         Height          =   2205
         Left            =   240
         TabIndex        =   26
         Top             =   600
         Width           =   1935
      End
      Begin VB.ListBox lstRegioniDisponibili 
         Enabled         =   0   'False
         Height          =   2400
         Left            =   2400
         TabIndex        =   25
         Top             =   600
         Width           =   1695
      End
      Begin VB.ListBox lstProvinceDisponibili 
         Enabled         =   0   'False
         Height          =   2400
         Left            =   4200
         TabIndex        =   24
         Top             =   600
         Width           =   1695
      End
      Begin VB.ListBox lstComuniDisponibili 
         Enabled         =   0   'False
         Height          =   2400
         Left            =   6000
         TabIndex        =   23
         Top             =   600
         Width           =   1695
      End
      Begin VB.ListBox lstPuntiVenditaDisponibili 
         Height          =   2400
         Left            =   8040
         MultiSelect     =   2  'Extended
         TabIndex        =   22
         Top             =   600
         Width           =   4935
      End
      Begin VB.ListBox lstPuntiVenditaSelezionati 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2400
         Left            =   8040
         MultiSelect     =   2  'Extended
         TabIndex        =   21
         Top             =   4080
         Width           =   4935
      End
      Begin VB.ListBox lstComuniSelezionati 
         Enabled         =   0   'False
         Height          =   2400
         Left            =   6000
         TabIndex        =   20
         Top             =   4080
         Width           =   1695
      End
      Begin VB.ListBox lstProvinceSelezionate 
         Enabled         =   0   'False
         Height          =   2400
         Left            =   4200
         TabIndex        =   19
         Top             =   4080
         Width           =   1695
      End
      Begin VB.ListBox lstRegioniSelezionate 
         Enabled         =   0   'False
         Height          =   2400
         Left            =   2400
         TabIndex        =   18
         Top             =   4080
         Width           =   1695
      End
      Begin VB.CommandButton cmdAbilita 
         Caption         =   "Abilita"
         Height          =   375
         Left            =   9480
         TabIndex        =   17
         Top             =   3000
         Width           =   3495
      End
      Begin VB.CommandButton cmdDisabilita 
         Caption         =   "Disabilita"
         Height          =   375
         Left            =   8040
         TabIndex        =   16
         Top             =   6480
         Width           =   4935
      End
      Begin MSComDlg.CommonDialog cdlFileImport 
         Left            =   7560
         Top             =   3000
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label Label3 
         Caption         =   "Abilita PV selezionati per"
         Height          =   255
         Left            =   240
         TabIndex        =   34
         Top             =   3120
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Punti vendita non abilitati"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2400
         TabIndex        =   32
         Top             =   240
         Width           =   3435
      End
      Begin VB.Label Label2 
         Caption         =   "Punti vendita abilitati"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2400
         TabIndex        =   31
         Top             =   3720
         Width           =   3555
      End
      Begin VB.Label lblInsiemePuntiVenditaDisponibili 
         Caption         =   "Insieme punti vendita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8040
         TabIndex        =   30
         Top             =   120
         Width           =   4875
      End
      Begin VB.Label lblInsiemePuntiVenditaSelezionati 
         Caption         =   "Insieme punti vendita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8040
         TabIndex        =   29
         Top             =   3600
         Width           =   4875
      End
   End
   Begin VB.Frame frmPuntiVenditaConfigurati 
      Caption         =   "Punti vendita configurati"
      Height          =   2535
      Left            =   120
      TabIndex        =   12
      Top             =   600
      Width           =   13095
      Begin VB.ListBox lstClassiPV 
         Height          =   2010
         Left            =   240
         TabIndex        =   14
         Top             =   360
         Width           =   4935
      End
      Begin VB.ListBox lstPuntiVenditaConfigurati 
         Height          =   2010
         Left            =   5280
         TabIndex        =   13
         Top             =   360
         Width           =   7695
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   10140
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9780
      TabIndex        =   5
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   11520
      TabIndex        =   4
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   9120
      TabIndex        =   0
      Top             =   10200
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle abilitazioni dei punti vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   9780
      TabIndex        =   10
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   11520
      TabIndex        =   9
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoAbilitazionePuntiVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private listaOperatoriDisponibiliNonDiRicevitoria As Collection
Private listaOperatoriSelezionatiNonDiRicevitoria As Collection
Private listaPuntiVenditaDisponibili As Collection
Private listaPuntiVenditaSelezionati As Collection
Private idClasseSuperareeProdottoSelezionata As Long

Private idClassePVSelezionata As Long
Private idClassePVDaModificareSelezionata As Long

Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum
Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
        
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Successivo"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
    ' tanto non servono
    cmdConferma.Enabled = False
    cmdAnnulla.Enabled = False
End Sub

Public Sub Init()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    
    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    
    idClasseSuperareeProdottoSelezionata = idTuttiGliElementiSelezionati
    Call initListaClassiPV
    Call CaricaComboClassiSuperaree
'    Call initOptionZoneImpianto
    Call initListaClassiPVDaModificare
    Call AggiornaAbilitazioneControlli
    internalEvent = internalEventOld
    Call Me.Show(vbModal)
End Sub

Private Sub initListaClassiPV()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
        
    sql = "SELECT IDCLASSEPUNTOVENDITA, DESCRIZIONE FROM CLASSEPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstClassiPV.AddItem(rec("DESCRIZIONE"), i)
            lstClassiPV.ItemData(i) = rec("IDCLASSEPUNTOVENDITA")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call lstClassiPV.AddItem("Tutti", i)
    lstClassiPV.ItemData(i) = idNessunElementoSelezionato

End Sub
'
'Private Sub initOptionZoneImpianto()
'    chkInteroImpianto.Visible = True
'    chkInteroImpianto.Value = VB_VERO
'
'    cmbClassiSuperaree.Enabled = False
'End Sub

Private Sub initListaClassiPVDaModificare()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    sql = "SELECT IDCLASSEPUNTOVENDITA, NOME FROM CLASSEPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstClassiPVDaModificare.AddItem(rec("NOME"), i)
            lstClassiPVDaModificare.ItemData(i) = rec("IDCLASSEPUNTOVENDITA")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub chkInteroImpianto_Click()
'    If Not internalEvent Then
'        If chkInteroImpianto.Value = VB_FALSO Then
'            Call CaricaComboClassiSuperaree
'            lstPuntiVenditaDisponibili.Clear
'            lstPuntiVenditaSelezionati.Clear
'        Else
'            cmbClassiSuperaree.Clear
'            cmbClassiSuperaree.Enabled = False
'            Call PopolaListaPuntiVenditaDisponibili
'            Call PopolaListaPuntiVenditaSelezionati
'        End If
'    End If
    If lstClassiPVDaModificare.ListCount > 0 Then
        Call PopolaListaPuntiVenditaDisponibili
        Call PopolaListaPuntiVenditaSelezionati
    End If
End Sub
        
Private Sub CaricaComboClassiSuperaree()
    Dim sql As String
    
    cmbClassiSuperaree.Enabled = True
    sql = "SELECT CSP.IDCLASSESUPERAREAPRODOTTO ID, CS.NOME" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESUPERAREA CS" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND CSP.IDCLASSESUPERAREA = CS.IDCLASSESUPERAREA" & _
        " ORDER BY -CS.CLASSEPRINCIPALE, CS.NOME"
'    Call CaricaValoriCombo2(cmbClassiSuperaree, sql, "ID", False)
    Call CaricaValoriComboConOpzioneTuttiIniziale(cmbClassiSuperaree, sql, "ID", "Intero impianto")
    cmbClassiSuperaree.ListIndex = 0
End Sub

Private Sub cmbClassiSuperaree_Click()
    Call SelezionaClasseSuperarea
End Sub

Private Sub SelezionaClasseSuperarea()
    If lstClassiPVDaModificare.SelCount > 0 Then
        idClasseSuperareeProdottoSelezionata = cmbClassiSuperaree.ItemData(cmbClassiSuperaree.ListIndex)
        Call PopolaListaPuntiVenditaDisponibili
        Call PopolaListaPuntiVenditaSelezionati
    End If
End Sub

Private Sub cmdAnnullaAggiornamento_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call AnnullaAggiornamento
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConfermaAggiornamento_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfermaAggiornamento
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdSelezionaFile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ImportaPuntiVendita
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    criteriSelezioneImpostati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoClassiSuperAree.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoClassiSuperAree.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
'        If isConfigurabile Then
'            Call SetGestioneExitCode(EC_CONFERMA)
'            Call AggiornaAbilitazioneControlli
'            Call InserisciNellaBaseDati
'            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, stringaNota, idProdottoSelezionato)
'            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'            Call AggiornaAbilitazioneControlli
'        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
'    Call CaricaFormAbilitazioniSupportiDigitali
    Call CaricaFormConfigurazioneProdottoDateOperazioni
End Sub
'
'Private Sub CaricaFormAbilitazioniSupportiDigitali()
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdProdottoSelezionato(idProdottoSelezionato)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdPiantaSelezionata(idPiantaSelezionata)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetModalitāForm(A_NUOVO)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
'    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.Init
'End Sub

Private Sub CaricaFormConfigurazioneProdottoDateOperazioni()
    Call frmConfigurazioneProdottoDateOperazioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoDateOperazioni.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoDateOperazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoDateOperazioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoDateOperazioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoDateOperazioni.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoDateOperazioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoDateOperazioni.Init
End Sub
   
Private Sub ImportaPuntiVendita()
    SelezionaTuttiDaDisabilitare
    Call Disabilita
    Call ApriFileImportazionePuntiVendita
End Sub

Private Sub ApriFileImportazionePuntiVendita()
    
On Error GoTo gestioneErroreAperturaFileExcel

    DoEvents
    
    cdlFileImport.InitDir = App.Path
    cdlFileImport.Filter = "Excel (*.xls)|*.xls|File di testo (*.txt)|*.txt"
    'cdlFileImport.DefaultExt = "xls"
    cdlFileImport.Flags = cdlOFNFileMustExist Or cdlOFNExplorer
    cdlFileImport.FileName = ""
    cdlFileImport.CancelError = True
    cdlFileImport.ShowOpen
    
    nomeFileImportazione = cdlFileImport.FileName
    DoEvents
    If Mid(nomeFileImportazione, Len(nomeFileImportazione) - 2, 3) = "xls" Then
        Call LeggiPuntiVenditaDaFileExcel
    Else
        If Mid(nomeFileImportazione, Len(nomeFileImportazione) - 2, 3) = "txt" Then
            Call LeggiPuntiVenditaDaFileTXT
        Else
            MsgBox "Tipo di file non riconosciuto"
        End If
    End If
    
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
    
    Exit Sub
    
gestioneErroreAperturaFileExcel:
    'do nothing
End Sub

Private Sub LeggiPuntiVenditaDaFileExcel()
    Dim workbook As workbook
    Dim workSheet As workSheet
    Dim nomePuntoVendita As String
    Dim idPuntoVendita As Long
    Dim i As Long
    Dim numeroNulli As Long
    Dim numeroValori As Long
    Dim rangeTotale As range
    Dim quantitaPuntiVenditaInseriti As Long
    Dim quantitaDatiErrati As Long
    Dim quantitaDatiRipetuti As Long
    Dim listaErrori As String
    
    Set workbook = excImportazione.Workbooks.Open(nomeFileImportazione)
    Set workSheet = workbook.ActiveSheet
    
On Error Resume Next
    
    i = 1
    nomePuntoVendita = ""
    quantitaPuntiVenditaInseriti = 0
    quantitaDatiErrati = 0
    listaErrori = ""
    
    Set rangeTotale = workSheet.range("A:A")
    numeroNulli = excImportazione.WorksheetFunction.CountBlank(rangeTotale)
    numeroValori = 65536 - numeroNulli
    
    While workSheet.Rows.Cells(i, 1) <> ""
        nomePuntoVendita = workSheet.Rows.Cells(i, 1)
        idPuntoVendita = getIdPuntoVendita(nomePuntoVendita)
        If idPuntoVendita = idNessunElementoSelezionato Then
            quantitaDatiErrati = quantitaDatiErrati + 1
            listaErrori = listaErrori & " " & nomePuntoVendita
        Else
            AbilitaPuntoVendita (idPuntoVendita)
            quantitaPuntiVenditaInseriti = quantitaPuntiVenditaInseriti + 1
        End If
        i = i + 1
        DoEvents
    Wend
    MsgBox "Trovati " & quantitaPuntiVenditaInseriti & " punti vendita; trovati " & quantitaDatiErrati & " errori (" & listaErrori & ")"
    
    Call excImportazione.Quit
End Sub

Private Sub LeggiPuntiVenditaDaFileTXT()
    Dim nomePuntoVendita As String
    Dim idPuntoVendita As Long
    Dim numeroValori As Long
    Dim rangeTotale As range
    Dim quantitaPuntiVenditaInseriti As Long, quantitaDatiErrati, quantitaDatiRipetuti As Long
    Dim listaErrori As String
    
    Dim riga As String
    
On Error Resume Next
    nomePuntoVendita = ""
    quantitaPuntiVenditaInseriti = 0
    quantitaDatiErrati = 0
    quantitaDatiRipetuti = 0
    listaErrori = ""

    Open nomeFileImportazione For Input As #1
    While Not EOF(1)
        Line Input #1, nomePuntoVendita
        idPuntoVendita = getIdPuntoVendita(nomePuntoVendita)
        If idPuntoVendita = idNessunElementoSelezionato Then
            quantitaDatiErrati = quantitaDatiErrati + 1
            listaErrori = listaErrori & " " & nomePuntoVendita
        Else
            AbilitaPuntoVendita (idPuntoVendita)
            quantitaPuntiVenditaInseriti = quantitaPuntiVenditaInseriti + 1
        End If
    Wend
    Close #1
    
End Sub

Private Sub ClearListe()
    lstRegioniDisponibili.Clear
    lstRegioniSelezionate.Clear
    lstProvinceDisponibili.Clear
    lstProvinceSelezionate.Clear
    lstComuniDisponibili.Clear
    lstComuniSelezionati.Clear
    lstPuntiVenditaDisponibili.Clear
    lstPuntiVenditaSelezionati.Clear
End Sub

Private Sub AnnullaAggiornamento()
    ' se c'č qualcosa in sospeso viene annullato e si comincia una nuova transazione
    ORADB.Rollback
    ORADB.BeginTrans
    Call ClearListe
End Sub

Private Sub ConfermaAggiornamento()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            ' se c'č qualcosa in sospeso viene confermato e si comincia una nuova transazione
            ORADB.CommitTrans
            ORADB.BeginTrans
            Call ClearListe
            PopolaListaPuntiVenditaConfigurati
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub lstClassiPV_Click()
    Dim numeroElementiSelezionati As Long
    
    idClassePVSelezionata = lstClassiPV.ItemData(lstClassiPV.ListIndex)
    Call PopolaListaPuntiVenditaConfigurati
    
End Sub

' Popola la lista dei punti vendita configurati
Private Sub PopolaListaPuntiVenditaConfigurati()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim s As String
    Dim idPuntoVendita As Long

    Call lstPuntiVenditaConfigurati.Clear
    idPuntoVendita = idNessunElementoSelezionato
    
    If idClassePVSelezionata = idNessunElementoSelezionato Then
        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA, PV.NOME, C.NOME COMUNE, P.SIGLA, CSA.NOME CLASSESA, CSA.CLASSEPRINCIPALE" & _
            " FROM PUNTOVENDITA PV, CLASSESAPROD_PUNTOVENDITA CSAPPV, CLASSESUPERAREAPRODOTTO CSAP, CLASSESUPERAREA CSA, COMUNE C, PROVINCIA P" & _
            " WHERE CSAP.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND CSAP.IDCLASSESUPERAREA = CSA.IDCLASSESUPERAREA" & _
            " AND CSAP.IDCLASSESUPERAREAPRODOTTO = CSAPPV.IDCLASSESUPERAREAPRODOTTO" & _
            " AND CSAPPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " AND PV.IDCOMUNE = C.IDCOMUNE (+)" & _
            " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
            " ORDER BY PV.NOME, -CSA.CLASSEPRINCIPALE, CSA.NOME"
    Else
        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA, PV.NOME, C.NOME COMUNE, P.SIGLA, CSA.NOME CLASSESA, CSA.CLASSEPRINCIPALE" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, CLASSESAPROD_PUNTOVENDITA CSAPPV, CLASSESUPERAREAPRODOTTO CSAP, CLASSESUPERAREA CSA, COMUNE C, PROVINCIA P" & _
            " WHERE OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " AND OCPV.IDPUNTOVENDITA = CSAPPV.IDPUNTOVENDITA" & _
            " AND CSAPPV.IDCLASSESUPERAREAPRODOTTO = CSAP.IDCLASSESUPERAREAPRODOTTO" & _
            " AND CSAP.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND CSAP.IDCLASSESUPERAREA = CSA.IDCLASSESUPERAREA" & _
            " AND PV.IDCOMUNE = C.IDCOMUNE (+)" & _
            " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
            " ORDER BY PV.NOME, -CSA.CLASSEPRINCIPALE, CSA.NOME"
    End If

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        s = ""
        While Not rec.EOF
            If rec("IDPUNTOVENDITA") <> idPuntoVendita Then
                If idPuntoVendita <> idNessunElementoSelezionato Then
                    Call lstPuntiVenditaConfigurati.AddItem(s, i)
                    i = i + 1
                End If
                idPuntoVendita = rec("IDPUNTOVENDITA")
                s = rec("NOME") & " - " & rec("COMUNE") & " (" & rec("SIGLA") & "): " & rec("CLASSESA")
            Else
                s = s & ", " & rec("CLASSESA")
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call lstPuntiVenditaConfigurati.AddItem(s, i)

End Sub

Private Sub lstClassiPVDaModificare_Click()
    Dim numeroElementiSelezionati As Long
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
        
    idClassePVDaModificareSelezionata = lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex)
    If idClassePVDaModificareSelezionata = TCPV_INTERNET Or idClassePVDaModificareSelezionata = TCPV_CALL_CENTER_LIS Or idClassePVDaModificareSelezionata = TCPV_ALTRI_CALL_CENTER Then
        lstRegioniDisponibili.Clear
        lstRegioniSelezionate.Clear
        lstProvinceDisponibili.Clear
        lstProvinceSelezionate.Clear
        lstComuniDisponibili.Clear
        lstComuniSelezionati.Clear
    Else
        Call PopolaListeRegioni
    End If
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
    
    internalEvent = internalEventOld
End Sub

Private Sub PopolaListeRegioni()
    Dim sql As String
    
    sql = "SELECT IDREGIONE ID, NOME FROM REGIONE ORDER BY NOME"
    Call PopolaLista(lstRegioniDisponibili, sql)
    Call PopolaLista(lstRegioniSelezionate, sql)
End Sub

Private Sub PopolaListePVDisponibiliConClassePV(idClassePV)
    Dim sql As String
    
'    If chkInteroImpianto.Value = VB_VERO Then
    If idClasseSuperareeProdottoSelezionata = idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV," & _
            " (" & _
            " SELECT DISTINCT IDPUNTOVENDITA" & _
            " FROM CLASSESAPROD_PUNTOVENDITA CSAPPV, CLASSESUPERAREAPRODOTTO CSAP" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND CSAP.IDCLASSESUPERAREAPRODOTTO = CSAPPV.IDCLASSESUPERAREAPRODOTTO" & _
            " ) T" & _
            " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
            " AND T.IDPUNTOVENDITA IS NULL" & _
            " AND PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
            " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " ORDER BY NOME"
    Else
        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV," & _
            " (" & _
            " SELECT DISTINCT IDPUNTOVENDITA" & _
            " FROM CLASSESAPROD_PUNTOVENDITA" & _
            " WHERE IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareeProdottoSelezionata & _
            " ) T" & _
            " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
            " AND T.IDPUNTOVENDITA IS NULL" & _
            " AND PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
            " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " ORDER BY NOME"
    End If
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili"
End Sub

Private Sub PopolaListePVSelezionatiConClassePV(idClassePV)
    Dim sql As String
    
'    If chkInteroImpianto.Value = VB_VERO Then
    If idClasseSuperareeProdottoSelezionata = idTuttiGliElementiSelezionati Then
'        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
'            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, CLASSESAPROD_PUNTOVENDITA CSAPPV, CLASSESUPERAREAPRODOTTO CSAP" & _
'            " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
'            " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
'            " AND OCPVPV.IDPUNTOVENDITA = CSAPPV.IDPUNTOVENDITA" & _
'            " AND CSAPPV.IDCLASSESUPERAREAPRODOTTO = CSAP.IDCLASSESUPERAREAPRODOTTO" & _
'            " AND CSAP.IDPRODOTTO = " & idProdottoSelezionato & _
'            " GROUP BY PV.IDPUNTOVENDITA, PV.NOME" & _
'            " HAVING COUNT(*) >= (SELECT COUNT(*) FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & ")" & _
'            " ORDER BY NOME"
        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
            " FROM PUNTOVENDITA PV," & _
            " (" & _
            " SELECT IDPUNTOVENDITA" & _
            " FROM" & _
            " (" & _
            " SELECT DISTINCT CSAPPV.IDCLASSESUPERAREAPRODOTTO, CSAPPV.IDPUNTOVENDITA" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, CLASSESAPROD_PUNTOVENDITA CSAPPV, CLASSESUPERAREAPRODOTTO CSAP" & _
            " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
            " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
            " AND OCPVPV.IDPUNTOVENDITA = CSAPPV.IDPUNTOVENDITA" & _
            " AND CSAPPV.IDCLASSESUPERAREAPRODOTTO = CSAP.IDCLASSESUPERAREAPRODOTTO" & _
            " AND CSAP.IDPRODOTTO = " & idProdottoSelezionato & _
            " )" & _
            " GROUP BY IDPUNTOVENDITA" & _
            " HAVING COUNT(*) >= (" & _
            " SELECT COUNT(*) FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " )" & _
            " ) P" & _
            " WHERE PV.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
            " ORDER BY NOME"
    Else
        sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, CLASSESAPROD_PUNTOVENDITA CSAPPV" & _
            " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
            " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
            " AND OCPVPV.IDPUNTOVENDITA = CSAPPV.IDPUNTOVENDITA" & _
            " AND CSAPPV.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareeProdottoSelezionata & _
            " ORDER BY NOME"
    End If
    
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita della classe selezionata"
End Sub

Private Sub PopolaListePVDisponibiliConRegione(idRegione)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, COMUNE C, PROVINCIA P," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM PRODOTTO_PUNTOVENDITA WHERE IDPRODOTTO = " & idProdottoSelezionato & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " AND PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
        " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = P.IDPROVINCIA" & _
        " AND P.IDREGIONE = " & idRegione & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili nella regione selezionata"
    
End Sub

Private Sub PopolaListePVSelezionatiConRegione(idRegione)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, PRODOTTO_PUNTOVENDITA PPV, COMUNE C, PROVINCIA P" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND PV.IDPUNTOVENDITA = PPV.IDPUNTOVENDITA" & _
        " AND PPV.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = P.IDPROVINCIA" & _
        " AND P.IDREGIONE = " & idRegione & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita della regione selezionata"
    
End Sub

Private Sub PopolaListePVDisponibiliConProvincia(idProvincia)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, COMUNE C," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM PRODOTTO_PUNTOVENDITA WHERE IDPRODOTTO = " & idProdottoSelezionato & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " AND PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
        " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = " & idProvincia & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili nella provincia selezionata"
    
End Sub

Private Sub PopolaListePVSelezionatiConProvincia(idProvincia)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, PRODOTTO_PUNTOVENDITA PPV, COMUNE C" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND PV.IDPUNTOVENDITA = PPV.IDPUNTOVENDITA" & _
        " AND PPV.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = " & idProvincia & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita della provincia selezionata"
    
End Sub

Private Sub PopolaListePVDisponibiliConComune(idComune)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM PRODOTTO_PUNTOVENDITA WHERE IDPRODOTTO = " & idProdottoSelezionato & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " AND PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
        " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND IDCOMUNE = " & idComune & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili nel comune selezionato"
    
End Sub

Private Sub PopolaListePVSelezionatiConComune(idComune)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, PRODOTTO_PUNTOVENDITA PPV" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND PV.IDPUNTOVENDITA = PPV.IDPUNTOVENDITA" & _
        " AND PPV.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDCOMUNE = " & idComune & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita del comune selezionato"
    
End Sub

Private Sub lstRegioniDisponibili_Click()
    Dim sql As String
    
    sql = "SELECT IDPROVINCIA ID, NOME FROM PROVINCIA WHERE IDREGIONE = " & lstRegioniDisponibili.ItemData(lstRegioniDisponibili.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstProvinceDisponibili, sql)
    lstComuniDisponibili.Clear
    Call PopolaListePVDisponibiliConRegione(lstRegioniDisponibili.ItemData(lstRegioniDisponibili.ListIndex))
End Sub

Private Sub lstRegioniSelezionate_Click()
    Dim sql As String
    
    sql = "SELECT IDPROVINCIA ID, NOME FROM PROVINCIA WHERE IDREGIONE = " & lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstProvinceSelezionate, sql)
    lstComuniSelezionati.Clear
    Call PopolaListePVSelezionatiConRegione(lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex))
End Sub

Private Sub lstProvinceDisponibili_Click()
    Dim sql As String
    
    sql = "SELECT IDCOMUNE ID, NOME FROM COMUNE WHERE IDPROVINCIA = " & lstProvinceDisponibili.ItemData(lstProvinceDisponibili.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstComuniDisponibili, sql)
    Call PopolaListePVDisponibiliConProvincia(lstProvinceDisponibili.ItemData(lstProvinceDisponibili.ListIndex))
End Sub

Private Sub lstProvinceSelezionate_Click()
    Dim sql As String
    
    sql = "SELECT IDCOMUNE ID, NOME FROM COMUNE WHERE IDPROVINCIA = " & lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstComuniSelezionati, sql)
    Call PopolaListePVSelezionatiConProvincia(lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex))
End Sub

Private Sub lstComuniDisponibili_Click()
    Call PopolaListaPuntiVenditaDisponibili
End Sub

Private Sub PopolaListaPuntiVenditaDisponibili()
    
    If lstComuniDisponibili.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVDisponibiliConComune (lstComuniDisponibili.ItemData(lstComuniDisponibili.ListIndex))
    ElseIf lstProvinceDisponibili.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVDisponibiliConProvincia (lstProvinceDisponibili.ItemData(lstProvinceDisponibili.ListIndex))
    ElseIf lstRegioniDisponibili.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVDisponibiliConRegione (lstRegioniDisponibili.ItemData(lstRegioniDisponibili.ListIndex))
    Else
        PopolaListePVDisponibiliConClassePV (lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
    End If

End Sub

Private Sub lstComuniSelezionati_Click()
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub PopolaListaPuntiVenditaSelezionati()
    
    If lstComuniSelezionati.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVSelezionatiConComune (lstComuniSelezionati.ItemData(lstComuniSelezionati.ListIndex))
    ElseIf lstProvinceSelezionate.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVSelezionatiConProvincia (lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex))
    ElseIf lstRegioniSelezionate.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVSelezionatiConRegione (lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex))
    Else
        PopolaListePVSelezionatiConClassePV (lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
    End If

End Sub

Private Sub cmdAbilita_Click()
    Call Abilita
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub cmdDisabilita_Click()
    Call Disabilita
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub Abilita()
'    Dim sql As String
'    Dim n As Long
    Dim idPuntoVendita As Long
    Dim i As Long
    Dim aggiungiDiritti As Boolean
    
    If idClasseSuperareeProdottoSelezionata <> idNessunElementoSelezionato Then
        aggiungiDiritti = classePuntoVenditaConDirittiOperatoreGestibili(idClassePVSelezionata)
        
        For i = 0 To lstPuntiVenditaDisponibili.ListCount - 1
            If lstPuntiVenditaDisponibili.Selected(i) = True Then
                idPuntoVendita = lstPuntiVenditaDisponibili.ItemData(i)
    
                Call AbilitaPuntoVendita(idPuntoVendita)
    '            If chkInteroImpianto.Value = VB_VERO Then
    '                sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
    '                    " SELECT IDCLASSESUPERAREAPRODOTTO, " & idPuntoVendita & _
    '                    " FROM CLASSESUPERAREAPRODOTTO" & _
    '                    " WHERE IDPRODOTTO = " & idProdottoSelezionato
    '            Else
    '                sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
    '                    " VALUES (" & idClasseSuperareeProdottoSelezionata & ", " & idPuntoVendita & ")"
    '            End If
    '            n = ORADB.ExecuteSQL(sql)
            End If
        Next i
    End If

End Sub

Private Function AbilitaPuntoVendita(idPV As Long) As Long
    Dim sql As String
    Dim n As Long
    
    If idClasseSuperareeProdottoSelezionata = idTuttiGliElementiSelezionati Then
        sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
            " WHERE IDPUNTOVENDITA = " & idPV & _
            " AND IDCLASSESUPERAREAPRODOTTO IN" & _
            " (" & _
            " SELECT IDCLASSESUPERAREAPRODOTTO" & _
            " FROM CLASSESUPERAREAPRODOTTO" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " )"
        n = ORADB.ExecuteSQL(sql)
    
        sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
            " SELECT IDCLASSESUPERAREAPRODOTTO, " & idPV & _
            " FROM CLASSESUPERAREAPRODOTTO" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
        n = ORADB.ExecuteSQL(sql)
    Else
        sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
            " WHERE IDPUNTOVENDITA = " & idPV & _
            " AND IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareeProdottoSelezionata
        n = ORADB.ExecuteSQL(sql)
        
        sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
            " VALUES (" & idClasseSuperareeProdottoSelezionata & ", " & idPV & ")"
        n = ORADB.ExecuteSQL(sql)
    End If
    
End Function

Private Sub Disabilita()
    Dim sql As String
    Dim n As Long
    Dim idPuntoVendita As Long
    Dim i As Long
    
    If idClasseSuperareeProdottoSelezionata <> idNessunElementoSelezionato Then
        For i = 0 To lstPuntiVenditaSelezionati.ListCount - 1
            If lstPuntiVenditaSelezionati.Selected(i) = True Then
                idPuntoVendita = lstPuntiVenditaSelezionati.ItemData(i)
    
    '            If chkInteroImpianto.Value = VB_VERO Then
                If idClasseSuperareeProdottoSelezionata = idTuttiGliElementiSelezionati Then
                    sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
                        " WHERE IDPUNTOVENDITA = " & idPuntoVendita & _
                        " AND IDCLASSESUPERAREAPRODOTTO IN" & _
                        " (" & _
                        " SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                        " )"
                Else
                    sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
                        " WHERE IDPUNTOVENDITA = " & idPuntoVendita & _
                        " AND IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareeProdottoSelezionata
                End If
                n = ORADB.ExecuteSQL(sql)
                
            End If
        Next i
    End If

End Sub

Private Sub SelezionaTuttiDaDisabilitare()
    Dim i As Long
    
    For i = 0 To lstPuntiVenditaSelezionati.ListCount - 1
        lstPuntiVenditaSelezionati.Selected(i) = True
    Next i

End Sub

