VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneProdottoDateOperazioni 
   Caption         =   "Configurazione prodotto date operazioni"
   ClientHeight    =   11295
   ClientLeft      =   165
   ClientTop       =   165
   ClientWidth     =   13500
   LinkTopic       =   "Form1"
   ScaleHeight     =   11295
   ScaleWidth      =   13500
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVerificaDirittiOperatore 
      Caption         =   "Verifica diritti operatore"
      Height          =   375
      Left            =   120
      TabIndex        =   53
      Top             =   5280
      Width           =   2535
   End
   Begin VB.CommandButton cmdSelezionaTutti 
      Caption         =   "Seleziona tutti"
      Height          =   375
      Left            =   5040
      TabIndex        =   51
      Top             =   2520
      Width           =   2535
   End
   Begin VB.CheckBox chkVisualizzaTuttiITipiOperazione 
      Caption         =   "Visualizza tutti i tipi operazione"
      Height          =   375
      Left            =   5040
      TabIndex        =   50
      Top             =   2040
      Width           =   2535
   End
   Begin VB.ComboBox cmbClassiSuperaree 
      Height          =   315
      Left            =   5040
      TabIndex        =   46
      Top             =   840
      Width           =   2535
   End
   Begin VB.ListBox lstClassiPuntoVendita 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   120
      TabIndex        =   44
      Top             =   840
      Width           =   4815
   End
   Begin VB.ListBox lstTipiOperazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   39
      Top             =   3240
      Visible         =   0   'False
      Width           =   13275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Catalogo WEB (le modifiche non necessitano di riavvi)"
      Height          =   855
      Left            =   120
      TabIndex        =   26
      Top             =   6000
      Width           =   11475
      Begin VB.CheckBox chkVisibileSuPortaleSport 
         Alignment       =   1  'Right Justify
         Caption         =   "Visibile Portale Sport"
         Height          =   255
         Left            =   9480
         TabIndex        =   54
         Top             =   360
         Width           =   1815
      End
      Begin VB.TextBox txtDescrizioneCatalogoWEB 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         MaxLength       =   255
         TabIndex        =   28
         Top             =   300
         Width           =   6495
      End
      Begin VB.CheckBox chkVisibileSuCatalogoWEB 
         Alignment       =   1  'Right Justify
         Caption         =   "Visibile Listicket"
         Height          =   255
         Left            =   7560
         TabIndex        =   27
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblDescrizioneWEB 
         Alignment       =   1  'Right Justify
         Caption         =   "Descr."
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   855
      Left            =   4080
      TabIndex        =   17
      Top             =   9960
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   19
         Top             =   240
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   240
         TabIndex        =   18
         Top             =   240
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   11760
      TabIndex        =   16
      Top             =   300
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9960
      TabIndex        =   15
      Top             =   300
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   9240
      TabIndex        =   11
      Top             =   10320
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   795
      Left            =   4080
      TabIndex        =   8
      Top             =   9120
      Width           =   2775
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   1440
         TabIndex        =   9
         Top             =   240
         Width           =   1155
      End
   End
   Begin VB.Frame fraDateDurate 
      Caption         =   "Inizio / fine operazioni"
      Height          =   1095
      Left            =   6960
      TabIndex        =   0
      Top             =   9120
      Width           =   6315
      Begin VB.TextBox txtMinutiFine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5640
         MaxLength       =   2
         TabIndex        =   31
         Top             =   660
         Width           =   435
      End
      Begin VB.TextBox txtOraFine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5040
         MaxLength       =   2
         TabIndex        =   30
         Top             =   660
         Width           =   435
      End
      Begin VB.TextBox txtOraInizio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5040
         MaxLength       =   2
         TabIndex        =   2
         Top             =   300
         Width           =   435
      End
      Begin VB.TextBox txtMinutiInizio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5640
         MaxLength       =   2
         TabIndex        =   1
         Top             =   300
         Width           =   435
      End
      Begin MSComCtl2.DTPicker dtpDataInizio 
         Height          =   315
         Left            =   1620
         TabIndex        =   3
         Top             =   300
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   87293953
         CurrentDate     =   37684.7534259259
      End
      Begin MSComCtl2.DTPicker dtpDataFine 
         Height          =   315
         Left            =   1620
         TabIndex        =   32
         Top             =   660
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   87293953
         CurrentDate     =   37684.7534259259
      End
      Begin VB.Label lblOraFine 
         Alignment       =   1  'Right Justify
         Caption         =   "ora (HH:MM)"
         Height          =   255
         Left            =   4020
         TabIndex        =   36
         Top             =   720
         Width           =   915
      End
      Begin VB.Label lblSeparatoreOreMinutiFine 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         TabIndex        =   35
         Top             =   660
         Width           =   75
      End
      Begin VB.Label lblDataFine 
         Alignment       =   1  'Right Justify
         Caption         =   "data"
         Height          =   255
         Left            =   1080
         TabIndex        =   34
         Top             =   720
         Width           =   435
      End
      Begin VB.Label lblInizio 
         Caption         =   "Inizio:"
         Height          =   255
         Left            =   180
         TabIndex        =   33
         Top             =   360
         Width           =   915
      End
      Begin VB.Label lblFine 
         Caption         =   "Fine:"
         Height          =   255
         Left            =   180
         TabIndex        =   7
         Top             =   720
         Width           =   915
      End
      Begin VB.Label lblDataInizio 
         Alignment       =   1  'Right Justify
         Caption         =   "data"
         Height          =   255
         Left            =   1080
         TabIndex        =   6
         Top             =   360
         Width           =   435
      End
      Begin VB.Label lblSeparatoreOreMinutiInizio 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         TabIndex        =   5
         Top             =   300
         Width           =   75
      End
      Begin VB.Label lblOraInizio 
         Alignment       =   1  'Right Justify
         Caption         =   "ora (HH:MM)"
         Height          =   255
         Left            =   4020
         TabIndex        =   4
         Top             =   360
         Width           =   915
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoDateOperazioniCatalogoWEB 
      Height          =   330
      Left            =   11880
      Top             =   6360
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoDateOperazioniCatalogoWEB 
      Bindings        =   "frmConfigurazioneProdottoDateOperazioni.frx":0000
      Height          =   1995
      Left            =   120
      TabIndex        =   20
      Top             =   6960
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   3519
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCorrispondenze 
      Caption         =   "Corrispondenze tra classi punto vendita e canali di vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   52
      Top             =   1080
      Width           =   5475
   End
   Begin VB.Label lblClasseSuperareaSecondaria 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8040
      TabIndex        =   49
      Top             =   3000
      Width           =   4095
   End
   Begin VB.Label lblClasseSuperareaPrincipale 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3360
      TabIndex        =   48
      Top             =   3000
      Width           =   3975
   End
   Begin VB.Label lblTipiOperazione 
      Caption         =   "Tipi operazione"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   47
      Top             =   3000
      Width           =   2895
   End
   Begin VB.Label Label3 
      Caption         =   "Classi superarea"
      Height          =   255
      Left            =   5040
      TabIndex        =   45
      Top             =   600
      Width           =   2415
   End
   Begin VB.Label lblWEBsuInternet 
      Caption         =   """INTERNET"": canale di vendita Internet"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   37
      Top             =   2100
      Width           =   5415
   End
   Begin VB.Label lblTOTEMsuAltriCallCenter 
      Caption         =   """ALTRI CALL CENTER"": altri Call Center"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   43
      Top             =   2700
      Width           =   5415
   End
   Begin VB.Label lblWEBsuCallCenterLIS 
      Caption         =   """CALL CENTER LIS"": Call Center LIS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   42
      Top             =   2400
      Width           =   5415
   End
   Begin VB.Label lblPOSsuReteIstituzionale 
      Caption         =   """RETE LISTICKET"": canale di vendita rete LIS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   41
      Top             =   1800
      Width           =   5415
   End
   Begin VB.Label lblPOSsuReteLIS 
      Caption         =   """RETE LISTICKET"": canale di vendita rete istituzionale"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   40
      Top             =   1500
      Width           =   5475
   End
   Begin VB.Label Label1 
      Caption         =   "Classi punto vendita"
      Height          =   255
      Left            =   120
      TabIndex        =   38
      Top             =   600
      Width           =   2415
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione date operazioni e catalogo WEB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   60
      TabIndex        =   25
      Top             =   60
      Width           =   7755
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   11760
      TabIndex        =   24
      Top             =   60
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   9960
      TabIndex        =   23
      Top             =   60
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   495
      Left            =   1440
      TabIndex        =   22
      Top             =   9060
      Width           =   2535
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Oper in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   60
      TabIndex        =   21
      Top             =   9060
      Width           =   1335
   End
   Begin VB.Menu mnuAzioneSuOperazione 
      Caption         =   "Azione su operazione"
      Visible         =   0   'False
      Begin VB.Menu mnuModificaOperazione 
         Caption         =   "Modifica operazione"
      End
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoDateOperazioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Su PRODOTTO_CANALEDIVENDITA se c'č la riga ci devono essere entrambe le date

Option Explicit

Private internalEvent As Boolean

Private idClassePuntoVenditaSelezionata As Long
Private dataInizioOperazioneSelezionata_Principale(1 To NUMERO_TIPI_OPERAZIONI) As Date
Private dataFineOperazioneSelezionata_Principale(1 To NUMERO_TIPI_OPERAZIONI) As Date
Private dataInizioOperazioneSelezionata_Secondaria(1 To NUMERO_TIPI_OPERAZIONI) As Date
Private dataFineOperazioneSelezionata_Secondaria(1 To NUMERO_TIPI_OPERAZIONI) As Date
Private elencoIdTipiOperazione(1 To NUMERO_TIPI_OPERAZIONI) As String
Private elencoStringheDateOperazione(1 To NUMERO_TIPI_OPERAZIONI) As String
Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
Private dataOraTermine As Date
Private durataRiservazioni As Long
Private durataPeriodoScadenzaForzata As Long
Private isProdottoAttivoSuTL As Boolean
Private rateo As Long
'Private isRecordEditabile As Boolean
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private idClasseSuperareaProdottoPrincipale As Long
Private nomeClasseSuperareaProdottoPrincipale As String
Private idClasseSuperareaProdottoSecondaria As Long
Private nomeClasseSuperareaProdottoSecondaria As String

Private dataOraInizio As Date
Private dataOraInizioOld As Date
Private dataOraFine As Date
Private dataOraFineOld As Date

Private isProdottoVisibileSuCatalogoWEB As ValoreBooleanoEnum
Private isProdottoVisibileSuPortaleSport As ValoreBooleanoEnum
Private descrizioneCatalogoWEB As String

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    lblInfo1.Enabled = True
    lblInfo2.Enabled = True
    
    dgrConfigurazioneProdottoDateOperazioniCatalogoWEB.Caption = "DATE OPERAZIONI PER CATALOGO WEB"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtOraInizio.Text = ""
        txtOraFine.Text = ""
        txtMinutiInizio.Text = ""
        txtMinutiFine.Text = ""
        dtpDataInizio.Value = Null
        dtpDataFine.Value = Null
        dtpDataInizio.Enabled = False
        dtpDataFine.Enabled = False
        txtOraInizio.Enabled = False
        txtOraFine.Enabled = False
        txtMinutiInizio.Enabled = False
        txtMinutiFine.Enabled = False
        lblDataInizio.Enabled = False
        lblDataFine.Enabled = False
        lblOraInizio.Enabled = False
        lblOraFine.Enabled = False
        lblInizio.Enabled = False
        lblFine.Enabled = False
        lblSeparatoreOreMinutiInizio.Enabled = False
        lblSeparatoreOreMinutiFine.Enabled = False
        lblOraInizio.Enabled = False
        lblOraFine.Enabled = False
        
        chkVisibileSuCatalogoWEB.Enabled = True
        chkVisibileSuPortaleSport.Enabled = True
        txtDescrizioneCatalogoWEB.Enabled = isProdottoVisibileSuCatalogoWEB Or isProdottoVisibileSuPortaleSport
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB.Enabled = isProdottoVisibileSuCatalogoWEB
        
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = Not ((isProdottoVisibileSuCatalogoWEB = VB_VERO Or isProdottoVisibileSuPortaleSport = VB_VERO) And txtDescrizioneCatalogoWEB.Text = "")
        cmdAnnulla.Enabled = Not ((isProdottoVisibileSuCatalogoWEB = VB_VERO Or isProdottoVisibileSuPortaleSport = VB_VERO) And txtDescrizioneCatalogoWEB.Text = "")
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB.Enabled = False
        fraDateDurate.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        dtpDataInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        dtpDataFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblDataInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblDataFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtOraInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataInizio.Value))
        txtOraFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataFine.Value))
        lblOraInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataInizio.Value))
        lblOraFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataFine.Value))
        lblSeparatoreOreMinutiInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataInizio.Value))
        lblSeparatoreOreMinutiFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataFine.Value))
        lblInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtMinutiInizio.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataInizio.Value))
        txtMinutiFine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And Not (IsNull(dtpDataFine.Value))
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Len(Trim(txtOraInizio.Text)) = 2 And Len(Trim(txtMinutiInizio.Text)) = 2 And Not IsNull(dtpDataInizio.Value)) And _
            (Len(Trim(txtOraFine.Text)) = 2 And Len(Trim(txtMinutiFine.Text)) = 2 And Not IsNull(dtpDataFine.Value))
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB.Enabled = True
        dtpDataFine.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtOraInizio.Text = ""
        txtOraFine.Text = ""
        txtMinutiInizio.Text = ""
        txtMinutiFine.Text = ""
        dtpDataInizio.Enabled = False
        dtpDataFine.Enabled = False
        dtpDataInizio.Value = Null
        dtpDataFine.Value = Null
        txtOraInizio.Enabled = False
        txtOraFine.Enabled = False
        txtMinutiInizio.Enabled = False
        txtMinutiFine.Enabled = False
        lblDataInizio.Enabled = False
        lblDataFine.Enabled = False
        lblOraInizio.Enabled = False
        lblOraFine.Enabled = False
        lblInizio.Enabled = False
        lblFine.Enabled = False
        lblSeparatoreOreMinutiInizio.Enabled = False
        lblSeparatoreOreMinutiFine.Enabled = False
        fraDateDurate.Enabled = False
        cmdModifica.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            If ((isProdottoVisibileSuCatalogoWEB = VB_VERO Or isProdottoVisibileSuPortaleSport = VB_VERO) And txtDescrizioneCatalogoWEB.Text = "") Then
                cmdSuccessivo.Enabled = False
            End If
            
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
            cmdEsci.Enabled = True
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub chkVisibileSuCatalogoWEB_Click()
    If Not internalEvent Then
        Call chkVisibileSuCatalogoWEB_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkVisibileSuPortaleSport_Click()
    If Not internalEvent Then
        Call chkVisibileSuPortaleSport_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkVisibileSuCatalogoWEB_Update()
    isProdottoVisibileSuCatalogoWEB = chkVisibileSuCatalogoWEB.Value
End Sub

Private Sub chkVisibileSuPortaleSport_Update()
    isProdottoVisibileSuPortaleSport = chkVisibileSuPortaleSport.Value
End Sub

Private Sub chkVisualizzaTuttiITipiOperazione_Click()
    visualizzaOperazioniPerClassePuntoVendita
End Sub

Private Sub cmbClassiSuperaree_Click()
    Call SelezionaClasseSuperareaProdotto
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato & _
        "; IDCANALEVENDITA = " & idRecordSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        Call SetGestioneExitCode(EC_CONFERMA)
        
        If valoriCampiOK Then
            Call ModificaBaseDati_Prodotto
        End If
        
        Select Case gestioneRecordGriglia
            Case ASG_MODIFICA
                If valoriCampiOK Then
                    Call ModificaBaseDati_DateCatalogoWEB
                End If
            Case ASG_ELIMINA
                EliminaDallaBaseDati
        End Select
        
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        Call dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
        
        Call AggiornaAbilitazioneControlli
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
        
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Dim stringaNota As String

    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
'            Call Esci
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
'    Dim rec As ADODB.Recordset
'    Dim internalEventOld As Boolean
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    Set rec = adcConfigurazioneProdottoDateOperazioni.Recordset
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        Do While Not rec.EOF
'            If id = rec("ID") Then
'                Exit Do
'            End If
'            rec.MoveNext
'        Loop
'    End If
'    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call GetIdRecordSelezionato
    Call GetDateRecordSelezionato
    dataOraInizioOld = dataOraInizio
    dataOraFineOld = dataOraFine
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionaTutti_Click()
    Call selezionaTuttiTipiOperazione
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdVerificaDirittiOperatore_Click()
    If idClassePuntoVenditaSelezionata <> idNessunElementoSelezionato Then
        Call frmConfigurazioneProdottoDirittiOperatore.Init(idOrganizzazioneSelezionata, idProdottoSelezionato, nomeProdottoSelezionato, idClassePuntoVenditaSelezionata, lstClassiPuntoVendita)
    End If
End Sub

Private Sub dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub Init()

    Call InitListaClassiPuntiVendita
    
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    idClassePuntoVenditaSelezionata = idNessunElementoSelezionato
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    Call dtpDataInizio_Init
    Call dtpDataFine_Init
    
    CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)

End Sub

Private Sub GetDateRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    dataOraInizio = dataNulla
    dataOraFine = dataNulla
    
    Set rec = adcConfigurazioneProdottoDateOperazioniCatalogoWEB.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
        If Not IsNull(rec("Inizio")) Then
            dataOraInizio = rec("Inizio")
        End If
        If Not IsNull(rec("Fine")) Then
            dataOraFine = rec("Fine")
        End If
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoDateOperazioniCatalogoWEB.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazioneProdottoDateOperazioniCatalogoWEB

    sql = "SELECT CV.IDCANALEDIVENDITA ""ID"", CV.DESCRIZIONE ""Canale di vendita"","
    sql = sql & " P_CV.DATAORAINIZIO ""Inizio"","
    sql = sql & " P_CV.DATAORAFINE ""Fine"""
    sql = sql & " FROM PRODOTTO_CANALEDIVENDITA P_CV, CANALEDIVENDITA CV"
    sql = sql & " WHERE CV.IDCANALEDIVENDITA = P_CV.IDCANALEDIVENDITA(+)"
    sql = sql & " AND IDPRODOTTO(+) = " & idProdottoSelezionato
    sql = sql & " ORDER BY ""Canale di vendita"""

    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh

    Set dgrConfigurazioneProdottoDateOperazioniCatalogoWEB.dataSource = d
    
    internalEvent = internalEventOld

End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT DESCRIZIONECATALOGOWEB, VISIBILESUCATALOGOWEB, VISIBILESUPORTALESPORT"
    sql = sql & " FROM PRODOTTO"
    sql = sql & " WHERE IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        descrizioneCatalogoWEB = IIf(IsNull(rec("DESCRIZIONECATALOGOWEB")), "", rec("DESCRIZIONECATALOGOWEB"))
        isProdottoVisibileSuCatalogoWEB = rec("VISIBILESUCATALOGOWEB")
        isProdottoVisibileSuPortaleSport = rec("VISIBILESUPORTALESPORT")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim hDur As Integer
    Dim hSca As Integer

    internalEventOld = internalEvent
    internalEvent = True

'    txtOraTermine.Text = ""
'    txtMinutiTermine.Text = ""
'
    If idRecordSelezionato <> idNessunElementoSelezionato Then
        If dataOraInizio = dataNulla Then
            'Do Nothing
        Else
            dtpDataInizio.Value = dataOraInizio
            txtOraInizio.Text = StringaOraMinuti(CStr(Hour(dataOraInizio)))
            txtMinutiInizio.Text = StringaOraMinuti(CStr(Minute(dataOraInizio)))
        End If
        
        If dataOraFine = dataNulla Then
            'Do Nothing
        Else
            dtpDataFine.Value = dataOraFine
            txtOraFine.Text = StringaOraMinuti(CStr(Hour(dataOraFine)))
            txtMinutiFine.Text = StringaOraMinuti(CStr(Minute(dataOraFine)))
        End If
    End If
    
    txtDescrizioneCatalogoWEB.Text = descrizioneCatalogoWEB
    chkVisibileSuCatalogoWEB.Value = isProdottoVisibileSuCatalogoWEB
    chkVisibileSuPortaleSport.Value = isProdottoVisibileSuPortaleSport
    
    internalEvent = internalEventOld

End Sub

Private Sub ModificaBaseDati_Prodotto()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim condizioniSQL As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErroriModificaBaseDati_Prodotto
    
    sql = "UPDATE PRODOTTO SET"
    sql = sql & " DESCRIZIONECATALOGOWEB = " & SqlStringValue(descrizioneCatalogoWEB) & ","
    sql = sql & " VISIBILESUCATALOGOWEB = " & isProdottoVisibileSuCatalogoWEB & ","
    sql = sql & " VISIBILESUPORTALESPORT = " & isProdottoVisibileSuPortaleSport
    sql = sql & " WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErroriModificaBaseDati_Prodotto:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub ModificaBaseDati_DateCatalogoWEB()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim condizioniSQL As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErroriModificaBaseDati_DateCatalogoWEB
    
    SETAConnection.BeginTrans
    
' o ci sono entrambe le date o la riga non va inserita
    
    sql = "DELETE FROM PRODOTTO_CANALEDIVENDITA " & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDCANALEDIVENDITA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
      
    sql = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE) " & _
        " VALUES (" & idProdottoSelezionato & ", " & _
        idRecordSelezionato & ", " & _
        SqlDateTimeValue(dataOraInizio) & "," & _
        SqlDateTimeValue(dataOraFine) & ")"
    SETAConnection.Execute sql, n, adCmdText
      
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErroriModificaBaseDati_DateCatalogoWEB:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
    sql = "DELETE FROM PRODOTTO_CANALEDIVENDITA" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDCANALEDIVENDITA = " & idRecordSelezionato
        
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD

End Sub

Private Sub dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoDateOperazioniCatalogoWEB
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 8000
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (8000)
    g.Columns(2).Width = ((dimensioneGrid - 700) / (numeroCampi - 1))
    g.Columns(3).Width = ((dimensioneGrid - 700) / (numeroCampi - 1))
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneStampeAggiuntive
End Sub

Private Sub CaricaFormConfigurazioneStampeAggiuntive()
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetRateo(rateo)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoStampeAggiuntive.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoStampeAggiuntive.Init
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoAbilitazionePuntiVendita.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoAbilitazionePuntiVendita.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Private Function valoriCampiOK() As Boolean
    Dim data As Date
    Dim ora As Integer
    Dim minuti As Integer
    Dim oraDurata As Integer
    Dim minutiDurata As Integer
    Dim oraDurataScadenza As Integer
    Dim minutiDurataScadenza As Integer
    Dim listaNonConformitā As Collection

    valoriCampiOK = True
    
    Set listaNonConformitā = New Collection

    descrizioneCatalogoWEB = Trim(txtDescrizioneCatalogoWEB.Text)
    
    If IsNull(dtpDataInizio.Value) Then
        dataOraInizio = dataNulla
    Else
        data = FormatDateTime(dtpDataInizio.Value, vbShortDate)
        If txtOraInizio.Text <> "" Then
            If IsCampoOraCorretto(txtOraInizio) Then
                ora = CInt(Trim(txtOraInizio.Text))
            Else
                valoriCampiOK = False
                Call listaNonConformitā.Add("- il valore immesso sul campo Ora inizio deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiInizio.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiInizio) Then
                minuti = CInt(Trim(txtMinutiInizio.Text))
            Else
                valoriCampiOK = False
                Call listaNonConformitā.Add("- il valore immesso sul campo Minuti inizio deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraInizio = FormatDateTime(data & " " & ora & ":" & minuti, vbGeneralDate)
    End If
    
    If IsNull(dtpDataFine.Value) Then
        dataOraFine = dataNulla
    Else
        data = FormatDateTime(dtpDataFine.Value, vbShortDate)
        If txtOraFine.Text <> "" Then
            If IsCampoOraCorretto(txtOraFine) Then
                ora = CInt(Trim(txtOraFine.Text))
            Else
                valoriCampiOK = False
                Call listaNonConformitā.Add("- il valore immesso sul campo Ora fine deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiFine.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiFine) Then
                minuti = CInt(Trim(txtMinutiFine.Text))
            Else
                valoriCampiOK = False
                Call listaNonConformitā.Add("- il valore immesso sul campo Minuti fine deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraFine = FormatDateTime(data & " " & ora & ":" & minuti, vbGeneralDate)
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub SelezionaClasseSuperareaProdotto()
    If Not internalEvent Then
        If cmbClassiSuperaree.ItemData(cmbClassiSuperaree.ListIndex) <> idClasseSuperareaProdottoPrincipale Then
            idClasseSuperareaProdottoSecondaria = cmbClassiSuperaree.ItemData(cmbClassiSuperaree.ListIndex)
            nomeClasseSuperareaProdottoSecondaria = cmbClassiSuperaree
            lblClasseSuperareaSecondaria.Caption = nomeClasseSuperareaProdottoSecondaria
        Else
            idClasseSuperareaProdottoSecondaria = idNessunElementoSelezionato
            nomeClasseSuperareaProdottoSecondaria = ""
            lblClasseSuperareaSecondaria.Caption = ""
        End If
        visualizzaOperazioniPerClassePuntoVendita
    End If
End Sub

Private Sub lstClassiPuntoVendita_Click()
    Dim i As Long
    
    lstTipiOperazione.Clear
    idClassePuntoVenditaSelezionata = idNessunElementoSelezionato
    For i = 1 To lstClassiPuntoVendita.ListCount
        If lstClassiPuntoVendita.Selected(i - 1) Then
            idClassePuntoVenditaSelezionata = lstClassiPuntoVendita.ItemData(i - 1)
        End If
    Next i
    
    If idClassePuntoVenditaSelezionata <> idNessunElementoSelezionato Then
        Call InitClassiSuperaree
'        Call visualizzaOperazioniPerClassePuntoVendita
'        Call aggiornaLegenda(idNessunElementoSelezionato)
    End If
    
End Sub
'
'Private Sub lstTipiOperazione_Click()
'    Dim i As Long
'
'    idTipoOperazioneSelezionato = idNessunElementoSelezionato
'    For i = 1 To lstTipiOperazione.ListCount
'        If lstTipiOperazione.Selected(i - 1) Then
'            idTipoOperazioneSelezionato = lstTipiOperazione.ItemData(i - 1)
'        End If
'    Next i
'
'End Sub

Private Sub lstTipiOperazione_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
        Call PopupMenu(mnuAzioneSuOperazione)
    End If
End Sub

Private Sub mnuAzioneSuOperazione_Click()
    Dim listaIdTO As Collection
    
    Set listaIdTO = inizializzaListaIdTipiOperazione
    If idProdottoSelezionato <> idNessunElementoSelezionato And listaIdTO.count > 0 Then
        Call frmDettagliDateOperazioneSingola.Init2(idOrganizzazioneSelezionata, idProdottoSelezionato, idClassePuntoVenditaSelezionata, idClasseSuperareaProdottoPrincipale, idClasseSuperareaProdottoSecondaria, nomeClasseSuperareaProdottoPrincipale, nomeClasseSuperareaProdottoSecondaria, listaIdTO, dataInizioOperazioneSelezionata_Principale(listaIdTO(1)), dataFineOperazioneSelezionata_Principale(listaIdTO(1)), dataInizioOperazioneSelezionata_Secondaria(listaIdTO(1)), dataFineOperazioneSelezionata_Secondaria(listaIdTO(1)))
        Call visualizzaOperazioniPerClassePuntoVendita
        adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    End If
End Sub

Private Sub mnuModificaOperazionePrincipale_Click()
    Dim listaIdTO As Collection
    
    Set listaIdTO = inizializzaListaIdTipiOperazione
    If idProdottoSelezionato <> idNessunElementoSelezionato And listaIdTO.count > 0 Then
        Call frmDettagliDateOperazioneSingola.Init(ADDO_MODIFICA_PRINCIPALE, idOrganizzazioneSelezionata, idProdottoSelezionato, idClassePuntoVenditaSelezionata, idClasseSuperareaProdottoPrincipale, nomeClasseSuperareaProdottoPrincipale, True, listaIdTO, dataInizioOperazioneSelezionata_Principale(listaIdTO(1)), dataFineOperazioneSelezionata_Principale(listaIdTO(1)))
        Call visualizzaOperazioniPerClassePuntoVendita
        adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    End If
End Sub

Private Sub mnuModificaOperazioneSecondaria_Click()
    Dim listaIdTO As Collection
    
    Set listaIdTO = inizializzaListaIdTipiOperazione
    If idProdottoSelezionato <> idNessunElementoSelezionato And listaIdTO.count > 0 And idClasseSuperareaProdottoSecondaria <> idNessunElementoSelezionato Then
        Call frmDettagliDateOperazioneSingola.Init(ADDO_MODIFICA_SECONDARIA, idOrganizzazioneSelezionata, idProdottoSelezionato, idClassePuntoVenditaSelezionata, idClasseSuperareaProdottoSecondaria, nomeClasseSuperareaProdottoSecondaria, False, listaIdTO, dataInizioOperazioneSelezionata_Secondaria(listaIdTO(1)), dataFineOperazioneSelezionata_Secondaria(listaIdTO(1)))
        Call visualizzaOperazioniPerClassePuntoVendita
        adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    End If
End Sub

Private Sub mnuEliminaOperazionePrincipale_Click()
    Dim listaIdTO As Collection
    
    Set listaIdTO = inizializzaListaIdTipiOperazione
    If idProdottoSelezionato <> idNessunElementoSelezionato And listaIdTO.count > 0 Then
        Call frmDettagliDateOperazioneSingola.Init(ADDO_ELIMINA_PRINCIPALE, idOrganizzazioneSelezionata, idProdottoSelezionato, idClassePuntoVenditaSelezionata, idClasseSuperareaProdottoPrincipale, nomeClasseSuperareaProdottoPrincipale, True, listaIdTO, dataInizioOperazioneSelezionata_Principale(listaIdTO(1)), dataFineOperazioneSelezionata_Principale(listaIdTO(1)))
        Call visualizzaOperazioniPerClassePuntoVendita
        adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    End If
End Sub

Private Sub mnuEliminaOperazioneSecondaria_Click()
    Dim listaIdTO As Collection
    
    Set listaIdTO = inizializzaListaIdTipiOperazione
    If idProdottoSelezionato <> idNessunElementoSelezionato And listaIdTO.count > 0 And idClasseSuperareaProdottoSecondaria <> idNessunElementoSelezionato Then
        Call frmDettagliDateOperazioneSingola.Init(ADDO_ELIMINA_SECONDARIA, idOrganizzazioneSelezionata, idProdottoSelezionato, idClassePuntoVenditaSelezionata, idClasseSuperareaProdottoSecondaria, nomeClasseSuperareaProdottoSecondaria, False, listaIdTO, dataInizioOperazioneSelezionata_Secondaria(listaIdTO(1)), dataFineOperazioneSelezionata_Secondaria(listaIdTO(1)))
        Call visualizzaOperazioniPerClassePuntoVendita
        adcConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
        dgrConfigurazioneProdottoDateOperazioniCatalogoWEB_Init
    End If
End Sub

Private Sub txtDescrizioneCatalogoWEB_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub InitListaClassiPuntiVendita()
    Dim sql As String
    
    sql = "SELECT IDCLASSEPUNTOVENDITA ID, DESCRIZIONE AS NOME FROM CLASSEPUNTOVENDITA ORDER BY IDCLASSEPUNTOVENDITA"
    Call CaricaValoriLista(lstClassiPuntoVendita, sql)
End Sub
    
Private Sub InitClassiSuperaree()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    idClasseSuperareaProdottoPrincipale = idNessunElementoSelezionato
    idClasseSuperareaProdottoSecondaria = idNessunElementoSelezionato
    cmbClassiSuperaree.Clear
    i = 0
    
    Call ApriConnessioneBD_ORA
       
' come primo elemento mettiamo sempre il principale, se c'e'
    sql = "SELECT DISTINCT CSP.IDCLASSESUPERAREAPRODOTTO ID, CS.NOME" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESUPERAREA CS, CLASSESAPROD_PUNTOVENDITA CSAP_PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND CSP.IDCLASSESUPERAREA = CS.IDCLASSESUPERAREA" & _
        " AND CS.CLASSEPRINCIPALE = 1" & _
        " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND CSP.IDCLASSESUPERAREAPRODOTTO = CSAP_PV.IDCLASSESUPERAREAPRODOTTO" & _
        " AND CSAP_PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If Not rec.EOF Then
            cmbClassiSuperaree.AddItem (rec("NOME"))
            cmbClassiSuperaree.ItemData(i) = rec("ID")
            idClasseSuperareaProdottoPrincipale = rec("ID")
            nomeClasseSuperareaProdottoPrincipale = rec("NOME")
            i = i + 1
        End If
    End If
    rec.Close
        
    sql = "SELECT DISTINCT CSP.IDCLASSESUPERAREAPRODOTTO ID, CS.NOME" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESUPERAREA CS, CLASSESAPROD_PUNTOVENDITA CSAP_PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND CSP.IDCLASSESUPERAREA = CS.IDCLASSESUPERAREA" & _
        " AND CS.CLASSEPRINCIPALE = 0" & _
        " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND CSP.IDCLASSESUPERAREAPRODOTTO = CSAP_PV.IDCLASSESUPERAREAPRODOTTO" & _
        " AND CSAP_PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            cmbClassiSuperaree.AddItem (rec("NOME"))
            cmbClassiSuperaree.ItemData(i) = rec("ID")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    If cmbClassiSuperaree.ListCount > 0 Then
        cmbClassiSuperaree.ListIndex = 0
    End If
    lblClasseSuperareaPrincipale.Caption = nomeClasseSuperareaProdottoPrincipale
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub dtpDataInizio_Init()
    dtpDataFine.MinDate = dataNulla
End Sub

Private Sub dtpDataInizio_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dtpDataFine_Init()
    dtpDataFine.MinDate = dataNulla
End Sub

Private Sub dtpDataFine_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraInizio_Change()
    If Not internalEvent Then
        If Len(txtOraInizio.Text) = 2 Then
            txtMinutiInizio.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiInizio_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtOraFine_Change()
    If Not internalEvent Then
        If Len(txtOraFine.Text) = 2 Then
            txtMinutiFine.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiFine_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetIdStagioneSelezionata(idS As Long)
    idStagioneSelezionata = idS
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call GetIdRecordSelezionato
    Call GetDateRecordSelezionato
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub visualizzaOperazioniPerClassePuntoVendita()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim j As Long
    Dim nome As String
    Dim s As String
    
    lstTipiOperazione.Clear
    lstTipiOperazione.Visible = True
    
    For i = 1 To NUMERO_TIPI_OPERAZIONI
        elencoStringheDateOperazione(i) = ""
        elencoIdTipiOperazione(i) = idNessunElementoSelezionato
    Next i
    
    Call ApriConnessioneBD_ORA
    
'    sql = "SELECT O.IDTIPOOPERAZIONE ID, O.NOME, PCPVTO.DATAORAINIZIOVALIDITA, PCPVTO.DATAORAFINEVALIDITA" & _
'        " FROM TIPOOPERAZIONE O, PRODOTTO_CLASSEPV_TIPOOPERAZ PCPVTO, CLASSESUPERAREAPRODOTTO CSAP, CLASSESUPERAREA CSA" & _
'        " WHERE O.IDTIPOOPERAZIONE > 0" & _
'        " AND O.IDTIPOOPERAZIONE = PCPVTO.IDTIPOOPERAZIONE(+)" & _
'        " AND IDCLASSEPUNTOVENDITA(+) = " & idClassePuntoVenditaSelezionata & _
'        " AND PCPVTO.IDCLASSESUPERAREAPRODOTTO = CSAP.IDCLASSESUPERAREAPRODOTTO(+)" & _
'        " AND CSAP.IDCLASSESUPERAREA = CSA.IDCLASSESUPERAREA(+)" & _
'        " AND CSA.CLASSEPRINCIPALE(+) = 1" & _
'        " ORDER BY O.NOME"
    sql = "SELECT ID, NOME, MIN (DATAORAINIZIOVALIDITA) DATAORAINIZIOVALIDITA, MAX(DATAORAFINEVALIDITA) DATAORAFINEVALIDITA" & _
        " FROM" & _
        " (" & _
        " SELECT IDTIPOOPERAZIONE ID, NOME, NULL DATAORAINIZIOVALIDITA, NULL DATAORAFINEVALIDITA" & _
        " FROM TIPOOPERAZIONE" & _
        " UNION" & _
        " SELECT O.IDTIPOOPERAZIONE ID, O.NOME, PCPVTO.DATAORAINIZIOVALIDITA, PCPVTO.DATAORAFINEVALIDITA" & _
        " FROM TIPOOPERAZIONE O, PRODOTTO_CLASSEPV_TIPOOPERAZ PCPVTO" & _
        " WHERE O.IDTIPOOPERAZIONE > 0" & _
        " AND O.IDTIPOOPERAZIONE = PCPVTO.IDTIPOOPERAZIONE" & _
        " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
        " AND PCPVTO.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoPrincipale & _
        ")" & _
        " WHERE ID > 0"
    
    If chkVisualizzaTuttiITipiOperazione.Value = vbUnchecked Then
        If idClassePuntoVenditaSelezionata = 3 Then ' altri PV organizzatore
            sql = sql & " AND ID IN (9,7,6,4,3)"
        End If
        If idClassePuntoVenditaSelezionata = 4 Then ' affiliati
            sql = sql & " AND ID IN (9,7,6,4,3)"
        End If
        If idClassePuntoVenditaSelezionata = 5 Then ' rete listicket
            sql = sql & " AND ID IN (9,7,6,4,3)"
        End If
        If idClassePuntoVenditaSelezionata = 6 Then ' internet
            sql = sql & " AND ID IN (2,5,6,9,11,12,23,24,31,36,37)"
        End If
        If idClassePuntoVenditaSelezionata = 7 Then ' call center lis
            sql = sql & " AND ID IN (2,3,4,5,6,7,9,11,12,20)"
        End If
        If idClassePuntoVenditaSelezionata = 8 Then ' altri CC
            sql = sql & " AND ID IN (2,5,9,11,12)"
        End If
    End If
    
    sql = sql & " GROUP BY ID, NOME" & _
        " ORDER BY NOME"

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            nome = lPad(rec("NOME"), 30, " ")
            s = nome & ": "
            If IsNull(rec("DATAORAINIZIOVALIDITA")) Then
                s = s & lPad("NON DEFINITA", 19, " ")
            ElseIf rec("DATAORAINIZIOVALIDITA") = dataInferioreMinima Then
                s = s & lPad("ILLIMITATA", 19, " ")
            Else
                s = s & lPad(rec("DATAORAINIZIOVALIDITA"), 19, " ")
            End If
            s = s & " - "
            If IsNull(rec("DATAORAFINEVALIDITA")) Then
                s = s & lPad("NON DEFINITA", 19, " ")
            ElseIf rec("DATAORAFINEVALIDITA") = dataSuperioreMassima Then
                s = s & lPad("ILLIMITATA", 19, " ")
            Else
                s = s & lPad(rec("DATAORAFINEVALIDITA"), 19, " ")
            End If
            s = s & " ; "
            
            elencoStringheDateOperazione(i + 1) = s
            elencoIdTipiOperazione(i + 1) = rec("ID")
            
            dataInizioOperazioneSelezionata_Principale(rec("ID")) = IIf(IsNull(rec("DATAORAINIZIOVALIDITA")), dataNulla, rec("DATAORAINIZIOVALIDITA"))
            dataFineOperazioneSelezionata_Principale(rec("ID")) = IIf(IsNull(rec("DATAORAFINEVALIDITA")), dataNulla, rec("DATAORAFINEVALIDITA"))
            dataInizioOperazioneSelezionata_Secondaria(rec("ID")) = dataNulla
            dataFineOperazioneSelezionata_Secondaria(rec("ID")) = dataNulla
            
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    ' aggiunge anche i dati della classesuperarea secondaria
    If idClasseSuperareaProdottoSecondaria <> idNessunElementoSelezionato Then
'        sql = "SELECT O.IDTIPOOPERAZIONE ID, O.NOME, DECODE (CSAP.IDCLASSESUPERAREA, NULL, NULL, PCPVTO.DATAORAINIZIOVALIDITA) DATAORAINIZIOVALIDITA, DECODE (CSAP.IDCLASSESUPERAREA, NULL, NULL, PCPVTO.DATAORAFINEVALIDITA) DATAORAFINEVALIDITA" & _
'            " FROM TIPOOPERAZIONE O, PRODOTTO_CLASSEPV_TIPOOPERAZ PCPVTO, CLASSESUPERAREAPRODOTTO CSAP" & _
'            " WHERE O.IDTIPOOPERAZIONE > 0" & _
'            " AND O.IDTIPOOPERAZIONE = PCPVTO.IDTIPOOPERAZIONE(+)" & _
'            " AND IDCLASSEPUNTOVENDITA(+) = " & idClassePuntoVenditaSelezionata & _
'            " AND PCPVTO.IDCLASSESUPERAREAPRODOTTO = CSAP.IDCLASSESUPERAREAPRODOTTO(+)" & _
'            " AND CSAP.IDCLASSESUPERAREA(+) = " & idClasseSuperareaProdottoSecondaria & _
'            " ORDER BY O.NOME"
        sql = "SELECT ID, NOME, MIN (DATAORAINIZIOVALIDITA) DATAORAINIZIOVALIDITA, MAX(DATAORAFINEVALIDITA) DATAORAFINEVALIDITA" & _
            " FROM" & _
            " (" & _
            " SELECT IDTIPOOPERAZIONE ID, NOME, NULL DATAORAINIZIOVALIDITA, NULL DATAORAFINEVALIDITA" & _
            " FROM TIPOOPERAZIONE" & _
            " UNION" & _
            " SELECT O.IDTIPOOPERAZIONE ID, O.NOME, PCPVTO.DATAORAINIZIOVALIDITA, PCPVTO.DATAORAFINEVALIDITA" & _
            " FROM TIPOOPERAZIONE O, PRODOTTO_CLASSEPV_TIPOOPERAZ PCPVTO" & _
            " WHERE O.IDTIPOOPERAZIONE > 0" & _
            " AND O.IDTIPOOPERAZIONE = PCPVTO.IDTIPOOPERAZIONE" & _
            " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
            " AND PCPVTO.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSecondaria & _
            " )" & _
            " WHERE ID > 0"
        
        If chkVisualizzaTuttiITipiOperazione.Value = vbUnchecked Then
            If idClassePuntoVenditaSelezionata = 3 Then ' altri PV organizzatore
                sql = sql & " AND ID IN (9,7,6,4,3)"
            End If
            If idClassePuntoVenditaSelezionata = 4 Then ' affiliati
                sql = sql & " AND ID IN (9,7,6,4,3)"
            End If
            If idClassePuntoVenditaSelezionata = 5 Then ' rete listicket
                sql = sql & " AND ID IN (9,7,6,4,3)"
            End If
            If idClassePuntoVenditaSelezionata = 6 Then ' internet
                sql = sql & " AND ID IN (2,5,6,9,11,12,23,24)"
            End If
            If idClassePuntoVenditaSelezionata = 7 Then ' call center lis
                sql = sql & " AND ID IN (2,3,4,5,6,7,9,11,12,20)"
            End If
            If idClassePuntoVenditaSelezionata = 8 Then ' altri CC
                sql = sql & " AND ID IN (2,5,9,11,12)"
            End If
        End If
        
        sql = sql & " GROUP BY ID, NOME" & _
            " ORDER BY NOME"
        
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 0
            While Not rec.EOF
                s = elencoStringheDateOperazione(i + 1) & " "
                If IsNull(rec("DATAORAINIZIOVALIDITA")) Then
                    s = s & lPad("NON DEFINITA", 19, " ")
                ElseIf rec("DATAORAINIZIOVALIDITA") = dataInferioreMinima Then
                    s = s & lPad("ILLIMITATA", 19, " ")
                Else
                    s = s & lPad(rec("DATAORAINIZIOVALIDITA"), 19, " ")
                End If
                s = s & " - "
                If IsNull(rec("DATAORAFINEVALIDITA")) Then
                    s = s & lPad("NON DEFINITA", 19, " ")
                ElseIf rec("DATAORAFINEVALIDITA") = dataSuperioreMassima Then
                    s = s & lPad("ILLIMITATA", 19, " ")
                Else
                    s = s & lPad(rec("DATAORAFINEVALIDITA"), 19, " ")
                End If
                s = s & " ; "
                elencoStringheDateOperazione(i + 1) = s
                
                dataInizioOperazioneSelezionata_Secondaria(rec("ID")) = IIf(IsNull(rec("DATAORAINIZIOVALIDITA")), dataNulla, rec("DATAORAINIZIOVALIDITA"))
                dataFineOperazioneSelezionata_Secondaria(rec("ID")) = IIf(IsNull(rec("DATAORAFINEVALIDITA")), dataNulla, rec("DATAORAFINEVALIDITA"))
                
                rec.MoveNext
                i = i + 1
            Wend
        End If
        rec.Close
        
    End If
    
    lstTipiOperazione.Clear
    j = 1
    For i = 1 To NUMERO_TIPI_OPERAZIONI
        If elencoStringheDateOperazione(j) <> "" Then
            lstTipiOperazione.AddItem elencoStringheDateOperazione(j)
            lstTipiOperazione.ItemData(j - 1) = elencoIdTipiOperazione(j)
            j = j + 1
        End If
    Next i
    
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub aggiornaLegenda(idTipoTerminale As Long)
        Select Case idTipoTerminale
            Case TT_TERMINALE_POS
                lblPOSsuReteIstituzionale.ForeColor = &HC0&
                lblPOSsuReteLIS.ForeColor = &HC0&
                lblWEBsuCallCenterLIS.ForeColor = &H80000012
                lblWEBsuInternet.ForeColor = &H80000012
                lblTOTEMsuAltriCallCenter.ForeColor = &H80000012
            Case TT_TERMINALE_WEB
                lblPOSsuReteIstituzionale.ForeColor = &H80000012
                lblPOSsuReteLIS.ForeColor = &H80000012
                lblWEBsuCallCenterLIS.ForeColor = &HC0&
                lblWEBsuInternet.ForeColor = &HC0&
                lblTOTEMsuAltriCallCenter.ForeColor = &H80000012
            Case TT_TERMINALE_TOTEM
                lblPOSsuReteIstituzionale.ForeColor = &H80000012
                lblPOSsuReteLIS.ForeColor = &H80000012
                lblWEBsuCallCenterLIS.ForeColor = &H80000012
                lblWEBsuInternet.ForeColor = &H80000012
                lblTOTEMsuAltriCallCenter.ForeColor = &HC0&
            Case Else
                lblPOSsuReteIstituzionale.ForeColor = &H80000012
                lblPOSsuReteLIS.ForeColor = &H80000012
                lblWEBsuCallCenterLIS.ForeColor = &H80000012
                lblWEBsuInternet.ForeColor = &H80000012
                lblTOTEMsuAltriCallCenter.ForeColor = &H80000012
        End Select
End Sub

Private Sub selezionaTuttiTipiOperazione()
    Dim i As Long
    
    For i = 0 To lstTipiOperazione.ListCount - 1
        lstTipiOperazione.Selected(i) = True
    Next i

End Sub

Private Function inizializzaListaIdTipiOperazione() As Collection
    Dim lista As Collection
    Dim i As Long
    
    Set lista = New Collection
    
    For i = 0 To lstTipiOperazione.ListCount - 1
        If lstTipiOperazione.Selected(i) Then
            lista.Add (lstTipiOperazione.ItemData(i))
        End If
    Next i
    
    Set inizializzaListaIdTipiOperazione = lista

End Function

