VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAssocTipoTerm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idTipoTerm As Long
Public labelTipoTerm As String

Public Function Clona() As clsAssocTipoTerm
    Dim b As clsAssocTipoTerm

    Set b = New clsAssocTipoTerm

    b.idTipoTerm = idTipoTerm
    b.labelTipoTerm = labelTipoTerm

    Set Clona = b
End Function

