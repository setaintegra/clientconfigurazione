VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneTariffe 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tariffe"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   11
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   13
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   12
      Top             =   4860
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   3600
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   6180
      Width           =   3315
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      MaxLength       =   30
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   6180
      Width           =   3315
   End
   Begin VB.ComboBox cmbTipoTariffa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7020
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   6180
      Width           =   3375
   End
   Begin VB.TextBox txtDescrizioneAlternativa 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      MaxLength       =   30
      TabIndex        =   8
      Top             =   6840
      Width           =   3315
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10500
      MaxLength       =   1
      TabIndex        =   7
      Top             =   6180
      Width           =   375
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneTariffe 
      Height          =   330
      Left            =   240
      Top             =   4080
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneTariffe 
      Height          =   3915
      Left            =   120
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Tariffe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   4815
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   20
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   3600
      TabIndex        =   19
      Top             =   5940
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   180
      TabIndex        =   18
      Top             =   5940
      Width           =   1695
   End
   Begin VB.Label lblDescrizioneAlternativa 
      Caption         =   "Descrizione alternativa"
      Height          =   255
      Left            =   180
      TabIndex        =   17
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Label lblTipoTariffa 
      Caption         =   "Tipo Tariffa - Cod."
      Height          =   255
      Left            =   7020
      TabIndex        =   16
      Top             =   5940
      Width           =   1695
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   10500
      TabIndex        =   15
      Top             =   5940
      Width           =   675
   End
End
Attribute VB_Name = "frmConfigurazioneTariffe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idTipoTariffa As Long
Private tipoTariffa As String
Private codice As String
Private codiceTipoTariffa As String
Private nomeRecordSelezionato As String
Private descrizioneRecordSelezionato As String
Private descrizioneAlternativaRecordSelezionato As String
'Private listaCampiValoriUnici As Collection

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
    
    dgrConfigurazioneTariffe.Caption = "TARIFFE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTariffe.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblTipoTariffa.Enabled = False
        lblCodice.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTariffe.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbTipoTariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipoTariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                               Len(Trim(txtCodice.Text)) = 1 And _
                               cmbTipoTariffa.ListIndex <> idNessunElementoSelezionato)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTariffe.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblTipoTariffa.Enabled = False
        lblCodice.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
    gestioneFormCorrente = gs
End Sub

Private Sub cmbTipoTariffa_Click()
    idTipoTariffa = cmbTipoTariffa.ItemData(cmbTipoTariffa.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDTARIFFA = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_TARIFFA, , stringaNota)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneTariffe_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneTariffe_Init
    Else
        If ValoriCampiOK Then
            If ValoriCampiOK Then
                If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_TARIFFA, , stringaNota)
                ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_TARIFFA, , stringaNota)
                End If
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneTariffe_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneTariffe_Init
            End If
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDTIPOTARIFFA AS ID, NOME || ' - ' || CODICE AS COD" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sql, "COD")
    Call AssegnaValoriCampi
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    sql = "SELECT IDTIPOTARIFFA AS ID, NOME || ' - ' || CODICE AS COD" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sql, "COD")
    Call AssegnaValoriCampi
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    sql = "SELECT IDTIPOTARIFFA AS ID, NOME || ' - ' || CODICE AS COD" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriCombo(cmbTipoTariffa, sql, "COD")
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizioneAlternativa_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDTIPOTARIFFA AS ID, NOME || ' - ' || CODICE AS COD" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sql, "COD")
    Call AssegnaValoriCampi
End Sub

Private Sub dgrConfigurazioneTariffe_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneTariffe_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneTariffe_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneTariffe.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec(0).Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneTariffe_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneTariffe
    
    sql = "SELECT T.IDTARIFFA AS ""ID"", T.NOME AS ""Nome""," & _
        " T.DESCRIZIONE AS ""Descrizione"", T.DESCRIZIONEALTERNATIVA AS ""Descr. altern.""," & _
        " T.IDTIPOTARIFFA AS IDTT, T.CODICE AS ""Codice""," & _
        " TT.NOME || ' - ' || TT.CODICE AS ""Tipo - Codice""" & _
        " FROM TARIFFA T, TIPOTARIFFA TT" & _
        " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA" & _
        " AND IDORGANIZZAZIONE IS NULL" & _
        " AND IDPRODOTTO IS NULL" & _
        " ORDER BY T.NOME"
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneTariffe.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovaTariffa As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
    sql = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA," & _
        " CODICE, IDTIPOTARIFFA, IDORGANIZZAZIONE, IDPRODOTTO)" & _
        " VALUES (" & _
        idNuovaTariffa & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneAlternativaRecordSelezionato) & ", " & _
        SqlStringValue(codice) & ", " & _
        idTipoTariffa & ", " & _
        "NULL, " & _
        "NULL)"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaTariffa)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT T.IDTARIFFA ID, T.NOME NOME, T.CODICE COD," & _
          " T.DESCRIZIONE DESCR, T.DESCRIZIONEALTERNATIVA DESCRALT," & _
          " T.IDTIPOTARIFFA IDTT, TT.NOME TIPOTAR, TT.CODICE TIPOCOD" & _
          " FROM TARIFFA T, TIPOTARIFFA TT" & _
          " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA" & _
          " AND T.IDTARIFFA = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCR")), "", rec("DESCR"))
        descrizioneAlternativaRecordSelezionato = IIf(IsNull(rec("DESCRALT")), "", rec("DESCRALT"))
        idTipoTariffa = rec("IDTT")
        codice = rec("COD")
        codiceTipoTariffa = rec("TIPOCOD")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDescrizioneAlternativa.Text = ""
    txtCodice.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizioneAlternativa.Text = descrizioneAlternativaRecordSelezionato
    txtCodice.Text = codice
    Call SelezionaElementoSuCombo(cmbTipoTariffa, idTipoTariffa)

    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "UPDATE TARIFFA SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " DESCRIZIONEALTERNATIVA = " & SqlStringValue(descrizioneAlternativaRecordSelezionato) & "," & _
        " CODICE = " & SqlStringValue(codice) & "," & _
        " IDTIPOTARIFFA = " & idTipoTariffa & _
        " WHERE IDTARIFFA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazioneTariffe_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneTariffe
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

On Error Resume Next

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTARIFFA <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("TARIFFA", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDORGANIZZAZIONE IS NULL AND IDPRODOTTO IS NULL ", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrizioneAlternativaRecordSelezionato = Trim(txtDescrizioneAlternativa.Text)
    codice = Trim(txtCodice.Text)
    If IsNumeric(codice) Then
        ValoriCampiOK = False
'        Call frmMessaggio.Visualizza("ErroreCodicetariffa")
        Call listaNonConformitā.Add("- il valore immesso sul campo codice non deve essere numerico;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function
'
'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub txtCodice_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub





