VERSION 5.00
Begin VB.Form frmInizialeOperatoreAbilitabileAIVAPreassoltaObbligatoria 
   Caption         =   "Operatori abilitabili a IVA preassolta obbligatoria"
   ClientHeight    =   3885
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13170
   LinkTopic       =   "Form1"
   ScaleHeight     =   3885
   ScaleWidth      =   13170
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   8
      Top             =   2640
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   1875
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   12855
      Begin VB.TextBox txtUserName 
         BackColor       =   &H00C0FFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         MaxLength       =   30
         TabIndex        =   4
         Top             =   480
         Width           =   3315
      End
      Begin VB.TextBox txtOperatore 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2520
         MaxLength       =   30
         TabIndex        =   3
         Top             =   1080
         Width           =   3315
      End
      Begin VB.ComboBox cmbOperatori 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8520
         TabIndex        =   2
         Top             =   1080
         Width           =   3615
      End
      Begin VB.CommandButton cmdCercaOperatore 
         Caption         =   "Cerca operatore"
         Height          =   375
         Left            =   6000
         TabIndex        =   1
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label lblUserName 
         Alignment       =   1  'Right Justify
         Caption         =   "Username"
         Height          =   255
         Left            =   480
         TabIndex        =   7
         Top             =   540
         Width           =   1875
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Operatore"
         Height          =   255
         Left            =   480
         TabIndex        =   6
         Top             =   1140
         Width           =   1935
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "seleziona"
         Height          =   255
         Left            =   7560
         TabIndex        =   5
         Top             =   1080
         Width           =   855
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Operatore abilitabile a IVA preassolta obbligatoria"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeOperatoreAbilitabileAIVAPreassoltaObbligatoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOperatoreSelezionato As Long
Private userName As String

Private isRecordEditabile As Boolean

Private internalEvent As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalitāFormCorrente As AzioneEnum
    
Public Sub Init()
    
    Call Variabili_Init
        
    Select Case modalitāFormCorrente
        Case A_NUOVO
            idOperatoreSelezionato = idNessunElementoSelezionato
            isRecordEditabile = True
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
    End Select
    
    Call AggiornaAbilitazioneControlli
    
    Call frmInizialeOperatoreAbilitabileAIVAPreassoltaObbligatoria.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    userName = ""
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetIdOperatoreSelezionato(id As Long)
    idOperatoreSelezionato = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalitāFormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Operatore abilitabile"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Operatore configurato"
    End Select
    
    cmdConferma.Enabled = Trim(txtUserName.Text) <> ""
    fraDati.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    txtUserName.Enabled = False
    lblUserName.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    
End Sub

Private Sub CaricaValoriComboOperatori()
    Dim sql As String
    
    sql = "SELECT OPT.IDOPERATORE ID, OPT.USERNAME NOME" & _
        " FROM OPERATORE OPT" & _
        " LEFT OUTER JOIN OPABILITABILEIVAPREASSOLTAOBBL OPTAB on OPT.IDOPERATORE = OPTAB.IDOPERATORE" & _
        " WHERE OPT.USERNAME LIKE '" & txtOperatore & "%'" & _
        " AND OPT.ABILITATO = 1" & _
        " AND OPTAB.IDOPERATORE IS NULL" & _
        " ORDER BY USERNAME"
    Call CaricaValoriCombo3(cmbOperatori, sql, "NOME", 100)
End Sub

Private Sub cmbOperatori_Click()
    If Not internalEvent Then
        idOperatoreSelezionato = cmbOperatori.ItemData(cmbOperatori.ListIndex)
    End If
    txtUserName.Text = cmbOperatori
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
        
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaOperatoreAbilitabileAIVAPreassoltaObbligatoria.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
        sql = "SELECT USERNAME" & _
            " FROM OPERATORE O" & _
            " WHERE IDOPERATORE = " & idOperatoreSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        userName = rec("USERNAME")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtUserName.Text = ""
    txtUserName.Text = userName
    
    sql = "SELECT IDOPERATORE ID, USERNAME NOME" & _
        " FROM OPERATORE" & _
        " WHERE ABILITATO = 1" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo3(cmbOperatori, sql, "NOME", 100)
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdCercaOperatore_Click()
    CaricaValoriComboOperatori
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDOPERATORE = " & idOperatoreSelezionato
    
    If isRecordEditabile Then
        Select Case modalitāFormCorrente
            Case A_NUOVO
                If valoriCampiOK = True Then
                    Call InserisciNellaBaseDati
                    stringaNota = "IDOPERATORE = " & idOperatoreSelezionato
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
                    Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                    Call Esci
                End If
            Case A_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
                Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
                Call Esci
        End Select
    End If
    Call frmSceltaOperatoreAbilitabileAIVAPreassoltaObbligatoria.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim condizioniSQL As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "UPDATE OPERATORE SET IVAPREASSOLTAOBBLIGATORIA = 0 WHERE IDOPERATORE = " & idOperatoreSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM OPABILITABILEIVAPREASSOLTAOBBL WHERE IDOPERATORE = " & idOperatoreSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
On Error Resume Next

    valoriCampiOK = True
    Set listaNonConformitā = New Collection
    condizioneSql = ""
    If modalitāFormCorrente = A_MODIFICA Or modalitāFormCorrente = A_ELIMINA Then
        condizioneSql = "IDOPERATORE <> " & idOperatoreSelezionato
    End If
    If ViolataUnicitā("OPABILITABILEIVAPREASSOLTAOBBL", "IDOPERATORE = " & idOperatoreSelezionato, "", condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore č giā presente in DB;")
    End If
        
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    
    sql = "INSERT INTO OPABILITABILEIVAPREASSOLTAOBBL (IDOPERATORE)"
    sql = sql & " VALUES (" & idOperatoreSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdOperatoreSelezionato(idOperatoreSelezionato)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub
