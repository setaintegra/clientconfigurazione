VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmModificaDataRappresentazione 
   Caption         =   "Modifica della data di rappresentazione"
   ClientHeight    =   6360
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7590
   LinkTopic       =   "Form1"
   ScaleHeight     =   6360
   ScaleWidth      =   7590
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtMinuti 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2175
      MaxLength       =   2
      TabIndex        =   15
      Top             =   4740
      Width           =   435
   End
   Begin VB.TextBox txtOra 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1575
      MaxLength       =   2
      TabIndex        =   14
      Top             =   4740
      Width           =   435
   End
   Begin VB.TextBox txtOreDurata 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2715
      MaxLength       =   3
      TabIndex        =   13
      Top             =   4740
      Width           =   555
   End
   Begin VB.TextBox txtMinutiDurata 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3435
      MaxLength       =   2
      TabIndex        =   12
      Top             =   4740
      Width           =   435
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   2595
      Width           =   3795
   End
   Begin VB.ComboBox cmbProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmModificaDataRappresentazione.frx":0000
      Left            =   75
      List            =   "frmModificaDataRappresentazione.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   3270
      Width           =   3780
   End
   Begin VB.ComboBox cmbRappresentazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmModificaDataRappresentazione.frx":0004
      Left            =   75
      List            =   "frmModificaDataRappresentazione.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   3945
      Width           =   3780
   End
   Begin VB.Frame fraPulsanti 
      Height          =   915
      Left            =   75
      TabIndex        =   1
      Top             =   5205
      Width           =   1740
      Begin VB.CommandButton cmdModificaDataRappresentazione 
         Caption         =   "Modifica"
         Height          =   390
         Left            =   225
         TabIndex        =   2
         Top             =   300
         Width           =   1290
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   465
      Left            =   5775
      TabIndex        =   0
      Top             =   5475
      Width           =   1140
   End
   Begin MSComCtl2.DTPicker dtpData 
      Height          =   315
      Left            =   75
      TabIndex        =   16
      Top             =   4740
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      DateIsNull      =   -1  'True
      Format          =   66650113
      CurrentDate     =   37606
   End
   Begin VB.Label lblData 
      Caption         =   "Data"
      Height          =   195
      Left            =   75
      TabIndex        =   21
      Top             =   4500
      Width           =   1395
   End
   Begin VB.Label lblOra 
      Caption         =   "Ora (HH:MM)"
      Height          =   195
      Left            =   1575
      TabIndex        =   20
      Top             =   4500
      Width           =   1035
   End
   Begin VB.Label lblSeparatoreOreMinuti 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2055
      TabIndex        =   19
      Top             =   4740
      Width           =   75
   End
   Begin VB.Label lblSeparatoreOreMinutiDurata 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3315
      TabIndex        =   18
      Top             =   4740
      Width           =   75
   End
   Begin VB.Label lblDurata 
      Caption         =   "Durata (HH:MM)"
      Height          =   195
      Left            =   2715
      TabIndex        =   17
      Top             =   4500
      Width           =   1215
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Modifica della data di rappresentazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   75
      TabIndex        =   11
      Top             =   150
      Width           =   5775
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazioni"
      Height          =   255
      Left            =   75
      TabIndex        =   10
      Top             =   2295
      Width           =   1470
   End
   Begin VB.Label lblProdotto 
      Caption         =   "Abbonamenti attivi"
      Height          =   195
      Left            =   75
      TabIndex        =   9
      Top             =   3045
      Width           =   1500
   End
   Begin VB.Label lblRappresentazione 
      Caption         =   "Rappresentazioni"
      Height          =   195
      Left            =   75
      TabIndex        =   8
      Top             =   3705
      Width           =   1500
   End
   Begin VB.Label lblMessaggio 
      Caption         =   "Questa maschera � utilizzata per modificare la data di una rappresentazione"
      Height          =   390
      Left            =   75
      TabIndex        =   7
      Top             =   675
      Width           =   7365
   End
   Begin VB.Label Label1 
      Caption         =   "� respoonsabilit� dell'operatore accertarsi che l'azione non pregiudichi la consistenza dei dati (ad esempio posti doppi...)"
      Height          =   765
      Left            =   75
      TabIndex        =   6
      Top             =   1275
      Width           =   7365
   End
End
Attribute VB_Name = "frmModificaDataRappresentazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idRappresentazioneSelezionata As Long
Private dataOraInizio As Date
Private durataInMinuti As Long

Private Sub cmbOrganizzazione_Click()
    Call cmbProdotto_Update
End Sub

Private Sub cmbProdotto_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
    sql = "SELECT   P.idprodotto ID, P.nome NOME"
    sql = sql & " FROM prodotto P, tipostatoprodotto TSP"
    sql = sql & " WHERE TSP.idtipostatoprodotto = P.idtipostatoprodotto"
    sql = sql & " AND P.idorganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " AND TSP.idtipostatoprodotto = " & TSP_ATTIVO
    sql = sql & " AND P.idclasseprodotto = " & CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO
    sql = sql & " ORDER BY P.nome ASC"

    Call CaricaValoriCombo(cmbProdotto, sql, "NOME")
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbProdotto_Click()
    Call cmbRappresentazione_Update
End Sub

Private Sub cmbRappresentazione_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
    
    sql = "SELECT R.IDRAPPRESENTAZIONE ID, S.NOME || ' ' || R.DATAORAINIZIO NOME"
    sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, SPETTACOLO S"
    sql = sql & " Where PR.idProdotto = " & idProdottoSelezionato
    sql = sql & " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " AND R.IDSPETTACOLO = S.IDSPETTACOLO"
    sql = sql & " ORDER BY R.DATAORAINIZIO ASC"

    Call CaricaValoriCombo(cmbRappresentazione, sql, "NOME")
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmbRappresentazione_Click()
    Dim s As String
    
    idRappresentazioneSelezionata = cmbRappresentazione.ItemData(cmbRappresentazione.ListIndex)
    
    dtpData.Enabled = True
    txtOra.Enabled = True
    txtMinuti.Enabled = True
    txtOreDurata.Enabled = True
    txtMinutiDurata.Enabled = True
    
    Call CaricaDallaBaseDati
    idRecordSelezionato = 1
    
    dtpData.Value = dataOraInizio
    
    txtOra.Text = StringaOraMinuti(CStr(Hour(dataOraInizio)))
    
    txtMinuti.Text = StringaOraMinuti(CStr(Minute(dataOraInizio)))
    
    s = StringaOraMinuti(Int(durataInMinuti / 60))
    If Len(s) = 3 Then
        txtOreDurata.Text = s
    Else
        txtOreDurata.Text = "0" & s
    End If
    
    txtMinutiDurata.Text = StringaOraMinuti(durataInMinuti Mod 60)
    
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT DATAORAINIZIO, DURATAINMINUTI"
    sql = sql & " FROM RAPPRESENTAZIONE"
    sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRappresentazioneSelezionata
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        dataOraInizio = rec("DATAORAINIZIO")
        durataInMinuti = rec("DURATAINMINUTI")
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Public Sub Init()
    Dim sql As String

    sql = "SELECT   O.idorganizzazione ID, O.nome NOME"
    'sql = sql & " tipoStatoOrganizzazione.idTipoStatoOrganizzazione"
    sql = sql & " From organizzazione O, tipoStatoOrganizzazione TSO"
    sql = sql & " WHERE (    (TSO.idtipostatoorganizzazione ="
    sql = sql & " O.idTipoStatoOrganizzazione"
    sql = sql & " )"
    sql = sql & " AND (TSO.idtipostatoorganizzazione = " & TSO_ATTIVA & ")"
    sql = sql & " )"
    sql = sql & " ORDER BY O.nome ASC"

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idProdottoSelezionato = idNessunElementoSelezionato
    idRappresentazioneSelezionata = idNessunElementoSelezionato
    
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    
    txtOra.Text = ""
    txtMinuti.Text = ""
    txtOreDurata.Text = ""
    txtMinutiDurata.Text = ""

    dtpData.Enabled = False
    txtOra.Enabled = False
    txtMinuti.Enabled = False
    txtOreDurata.Enabled = False
    txtMinutiDurata.Enabled = False

    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdModificaDataRappresentazione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub cmdModificaDataRappresentazione_Click()
    Dim sql As String
    Dim n As Long
    Dim msg As String
    
    On Error GoTo gestioneErrori
    
    durataInMinuti = (CStr(Trim(txtOreDurata.Text)) * 60) + CStr(Trim(txtMinutiDurata.Text))
    dataOraInizio = FormatDateTime(FormatDateTime(dtpData.Value, vbShortDate) & " " & CStr(Trim(txtOra.Text)) & ":" & CStr(Trim(txtMinuti.Text)) & ":" & "00", vbGeneralDate)
    
    sql = " UPDATE RAPPRESENTAZIONE"
    sql = sql & " SET DATAORAINIZIO = " & SqlDateTimeValue(dataOraInizio) & ", DURATAINMINUTI = " & durataInMinuti
    sql = sql & " WHERE IDRAPPRESENTAZIONE =  " & idRappresentazioneSelezionata

    Call ApriConnessioneBD
    SETAConnection.BeginTrans
    
    SETAConnection.Execute sql, n, adCmdText
    
    If (n = 1) Then
        Call frmMessaggio.Visualizza("ConfermaModificaDataRappresentazione")
        
        If frmMessaggio.exitCode = EC_CONFERMA Then
            SETAConnection.CommitTrans
            MsgBox "Avvenuta modifica", vbInformation, "Rappresentazione"
        Else
            SETAConnection.RollbackTrans
            MsgBox "Operazione annullata", vbInformation, "Rappresentazione"
        End If
    Else
        SETAConnection.RollbackTrans
        msg = "Errore nella modifica: sono stati modificati " & n & " record!" & vbCrLf
        msg = msg & "Operazione annullata"
        MsgBox msg, vbCritical, "Attenzione"
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub txtMinuti_Change()
    If Len(txtMinuti.Text) > 0 Then
        idRecordSelezionato = 1
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli

End Sub

Private Sub txtMinuti_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
        Case 8
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txtMinutiDurata_Change()
    If Len(txtMinutiDurata.Text) > 0 Then
        idRecordSelezionato = 1
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub txtMinutiDurata_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
        Case 8
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txtOra_Change()
    If Len(txtOra.Text) > 0 Then
        idRecordSelezionato = 1
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub txtOra_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
        Case 8
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txtOreDurata_Change()
    If Len(txtOreDurata.Text) > 0 Then
        idRecordSelezionato = 1
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli

End Sub

Private Sub txtOreDurata_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
        Case 8
        Case Else
            KeyAscii = 0
    End Select
End Sub
