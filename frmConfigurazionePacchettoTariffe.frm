VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmConfigurazionePacchettoTariffe 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtDataOraInizio 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7320
      MaxLength       =   30
      TabIndex        =   27
      Top             =   3060
      Width           =   2175
   End
   Begin VB.TextBox txtDataOraFine 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9660
      MaxLength       =   30
      TabIndex        =   26
      Top             =   3060
      Width           =   2175
   End
   Begin MSAdodcLib.Adodc adcDefinizionePrezzi 
      Height          =   330
      Left            =   10320
      Top             =   4320
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   5520
      Width           =   3795
   End
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca"
      Height          =   435
      Left            =   11040
      TabIndex        =   5
      Top             =   3840
      Width           =   795
   End
   Begin VB.ListBox lstProdottiTariffe 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2700
      Left            =   4080
      Style           =   1  'Checkbox
      TabIndex        =   8
      Top             =   4860
      Width           =   7755
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   17
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   16
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   15
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   14
      Top             =   2640
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   6
      Top             =   4860
      Width           =   3315
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePacchettoTariffe 
      Height          =   330
      Left            =   6360
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePacchettoTariffe 
      Height          =   1695
      Left            =   120
      TabIndex        =   18
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   2990
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSDataGridLib.DataGrid dgrDefinizionePrezzi 
      Height          =   915
      Left            =   120
      TabIndex        =   4
      Top             =   3660
      Width           =   10875
      _ExtentX        =   19182
      _ExtentY        =   1614
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      WrapCellPointer =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   "000;(000)"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   "000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   2
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblPeriodoCommerciale 
      Alignment       =   1  'Right Justify
      Caption         =   "Periodo commerciale:"
      Height          =   255
      Left            =   5580
      TabIndex        =   30
      Top             =   3120
      Width           =   1635
   End
   Begin VB.Label lblDataOraInizio 
      Caption         =   "Data/ora inizio"
      Height          =   255
      Left            =   7320
      TabIndex        =   29
      Top             =   2820
      Width           =   2175
   End
   Begin VB.Label lblDataOraFine 
      Caption         =   "Data/ora fine"
      Height          =   255
      Left            =   9660
      TabIndex        =   28
      Top             =   2820
      Width           =   2175
   End
   Begin VB.Label lblProdottiTariffe 
      Caption         =   "Prodotti / tariffe"
      Height          =   255
      Left            =   4080
      TabIndex        =   25
      Top             =   4620
      Width           =   4215
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Tariffe pacchetto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   24
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   23
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   22
      Top             =   2400
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   2400
      Width           =   1815
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   5280
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   4620
      Width           =   1695
   End
End
Attribute VB_Name = "frmConfigurazionePacchettoTariffe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOffertaPacchettoSelezionata As Long
'Private idOrganizzazioneSelezionata As Long
'Private idTipoTariffa As Long
Private isRecordEditabile As Boolean
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeOffertaPacchettoSelezionata As String
'Private nomeOrganizzazioneSelezionata As String
Private listaProdottiTariffe As Collection
Private nomeTabellaTemporanea As String
Private nomeTabellaPrezziTemporanea As String
'Private numeroProdottiAssociati As Long
Private listaAreePacchetto As Collection
Private listaPrezzi As Collection
Private ricercaTariffeEseguita As Boolean

Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pacchetto"
    txtInfo1.Text = nomeOffertaPacchettoSelezionata
    txtInfo1.Enabled = False
    
    txtDataOraInizio.Enabled = False
    txtDataOraFine.Enabled = False
    
    dgrConfigurazionePacchettoTariffe.Caption = "TARIFFE PACCHETTO CONFIGURATE"
    dgrDefinizionePrezzi.Caption = "DEFINIZIONE PREZZI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoTariffe.Enabled = True
        If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Then
            dgrDefinizionePrezzi.Enabled = False
            dgrDefinizionePrezzi.AllowUpdate = False
            cmdCerca.Visible = True
            cmdCerca.Enabled = False
        Else
            dgrDefinizionePrezzi.Visible = False
            cmdCerca.Visible = False
        End If
'        dgrDefinizionePrezzi.Columns(1).Locked = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lstProdottiTariffe.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblProdottiTariffe.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoTariffe.Enabled = False
        If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Then
'            dgrDefinizionePrezzi.Enabled = True
'            dgrDefinizionePrezzi.AllowUpdate = True
'            cmdCerca.Visible = True
'            cmdCerca.Enabled = True
            dgrDefinizionePrezzi.Enabled = Not ricercaTariffeEseguita
            dgrDefinizionePrezzi.AllowUpdate = Not ricercaTariffeEseguita
            cmdCerca.Visible = True
            cmdCerca.Enabled = Not ricercaTariffeEseguita
        Else
            dgrDefinizionePrezzi.Visible = False
            cmdCerca.Enabled = False
        End If
'        dgrConfigurazioneProdottoPrezzi.Columns(1).Locked = True
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And ricercaTariffeEseguita)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And ricercaTariffeEseguita)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And ricercaTariffeEseguita)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And ricercaTariffeEseguita)
        lstProdottiTariffe.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And ricercaTariffeEseguita)
'        Call lstProdottiTariffe.Clear
        lblProdottiTariffe.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And ricercaTariffeEseguita)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoTariffe.Enabled = True
        If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Then
            dgrDefinizionePrezzi.Enabled = False
            dgrDefinizionePrezzi.AllowUpdate = False
            cmdCerca.Visible = True
            cmdCerca.Enabled = False
        Else
            dgrDefinizionePrezzi.Visible = False
            cmdCerca.Visible = False
        End If
'        dgrDefinizionePrezzi.Columns(1).Locked = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call lstProdottiTariffe.Clear
        lstProdottiTariffe.Enabled = True
        lblProdottiTariffe.Enabled = True
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioPacchettoTariffa
    Call EliminaTabellaAppoggioPrezzi
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchettoSelezionata)
        Call EliminaTabellaAppoggioPacchettoTariffa
        Call EliminaTabellaAppoggioPrezzi
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub
'
'Private Sub CaricaFormConfigurazioneAreePacchetto()
'    Call frmConfigurazionePacchettoAree.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoAree.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoAree.SetNumeroProdottiAssociati(numeroProdottiAssociati)
'    Call frmConfigurazionePacchettoAree.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazionePacchettoAree.SetModalitāForm(modalitāFormCorrente)
'    Call frmConfigurazionePacchettoAree.Init
'End Sub

Public Sub SetIdOffertaPacchettoSelezionata(id As Long)
    idOffertaPacchettoSelezionata = id
End Sub
'
'Public Sub SetNumeroProdottiAssociati(numero As Long)
'    numeroProdottiAssociati = numero
'End Sub

Public Sub SetNomeOffertaPacchettoSelezionata(nome As String)
    nomeOffertaPacchettoSelezionata = nome
End Sub

Public Sub SetTipoOffertaPacchettoSelezionata(tipo As TipoOffertaPacchettoEnum)
    tipoOffertaPacchettoSelezionata = tipo
End Sub

Private Sub Successivo()
'    Call AzionePercorsoGuidato(TNCP_FINE)
    Call CaricaFormConfigurazionePacchettoOperatori
    Call EliminaTabellaAppoggioPacchettoTariffa
    Call EliminaTabellaAppoggioPrezzi
End Sub

Private Sub CaricaFormConfigurazionePacchettoOperatori()
    Call frmConfigurazionePacchettoOperatori.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOperatori.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOperatori.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOperatori.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoOperatori.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazionePacchettoOperatori.Init
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    ricercaTariffeEseguita = False
    'pulire i prezzi presenti sul data grid
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCerca_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Cerca
    
    MousePointer = mousePointerOld
End Sub

Private Sub Cerca()
    Call RilevaValoriPrezzi
    Call CaricaValoriLstProdottiTariffe
    ricercaTariffeEseguita = True
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
'    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
'        isConfigurabile = True
'        If tipoStatoProdotto = TSP_ATTIVO Then
'            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
'            If frmMessaggio.exitCode <> EC_CONFERMA Then
'                isConfigurabile = False
'            End If
'        End If
'        If isConfigurabile Then
    
    If isOffertaPacchettoBloccataDaUtente Then
        Call SetGestioneExitCode(EC_CONFERMA)
        Call AggiornaAbilitazioneControlli
    
        If ValoriCampiOK Then
            If isRecordEditabile Then
                Select Case gestioneRecordGriglia
                    Case ASG_INSERISCI_NUOVO
                        Call InserisciNellaBaseDati
                        Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_TARIFFA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoTariffe_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazionePacchettoTariffe_Init
                    Case ASG_INSERISCI_DA_SELEZIONE
                        Call InserisciNellaBaseDati
                        Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_TARIFFA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoTariffe_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazionePacchettoTariffe_Init
                    Case ASG_MODIFICA
                        Call AggiornaNellaBaseDati
                        Call ScriviLog(CCTA_MODIFICA, CCDA_OFFERTA_PACCHETTO, CCDA_TARIFFA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoTariffe_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazionePacchettoTariffe_Init
                    Case ASG_ELIMINA
                        Call EliminaDallaBaseDati
                        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OFFERTA_PACCHETTO, CCDA_TARIFFA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoTariffe_Init
                        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                        Call dgrConfigurazionePacchettoTariffe_Init
                End Select
            End If
            ricercaTariffeEseguita = False
            'pulire i prezzi presenti sul data grid
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Select Case tipoOffertaPacchettoSelezionata
        Case TOP_PREZZO_RATEIZZABILE
            Call CreaTabellaAppoggioPrezzi
            Call adcDefinizionePrezzi_Init
            Call dgrDefinizionePrezzi_Init
            Call Cerca
        Case TOP_PREZZO_FISSO
            ricercaTariffeEseguita = True
            Call CaricaValoriLstProdottiTariffe
        Case TOP_PREZZO_VARIABILE
            ricercaTariffeEseguita = True
            Call CaricaValoriLstProdottiTariffe
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioPacchettoTariffa
            Call EliminaTabellaAppoggioPrezzi
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Select Case tipoOffertaPacchettoSelezionata
        Case TOP_PREZZO_RATEIZZABILE
            Call CreaTabellaAppoggioPrezzi
            Call adcDefinizionePrezzi_Init
            Call dgrDefinizionePrezzi_Init
            Call Cerca
        Case TOP_PREZZO_FISSO
            ricercaTariffeEseguita = True
            Call CaricaValoriLstProdottiTariffe
        Case TOP_PREZZO_VARIABILE
            ricercaTariffeEseguita = True
            Call CaricaValoriLstProdottiTariffe
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    isRecordEditabile = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriLstProdottiTariffe
    Select Case tipoOffertaPacchettoSelezionata
        Case TOP_PREZZO_RATEIZZABILE
            Call CreaTabellaAppoggioPrezzi
            Call adcDefinizionePrezzi_Init
            Call dgrDefinizionePrezzi_Init
        Case TOP_PREZZO_FISSO
            ricercaTariffeEseguita = True
        Case TOP_PREZZO_VARIABILE
            ricercaTariffeEseguita = True
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePacchettoTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
'    Call CaricaValoriLstProdottiTariffe
    Select Case tipoOffertaPacchettoSelezionata
        Case TOP_PREZZO_RATEIZZABILE
            Call CreaTabellaAppoggioPrezzi
            Call adcDefinizionePrezzi_Init
            Call dgrDefinizionePrezzi_Init
        Case TOP_PREZZO_FISSO
            ricercaTariffeEseguita = True
            Call CaricaValoriLstProdottiTariffe
        Case TOP_PREZZO_VARIABILE
            ricercaTariffeEseguita = True
            Call CaricaValoriLstProdottiTariffe
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazionePacchettoTariffe_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    ricercaTariffeEseguita = False
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call ValorizzaCampiDate
    Call CreaTabellaAppoggioPacchettoTariffa
    Call adcConfigurazionePacchettoTariffe_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePacchettoTariffe_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePacchettoTariffe.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePacchettoTariffe_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioPacchettoTariffa

    Set d = adcConfigurazionePacchettoTariffe
        
    sql = " SELECT T.IDTARIFFAPACCHETTO ID, T.NOME ""Nome"", "
    sql = sql & " T.DESCRIZIONE ""Descrizione"", TMP.NOME ""Tariffe"","
    sql = sql & " DECODE(NUMEROASSOCIAZIONI, " & NumeroProdottiAssociatiAdOffertaPacchetto(idOffertaPacchettoSelezionata) & ", 'SI', 'NO') ""Associazione completa"""
    sql = sql & " FROM TARIFFAPACCHETTO T, " & nomeTabellaTemporanea & " TMP"
    sql = sql & " WHERE T.IDTARIFFAPACCHETTO = TMP.IDTARIFFAPACCHETTO(+)"
    sql = sql & " AND T.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazionePacchettoTariffe.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idNuovaTariffaPacchetto As Long
    Dim n As Long
    Dim pt As clsElementoLista

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA TARIFFAPACCHETTO
    idNuovaTariffaPacchetto = OttieniIdentificatoreDaSequenza("SQ_TARIFFAPACCHETTO")
    sql = " INSERT INTO TARIFFAPACCHETTO ("
    sql = sql & " IDTARIFFAPACCHETTO, NOME, DESCRIZIONE,"
    sql = sql & " IDOFFERTAPACCHETTO)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaTariffaPacchetto & " , "
    sql = sql & SqlStringValue(nomeRecordSelezionato) & " , "
    sql = sql & SqlStringValue(descrizioneRecordSelezionato) & " , "
    sql = sql & idOffertaPacchettoSelezionata & " )"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO", "IDTARIFFAPACCHETTO", idNuovaTariffaPacchetto, TSR_NUOVO)
'    End If

'    INSERIMENTO IN TABELLA TARIFFAPACCHETTO_TARIFFA
    If Not (listaProdottiTariffe Is Nothing) Then
        For Each pt In listaProdottiTariffe
            If pt.idAttributoElementoLista <> idNessunElementoSelezionato Then
                sql = " INSERT INTO TARIFFAPACCHETTO_TARIFFA ("
                sql = sql & " IDTARIFFAPACCHETTO, IDTARIFFA, IDPRODOTTO)"
                sql = sql & " VALUES ( "
                sql = sql & idNuovaTariffaPacchetto & " , "
                sql = sql & pt.idAttributoElementoLista & " , "
                sql = sql & pt.idElementoLista & " )"
                SETAConnection.Execute sql, n, adCmdText
            End If
        Next pt
    End If
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO_TARIFFA", "IDTARIFFAPACCHETTO", idNuovaTariffaPacchetto, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD

    Call SetIdRecordSelezionato(idNuovaTariffaPacchetto)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long

    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        sql = "SELECT T.IDTARIFFA ID, T.NOME NOME, T.CODICE COD, T.IDTIPOTARIFFA IDTT," & _
''            " T.DESCRIZIONE DESCR, T.DESCRIZIONEALTERNATIVA DESCRALT," & _
''            " TT.NOME TIPOTAR, TT.CODICE TIPOCOD," & _
''            " IDMODALITAEMISSIONE, IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
''            " FROM TARIFFA T, TIPOTARIFFA TT" & _
''            " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA" & _
''            " AND T.IDTARIFFA = " & idRecordSelezionato
'        sql = " SELECT IDTARIFFAPACCHETTO, NOME, DESCRIZIONE,"
'        sql = sql & " IDOFFERTAPACCHETTO, IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE"
'        sql = sql & " FROM TARIFFAPACCHETTO"
'        sql = sql & " WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
'    Else
'        sql = "SELECT T.IDTARIFFA ID, T.NOME NOME, T.CODICE COD, T.IDTIPOTARIFFA IDTT," & _
'            " T.DESCRIZIONE DESCR, T.DESCRIZIONEALTERNATIVA DESCRALT," & _
'            " IDMODALITAEMISSIONE," & _
'            " TT.NOME TIPOTAR, TT.CODICE TIPOCOD" & _
'            " FROM TARIFFA T, TIPOTARIFFA TT" & _
'            " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA" & _
'            " AND T.IDTARIFFA = " & idRecordSelezionato
        sql = " SELECT IDTARIFFAPACCHETTO, NOME, DESCRIZIONE,"
        sql = sql & " IDOFFERTAPACCHETTO"
        sql = sql & " FROM TARIFFAPACCHETTO"
        sql = sql & " WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close

    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
'    Call SelezionaElementoSuCombo(cmbTipoTariffa, idTipoTariffa)
'    Call SelezionaElementoSuCombo(cmbModalitāEmissione, idModalitāEmissione)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim pt As clsElementoLista
    Dim condizioniSQL As String

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   AGGIORNAMENTO TABELLA TARIFFAPACCHETTO
    sql = " UPDATE TARIFFAPACCHETTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ", "
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato)
    sql = sql & " WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO", "IDTARIFFAPACCHETTO", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If

'   AGGIORNAMENTO TABELLA PACCHETTOTARIFFA_TARIFFA
    If Not (listaProdottiTariffe Is Nothing) Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO_TARIFFA", "IDTARIFFAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'        Else
            sql = "DELETE FROM TARIFFAPACCHETTO_TARIFFA WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
        For Each pt In listaProdottiTariffe
            If pt.idAttributoElementoLista <> idNessunElementoSelezionato Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO TARIFFAPACCHETTO_TARIFFA"
'                    sql = sql & " (IDTARIFFAPACCHETTO, IDTARIFFA, IDPRODOTTO)"
'                    sql = sql & " (SELECT "
'                    sql = sql & idRecordSelezionato & " IDTARIFFAPACCHETTO, "
'                    sql = sql & pt.idAttributoElementoLista & " IDTARIFFA, "
'                    sql = sql & pt.idElementoLista & " IDPRODOTTO"
'                    sql = sql & " FROM DUAL"
'                    sql = sql & " MINUS"
'                    sql = sql & " SELECT IDTARIFFAPACCHETTO, IDTARIFFA, IDPRODOTTO"
'                    sql = sql & " FROM TARIFFAPACCHETTO_TARIFFA"
'                    sql = sql & " WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
'                    sql = sql & " AND IDPRODOTTO = " & pt.idElementoLista
'                    sql = sql & " AND IDTARIFFA = " & pt.idAttributoElementoLista & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                    condizioniSQL = " AND IDTARIFFAPACCHETTO = " & pt.idAttributoElementoLista & _
'                        " AND IDPRODOTTO " & pt.idElementoLista
'                    Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO_TARIFFA", "IDTARIFFAPACCHETTO", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'                Else
                    sql = "INSERT INTO TARIFFAPACCHETTO_TARIFFA"
                    sql = sql & " (IDTARIFFAPACCHETTO, IDTARIFFA, IDPRODOTTO)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & idRecordSelezionato & ", "
                    sql = sql & pt.idAttributoElementoLista & ", "
                    sql = sql & pt.idElementoLista & ")"
                    SETAConnection.Execute sql, n, adCmdText
'                End If
            End If
        Next pt
    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazionePacchettoTariffe_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePacchettoTariffe
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTARIFFAPACCHETTO <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("TARIFFAPACCHETTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata, _
        "IDTARIFFAPACCHETTO <> " & idRecordSelezionato, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB per lo stasso Pacchetto;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If
    
End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazionePacchettoAree.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazionePacchettoAree.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub
'
'Private Function IsConfigurataAlmenoUnaTariffa() As Boolean
''    Dim n As Integer
''
''    n = adcConfigurazioneProdottoTariffe.Recordset.RecordCount
''    If n = 0 Then
''        IsConfigurataAlmenoUnaTariffa = False
''    Else
''        IsConfigurataAlmenoUnaTariffa = True
''    End If
'End Function

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim listaTabelleCorrelate As Collection
    Dim tariffaPacchettoEliminabile As Boolean
    Dim tabelleCorrelate As String
    Dim n As Long

    Call ApriConnessioneBD

    Set listaTabelleCorrelate = New Collection
    tariffaPacchettoEliminabile = True

'    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTO", CStr(idTar)) Then
'        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
'        tariffaEliminabile = False
'    End If

    If tariffaPacchettoEliminabile Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                sql = "DELETE FROM TARIFFAPACCHETTO_TARIFFA WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'                sql = "DELETE FROM TARIFFAPACCHETTO WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'            Else
'                Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO_TARIFFA", "IDTARIFFAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'                Call AggiornaParametriSessioneSuRecord("TARIFFAPACCHETTO", "IDTARIFFAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'            End If
'        Else
            sql = "DELETE FROM TARIFFAPACCHETTO_TARIFFA WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
            sql = "DELETE FROM TARIFFAPACCHETTO WHERE IDTARIFFAPACCHETTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Tariffa pacchetto selezionata", tabelleCorrelate)
    End If

    Call ChiudiConnessioneBD

End Sub

Private Sub CreaTabellaAppoggioPacchettoTariffa()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_PACC_TAR_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea
    sql = sql & " (IDTARIFFAPACCHETTO NUMBER(10), NOME VARCHAR2(2000), NUMEROASSOCIAZIONI NUMBER(10))"
    
    Call EliminaTabellaAppoggioPacchettoTariffa
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioPacchettoTariffa()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioPacchettoTariffa()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idTariffaPacchetto As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    Dim listaAppoggioPacchettoTariffa As Collection
    Dim descrizione As String
    
    Call ApriConnessioneBD
    
    Set listaAppoggioPacchettoTariffa = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDTARIFFAPACCHETTO"
'        sql = sql & " FROM TARIFFAPACCHETTO"
'        sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (IDTIPOSTATORECORD IS NULL OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT IDTARIFFAPACCHETTO"
        sql = sql & " FROM TARIFFAPACCHETTO"
        sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDTARIFFAPACCHETTO").Value
            Call listaAppoggioPacchettoTariffa.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioPacchettoTariffa
        i = 0
        campoNome = ""
        sql = " SELECT T.IDTARIFFA ID, P.NOME PROD, "
        sql = sql & " T.CODICE || ' - ' || T.NOME LABEL "
        sql = sql & " FROM TARIFFA T, TARIFFAPACCHETTO_TARIFFA TT, PRODOTTO P "
        sql = sql & " WHERE TT.IDTARIFFA(+) = T.IDTARIFFA "
        sql = sql & " AND T.IDPRODOTTO = P.IDPRODOTTO"
        sql = sql & " AND TT.IDTARIFFAPACCHETTO = " & recordTemporaneo.idElementoLista
        sql = sql & " ORDER BY PROD"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                descrizione = rec("PROD") & ": " & rec("LABEL")
                campoNome = IIf(campoNome = "", descrizione, campoNome & "; " & descrizione)
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
        recordTemporaneo.idAttributoElementoLista = i 'nota: qui dentro metto il numero di tariffe (quindi non č un id)
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioPacchettoTariffa
        idTariffaPacchetto = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea
        sql = sql & " VALUES (" & idTariffaPacchetto & ", "
        sql = sql & SqlStringValue(elencoNomi) & ", "
        sql = sql & recordTemporaneo.idAttributoElementoLista & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub lstProdottiTariffe_Click()
    Call VisualizzaListBoxToolTip(lstProdottiTariffe, "ID = " & lstProdottiTariffe.ItemData(lstProdottiTariffe.ListIndex) & "; " & lstProdottiTariffe.Text)
End Sub

Private Sub lstProdottiTariffe_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaProdottoTariffa
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaDeselezionaProdottoTariffa()
    Dim idProdotto As Long
    Dim pt As clsElementoLista

On Error Resume Next

    idProdotto = lstProdottiTariffe.ItemData(lstProdottiTariffe.ListIndex)
    Set pt = listaProdottiTariffe.Item(ChiaveId(idProdotto))
    If lstProdottiTariffe.Selected(lstProdottiTariffe.ListIndex) Then
        If RilevataTariffaAssociataAProdotto(idProdotto) Then
'            Set pt = New clsElementoLista
'            pt.idElementoLista = idProdotto
            pt.idAttributoElementoLista = frmDettagliTariffaPacchetto.GetIdTariffaSelezionata
            pt.descrizioneAttributoElementoLista = frmDettagliTariffaPacchetto.GetLabelTariffaSelezionata
            lstProdottiTariffe.List(lstProdottiTariffe.ListIndex) = pt.descrizioneElementoLista & _
                " / " & _
                pt.descrizioneAttributoElementoLista
            Call listaProdottiTariffe.Remove(ChiaveId(idProdotto))
            Call listaProdottiTariffe.Add(pt, ChiaveId(idProdotto))
        Else
            pt.idElementoLista = idProdotto
            pt.idAttributoElementoLista = idNessunElementoSelezionato
            pt.descrizioneElementoLista = pt.nomeElementoLista
            lstProdottiTariffe.Selected(lstProdottiTariffe.ListIndex) = False
        End If
    Else
'        Call listaProdottiSelezionati.Remove(ChiaveId(idProdotto))
        pt.idElementoLista = idProdotto
        pt.idAttributoElementoLista = idNessunElementoSelezionato
        pt.descrizioneElementoLista = pt.nomeElementoLista
        Call listaProdottiTariffe.Remove(ChiaveId(idProdotto))
        Call listaProdottiTariffe.Add(pt, ChiaveId(idProdotto))
        lstProdottiTariffe.List(lstProdottiTariffe.ListIndex) = pt.descrizioneElementoLista
        lstProdottiTariffe.Selected(lstProdottiTariffe.ListIndex) = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Function RilevataTariffaAssociataAProdotto(idProdotto As Long) As Boolean
    Dim idTariffa As Long
    
    Call frmDettagliTariffaPacchetto.SetIdProdottoSelezionato(idProdotto)
    Call frmDettagliTariffaPacchetto.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
'        Call frmDettagliTariffaPacchetto.SetIsPrimaTariffaAssociataPerSceltaProdotto(isPrimaTariffaAssociataPerSceltaProdotto(idProdotto))
        idTariffa = IdPrimaTariffaAssociataPerSceltaProdotto(idProdotto)
        Call frmDettagliTariffaPacchetto.SetIdPrimaTariffaAssociataPerSceltaProdotto(idTariffa)
        Call CaricaListaAreePacchetto
        Call CaricaListaPrezzi(idTariffa)
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        Call CaricaListaAreePacchetto
    End If
    Call frmDettagliTariffaPacchetto.SetListaPrezzi(listaPrezzi)
    Call frmDettagliTariffaPacchetto.SetListaAreePacchetto(listaAreePacchetto)
    Call frmDettagliTariffaPacchetto.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmDettagliTariffaPacchetto.Init
    If frmDettagliTariffaPacchetto.GetExitCode = EC_CONFERMA Then
        RilevataTariffaAssociataAProdotto = True
    Else
        RilevataTariffaAssociataAProdotto = False
    End If
End Function

'Private Function IsPrimaTariffaAssociataPerSceltaProdotto_OLD(idProdotto As Long) As Boolean
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim cont As Integer
'
'    cont = 0
''    sql = " SELECT COUNT(DISTINCT TP.IDTARIFFA) CONT"
''    sql = sql & " FROM"
''    sql = sql & " (SELECT SP.IDSCELTAPRODOTTO"
''    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
''    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
''    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
''    sql = sql & " AND SP.IDPRODOTTO = " & idProdotto
''    sql = sql & " ) SPR, SCELTAPRODOTTO_PRODOTTO SP, TARIFFAPACCHETTO_TARIFFA TP, TARIFFA T"
''    sql = sql & " WHERE SPR.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
''    sql = sql & " AND SP.IDPRODOTTO = TP.IDPRODOTTO"
''    sql = sql & " AND T.IDTARIFFA = TP.IDTARIFFA"
'    sql = " SELECT DISTINCT TP.IDTARIFFA"
'    sql = sql & " FROM"
'    sql = sql & " ("
'    sql = sql & " SELECT SP.IDSCELTAPRODOTTO"
'    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
'    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
'    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    sql = sql & " AND SP.IDPRODOTTO = " & idProdotto
'    sql = sql & " ) SC, SCELTAPRODOTTO_PRODOTTO SP, TARIFFAPACCHETTO_TARIFFA TP, TARIFFAPACCHETTO T"
'    sql = sql & " WHERE SC.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
'    sql = sql & " AND SP.IDPRODOTTO = TP.IDPRODOTTO"
'    sql = sql & " AND TP.IDTARIFFAPACCHETTO = T.IDTARIFFAPACCHETTO"
'    sql = sql & " AND T.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.EOF And rec.BOF) Then
'        cont = rec("CONT").Value
'    End If
'    rec.Close
'
'    IsPrimaTariffaAssociataPerSceltaProdotto_OLD = (cont = 0)
'End Function
'
'Private Sub CaricaValoriLstProdottiTariffe_OLD()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim idProdotto As Long
'    Dim descr As String
'    Dim pt As clsElementoLista
'    Dim i As Integer
'    Dim internalEventOld As Boolean
'
'    i = 1
'    Call ApriConnessioneBD
'    Call lstProdottiTariffe.Clear
'    Set listaProdottiTariffe = New Collection
'
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'    Else
''        sql = " SELECT P.IDPRODOTTO IDP, P.NOME PR, T.IDTARIFFA IDT, T.CODICE TCOD, T.NOME TNOM"
''        sql = sql & " FROM PRODOTTO P, SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S,"
''        sql = sql & " ("
''        sql = sql & " SELECT T.IDTARIFFA, T.CODICE, T.NOME, T.IDPRODOTTO, TP.IDOFFERTAPACCHETTO"
''        sql = sql & " FROM TARIFFA T, TARIFFAPACCHETTO TP, TARIFFAPACCHETTO_TARIFFA TT"
''        sql = sql & " WHERE T.IDTARIFFA = TT.IDTARIFFA"
''        sql = sql & " AND TT.IDTARIFFAPACCHETTO = TP.IDTARIFFAPACCHETTO"
'''        sql = sql & " AND TP.IDTARIFFAPACCHETTO = -1 "
''        sql = sql & " ) T  "
''        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO  "
''        sql = sql & " AND P.IDPRODOTTO = T.IDPRODOTTO(+)  "
''        sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
''        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
''        sql = sql & " ORDER BY PR"
''VA NECESSARIAMENTE AGGIUNTA UNA CONDIZIONE SUI PREZZI ESPRESSI SULLA GRIGLIA (SOLO PACCH. RATEIZZABILI);
''COSI' QUANDO SI CAMBIANO I PREZZI E SI FA CERCA LE EVENTUALI TARIFFE ASSOCIATE PRECEDENTEMENTE
''CON UN PREZZO DIVERSO NON COMPAIONO PIU'
''OPPURE
''UNA VOLTA FATTA LA QUERY, TARIFFA PER TARIFFA DI QUELLE GIA' ASSOCIATE SI DEVE
''DECIDERE SE E' ANCORA SSOCIATA, CONFRONTANDO I PREZZI DELLA TABELLA TEMPORANEA
''CON QUELLI DEL PRODOTTO CORRISPONDENTE
'        sql = " SELECT DISTINCT P.IDPRODOTTO IDP, P.NOME PR, T.IDTARIFFA IDT, T.CODICE TCOD, T.NOME TNOM"
'        sql = sql & " FROM PRODOTTO P, SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S,"
'        sql = sql & " ("
'        sql = sql & " SELECT T.IDTARIFFA, T.CODICE, T.NOME, T.IDPRODOTTO, TP.IDOFFERTAPACCHETTO"
'        sql = sql & " FROM TARIFFA T, TARIFFAPACCHETTO TP, TARIFFAPACCHETTO_TARIFFA TT"
'        sql = sql & " WHERE T.IDTARIFFA = TT.IDTARIFFA"
'        sql = sql & " AND TT.IDTARIFFAPACCHETTO = TP.IDTARIFFAPACCHETTO"
'        sql = sql & " AND TP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND TP.IDTARIFFAPACCHETTO = " & idRecordSelezionato
'        sql = sql & " ) T  "
'        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
'        sql = sql & " AND P.IDPRODOTTO = T.IDPRODOTTO(+)"
'        sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
'        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " ORDER BY PR"
'    End If
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set pt = New clsElementoLista
'            pt.idElementoLista = rec("IDP").Value
'            pt.idAttributoElementoLista = IIf(IsNull(rec("IDT")), idNessunElementoSelezionato, rec("IDT").Value)
'            pt.nomeElementoLista = rec("PR")
'            pt.nomeAttributoElementoLista = IIf(IsNull(rec("TCOD")), "", rec("TCOD") & " - " & rec("TNOM"))
'            If pt.nomeAttributoElementoLista = "" Then
'                pt.descrizioneElementoLista = pt.nomeElementoLista
'            Else
'                pt.descrizioneElementoLista = pt.nomeElementoLista & " / " & pt.nomeAttributoElementoLista
'            End If
''            Call EliminaElementoDaLista(listaProdottiTariffe, ChiaveId(pt.idElementoLista))
'            Call listaProdottiTariffe.Add(pt, ChiaveId(pt.idElementoLista))
'            Call lstProdottiTariffe.AddItem(pt.descrizioneElementoLista)
'            lstProdottiTariffe.ItemData(i - 1) = pt.idElementoLista
'
'            internalEventOld = internalEvent
'            internalEvent = True
'
'            lstProdottiTariffe.Selected(i - 1) = (pt.idAttributoElementoLista <> idNessunElementoSelezionato)
'
'            internalEvent = internalEventOld
'
'            i = i + 1
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'    Call ChiudiConnessioneBD
'End Sub
'
Private Sub EliminaElementoDaLista(lista As Collection, chiave As String)
    On Error Resume Next
    Call lista.Remove(chiave)
End Sub

Private Sub adcDefinizionePrezzi_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    Dim i As Integer
    Dim campoImportoBase As String
    Dim campoImportoPrevendita As String
    Dim areaPacchetto As clsElementoLista
'    Dim prezzo As clsPrezzo

    internalEventOld = internalEvent
    internalEvent = True

    Call PopolaTabellaAppoggioPrezzi

    Set d = adcDefinizionePrezzi

'    sql = "SELECT IDAREA, SUPERAREA"
    sql = "SELECT IDTARIFFAPACCHETTO"
'    For i = 1 To listaIdTariffe.count
'        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffe.Item(i), 30))
'        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffe.Item(i), 30))
'        sql = sql & ", " & _
'            campoImportoBase & ", " & campoImportoPrevendita
'    Next i
    For Each areaPacchetto In listaAreePacchetto
'        Set prezzo = listaPrezzi.Item(ChiaveId(areaPacchetto.idElementoLista))
        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(areaPacchetto.nomeElementoLista, 30))
        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & areaPacchetto.nomeElementoLista, 30))
        sql = sql & ", " & _
            campoImportoBase & ", " & campoImportoPrevendita
    Next areaPacchetto
'    For i = 1 To listaIdTariffeOmaggio.count
'        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggio.Item(i), 30))
'        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffeOmaggio.Item(i), 30))
'        sql = sql & ", " & _
'            campoImportoBase & ", " & campoImportoPrevendita
'    Next i
'    For i = 1 To listaIdTariffeServizio.count
'        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeServizio.Item(i), 30))
'        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffeServizio.Item(i), 30))
'        sql = sql & ", " & _
'            campoImportoBase & ", " & campoImportoPrevendita
'    Next i
    sql = sql & " FROM " & nomeTabellaPrezziTemporanea
'    sql = sql & " ORDER BY SUPERAREA"
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh

    internalEvent = internalEventOld

    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrDefinizionePrezzi_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim i As Integer
'    Dim j As Integer
'    Dim k As Integer
    Dim formato As New StdDataFormat
    Dim numeroColonne As Integer
    Dim areaPacchetto As clsElementoLista

    formato.Format = "###0.00"
    Set g = dgrDefinizionePrezzi
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
'    numeroColonne = 2 * (listaIdTariffe.count + listaIdTariffeOmaggio.count + listaIdTariffeServizio.count) + 1
    numeroColonne = 2 * (listaAreePacchetto.count) + 1
    g.Columns(0).Visible = False
    g.Columns(1).Width = 1500
    g.Columns(1).DividerStyle = dbgBlackLine
'    For i = 2 To (listaIdTariffe.count * 2) Step 2
    i = 1
    For Each areaPacchetto In listaAreePacchetto
        g.Columns(i + 1).DividerStyle = dbgBlackLine
        g.Columns(i).DividerStyle = dbgLightGrayLine
        g.Columns(i + 1).Width = 600
        g.Columns(i + 1).Caption = "PREV"
        g.Columns(i).Width = 1500
        Set g.Columns(i + 1).DataFormat = formato
        Set g.Columns(i).DataFormat = formato
        i = i + 2
'    Next i
    Next areaPacchetto
'    For j = i To i + (listaIdTariffeOmaggio.count * 2) - 1 Step 2
'        g.Columns(j + 1).DividerStyle = dbgBlackLine
'        g.Columns(j).DividerStyle = dbgLightGrayLine
'        g.Columns(j + 1).Width = 600
'        g.Columns(j + 1).Caption = "PREV"
'        g.Columns(j).Width = 1500
'        Set g.Columns(j + 1).DataFormat = formato
'        Set g.Columns(j).DataFormat = formato
'    Next j
'    For k = j To j + (listaIdTariffeServizio.count * 2) - 1 Step 2
'        g.Columns(k + 1).DividerStyle = dbgBlackLine
'        g.Columns(k).DividerStyle = dbgLightGrayLine
'        g.Columns(k + 1).Width = 600
'        g.Columns(k + 1).Caption = "PREV"
'        g.Columns(k).Width = 1500
'        Set g.Columns(k + 1).DataFormat = formato
'        Set g.Columns(k).DataFormat = formato
'    Next k

    g.MarqueeStyle = dbgHighlightCell
End Sub

Private Sub PopolaTabellaAppoggioPrezzi()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim id As Integer
    Dim i As Integer
'    Dim j As Integer
'    Dim k As Integer
'    Dim l As Integer
'    Dim m As Integer
'    Dim idArea As Long
'    Dim campoNome As String
'    Dim codice As String
'    Dim label As String
'    Dim idImporto As Long
'    Dim campoImportoBase As String
'    Dim campoImportoPrevendita As String
    Dim valori As String
    Dim valoriPrevendita As String
'    Dim tipoSA As TipoAreaEnum
    Dim n As Long
    Dim areaPacchetto As clsElementoLista
    Dim prezzo As clsPrezzo
    
    Call ApriConnessioneBD
    
'    Set listaIdArea = New Collection
'    Set listaNomeArea = New Collection
    
    'A: la tabella viene pulita (solo se precedentemente popolata)
    sql = "DELETE FROM " & nomeTabellaPrezziTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'    'B: si ricavano le superaree relative al prodotto corrente (e alla sua pianta), compresa la superarea di servizio
'    sql = "SELECT CODICE, IDAREA, NOME, IDTIPOAREA" & _
'        " FROM AREA" & _
'        " WHERE IDPIANTA = " & idPiantaSelezionata & _
'        " AND IDTIPOAREA IN (" & TA_SUPERAREA_SERVIZIO & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
'        " ORDER BY CODICE"
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            tipoSA = rec("IDTIPOAREA")
'            idArea = rec("IDAREA")
'            campoNome = rec("NOME")
'            codice = rec("CODICE")
'            label = codice & " - " & campoNome
'            If tipoSA = TA_SUPERAREA_SERVIZIO Then
'                idSuperareaServizio = idArea
'            Else
'                Call listaIdArea.Add(idArea)
'                Call listaNomeArea.Add(label)
'            End If
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close

    'C: vengono caricati i dati (importi) per area, tariffa e prodotto
'    For j = 1 To listaIdArea.count
'        Set listaIdAppoggioPrezzi = New Collection
'        Set listaNomeAppoggioPrezzi = New Collection
'        Set listaPrevenditaAppoggioPrezzi = New Collection
'        Set listaPrezzi = New Collection
''        For i = 1 To listaIdTariffe.count
'        For Each areaPacchetto In listaAreePacchetto
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''                sql = "SELECT IMPORTOBASE, "
''                sql = sql & " IMPORTOSERVIZIOPREVENDITA,"
''                sql = sql & " IDPREZZOTITOLOPRODOTTO AS ""IDIM"""
''                sql = sql & " FROM PREZZOTITOLOPRODOTTO"
''                sql = sql & " WHERE IDTARIFFA = " & listaIdTariffe.Item(i)
''                sql = sql & " AND IDAREA = " & listaIdArea.Item(j)
''                sql = sql & " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
''                sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
''                sql = sql & " AND (IDTIPOSTATORECORD <> " & TSR_ELIMINATO
''                sql = sql & " OR IDTIPOSTATORECORD IS NULL)"
'            Else
''                sql = " SELECT IMPORTOBASE, "
''                sql = sql & " IMPORTOSERVIZIOPREVENDITA,"
''                sql = sql & " IDPREZZOTITOLOPRODOTTO IDIM"
''                sql = sql & " FROM PREZZOTITOLOPRODOTTO"
''                sql = sql & " WHERE IDTARIFFA = " & listaIdTariffe.Item(i)
''                sql = sql & " AND IDAREA = " & listaIdArea.Item(j)
''                sql = sql & " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
''                sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
'                sql = " SELECT IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
'                sql = sql & " IDPREZZOTITOLOPRODOTTO"
'                sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP,"
'                sql = sql & " ("
'                sql = sql & " SELECT DISTINCT T.IDTARIFFA"
'                sql = sql & " FROM TARIFFAPACCHETTO_TARIFFA TT, TARIFFA T"
'                sql = sql & " WHERE TT.IDTARIFFA = T.IDTARIFFA "
'                sql = sql & " AND TT.IDTARIFFAPACCHETTO = " & idRecordSelezionato
'                sql = sql & " AND ROWNUM = 1"
'                sql = sql & " ) OT,"
'                sql = sql & " ("
'                sql = sql & " SELECT DISTINCT A.IDAREA"
'                sql = sql & " FROM AREAPACCHETTO_AREA AA, AREA A"
'                sql = sql & " WHERE AA.IDAREA = A.IDAREA "
'                sql = sql & " AND AA.IDAREAPACCHETTO = " & areaPacchetto.idElementoLista
'                sql = sql & " ) OA"
'                sql = sql & " WHERE PTP.IDAREA = OA.IDAREA"
'                sql = sql & " AND PTP.IDTARIFFA = OT.IDTARIFFA"
''                sql = sql & " AND ROWNUM = 1"
'            End If
'            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'            If Not (rec.BOF And rec.EOF) Then
'                rec.MoveFirst
'                While Not rec.EOF
'                    Set prezzo = New clsPrezzo
''                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
''                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
''                    idImporto = IIf(IsNull(rec("IDPREZZOTITOLOPRODOTTO")), idNessunElementoSelezionato, rec("IDPREZZOTITOLOPRODOTTO"))
'                    prezzo.stringaImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
'                    prezzo.stringaImportoServizioPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
'                    prezzo.idPrezzoTitoloProdotto = IIf(IsNull(rec("IDPREZZOTITOLOPRODOTTO")), idNessunElementoSelezionato, rec("IDPREZZOTITOLOPRODOTTO"))
'                    rec.MoveNext
'                Wend
''                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
''                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
''                Call listaIdAppoggioPrezzi.Add(idImporto)
'                Call listaPrezzi.Add(prezzo, ChiaveId(areaPacchetto.idElementoLista))
'            Else
''                Call listaNomeAppoggioPrezzi.Add("")
''                Call listaPrevenditaAppoggioPrezzi.Add("")
''                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
'                Call listaPrezzi.Add(PrezzoNullo, ChiaveId(areaPacchetto.idElementoLista))
'            End If
'            rec.Close
'        Next areaPacchetto
''        For i = 1 To listaIdTariffeOmaggio.count 'OMAGGIO
''            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''                sql = "SELECT IMPORTOBASE, " & _
''                    " IMPORTOSERVIZIOPREVENDITA," & _
''                    " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
''                    " FROM PREZZOTITOLOPRODOTTO" & _
''                    " WHERE IDTARIFFA = " & listaIdTariffeOmaggio.Item(i) & _
''                    " AND IDAREA = " & listaIdArea.Item(j) & _
''                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
''                    " AND IDPRODOTTO = " & idProdottoSelezionato & _
''                    " AND (IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
''                    " OR IDTIPOSTATORECORD IS NULL)"
''            Else
''                sql = "SELECT IMPORTOBASE, " & _
''                    " IMPORTOSERVIZIOPREVENDITA," & _
''                    " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
''                    " FROM PREZZOTITOLOPRODOTTO" & _
''                    " WHERE IDTARIFFA = " & listaIdTariffeOmaggio.Item(i) & _
''                    " AND IDAREA = " & listaIdArea.Item(j) & _
''                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
''                    " AND IDPRODOTTO = " & idProdottoSelezionato
''            End If
''            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
''            If Not (rec.BOF And rec.EOF) Then
''                rec.MoveFirst
''                While Not rec.EOF
''                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
''                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
''                    idImporto = IIf(IsNull(rec("IDIM")), idNessunElementoSelezionato, rec("IDIM"))
''                    rec.MoveNext
''                Wend
''                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
''                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
''                Call listaIdAppoggioPrezzi.Add(idImporto)
''            Else
''                Call listaNomeAppoggioPrezzi.Add("")
''                Call listaPrevenditaAppoggioPrezzi.Add("")
''                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
''            End If
''            rec.Close
''        Next i
''        For i = 1 To listaIdTariffeServizio.count 'SERVIZIO
''            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''                sql = "SELECT IMPORTOBASE, " & _
''                    " IMPORTOSERVIZIOPREVENDITA," & _
''                    " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
''                    " FROM PREZZOTITOLOPRODOTTO" & _
''                    " WHERE IDTARIFFA = " & listaIdTariffeServizio.Item(i) & _
''                    " AND IDAREA = " & listaIdArea.Item(j) & _
''                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
''                    " AND IDPRODOTTO = " & idProdottoSelezionato & _
''                    " AND (IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
''                    " OR IDTIPOSTATORECORD IS NULL)"
''            Else
''                sql = "SELECT IMPORTOBASE, " & _
''                    " IMPORTOSERVIZIOPREVENDITA," & _
''                    " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
''                    " FROM PREZZOTITOLOPRODOTTO" & _
''                    " WHERE IDTARIFFA = " & listaIdTariffeServizio.Item(i) & _
''                    " AND IDAREA = " & listaIdArea.Item(j) & _
''                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
''                    " AND IDPRODOTTO = " & idProdottoSelezionato
''            End If
''            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
''            If Not (rec.BOF And rec.EOF) Then
''                rec.MoveFirst
''                While Not rec.EOF
''                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
''                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
''                    idImporto = IIf(IsNull(rec("IDIM")), idNessunElementoSelezionato, rec("IDIM"))
''                    rec.MoveNext
''                Wend
''                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
''                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
''                Call listaIdAppoggioPrezzi.Add(idImporto)
''            Else
''                Call listaNomeAppoggioPrezzi.Add("")
''                Call listaPrevenditaAppoggioPrezzi.Add("")
''                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
''            End If
''            rec.Close
''        Next i
        Call CaricaListaPrezzi(idNessunElementoSelezionato)
        
        'D: vengono inseriti i record precedentemente tirati sų
        Dim val1 As String
        Dim val2 As String
        valori = ""
        valoriPrevendita = ""
'        If listaNomeAppoggioPrezzi.count > 0 Then
        If listaPrezzi.count > 0 Then
'            sql = "INSERT INTO " & nomeTabellaPrezziTemporanea & " (IDAREA, SUPERAREA"
            sql = "INSERT INTO " & nomeTabellaPrezziTemporanea & " (IDTARIFFAPACCHETTO"
'            valori = listaIdArea.Item(j) & ", " & SqlStringValue(listaNomeArea.Item(j))
            valori = CStr(idRecordSelezionato)
'            For k = 1 To listaIdTariffe.count
            For Each areaPacchetto In listaAreePacchetto
                Set prezzo = listaPrezzi.Item(ChiaveId(areaPacchetto.idElementoLista))
'                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffe.Item(k), 30)) & _
'                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffe.Item(k), 30))
                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(areaPacchetto.nomeElementoLista, 30)) & _
                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & areaPacchetto.nomeElementoLista, 30))
'                If listaNomeAppoggioPrezzi.Item(k) = "" Then
'                    val1 = "NULL"
'                Else
'                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k)))
'                End If
                If prezzo.stringaImportoBase = "" Then
                    val1 = "NULL"
                Else
                    val1 = StringaConPuntoDecimale(CDbl(prezzo.stringaImportoBase))
                End If
                If prezzo.stringaImportoServizioPrevendita = "" Then
                    val2 = "NULL"
                Else
                    val2 = StringaConPuntoDecimale(CDbl(prezzo.stringaImportoServizioPrevendita))
                End If
                valori = valori & "," & val1 & "," & val2
'            Next k
            Next areaPacchetto
'            For l = 1 To listaIdTariffeOmaggio.count
'                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggio.Item(l), 30)) & _
'                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffeOmaggio.Item(l), 30))
'                If listaNomeAppoggioPrezzi.Item(k - 1 + l) = "" Then
'                    val1 = "NULL"
'                Else
'                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k - 1 + l)))
'                End If
'                If listaPrevenditaAppoggioPrezzi.Item(k - 1 + l) = "" Then
'                    val2 = "NULL"
'                Else
'                    val2 = StringaConPuntoDecimale(CDbl(listaPrevenditaAppoggioPrezzi.Item(k - 1 + l)))
'                End If
'                valori = valori & "," & val1 & "," & val2
'            Next l
'            For m = 1 To listaIdTariffeServizio.count
'                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeServizio.Item(m), 30)) & _
'                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffeServizio.Item(m), 30))
'                If listaNomeAppoggioPrezzi.Item(k - 2 + l + m) = "" Then
'                    val1 = "NULL"
'                Else
'                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k - 2 + l + m)))
'                End If
'                If listaPrevenditaAppoggioPrezzi.Item(k - 2 + l + m) = "" Then
'                    val2 = "NULL"
'                Else
'                    val2 = StringaConPuntoDecimale(CDbl(listaPrevenditaAppoggioPrezzi.Item(k - 2 + l + m)))
'                End If
'                valori = valori & "," & val1 & "," & val2
'            Next m
            If Right$(valori, 2) = ", " Then valori = Left$(valori, Len(valori) - 2)
            sql = sql & ") VALUES (" & valori & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
'    Next j
    
    Call ChiudiConnessioneBD
'    Call pgbAvanzamento_Init(listaIdArea.count * _
'        (listaIdTariffe.count + listaIdTariffeOmaggio.count + listaIdTariffeServizio.count + 1) + listaIdTariffeServizio.count)
End Sub

Private Sub CaricaListaPrezzi(idTariffa As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim areaPacchetto As clsElementoLista
    Dim prezzo As clsPrezzo
'    Dim internalEventOld As Boolean
'
'    internalEventOld = internalEvent
'    internalEvent = True
    
    Set listaPrezzi = New Collection
    For Each areaPacchetto In listaAreePacchetto
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'
'        Else
'            If Not IsMissing(idTariffa) Then
            If idTariffa <> idNessunElementoSelezionato Then
                sql = " SELECT IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
                sql = sql & " IDPREZZOTITOLOPRODOTTO, PTP.IDAREA"
                sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP,"
                sql = sql & " ("
                sql = sql & " SELECT DISTINCT A.IDAREA"
                sql = sql & " FROM AREAPACCHETTO_AREA AA, AREA A, AREAPACCHETTO AP"
                sql = sql & " WHERE AA.IDAREA = A.IDAREA "
                sql = sql & " AND AP.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
                sql = sql & " AND AA.IDAREAPACCHETTO = " & areaPacchetto.idElementoLista
                sql = sql & " AND AP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
                sql = sql & " ) OA"
                sql = sql & " WHERE PTP.IDAREA = OA.IDAREA"
                sql = sql & " AND PTP.IDTARIFFA = " & idTariffa
            Else
                sql = " SELECT IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
                sql = sql & " IDPREZZOTITOLOPRODOTTO, PTP.IDAREA"
                sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP,"
                sql = sql & " ("
                sql = sql & " SELECT DISTINCT T.IDTARIFFA"
                sql = sql & " FROM TARIFFAPACCHETTO_TARIFFA TT, TARIFFA T, TARIFFAPACCHETTO TP"
                sql = sql & " WHERE TT.IDTARIFFA = T.IDTARIFFA "
                sql = sql & " AND TP.IDTARIFFAPACCHETTO = TT.IDTARIFFAPACCHETTO"
                sql = sql & " AND TT.IDTARIFFAPACCHETTO = " & idRecordSelezionato
                sql = sql & " AND TP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
                sql = sql & " AND ROWNUM = 1"
                sql = sql & " ) OT,"
                sql = sql & " ("
                sql = sql & " SELECT DISTINCT A.IDAREA"
                sql = sql & " FROM AREAPACCHETTO_AREA AA, AREA A, AREAPACCHETTO AP"
                sql = sql & " WHERE AA.IDAREA = A.IDAREA "
                sql = sql & " AND AP.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
                sql = sql & " AND AA.IDAREAPACCHETTO = " & areaPacchetto.idElementoLista
                sql = sql & " AND AP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
                sql = sql & " ) OA"
                sql = sql & " WHERE PTP.IDAREA = OA.IDAREA"
                sql = sql & " AND PTP.IDTARIFFA = OT.IDTARIFFA"
            End If
'        End If
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set prezzo = New clsPrezzo
                prezzo.stringaImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
                prezzo.importoBase = IIf(IsNull(rec("IMPORTOBASE")), idNessunElementoSelezionato, rec("IMPORTOBASE"))
                prezzo.stringaImportoServizioPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
                prezzo.importoServizioPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), idNessunElementoSelezionato, rec("IMPORTOSERVIZIOPREVENDITA"))
                prezzo.idPrezzoTitoloProdotto = IIf(IsNull(rec("IDPREZZOTITOLOPRODOTTO")), idNessunElementoSelezionato, rec("IDPREZZOTITOLOPRODOTTO"))
                prezzo.idArea = IIf(IsNull(rec("IDAREA")), idNessunElementoSelezionato, rec("IDAREA"))
                rec.MoveNext
            Wend
            Call listaPrezzi.Add(prezzo, ChiaveId(areaPacchetto.idElementoLista))
        Else
            Call listaPrezzi.Add(PrezzoNullo, ChiaveId(areaPacchetto.idElementoLista))
        End If
        rec.Close
    Next areaPacchetto
'
'    internalEvent = internalEventOld
End Sub

Private Sub CreaTabellaAppoggioPrezzi()
    Dim sql As String
    Dim i As Integer
    Dim stringaSQL As String
    Dim tariffaSQL As String
    Dim areaPacchetto As clsElementoLista
    
    Call CaricaListaAreePacchetto
    
    nomeTabellaPrezziTemporanea = SqlStringTableName("TMP_PAC_PREZ_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaPrezziTemporanea
'    sql = sql & " (IDAREA NUMBER(10), SUPERAREA VARCHAR2(50)"
    sql = sql & " (IDTARIFFAPACCHETTO NUMBER(10)"
'    For i = 1 To listaIdAreePacchetto.count
'        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffe.Item(i), 30)) & " NUMBER(10,2)" & _
'            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffe.Item(i), 30)) & " NUMBER (10,2)"
'    Next i
'    For i = 1 To listaIdTariffeOmaggio.count
'        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggio.Item(i), 30)) & " NUMBER(10,2)" & _
'            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffeOmaggio.Item(i), 30)) & " NUMBER (10,2)"
'    Next i
'    For i = 1 To listaIdTariffeServizio.count
'        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeServizio.Item(i), 30)) & " NUMBER(10,2)" & _
'            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & listaNomiTariffeServizio.Item(i), 30)) & " NUMBER (10,2)"
'    Next i
    For Each areaPacchetto In listaAreePacchetto
        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(areaPacchetto.nomeElementoLista, 30)) & " NUMBER(10,2)" & _
            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & areaPacchetto.nomeElementoLista, 30)) & " NUMBER (10,2)"
    Next areaPacchetto
    sql = sql & ")"
    
    Call EliminaTabellaAppoggioPrezzi
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioPrezzi()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaPrezziTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

'Private Sub CaricaListaAreePacchetto_old()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim id As Long
'    Dim nome As String
'    Dim elemento As clsElementoLista
'
'    Call ApriConnessioneBD
'
'    Set listaAreePacchetto = New Collection
'    sql = "SELECT IDAREAPACCHETTO, NOME"
'    sql = sql & " FROM AREAPACCHETTO"
'    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.EOF And rec.BOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set elemento = New clsElementoLista
'            id = rec("IDAREAPACCHETTO").Value
'            nome = rec("NOME").Value
'            elemento.idElementoLista = id
'            elemento.nomeElementoLista = nome
'            Call listaAreePacchetto.Add(elemento, ChiaveId(id))
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
'    Call ChiudiConnessioneBD
'
'End Sub
'
Private Function PrezzoNullo() As clsPrezzo
    Dim p As clsPrezzo
    
    Set p = New clsPrezzo
    p.idPrezzoTitoloProdotto = idNessunElementoSelezionato
    p.idProdotto = idNessunElementoSelezionato
    p.idTariffa = idNessunElementoSelezionato
    p.idArea = idNessunElementoSelezionato
    p.idPeriodoCommerciale = idNessunElementoSelezionato
    p.idTipoTariffaSIAE = idNessunElementoSelezionato
    p.importoBase = 0
    p.importoServizioPrevendita = 0
    p.stringaImportoBase = ""
    p.stringaImportoServizioPrevendita = ""
    
    Set PrezzoNullo = p
End Function

Private Sub RilevaValoriPrezzi()
    Dim g As DataGrid
    Dim a As Adodc
    Dim internalEventOld As Boolean
    Dim prezzo As clsPrezzo
    Dim areaPacchetto As clsElementoLista
    Dim nomeCampoImporto As String
    Dim nomeCampoPrevendita As String
    
    Set g = dgrDefinizionePrezzi
    Set a = adcDefinizionePrezzi
'    Set listaPrezzi = New Collection

    Call ApriConnessioneBD
    
    internalEventOld = internalEvent
    internalEvent = True
    If a.Recordset.RecordCount > 0 Then
        For Each areaPacchetto In listaAreePacchetto
'            Set prezzo = listaPrezzi.Item(ChiaveId(areaPacchetto.idElementoLista))
            Set prezzo = New clsPrezzo
            nomeCampoImporto = StringaAlfanumerica(StringaLunghezzaMaxFissata(areaPacchetto.nomeElementoLista, 30))
            nomeCampoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PREV_" & areaPacchetto.nomeElementoLista, 30))
            prezzo.stringaImportoBase = ""
            If Not IsNull(a.Recordset(nomeCampoImporto)) Then
                prezzo.stringaImportoBase = CStr(a.Recordset(nomeCampoImporto).Value * 100)
                prezzo.importoBase = CLng(a.Recordset(nomeCampoImporto).Value * 100)
            End If
            prezzo.stringaImportoServizioPrevendita = ""
            If Not IsNull(a.Recordset(nomeCampoPrevendita)) Then
                prezzo.stringaImportoServizioPrevendita = CStr(a.Recordset(nomeCampoPrevendita).Value * 100)
                prezzo.importoServizioPrevendita = CLng(a.Recordset(nomeCampoPrevendita).Value * 100)
            End If
            prezzo.idArea = areaPacchetto.idAttributoElementoLista
            Call listaPrezzi.Remove(ChiaveId(areaPacchetto.idElementoLista))
            Call listaPrezzi.Add(prezzo, ChiaveId(areaPacchetto.idElementoLista))
        Next areaPacchetto
    End If
    internalEvent = internalEventOld
    
    Call ChiudiConnessioneBD
        
End Sub

Private Sub ValorizzaCampiDate()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    
    Call ApriConnessioneBD
    
    dataOraInizio = dataNulla
    dataOraFine = dataNulla
    sql = " SELECT MAX(DATAORAINIZIO) INIZIO, MIN(DATAORAFINE) FINE"
    sql = sql & " FROM PERIODOCOMMERCIALE P, SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
    sql = sql & " WHERE P.IDPRODOTTO = SP.IDPRODOTTO"
    sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
'        If Not IsNull(rec("INIZIO")) Then
        If Not (IsNull(rec("INIZIO")) Or IsNull(rec("FINE"))) Then
            dataOraInizio = rec("INIZIO").Value
            dataOraFine = rec("FINE").Value
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    txtDataOraInizio.Text = IIf(dataOraInizio = dataNulla, "", CStr(dataOraInizio))
    txtDataOraFine.Text = IIf(dataOraFine = dataNulla, "", CStr(dataOraFine))
End Sub

'Private Function isPrimaTariffaAssociataPerSceltaProdotto(idProdotto As Long) As Boolean
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim cont As Integer
'    Dim prodotto As clsElementoLista
'
'    cont = 0
'    sql = " SELECT DISTINCT SPP.IDPRODOTTO"
'    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP,"
'    sql = sql & " ("
'    sql = sql & " SELECT SP.IDSCELTAPRODOTTO"
'    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP"
'    sql = sql & " WHERE SP.IDPRODOTTO = " & idProdotto
'    sql = sql & " ) S"
'    sql = sql & " WHERE SPP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.EOF And rec.BOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set prodotto = listaProdottiTariffe.Item(ChiaveId(rec("IDPRODOTTO").Value))
''            If Not (prodotto Is Nothing) Then
'            If prodotto.idAttributoElementoLista <> idNessunElementoSelezionato Then
'                cont = cont + 1
'            End If
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
'    isPrimaTariffaAssociataPerSceltaProdotto = (cont = 0)
'End Function
'
Private Function IdPrimaTariffaAssociataPerSceltaProdotto(idProdotto As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim prodotto As clsElementoLista
    Dim idTariffa As Long
'    Dim internalEventOld As Boolean
    
    Call ApriConnessioneBD
    
'    internalEventOld = internalEvent
'    internalEvent = True
    
    idTariffa = idNessunElementoSelezionato
'    sql = " SELECT DISTINCT SPP.IDPRODOTTO"
'    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP,"
'    sql = sql & " ("
'    sql = sql & " SELECT SP.IDSCELTAPRODOTTO"
'    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP"
'    sql = sql & " WHERE SP.IDPRODOTTO = " & idProdotto
'    sql = sql & " ) S"
    sql = " SELECT DISTINCT SPP.IDPRODOTTO"
    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP,"
    sql = sql & " ("
    sql = sql & " SELECT SP.IDSCELTAPRODOTTO "
    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    sql = sql & " AND SP.IDPRODOTTO = " & idProdotto
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " ) S"
    sql = sql & " WHERE SPP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodotto = listaProdottiTariffe.Item(ChiaveId(rec("IDPRODOTTO").Value))
'            If Not (prodotto Is Nothing) Then
            If prodotto.idAttributoElementoLista <> idNessunElementoSelezionato Then
                idTariffa = prodotto.idAttributoElementoLista
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
'    internalEvent = internalEventOld
    
    Call ChiudiConnessioneBD
    
    IdPrimaTariffaAssociataPerSceltaProdotto = idTariffa
End Function

Private Sub CaricaValoriLstProdottiTariffe()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idProdotto As Long
    Dim descr As String
    Dim pt As clsElementoLista
    Dim i As Integer
    Dim idTariffa As Long
    Dim nomeTariffa As String
    Dim internalEventOld As Boolean
    
    i = 1
    Call ApriConnessioneBD
    Call lstProdottiTariffe.Clear
    Set listaProdottiTariffe = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        'To Do
'    Else
        sql = " SELECT DISTINCT P.IDPRODOTTO IDP, P.NOME PR, T.IDTARIFFA IDT, T.CODICE TCOD, T.NOME TNOM"
        sql = sql & " FROM PRODOTTO P, SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S,"
        sql = sql & " ("
        sql = sql & " SELECT T.IDTARIFFA, T.CODICE, T.NOME, T.IDPRODOTTO, TP.IDOFFERTAPACCHETTO"
        sql = sql & " FROM TARIFFA T, TARIFFAPACCHETTO TP, TARIFFAPACCHETTO_TARIFFA TT"
        sql = sql & " WHERE T.IDTARIFFA = TT.IDTARIFFA"
        sql = sql & " AND TT.IDTARIFFAPACCHETTO = TP.IDTARIFFAPACCHETTO"
        sql = sql & " AND TP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " AND TP.IDTARIFFAPACCHETTO = " & idRecordSelezionato
        sql = sql & " ) T  "
        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
        sql = sql & " AND P.IDPRODOTTO = T.IDPRODOTTO(+)"
        sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ORDER BY PR"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set pt = New clsElementoLista
            pt.idElementoLista = rec("IDP").Value
            pt.nomeElementoLista = rec("PR")
            idTariffa = IIf(IsNull(rec("IDT")), idNessunElementoSelezionato, rec("IDT").Value)
            nomeTariffa = IIf(IsNull(rec("TCOD")), "", rec("TCOD") & " - " & rec("TNOM"))
            If tipoOffertaPacchettoSelezionata <> TOP_PREZZO_RATEIZZABILE Or _
                (tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE And _
                IsTariffaConformeConPrezziIndicati(idTariffa)) Then
                pt.idAttributoElementoLista = idTariffa
                pt.nomeAttributoElementoLista = nomeTariffa
            Else
                pt.idAttributoElementoLista = idNessunElementoSelezionato
                pt.nomeAttributoElementoLista = ""
            End If
            If pt.nomeAttributoElementoLista = "" Then
                pt.descrizioneElementoLista = pt.nomeElementoLista
            Else
                pt.descrizioneElementoLista = pt.nomeElementoLista & " / " & pt.nomeAttributoElementoLista
            End If
            Call listaProdottiTariffe.Add(pt, ChiaveId(pt.idElementoLista))
            Call lstProdottiTariffe.AddItem(pt.descrizioneElementoLista)
            lstProdottiTariffe.ItemData(i - 1) = pt.idElementoLista
            
            internalEventOld = internalEvent
            internalEvent = True
            
            lstProdottiTariffe.Selected(i - 1) = (pt.idAttributoElementoLista <> idNessunElementoSelezionato)
            
            internalEvent = internalEventOld
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD
End Sub

Private Function IsTariffaConformeConPrezziIndicati(idTariffa As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim prezzo As clsPrezzo
    Dim prezziOK As Boolean
    Dim idArea As Long
    Dim idAreaPacchetto As Long
    
    prezziOK = False
    sql = " SELECT IDPREZZOTITOLOPRODOTTO,"
    sql = sql & " IDAREA, IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA"
    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP"
    sql = sql & " WHERE PTP.IDTARIFFA = " & idTariffa
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idArea = IIf(IsNull(rec("IDAREA")), idNessunElementoSelezionato, rec("IDAREA").Value)
            idAreaPacchetto = IIf(idArea = idNessunElementoSelezionato, idNessunElementoSelezionato, IdAreaPacchettoDaIdArea(idArea, idOffertaPacchettoSelezionata))
            Set prezzo = PrezzoDaLista(ChiaveId(idAreaPacchetto))
            If Not (prezzo Is Nothing) Then
                If prezzo.importoBase = rec("IMPORTOBASE").Value And _
                    prezzo.importoServizioPrevendita = rec("IMPORTOSERVIZIOPREVENDITA").Value Then
                    prezziOK = True
                End If
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    IsTariffaConformeConPrezziIndicati = prezziOK
End Function

'Private Function IdAreaPacchettoDaIdArea(idArea As Long) As Long
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim id As Long
'
'    id = idNessunElementoSelezionato
'    sql = " SELECT DISTINCT A.IDAREAPACCHETTO ID"
'    sql = sql & " FROM AREAPACCHETTO_AREA AA, AREAPACCHETTO A"
'    sql = sql & " WHERE A.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
'    sql = sql & " AND AA.IDAREA = " & idArea
'    sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.EOF And rec.BOF) Then
'        rec.MoveFirst
'        id = rec("ID").Value
'    End If
'    rec.Close
'
'    IdAreaPacchettoDaIdArea = id
'End Function

Private Function PrezzoDaLista(chiave As String) As clsPrezzo
    On Error Resume Next
    Set PrezzoDaLista = listaPrezzi.Item(chiave)
End Function

Private Sub CaricaListaAreePacchetto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idCorrente As Long
    Dim idPrecedente As Long
    Dim nome As String
    Dim elemento As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAreePacchetto = New Collection
    idPrecedente = idNessunElementoSelezionato
'    sql = "SELECT IDAREAPACCHETTO, NOME"
'    sql = sql & " FROM AREAPACCHETTO"
'    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = " SELECT DISTINCT A.IDAREAPACCHETTO, A.NOME, AA.IDAREA"
    sql = sql & " FROM AREAPACCHETTO A, AREAPACCHETTO_AREA AA"
    sql = sql & " WHERE A.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
    sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " ORDER BY A.IDAREAPACCHETTO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set elemento = New clsElementoLista
            idCorrente = rec("IDAREAPACCHETTO").Value
            nome = rec("NOME").Value
            elemento.idElementoLista = idCorrente
            elemento.idAttributoElementoLista = rec("IDAREA").Value
            elemento.nomeElementoLista = nome
            If idCorrente <> idPrecedente Then
                Call listaAreePacchetto.Add(elemento, ChiaveId(idCorrente))
            End If
            idPrecedente = idCorrente
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

