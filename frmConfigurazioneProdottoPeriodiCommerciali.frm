VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneProdottoPeriodiCommerciali 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "10"
   Begin VB.TextBox txtDataOraTermineAnnullo 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8220
      MaxLength       =   30
      TabIndex        =   47
      Top             =   4980
      Width           =   2175
   End
   Begin VB.TextBox txtDataOraInizio 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5760
      MaxLength       =   30
      TabIndex        =   45
      Top             =   4980
      Width           =   2175
   End
   Begin VB.TextBox txtMinutiTerminePrevendita 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   11040
      MaxLength       =   2
      TabIndex        =   14
      Top             =   6840
      Width           =   435
   End
   Begin VB.TextBox txtOraTerminePrevendita 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   10440
      MaxLength       =   2
      TabIndex        =   13
      Top             =   6840
      Width           =   435
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   6480
      Width           =   4395
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   5820
      Width           =   3315
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   36
      Top             =   4560
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtOraInizio 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   10440
      MaxLength       =   2
      TabIndex        =   7
      Top             =   5820
      Width           =   435
   End
   Begin VB.TextBox txtMinutiInizio 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   11040
      MaxLength       =   2
      TabIndex        =   8
      Top             =   5820
      Width           =   435
   End
   Begin VB.TextBox txtOraTermine 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   10440
      MaxLength       =   2
      TabIndex        =   10
      Top             =   6180
      Width           =   435
   End
   Begin VB.TextBox txtMinutiTermine 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   11040
      MaxLength       =   2
      TabIndex        =   11
      Top             =   6180
      Width           =   435
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   27
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   19
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   24
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   23
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   16
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoPeriodiCommerciali 
      Height          =   330
      Left            =   240
      Top             =   3780
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoPeriodiCommerciali 
      Height          =   3615
      Left            =   120
      TabIndex        =   21
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6376
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDataTermine 
      Height          =   315
      Left            =   6900
      TabIndex        =   9
      Top             =   6180
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   556
      _Version        =   393216
      Format          =   72024065
      CurrentDate     =   37606
   End
   Begin MSComCtl2.DTPicker dtpDataInizio 
      Height          =   315
      Left            =   6900
      TabIndex        =   6
      Top             =   5820
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   556
      _Version        =   393216
      Format          =   72024065
      CurrentDate     =   37606
   End
   Begin MSComCtl2.DTPicker dtpDataTerminePrevendita 
      Height          =   315
      Left            =   6900
      TabIndex        =   12
      Top             =   6840
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   556
      _Version        =   393216
      Format          =   72024065
      CurrentDate     =   37606
   End
   Begin VB.Label lblDataOraTermineAnnullo 
      Caption         =   "Data/ora termine annullo"
      Height          =   255
      Left            =   8220
      TabIndex        =   48
      Top             =   4740
      Width           =   2175
   End
   Begin VB.Label lblDataOraInizio 
      Caption         =   "Data/ora inizio"
      Height          =   255
      Left            =   5760
      TabIndex        =   46
      Top             =   4740
      Width           =   2175
   End
   Begin VB.Label lblOraTerminePrevendita 
      Alignment       =   1  'Right Justify
      Caption         =   "ora (HH:MM)"
      Height          =   255
      Left            =   9360
      TabIndex        =   44
      Top             =   6900
      Width           =   975
   End
   Begin VB.Label lblSeparatoreOreMinutiTerminePrevendita 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10920
      TabIndex        =   43
      Top             =   6840
      Width           =   75
   End
   Begin VB.Label lblDataTerminePrevendita 
      Alignment       =   1  'Right Justify
      Caption         =   "data"
      Height          =   255
      Left            =   6420
      TabIndex        =   42
      Top             =   6900
      Width           =   375
   End
   Begin VB.Label lblTerminePrevendita 
      Alignment       =   1  'Right Justify
      Caption         =   "Fine Prevendita:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5040
      TabIndex        =   41
      Top             =   6900
      Width           =   1275
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   40
      Top             =   6240
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   39
      Top             =   5580
      Width           =   1695
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   38
      Top             =   4320
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   37
      Top             =   4320
      Width           =   1815
   End
   Begin VB.Label lblTermine 
      Alignment       =   1  'Right Justify
      Caption         =   "Fine:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5820
      TabIndex        =   35
      Top             =   6240
      Width           =   495
   End
   Begin VB.Label lblInizio 
      Alignment       =   1  'Right Justify
      Caption         =   "Inizio:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5820
      TabIndex        =   34
      Top             =   5880
      Width           =   495
   End
   Begin VB.Label lblDataInizio 
      Alignment       =   1  'Right Justify
      Caption         =   "data"
      Height          =   255
      Left            =   6420
      TabIndex        =   33
      Top             =   5880
      Width           =   375
   End
   Begin VB.Label lblSeparatoreOreMinutiInizio 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10920
      TabIndex        =   32
      Top             =   5820
      Width           =   75
   End
   Begin VB.Label lblOraInizio 
      Alignment       =   1  'Right Justify
      Caption         =   "ora (HH:MM)"
      Height          =   255
      Left            =   9360
      TabIndex        =   31
      Top             =   5880
      Width           =   975
   End
   Begin VB.Label lblDataTermine 
      Alignment       =   1  'Right Justify
      Caption         =   "data"
      Height          =   255
      Left            =   6420
      TabIndex        =   30
      Top             =   6240
      Width           =   375
   End
   Begin VB.Label lblSeparatoreOreMinutiTermine 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10920
      TabIndex        =   29
      Top             =   6180
      Width           =   75
   End
   Begin VB.Label lblOraTermine 
      Alignment       =   1  'Right Justify
      Caption         =   "ora (HH:MM)"
      Height          =   255
      Left            =   9360
      TabIndex        =   28
      Top             =   6240
      Width           =   975
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   26
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   25
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Periodi Commerciali"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoPeriodiCommerciali"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private isProdottoTMaster As ValoreBooleanoEnum
Private isProdottoTDL As ValoreBooleanoEnum

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private dataOraInizioRecordSelezionato As Date
Private dataOraFineRecordSelezionato As Date
Private dataOraFinePrevenditaRecordSelezionato As Date
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long

Private isRecordEditabile As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitaFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    lblNome.Caption = "Nome"
    lblDescrizione.Caption = "Descrizione"
    txtDataOraInizio.Enabled = False
    txtDataOraTermineAnnullo.Enabled = False
    
    dgrConfigurazioneProdottoPeriodiCommerciali.Caption = "PERIODI COMMERCIALI CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoPeriodiCommerciali.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtNome.Enabled = False
        lblNome.Enabled = False
        txtOraInizio.Text = ""
        txtOraInizio.Enabled = False
        lblOraInizio.Enabled = False
        txtMinutiInizio.Text = ""
        txtMinutiInizio.Enabled = False
        lblSeparatoreOreMinutiInizio.Enabled = False
        txtOraTermine.Text = ""
        txtOraTermine.Enabled = False
        lblOraTermine.Enabled = False
        txtMinutiTermine.Text = ""
        txtMinutiTermine.Enabled = False
        lblSeparatoreOreMinutiTermine.Enabled = False
        txtOraTerminePrevendita.Text = ""
        txtOraTerminePrevendita.Enabled = False
        lblOraTerminePrevendita.Enabled = False
        txtMinutiTerminePrevendita.Text = ""
        txtMinutiTerminePrevendita.Enabled = False
        lblSeparatoreOreMinutiTerminePrevendita.Enabled = False
        txtDescrizione.Text = ""
        txtDescrizione.Enabled = False
        lblDescrizione.Enabled = False
        dtpDataInizio.Enabled = False
        dtpDataTermine.Enabled = False
        dtpDataTerminePrevendita.Enabled = False
        lblDataInizio.Enabled = False
        lblDataTermine.Enabled = False
        lblDataTerminePrevendita.Enabled = False
        lblInizio.Enabled = False
        lblTermine.Enabled = False
        lblTerminePrevendita.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoPeriodiCommerciali.Enabled = False
        dtpDataInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        dtpDataTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        dtpDataTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDataInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDataTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDataTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtOraInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtMinutiInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtOraTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtMinutiTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtOraTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtMinutiTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOraInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSeparatoreOreMinutiInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOraTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSeparatoreOreMinutiTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOraTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSeparatoreOreMinutiTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblInizio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTermine.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTerminePrevendita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                        Len(txtOraInizio.Text) = 2 And Len(txtMinutiInizio.Text) = 2 And _
                        Len(txtMinutiInizio.Text) = 2 And Len(txtMinutiTermine.Text) = 2)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoPeriodiCommerciali.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtNome.Enabled = False
        lblNome.Enabled = False
        txtOraInizio.Text = ""
        txtOraInizio.Enabled = False
        lblOraInizio.Enabled = False
        txtMinutiInizio.Text = ""
        txtMinutiInizio.Enabled = False
        lblSeparatoreOreMinutiInizio.Enabled = False
        txtOraTermine.Text = ""
        txtOraTermine.Enabled = False
        lblOraTermine.Enabled = False
        txtMinutiTermine.Text = ""
        txtMinutiTermine.Enabled = False
        lblSeparatoreOreMinutiTermine.Enabled = False
        txtOraTerminePrevendita.Text = ""
        txtOraTerminePrevendita.Enabled = False
        lblOraTerminePrevendita.Enabled = False
        txtMinutiTerminePrevendita.Text = ""
        txtMinutiTerminePrevendita.Enabled = False
        lblSeparatoreOreMinutiTerminePrevendita.Enabled = False
        txtDescrizione.Text = ""
        txtDescrizione.Enabled = False
        lblDescrizione.Enabled = False
        dtpDataInizio.Enabled = False
        dtpDataTermine.Enabled = False
        dtpDataTerminePrevendita.Enabled = False
        lblDataInizio.Enabled = False
        lblDataTermine.Enabled = False
        lblDataTerminePrevendita.Enabled = False
        lblInizio.Enabled = False
        lblTermine.Enabled = False
        lblTerminePrevendita.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitaFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)) And _
                                    (IsConfigurataAlmenoUnPeriodoCommerciale)
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
    If isProdottoTMaster Or isProdottoTDL Then
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = False
    End If
    
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetIsProdottoMaster(b As ValoreBooleanoEnum)
    isProdottoTMaster = b
End Sub

Public Sub SetIsProdottoTDL(b As ValoreBooleanoEnum)
    isProdottoTDL = b
End Sub

Public Sub SetRateo(id As Long)
    rateo = id
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazionePrezzi
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
        
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call dtpDataInizio_Init
    Call dtpDataTermine_Init
    Call dtpDataTerminePrevendita_Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma_old()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato & _
        "; IDPERIODOCOMMERCIALE = " & idRecordSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If valoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoPeriodiCommerciali_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
                            Call dtpDataInizio_Init
                            Call dtpDataTermine_Init
                            Call dtpDataTerminePrevendita_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoPeriodiCommerciali_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
                            Call dtpDataInizio_Init
                            Call dtpDataTermine_Init
                            Call dtpDataTerminePrevendita_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoPeriodiCommerciali_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
                            Call dtpDataInizio_Init
                            Call dtpDataTermine_Init
                            Call dtpDataTerminePrevendita_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoPeriodiCommerciali_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
                            Call dtpDataInizio_Init
                            Call dtpDataTermine_Init
                            Call dtpDataTerminePrevendita_Init
                    End Select
                End If
                
                Call AggiornaAbilitazioneControlli
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciDaSelezione()
    If NumeroPeriodiCommercialiPerProdotto < 2 Then
        Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
        Call SetGestioneExitCode(EC_NON_SPECIFICATO)
        Call AggiornaAbilitazioneControlli
        Call GetIdRecordSelezionato
        Call CaricaDallaBaseDati
        Call AssegnaValoriCampi
        Call AggiornaAbilitazioneControlli
    Else
        Call frmMessaggio.Visualizza("NotificaNumeroMaxPeriodiCommerciali")
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitaFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    If NumeroPeriodiCommercialiPerProdotto < 2 Then
        isRecordEditabile = True
        Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
        Call SetGestioneExitCode(EC_NON_SPECIFICATO)
        Call AggiornaAbilitazioneControlli
        Call SetIdRecordSelezionato(idRecordSelezionato)
    Else
        Call frmMessaggio.Visualizza("NotificaNumeroMaxPeriodiCommerciali")
    End If
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoPeriodiCommerciali.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaFormConfigurazionePrezzi()
    Call frmConfigurazioneProdottoPrezzi.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoPrezzi.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoPrezzi.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetRateo(rateo)
    Call frmConfigurazioneProdottoPrezzi.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoPrezzi.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoPrezzi.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoPrezzi.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoPrezzi.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoPrezzi.Init
End Sub

Private Sub dgrConfigurazioneProdottoPeriodiCommerciali_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoPeriodiCommerciali_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
    Call ValorizzaCampiDate
    Call dtpDataInizio_Init
    Call dtpDataTermine_Init
    Call dtpDataTerminePrevendita_Init
    Call Me.Show(vbModal)

End Sub

Private Sub ValorizzaCampiDate()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim dataOraInizio As Date
    
    Call ApriConnessioneBD
    
    dataOraInizio = dataNulla
'    sql = "SELECT MIN(DATAORAINIZIO + DURATAINMINUTI/1440) SCADENZA"
'    sql = sql & " FROM RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE PR"
'    sql = sql & " WHERE R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
'    sql = sql & " AND PR.IDPRODOTTO = " & idProdottoSelezionato
    sql = "SELECT MIN(DATAORAINIZIO) INIZIO"
    sql = sql & " FROM RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE PR"
    sql = sql & " WHERE R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
    sql = sql & " AND PR.IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        If Not IsNull(rec("INIZIO")) Then
            dataOraInizio = rec("INIZIO").Value
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    txtDataOraInizio.Text = IIf(dataOraInizio = dataNulla, "", CStr(dataOraInizio))
    txtDataOraTermineAnnullo.Text = IIf(dataOraInizio = dataNulla, "", CStr(DataOraTermineAnnullo(dataOraInizio)))
End Sub

Private Function DataOraTermineAnnullo(dataScadenza As Date) As Date
    Dim objSP As New clsSETA
    Dim ORADB As OraDatabase
    Dim ORASession As New OraSessionClass
    Dim opt As Long
    Dim dataOraTermine As Date
    
    DataOraTermineAnnullo = dataNulla
    
    Set ORADB = ORASession.OpenDatabase(NomeTNS, userID & "/" & passWord, opt)

    objSP.database = ORADB
    Call objSP.CALCOLADATAORATERMINEANNULLO(idOrganizzazioneSelezionata, dataScadenza, dataOraTermine)
    
    ORADB.Close
    
    DataOraTermineAnnullo = dataOraTermine
End Function

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoPeriodiCommerciali.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoPeriodiCommerciali_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
        
    Set d = adcConfigurazioneProdottoPeriodiCommerciali
    
    sql = "SELECT DISTINCT IDPERIODOCOMMERCIALE ID," & _
        " NOME ""Nome""," & _
        " DESCRIZIONE ""Descrizione""," & _
        " DATAORAINIZIO ""Data/Ora Inizio""," & _
        " DATAORAFINE ""Data/Ora Fine""," & _
        " DATAORAFINEPREVENDITA ""Data/Ora Fine Prevendita""" & _
        " FROM PERIODOCOMMERCIALE" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneProdottoPeriodiCommerciali.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoPeriodoCommerciale As Long
    Dim i As Integer
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    idNuovoPeriodoCommerciale = OttieniIdentificatoreDaSequenza("SQ_PERIODOCOMMERCIALE")
    sql = "INSERT INTO PERIODOCOMMERCIALE (IDPERIODOCOMMERCIALE, NOME," & _
        " DESCRIZIONE, DATAORAINIZIO, DATAORAFINE, DATAORAFINEPREVENDITA, IDPRODOTTO)" & _
        " VALUES (" & _
        idNuovoPeriodoCommerciale & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        SqlDateTimeValue(dataOraInizioRecordSelezionato) & ", " & _
        SqlDateTimeValue(dataOraFineRecordSelezionato) & ", " & _
        SqlDateTimeValue(dataOraFinePrevenditaRecordSelezionato) & ", " & _
        idProdottoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("PERIODOCOMMERCIALE", "IDPERIODOCOMMERCIALE", idNuovoPeriodoCommerciale, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoPeriodoCommerciale)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT NOME, DESCRIZIONE, DATAORAINIZIO," & _
'            " DATAORAFINE, DATAORAFINEPREVENDITA," & _
'            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
'            " FROM PERIODOCOMMERCIALE" & _
'            " WHERE IDPERIODOCOMMERCIALE = " & idRecordSelezionato
'    Else
        sql = "SELECT NOME, DESCRIZIONE, DATAORAINIZIO," & _
            " DATAORAFINE, DATAORAFINEPREVENDITA" & _
            " FROM PERIODOCOMMERCIALE" & _
            " WHERE IDPERIODOCOMMERCIALE = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        dataOraInizioRecordSelezionato = rec("DATAORAINIZIO")
        dataOraFineRecordSelezionato = rec("DATAORAFINE")
        dataOraFinePrevenditaRecordSelezionato = rec("DATAORAFINEPREVENDITA")
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = ""
    txtDescrizione.Text = descrizioneRecordSelezionato
    
    dtpDataInizio.Value = dataOraInizioRecordSelezionato
    txtOraInizio.Text = ""
    txtOraInizio.Text = StringaOraMinuti(CStr(Hour(dataOraInizioRecordSelezionato)))
    txtMinutiInizio.Text = ""
    txtMinutiInizio.Text = StringaOraMinuti(CStr(Minute(dataOraInizioRecordSelezionato)))
    
    dtpDataTermine.Value = dataOraFineRecordSelezionato
    txtOraTermine.Text = ""
    txtOraTermine.Text = StringaOraMinuti(CStr(Hour(dataOraFineRecordSelezionato)))
    txtMinutiTermine.Text = ""
    txtMinutiTermine.Text = StringaOraMinuti(CStr(Minute(dataOraFineRecordSelezionato)))
    
    dtpDataTerminePrevendita.Value = dataOraFinePrevenditaRecordSelezionato
    txtOraTerminePrevendita.Text = ""
    txtOraTerminePrevendita.Text = StringaOraMinuti(CStr(Hour(dataOraFinePrevenditaRecordSelezionato)))
    txtMinutiTerminePrevendita.Text = ""
    txtMinutiTerminePrevendita.Text = StringaOraMinuti(CStr(Minute(dataOraFinePrevenditaRecordSelezionato)))
        
    internalEvent = internalEventOld

End Sub

Private Sub dgrConfigurazioneProdottoPeriodiCommerciali_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoPeriodiCommerciali
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Function valoriCampiOK() As Boolean
    Dim dataInizio As Date
    Dim oraInizio As Integer
    Dim minutiInizio As Integer
    Dim dataTermine As Date
    Dim dataTerminePrevendita As Date
    Dim oraTermine As Integer
    Dim minutiTermine As Integer
    Dim oraTerminePrevendita As Integer
    Dim minutiTerminePrevendita As Integer
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    
    Set listaNonConformitā = New Collection
    condizioneSql = ""
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDPERIODOCOMMERCIALE <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("PERIODOCOMMERCIALE", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDPRODOTTO = " & idProdottoSelezionato, _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB per lo stesso Prodotto;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    dataInizio = FormatDateTime(dtpDataInizio.Value, vbShortDate)
    If IsCampoOraCorretto(txtOraInizio) Then
        oraInizio = CInt(Trim(txtOraInizio.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoOra", "Inizio ore")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Inizio ore deve essere numerico di tipo intero e compreso tra 0 e 23;")
    End If
    If IsCampoMinutiCorretto(txtMinutiInizio) Then
        minutiInizio = CInt(Trim(txtMinutiInizio.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Inizio minuti")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Inizio minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    dataOraInizioRecordSelezionato = FormatDateTime(dataInizio & " " & oraInizio & ":" & minutiInizio, vbGeneralDate)
    
    dataTermine = FormatDateTime(dtpDataTermine.Value, vbShortDate)
    If IsCampoOraCorretto(txtOraTermine) Then
        oraTermine = CInt(Trim(txtOraTermine.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoOra", "Termine ore")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Termine ore deve essere numerico di tipo intero e compreso tra 0 e 23;")
    End If
    If IsCampoMinutiCorretto(txtMinutiTermine) Then
        minutiTermine = CInt(Trim(txtMinutiTermine.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Termine minuti")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Termine minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    dataOraFineRecordSelezionato = FormatDateTime(dataTermine & " " & oraTermine & ":" & minutiTermine, vbGeneralDate)
    
    dataTerminePrevendita = FormatDateTime(dtpDataTerminePrevendita.Value, vbShortDate)
    If IsCampoOraCorretto(txtOraTerminePrevendita) Then
        oraTerminePrevendita = CInt(Trim(txtOraTerminePrevendita.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoOra", "Termine prevendita ore")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Termine prevendita ore deve essere numerico di tipo intero e compreso tra 0 e 23;")
    End If
    If IsCampoMinutiCorretto(txtMinutiTerminePrevendita) Then
        minutiTerminePrevendita = CInt(Trim(txtMinutiTerminePrevendita.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Termine prevendita minuti")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Termine prevendita minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    dataOraFinePrevenditaRecordSelezionato = FormatDateTime(dataTerminePrevendita & " " & oraTerminePrevendita & ":" & minutiTerminePrevendita, vbGeneralDate)
    
    If dataOraInizioRecordSelezionato > dataOraFineRecordSelezionato Then
'        Call frmMessaggio.Visualizza("ErroreSuccessioneDate", "Inizio", "Fine")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- la data Inizio deve essere precedente alla data Fine;")
    End If
    
    If (dataOraFinePrevenditaRecordSelezionato < dataOraInizioRecordSelezionato) Or _
       (dataOraFinePrevenditaRecordSelezionato > dataOraFineRecordSelezionato) Then
'        Call frmMessaggio.Visualizza("ErroreIntervalloDate", "Fine Prevendita", "Inizio", "Fine")
        valoriCampiOK = False
        Call listaNonConformitā.Add("- La data Fine prevendita deve essere compresa tra la data Inizio e la data Fine, ed eventualmente coincidere con una di esse;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitaFormCorrente = mf
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoTariffe.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoTariffe.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE PERIODOCOMMERCIALE SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " DATAORAINIZIO = " & SqlDateTimeValue(dataOraInizioRecordSelezionato) & "," & _
        " DATAORAFINE = " & SqlDateTimeValue(dataOraFineRecordSelezionato) & "," & _
        " DATAORAFINEPREVENDITA = " & SqlDateTimeValue(dataOraFinePrevenditaRecordSelezionato) & _
        " WHERE IDPERIODOCOMMERCIALE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("PERIODOCOMMERCIALE", "IDPERIODOCOMMERCIALE", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Function NumeroPeriodiCommercialiPerProdotto() As Integer
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Integer

    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(*) FROM PERIODOCOMMERCIALE WHERE IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    n = rec("COUNT(*)")
    rec.Close
    
    Call ChiudiConnessioneBD
    
    NumeroPeriodiCommercialiPerProdotto = n
    
End Function

Private Function IsConfigurataAlmenoUnPeriodoCommerciale() As Boolean
    Dim n As Integer
    
    n = adcConfigurazioneProdottoPeriodiCommerciali.Recordset.RecordCount
    If n = 0 Then
        IsConfigurataAlmenoUnPeriodoCommerciale = False
    Else
        IsConfigurataAlmenoUnPeriodoCommerciale = True
    End If
End Function

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub dtpDataInizio_Init()
    dtpDataInizio.Value = Now
End Sub

Private Sub dtpDataTermine_Init()
    dtpDataTermine.Value = Now
End Sub

Private Sub dtpDataTerminePrevendita_Init()
    dtpDataTerminePrevendita.Value = Now
End Sub

Private Sub txtMinutiTerminePrevendita_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraInizio_Change()
    If Not internalEvent Then
        If Len(txtOraInizio.Text) = 2 Then
            txtMinutiInizio.SetFocus
        End If
    End If
End Sub

Private Sub txtMinutiInizio_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraTermine_Change()
    If Not internalEvent Then
        If Len(txtOraTermine.Text) = 2 Then
            txtMinutiTermine.SetFocus
        End If
    End If
End Sub

Private Sub txtMinutiTermine_Change()
    Call AggiornaAbilitazioneControlli
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim periodoEliminabile As Boolean
    Dim n As Long

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    
    periodoEliminabile = True
    If Not IsRecordEliminabile("IDPERIODOCOMMERCIALE", "PREZZOTITOLOPRODOTTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PREZZOTITOLOPRODOTTO")
        periodoEliminabile = False
    End If
    
    If periodoEliminabile Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                sql = "DELETE FROM PERIODOCOMMERCIALE" & _
'                    " WHERE IDPERIODOCOMMERCIALE = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'            Else
'                Call AggiornaParametriSessioneSuRecord("PERIODOCOMMERCIALE", "IDPERIODOCOMMERCIALE", idRecordSelezionato, TSR_ELIMINATO)
'            End If
'        Else
            sql = "DELETE FROM PERIODOCOMMERCIALE" & _
                " WHERE IDPERIODOCOMMERCIALE = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il Periodo commerciale", "PREZZOTITOLOPRODOTTO")
    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub txtOraTerminePrevendita_Change()
    If Not internalEvent Then
        If Len(txtOraTerminePrevendita.Text) = 2 Then
            txtMinutiTerminePrevendita.SetFocus
        End If
    End If
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato & _
        "; IDPERIODOCOMMERCIALE = " & idRecordSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If isRecordEditabile Then
                If gestioneRecordGriglia = ASG_ELIMINA Then
                    Call EliminaDallaBaseDati
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazioneProdottoPeriodiCommerciali_Init
                    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                    Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
                    Call dtpDataInizio_Init
                    Call dtpDataTermine_Init
                    Call dtpDataTerminePrevendita_Init
                Else
                    If valoriCampiOK Then
                        If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                        ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_PERIODO_COMMERCIALE, stringaNota, idProdottoSelezionato)
                        End If
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazioneProdottoPeriodiCommerciali_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazioneProdottoPeriodiCommerciali_Init
                        Call dtpDataInizio_Init
                        Call dtpDataTermine_Init
                        Call dtpDataTerminePrevendita_Init
                    End If
                End If
            End If
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub



