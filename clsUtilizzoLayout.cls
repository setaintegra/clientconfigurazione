VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsUsoLayout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public collAree As New Collection
'
'Public Sub Inizializza_old(idProd As Long)
'    Dim sql1 As String
'    Dim rec1 As New ADODB.Recordset
'    Dim sql2 As String
'    Dim rec2 As New ADODB.Recordset
'    Dim sql3 As String
'    Dim rec3 As New ADODB.Recordset
'    Dim area As clsAssocArea
'    Dim tariffa As clsAssocTariffa
'    Dim tipoSupporto As clsAssocSupporto
'
'    Set collAree = New Collection
'    sql1 = "SELECT DISTINCT A.IDAREA, A.NOME NOMEAREA, A.CODICE CODAREA" & '        " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T, AREA A WHERE" & '        " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & '        " (ULS.IDAREA = A.IDAREA) AND" & '        " (T.IDPRODOTTO = " & idProd & ")"
'    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec1.EOF And rec1.BOF) Then
'        rec1.MoveFirst
'        While Not rec1.EOF
'            Set area = New clsAssocArea
'            area.idArea = rec1("IDAREA")
'            area.codiceArea = rec1("CODAREA")
'            area.nomeArea = rec1("NOMEAREA")
''            Set collAree = New Collection
'            Set area.collTariffe = New Collection
'            sql2 = "SELECT DISTINCT T.IDTARIFFA, T.NOME NOMETARIFFA, T.CODICE CODTARIFFA" & '                " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T WHERE" & '                " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & '                " (T.IDPRODOTTO = " & idProd & ") AND" & '                " (ULS.IDAREA = " & area.idArea & ")"
'            rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
'            If Not (rec2.BOF And rec2.EOF) Then
'                rec2.MoveFirst
'                While Not rec2.EOF
'                    Set tariffa = New clsAssocTariffa
'                    tariffa.idTariffa = rec2("IDTARIFFA")
'                    tariffa.nomeTariffa = rec2("NOMETARIFFA")
'                    tariffa.codiceTariffa = rec2("CODTARIFFA")
'                    tariffa.labelTariffa = tariffa.codiceTariffa & " - " & tariffa.nomeTariffa
''                    Set area.collTariffe = New Collection
'                    Set tariffa.collTipiSupporto = New Collection
'                    sql3 = "SELECT ULS.IDTIPOSUPPORTO IDTS, ULS.IDLAYOUTSUPPORTO IDLS," & '                        " TS.NOME NOMETS, TS.CODICE CODTS," & '                        " LS.NOME NOMELS, LS.CODICE CODLS" & '                        " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T, TIPOSUPPORTO TS, LAYOUTSUPPORTO LS WHERE" & '                        " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & '                        " (ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO) AND" & '                        " (ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO) AND" & '                        " (T.IDPRODOTTO = " & idProd & ") AND" & '                        " (ULS.IDAREA = " & area.idArea & ") AND" & '                        " (ULS.IDTARIFFA = " & tariffa.idTariffa & ")"
'                    rec3.Open sql3, SETAConnection, adOpenDynamic, adLockOptimistic
'                    If Not (rec3.BOF And rec3.EOF) Then
'                        rec3.MoveFirst
'                        While Not rec3.EOF
'                            Set tipoSupporto = New clsAssocSupporto
'                            tipoSupporto.idTipoSupporto = rec3("IDTS")
'                            tipoSupporto.idLayoutSupporto = rec3("IDLS")
'                            tipoSupporto.codiceTipoSupporto = rec3("CODTS")
'                            tipoSupporto.codiceLayoutSupporto = rec3("CODLS")
'                            tipoSupporto.nomeTipoSupporto = rec3("NOMETS")
'                            tipoSupporto.nomeLayoutSupporto = rec3("NOMELS")
'                            tipoSupporto.labelTipoSupporto = tipoSupporto.codiceTipoSupporto & " - " & tipoSupporto.nomeTipoSupporto
'                            tipoSupporto.labelLayoutSupporto = tipoSupporto.codiceLayoutSupporto & " - " & tipoSupporto.nomeLayoutSupporto
'                            Call tariffa.collTipiSupporto.Add(tipoSupporto)
'                            rec3.MoveNext
'                        Wend
'                    End If
'                    Call area.collTariffe.Add(tariffa)
'                    rec3.Close
'                    rec2.MoveNext
'                Wend
'            End If
'            Call collAree.Add(area)
'            rec2.Close
'            rec1.MoveNext
'        Wend
'    End If
'    rec1.Close
'
'End Sub

Public Function ottieniAreaDaIdArea_old(idA As Long) As clsAssocArea
    Dim a As clsAssocArea
    Dim iArea As Integer
    Dim trovato As Boolean
    
'    For Each a In collAree
'        If a.idArea = idA Then
'            Set OttieniAreaDaIdArea = a
'            ' come si esce dai foreach?
'        End If
'    Next a
    
    iArea = 1
    trovato = False
    While iArea < collAree.count + 1 And Not trovato
        Set a = collAree(iArea)
        If idA = a.idArea Then
            trovato = True
        End If
        iArea = iArea + 1
    Wend
    ottieniAreaDaIdArea_old = a
End Function

Public Function ottieniAreaDaIdArea(idA As Long, ByRef newArea As clsAssocArea) As Boolean
    Dim a As clsAssocArea
    Dim iArea As Integer
    Dim trovato As Boolean
    
    iArea = 1
    trovato = False
    While iArea < collAree.count + 1 And Not trovato
        Set a = collAree(iArea)
        If idA = a.idArea Then
            trovato = True
            Set newArea = a
        End If
        iArea = iArea + 1
    Wend
    If Not trovato Then
        Set newArea = New clsAssocArea
    End If
    ottieniAreaDaIdArea = trovato
End Function

Public Function IsAreaUtilizzata(idA As Long) As Boolean
    Dim a As clsAssocArea
    Dim iArea As Integer
    Dim trovato As Boolean
    
    iArea = 1
    trovato = False
    While iArea < collAree.count + 1 And Not trovato
        Set a = collAree(iArea)
        If idA = a.idArea Then
            trovato = True
        End If
        iArea = iArea + 1
    Wend
    IsAreaUtilizzata = trovato
End Function

Public Sub EliminaAreaDaUtilizzoLayout(idA As Long)
    Dim a As clsAssocArea
    Dim i As Integer
    Dim index As Integer
    
    i = 1
    For Each a In collAree
        If a.idArea = idA Then
            index = i
            ' come si esce dai foreach?
        End If
        i = i + 1
    Next a
    Call collAree.Remove(index)
End Sub

Public Sub AggiungiAreaAdUtilizzoLayout(area As clsAssocArea)
    Call collAree.Add(area)
End Sub

Public Sub inizializza(idProd As Long)
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim sql2 As String
    Dim rec2 As New ADODB.Recordset
    Dim area As clsAssocArea
    Dim tariffa As clsAssocTariffa
    Dim tipoSupporto As clsAssocSupporto
    Dim tipoTerm As clsAssocTipoTerm
    Dim condizioniSQL As String
    
    Set collAree = New Collection
    condizioniSQL = " AND (ULS.IDTIPOSTATORECORD IS NULL" & _
        " OR ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
    sql1 = "SELECT DISTINCT A.IDAREA, A.NOME NOMEAREA, A.CODICE CODAREA" & _
        " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, AREA A" & _
        " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
        " AND ULS.IDAREA = A.IDAREA" & _
        " AND T.IDPRODOTTO = " & idProd
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql1 = sql1 & condizioniSQL
'    End If
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.EOF And rec1.BOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            Set area = New clsAssocArea
            area.idArea = rec1("IDAREA")
            area.codiceArea = rec1("CODAREA")
            area.nomeArea = rec1("NOMEAREA")
            Set area.collTariffe = New Collection
            Set area.collTipoTerm = New Collection
            Set area.collTipiSupporto = New Collection
            'tariffa
'            sql2 = "SELECT DISTINCT T.IDTARIFFA, T.NOME NOMETARIFFA," & _
'                " T.CODICE CODTARIFFA, T.IDTIPOTARIFFALOTTO TTLOTTO" & _
'                " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T WHERE" & _
'                " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & _
'                " (T.IDPRODOTTO = " & idProd & ") AND" & _
'                " (ULS.IDAREA = " & area.idArea & ")"
            sql2 = "SELECT DISTINCT T.IDTARIFFA, T.NOME NOMETARIFFA," & _
                " T.CODICE CODTARIFFA" & _
                " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T" & _
                " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
                " AND T.IDPRODOTTO = " & idProd & _
                " AND ULS.IDAREA = " & area.idArea
'            If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'                sql2 = sql2 & condizioniSQL
'            End If
            rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec2.BOF And rec2.EOF) Then
                rec2.MoveFirst
                While Not rec2.EOF
                    Set tariffa = New clsAssocTariffa
                    tariffa.idTariffa = rec2("IDTARIFFA")
'                    tariffa.utilizzabileDaTL = IIf(IsNull(rec2("TTLOTTO")), False, True)
'                    tariffa.nomeTariffa = rec2("NOMETARIFFA")
'                    tariffa.codiceTariffa = rec2("CODTARIFFA")
'                    tariffa.labelTariffa = tariffa.codiceTariffa & " - " & tariffa.nomeTariffa
                    tariffa.labelTariffa = CStr(rec2("CODTARIFFA")) & " - " & CStr(rec2("NOMETARIFFA"))
                    Call area.collTariffe.Add(tariffa)
                    rec2.MoveNext
                Wend
            End If
            rec2.Close
            If area.collTariffe.count <> 0 Then
'                'tipo terminale
'                sql2 = "SELECT DISTINCT ULS.IDTIPOTERMINALE IDTS, TT.NOME NOMETT" & _
'                    " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T, TIPOTERMINALE TT" & _
'                    " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
'                    " AND ULS.IDTIPOTERMINALE = TT.IDTIPOTERMINALE" & _
'                    " AND T.IDPRODOTTO = " & idProd & _
'                    " AND ULS.IDAREA = " & area.idArea & _
'                    " AND ULS.IDTARIFFA = " & tariffa.idTariffa
''                If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
''                    sql2 = sql2 & condizioniSQL
''                End If
'                rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
'                If Not (rec2.BOF And rec2.EOF) Then
'                    rec2.MoveFirst
'                    While Not rec2.EOF
'                        Set tipoTerm = New clsAssocTipoTerm
'                        tipoTerm.idTipoTerm = rec2("IDTS")
'                        tipoTerm.labelTipoTerm = rec2("NOMETT")
'                        Call area.collTipoTerm.Add(tipoTerm)
'                        rec2.MoveNext
'                    Wend
'                End If
'                rec2.Close
                'tipo supporto e layout supporto
                sql2 = "SELECT ULS.IDTIPOSUPPORTO IDTS, ULS.IDLAYOUTSUPPORTO IDLS," & _
                    " TS.NOME NOMETS, TS.CODICE CODTS," & _
                    " LS.NOME NOMELS, LS.CODICE CODLS" & _
                    " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, TIPOSUPPORTO TS, LAYOUTSUPPORTO LS" & _
                    " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
                    " AND ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO" & _
                    " AND ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
                    " AND T.IDPRODOTTO = " & idProd & _
                    " AND ULS.IDAREA = " & area.idArea & _
                    " AND ULS.IDTARIFFA = " & tariffa.idTariffa
'                If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'                    sql2 = sql2 & condizioniSQL
'                End If
                rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
                If Not (rec2.BOF And rec2.EOF) Then
                    rec2.MoveFirst
                    While Not rec2.EOF
                        Set tipoSupporto = New clsAssocSupporto
                        tipoSupporto.idTipoSupporto = rec2("IDTS")
                        tipoSupporto.idLayoutSupporto = rec2("IDLS")
    '                    tipoSupporto.codiceTipoSupporto = rec2("CODTS")
    '                    tipoSupporto.codiceLayoutSupporto = rec2("CODLS")
    '                    tipoSupporto.nomeTipoSupporto = rec2("NOMETS")
    '                    tipoSupporto.nomeLayoutSupporto = rec2("NOMELS")
    '                    tipoSupporto.labelTipoSupporto = tipoSupporto.codiceTipoSupporto & " - " & tipoSupporto.nomeTipoSupporto
    '                    tipoSupporto.labelLayoutSupporto = tipoSupporto.codiceLayoutSupporto & " - " & tipoSupporto.nomeLayoutSupporto
                        tipoSupporto.labelTipoSupporto = CStr(rec2("CODTS")) & " - " & CStr(rec2("NOMETS"))
                        tipoSupporto.labelLayoutSupporto = CStr(rec2("CODLS")) & " - " & CStr(rec2("NOMELS"))
                        Call area.collTipiSupporto.Add(tipoSupporto)
                        rec2.MoveNext
                    Wend
                End If
                rec2.Close
            End If
            Call collAree.Add(area)
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
End Sub
'
'Public Function ClonaArea(a As clsAssocArea) As clsAssocArea
'    Dim b As clsAssocArea
'
'    Set b = New clsAssocArea
'
'    b.idArea = a.idArea
'    b.codiceArea = a.codiceArea
'    b.nomeArea = a.nomeArea
'    b.labelArea = a.labelArea
'    Set b.collTariffe = a.collTariffe
'    Set b.collTipoTerm = a.collTipoTerm
'    Set b.collTipiSupporto = a.collTipiSupporto
'
'End Function


