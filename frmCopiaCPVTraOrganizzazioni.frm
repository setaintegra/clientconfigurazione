VERSION 5.00
Begin VB.Form frmCopiaCPVTraOrganizzazioni 
   Caption         =   "Copia CPV tra organizzazioni"
   ClientHeight    =   4770
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10170
   LinkTopic       =   "Form1"
   ScaleHeight     =   4770
   ScaleWidth      =   10170
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   495
      Left            =   3960
      TabIndex        =   8
      Top             =   4080
      Width           =   2175
   End
   Begin VB.CommandButton cmdEsegui 
      Caption         =   "Esegui"
      Height          =   495
      Left            =   3960
      TabIndex        =   6
      Top             =   2760
      Width           =   2175
   End
   Begin VB.ComboBox cmbOrganizzazioneSlave 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   8025
   End
   Begin VB.ComboBox cmbOrganizzazioneMaster 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   120
      Width           =   8025
   End
   Begin VB.Label lblAvanzamento 
      Height          =   195
      Left            =   240
      TabIndex        =   7
      Top             =   3480
      Width           =   8415
   End
   Begin VB.Label Label3 
      Caption         =   "Eventuali configurazioni esistenti verranno sovrascritte."
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   1560
      Width           =   8535
   End
   Begin VB.Label Label2 
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   9855
   End
   Begin VB.Label Label1 
      Caption         =   "Organizzazione slave"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   1695
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione master"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frmCopiaCPVTraOrganizzazioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private idOrganizzazioneMaster As Long
Private idOrganizzazioneSlave As Long

Private internalEvent As Boolean

Public Sub Init()
    Call CaricaComboOrganizzazioneMaster
    Call Me.Show
End Sub

Private Sub CaricaComboOrganizzazioneMaster()
    Dim sqlOrg As String
    
    Call cmbOrganizzazioneMaster.Clear
    sqlOrg = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE ORDER BY NOME"
    Call CaricaValoriCombo3(cmbOrganizzazioneMaster, sqlOrg, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbOrganizzazioneMaster_Click()
    idOrganizzazioneMaster = cmbOrganizzazioneMaster.ItemData(cmbOrganizzazioneMaster.ListIndex)
    Call CaricaComboOrganizzazioneSlave
End Sub

Private Sub CaricaComboOrganizzazioneSlave()
    Dim sqlOrg As String
    
    Call cmbOrganizzazioneSlave.Clear
    sqlOrg = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " WHERE IDORGANIZZAZIONE <> " & idOrganizzazioneMaster & _
        " ORDER BY NOME"
    Call CaricaValoriCombo3(cmbOrganizzazioneSlave, sqlOrg, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbOrganizzazioneSlave_Click()
    idOrganizzazioneSlave = cmbOrganizzazioneSlave.ItemData(cmbOrganizzazioneSlave.ListIndex)
End Sub

Private Sub cmdChiudi_Click()
    Unload Me
End Sub

Private Sub cmdEsci_Click()
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub
Private Sub cmdEsegui_Click()
    Dim sql As String
    Dim rec As OraDynaset
    Dim sqlO As String
    Dim sqlCS As String
    Dim recCS As OraDynaset
    Dim sqlPV As String
    Dim n As Long
    Dim i As Long
    Dim idProdotto As Long
    Dim idPianta As Long
    Dim idClasseSuperarea As Long
    Dim idClasseSuperareaProdotto As Long

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    
On Error GoTo frmCopiaCPVTraOrganizzazioni_Esegui

    sqlO = "DELETE FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSlave & _
        " AND IDCLASSEPUNTOVENDITA <> 1"
    n = ORADB.ExecuteSQL(sqlO)

    sqlO = "INSERT INTO ORGANIZ_CLASSEPV_PUNTOVENDITA (IDORGANIZZAZIONE, IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA)" & _
        " SELECT " & idOrganizzazioneSlave & ", IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA" & _
        " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneMaster & _
        " AND IDCLASSEPUNTOVENDITA <> 1"
    n = ORADB.ExecuteSQL(sqlO)

    Call ORADB.CommitTrans


' Per i prodotti non si fa nulla; questo codice commentato non e' verificato!
'    i = 0
'    sql = "SELECT DISTINCT P.IDPRODOTTO, P.NOME, P.IDPIANTA" & _
'        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
'        " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSlave & _
'        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
'        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
'    Set rec = ORADB.CreateDynaset(sql, 0&)
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'
'            idProdotto = rec("IDPRODOTTO")
'            idPianta = rec("IDPIANTA")
'
'            idClasseSuperarea = idNessunElementoSelezionato
'            sqlCS = "SELECT IDCLASSESUPERAREA" & _
'                " FROM CLASSESUPERAREA" & _
'                " WHERE IDPIANTA = " & rec("IDPIANTA") & _
'                " AND CLASSEPRINCIPALE = 1"
'            Set recCS = ORADB.CreateDynaset(sqlCS, 0&)
'            If Not (recCS.BOF And recCS.EOF) Then
'                recCS.MoveFirst
'                While Not recCS.EOF
'                    idClasseSuperarea = rec("IDCLASSESUPERAREA")
'                Wend
'            End If
'            recCS.Close
'
'            If idClasseSuperarea = idNessunElementoSelezionato Then
'                idClasseSuperarea = OttieniIdentificatoreDaSequenza("SQ_CLASSESUPERAREA")
'
'                sqlCS = "INSERT INTO CLASSESUPERAREA (IDCLASSESUPERAREA, NOME, DESCRIZIONE, IDPIANTA, CLASSEPRINCIPALE)" & _
'                    " VALUES (" & idClasseSuperarea & ", 'SETTORI ORDINARI', 'SETTORI ORDINARI', " & idPianta & ", 1)"
'                n = ORADB.ExecuteSQL(sql)
'            End If
'
'            sqlCS = "DELETE FROM CLASSESUPERAREAPRODOTTO WHERE IDCLASSESUPERAREA = " & idClasseSuperarea
'            n = ORADB.ExecuteSQL(sql)
'
'            idClasseSuperareaProdotto = OttieniIdentificatoreDaSequenza("SQ_CLASSESUPERAREAPRODOTTO")
'            sqlCS = "INSERT INTO CLASSESUPERAREAPRODOTTO (IDCLASSESUPERAREAPRODOTTO, IDCLASSESUPERAREA, IDPRODOTTO)" & _
'                " VALUES (" & idClasseSuperareaProdotto & ", " & idClasseSuperarea & ", " & idProdotto & ")"
'            n = ORADB.ExecuteSQL(sql)
'
'            sqlCS = "INSERT INTO CLASSESUPERAREAPROD_SUPERAREA (IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA)" & _
'                " SELECT " & idClasseSuperareaProdotto & ", IDAREA" & _
'                " FROM AREA" & _
'                " WHERE IDPIANTA = " & idPianta & _
'                " AND IDTIPOAREA IN (5, 6)"
'            n = ORADB.ExecuteSQL(sql)
'
'            sqlCS = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
'                " SELECT " & idClasseSuperareaProdotto & ", IDPUNTOVENDITA" & _
'                " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
'                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneMaster
'            n = ORADB.ExecuteSQL(sql)
'
'            i = i + 1
'            lblAvanzamento.Caption = "Prodotto: " & rec("NOME")
'            DoEvents
'            DoEvents
'
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
'    lblAvanzamento.Caption = "Conclusi " & i & " prodotti."
'    DoEvents
'    DoEvents
    
    Exit Sub
    
frmCopiaCPVTraOrganizzazioni_Esegui:
    ORADB.Rollback
    
End Sub
