VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDettagliDateOperazioneSingola 
   Caption         =   "Dettagli date operazione"
   ClientHeight    =   4830
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8220
   LinkTopic       =   "Form1"
   ScaleHeight     =   4830
   ScaleWidth      =   8220
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkEstendiASecondarie 
      Caption         =   "Estendi configurazione anche sulle classi superaree secondarie"
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   2400
      Width           =   7935
   End
   Begin VB.Frame frmPeriodo 
      Caption         =   "Classe superarea"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   7935
      Begin VB.TextBox txtMinutiFine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7320
         MaxLength       =   2
         TabIndex        =   18
         Top             =   1200
         Width           =   435
      End
      Begin VB.TextBox txtOraFine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6660
         MaxLength       =   2
         TabIndex        =   17
         Top             =   1200
         Width           =   435
      End
      Begin VB.TextBox txtMinutiInizio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7320
         MaxLength       =   2
         TabIndex        =   15
         Top             =   720
         Width           =   435
      End
      Begin VB.TextBox txtOraInizio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6660
         MaxLength       =   2
         TabIndex        =   14
         Top             =   720
         Width           =   435
      End
      Begin VB.OptionButton optPeriodoDefinito 
         Caption         =   "Periodo"
         Height          =   255
         Left            =   3960
         TabIndex        =   7
         Top             =   360
         Width           =   1815
      End
      Begin VB.OptionButton optPeriodoIllimitato 
         Caption         =   "Illimitato"
         Height          =   255
         Left            =   2160
         TabIndex        =   6
         Top             =   360
         Width           =   1455
      End
      Begin VB.OptionButton optPeriodoNonDefinito 
         Caption         =   "Non applicabile"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1455
      End
      Begin MSComCtl2.DTPicker dtpDataInizio 
         Height          =   315
         Left            =   4560
         TabIndex        =   13
         Top             =   720
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   67043329
         CurrentDate     =   37607
      End
      Begin MSComCtl2.DTPicker dtpDataFine 
         Height          =   315
         Left            =   4560
         TabIndex        =   16
         Top             =   1200
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   67043329
         CurrentDate     =   37607
      End
      Begin VB.Label lblSeparatoreOreMinutiFine 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         TabIndex        =   11
         Top             =   1200
         Width           =   75
      End
      Begin VB.Label lblSeparatoreOreMinutiInizio 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         TabIndex        =   10
         Top             =   720
         Width           =   75
      End
      Begin VB.Label lblInizio 
         Alignment       =   1  'Right Justify
         Caption         =   "Inizio:"
         Height          =   255
         Left            =   3720
         TabIndex        =   9
         Top             =   780
         Width           =   735
      End
      Begin VB.Label lblFine 
         Alignment       =   1  'Right Justify
         Caption         =   "Fine:"
         Height          =   255
         Left            =   3780
         TabIndex        =   8
         Top             =   1260
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   435
      Left            =   2880
      TabIndex        =   19
      Top             =   4200
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   435
      Left            =   4320
      TabIndex        =   0
      Top             =   4200
      Width           =   1035
   End
   Begin VB.Label Label3 
      Caption         =   "Questa modifica viene riportata anche sul catalogo WEB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   3600
      Width           =   7935
   End
   Begin VB.Label Label2 
      Caption         =   "Questa modifica sarą operativa per le sessioni prodotto avviate dopo la conferma"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   435
      Left            =   120
      TabIndex        =   2
      Top             =   3120
      Width           =   7815
   End
   Begin VB.Label lblIntestazione 
      Caption         =   "Tipo operazione:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   315
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmDettagliDateOperazioneSingola"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private dataOraInizio As Date
Private dataOraFine As Date
Private dataOraInizioSupGestSep As Date
Private dataOraFineSupGestSep As Date

Private azione As AzioneDettaglioDateOperazioniEnum
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idClassePuntoVenditaSelezionata As Long
Private idClasseSuperareaProdottoSelezionata As Long
Private nomeClasseSuperAreaSelezionata As String
Private classeSuperAreaPrincipale As Boolean
Private idTipoOperazioneSelezionato As Long
Private listaIdTipiOperazioneSelezionati As Collection
Private dataInizioSelezionata As Date
Private dataFineSelezionata As Date
Private dataInizio_old As Date
Private dataFine_old As Date
Private dataInizioSelezionata_SupGestSep As Date
Private dataFineSelezionata_SupGestSep As Date
Private dataInizio_old_SupGestSep As Date
Private dataFine_old_SupGestSep As Date

Private internalEvent As Boolean
Private exitCode As ExitCodeEnum

Private Const CPV_SEDE_ORGANIZZATORE = 1
Private Const CPV_IMPIANTO = 2
Private Const CPV_ALTRI_PV_ORGANIZZATORE = 3
Private Const CPV_RETE_LISTICKET = 5
Private Const CPV_INTERNET = 6
Private Const CPV_CALL_CENTER_LIS = 7
Private Const CPV_ALTRI_CALL_CENTER = 8

Public Sub Init(az As AzioneDettaglioDateOperazioniEnum, idO As Long, idP As Long, idCPV As Long, idCSAP As Long, nomeCSA As String, principale As Boolean, listaIdTO As Collection, dataInizio As Date, dataFine As Date)
    azione = az
    idOrganizzazioneSelezionata = idO
    idProdottoSelezionato = idP
    idClassePuntoVenditaSelezionata = idCPV
    idClasseSuperareaProdottoSelezionata = idCSAP
    nomeClasseSuperAreaSelezionata = nomeCSA
    classeSuperAreaPrincipale = principale
    Set listaIdTipiOperazioneSelezionati = listaIdTO
    dataInizioSelezionata = dataInizio
    dataFineSelezionata = dataFine
    
    dataInizio_old = dataInizioSelezionata
    dataFine_old = dataFineSelezionata
    
    Call InizializzaCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub Init2(idO As Long, idP As Long, idCPV As Long, idCSAPP As Long, idCSAPS As Long, nomeCSAPP As String, nomeCSAPS As String, listaIdTO As Collection, dataInizioPrincipale As Date, dataFinePrincipale As Date, dataInizioSecondaria As Date, dataFineSecondaria As Date)
    idOrganizzazioneSelezionata = idO
    idProdottoSelezionato = idP
    idClassePuntoVenditaSelezionata = idCPV
    If idCSAPS = idNessunElementoSelezionato Then
        idClasseSuperareaProdottoSelezionata = idCSAPP
        nomeClasseSuperAreaSelezionata = nomeCSAPP
        dataInizioSelezionata = dataInizioPrincipale
        dataFineSelezionata = dataFinePrincipale
        classeSuperAreaPrincipale = True
    Else
        idClasseSuperareaProdottoSelezionata = idCSAPS
        nomeClasseSuperAreaSelezionata = nomeCSAPS
        dataInizioSelezionata = dataInizioSecondaria
        dataFineSelezionata = dataFineSecondaria
        classeSuperAreaPrincipale = False
    End If
    Set listaIdTipiOperazioneSelezionati = listaIdTO
    
    dataInizio_old = dataInizioSelezionata
    dataFine_old = dataFineSelezionata
    
    Call InizializzaCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AbilitaPeriodoNormale()
    lblInizio.Visible = True
    dtpDataInizio.Visible = True
    txtOraInizio.Visible = True
    txtMinutiInizio.Visible = True
    lblSeparatoreOreMinutiInizio.Visible = True
    lblFine.Visible = True
    dtpDataFine.Visible = True
    txtOraFine.Visible = True
    txtMinutiFine.Visible = True
    lblSeparatoreOreMinutiFine.Visible = True
End Sub

Private Sub DisabilitaPeriodoNormale()
    lblInizio.Visible = False
    dtpDataInizio.Visible = False
    txtOraInizio.Visible = False
    txtMinutiInizio.Visible = False
    lblSeparatoreOreMinutiInizio.Visible = False
    lblFine.Visible = False
    dtpDataFine.Visible = False
    txtOraFine.Visible = False
    txtMinutiFine.Visible = False
    lblSeparatoreOreMinutiFine.Visible = False
End Sub

Private Sub InizializzaCampi()

    If azione = ADDO_MODIFICA_PRINCIPALE Or azione = ADDO_MODIFICA_SECONDARIA Then
        lblIntestazione.Caption = "Tipo operazione: modifica"
    Else
        If azione = ADDO_ELIMINA_PRINCIPALE Or azione = ADDO_ELIMINA_SECONDARIA Then
            lblIntestazione.Caption = "Tipo operazione: elimina"
        End If
    End If
        
    frmPeriodo.Caption = nomeClasseSuperAreaSelezionata

    If dataInizioSelezionata = dataNulla And dataFineSelezionata = dataNulla Then
        optPeriodoNonDefinito.Value = True
        optPeriodoIllimitato.Value = False
        optPeriodoDefinito.Value = False
        Call DisabilitaPeriodoNormale
    ElseIf dataInizioSelezionata = dataInferioreMinima And dataFineSelezionata = dataSuperioreMassima Then
        optPeriodoNonDefinito.Value = False
        optPeriodoIllimitato.Value = True
        optPeriodoDefinito.Value = False
        Call DisabilitaPeriodoNormale
    Else
        optPeriodoNonDefinito.Value = False
        optPeriodoIllimitato.Value = False
        optPeriodoDefinito.Value = True
        dtpDataInizio.Value = dataInizioSelezionata
        txtOraInizio.Text = StringaOraMinuti(CStr(Hour(dataInizioSelezionata)))
        txtMinutiInizio.Text = StringaOraMinuti(CStr(Minute(dataInizioSelezionata)))
        dtpDataFine.Value = dataFineSelezionata
        txtOraFine.Text = StringaOraMinuti(CStr(Hour(dataFineSelezionata)))
        txtMinutiFine.Text = StringaOraMinuti(CStr(Minute(dataFineSelezionata)))
        Call AbilitaPeriodoNormale
    End If
    
End Sub
    
Private Sub AggiornaAbilitazioneControlli()
    If classeSuperAreaPrincipale Then
        chkEstendiASecondarie.Visible = True
        chkEstendiASecondarie.Enabled = True
    Else
        chkEstendiASecondarie.Enabled = False
        chkEstendiASecondarie.Visible = False
    End If
'    txtOraInizio.Enabled = Not IsNull(dtpDataInizio.Value)
'    txtMinutiInizio.Enabled = Not IsNull(dtpDataInizio.Value)
'    lblSeparatoreOreMinutiInizio.Enabled = Not IsNull(dtpDataInizio.Value)
'    txtOraFine.Enabled = Not IsNull(dtpDataFine.Value)
'    txtMinutiFine.Enabled = Not IsNull(dtpDataFine.Value)
'    lblSeparatoreOreMinutiFine.Enabled = Not IsNull(dtpDataFine.Value)
'    cmdConferma.Enabled = _
'        (IsNull(dtpDataInizio.Value) And IsNull(dtpDataFine.Value)) Or _
'        (Not IsNull(dtpDataInizio.Value) And Not IsNull(dtpDataFine.Value))
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim dataInizio As Date
    Dim oraInizio As Integer
    Dim minutiInizio As Integer
    Dim dataFine As Date
    Dim oraFine As Integer
    Dim minutiFine As Integer
    Dim dataInizioSupGestSep As Date
    Dim oraInizioSupGestSep As Integer
    Dim minutiInizioSupGestSep As Integer
    Dim dataFineSupGestSep As Date
    Dim oraFineSupGestSep As Integer
    Dim minutiFineSupGestSep As Integer
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    
    Set listaNonConformitą = New Collection

    If IsNull(dtpDataInizio.Value) Then
        dataOraInizio = dataNulla
    Else
        dataInizio = FormatDateTime(dtpDataInizio.Value, vbShortDate)
        If txtOraInizio.Text <> "" Then
            If IsCampoOraCorretto(txtOraInizio) Then
                oraInizio = CInt(Trim(txtOraInizio.Text))
            Else
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Ora inizio deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiInizio.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiInizio) Then
                minutiInizio = CInt(Trim(txtMinutiInizio.Text))
            Else
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Minuti inizio deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraInizio = FormatDateTime(dataInizio & " " & oraInizio & ":" & minutiInizio, vbGeneralDate)
    End If
    
    If IsNull(dtpDataFine.Value) Then
        dataOraFine = dataNulla
    Else
        dataFine = FormatDateTime(dtpDataFine.Value, vbShortDate)
        If txtOraFine.Text <> "" Then
            If IsCampoOraCorretto(txtOraFine) Then
                oraFine = CInt(Trim(txtOraFine.Text))
            Else
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Ora fine deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiFine.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiFine) Then
                minutiFine = CInt(Trim(txtMinutiFine.Text))
            Else
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Minuti fine deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraFine = FormatDateTime(dataFine & " " & oraFine & ":" & minutiFine, vbGeneralDate)
    End If
    
    If (dataOraInizio <> dataNulla) And (dataOraFine <> dataNulla) And (dataOraFine <= dataOraInizio) Then
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- la data Inizio deve essere precedente alla data Fine;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    If ValoriCampiOK Then
        exitCode = EC_CONFERMA
        AggiornaNellaBaseDati
        Unload Me
    End If
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim rec As OraDynaset
    Dim sql As String
    Dim sqlDelete As String
    Dim sqlInsert As String
    Dim i As Long
    Dim c As Long
    Dim n As Long
    Dim nuoviRecord As ValoreBooleanoEnum
    Dim qtaRigheEsistenti As Long
    Dim qtaRighePreviste As Long
    Dim listaTipiOperazioneSelezionati As String
    Dim qtaTipiOperazioneSelezionati As Long
    Dim abilitazioniDaSovrascrivere As Boolean
    Dim risposta As VbMsgBoxResult

    On Error GoTo gestioneErrori
            
    listaTipiOperazioneSelezionati = "(-1"
    qtaTipiOperazioneSelezionati = 0
    qtaTipiOperazioneSelezionati = listaIdTipiOperazioneSelezionati.count
    For i = 1 To qtaTipiOperazioneSelezionati
        idTipoOperazioneSelezionato = listaIdTipiOperazioneSelezionati(i)
        listaTipiOperazioneSelezionati = listaTipiOperazioneSelezionati & "," & idTipoOperazioneSelezionato
    Next i
    listaTipiOperazioneSelezionati = listaTipiOperazioneSelezionati & ")"

    Call ApriConnessioneBD

    risposta = vbNo
    
    If classePuntoVenditaConDirittiOperatoreGestibili(idClassePuntoVenditaSelezionata) Then
        sql = "SELECT COUNT(*) QTA" & _
             " FROM OPERATORE_TIPOOPERAZIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDTIPOOPERAZIONE IN " & listaTipiOperazioneSelezionati & _
            " AND IDOPERATORE IN (" & _
            " SELECT DISTINCT IDOPERATORE" & _
            " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, CLASSESAPROD_PUNTOVENDITA CSP_PV" & _
            " WHERE OP.IDPUNTOVENDITAELETTIVO = OCPV.IDPUNTOVENDITA" & _
            " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND CSP_PV.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
            " AND CSP_PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
            ")"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            qtaRigheEsistenti = rec("QTA")
        End If
        rec.Close
        
        sql = "SELECT " & qtaTipiOperazioneSelezionati & " * (SELECT COUNT(IDOPERATORE)" & _
            " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, CLASSESAPROD_PUNTOVENDITA CSP_PV" & _
            " WHERE OP.idPuntoVenditaElettivo = OCPV.idPuntoVendita" & _
            " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND CSP_PV.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
            " AND CSP_PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
            " AND OP.IDPERFEZIONATORE IS NULL" & _
            " AND USERNAME NOT LIKE 'MGZ%'" & _
            ") QTA FROM DUAL"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            qtaRighePreviste = rec("QTA")
        End If
        rec.Close
        
        If qtaRigheEsistenti <> qtaRighePreviste Then
            risposta = MsgBox("Il numero di informazioni configurate (diritti operatore su tipi operazione) e' diverso da quello attualmente presente su DB. Si vuole procedere?", vbYesNoCancel, "Avviso")
        End If
    End If

' Cambiato il meccanismo (20 settembre 2011):
' - si contano le righe complessive presenti sul DB e poi si verifica se differiscono da quelle appena impostate
    If risposta <> vbCancel Then
        SETAConnection.BeginTrans
    
        c = listaIdTipiOperazioneSelezionati.count
        For i = 1 To c
            idTipoOperazioneSelezionato = listaIdTipiOperazioneSelezionati(i)
            
            If classePuntoVenditaConDirittiOperatoreGestibili(idClassePuntoVenditaSelezionata) And risposta = vbYes Then ' si e' deciso di sovrascrivere i dati presenti sul DB
            
                If tipoOperazioneAncoraNonAttiva(idClasseSuperareaProdottoSelezionata, idTipoOperazioneSelezionato, idClassePuntoVenditaSelezionata) Then
                    sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE" & _
                        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                        " AND IDTIPOOPERAZIONE = " & idTipoOperazioneSelezionato & _
                        " AND IDOPERATORE IN" & _
                        "(" & _
                        " SELECT IDOPERATORE" & _
                        " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, CLASSESAPROD_PUNTOVENDITA CSP_PV" & _
                        " WHERE OP.IDPUNTOVENDITAELETTIVO = OCPV.IDPUNTOVENDITA" & _
                        " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                        " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                        " AND CSP_PV.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
                        " AND CSP_PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
                        ")"
                    SETAConnection.Execute sql, n, adCmdText
    
                    If optPeriodoNonDefinito.Value = False Then
                        sql = "INSERT INTO OPERATORE_TIPOOPERAZIONE (IDOPERATORE, IDPRODOTTO, IDTIPOOPERAZIONE) " & _
                            " SELECT DISTINCT IDOPERATORE, " & idProdottoSelezionato & ", " & idTipoOperazioneSelezionato & _
                            " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, CLASSESAPROD_PUNTOVENDITA CSP_PV" & _
                            " WHERE OP.IDPUNTOVENDITAELETTIVO = OCPV.IDPUNTOVENDITA" & _
                            " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                            " AND CSP_PV.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
                            " AND CSP_PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
                            " AND (" & _
                            " OP.IDPERFEZIONATORE IS NULL AND " & idTipoOperazioneSelezionato & " <> " & TO_PERFEZIONAMENTO & _
                            " OR" & _
                            " OP.IDPERFEZIONATORE IS NOT NULL AND " & idTipoOperazioneSelezionato & " = " & TO_PERFEZIONAMENTO & _
                            ")"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                Else
                ' se pero' vado a mettere non abilitato comunque la cancello
                    If optPeriodoNonDefinito.Value = True Then
                        sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE" & _
                            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                            " AND IDTIPOOPERAZIONE = " & idTipoOperazioneSelezionato & _
                            " AND IDOPERATORE IN" & _
                            "(" & _
                            " SELECT IDOPERATORE" & _
                            " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, CLASSESAPROD_PUNTOVENDITA CSP_PV" & _
                            " WHERE OP.IDPUNTOVENDITAELETTIVO = OCPV.IDPUNTOVENDITA" & _
                            " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                            " AND CSP_PV.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
                            " AND CSP_PV.IDPUNTOVENDITA = OCPV.IDPUNTOVENDITA" & _
                            ")"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                End If
            End If
            
            If chkEstendiASecondarie.Value = vbChecked Then
                sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
                    " WHERE IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                    " AND IDTIPOOPERAZIONE = " & idTipoOperazioneSelezionato & _
                    " AND IDCLASSESUPERAREAPRODOTTO IN (" & _
                    " SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    ")"
                SETAConnection.Execute sql, n, adCmdText
                
                If optPeriodoNonDefinito.Value = False Then
                    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                        " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                        ", " & idClassePuntoVenditaSelezionata & _
                        ", " & idTipoOperazioneSelezionato & _
                        ", "
                    If optPeriodoNonDefinito.Value = True Then
                        sql = sql & "NULL, "
                        sql = sql & "NULL"
                    ElseIf optPeriodoIllimitato.Value = True Then
                        sql = sql & SqlDateTimeValue(dataInferioreMinima) & ", "
                        sql = sql & SqlDateTimeValue(dataSuperioreMassima)
                    Else
                        sql = sql & SqlDateTimeValue(dataOraInizio) & ", "
                        sql = sql & SqlDateTimeValue(dataOraFine)
                    End If
                    sql = sql & " FROM CLASSESUPERAREAPRODOTTO" & _
                        " WHERE IDPRODOTTO = " & idProdottoSelezionato
                    SETAConnection.Execute sql, n, adCmdText
                    
                    ' aggiunto il 20 febbraio 2013 per gestire direttamente annullamento riservazione e vendita senza stanmpa in caso di vendita (INTERNET)
                    If idTipoOperazioneSelezionato = TO_VENDITA And idClassePuntoVenditaSelezionata = CPV_INTERNET Then
                    ' cancella e inserisce le date di annullamento riservazione e vendita senza stampa
                        sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
                            " WHERE IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                            " AND IDTIPOOPERAZIONE IN (" & TO_ANNULLAMENTO_RISERVAZIONE & ", " & TO_VENDITA_SENZA_STAMPA & ")" & _
                            " AND IDCLASSESUPERAREAPRODOTTO IN (" & _
                            " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                            " FROM CLASSESUPERAREAPRODOTTO" & _
                            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                            ")"
                        SETAConnection.Execute sql, n, adCmdText
                    
                        sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                            " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                            ", " & CPV_INTERNET & _
                            ", " & TO_ANNULLAMENTO_RISERVAZIONE & _
                            ", "
                        If optPeriodoNonDefinito.Value = True Then
                            sql = sql & "NULL, "
                            sql = sql & "NULL"
                        ElseIf optPeriodoIllimitato.Value = True Then
                            sql = sql & SqlDateTimeValue(dataInferioreMinima) & ", "
                            sql = sql & SqlDateTimeValue(dataSuperioreMassima)
                        Else
                            sql = sql & SqlDateTimeValue(dataOraInizio) & ", "
                            sql = sql & SqlDateTimeValue(dataOraFine) & "+30/24/60"
                        End If
                        sql = sql & " FROM CLASSESUPERAREAPRODOTTO" & _
                            " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        SETAConnection.Execute sql, n, adCmdText
                        
                        sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                            " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                            ", " & CPV_INTERNET & _
                            ", " & TO_VENDITA_SENZA_STAMPA & _
                            ", "
                        If optPeriodoNonDefinito.Value = True Then
                            sql = sql & "NULL, "
                            sql = sql & "NULL"
                        ElseIf optPeriodoIllimitato.Value = True Then
                            sql = sql & SqlDateTimeValue(dataInferioreMinima) & ", "
                            sql = sql & SqlDateTimeValue(dataSuperioreMassima)
                        Else
                            sql = sql & SqlDateTimeValue(dataOraInizio) & ", "
                            sql = sql & SqlDateTimeValue(dataOraFine) & "+30/24/60"
                        End If
                        sql = sql & " FROM CLASSESUPERAREAPRODOTTO" & _
                            " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        SETAConnection.Execute sql, n, adCmdText
                        
                    End If
                    
                End If
            Else
                sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
                    " WHERE IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
                    " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                    " AND IDTIPOOPERAZIONE = " & idTipoOperazioneSelezionato
                SETAConnection.Execute sql, n, adCmdText
        
                If optPeriodoNonDefinito.Value = False Then
                    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                        " VALUES (" & idClasseSuperareaProdottoSelezionata & _
                        ", " & idClassePuntoVenditaSelezionata & _
                        ", " & idTipoOperazioneSelezionato & _
                        ", "
                    If optPeriodoNonDefinito.Value = True Then
                        sql = sql & "NULL, "
                        sql = sql & "NULL)"
                    ElseIf optPeriodoIllimitato.Value = True Then
                        sql = sql & SqlDateTimeValue(dataInferioreMinima) & ", "
                        sql = sql & SqlDateTimeValue(dataSuperioreMassima) & ")"
                    Else
                        sql = sql & SqlDateTimeValue(dataOraInizio) & ", "
                        sql = sql & SqlDateTimeValue(dataOraFine) & ")"
                    End If
                    SETAConnection.Execute sql, n, adCmdText
                    
                    ' aggiunto il 20 febbraio 2013 per gestire direttamente annullamento riservazione e vendita senza stanmpa in caso di vendita (INTERNET)
                    If idTipoOperazioneSelezionato = TO_VENDITA And idClassePuntoVenditaSelezionata = CPV_INTERNET Then
                    ' cancella e inserisce le date di annullamento riservazione e vendita senza stampa
                        sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
                            " WHERE IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
                            " AND IDTIPOOPERAZIONE IN (" & TO_ANNULLAMENTO_RISERVAZIONE & ", " & TO_VENDITA_SENZA_STAMPA & ")" & _
                            " AND IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata
                        SETAConnection.Execute sql, n, adCmdText
                    
                        sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                            " VALUES (" & idClasseSuperareaProdottoSelezionata & _
                            ", " & CPV_INTERNET & _
                            ", " & TO_ANNULLAMENTO_RISERVAZIONE & _
                            ", "
                        If optPeriodoNonDefinito.Value = True Then
                            sql = sql & "NULL, "
                            sql = sql & "NULL"
                        ElseIf optPeriodoIllimitato.Value = True Then
                            sql = sql & SqlDateTimeValue(dataInferioreMinima) & ", "
                            sql = sql & SqlDateTimeValue(dataSuperioreMassima)
                        Else
                            sql = sql & SqlDateTimeValue(dataOraInizio) & ", "
                            sql = sql & SqlDateTimeValue(dataOraFine) & "+30/24/60"
                        End If
                        sql = sql & ")"
                        SETAConnection.Execute sql, n, adCmdText
                        
                        sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                            " VALUES (" & idClasseSuperareaProdottoSelezionata & _
                            ", " & CPV_INTERNET & _
                            ", " & TO_VENDITA_SENZA_STAMPA & _
                            ", "
                        If optPeriodoNonDefinito.Value = True Then
                            sql = sql & "NULL, "
                            sql = sql & "NULL"
                        ElseIf optPeriodoIllimitato.Value = True Then
                            sql = sql & SqlDateTimeValue(dataInferioreMinima) & ", "
                            sql = sql & SqlDateTimeValue(dataSuperioreMassima)
                        Else
                            sql = sql & SqlDateTimeValue(dataOraInizio) & ", "
                            sql = sql & SqlDateTimeValue(dataOraFine) & "+30/24/60"
                        End If
                        sql = sql & ")"
                        SETAConnection.Execute sql, n, adCmdText
                        
                    End If
                    
                End If
                
            End If
        
    ' catalogo
    
            If idTipoOperazioneSelezionato = TO_ABILITAZIONE_PRODOTTO Then
                sqlInsert = ""
    
                Select Case idClassePuntoVenditaSelezionata
                    Case CPV_RETE_LISTICKET
                        sqlDelete = "DELETE FROM PRODOTTO_CANALEDIVENDITA"
                        sqlDelete = sqlDelete & " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        sqlDelete = sqlDelete & " AND IDCANALEDIVENDITA = " & TCDV_RETE_ISTITUZIONALE
                        SETAConnection.Execute sqlDelete, n, adCmdText
                        
                        If dataOraInizio <> dataNulla And dataOraInizio <> dataInferioreMinima Then
                            sqlInsert = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)"
                            sqlInsert = sqlInsert & " VALUES (" & idProdottoSelezionato & ", "
                            sqlInsert = sqlInsert & TCDV_RETE_ISTITUZIONALE & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraInizio) & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraFine)
                            sqlInsert = sqlInsert & ")"
                            SETAConnection.Execute sqlInsert, n, adCmdText
                        End If
                        
                        sqlDelete = "DELETE FROM PRODOTTO_CANALEDIVENDITA"
                        sqlDelete = sqlDelete & " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        sqlDelete = sqlDelete & " AND IDCANALEDIVENDITA = " & TCDV_RETE_LIS
                        SETAConnection.Execute sqlDelete, n, adCmdText
                            
                        If dataOraInizio <> dataNulla And dataOraInizio <> dataInferioreMinima Then
                            sqlInsert = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)"
                            sqlInsert = sqlInsert & " VALUES (" & idProdottoSelezionato & ", "
                            sqlInsert = sqlInsert & TCDV_RETE_LIS & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraInizio) & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraFine)
                            sqlInsert = sqlInsert & ")"
                            SETAConnection.Execute sqlInsert, n, adCmdText
                        End If
                    Case CPV_INTERNET
                        sqlDelete = "DELETE FROM PRODOTTO_CANALEDIVENDITA"
                        sqlDelete = sqlDelete & " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        sqlDelete = sqlDelete & " AND IDCANALEDIVENDITA = " & TCDV_INTERNET
                        SETAConnection.Execute sqlDelete, n, adCmdText
                            
                        If dataOraInizio <> dataNulla And dataOraInizio <> dataInferioreMinima Then
                            sqlInsert = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)"
                            sqlInsert = sqlInsert & " VALUES (" & idProdottoSelezionato & ", "
                            sqlInsert = sqlInsert & TCDV_INTERNET & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraInizio) & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraFine)
                            sqlInsert = sqlInsert & ")"
                            SETAConnection.Execute sqlInsert, n, adCmdText
                        End If
                    Case CPV_CALL_CENTER_LIS
                        sqlDelete = "DELETE FROM PRODOTTO_CANALEDIVENDITA"
                        sqlDelete = sqlDelete & " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        sqlDelete = sqlDelete & " AND IDCANALEDIVENDITA = " & TCDV_CALL_CENTER_LIS
                        SETAConnection.Execute sqlDelete, n, adCmdText
                            
                        If dataOraInizio <> dataNulla And dataOraInizio <> dataInferioreMinima Then
                            sqlInsert = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)"
                            sqlInsert = sqlInsert & " VALUES (" & idProdottoSelezionato & ", "
                            sqlInsert = sqlInsert & TCDV_CALL_CENTER_LIS & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraInizio) & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraFine)
                            sqlInsert = sqlInsert & ")"
                            SETAConnection.Execute sqlInsert, n, adCmdText
                        End If
                    Case CPV_ALTRI_CALL_CENTER
                        sqlDelete = "DELETE FROM PRODOTTO_CANALEDIVENDITA"
                        sqlDelete = sqlDelete & " WHERE IDPRODOTTO = " & idProdottoSelezionato
                        sqlDelete = sqlDelete & " AND IDCANALEDIVENDITA = " & TCDV_ALTRI_CALL_CENTER
                        SETAConnection.Execute sqlDelete, n, adCmdText
                            
                        If dataOraInizio <> dataNulla And dataOraInizio <> dataInferioreMinima Then
                            sqlInsert = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)"
                            sqlInsert = sqlInsert & " VALUES (" & idProdottoSelezionato & ", "
                            sqlInsert = sqlInsert & TCDV_ALTRI_CALL_CENTER & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraInizio) & ", "
                            sqlInsert = sqlInsert & SqlDateTimeValue(dataOraFine)
                            sqlInsert = sqlInsert & ")"
                            SETAConnection.Execute sqlInsert, n, adCmdText
                        End If
                End Select
                            
            End If
        Next i
    End If
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    Dim c As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
        
    sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ"
    sql = sql & " WHERE IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata
    sql = sql & " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata
    sql = sql & " AND IDTIPOOPERAZIONE IN ("
    c = listaIdTipiOperazioneSelezionati.count
    For i = 1 To c
        sql = sql & listaIdTipiOperazioneSelezionati(i)
        If i < c Then
            sql = sql & ","
        End If
    Next i
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText

    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Function GetDataOraInizio() As Date
    GetDataOraInizio = dataOraInizio
End Function

Public Function GetDataOraFine() As Date
    GetDataOraFine = dataOraFine
End Function

Public Function GetDataOraInizioSupGestSep() As Date
    GetDataOraInizioSupGestSep = dataOraInizioSupGestSep
End Function

Public Function GetDataOraFineSupGestSep() As Date
    GetDataOraFineSupGestSep = dataOraFineSupGestSep
End Function

Private Sub optPeriodoNonDefinito_Click()
    Call DisabilitaPeriodoNormale
End Sub

Private Sub optPeriodoIllimitato_Click()
    Call DisabilitaPeriodoNormale
End Sub

Private Sub optPeriodoDefinito_Click()
    Call AbilitaPeriodoNormale
End Sub

Private Sub txtOraInizio_Change()
    If Not internalEvent Then
        If Len(txtOraInizio.Text) = 2 Then
            If txtMinutiInizio.Visible = True Then
                txtMinutiInizio.SetFocus
            End If
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiInizio_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtOraFine_Change()
    If Not internalEvent Then
        If Len(txtOraFine.Text) = 2 Then
            If txtMinutiFine.Visible = True Then
                txtMinutiFine.SetFocus
            End If
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiFine_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub dtpDataInizio_Init()
    dtpDataInizio.Value = Null
    dtpDataInizio.MinDate = dataNulla
End Sub

Private Sub dtpDataInizio_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dtpDataFine_Change()
    Call AggiornaAbilitazioneControlli
End Sub

