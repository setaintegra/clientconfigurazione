VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmConfigurazioneCategoriaCartaDiPagamento 
   Caption         =   "Categoria Carta di Pagamento"
   ClientHeight    =   8700
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11985
   LinkTopic       =   "Form1"
   ScaleHeight     =   8700
   ScaleWidth      =   11985
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtFineIntervalloNumeroDiSerie 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3060
      MaxLength       =   8
      TabIndex        =   21
      Top             =   6960
      Width           =   1635
   End
   Begin VB.TextBox txtDescrizione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4080
      MaxLength       =   255
      TabIndex        =   2
      Top             =   6240
      Width           =   6195
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   1
      Top             =   6240
      Width           =   3675
   End
   Begin VB.TextBox txtInizioIntervalloNumeroDiSerie 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   8
      TabIndex        =   3
      Top             =   6960
      Width           =   1635
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10680
      TabIndex        =   12
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   9
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   4
      Top             =   4860
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   0
      Top             =   240
      Width           =   3255
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneCategorieCartaDiPagamento 
      Height          =   330
      Left            =   240
      Top             =   4080
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneCategoriaCartaDiPagamento 
      Height          =   3915
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblFineNumeroDiSerie 
      Caption         =   "Fine intervallo numero di serie"
      Height          =   195
      Left            =   3060
      TabIndex        =   22
      Top             =   6720
      Width           =   2415
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   195
      Left            =   4080
      TabIndex        =   20
      Top             =   6000
      Width           =   1035
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   195
      Left            =   120
      TabIndex        =   19
      Top             =   6000
      Width           =   1035
   End
   Begin VB.Label lblInizioNumeroDiSerie 
      Caption         =   "Inizio intervallo numero di serie"
      Height          =   195
      Left            =   120
      TabIndex        =   18
      Top             =   6720
      Width           =   2415
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione Categorie Carta di Pagamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   15
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   14
      Top             =   0
      Width           =   3195
   End
End
Attribute VB_Name = "frmConfigurazioneCategoriaCartaDiPagamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idGenereCartaDiPagamentoSelezionato As Long
Private SQLCaricamentoCombo As String
Private nomeGenereCartaDiPagamentoSelezionato As String
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String
Private nome As String
Private descrizione As String
Private inizioIntervalloNumeroDiSerie As String
Private fineIntervalloNumeroDiSerie As String

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()

    lblInfo1.Caption = "Genere Carta di Pagamento"
    txtInfo1.Text = nomeGenereCartaDiPagamentoSelezionato
    txtInfo1.Enabled = False

    dgrConfigurazioneCategoriaCartaDiPagamento.Caption = "CATEGORIE CARTA DI PAGAMENTO CONFIGURATE"

    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneCategoriaCartaDiPagamento.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtInizioIntervalloNumeroDiSerie.Text = ""
        txtFineIntervalloNumeroDiSerie.Text = ""
        
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtInizioIntervalloNumeroDiSerie.Enabled = False
        txtFineIntervalloNumeroDiSerie.Enabled = False
        
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblInizioNumeroDiSerie.Enabled = False
        lblFineNumeroDiSerie.Enabled = False
        
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False

    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneCategoriaCartaDiPagamento.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtInizioIntervalloNumeroDiSerie.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtFineIntervalloNumeroDiSerie.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Len(txtNome.Text) > 1 And _
                               Len(txtInizioIntervalloNumeroDiSerie.Text) <= 8 And _
                               Len(txtFineIntervalloNumeroDiSerie.Text) <= 8)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneCategoriaCartaDiPagamento.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtInizioIntervalloNumeroDiSerie.Text = ""
        txtFineIntervalloNumeroDiSerie.Text = ""
        
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtInizioIntervalloNumeroDiSerie.Enabled = False
        txtFineIntervalloNumeroDiSerie.Enabled = False
        
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblInizioNumeroDiSerie.Enabled = False
        lblFineNumeroDiSerie.Enabled = False
        
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False

    End If
    
End Sub

Private Sub Controlli_Init()

    lblInfo1.Caption = "Genere Carta di Pagamento"
    txtInfo1.Text = nomeGenereCartaDiPagamentoSelezionato
    txtInfo1.Enabled = False
    lblNome.Caption = "Nome"
    lblDescrizione.Caption = "Descrizione"
    lblInizioNumeroDiSerie.Caption = "Inizio intervallo numero di serie"
    lblFineNumeroDiSerie.Caption = "Fine intervallo numero di serie"

    lblIntestazioneForm.Caption = "Configurazione delle Categorie Carta di Pagamento"
    dgrConfigurazioneCategoriaCartaDiPagamento.Caption = "CATEGORIE CARTA DI PAGAMENTO CONFIGURATE"

    dgrConfigurazioneCategoriaCartaDiPagamento.Enabled = True
    lblOperazioneInCorso.Caption = ""
    cmdInserisciNuovo.Enabled = True
    cmdInserisciDaSelezione.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdModifica.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdConferma.Enabled = False
    cmdAnnulla.Enabled = False

End Sub

Public Sub SetIdGenereCartaDiPagamentoSelezionato(id As Long)
    idGenereCartaDiPagamentoSelezionato = id
End Sub

Public Sub SetNomeGenereCartaDiPagamentoSelezionato(nome As String)
    nomeGenereCartaDiPagamentoSelezionato = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    stringaNota = "IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato

    
    If ValoriCampiOK Then
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_GENERECARTADIPAGAMENTO, CCDA_CATEGORIACARTADIPAGAMENTO, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneCategoriaCartaDiPagamento_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneCategoriaCartaDiPagamento_Init
            Case ASG_INSERISCI_DA_SELEZIONE
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_GENERECARTADIPAGAMENTO, CCDA_CATEGORIACARTADIPAGAMENTO, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneCategoriaCartaDiPagamento_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneCategoriaCartaDiPagamento_Init
            Case ASG_MODIFICA
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_GENERECARTADIPAGAMENTO, CCDA_CATEGORIACARTADIPAGAMENTO, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneCategoriaCartaDiPagamento_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneCategoriaCartaDiPagamento_Init
            Case ASG_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_GENERECARTADIPAGAMENTO, CCDA_CATEGORIACARTADIPAGAMENTO, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneCategoriaCartaDiPagamento_Init
                Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                Call dgrConfigurazioneCategoriaCartaDiPagamento_Init
        End Select
        
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneCategorieCartaDiPagamento.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneCategoriaCartaDiPagamento_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneCategoriaCartaDiPagamento_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneCategoriaCartaDiPagamento_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset

    Set rec = adcConfigurazioneCategorieCartaDiPagamento.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneCategoriaCartaDiPagamento_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazioneCategorieCartaDiPagamento
    
    sql = "SELECT C.IDCATEGORIACARTADIPAGAMENTO AS ""ID""," & _
        " C.NOME AS ""Nome""," & _
        " C.DESCRIZIONE AS ""Descrizione""," & _
        " C.INIZIOINTERVALLONUMERODISERIE AS ""Inizio""," & _
        " C.FINEINTERVALLONUMERODISERIE AS ""Fine""" & _
        " FROM CATEGORIACARTADIPAGAMENTO C" & _
        " WHERE C.IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato & _
        " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneCategoriaCartaDiPagamento.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    sql = "SELECT IDCATEGORIACARTADIPAGAMENTO, NOME, DESCRIZIONE, INIZIOINTERVALLONUMERODISERIE, FINEINTERVALLONUMERODISERIE"
    sql = sql & " FROM CATEGORIACARTADIPAGAMENTO"
    sql = sql & " WHERE IDCATEGORIACARTADIPAGAMENTO = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nome = rec("NOME")
        descrizione = rec("DESCRIZIONE")
        inizioIntervalloNumeroDiSerie = rec("INIZIOINTERVALLONUMERODISERIE")
        fineIntervalloNumeroDiSerie = rec("FINEINTERVALLONUMERODISERIE")
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim s As String

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = nome
    txtDescrizione.Text = descrizione
    txtInizioIntervalloNumeroDiSerie.Text = inizioIntervalloNumeroDiSerie
    txtFineIntervalloNumeroDiSerie.Text = fineIntervalloNumeroDiSerie
    
    internalEvent = internalEventOld

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim categoriaCartaDiPagamentoEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    categoriaCartaDiPagamentoEliminabile = True
    
    If Not IsRecordEliminabile("IDCATEGORIACARTADIPAGAMENTO", "TAR_CATEGORIACARTADIPAGAMENTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("TAR_CATEGORIACARTADIPAGAMENTO")
        categoriaCartaDiPagamentoEliminabile = False
    End If
    
    If categoriaCartaDiPagamentoEliminabile Then
        sql = "DELETE FROM CATEGORIACARTADIPAGAMENTO" & _
            " WHERE IDCATEGORIACARTADIPAGAMENTO = " & idRecordSelezionato
        SETAConnection.Execute sql, , adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Categoria Carta di Pagamento selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub dgrConfigurazioneCategoriaCartaDiPagamento_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneCategoriaCartaDiPagamento
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim ora As String
    Dim minuti As String
    Dim oreDurata As String
    Dim minutiDurata As String
    Dim secondi As String
    Dim dataInizio As Date
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    
    Set listaNonConformitą = New Collection
    
    nome = CStr(Trim(txtNome.Text))
    descrizione = CStr(Trim(txtDescrizione.Text))
    
    If Len(txtInizioIntervalloNumeroDiSerie.Text) <= 8 Then
        inizioIntervalloNumeroDiSerie = CStr(Trim(txtInizioIntervalloNumeroDiSerie.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Inizio intervallo numero di serie deve essere minore o uguale a 8 caratteri;")
    End If
    If Len(txtFineIntervalloNumeroDiSerie.Text) <= 8 Then
        fineIntervalloNumeroDiSerie = CStr(Trim(txtFineIntervalloNumeroDiSerie.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Fine intervallo numero di serie deve essere minore o uguale a 8 caratteri;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtInizioIntervalloNumeroDiSerie_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtFineIntervalloNumeroDiSerie_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim idNuovaCategoriaCartaDiPagamento As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovaCategoriaCartaDiPagamento = OttieniIdentificatoreDaSequenza("SQ_CATEGORIACARTADIPAGAMENTO")
    
'   INSERIMENTO IN TABELLA CATEGORIACARTADIPAGAMENTO
    sql = "INSERT INTO CATEGORIACARTADIPAGAMENTO (IDCATEGORIACARTADIPAGAMENTO, NOME, DESCRIZIONE, INIZIOINTERVALLONUMERODISERIE, FINEINTERVALLONUMERODISERIE, IDGENERECARTADIPAGAMENTO)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaCategoriaCartaDiPagamento & ", '"
    sql = sql & nome & "', '"
    sql = sql & descrizione & "', "
    sql = sql & inizioIntervalloNumeroDiSerie & ", "
    sql = sql & fineIntervalloNumeroDiSerie & ", "
    sql = sql & idGenereCartaDiPagamentoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaCategoriaCartaDiPagamento)
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Call SETAConnection.BeginTrans
    
    sql = "UPDATE CATEGORIACARTADIPAGAMENTO SET"
    sql = sql & " NOME = " & SqlStringValue(nome) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizione) & ","
    sql = sql & " INIZIOINTERVALLONUMERODISERIE = " & SqlStringValue(inizioIntervalloNumeroDiSerie) & ","
    sql = sql & " FINEINTERVALLONUMERODISERIE = " & SqlStringValue(fineIntervalloNumeroDiSerie)
    sql = sql & " WHERE IDCATEGORIACARTADIPAGAMENTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub
