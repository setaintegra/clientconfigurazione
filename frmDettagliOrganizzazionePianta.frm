VERSION 5.00
Begin VB.Form frmDettagliOrganizzazionePianta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli Organizzazione"
   ClientHeight    =   3885
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4755
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3885
   ScaleWidth      =   4755
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstCodiciPiantaOrganizzazione 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   3915
   End
   Begin VB.TextBox txtPianta 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      MaxLength       =   30
      TabIndex        =   3
      Top             =   2700
      Width           =   3255
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2640
      TabIndex        =   5
      Top             =   3240
      Width           =   1035
   End
   Begin VB.TextBox txtCodicePianta 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      MaxLength       =   2
      TabIndex        =   1
      Top             =   1860
      Width           =   435
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   960
      TabIndex        =   4
      Top             =   3240
      Width           =   1035
   End
   Begin VB.TextBox txtOrganizzazione 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      MaxLength       =   30
      TabIndex        =   2
      Top             =   2280
      Width           =   3255
   End
   Begin VB.Label lblCodiciOrganizzazione 
      Alignment       =   2  'Center
      Caption         =   "Codici giā presenti per l'Organizzazione"
      Height          =   195
      Left            =   360
      TabIndex        =   9
      Top             =   120
      Width           =   3915
   End
   Begin VB.Label lblPianta 
      Alignment       =   1  'Right Justify
      Caption         =   "Pianta"
      Height          =   195
      Left            =   120
      TabIndex        =   8
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Label lblOrganizzazione 
      Alignment       =   1  'Right Justify
      Caption         =   "Organizzazione"
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   2340
      Width           =   1095
   End
   Begin VB.Label lblCodicePianta 
      Alignment       =   1  'Right Justify
      Caption         =   "Codice pianta"
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   1920
      Width           =   1155
   End
End
Attribute VB_Name = "frmDettagliOrganizzazionePianta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strCodicePianta As String
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
Private idPiantaSelezionata As Long
Private nomePianta As String

Private operazioneSuPianta As AzioneEnum

Public Sub Init()
    Dim sql As String
    
    Select Case operazioneSuPianta
        Case A_NUOVO
            sql = "SELECT ORGANIZZAZIONE_PIANTA.CODICEPIANTA COD, PIANTA.IDPIANTA ID, PIANTA.NOME NOME" & _
                " FROM ORGANIZZAZIONE_PIANTA, PIANTA WHERE" & _
                " (ORGANIZZAZIONE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
                " (ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") AND" & _
                " (PIANTA.IDPIANTA <> " & idPiantaSelezionata & ")" & _
                " ORDER BY COD"
        Case A_CLONA
            sql = "SELECT ORGANIZZAZIONE_PIANTA.CODICEPIANTA COD, PIANTA.IDPIANTA ID, PIANTA.NOME NOME" & _
                " FROM ORGANIZZAZIONE_PIANTA, PIANTA WHERE" & _
                " (ORGANIZZAZIONE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
                " (ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")" & _
                " ORDER BY COD"
        Case Else
            'Do Nothing
    End Select
    Call CaricaValoriLista(lstCodiciPiantaOrganizzazione, sql, "COD", "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    txtPianta.Enabled = False
    txtOrganizzazione.Enabled = False
    cmdConferma.Enabled = Trim(txtCodicePianta.Text) <> ""
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection

On Error Resume Next

'    ValoriCampiOK = True
'    Set listaNonConformitā = New Collection
'
'    If IsCampoInteroCorretto(txtCodicePianta) Then
'        codicePianta = CInt(Trim(txtCodicePianta.Text))
'        If Not IsCodiceUnivoco Then
''            Call frmMessaggio.Visualizza("ErroreDuplicazioneCodiceConstraint", codicePianta, nomeOrganizzazioneSelezionata)
'            ValoriCampiOK = False
'            Call listaNonConformitā.Add("- il valore Codice pianta = " & SqlStringValue(CStr(codicePianta)) & _
'                " č giā presente in DB per la stessa Organizzazione;")
'        End If
'    Else
''        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero")
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore immesso sul campo Codice pianta deve essere numerico di tipo intero;")
'    End If
'
    valoriCampiOK = True
    Set listaNonConformitā = New Collection
    
    strCodicePianta = Trim(txtCodicePianta.Text)
    If Not IsCodiceUnivoco Then
'            Call frmMessaggio.Visualizza("ErroreDuplicazioneCodiceConstraint", codicePianta, nomeOrganizzazioneSelezionata)
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore Codice pianta = " & SqlStringValue(strCodicePianta) & _
            " č giā presente in DB per la stessa Organizzazione;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call frmInizialePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_ANNULLA)
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    If valoriCampiOK Then
        Select Case operazioneSuPianta
            Case A_NUOVO
                Call frmInizialePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_CONFERMA)
                Call frmInizialePianta.SetCodicePiantaOrganizzazioneSelezionata(strCodicePianta)
            Case A_CLONA
                Call frmClonazionePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_CONFERMA)
                Call frmClonazionePianta.SetCodicePiantaOrganizzazioneSelezionata(strCodicePianta)
            Case Else
        End Select
        Unload Me
    End If
    
End Sub

Private Sub txtCodicePianta_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetCodicePianta(cod As String)
    strCodicePianta = cod
End Sub

Private Sub AssegnaValoriCampi()
    txtCodicePianta.Text = ""
    txtOrganizzazione.Text = Trim(nomeOrganizzazioneSelezionata)
    txtPianta.Text = Trim(nomePianta)
End Sub

Public Sub SetIdOrganizzazioneSelezionata(idO As Long)
    idOrganizzazioneSelezionata = idO
End Sub

Public Sub SetIdPiantaSelezionata(idP As Long)
    idPiantaSelezionata = idP
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nomeO As String)
    nomeOrganizzazioneSelezionata = nomeO
End Sub

Public Sub SetNomePianta(nomeP As String)
    nomePianta = nomeP
End Sub

Private Function IsCodiceUnivoco() As Boolean
    Dim iCod As Integer
    Dim trovato As Boolean
    Dim tmpCodice As String
    Dim substr As String
    Dim fine As Long

    trovato = False
    iCod = 0
    While iCod <= (lstCodiciPiantaOrganizzazione.ListCount - 1) And Not trovato
        lstCodiciPiantaOrganizzazione.Selected(iCod) = True
        tmpCodice = lstCodiciPiantaOrganizzazione.Text
        fine = InStr(tmpCodice, " ")
        substr = Mid(tmpCodice, 1, fine - 1)
        If strCodicePianta = substr Then
            trovato = True
        End If
        iCod = iCod + 1
    Wend
    IsCodiceUnivoco = Not trovato
End Function

Private Sub CaricaValoriLista(lst As ListBox, strSQL As String, NomeCampo1 As String, Optional nomeCampo2 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaVisualizzata As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            stringaVisualizzata = rec(NomeCampo1) & " - " & rec(nomeCampo2)
            lst.AddItem stringaVisualizzata
'            lst.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetOperazioneSuPianta(operaz As AzioneEnum)
    operazioneSuPianta = operaz
End Sub


