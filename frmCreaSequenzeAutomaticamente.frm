VERSION 5.00
Begin VB.Form frmCreaSequenzeAutomaticamente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Crea sequenze automaticamente"
   ClientHeight    =   3285
   ClientLeft      =   11040
   ClientTop       =   6690
   ClientWidth     =   4230
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   4230
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdABC123S 
      Height          =   435
      Left            =   180
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdABC321S 
      Height          =   435
      Left            =   660
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdCBA123S 
      Height          =   435
      Left            =   1140
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdCBA321S 
      Height          =   435
      Left            =   1620
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdABC123L 
      Height          =   435
      Left            =   2160
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdABC321L 
      Height          =   435
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdCBA123L 
      Height          =   435
      Left            =   3120
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdCBA321L 
      Height          =   435
      Left            =   3600
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   2160
      Width           =   435
   End
   Begin VB.CommandButton cmdW2 
      Height          =   435
      Left            =   2160
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdE2 
      Height          =   435
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdN2 
      Height          =   435
      Left            =   3120
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdS2 
      Height          =   435
      Left            =   3600
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdS1 
      Height          =   435
      Left            =   1620
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdN1 
      Height          =   435
      Left            =   1140
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdE1 
      Height          =   435
      Left            =   660
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdW1 
      Height          =   435
      Left            =   180
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   960
      Width           =   435
   End
   Begin VB.CommandButton cmdSEV 
      Height          =   435
      Left            =   3600
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdSWV 
      Height          =   435
      Left            =   3120
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdNEV 
      Height          =   435
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdNWV 
      Height          =   435
      Left            =   2160
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdSEO 
      Height          =   435
      Left            =   1620
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdSWO 
      Height          =   435
      Left            =   1140
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdNEO 
      Height          =   435
      Left            =   660
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdNWO 
      Height          =   435
      Left            =   180
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Default         =   -1  'True
      Height          =   315
      Left            =   1620
      TabIndex        =   0
      Top             =   2880
      Width           =   1035
   End
   Begin VB.Line Line1 
      X1              =   180
      X2              =   4020
      Y1              =   1620
      Y2              =   1620
   End
   Begin VB.Label lblOrdinamentoLogico 
      Caption         =   "Ordina in sequenza per Nome fila e Nome posto"
      Height          =   195
      Left            =   180
      TabIndex        =   26
      Top             =   1860
      Width           =   3855
   End
   Begin VB.Label lblOrdinamentoGeometrico 
      Caption         =   "Ordina in sequenza posti contigui"
      Height          =   195
      Left            =   180
      TabIndex        =   25
      Top             =   120
      Width           =   3855
   End
End
Attribute VB_Name = "frmCreaSequenzeAutomaticamente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private exitCode As ExitCodeEnum
Private tipoSequenza As TipoSequenzaEnum

Public Sub Init()
On Error GoTo frmCreaSequenzeAutomaticamente_Init
    
    Set cmdNWO.Picture = LoadPicture(App.Path & "\NWO.ico")
    Set cmdNEO.Picture = LoadPicture(App.Path & "\NEO.ico")
    Set cmdSWO.Picture = LoadPicture(App.Path & "\SWO.ico")
    Set cmdSEO.Picture = LoadPicture(App.Path & "\SEO.ico")
    Set cmdNWV.Picture = LoadPicture(App.Path & "\NWV.ico")
    Set cmdNEV.Picture = LoadPicture(App.Path & "\NEV.ico")
    Set cmdSWV.Picture = LoadPicture(App.Path & "\SWV.ico")
    Set cmdSEV.Picture = LoadPicture(App.Path & "\SEV.ico")
    Set cmdW1.Picture = LoadPicture(App.Path & "\W1.ico")
    Set cmdE1.Picture = LoadPicture(App.Path & "\E1.ico")
    Set cmdN1.Picture = LoadPicture(App.Path & "\N1.ico")
    Set cmdS1.Picture = LoadPicture(App.Path & "\S1.ico")
    Set cmdW2.Picture = LoadPicture(App.Path & "\W2.ico")
    Set cmdE2.Picture = LoadPicture(App.Path & "\E2.ico")
    Set cmdN2.Picture = LoadPicture(App.Path & "\N2.ico")
    Set cmdS2.Picture = LoadPicture(App.Path & "\S2.ico")
    
    Set cmdABC123S.Picture = LoadPicture(App.Path & "\ABC123S.ico")
    Set cmdABC321S.Picture = LoadPicture(App.Path & "\ABC321S.ico")
    Set cmdCBA123S.Picture = LoadPicture(App.Path & "\CBA123S.ico")
    Set cmdCBA321S.Picture = LoadPicture(App.Path & "\CBA321S.ico")
    Set cmdABC123L.Picture = LoadPicture(App.Path & "\ABC123L.ico")
    Set cmdABC321L.Picture = LoadPicture(App.Path & "\ABC321L.ico")
    Set cmdCBA123L.Picture = LoadPicture(App.Path & "\CBA123L.ico")
    Set cmdCBA321L.Picture = LoadPicture(App.Path & "\CBA321L.ico")
    
    Call Me.Show(vbModal)
    Exit Sub
    
frmCreaSequenzeAutomaticamente_Init:
    MsgBox Err.Description
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Function GetTipoSequenza() As TipoSequenzaEnum
    GetTipoSequenza = tipoSequenza
End Function

Private Sub cmdNWO_Click()
    Call TipoSequenzaNWO
End Sub

Private Sub TipoSequenzaNWO()
    tipoSequenza = TS_NWO
    Call Esci
End Sub

Private Sub cmdNEO_Click()
    Call TipoSequenzaNEO
End Sub

Private Sub TipoSequenzaNEO()
    tipoSequenza = TS_NEO
    Call Esci
End Sub

Private Sub cmdSWO_Click()
    Call TipoSequenzaSWO
End Sub

Private Sub TipoSequenzaSWO()
    tipoSequenza = TS_SWO
    Call Esci
End Sub

Private Sub cmdSEO_Click()
    Call TipoSequenzaSEO
End Sub

Private Sub TipoSequenzaSEO()
    tipoSequenza = TS_SEO
    Call Esci
End Sub

Private Sub cmdNWV_Click()
    Call TipoSequenzaNWV
End Sub

Private Sub TipoSequenzaNWV()
    tipoSequenza = TS_NWV
    Call Esci
End Sub

Private Sub cmdNEV_Click()
    Call TipoSequenzaNEV
End Sub

Private Sub TipoSequenzaNEV()
    tipoSequenza = TS_NEV
    Call Esci
End Sub

Private Sub cmdSWV_Click()
    Call TipoSequenzaSWV
End Sub

Private Sub TipoSequenzaSWV()
    tipoSequenza = TS_SWV
    Call Esci
End Sub

Private Sub cmdSEV_Click()
    Call TipoSequenzaSEV
End Sub

Private Sub TipoSequenzaSEV()
    tipoSequenza = TS_SEV
    Call Esci
End Sub

Private Sub cmdW1_Click()
    Call TipoSequenzaW1
End Sub

Private Sub TipoSequenzaW1()
    tipoSequenza = TS_W1
    Call Esci
End Sub

Private Sub cmdE1_Click()
    Call TipoSequenzaE1
End Sub

Private Sub TipoSequenzaE1()
    tipoSequenza = TS_E1
    Call Esci
End Sub

Private Sub cmdN1_Click()
    Call TipoSequenzaN1
End Sub

Private Sub TipoSequenzaN1()
    tipoSequenza = TS_N1
    Call Esci
End Sub

Private Sub cmdS1_Click()
    Call TipoSequenzaS1
End Sub

Private Sub TipoSequenzaS1()
    tipoSequenza = TS_S1
    Call Esci
End Sub

Private Sub Esci()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub cmdW2_Click()
    Call TipoSequenzaW2
End Sub

Private Sub TipoSequenzaW2()
    tipoSequenza = TS_W2
    Call Esci
End Sub

Private Sub cmdE2_Click()
    Call TipoSequenzaE2
End Sub

Private Sub TipoSequenzaE2()
    tipoSequenza = TS_E2
    Call Esci
End Sub

Private Sub cmdN2_Click()
    Call TipoSequenzaN2
End Sub

Private Sub TipoSequenzaN2()
    tipoSequenza = TS_N2
    Call Esci
End Sub

Private Sub cmdS2_Click()
    Call TipoSequenzaS2
End Sub

Private Sub TipoSequenzaS2()
    tipoSequenza = TS_S2
    Call Esci
End Sub

Private Sub cmdABC123S_Click()
    Call TipoSequenzaABC123S
End Sub

Private Sub TipoSequenzaABC123S()
    tipoSequenza = TS_ABC123S
    Call Esci
End Sub

Private Sub cmdABC321S_Click()
    Call TipoSequenzaABC321S
End Sub

Private Sub TipoSequenzaABC321S()
    tipoSequenza = TS_ABC321S
    Call Esci
End Sub

Private Sub cmdCBA123S_Click()
    Call TipoSequenzaCBA123S
End Sub

Private Sub TipoSequenzaCBA123S()
    tipoSequenza = TS_CBA123S
    Call Esci
End Sub

Private Sub cmdCBA321S_Click()
    Call TipoSequenzaCBA321S
End Sub

Private Sub TipoSequenzaCBA321S()
    tipoSequenza = TS_CBA321S
    Call Esci
End Sub

Private Sub cmdABC123L_Click()
    Call TipoSequenzaABC123L
End Sub

Private Sub TipoSequenzaABC123L()
    tipoSequenza = TS_ABC123L
    Call Esci
End Sub

Private Sub cmdABC321L_Click()
    Call TipoSequenzaABC321L
End Sub

Private Sub TipoSequenzaABC321L()
    tipoSequenza = TS_ABC321L
    Call Esci
End Sub

Private Sub cmdCBA123L_Click()
    Call TipoSequenzaCBA123L
End Sub

Private Sub TipoSequenzaCBA123L()
    tipoSequenza = TS_CBA123L
    Call Esci
End Sub

Private Sub cmdCBA321L_Click()
    Call TipoSequenzaCBA321L
End Sub

Private Sub TipoSequenzaCBA321L()
    tipoSequenza = TS_CBA321L
    Call Esci
End Sub


