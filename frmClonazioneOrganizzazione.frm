VERSION 5.00
Begin VB.Form frmClonazioneOrganizzazione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   9105
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9105
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   26
      Top             =   7980
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   16
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10500
      TabIndex        =   18
      Top             =   8280
      Width           =   1155
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9540
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   240
      Width           =   2295
   End
   Begin VB.Frame fraDatiNuovoProdotto 
      Caption         =   "Dati nuovo prodotto "
      Height          =   7275
      Left            =   120
      TabIndex        =   19
      Top             =   600
      Width           =   9855
      Begin VB.CheckBox chkImportazioneLayoutAlfresco 
         Caption         =   "Importa Layout Alfresco"
         Height          =   195
         Left            =   660
         TabIndex        =   33
         Top             =   6240
         Width           =   2355
      End
      Begin VB.ComboBox cmbImportazioneLayoutAlfresco 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   6480
         Width           =   3585
      End
      Begin VB.TextBox txtCodiceFiscaleSocieta 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7320
         MaxLength       =   16
         TabIndex        =   30
         Top             =   1740
         Width           =   1815
      End
      Begin VB.ComboBox cmbImportazioneCausaliProtezione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   5820
         Width           =   3585
      End
      Begin VB.CheckBox chkImportazioneCausaliProtezione 
         Caption         =   "Importa Causali di protezione"
         Height          =   195
         Left            =   660
         TabIndex        =   14
         Top             =   5580
         Width           =   2355
      End
      Begin VB.ComboBox cmbImportazioneOperatori 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   5160
         Width           =   3585
      End
      Begin VB.CheckBox chkImportazioneOperatori 
         Caption         =   "Importa Operatori"
         Height          =   195
         Left            =   660
         TabIndex        =   12
         Top             =   4920
         Width           =   2355
      End
      Begin VB.TextBox txtCodiceFiscalePartitaIVA 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3480
         MaxLength       =   16
         TabIndex        =   3
         Top             =   1740
         Width           =   1815
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1500
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   720
         Width           =   7635
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   0
         Top             =   300
         Width           =   3255
      End
      Begin VB.TextBox txtCodiceTLOrganizzazione 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   4
         TabIndex        =   2
         Top             =   1740
         Width           =   555
      End
      Begin VB.ListBox lstCodiciTLOrganizzazione 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4260
         Left            =   4560
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   2520
         Width           =   4575
      End
      Begin VB.CheckBox chkImportazioneTipiLayout 
         Caption         =   "Importa Tipi/Layout"
         Height          =   195
         Left            =   660
         TabIndex        =   4
         Top             =   2280
         Width           =   2955
      End
      Begin VB.CheckBox chkImportazionePuntiVendita 
         Caption         =   "Importa Punti vendita"
         Height          =   195
         Left            =   660
         TabIndex        =   8
         Top             =   3600
         Width           =   2355
      End
      Begin VB.ComboBox cmbImportazioneTipiLayout 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   2520
         Width           =   3585
      End
      Begin VB.ComboBox cmbImportazionePuntiVendita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   3840
         Width           =   3585
      End
      Begin VB.ComboBox cmbImportazioneVenditoriEsterni 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   4500
         Width           =   3585
      End
      Begin VB.CheckBox chkImportazioneVenditoriEsterni 
         Caption         =   "Importa Venditori esterni"
         Height          =   195
         Left            =   660
         TabIndex        =   10
         Top             =   4260
         Width           =   2355
      End
      Begin VB.ComboBox cmbImportazioneTariffa 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   3180
         Width           =   3585
      End
      Begin VB.CheckBox chkImportazioneTariffa 
         Caption         =   "Importa Tariffa"
         Height          =   195
         Left            =   660
         TabIndex        =   6
         Top             =   2940
         Width           =   2475
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "codice fiscale societa"
         Height          =   255
         Left            =   5640
         TabIndex        =   31
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label lblCodiceFiscalePartitaIVA 
         Alignment       =   1  'Right Justify
         Caption         =   "CF / partita IVA"
         Height          =   255
         Left            =   2340
         TabIndex        =   29
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   300
         TabIndex        =   23
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblCodiceTerminaleLotto 
         Alignment       =   1  'Right Justify
         Caption         =   "Codice TL"
         Height          =   255
         Left            =   480
         TabIndex        =   22
         Top             =   1800
         Width           =   855
      End
      Begin VB.Label lblCodiciOrganizzazione 
         Caption         =   "Codici TL giā presenti"
         Height          =   195
         Left            =   4560
         TabIndex        =   21
         Top             =   2280
         Width           =   3015
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Clonazione dell'Organizzazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   6975
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9540
      TabIndex        =   27
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmClonazioneOrganizzazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private newNome As String
Private newDescrizione As String
Private newCodiceTLOrganizzazione As String
Private newCodiceFiscalePartitaIVA As String
Private newCodiceFiscaleSocieta As String
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
'Private listaCampiValoriUnici As Collection
Private idOrganizzazioneTipiLayout As Long
Private idOrganizzazioneTariffa As Long
Private idOrganizzazionePuntiVendita As Long
Private idOrganizzazioneVenditoriEsterni As Long
Private idOrganizzazioneOperatori As Long
Private idOrganizzazioneCausaliProtezione As Long
Private idOrganizzazioneLayoutAlfresco As Long
Private internalEvent As Boolean
Private idPiantaSelezionata As Long
'Private isProdottoDefinitoSuMedesimaPianta As Boolean

Private importaTipiLayout As ValoreBooleanoEnum
Private importaTariffa As ValoreBooleanoEnum
Private importaPuntiVendita As ValoreBooleanoEnum
Private importaVenditoriEsterni As ValoreBooleanoEnum
Private importaOperatori As ValoreBooleanoEnum
Private importaCausaliProtezione As ValoreBooleanoEnum
Private importaLayoutAlfresco As ValoreBooleanoEnum

Private exitCode As ExitCodeEnum

Private Sub AggiornaAbilitazioneControlli()
    txtInfo1.Enabled = False
    lblInfo1.Caption = "Organizzazione"
    cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
        Len(Trim(txtCodiceTLOrganizzazione.Text)) = 4 And _
        (Len(Trim(txtCodiceFiscalePartitaIVA.Text)) = 11 Or _
            Len(Trim(txtCodiceFiscalePartitaIVA)) = 16))
    cmbImportazioneTipiLayout.Enabled = (importaTipiLayout = VB_VERO)
    cmbImportazioneTariffa.Enabled = (importaTariffa = VB_VERO)
    cmbImportazionePuntiVendita.Enabled = (importaPuntiVendita = VB_VERO)
    cmbImportazioneVenditoriEsterni.Enabled = (importaVenditoriEsterni = VB_VERO)
    cmbImportazioneOperatori.Enabled = (importaOperatori = VB_VERO)
    chkImportazioneCausaliProtezione.Enabled = (importaOperatori = VB_VERO And _
        idOrganizzazioneOperatori <> idNessunElementoSelezionato)
    cmbImportazioneCausaliProtezione.Enabled = False
    cmbImportazioneLayoutAlfresco.Enabled = (importaLayoutAlfresco = VB_VERO)
    'chkImportazioneProtezioniSuPosti.Enabled = (importaCausaliProtezione = VB_VERO And _
    '    isProdottoDefinitoSuMedesimaPianta)
    'cmbImportazioneProtezioniSuPosti.Enabled = False
    'SAREBBE NECESSARIA UNA FUNZIONE ANALOGA DI isProdottoDefinitoSuMedesimaPianta
    'CHE MI DICE SE GLI OPERATORI PER CUI SONO DEFAULT LE CAUSALI SONO PRESENTI
    'TRA QUELLI IMPORTANDI PER L'ORGANIZZAZIONE CHE SI STA CLONANDO
End Sub

Public Sub Init()
    Dim sql As String
    
    Call Variabili_Init
    sql = "SELECT IDORGANIZZAZIONE ID, CODICETERMINALELOTTO COD, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " ORDER BY COD"
    Call CaricaDallaBaseDati
    Call CaricaValoriLista(lstCodiciTLOrganizzazione, sql, "COD", "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub Variabili_Init()
    
    idOrganizzazioneTipiLayout = idNessunElementoSelezionato
    idOrganizzazioneTariffa = idNessunElementoSelezionato
    idOrganizzazionePuntiVendita = idNessunElementoSelezionato
    idOrganizzazioneVenditoriEsterni = idNessunElementoSelezionato
    idOrganizzazioneOperatori = idNessunElementoSelezionato
    idOrganizzazioneCausaliProtezione = idNessunElementoSelezionato
    idOrganizzazioneLayoutAlfresco = idNessunElementoSelezionato
    
    importaTipiLayout = VB_FALSO
    importaTariffa = VB_FALSO
    importaPuntiVendita = VB_FALSO
    importaVenditoriEsterni = VB_FALSO
    importaOperatori = VB_FALSO
    importaCausaliProtezione = VB_FALSO
    importaLayoutAlfresco = VB_FALSO
    
    newNome = ""
    newCodiceTLOrganizzazione = ""
    newDescrizione = ""
    newCodiceFiscalePartitaIVA = ""
    newCodiceFiscaleSocieta = ""
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtNome.Text = newNome
    txtDescrizione.Text = newDescrizione
    txtCodiceTLOrganizzazione.Text = newCodiceTLOrganizzazione
    txtCodiceFiscalePartitaIVA.Text = newCodiceFiscalePartitaIVA
    txtCodiceFiscaleSocieta.Text = newCodiceFiscaleSocieta
    
    chkImportazioneTipiLayout.Value = importaTipiLayout
    chkImportazioneTariffa.Value = importaTariffa
    chkImportazionePuntiVendita.Value = importaPuntiVendita
    chkImportazioneVenditoriEsterni.Value = importaVenditoriEsterni
    chkImportazioneOperatori.Value = importaOperatori
    chkImportazioneCausaliProtezione.Value = importaCausaliProtezione
        
    Call SelezionaElementoSuCombo(cmbImportazioneTipiLayout, idOrganizzazioneTipiLayout)
    Call SelezionaElementoSuCombo(cmbImportazioneTariffa, idOrganizzazioneTariffa)
    Call SelezionaElementoSuCombo(cmbImportazionePuntiVendita, idOrganizzazionePuntiVendita)
    Call SelezionaElementoSuCombo(cmbImportazioneVenditoriEsterni, idOrganizzazioneVenditoriEsterni)
    Call SelezionaElementoSuCombo(cmbImportazioneOperatori, idOrganizzazioneOperatori)
    Call SelezionaElementoSuCombo(cmbImportazioneCausaliProtezione, idOrganizzazioneCausaliProtezione)
    Call SelezionaElementoSuCombo(cmbImportazioneLayoutAlfresco, idOrganizzazioneLayoutAlfresco)
        
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdOrganizzazioneSelezionata(idO As Long)
    idOrganizzazioneSelezionata = idO
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    
    ValoriCampiOK = True
    Set listaNonConformitā = New Collection
    newNome = Trim(txtNome.Text)
    If ViolataUnicitā("ORGANIZZAZIONE", "NOME = " & SqlStringValue(newNome), "", _
        "", "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(newNome) & _
            " č giā presente in DB;")
    End If
    newDescrizione = Trim(txtDescrizione.Text)
    newCodiceTLOrganizzazione = UCase(Trim(txtCodiceTLOrganizzazione.Text))
    If ViolataUnicitā("ORGANIZZAZIONE", "CODICETERMINALELOTTO = " & SqlStringValue(newCodiceTLOrganizzazione), "", _
        "", "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice TL = " & SqlStringValue(newCodiceTLOrganizzazione) & _
            " č giā presente in DB;")
    End If
    newCodiceFiscalePartitaIVA = UCase(Trim(txtCodiceFiscalePartitaIVA.Text))
    newCodiceFiscaleSocieta = UCase(Trim(txtCodiceFiscaleSocieta.Text))
    If ViolataUnicitā("ORGANIZZAZIONE", "CODICEFISCALE = " & SqlStringValue(newCodiceFiscalePartitaIVA), "", _
        "", "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice fiscale/partita IVA = " & SqlStringValue(newCodiceFiscalePartitaIVA) & _
            " č giā presente in DB;")
    End If
    importaTipiLayout = IIf(chkImportazioneTipiLayout.Value = vbChecked, VB_VERO, VB_FALSO)
    importaTariffa = IIf(chkImportazioneTariffa.Value = vbChecked, VB_VERO, VB_FALSO)
    importaPuntiVendita = IIf(chkImportazionePuntiVendita.Value = vbChecked, VB_VERO, VB_FALSO)
    importaVenditoriEsterni = IIf(chkImportazioneVenditoriEsterni.Value = vbChecked, VB_VERO, VB_FALSO)
    importaOperatori = IIf(chkImportazioneOperatori.Value = vbChecked, VB_VERO, VB_FALSO)
    importaCausaliProtezione = IIf(chkImportazioneCausaliProtezione.Value = vbChecked, VB_VERO, VB_FALSO)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If
    
End Function

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT NOME, DESCRIZIONE, CODICEFISCALE, CODICETERMINALELOTTO" & _
        " FROM ORGANIZZAZIONE" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeOrganizzazioneSelezionata = rec("NOME")
'        newDescrizione = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
'        newCodiceTLOrganizzazione = rec("CODICETERMINALELOTTO")
'        newCodiceFiscalePartitaIVA = rec("CODICEFISCALE")
'        'da aggiungere qualche campo
'        idPiantaSelezionata = rec("IDP")
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub cmbImportazioneTipiLayout_Click()
    If Not internalEvent Then
        idOrganizzazioneTipiLayout = cmbImportazioneTipiLayout.ItemData(cmbImportazioneTipiLayout.ListIndex)
    End If
End Sub

Private Sub cmbImportazioneTariffa_Click()
    If Not internalEvent Then
        idOrganizzazioneTariffa = cmbImportazioneTariffa.ItemData(cmbImportazioneTariffa.ListIndex)
    End If
End Sub

Private Sub cmbImportazionePuntiVendita_Click()
    If Not internalEvent Then
        idOrganizzazionePuntiVendita = cmbImportazionePuntiVendita.ItemData(cmbImportazionePuntiVendita.ListIndex)
    End If
End Sub

Private Sub cmbImportazioneVenditoriEsterni_Click()
    If Not internalEvent Then
        idOrganizzazioneVenditoriEsterni = cmbImportazioneVenditoriEsterni.ItemData(cmbImportazioneVenditoriEsterni.ListIndex)
    End If
End Sub

Private Sub cmbImportazioneOperatori_Click()
    If Not internalEvent Then
        idOrganizzazioneOperatori = cmbImportazioneOperatori.ItemData(cmbImportazioneOperatori.ListIndex)
        Call SelezionaOrganizzazionePerCausaliProtezione
    End If
End Sub

Private Sub cmbImportazioneCausaliProtezione_Click()
    If Not internalEvent Then
        idOrganizzazioneCausaliProtezione = cmbImportazioneCausaliProtezione.ItemData(cmbImportazioneCausaliProtezione.ListIndex)
    End If
End Sub

Private Sub cmbImportazioneLayoutAlfresco_Click()
    If Not internalEvent Then
        idOrganizzazioneLayoutAlfresco = cmbImportazioneLayoutAlfresco.ItemData(cmbImportazioneLayoutAlfresco.ListIndex)
    End If
End Sub

Private Sub chkImportazioneCausaliProtezione_Click()
    If Not internalEvent Then
        Call chkImportazioneCausaliProtezione_Update
    End If
End Sub

Private Sub chkImportazioneLayoutAlfresco_Click()
    If Not internalEvent Then
        Call chkImportazioneLayoutAlfresco_Update
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
'    Call PulisciValoriCampi
    Call Variabili_Init
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
'    Dim listaViolazioniUnicitā As Collection
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim inserisciInformazioniSistemistiche As Boolean
    
'    Set listaViolazioniUnicitā = New Collection
    If ValoriCampiOK Then
'        Call CreaListaCampiValoriUnici
'        Set listaViolazioniUnicitā = listaDatiDuplicati("ORGANIZZAZIONE", _
'                                    listaCampiValoriUnici)
'        If listaViolazioniUnicitā.count = 0 Then
            inserisciInformazioniSistemistiche = False
            Call frmMessaggio.Visualizza("ConfermaInserimentoInformazioniSistemistiche")
            If frmMessaggio.exitCode = EC_CONFERMA Then
                inserisciInformazioniSistemistiche = True
            End If
            dataOraInizio = FormatDateTime(Now, vbGeneralDate)
            If IsClonazioneOrganizzazioneOK(idOrganizzazioneSelezionata, newNome, newDescrizione, _
                    newCodiceTLOrganizzazione, newCodiceFiscalePartitaIVA, newCodiceFiscaleSocieta, _
                    idOrganizzazioneTipiLayout, idOrganizzazioneTariffa, _
                    idOrganizzazionePuntiVendita, idOrganizzazioneVenditoriEsterni, _
                    idOrganizzazioneOperatori, idOrganizzazioneCausaliProtezione, _
                    idOrganizzazioneLayoutAlfresco, _
                    inserisciInformazioniSistemistiche) Then
                dataOraFine = FormatDateTime(Now, vbGeneralDate)
                Call frmMessaggio.Visualizza("NotificaClonazioneOrganizzazione", newNome, nomeOrganizzazioneSelezionata, dataOraInizio, dataOraFine)
            Else
                dataOraFine = FormatDateTime(Now, vbGeneralDate)
                Call frmMessaggio.Visualizza("ErroreClonazione", dataOraInizio, dataOraFine)
            End If
            Call Esci
'        Else
'            Call frmMessaggio.Visualizza("ErroreDuplicazioneDatiDBConstraint", _
'                        ArgomentoMessaggio(listaViolazioniUnicitā))
'        End If
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub PulisciValoriCampi()
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtCodiceTLOrganizzazione.Text = ""
    txtCodiceFiscalePartitaIVA.Text = ""
End Sub
'
'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & "'" & newNome & "'")
'    Call listaCampiValoriUnici.Add("CODICETERMINALELOTTO = " & "'" & newCodiceTLOrganizzazione & "'")
'    Call listaCampiValoriUnici.Add("CODICEFISCALE = " & "'" & newCodiceFiscalePartitaIVA & "'")
'End Sub

Private Sub lstCodiciTLOrganizzazione_Click()
    Call VisualizzaListBoxToolTip(lstCodiciTLOrganizzazione, lstCodiciTLOrganizzazione.Text)
End Sub

Private Sub txtCodiceFiscalePartitaIVA_Change()
    Call AggiornaAbilitazioneControlli
End Sub


Private Sub txtCodiceTLOrganizzazione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNome_Change()
'    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
'    End If
End Sub

Private Sub CaricaValoriLista(lst As ListBox, strSQL As String, NomeCampo1 As String, Optional nomeCampo2 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaVisualizzata As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            stringaVisualizzata = rec(NomeCampo1) & " - " & rec(nomeCampo2)
            lst.AddItem stringaVisualizzata
            lst.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Private Sub chkImportazioneTipiLayout_Click()
    If Not internalEvent Then
        Call chkImportazioneTipiLayout_Update
    End If
End Sub

Private Sub chkImportazioneTipiLayout_Update()
    If chkImportazioneTipiLayout.Value = vbChecked Then
        importaTipiLayout = VB_VERO
        Call cmbImportazioneTipiLayout.Clear
        Call CaricaListaOrganizzazioniTipiLayout
        Call SelezionaElementoSuCombo(cmbImportazioneTipiLayout, idOrganizzazioneSelezionata)
    ElseIf chkImportazioneTipiLayout.Value = vbUnchecked Then
        importaTipiLayout = VB_FALSO
        idOrganizzazioneTipiLayout = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaOrganizzazioniTipiLayout()
    Dim sql As String
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
'            " FROM ORGANIZZAZIONE" & _
'            " WHERE (IDSESSIONECONFIGURAZIONE IS NULL" & _
'            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
'            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            " ORDER BY NOME"
'    Else
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " ORDER BY NOME"
'    End If
    
    Call cmbImportazioneTipiLayout.Clear
    Call CaricaValoriCombo(cmbImportazioneTipiLayout, sql, "NOME")
End Sub

Private Sub chkImportazionePuntiVendita_Click()
    If Not internalEvent Then
        Call chkImportazionePuntiVendita_Update
    End If
End Sub

Private Sub chkImportazionePuntiVendita_Update()
    If chkImportazionePuntiVendita.Value = vbChecked Then
        importaPuntiVendita = VB_VERO
        Call cmbImportazionePuntiVendita.Clear
        Call CaricaListaOrganizzazioniPuntiVendita
        Call SelezionaElementoSuCombo(cmbImportazionePuntiVendita, idOrganizzazioneSelezionata)
    ElseIf chkImportazionePuntiVendita.Value = vbUnchecked Then
        importaPuntiVendita = VB_FALSO
        idOrganizzazionePuntiVendita = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaOrganizzazioniPuntiVendita()
    Dim sql As String
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
'            " FROM ORGANIZZAZIONE" & _
'            " WHERE (IDSESSIONECONFIGURAZIONE IS NULL" & _
'            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
'            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            " ORDER BY NOME"
'    Else
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " ORDER BY NOME"
'    End If
        
    Call cmbImportazionePuntiVendita.Clear
    Call CaricaValoriCombo(cmbImportazionePuntiVendita, sql, "NOME")
End Sub

Private Sub chkImportazioneOperatori_Click()
    If Not internalEvent Then
        Call chkImportazioneOperatori_Update
    End If
End Sub

Private Sub chkImportazioneOperatori_Update()
    If chkImportazioneOperatori.Value = vbChecked Then
        importaOperatori = VB_VERO
        Call cmbImportazioneOperatori.Clear
        Call CaricaListaOrganizzazioniOperatori
        Call SelezionaElementoSuCombo(cmbImportazioneOperatori, idOrganizzazioneSelezionata)
    ElseIf chkImportazioneOperatori.Value = vbUnchecked Then
        importaOperatori = VB_FALSO
        idOrganizzazioneOperatori = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaOrganizzazioniOperatori()
    Dim sql As String
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
'            " FROM ORGANIZZAZIONE" & _
'            " WHERE (IDSESSIONECONFIGURAZIONE IS NULL" & _
'            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
'            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            " ORDER BY NOME"
'    Else
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " ORDER BY NOME"
'    End If
    
    Call cmbImportazioneOperatori.Clear
    Call CaricaValoriCombo(cmbImportazioneOperatori, sql, "NOME")
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub
'
'Private Sub chkImportazioneModalitaPagamento_Click()
'    If Not internalEvent Then
'        Call chkImportazioneModalitaPagamento_Update
'    End If
'End Sub
'
'Private Sub chkImportazioneModalitaPagamento_Update()
'    If chkImportazioneModalitaPagamento.Value = vbChecked Then
'        importaModalitaPagamento = VB_VERO
'        Call cmbImportazioneModalitaPagamento.Clear
'        Call caricaListaOrganizzazioniModalitaPagamento
'    ElseIf chkImportazioneModalitaPagamento.Value = vbUnchecked Then
'        importaModalitaPagamento = VB_FALSO
'        idOrganizzazioneModalitaPagamento = idNessunElementoSelezionato
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub caricaListaOrganizzazioniModalitaPagamento()
'    Dim sql As String
'
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT MP.IDORGANIZZAZIONE ID,MP.NOME FROM ORGANIZZAZIONE_MODPAG_TIPOTERM OMT, MODALITADIPAGAMENTO MP" & _
'            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & " AND OMT.IDORGANIZZAZIONE = MP.IDORGANIZZAZIONE AND OMT.IDMODALITADIPAGAMENTO = MP.IDMODALITADIPAGAMENTO" & _
'            " AND (IDSESSIONECONFIGURAZIONE IS NULL" & _
'            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
'            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            " ORDER BY MP.NOME"
'    Else
'        sql = "SELECT MP.IDORGANIZZAZIONE ID,MP.NOME FROM ORGANIZZAZIONE_MODPAG_TIPOTERM OMT, MODALITADIPAGAMENTO MP" & _
'            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & " AND OMT.IDORGANIZZAZIONE = MP.IDORGANIZZAZIONE AND OMT.IDMODALITADIPAGAMENTO = MP.IDMODALITADIPAGAMENTO" & _
'            " ORDER BY MP.NOME"
'    End If
'
'    Call cmbImportazioneModalitaPagamento.Clear
'    Call CaricaValoriCombo(cmbImportazioneModalitaPagamento, sql, "NOME")
'
'End Sub
'
'Private Sub chkImportazioneRuoliServizio_Click()
'    If Not internalEvent Then
'        Call chkImportazioneRuoliServizio_Update
'    End If
'End Sub
'
'Private Sub chkImportazioneRuoliServizio_Update()
'    If chkImportazioneRuoliServizio.Value = vbChecked Then
'        importaRuoliServizio = VB_VERO
'        Call cmbImportazioneRuoliServizio.Clear
'        Call caricaListaOrganizzazioniRuoliServizio
'    ElseIf chkImportazioneRuoliServizio.Value = vbUnchecked Then
'        importaRuoliServizio = VB_FALSO
'        idOrganizzazioneRuoliServizio = idNessunElementoSelezionato
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub caricaListaOrganizzazioniRuoliServizio()
'    Dim sql As String
'
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT RDS.NOME,RDS.IDORGANIZZAZIONE ID FROM RUOLODISERVIZIO RDS" & _
'            " WHERE RDS.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND (IDSESSIONECONFIGURAZIONE IS NULL" & _
'            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
'            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            " ORDER BY RDS.NOME"
'    Else
'        sql = "SELECT RDS.NOME,RDS.IDORGANIZZAZIONE ID FROM RUOLODISERVIZIO RDS" & _
'            " WHERE RDS.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND IDPIANTA = " & idPiantaSelezionata & _
'            " ORDER BY RDS.NOME"
'    End If
'    Call cmbImportazioneRuoliServizio.Clear
'    Call CaricaValoriCombo(cmbImportazioneRuoliServizio, sql, "NOME")
'End Sub

Private Sub chkImportazioneCausaliProtezione_Update()
    If chkImportazioneCausaliProtezione.Value = vbChecked Then
        importaCausaliProtezione = VB_VERO
        idOrganizzazioneCausaliProtezione = cmbImportazioneCausaliProtezione.ItemData(cmbImportazioneCausaliProtezione.ListIndex)
        Call SelezionaElementoSuCombo(cmbImportazioneCausaliProtezione, idOrganizzazioneCausaliProtezione)
    ElseIf chkImportazioneCausaliProtezione.Value = vbUnchecked Then
        importaCausaliProtezione = VB_FALSO
        idOrganizzazioneCausaliProtezione = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkImportazioneLayoutAlfresco_Update()
    If chkImportazioneLayoutAlfresco.Value = vbChecked Then
        importaLayoutAlfresco = VB_VERO
        Call cmbImportazioneLayoutAlfresco.Clear
        Call CaricaListaOrganizzazioniLayoutAlfresco
        Call SelezionaElementoSuCombo(cmbImportazioneLayoutAlfresco, idOrganizzazioneSelezionata)
    ElseIf chkImportazioneLayoutAlfresco.Value = vbUnchecked Then
        importaLayoutAlfresco = VB_FALSO
        idOrganizzazioneLayoutAlfresco = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaOrganizzazioniCausaliProtezione()
    Dim sql As String
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " ORDER BY NOME"
    Call cmbImportazioneCausaliProtezione.Clear
    Call CaricaValoriCombo(cmbImportazioneCausaliProtezione, sql, "NOME")
End Sub

Private Sub CaricaListaOrganizzazioniLayoutAlfresco()
    Dim sql As String
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " ORDER BY NOME"
    Call cmbImportazioneLayoutAlfresco.Clear
    Call CaricaValoriCombo(cmbImportazioneLayoutAlfresco, sql, "NOME")
End Sub

'Private Sub SelezionaProdottoPerVenditoriEsterni()
'    If IdPiantaCausaliProtezione(idOrganizzazioneCausaliProtezione) = idPiantaSelezionata Then
'        isProdottoDefinitoSuMedesimaPianta = True
'        idOrganizzazioneVenditoriEsterni = idOrganizzazioneCausaliProtezione
'    Else
'        isProdottoDefinitoSuMedesimaPianta = False
'        idOrganizzazioneVenditoriEsterni = idNessunElementoSelezionato
'        importaVenditoriEsterni = VB_FALSO
'    End If
'    Call caricaListaOrganizzazioniVenditoriEsterni
'    Call SelezionaElementoSuCombo(cmbImportazioneVenditoriEsterni, idOrganizzazioneVenditoriEsterni)
'    idOrganizzazioneVenditoriEsterni = idNessunElementoSelezionato
'    importaVenditoriEsterni = VB_FALSO
'    Call AggiornaAbilitazioneControlli
'End Sub
'
Private Sub chkImportazioneTariffa_Click()
    If Not internalEvent Then
        Call chkImportazioneTariffa_Update
    End If
End Sub

Private Sub chkImportazioneTariffa_Update()
    If chkImportazioneTariffa.Value = vbChecked Then
        importaTariffa = VB_VERO
        Call cmbImportazioneTariffa.Clear
        Call CaricaListaOrganizzazioniTariffa
        Call SelezionaElementoSuCombo(cmbImportazioneTariffa, idOrganizzazioneSelezionata)
    ElseIf chkImportazioneTariffa.Value = vbUnchecked Then
        importaTariffa = VB_FALSO
        idOrganizzazioneTariffa = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaOrganizzazioniTariffa()
    Dim sql As String
    
    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " WHERE (IDSESSIONECONFIGURAZIONE IS NULL" & _
            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
            " ORDER BY NOME"
    Else
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " ORDER BY NOME"
    End If
    
    Call cmbImportazioneTariffa.Clear
    Call CaricaValoriCombo(cmbImportazioneTariffa, sql, "NOME")

End Sub

Private Sub chkImportazioneVenditoriEsterni_Click()
    If Not internalEvent Then
        Call chkImportazioneVenditoriEsterni_Update
    End If
End Sub

Private Sub chkImportazioneVenditoriEsterni_Update()
    If chkImportazioneVenditoriEsterni.Value = vbChecked Then
        importaVenditoriEsterni = VB_VERO
        Call cmbImportazioneVenditoriEsterni.Clear
        Call CaricaListaOrganizzazioniVenditoriEsterni
        Call SelezionaElementoSuCombo(cmbImportazioneVenditoriEsterni, idOrganizzazioneSelezionata)
    ElseIf chkImportazioneVenditoriEsterni.Value = vbUnchecked Then
        importaVenditoriEsterni = VB_FALSO
        idOrganizzazioneVenditoriEsterni = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaOrganizzazioniVenditoriEsterni()
    Dim sql As String
    
    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " WHERE (IDSESSIONECONFIGURAZIONE IS NULL" & _
            " OR (IDSESSIONECONFIGURAZIONE IS NOT NULL" & _
            " AND IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
            " ORDER BY NOME"
    Else
        sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
            " FROM ORGANIZZAZIONE" & _
            " ORDER BY NOME"
    End If
    
    Call cmbImportazioneVenditoriEsterni.Clear
    Call CaricaValoriCombo(cmbImportazioneVenditoriEsterni, sql, "NOME")
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Function IdPiantaCausaliProtezione(idOrganizzazione) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idOrganizzazione
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        rec.MoveFirst
        id = rec("IDPIANTA").Value
    rec.Close
    IdPiantaCausaliProtezione = id
    
    Call ChiudiConnessioneBD
    
End Function

Private Sub SelezionaOrganizzazionePerCausaliProtezione()
'    If IdPiantaCausaliProtezione(idProdottoCausaliProtezione) = idPiantaSelezionata Then
'        isProdottoDefinitoSuMedesimaPianta = True
'        idProdottoProtezioniSuPosto = idProdottoCausaliProtezione
'    Else
'        isProdottoDefinitoSuMedesimaPianta = False
'        idProdottoProtezioniSuPosto = idNessunElementoSelezionato
'        importaProtezioniSuPosti = VB_FALSO
'    End If
    Call CaricaListaOrganizzazioniCausaliProtezione
    Call SelezionaElementoSuCombo(cmbImportazioneCausaliProtezione, idOrganizzazioneOperatori)
    idOrganizzazioneCausaliProtezione = idNessunElementoSelezionato
    importaCausaliProtezione = VB_FALSO
    Call AggiornaAbilitazioneControlli
End Sub




