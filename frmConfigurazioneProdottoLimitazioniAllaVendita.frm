VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazioneProdottoLimitazioniAllaVendita 
   Caption         =   "Limitazioni alla vendita"
   ClientHeight    =   12120
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13455
   LinkTopic       =   "Form1"
   ScaleHeight     =   12120
   ScaleWidth      =   13455
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   24
      Top             =   11040
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   26
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   25
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame frmPuntiVenditaConfigurati 
      Caption         =   "Limitazioni configurate"
      Height          =   2775
      Left            =   120
      TabIndex        =   12
      Top             =   840
      Width           =   13095
      Begin VB.ListBox lstPuntiVenditaConfigurati 
         Height          =   2010
         Left            =   4680
         TabIndex        =   14
         Top             =   360
         Width           =   8295
      End
      Begin VB.ListBox lstClassiPV 
         Height          =   2010
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   4095
      End
   End
   Begin VB.Frame frmAggiornamento 
      Caption         =   "Aggiornamento"
      Height          =   6975
      Left            =   120
      TabIndex        =   9
      Top             =   3840
      Width           =   13095
      Begin VB.CommandButton cmdDisabilita 
         Caption         =   "Disabilita"
         Height          =   495
         Left            =   11640
         TabIndex        =   23
         Top             =   2160
         Width           =   1335
      End
      Begin VB.CommandButton cmdAbilita 
         Caption         =   "Abilita"
         Height          =   495
         Left            =   11640
         TabIndex        =   22
         Top             =   2760
         Width           =   1335
      End
      Begin VB.ListBox lstClassiPuntiVendita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1500
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   20
         Top             =   2880
         Width           =   3195
      End
      Begin VB.ListBox lstTariffe 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1500
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   18
         Top             =   720
         Width           =   3195
      End
      Begin VB.ListBox lstAreeSelezionate 
         Height          =   3660
         Left            =   4560
         Style           =   1  'Checkbox
         TabIndex        =   15
         Top             =   720
         Width           =   3375
      End
      Begin VB.CommandButton cmdAnnullaAggiornamento 
         Caption         =   "Annulla"
         Height          =   555
         Left            =   1320
         TabIndex        =   11
         Top             =   6240
         Width           =   1155
      End
      Begin VB.CommandButton cmdConfermaAggiornamento 
         Caption         =   "Conferma"
         Height          =   555
         Left            =   120
         TabIndex        =   10
         Top             =   6240
         Width           =   1155
      End
      Begin MSComctlLib.ImageList imlCompletezza 
         Left            =   4020
         Top             =   3840
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfigurazioneProdottoLimitazioniAllaVendita.frx":0000
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfigurazioneProdottoLimitazioniAllaVendita.frx":005E
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfigurazioneProdottoLimitazioniAllaVendita.frx":00BC
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwAreePianta 
         Height          =   4035
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   7117
         _Version        =   393217
         Indentation     =   706
         LabelEdit       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin VB.Label lblClassiPuntiVenditaLista 
         Caption         =   "Classi punti vendita"
         Height          =   195
         Left            =   8160
         TabIndex        =   21
         Top             =   2640
         Width           =   2955
      End
      Begin VB.Label lblTariffeLista 
         Caption         =   "Tariffe del prodotto"
         Height          =   195
         Left            =   8160
         TabIndex        =   19
         Top             =   480
         Width           =   2955
      End
      Begin VB.Label lblAreeSelezionate 
         Caption         =   "Aree selezionate"
         Height          =   195
         Left            =   4560
         TabIndex        =   17
         Top             =   360
         Width           =   3255
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   9240
      TabIndex        =   5
      Top             =   11040
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   11520
      TabIndex        =   1
      Top             =   360
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9780
      TabIndex        =   0
      Top             =   360
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   11520
      TabIndex        =   4
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   9780
      TabIndex        =   3
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle limitazioni alla vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   7935
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoLimitazioniAllaVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private listaOperatoriDisponibiliNonDiRicevitoria As Collection '+
Private listaOperatoriSelezionatiNonDiRicevitoria As Collection '+
Private listaAree As Collection

Private idClassePVSelezionata As Long
Private idClassePVDaModificareSelezionata As Long

Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum
Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
        
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Successivo"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    
    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    
'    Call initListaClassiPV
    Call PopolaLista(lstClassiPuntiVendita, "SELECT IDCLASSEPUNTOVENDITA ID, NOME FROM CLASSEPUNTOVENDITA ORDER BY IDCLASSEPUNTOVENDITA")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
'    Dim sql As String
'
'    Set utilizzoLayoutSupporto = New clsUsoLayout
'    isElementoSuGrigliaSelezionato = False
'    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'    Call utilizzoLayoutSupporto.inizializza(idProdottoSelezionato)
'    Call tvwAreePianta_Init
'    Call adcRiepilogo_Init
'    Call dgrRiepilogo_Init
'    Call CaricaGerarchiaAree
'    Call AssociaIconaANodi
'    Call AggiornaAbilitazioneControlli
'    nessunElementoSelezionato = False
'    Set listaAree = New Collection
'
'    Call Me.Show(vbModal)
        
End Sub
'
'Private Sub initListaClassiPV()
'    Dim sql As String
'    Dim rec As OraDynaset
'    Dim i As Long
'
'    sql = "SELECT IDCLASSEPUNTOVENDITA, NOME FROM CLASSEPUNTOVENDITA ORDER BY IDCLASSEPUNTOVENDITA"
'    Set rec = ORADB.CreateDynaset(sql, 0&)
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        i = 0
'        While Not rec.EOF
'            Call lstClassiPuntiVendita.AddItem(rec("NOME"), i)
'            lstClassiPuntiVendita.ItemData(i) = rec("IDCLASSEPUNTOVENDITA")
'            i = i + 1
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
'End Sub

Private Sub cmdAnnullaAggiornamento_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call AnnullaAggiornamento
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConfermaAggiornamento_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfermaAggiornamento
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    criteriSelezioneImpostati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
'        If isConfigurabile Then
'            Call SetGestioneExitCode(EC_CONFERMA)
'            Call AggiornaAbilitazioneControlli
'            Call InserisciNellaBaseDati
'            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, stringaNota, idProdottoSelezionato)
'            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'            Call AggiornaAbilitazioneControlli
'        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
'    Call AzionePercorsoGuidato(TNCP_FINE)
    Call CaricaFormModalitaFornitura
End Sub

Private Sub CaricaFormModalitaFornitura()
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoModalitaFornitura.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoModalitaFornitura.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoModalitaFornitura.Init
End Sub

Private Sub ClearListe()
End Sub

Private Sub AnnullaAggiornamento()
    ' se c'č qualcosa in sospeso viene annullato e si comincia una nuova transazione
    ORADB.Rollback
    ORADB.BeginTrans
    Call ClearListe
End Sub

Private Sub ConfermaAggiornamento()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            ' se c'č qualcosa in sospeso viene confermato e si comincia una nuova transazione
            ORADB.CommitTrans
            ORADB.BeginTrans
            Call ClearListe
            PopolaListaPuntiVenditaConfigurati
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub lstClassiPV_Click()
    Dim numeroElementiSelezionati As Long
    
    idClassePVSelezionata = lstClassiPV.ItemData(lstClassiPV.ListIndex)
    Call PopolaListaPuntiVenditaConfigurati
    
End Sub

' Popola la lista dei punti vendita configurati
Private Sub PopolaListaPuntiVenditaConfigurati()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim s As String

    Call lstPuntiVenditaConfigurati.Clear
   
    If idClassePVSelezionata = idNessunElementoSelezionato Then
        sql = "SELECT PV.NOME, C.NOME COMUNE, P.SIGLA, PPV.SUPERAREESENZALIMITAZIONI, PPV.SUPERAREEGESTIONESEPARATA" & _
            " FROM PUNTOVENDITA PV, PRODOTTO_PUNTOVENDITA PPV, COMUNE C, PROVINCIA P" & _
            " WHERE PPV.IDORGANIZZAZIONE = " & idProdottoSelezionato & _
            " AND PPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " AND PV.IDCOMUNE = C.IDCOMUNE (+)" & _
            " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
            " ORDER BY PV.NOME"
    Else
        sql = "SELECT PV.NOME, C.NOME COMUNE, P.SIGLA, PPV.SUPERAREESENZALIMITAZIONI, PPV.SUPERAREEGESTIONESEPARATA" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, PRODOTTO_PUNTOVENDITA PPV, COMUNE C, PROVINCIA P" & _
            " WHERE OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " AND OCPV.IDPUNTOVENDITA = PPV.IDPUNTOVENDITA" & _
            " AND PPV.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PV.IDCOMUNE = C.IDCOMUNE (+)" & _
            " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
            " ORDER BY PV.NOME"
    End If

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstPuntiVenditaConfigurati.AddItem(s, i)
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

End Sub

Private Sub PopolaListaPuntiVenditaSelezionati()

'    If lstComuniSelezionati.ListIndex <> idNessunElementoSelezionato Then
'        PopolaListePVSelezionatiConComune (lstComuniSelezionati.ItemData(lstComuniSelezionati.ListIndex))
'    ElseIf lstProvinceSelezionate.ListIndex <> idNessunElementoSelezionato Then
'        PopolaListePVSelezionatiConProvincia (lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex))
'    ElseIf lstRegioniSelezionate.ListIndex <> idNessunElementoSelezionato Then
'        PopolaListePVSelezionatiConRegione (lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex))
'    Else
'        PopolaListePVSelezionatiConClassePV (lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
'    End If
'
End Sub

Private Sub cmdAbilita_Click()
    Call Abilita
'    Call PopolaListaPuntiVenditaDisponibili
'    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub cmdDisabilita_Click()
    Call Disabilita
'    Call PopolaListaPuntiVenditaDisponibili
'    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub Abilita()
    Dim sql As String
    Dim n As Long
    Dim idPuntoVendita As Long
    Dim i As Long
    
'    For i = 0 To lstPuntiVenditaDisponibili.ListCount - 1
'        If lstPuntiVenditaDisponibili.Selected(i) = True Then
'            idPuntoVendita = lstPuntiVenditaDisponibili.ItemData(i)
'
'            If optInteroImpianto.Value = True Then
'                sql = "INSERT INTO PRODOTTO_PUNTOVENDITA (IDPRODOTTO, IDPUNTOVENDITA, SUPERAREESENZALIMITAZIONI, SUPERAREEGESTIONESEPARATA)" & _
'                    " VALUES (" & idProdottoSelezionato & ", " & idPuntoVendita & ", 1, 1)"
'            ElseIf optSuperAreeNormali.Value = True Then
'                sql = "INSERT INTO PRODOTTO_PUNTOVENDITA (IDPRODOTTO, IDPUNTOVENDITA, SUPERAREESENZALIMITAZIONI, SUPERAREEGESTIONESEPARATA)" & _
'                    " VALUES (" & idProdottoSelezionato & ", " & idPuntoVendita & ", 1, 0)"
'            ElseIf optSuperAreeGestioneSeparata.Value = True Then
'                sql = "INSERT INTO PRODOTTO_PUNTOVENDITA (IDPRODOTTO, IDPUNTOVENDITA, SUPERAREESENZALIMITAZIONI, SUPERAREEGESTIONESEPARATA)" & _
'                    " VALUES (" & idProdottoSelezionato & ", " & idPuntoVendita & ", 0, 1)"
'            End If
'            n = ORADB.ExecuteSQL(sql)
'        End If
'    Next i

End Sub

Private Sub Disabilita()
    Dim sql As String
    Dim n As Long
    Dim idPuntoVendita As Long
    Dim i As Long
    
'    For i = 0 To lstPuntiVenditaSelezionati.ListCount - 1
'        If lstPuntiVenditaSelezionati.Selected(i) = True Then
'            idPuntoVendita = lstPuntiVenditaSelezionati.ItemData(i)
'
'            sql = "DELETE FROM PRODOTTO_PUNTOVENDITA" & _
'                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND IDPUNTOVENDITA = " & idPuntoVendita
'            n = ORADB.ExecuteSQL(sql)
'        End If
'    Next i

End Sub

'''''''''''''''''''''''''''''''''

Private Sub tvwAreePianta_Click()
'    If Not internalEvent Then
'        If Not nessunElementoSelezionato Then
'            Call ElementoSelezionatoSuTreeView
'        End If
'    End If
'    nessunElementoSelezionato = False
End Sub

Private Sub ElementoSelezionatoSuTreeView()
    Dim sql As String
    Dim newArea As New clsAssocArea
    Dim idAreaOrigine As Long
    Dim mousePointerOld As Integer
    
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call GetIdRecordSelezionato
'
'    Call AggiungiAreaAListaAreeSelezionate(idRecordSelezionato)
'
'    MousePointer = mousePointerOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiungiAreaAListaAreeSelezionate(idArea As Long)
    Dim sql As String
    Dim rec As OraDynaset
    Dim listaIdAreeSelezionate As String
    Dim i As Long
        
    Call listaAree.Add(idArea)
    
    Call lstAreeSelezionate.Clear
    
    If listaAree.count > 0 Then
        listaIdAreeSelezionate = "(" & listaAree(1)
        For i = 2 To listaAree.count
            listaIdAreeSelezionate = listaIdAreeSelezionate & "," & listaAree(i)
        Next i
        listaIdAreeSelezionate = listaIdAreeSelezionate & ")"
    
        sql = "SELECT IDAREA ID_SA, CODICE CODICE_SA, NOME NOME_SA, NULL ID_A, NULL CODICE_A, NULL NOME_A, 1 SELEZIONATO" & _
            " FROM AREA" & _
            " WHERE IDAREA IN " & listaIdAreeSelezionate & _
            " AND IDTIPOAREA IN (4,5,6)"
        sql = sql & " UNION" & _
            " SELECT SA.IDAREA ID_SA, SA.CODICE CODICE_SA, SA.NOME NOME_SA, NULL ID_A, NULL CODICE_A, ' ' NOME_A, 0 SELEZIONATO" & _
            " FROM AREA A, AREA SA" & _
            " WHERE A.IDAREA IN " & listaIdAreeSelezionate & _
            " AND A.IDTIPOAREA IN (2,3)" & _
            " AND A.IDAREA_PADRE = SA.IDAREA" & _
            " AND SA.IDAREA NOT IN (" & _
            " SELECT IDAREA" & _
            " FROM AREA" & _
            " WHERE IDAREA IN " & listaIdAreeSelezionate & _
            " AND IDTIPOAREA IN (4,5,6)" & _
            " )"
        sql = sql & " UNION" & _
            " SELECT SA.IDAREA ID_SA, SA.CODICE CODICE_SA, SA.NOME NOME_SA, A.IDAREA ID_A, A.CODICE CODICE_A, A.NOME NOME_A, 1 SELEZIONATO" & _
            " FROM AREA A, AREA SA" & _
            " WHERE A.IDAREA IN " & listaIdAreeSelezionate & _
            " AND A.IDTIPOAREA IN (2,3)" & _
            " AND A.IDAREA_PADRE = SA.IDAREA" & _
            " AND SA.IDAREA NOT IN (" & _
            " SELECT IDAREA" & _
            " FROM AREA" & _
            " WHERE IDAREA IN " & listaIdAreeSelezionate & _
            " AND IDTIPOAREA IN (4,5,6)" & _
            " )" & _
            " ORDER BY NOME_SA, NOME_A"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 0
            While Not rec.EOF
                If IsNull(rec("ID_A")) Then
                    Call lstAreeSelezionate.AddItem(rec("NOME_SA"))
                    lstAreeSelezionate.ItemData(i) = rec("ID_SA")
                Else
                    Call lstAreeSelezionate.AddItem("    " & rec("NOME_A"))
                    lstAreeSelezionate.ItemData(i) = rec("ID_A")
                End If
                If rec("SELEZIONATO") = 1 Then
                    lstAreeSelezionate.Selected(i) = True
                Else
                    lstAreeSelezionate.Selected(i) = False
                End If
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
    End If
    
End Sub


