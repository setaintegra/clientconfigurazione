VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneSpettacolo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Spettacolo"
   ClientHeight    =   9915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9915
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDefault 
      Caption         =   "default"
      Height          =   375
      Left            =   7560
      TabIndex        =   44
      Top             =   8280
      Width           =   915
   End
   Begin VB.TextBox txtMinutiScaricoCEN 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6900
      MaxLength       =   2
      TabIndex        =   40
      Top             =   8280
      Width           =   435
   End
   Begin VB.TextBox txtOraScaricoCEN 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6300
      MaxLength       =   2
      TabIndex        =   39
      Top             =   8280
      Width           =   435
   End
   Begin VB.TextBox txtQuantitaIngressiVendibili 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3360
      MaxLength       =   6
      TabIndex        =   37
      Top             =   7680
      Width           =   795
   End
   Begin VB.CheckBox chkUtilizzatoriDaMantenere 
      Caption         =   "Utilizzatori da mantenere"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   7320
      Width           =   3375
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7860
      TabIndex        =   12
      Top             =   6240
      Width           =   435
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   35
      Top             =   240
      Width           =   3255
   End
   Begin VB.TextBox txtMinutiDurata 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3720
      MaxLength       =   2
      TabIndex        =   8
      Top             =   6180
      Width           =   435
   End
   Begin VB.TextBox txtOreDurata 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3000
      MaxLength       =   3
      TabIndex        =   7
      Top             =   6180
      Width           =   555
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   8340
      MultiSelect     =   2  'Extended
      TabIndex        =   16
      Top             =   6180
      Width           =   3435
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7860
      TabIndex        =   13
      Top             =   6660
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7860
      TabIndex        =   14
      Top             =   7080
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7860
      TabIndex        =   15
      Top             =   7500
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   4380
      MultiSelect     =   2  'Extended
      TabIndex        =   11
      Top             =   6180
      Width           =   3435
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   27
      Top             =   4860
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtOra 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1860
      MaxLength       =   2
      TabIndex        =   5
      Top             =   6180
      Width           =   435
   End
   Begin VB.TextBox txtMinuti 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2460
      MaxLength       =   2
      TabIndex        =   6
      Top             =   6180
      Width           =   435
   End
   Begin MSComCtl2.DTPicker dtpData 
      Height          =   315
      Left            =   120
      TabIndex        =   4
      Top             =   6180
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   556
      _Version        =   393216
      DateIsNull      =   -1  'True
      Format          =   16449537
      CurrentDate     =   37606
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   8880
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ComboBox cmbVenue 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   6840
      Width           =   4065
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10680
      TabIndex        =   19
      Top             =   9000
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneSpettacolo 
      Height          =   330
      Left            =   240
      Top             =   4080
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneSpettacolo 
      Height          =   3915
      Left            =   120
      TabIndex        =   21
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDataScaricoCEN 
      Height          =   315
      Left            =   4560
      TabIndex        =   41
      Top             =   8280
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   556
      _Version        =   393216
      DateIsNull      =   -1  'True
      Format          =   16449537
      CurrentDate     =   37606
   End
   Begin VB.Label lblDataScaricoCEN 
      Caption         =   "Data/ora scarico massivo verso CEN (Questure On Line)"
      Height          =   195
      Left            =   120
      TabIndex        =   43
      Top             =   8280
      Width           =   4395
   End
   Begin VB.Label lblSeparatoreOreMinutiScaricoCEN 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6780
      TabIndex        =   42
      Top             =   8280
      Width           =   75
   End
   Begin VB.Label lblQuantitaIngressiVendibili 
      Caption         =   "Quantita ingressi vendibili (prod. calendario)"
      Height          =   195
      Left            =   120
      TabIndex        =   38
      Top             =   7680
      Width           =   3075
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   36
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblDurata 
      Caption         =   "Durata (HH:MM)"
      Height          =   195
      Left            =   3000
      TabIndex        =   34
      Top             =   5940
      Width           =   1215
   End
   Begin VB.Label lblSeparatoreOreMinutiDurata 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3600
      TabIndex        =   33
      Top             =   6180
      Width           =   75
   End
   Begin VB.Label lblTurni 
      Alignment       =   2  'Center
      Caption         =   "TURNI RAPPRESENTAZIONE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5520
      TabIndex        =   32
      Top             =   5700
      Width           =   5115
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   4260
      TabIndex        =   31
      Top             =   5940
      Width           =   3555
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati"
      Height          =   195
      Left            =   8220
      TabIndex        =   30
      Top             =   5940
      Width           =   3555
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   29
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblSeparatoreOreMinuti 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2340
      TabIndex        =   26
      Top             =   6180
      Width           =   75
   End
   Begin VB.Label lblVenue 
      Caption         =   "Venue"
      Height          =   195
      Left            =   120
      TabIndex        =   25
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Label lblOra 
      Caption         =   "Ora (HH:MM)"
      Height          =   195
      Left            =   1860
      TabIndex        =   24
      Top             =   5940
      Width           =   1035
   End
   Begin VB.Label lblData 
      Caption         =   "Data"
      Height          =   195
      Left            =   120
      TabIndex        =   23
      Top             =   5940
      Width           =   1395
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Rappresentazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneSpettacolo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idVenueRapprSelezionata As Long
Private idSpettacoloSelezionato As Long
Private SQLCaricamentoCombo As String
Private dataOraRecordSelezionato As Date
Private dataOraScaricoCENRecordSelezionato As Date
Private durata As Long
Private controlloAccessiAbilitato As ValoreBooleanoEnum
Private nomeSpettacoloSelezionato As String
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private listaAppoggio As Collection
Private utilizzatoriDaMantenere As ValoreBooleanoEnum
Private quantitaIngressiVendibili As Long
Private spettacoloCalcio As Boolean

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()

    lblInfo1.Caption = "Spettacolo"
    txtInfo1.Text = nomeSpettacoloSelezionato
    txtInfo1.Enabled = False

    dgrConfigurazioneSpettacolo.Caption = "RAPPRESENTAZIONI CONFIGURATE"

    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneSpettacolo.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtOra.Text = ""
        txtMinuti.Text = ""
        txtOreDurata.Text = ""
        txtMinutiDurata.Text = ""
        txtQuantitaIngressiVendibili.Text = ""
        Call cmbVenue.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        
        dtpData.Enabled = False
        txtOra.Enabled = False
        txtMinuti.Enabled = False
        txtOreDurata.Enabled = False
        txtMinutiDurata.Enabled = False
        txtQuantitaIngressiVendibili.Enabled = False
        chkUtilizzatoriDaMantenere.Enabled = False
        dtpDataScaricoCEN.Enabled = False
        txtOraScaricoCEN.Enabled = False
        txtMinutiScaricoCEN.Enabled = False
        lblDataScaricoCEN.Enabled = False
        cmdDefault.Enabled = False
        
        lblData.Enabled = False
        lblOra.Enabled = False
        lblDurata.Enabled = False
        lblVenue.Enabled = False
        lblTurni.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        lblSeparatoreOreMinuti.Enabled = False
        lblSeparatoreOreMinutiDurata.Enabled = False
        lblQuantitaIngressiVendibili.Enabled = False
        
        cmbVenue.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False

    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneSpettacolo.Enabled = False
        dtpData.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtOra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtMinuti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        dtpDataScaricoCEN.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And spettacoloCalcio
        txtOraScaricoCEN.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And spettacoloCalcio
        txtMinutiScaricoCEN.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And spettacoloCalcio
        lblDataScaricoCEN.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And spettacoloCalcio
        cmdDefault.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And spettacoloCalcio
        txtOreDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtMinutiDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtQuantitaIngressiVendibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkUtilizzatoriDaMantenere.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbVenue.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblData.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblVenue.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTurni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSeparatoreOreMinuti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSeparatoreOreMinutiDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblQuantitaIngressiVendibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        If spettacoloCalcio Then
            cmdConferma.Enabled = (Len(txtOra.Text) = 2 And _
                                   Len(txtMinuti.Text) = 2 And _
                                   Len(txtOreDurata.Text) = 3 And _
                                   Len(txtMinutiDurata.Text) = 2 And _
                                   Len(txtOraScaricoCEN.Text) = 2 And _
                                   Len(txtMinutiScaricoCEN.Text) = 2 And _
                                   cmbVenue.ListIndex <> idNessunElementoSelezionato)
        Else
            cmdConferma.Enabled = (Len(txtOra.Text) = 2 And _
                                   Len(txtMinuti.Text) = 2 And _
                                   Len(txtOreDurata.Text) = 3 And _
                                   Len(txtMinutiDurata.Text) = 2 And _
                                   cmbVenue.ListIndex <> idNessunElementoSelezionato)
        End If
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneSpettacolo.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtOra.Text = ""
        txtMinuti.Text = ""
        txtOreDurata.Text = ""
        txtMinutiDurata.Text = ""
        txtQuantitaIngressiVendibili.Text = ""
        Call cmbVenue.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        
        dtpData.Enabled = False
        txtOra.Enabled = False
        txtMinuti.Enabled = False
        dtpDataScaricoCEN.Enabled = False
        txtOraScaricoCEN.Enabled = False
        txtMinutiScaricoCEN.Enabled = False
        lblDataScaricoCEN.Enabled = False
        cmdDefault.Enabled = False
        
        txtOreDurata.Enabled = False
        txtMinutiDurata.Enabled = False
        txtQuantitaIngressiVendibili.Enabled = False
        chkUtilizzatoriDaMantenere.Enabled = False
        
        lblData.Enabled = False
        lblOra.Enabled = False
        lblDurata.Enabled = False
        lblVenue.Enabled = False
        lblTurni.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        lblSeparatoreOreMinuti.Enabled = False
        lblSeparatoreOreMinutiDurata.Enabled = False
        lblQuantitaIngressiVendibili.Enabled = False
        
        cmbVenue.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False

    End If
    
End Sub

Private Sub Controlli_Init()

    lblInfo1.Caption = "Spettacolo"
    txtInfo1.Text = nomeSpettacoloSelezionato
    txtInfo1.Enabled = False
    lblData.Caption = "Data (GG/MM/AAAA)"
    lblOra.Caption = "Ora (HH.MM)"
    lblVenue.Caption = "Venue"
    lblTurni.Caption = "Turni Rappresentazione"
    lblDisponibili.Caption = "Disponibili"
    lblSelezionati.Caption = "Selezionati"
    lblQuantitaIngressiVendibili.Caption = "Quantita ingressi vendibili (prod. calendario)"

    lblIntestazioneForm.Caption = "Configurazione delle Rappresentazioni"
    dgrConfigurazioneSpettacolo.Caption = "RAPPRESENTAZIONI CONFIGURATE"

    dgrConfigurazioneSpettacolo.Enabled = True
    lblOperazioneInCorso.Caption = ""
    Call cmbVenue.Clear
    Call lstDisponibili.Clear
    Call lstSelezionati.Clear
    cmbVenue.Enabled = False
    cmdSelezionato.Enabled = False
    cmdDidsponibile.Enabled = False
    cmdSvuotaSelezionati.Enabled = False
    cmdInserisciNuovo.Enabled = True
    cmdInserisciDaSelezione.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdModifica.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdConferma.Enabled = False
    cmdAnnulla.Enabled = False

End Sub

Public Sub SetIdSpettacoloSelezionato(id As Long)
    idSpettacoloSelezionato = id
End Sub

Public Sub SetNomeSpettacoloSelezionato(nome As String)
    nomeSpettacoloSelezionato = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    stringaNota = "IDSPETTACOLO = " & idSpettacoloSelezionato

    If ValoriCampiOK Then
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_SPETTACOLO, CCDA_RAPPRESENTAZIONE, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneSpettacolo_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneSpettacolo_Init
            Case ASG_INSERISCI_DA_SELEZIONE
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_SPETTACOLO, CCDA_RAPPRESENTAZIONE, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneSpettacolo_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneSpettacolo_Init
            Case ASG_MODIFICA
                If IsRappresentazioneAggiornabile Then
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_SPETTACOLO, CCDA_RAPPRESENTAZIONE, stringaNota)
                Else
                    Call frmMessaggio.Visualizza("NotificaAggiornamentoRappresentazioneNonEseguita")
                End If
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneSpettacolo_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneSpettacolo_Init
            Case ASG_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_SPETTACOLO, CCDA_RAPPRESENTAZIONE, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneSpettacolo_Init
                Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                Call dgrConfigurazioneSpettacolo_Init
        End Select
        
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdDefault_Click()
    Call ImpostaDataScaricoDefault
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDVENUE AS ID, NOME FROM VENUE ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggio
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    sql = "SELECT IDVENUE AS ID, NOME FROM VENUE ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    sql = "SELECT IDVENUE AS ID, NOME FROM VENUE ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmbVenue_Click()
    If Not internalEvent Then
        idVenueRapprSelezionata = cmbVenue.ItemData(cmbVenue.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneSpettacolo.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDVENUE AS ID, NOME FROM VENUE ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
End Sub

Private Sub dgrConfigurazioneSpettacolo_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggio
    Call adcConfigurazioneSpettacolo_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneSpettacolo_Init
    Call dtpData_Init
    quantitaIngressiVendibili = 0
    spettacoloCalcio = isSpettacoloCalcio
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset

    Set rec = adcConfigurazioneSpettacolo.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneSpettacolo_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Call PopolaTabellaAppoggio

    Set d = adcConfigurazioneSpettacolo
    
    sql = "SELECT R.IDRAPPRESENTAZIONE AS ""ID""," & _
        " R.DATAORAINIZIO AS ""Inizio""," & _
        " R.IDSPETTACOLO, V.IDVENUE, V.NOME AS ""Venue""," & _
        " TMP.NOME AS ""Turni""" & _
        " FROM VENUE V, " & nomeTabellaTemporanea & " TMP, RAPPRESENTAZIONE R" & _
        " WHERE TMP.IDTURNORAPPRESENTAZIONE(+) = R.IDRAPPRESENTAZIONE" & _
        " AND V.IDVENUE = R.IDVENUE" & _
        " AND R.IDSPETTACOLO = " & idSpettacoloSelezionato & _
        " ORDER BY ""Inizio"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneSpettacolo.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub PopolaTabellaAppoggio()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim i As Integer
    Dim campoNome As String
    Dim idT As Long
    Dim nomeT As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggio = New Collection
    
    sql = "SELECT DISTINCT RAPPRESENTAZIONE_TURNORAPPR.IDRAPPRESENTAZIONE ID" & _
        " FROM SPETTACOLO, RAPPRESENTAZIONE, RAPPRESENTAZIONE_TURNORAPPR" & _
        " WHERE (RAPPRESENTAZIONE.IDRAPPRESENTAZIONE = " & _
        " RAPPRESENTAZIONE_TURNORAPPR.IDRAPPRESENTAZIONE) AND " & _
        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
        " (SPETTACOLO.IDSPETTACOLO = " & idSpettacoloSelezionato & ")"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("ID").Value
            Call listaAppoggio.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggio
        campoNome = ""
        sql = "SELECT RAPPRESENTAZIONE.IDRAPPRESENTAZIONE," & _
            " RAPPRESENTAZIONE.IDSPETTACOLO," & _
            " TURNORAPPRESENTAZIONE.IDTURNORAPPRESENTAZIONE," & _
            " TURNORAPPRESENTAZIONE.NOME FROM" & _
            " TURNORAPPRESENTAZIONE, RAPPRESENTAZIONE, RAPPRESENTAZIONE_TURNORAPPR WHERE" & _
            " (RAPPRESENTAZIONE.IDRAPPRESENTAZIONE = RAPPRESENTAZIONE_TURNORAPPR.IDRAPPRESENTAZIONE) AND" & _
            " (TURNORAPPRESENTAZIONE.IDTURNORAPPRESENTAZIONE = RAPPRESENTAZIONE_TURNORAPPR.IDTURNORAPPRESENTAZIONE) AND" & _
            " (RAPPRESENTAZIONE.IDRAPPRESENTAZIONE= " & recordTemporaneo.idElementoLista & ") AND" & _
            " (RAPPRESENTAZIONE.IDSPETTACOLO)= " & idSpettacoloSelezionato
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("NOME"), campoNome & "; " & rec("NOME"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, , adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sł
    For Each recordTemporaneo In listaAppoggio
        idT = recordTemporaneo.idElementoLista
        nomeT = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDTURNORAPPRESENTAZIONE, NOME)" & _
            " VALUES (" & idT & ", " & SqlStringValue(Trim(nomeT)) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub CreaTabellaAppoggio()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_TURNI_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDTURNORAPPRESENTAZIONE NUMBER(10), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggio
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggio()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT IDVENUE, DATAORAINIZIO, DURATAINMINUTI, UTILIZZATORIDAMANTENERE, QUANTITAINGRESSIVENDIBILI, DATAORASCARICOQUESTUREONLINE"
    sql = sql & " FROM RAPPRESENTAZIONE"
    sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        idVenueRapprSelezionata = rec("IDVENUE")
        dataOraRecordSelezionato = rec("DATAORAINIZIO")
        durata = rec("DURATAINMINUTI")
        utilizzatoriDaMantenere = rec("UTILIZZATORIDAMANTENERE")
        quantitaIngressiVendibili = IIf(IsNull(rec("QUANTITAINGRESSIVENDIBILI")), valoreLongNullo, rec("QUANTITAINGRESSIVENDIBILI"))
        dataOraScaricoCENRecordSelezionato = IIf(IsNull(rec("DATAORASCARICOQUESTUREONLINE")), dataNulla, rec("DATAORASCARICOQUESTUREONLINE"))
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim s As String

    internalEventOld = internalEvent
    internalEvent = True

    Call SelezionaElementoSuCombo(cmbVenue, idVenueRapprSelezionata)
    dtpData.Value = dataOraRecordSelezionato
    txtOra.Text = StringaOraMinuti(CStr(Hour(dataOraRecordSelezionato)))
    txtMinuti.Text = StringaOraMinuti(CStr(Minute(dataOraRecordSelezionato)))
    s = StringaOraMinuti(Int(durata / 60))
    If Len(s) = 3 Then
        txtOreDurata.Text = s
    Else
        txtOreDurata.Text = "0" & s
    End If
    txtMinutiDurata.Text = StringaOraMinuti(durata Mod 60)
    txtQuantitaIngressiVendibili.Text = IIf(quantitaIngressiVendibili = valoreLongNullo, "", quantitaIngressiVendibili)
    chkUtilizzatoriDaMantenere.Value = utilizzatoriDaMantenere

    If dataOraScaricoCENRecordSelezionato = dataNulla Then
        dtpDataScaricoCEN.Value = 0
        txtOraScaricoCEN.Text = ""
        txtMinutiScaricoCEN.Text = ""
    Else
        dtpDataScaricoCEN.Value = dataOraScaricoCENRecordSelezionato
        txtOraScaricoCEN.Text = StringaOraMinuti(CStr(Hour(dataOraScaricoCENRecordSelezionato)))
        txtMinutiScaricoCEN.Text = StringaOraMinuti(CStr(Minute(dataOraScaricoCENRecordSelezionato)))
    End If
    
    internalEvent = internalEventOld

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim rappresentazioneEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    rappresentazioneEliminabile = True
    
    If Not IsRecordEliminabile("IDRAPPRESENTAZIONE", "PRODOTTO_RAPPRESENTAZIONE", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PRODOTTO_RAPPRESENTAZIONE")
        rappresentazioneEliminabile = False
    End If
    
    If rappresentazioneEliminabile Then
        sql = "DELETE FROM RAPPRESENTAZIONE_TURNORAPPR" & _
            " WHERE IDRAPPRESENTAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, , adCmdText
        sql = "DELETE FROM RAPPRESENTAZIONE" & _
            " WHERE IDRAPPRESENTAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, , adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Tariffa selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub dgrConfigurazioneSpettacolo_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneSpettacolo
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Visible = False
    g.Columns(3).Visible = False
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim ora As String
    Dim minuti As String
    Dim oraScarico As String
    Dim minutiScarico As String
    Dim secondiScarico As String
    Dim oreDurata As String
    Dim minutiDurata As String
    Dim secondi As String
    Dim dataInizio As Date
    Dim dataScarico As Date
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    
    secondi = "00"
    secondiScarico = "00"
    dataInizio = FormatDateTime(dtpData.Value, vbShortDate)
    If IsNull(dtpDataScaricoCEN.Value) Then
        dataScarico = dataNulla
    Else
        dataScarico = FormatDateTime(dtpDataScaricoCEN.Value, vbShortDate)
    End If
    Set listaNonConformitą = New Collection
    
    utilizzatoriDaMantenere = chkUtilizzatoriDaMantenere.Value
    
    If IsCampoOraCorretto(txtOra) Then
        ora = CStr(Trim(txtOra.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Inizio ore deve essere numerico di tipo intero e compreso tra 0 e 23;")
    End If
    If IsCampoMinutiCorretto(txtMinuti) Then
        minuti = CStr(Trim(txtMinuti.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Inizio minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    
    If spettacoloCalcio Then
        If IsCampoOraCorretto(txtOraScaricoCEN) Then
            oraScarico = CStr(Trim(txtOraScaricoCEN.Text))
        Else
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore immesso sul campo Ora Scarico CEN deve essere numerico di tipo intero e compreso tra 0 e 23;")
        End If
        If IsCampoMinutiCorretto(txtMinutiScaricoCEN) Then
            minutiScarico = CStr(Trim(txtMinutiScaricoCEN.Text))
        Else
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore immesso sul campo Minuti Scarico CEN deve essere numerico di tipo intero e compreso tra 0 e 59;")
        End If
    End If
    
    If IsCampoInteroCorretto(txtOreDurata) Then
        oreDurata = CStr(Trim(txtOreDurata.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Durata ore deve essere numerico di tipo intero;")
    End If
    
    If IsCampoMinutiCorretto(txtMinutiDurata) Then
        minutiDurata = CStr(Trim(txtMinutiDurata.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Durata minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    
    If IsCampoInteroCorretto(txtQuantitaIngressiVendibili) Then
        quantitaIngressiVendibili = CLng(txtQuantitaIngressiVendibili.Text)
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Quantitą ingressi vendibili deve essere numerico di tipo intero;")
    End If
    
    durata = (oreDurata * 60) + minutiDurata
    dataOraRecordSelezionato = FormatDateTime(dataInizio & " " & ora & ":" & minuti & ":" & secondi, vbGeneralDate)
    If Not IsNull(dataScarico) Then
        dataOraScaricoCENRecordSelezionato = FormatDateTime(dataScarico & " " & oraScarico & ":" & minutiScarico & ":" & secondiScarico, vbGeneralDate)
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtOra_Change()
    If Not internalEvent Then
        If Len(txtOra.Text) = 2 Then
            txtMinuti.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinuti_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtOraScaricoCEN_Change()
'    If Not internalEvent Then
'        If Len(txtOraScaricoCEN.Text) = 2 Then
'            txtMinutiScaricoCEN.SetFocus
'        End If
'        Call AggiornaAbilitazioneControlli
'    End If
End Sub

Private Sub txtMinutiScaricoCEN_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtOreDurata_Change()
    If Not internalEvent Then
        If Len(txtOreDurata.Text) = 3 Then
            txtMinutiDurata.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiDurata_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub dtpData_Init()
    dtpData.Value = Now
    
    If spettacoloCalcio Then
        dtpDataScaricoCEN.Value = "31-12-2015"
        txtOraScaricoCEN = "23"
        txtMinutiScaricoCEN = "59"
    End If

End Sub

Private Sub dtpData_Click()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dtpDataScaricoCEN_Click()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Function IsRappresentazioneAggiornabile() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim aggiornabile As Boolean
    
    Call ApriConnessioneBD
    
    aggiornabile = False
'    a) tutti i prodotti correlati devono essere liberi, cioč non lockati da altro utente
'    b) tutti i prodotti correlati devono essere in stato "in configurazione"
    sql = " SELECT DISTINCT P.NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR" & _
        " WHERE P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE =  " & idRecordSelezionato & _
        " AND PR.IDPRODOTTO IN (SELECT IDPRODOTTO FROM CC_PRODOTTOUTILIZZATO)" & _
        " UNION" & _
        " SELECT DISTINCT P.NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR" & _
        " WHERE P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE =  " & idRecordSelezionato & _
        " AND P.IDTIPOSTATOPRODOTTO <> " & TSP_IN_CONFIGURAZIONE
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If (rec.BOF And rec.EOF) Then
        aggiornabile = True
    End If
    
    Call ChiudiConnessioneBD
    
    IsRappresentazioneAggiornabile = aggiornabile
End Function

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTurno As String
    Dim turnoRappresentazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    If idRecordSelezionato >= 0 Then
        sql = "SELECT T.IDTURNORAPPRESENTAZIONE IDT, T.NOME" & _
            " FROM TURNORAPPRESENTAZIONE T, RAPPRESENTAZIONE_TURNORAPPR R" & _
            " WHERE T.IDTURNORAPPRESENTAZIONE = R.IDTURNORAPPRESENTAZIONE(+)" & _
            " AND R.IDRAPPRESENTAZIONE(+) = " & idRecordSelezionato & _
            " AND R.IDTURNORAPPRESENTAZIONE IS NULL" & _
            " ORDER BY NOME"
    Else
        sql = "SELECT IDTURNORAPPRESENTAZIONE IDT, NOME FROM TURNORAPPRESENTAZIONE" & _
            " ORDER BY NOME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set turnoRappresentazioneCorrente = New clsElementoLista
            turnoRappresentazioneCorrente.nomeElementoLista = rec("NOME")
            turnoRappresentazioneCorrente.descrizioneElementoLista = rec("NOME")
            turnoRappresentazioneCorrente.idElementoLista = rec("IDT").Value
            chiaveTurno = ChiaveId(turnoRappresentazioneCorrente.idElementoLista)
            Call listaDisponibili.Add(turnoRappresentazioneCorrente, chiaveTurno)
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim turno As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear
    
    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each turno In listaDisponibili
            lstDisponibili.AddItem turno.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = turno.idElementoLista
            i = i + 1
        Next turno
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTurno As String
    Dim turnoRappresentazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD

    Set listaSelezionati = New Collection
        
    If idRecordSelezionato >= 0 Then
        sql = "SELECT RT.IDRAPPRESENTAZIONE, T.IDTURNORAPPRESENTAZIONE IDT, T.NOME FROM" & _
            " TURNORAPPRESENTAZIONE T, RAPPRESENTAZIONE R, RAPPRESENTAZIONE_TURNORAPPR RT" & _
            " WHERE R.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE" & _
            " AND T.IDTURNORAPPRESENTAZIONE = RT.IDTURNORAPPRESENTAZIONE" & _
            " AND RT.IDRAPPRESENTAZIONE = " & idRecordSelezionato & _
            " ORDER BY NOME"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set turnoRappresentazioneCorrente = New clsElementoLista
                turnoRappresentazioneCorrente.nomeElementoLista = rec("NOME")
                turnoRappresentazioneCorrente.descrizioneElementoLista = rec("NOME")
                turnoRappresentazioneCorrente.idElementoLista = rec("IDT").Value
                chiaveTurno = ChiaveId(turnoRappresentazioneCorrente.idElementoLista)
                Call listaSelezionati.Add(turnoRappresentazioneCorrente, chiaveTurno)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ChiudiConnessioneBD
        
        Call lstSelezionati_Init
    Else
        'do Nothing
    End If
    
End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim turno As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each turno In listaSelezionati
            lstSelezionati.AddItem turno.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = turno.idElementoLista
            i = i + 1
        Next turno
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SvuotaDisponibili()
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For Each turno In listaDisponibili
        chiaveTurno = ChiaveId(turno.idElementoLista)
        Call listaSelezionati.Add(turno, chiaveTurno)
    Next turno
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SvuotaSelezionati()
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For Each turno In listaSelezionati
        chiaveTurno = ChiaveId(turno.idElementoLista)
        Call listaDisponibili.Add(turno, chiaveTurno)
    Next turno
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idTurno As Long
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idTurno = lstSelezionati.ItemData(i - 1)
            chiaveTurno = ChiaveId(idTurno)
            Set turno = listaSelezionati.Item(chiaveTurno)
            Call listaDisponibili.Add(turno, chiaveTurno)
            Call listaSelezionati.Remove(chiaveTurno)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idTurno As Long
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idTurno = lstDisponibili.ItemData(i - 1)
            chiaveTurno = ChiaveId(idTurno)
            Set turno = listaDisponibili.Item(chiaveTurno)
            Call listaSelezionati.Add(turno, chiaveTurno)
            Call listaDisponibili.Remove(chiaveTurno)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim idNuovaRappresentazione As Long
    Dim n As Long
    Dim turno As clsElementoLista
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovaRappresentazione = OttieniIdentificatoreDaSequenza("SQ_RAPPRESENTAZIONE")
    
'   INSERIMENTO IN TABELLA RAPPRESENTAZIONE
    sql = "INSERT INTO RAPPRESENTAZIONE (IDRAPPRESENTAZIONE, DATAORAINIZIO, CONTROLLOACCESSIABILITATO,"
    sql = sql & " DURATAINMINUTI, IDVENUE, IDSPETTACOLO, UTILIZZATORIDAMANTENERE, QUANTITAINGRESSIVENDIBILI,"
    sql = sql & " DATAORASCARICOQUESTUREONLINE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaRappresentazione & ", "
    sql = sql & SqlDateTimeValue(dataOraRecordSelezionato) & ", "
    sql = sql & VB_FALSO & ", "
    sql = sql & durata & ", "
    sql = sql & idVenueRapprSelezionata & ", "
    sql = sql & idSpettacoloSelezionato & ", "
    sql = sql & utilizzatoriDaMantenere & ", "
    If quantitaIngressiVendibili = valoreLongNullo Then
        sql = sql & "NULL, "
    Else
        sql = sql & quantitaIngressiVendibili & ", "
    End If
    If spettacoloCalcio Then
        sql = sql & SqlDateTimeValue(dataOraScaricoCENRecordSelezionato) & ")"
    Else
        sql = sql & "NULL)"
    End If
    SETAConnection.Execute sql, n, adCmdText
    
'   INSERIMENTO IN TABELLA RAPPRESENTAZIONE_TURNORAPPR
    If Not (listaSelezionati Is Nothing) Then
        For Each turno In listaSelezionati
            sql = "INSERT INTO RAPPRESENTAZIONE_TURNORAPPR (IDTURNORAPPRESENTAZIONE,"
            sql = sql & " IDRAPPRESENTAZIONE)"
            sql = sql & " VALUES (" & turno.idElementoLista & ", "
            sql = sql & idNuovaRappresentazione & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next turno
    End If
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaRappresentazione)
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim turno As clsElementoLista

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Call SETAConnection.BeginTrans
    If ProdottiCorrelandiBloccati(idRecordSelezionato, idNessunElementoSelezionato) Then
        sql = "UPDATE RAPPRESENTAZIONE SET"
        sql = sql & " DATAORAINIZIO = " & SqlDateTimeValue(dataOraRecordSelezionato) & ","
        sql = sql & " DURATAINMINUTI = " & durata & ","
        sql = sql & " CONTROLLOACCESSIABILITATO = " & VB_FALSO & ","
        sql = sql & " IDVENUE = " & idVenueRapprSelezionata & ","
        sql = sql & " UTILIZZATORIDAMANTENERE = " & utilizzatoriDaMantenere & ","
        If quantitaIngressiVendibili = valoreLongNullo Then
            sql = sql & " QUANTITAINGRESSIVENDIBILI = NULL,"
        Else
            sql = sql & " QUANTITAINGRESSIVENDIBILI = " & quantitaIngressiVendibili & ","
        End If
        If spettacoloCalcio Then
            sql = sql & " DATAORASCARICOQUESTUREONLINE = " & SqlDateTimeValue(dataOraScaricoCENRecordSelezionato) & ","
        Else
            sql = sql & " DATAORASCARICOQUESTUREONLINE = NULL,"
        End If
        sql = sql & " IDSPETTACOLO = " & idSpettacoloSelezionato
        sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        If Not (listaSelezionati Is Nothing) Then
            sql = "DELETE FROM RAPPRESENTAZIONE_TURNORAPPR"
            sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
            For Each turno In listaSelezionati
                sql = "INSERT INTO RAPPRESENTAZIONE_TURNORAPPR (IDTURNORAPPRESENTAZIONE,"
                sql = sql & " IDRAPPRESENTAZIONE)"
                sql = sql & " VALUES (" & turno.idElementoLista & ", "
                sql = sql & idRecordSelezionato & ")"
                SETAConnection.Execute sql, n, adCmdText
            Next turno
        End If
        Call SETAConnection.CommitTrans
    Else
        Call SETAConnection.RollbackTrans
        Call frmMessaggio.Visualizza("NotificaAggiornamentoRappresentazioneNonEseguita")
    End If
    Call SbloccaProdottiCorrelandi(getNomeMacchina, idNessunElementoSelezionato)
        
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Function isSpettacoloCalcio() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Integer

    Call ApriConnessioneBD

    sql = "SELECT COUNT(*) CONT" & _
        " FROM SPETTACOLO_GENERESIAE" & _
        " WHERE IDSPETTACOLO = " & idSpettacoloSelezionato & _
        " AND IDGENERESIAE IN (2, 3)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close

    Call ChiudiConnessioneBD

    isSpettacoloCalcio = (cont > 0)
End Function

Private Sub ImpostaDataScaricoDefault()
    dtpDataScaricoCEN.Value = "31-12-2015"
    txtOraScaricoCEN = "23"
    txtMinutiScaricoCEN = "59"
End Sub
