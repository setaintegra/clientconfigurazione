VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfigurazioneProdottoTipoStampante 
   Caption         =   "Configurazione abilitazioni tipo stampante"
   ClientHeight    =   7170
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   ScaleHeight     =   7170
   ScaleWidth      =   11595
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Importazione da file Excel"
      Height          =   3375
      Left            =   7680
      TabIndex        =   13
      Top             =   1920
      Width           =   3135
      Begin VB.CommandButton cmdSelezionaFile 
         Caption         =   "Importa configurazioni  da ..."
         Height          =   495
         Left            =   120
         TabIndex        =   15
         Top             =   2640
         Width           =   2835
      End
      Begin MSComDlg.CommonDialog cdlFileImport 
         Left            =   2520
         Top             =   600
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label Label6 
         Caption         =   "I terminali non presenti nel file manterranno la configurazione corrente"
         Height          =   615
         Left            =   120
         TabIndex        =   18
         Top             =   2040
         Width           =   2895
      End
      Begin VB.Label Label5 
         Caption         =   "- colonna B TERMICA/LASER; qualsiasi altro valore della colonna (anche stringa vuota) elimina l'associazione corrente"
         Height          =   855
         Left            =   120
         TabIndex        =   17
         Top             =   960
         Width           =   2535
      End
      Begin VB.Label Label4 
         Caption         =   "- colonna A codice terminale"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   2535
      End
      Begin VB.Label Label3 
         Caption         =   "Il file Excel prevede due colonne:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   2535
      End
   End
   Begin VB.CommandButton cmdImpostaLaser 
      Caption         =   "LASER"
      Height          =   435
      Left            =   4440
      TabIndex        =   11
      Top             =   3720
      Width           =   1275
   End
   Begin VB.CommandButton cmdImpostaTermica 
      Caption         =   "TERMICA"
      Height          =   435
      Left            =   4440
      TabIndex        =   10
      Top             =   3120
      Width           =   1275
   End
   Begin VB.ListBox lstTerminali 
      Height          =   3570
      Left            =   240
      MultiSelect     =   2  'Extended
      TabIndex        =   8
      Top             =   1920
      Width           =   3855
   End
   Begin VB.CheckBox chkLetturaCodiceABarrePrevista 
      Alignment       =   1  'Right Justify
      Caption         =   "Lettura codice a barre prevista"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   960
      Width           =   2775
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10200
      TabIndex        =   3
      Top             =   6600
      Width           =   1275
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9780
      TabIndex        =   2
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8040
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   435
      Left            =   5160
      TabIndex        =   0
      Top             =   6600
      Width           =   1275
   End
   Begin VB.Label Label2 
      Caption         =   "L'impostazione e' immediatamente salvata su DB"
      Height          =   615
      Left            =   4440
      TabIndex        =   12
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Terminali rete LISTICKET"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1560
      Width           =   3855
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9780
      TabIndex        =   6
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8040
      TabIndex        =   5
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Abilitazioni tipo stampante"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   6375
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoTipoStampante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private internalEvent As Boolean

Private prodottoRientraDecretoSicurezza As ValoreBooleanoEnum
Private numeroMassimoTitoliPerAcqProdotto As Long

Private stringaListaSuperareeSelezionate As String
Private quantitaSuperareeSelezionate As Long

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
End Sub

Public Sub Init(idPianta As Long, idProdotto As Long, nomeProdotto As String, idOrganizzazione As Long, nomeOrganizzazione As String)

    idPiantaSelezionata = idPianta
    idProdottoSelezionato = idProdotto
    nomeProdottoSelezionato = nomeProdotto
    idOrganizzazioneSelezionata = idOrganizzazione
    nomeOrganizzazioneSelezionata = nomeOrganizzazione

    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValori
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    prodottoRientraDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMassimoTitoliPerAcqProdotto = n
End Sub

Private Sub CaricaValori()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim chiaveElemento As String
    Dim elemento As clsElementoLista
    
    Call ApriConnessioneBD_ORA

    sql = "SELECT LETTURACODICEABARREPREVISTA" & _
            " FROM PRODOTTO" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("LETTURACODICEABARREPREVISTA") = 1 Then
                chkLetturaCodiceABarrePrevista.Value = Checked
            Else
                chkLetturaCodiceABarrePrevista.Value = vbUnchecked
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call PopolaListaTerminali
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call InserisciNellaBaseDati
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idSuperarea As Long
    Dim idTemplate As Long
    Dim i As Long
    Dim j As Long
    
On Error GoTo errori_TipoStampante_InserisciNellaBaseDati

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans

    If chkLetturaCodiceABarrePrevista.Value = Checked Then
        sql = "UPDATE PRODOTTO SET LETTURACODICEABARREPREVISTA = 1 WHERE IDPRODOTTO = " & idProdottoSelezionato
    Else
        sql = "UPDATE PRODOTTO SET LETTURACODICEABARREPREVISTA = 0 WHERE IDPRODOTTO = " & idProdottoSelezionato
    End If
    n = ORADB.ExecuteSQL(sql)
            
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA

    Call AggiornaAbilitazioneControlli

    Exit Sub

errori_TipoStampante_InserisciNellaBaseDati:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdImpostaTermica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ImpostaTipoStampante(TS_TERMICA)
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdImpostaLaser_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ImpostaTipoStampante(TS_LASER)
    
    MousePointer = mousePointerOld

End Sub

Private Sub ImpostaTipoStampante(tipoStampante As TipoStampanteEnum)
    Dim sql As String
    Dim n As Long
    Dim i As Integer
    Dim idTerminale As Long
        
On Error GoTo errori_TipoStampante_ImpostaTipoStampante

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans

    For i = 1 To lstTerminali.ListCount
        If lstTerminali.Selected(i - 1) Then
            idTerminale = lstTerminali.ItemData(i - 1)
            sql = "DELETE FROM PROD_TERM_TIPOSTAMPANTE WHERE IDPRODOTTO = " & idProdottoSelezionato & " And idTerminale = " & idTerminale
            n = ORADB.ExecuteSQL(sql)
            sql = "INSERT INTO PROD_TERM_TIPOSTAMPANTE (IDPRODOTTO, IDTERMINALE, IDTIPOSTAMPANTE) VALUES (" & idProdottoSelezionato & ", " & idTerminale & ", " & tipoStampante & ")"
            n = ORADB.ExecuteSQL(sql)
        End If
    Next i
    
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    
    Call PopolaListaTerminali

    Call AggiornaAbilitazioneControlli

    Exit Sub

errori_TipoStampante_ImpostaTipoStampante:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub PopolaListaTerminali()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long

    lstTerminali.Clear
    
    sql = "SELECT DISTINCT TERM.IDTERMINALE, TERM.CODICE, TS.NOME" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESAPROD_PUNTOVENDITA CSP_PV, PRODOTTO P, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, TERMINALE TERM, PROD_TERM_TIPOSTAMPANTE PTTS, TIPOSTAMPANTE TS" & _
            " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND P.IDPRODOTTO = CSP.IDPRODOTTO" & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = CSP_PV.IDCLASSESUPERAREAPRODOTTO" & _
            " AND CSP_PV.IDPUNTOVENDITA = TERM.IDPUNTOVENDITA" & _
            " AND CSP.IDPRODOTTO = PTTS.IDPRODOTTO(+)" & _
            " AND TERM.IDTERMINALE = PTTS.IDTERMINALE(+)" & _
            " AND PTTS.IDTIPOSTAMPANTE = TS.IDTIPOSTAMPANTE(+)" & _
            " AND OCP.IDORGANIZZAZIONE = P.IDORGANIZZAZIONE" & _
            " AND OCP.IDCLASSEPUNTOVENDITA = " & TCPV_RETE_LISTICKET & _
            " AND OCP.IDPUNTOVENDITA = CSP_PV.IDPUNTOVENDITA" & _
            " ORDER BY TERM.CODICE"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstTerminali.AddItem rec("CODICE") & " - " & rec("NOME")
            lstTerminali.ItemData(i) = rec("IDTERMINALE")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close

End Sub

Private Sub cmdSelezionaFile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ApriFileConfigurazioneTerminali
    
    MousePointer = mousePointerOld
End Sub

Private Sub ApriFileConfigurazioneTerminali()
    Dim pippo As String
    
On Error GoTo gestioneErroreAperturaFileConfigurazioneTerminali

    DoEvents
    
    cdlFileImport.InitDir = App.Path
    cdlFileImport.Filter = "Excel (*.xls)|*.xls"
    cdlFileImport.DefaultExt = "xls"
    cdlFileImport.Flags = cdlOFNFileMustExist Or cdlOFNExplorer
    cdlFileImport.FileName = ""
    cdlFileImport.CancelError = True
    cdlFileImport.ShowOpen
    
    nomeFileImportazione = cdlFileImport.FileName
    DoEvents
    Call LeggiConfigurazioneTerminaliDaFileExcel
    
    Call PopolaListaTerminali
    
    Exit Sub
    
gestioneErroreAperturaFileConfigurazioneTerminali:
    'do nothing
End Sub

Private Sub LeggiConfigurazioneTerminaliDaFileExcel()
    Dim workbook As workbook
    Dim workSheet As workSheet
    Dim codiceTerminale As String
    Dim tipoStampante As String
    Dim idTerminale As Long
    Dim i As Long
    Dim numeroNulli As Long
    Dim numeroValori As Long
    Dim rangeTotale As range
    Dim quantitaTerminaliAggiornati As Long
    Dim quantitaDatiErrati As Long
    Dim quantitaDatiEliminati As Long
    Dim listaErrori As String
    
    Set workbook = excImportazione.Workbooks.Open(nomeFileImportazione)
    Set workSheet = workbook.ActiveSheet
    
On Error Resume Next
    
    i = 1
    codiceTerminale = ""
    quantitaTerminaliAggiornati = 0
    quantitaDatiErrati = 0
    quantitaDatiEliminati = 0
    listaErrori = ""
    
    Set rangeTotale = workSheet.range("A:A")
    numeroNulli = excImportazione.WorksheetFunction.CountBlank(rangeTotale)
    numeroValori = 65536 - numeroNulli
    
'    Call pgbAvanzamento_Init(numeroValori)
    
    While workSheet.Rows.Cells(i, 1) <> ""
        codiceTerminale = workSheet.Rows.Cells(i, 1)
        tipoStampante = workSheet.Rows.Cells(i, 2)
        If tipoStampante <> "TERMICA" And tipoStampante <> "LASER" Then
            tipoStampante = ""
            quantitaDatiEliminati = quantitaDatiEliminati + 1
        End If
            
        idTerminale = getIdTerminale(codiceTerminale)
        If idTerminale = idNessunElementoSelezionato Then
            quantitaDatiErrati = quantitaDatiErrati + 1
            listaErrori = listaErrori & " " & codiceTerminale & "; "
        Else
            If ConfiguraTerminale(idTerminale, tipoStampante) = 1 Then
                quantitaTerminaliAggiornati = quantitaTerminaliAggiornati + 1
            End If
        End If
        i = i + 1
        DoEvents
    Wend
    MsgBox "Aggiornati: " & quantitaTerminaliAggiornati & "; eliminati: " & quantitaDatiEliminati & "; errati: " & quantitaDatiErrati & " (" & listaErrori & ")"
    
    Call excImportazione.Quit
End Sub

Private Function ConfiguraTerminale(idTerminale As Long, tipoStampante As String) As Long
    Dim sql As String
    Dim n As Long
    Dim i As Integer
        
On Error GoTo errori_ConfiguraTerminale
    ConfiguraTerminale = 0

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans

    sql = "DELETE FROM PROD_TERM_TIPOSTAMPANTE WHERE IDPRODOTTO = " & idProdottoSelezionato & " And idTerminale = " & idTerminale
    n = ORADB.ExecuteSQL(sql)
    If tipoStampante <> "" Then
        sql = "INSERT INTO PROD_TERM_TIPOSTAMPANTE (IDPRODOTTO, IDTERMINALE, IDTIPOSTAMPANTE)" & _
            " SELECT " & idProdottoSelezionato & ", " & idTerminale & ", IDTIPOSTAMPANTE FROM TIPOSTAMPANTE WHERE NOME = '" & tipoStampante & "'"
        n = ORADB.ExecuteSQL(sql)
        ConfiguraTerminale = n
    End If
    
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    
    Exit Function

errori_ConfiguraTerminale:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Function


