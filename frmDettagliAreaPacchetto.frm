VERSION 5.00
Begin VB.Form frmDettagliAreaPacchetto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Aree"
   ClientHeight    =   1335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbArea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   300
      Width           =   4275
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2580
      TabIndex        =   1
      Top             =   780
      Width           =   1035
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   900
      TabIndex        =   0
      Top             =   780
      Width           =   1035
   End
End
Attribute VB_Name = "frmDettagliAreaPacchetto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idAreaSelezionata As Long
Private nomeAreaSelezionata As String
Private codiceAreaSelezionata As String
Private labelAreaSelezionata As String
Private idProdottoSelezionato As Long

Private exitCode As ExitCodeEnum

Public Sub Init()
    Dim sql As String

    Call cmbArea.Clear
    idAreaSelezionata = idNessunElementoSelezionato
'    sql = "SELECT IDTARIFFA ID, CODICE || ' - ' || NOME LABEL"
'    sql = sql & " FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSelezionato
'    sql = sql & " ORDER BY LABEL"
    sql = " SELECT IDAREA ID, A.CODICE || ' - ' || A.NOME LABEL"
    sql = sql & " FROM AREA A, PRODOTTO P"
    sql = sql & " WHERE A.IDPIANTA = P.IDPIANTA"
    sql = sql & " AND IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
    sql = sql & " AND P.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " ORDER BY LABEL"
    Call CaricaValoriCombo(cmbArea, sql, "LABEL")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = cmbArea.ListIndex <> idNessunElementoSelezionato
End Sub

Private Sub cmbArea_Click()
    Call cmbArea_Update
End Sub

Private Sub cmbArea_Update()
    idAreaSelezionata = cmbArea.ItemData(cmbArea.ListIndex)
    labelAreaSelezionata = Trim(cmbArea.Text)
'    codiceAreaSelezionata = Left(labelAreaSelezionata, 3)
    codiceAreaSelezionata = Trim(OttieniCodiceDaLabel(labelAreaSelezionata))
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo1 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            descrizione = rec(NomeCampo1)
            cmb.AddItem descrizione
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Function GetIdAreaSelezionata() As Long
    GetIdAreaSelezionata = idAreaSelezionata
End Function

Public Function GetLabelAreaSelezionata() As String
    GetLabelAreaSelezionata = labelAreaSelezionata
End Function

Public Function GetCodiceAreaSelezionata() As String
    GetCodiceAreaSelezionata = codiceAreaSelezionata
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub




