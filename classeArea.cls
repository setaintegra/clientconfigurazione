VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "classeArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idArea As Long
Public nomeArea As String
Public descrizioneArea As String
Public collFasce As New Collection
Public collPosti As New Collection

Public Sub inizializza_OLD(idA As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sql2 As String
    Dim rec2 As New ADODB.Recordset
    Dim i As Integer
    Dim j As Integer
    Dim numeroFasce As Integer
    Dim numeroTotaleSequenze As Integer
    Dim idP As Long
    Dim idPosto As Long
    Dim idSeq As Long
    Dim idSeqOld As Long
    Dim posto As clsPosto
    Dim postoArea As clsPosto
    
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    
On Error GoTo gestioneErrori
    
    idArea = idA
    sql = "SELECT IDAREA, NOME FROM AREA WHERE IDAREA = " & idArea
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    idArea = rec("IDAREA")
    nomeArea = rec("NOME")
    rec.Close
    
' posti totali
    sql = "SELECT IDPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE," & _
        " IDSEQUENZAPOSTI" & _
        " FROM POSTO" & _
        " WHERE IDAREA = " & idArea
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set posto = New clsPosto
            posto.idPosto = rec("IDPOSTO").Value
            posto.xPosto = rec("COORDINATAORIZZONTALE").Value
            posto.yPosto = rec("COORDINATAVERTICALE").Value
            If Not IsNull(rec("IDSEQUENZAPOSTI").Value) Then
                posto.idSequenzaPosti = rec("IDSEQUENZAPOSTI").Value
            Else
                posto.idSequenzaPosti = idNessunElementoSelezionato
            End If
            Call collPosti.Add(posto)
            rec.MoveNext
        Wend
    End If
    rec.Close

' fasce
    sql = "SELECT * FROM FASCIAPOSTI WHERE IDAREA = " & idArea & " ORDER BY INDICEDIPREFERIBILITA"
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set fascia = New classeFascia
            fascia.idFascia = rec("IDFASCIAPOSTI")
            fascia.indiceDiPreferibilita = rec("INDICEDIPREFERIBILITA")
            aggiungiFascia fascia
            rec.MoveNext
        Wend
    End If
    rec.Close
    
' sequenze
'    sql = "SELECT SEQUENZAPOSTI.IDSEQUENZAPOSTI AS IDS," & _
'        " FASCIAPOSTI.IDFASCIAPOSTI AS IDF," & _
'        " SEQUENZAPOSTI.ORDINEINFASCIA AS ORD" & _
'        " FROM FASCIAPOSTI, SEQUENZAPOSTI" & _
'        " WHERE FASCIAPOSTI.IDAREA = " & idArea & _
'        " AND FASCIAPOSTI.IDFASCIAPOSTI = SEQUENZAPOSTI.IDFASCIAPOSTI" & _
'        " ORDER BY FASCIAPOSTI.INDICEDIPREFERIBILITA, SEQUENZAPOSTI.ORDINEINFASCIA"
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set sequenza = New classeSequenza
'            sequenza.idSequenza = rec("IDS")
'            sequenza.idFascia = rec("IDF")
'            If Not IsNull(rec("ORD")) Then
'                sequenza.ordineInFascia = rec("ORD")
'            Else
'                sequenza.ordineInFascia = idNessunElementoSelezionato
'            End If
'
'            Set sequenza.collPosti = New Collection
'            sql2 = "SELECT IDPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE" & _
'                " FROM POSTO" & _
'                " WHERE POSTO.IDSEQUENZAPOSTI = " & sequenza.idSequenza & _
'                " ORDER BY POSTO.ORDINEINSEQUENZA"
'            rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
'            If Not (rec2.BOF And rec2.EOF) Then
'                rec2.MoveFirst
'                While Not rec2.EOF
'                    Set posto = New clsPosto
'                    posto.idPosto = rec2("IDPOSTO")
'                    posto.xPosto = rec2("COORDINATAORIZZONTALE")
'                    posto.yPosto = rec2("COORDINATAVERTICALE")
'                    ' dovrebbero bastare queste informazioni....
'                    sequenza.collPosti.Add posto
'                    rec2.MoveNext
'                Wend
'            End If
'            rec2.Close
'            aggiungiSequenzaAFascia sequenza.idFascia, sequenza
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close

' OPPURE ......
    idSeqOld = idNessunElementoSelezionato
    
    sql = "SELECT FASCIAPOSTI.IDFASCIAPOSTI AS IDF," & _
        " SEQUENZAPOSTI.IDSEQUENZAPOSTI AS IDS," & _
        " SEQUENZAPOSTI.ORDINEINFASCIA AS ORD," & _
        " IDPOSTO AS IDP, COORDINATAORIZZONTALE, COORDINATAVERTICALE" & _
        " FROM FASCIAPOSTI, SEQUENZAPOSTI, POSTO" & _
        " WHERE FASCIAPOSTI.IDAREA = " & idArea & _
        " AND FASCIAPOSTI.IDFASCIAPOSTI = SEQUENZAPOSTI.IDFASCIAPOSTI" & _
        " AND SEQUENZAPOSTI.IDSEQUENZAPOSTI = POSTO.IDSEQUENZAPOSTI (+)" & _
        " ORDER BY FASCIAPOSTI.INDICEDIPREFERIBILITA, SEQUENZAPOSTI.ORDINEINFASCIA, POSTO.ORDINEINSEQUENZA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idSeq = rec("IDS")
            If (idSeq <> idSeqOld) Then
                If idSeqOld <> idNessunElementoSelezionato Then
                    ' � finita la prima sequenza, va aggiunta alla fascia
                    aggiungiSequenzaAFascia sequenza.idFascia, sequenza
                End If
                idSeqOld = idSeq
                Set sequenza = New classeSequenza
                sequenza.idSequenza = idSeq
                sequenza.idFascia = rec("IDF")
                If Not IsNull(rec("ORD")) Then
                    sequenza.ordineInFascia = rec("ORD")
                Else
                    sequenza.ordineInFascia = idNessunElementoSelezionato
                End If
                
                Set sequenza.collPosti = New Collection
            End If
            
            Set posto = New clsPosto
            posto.idPosto = rec("IDP")
            posto.xPosto = rec("COORDINATAORIZZONTALE")
            posto.yPosto = rec("COORDINATAVERTICALE")
            sequenza.collPosti.Add posto
            
            rec.MoveNext
        Wend
    End If
    rec.Close
    If idSeqOld <> idNessunElementoSelezionato Then
        aggiungiSequenzaAFascia sequenza.idFascia, sequenza
    End If

    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Public Sub inizializza(idA As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idP As Long
    Dim idPosto As Long
    Dim idSeq As Long
    Dim idSeqOld As Long
    Dim posto As clsPosto
    Dim postoArea As clsPosto
    Dim indiceFascia As Long
    
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    
On Error GoTo gestioneErrori
    
    idArea = idA
    sql = "SELECT IDAREA, NOME FROM AREA WHERE IDAREA = " & idArea
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    idArea = rec("IDAREA")
    nomeArea = rec("NOME")
    rec.Close

' fasce
    sql = "SELECT IDFASCIAPOSTI, INDICEDIPREFERIBILITA"
    sql = sql & " FROM FASCIAPOSTI"
    sql = sql & " WHERE IDAREA = " & idArea
    sql = sql & " ORDER BY INDICEDIPREFERIBILITA"
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set fascia = New classeFascia
            fascia.idFascia = rec("IDFASCIAPOSTI")
            fascia.indiceDiPreferibilita = rec("INDICEDIPREFERIBILITA")
            Call aggiungiFascia(fascia)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
' sequenze
    idSeqOld = idNessunElementoSelezionato
    sql = "SELECT F.IDFASCIAPOSTI IDF, F.INDICEDIPREFERIBILITA,"
    sql = sql & " S.IDSEQUENZAPOSTI IDS,"
    sql = sql & " S.ORDINEINFASCIA ORD,"
    sql = sql & " IDPOSTO IDP, COORDINATAORIZZONTALE, COORDINATAVERTICALE,"
    sql = sql & " NOMEFILA, NOMEPOSTO"
    sql = sql & " FROM FASCIAPOSTI F, SEQUENZAPOSTI S, POSTO P"
    sql = sql & " WHERE P.IDAREA = " & idArea
    sql = sql & " AND F.IDFASCIAPOSTI(+) = S.IDFASCIAPOSTI"
    sql = sql & " AND S.IDSEQUENZAPOSTI(+) = P.IDSEQUENZAPOSTI"
    sql = sql & " ORDER BY F.INDICEDIPREFERIBILITA, S.ORDINEINFASCIA, P.ORDINEINSEQUENZA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            ' Aggiunge il posto all'area
            Set posto = New clsPosto
            posto.idPosto = rec("IDP").Value
            posto.xPosto = rec("COORDINATAORIZZONTALE").Value
            posto.yPosto = rec("COORDINATAVERTICALE").Value
            posto.nomeFila = rec("NOMEFILA")
            posto.nomePosto = rec("NOMEPOSTO")
            If Not IsNull(rec("IDS").Value) Then
                posto.idSequenzaPosti = rec("IDS").Value
            Else
                posto.idSequenzaPosti = idNessunElementoSelezionato
            End If
            Call collPosti.Add(posto, ChiaveId(posto.idPosto))
            
            If Not IsNull(rec("IDS").Value) And Not IsNull(rec("IDF").Value) Then
                idSeq = rec("IDS")
                If (idSeq <> idSeqOld) Then
                    If idSeqOld <> idNessunElementoSelezionato Then
                        ' � finita la prima sequenza, va aggiunta alla fascia
'                        Call aggiungiSequenzaAFascia(sequenza.idFascia, sequenza)
                        Call aggiungiSequenzaAFascia(indiceFascia, sequenza)
                    End If
                    indiceFascia = rec("INDICEDIPREFERIBILITA")
                    idSeqOld = idSeq
                    Set sequenza = New classeSequenza
                    sequenza.idSequenza = idSeq
                    sequenza.idFascia = rec("IDF")
                    If Not IsNull(rec("ORD")) Then
                        sequenza.ordineInFascia = rec("ORD")
                    Else
                        sequenza.ordineInFascia = idNessunElementoSelezionato
                    End If
                    
                    Set sequenza.collPosti = New Collection
                End If
                
'                Set posto = New clsPosto
'                posto.idPosto = rec("IDP")
'                posto.xPosto = rec("COORDINATAORIZZONTALE")
'                posto.yPosto = rec("COORDINATAVERTICALE")
'                posto.nomeFila = rec("NOMEFILA")
'                posto.nomePosto = rec("NOMEPOSTO")
                Call sequenza.collPosti.Add(posto, ChiaveId(posto.idPosto))
            End If
            
            rec.MoveNext
        Wend
    End If
    rec.Close
    If idSeqOld <> idNessunElementoSelezionato Then
'        aggiungiSequenzaAFascia sequenza.idFascia, sequenza
        Call aggiungiSequenzaAFascia(indiceFascia, sequenza)
    End If

    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Public Sub aggiungiFascia(fascia As classeFascia)
'    Call collFasce.Add(fascia, ChiaveId(fascia.idFascia))
    Call collFasce.Add(fascia, ChiaveId(fascia.indiceDiPreferibilita))
End Sub

Public Sub creaFascia(indice As Long, listaPosti As Collection)
    Dim fascia As classeFascia
    
    Set fascia = New classeFascia
    fascia.indiceDiPreferibilita = indice
    Set fascia.collPosti = listaPosti
'    collFasce.Add fascia
    Call collFasce.Add(fascia, ChiaveId(fascia.indiceDiPreferibilita))
End Sub

Public Function ottieniFasciaDaIndiceDiPreferibilita_old(indice As Integer) As classeFascia
    Dim f As classeFascia
    
    For Each f In collFasce
        If f.indiceDiPreferibilita = indice Then
            Set ottieniFasciaDaIndiceDiPreferibilita_old = f
            ' come si esce dai foreach?
        End If
    Next f
End Function

Public Function ottieniFasciaDaIndiceDiPreferibilita(indice As Long) As classeFascia
    Dim f As classeFascia
    
'    Set ottieniFasciaDaIndiceDiPreferibilita = collFasce.Item(ChiaveId(indice))
    For Each f In collFasce
        If f.indiceDiPreferibilita = indice Then
            Set ottieniFasciaDaIndiceDiPreferibilita = f
            ' come si esce dai foreach?
        End If
    Next f
End Function
'
''questo metodo fa perdere l'ordine alla collection,
'' inizialmente ordinata per indicie di preferibilit�
'
'Public Sub sostituisciFasciaDaIdFascia(idFascia As Long, fascia As classeFascia)
'    Dim f As classeFascia
'    Dim i As Integer
'    Dim index As Integer
'
'    i = 1
'    For Each f In collFasce
'        If f.idFascia = idFascia Then
'            index = i
'            ' come si esce dai foreach?
'        End If
'        i = i + 1
'    Next f
'    collFasce.Remove index
'    collFasce.Add fascia
'End Sub

Public Sub sostituisciFasciaDaIndiceDiPreferibilita_old(indOld As Integer, indNew As Integer)
    Dim f As classeFascia
    
    For Each f In collFasce
        If f.indiceDiPreferibilita = indOld Then
            f.indiceDiPreferibilita = indNew
        End If
    Next f
End Sub

Public Sub sostituisciFasciaDaIndiceDiPreferibilita(indOld As Long, indNew As Long)
    Dim f As classeFascia
    
    Set f = collFasce.Item(ChiaveId(indOld))
    f.indiceDiPreferibilita = indNew
    Call collFasce.Add(f, ChiaveId(f.indiceDiPreferibilita))
    Call collFasce.Remove(ChiaveId(indOld))
End Sub
'
'Public Sub EliminaFasciaDaId(idFascia As Long)
'    Dim f As classeFascia
'    Dim i As Integer
'    Dim index As Integer
'
'    i = 1
'    For Each f In collFasce
'        If f.idFascia = idFascia Then
'            index = i
'            ' come si esce dai foreach?
'        End If
'        i = i + 1
'    Next f
'    collFasce.Remove index
'End Sub

Public Sub EliminaFasciaDaIndiceDiPreferibilita_old(indice As Integer)
    Dim f As classeFascia
    Dim i As Integer
    Dim index As Integer
    
    i = 1
    For Each f In collFasce
        If f.indiceDiPreferibilita = indice Then
            index = i
            ' come si esce dai foreach?
        End If
        i = i + 1
    Next f
    collFasce.Remove index
End Sub

Public Sub EliminaFasciaDaIndiceDiPreferibilita(indice As Long)
    Call collFasce.Remove(ChiaveId(indice))
End Sub

Public Sub aggiungiSequenzaAFascia_OLD(idFascia As Long, sequenza As classeSequenza)
    Dim f As classeFascia
    
    For Each f In collFasce
        If f.idFascia = idFascia Then
            f.collSequenze.Add sequenza
            ' come si esce dai for each?
        End If
    Next f
End Sub

Public Sub aggiungiSequenzaAFascia(indiceFascia As Long, sequenza As classeSequenza)
    Dim f As classeFascia
    
    'rifare con indiceDiPreferibilit� e ordineInFascia
    Set f = collFasce.Item(ChiaveId(indiceFascia))
    Call f.collSequenze.Add(sequenza, ChiaveId(sequenza.ordineInFascia))
End Sub

Public Sub EliminaSequenza_old(idFascia As Long, idSequenza As Long)
    Dim f As classeFascia
    Dim s As classeSequenza
    Dim iSequenza As Integer
    Dim indiceSequenza As Integer
    Dim iFascia As Integer
    Dim indiceFascia As Integer
    
    iFascia = 1
    For Each f In collFasce
        If f.idFascia = idFascia Then
            indiceFascia = iFascia
            iSequenza = 1
            For Each s In f.collSequenze
                If s.idSequenza = idSequenza Then
                    indiceSequenza = iSequenza
                End If
                iSequenza = iSequenza + 1
            Next s
            f.collSequenze.Remove indiceSequenza
        End If
        iFascia = iFascia + 1
    Next f
    
    ' questo � da verificare bene
    f = collFasce.Item(iFascia)
    If f.collSequenze.count = 0 Then
        collFasce.Remove (iFascia)
    End If
End Sub
'' DA VERIFICARE BENE
'Public Sub eliminaPosto(idPostoDaCancellare As Long)
'    Dim fascia As classeFascia
'    Dim iFascia As Integer
'    Dim indiceFascia As Integer
'    Dim sequenza As classeSequenza
'    Dim iSequenza As Integer
'    Dim indiceSequenza As Integer
'    Dim iPosto As Long
'    Dim indicePosto As Long
'    Dim idPosto As Long
'    Dim trovato As Boolean
'
'    trovato = False
'    iFascia = 1
'    While iFascia < collFasce.count + 1 And Not trovato
'        Set fascia = collFasce(iFascia)
'        iSequenza = 1
'        While iSequenza < fascia.collSequenze.count + 1 And Not trovato
'            Set sequenza = fascia.collSequenze(iSequenza)
'            iPosto = 1
'            While iPosto < sequenza.collPosti.count + 1 And Not trovato
'                idPosto = sequenza.collPosti(iPosto)
'                If idPosto = idPostoDaCancellare Then
'                    trovato = True
'                End If
'            Wend
'        Wend
'    Wend
'
'    If trovato Then
'        sequenza.collPosti.Remove (iPosto)
'        If (sequenza.collPosti.count = 0) Then
'            fascia.collSequenze.Remove (iSequenza)
'            If (fascia.collSequenze.count = 0) Then
'                collFasce.Remove (iFascia)
'            End If
'        End If
'    End If
'
'End Sub
'
Public Function VerificaSePostoInFasciaSequenzaArea_old(idP As Long) As Boolean
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    Dim trovato As Boolean
    Dim iSequenza As Integer
    Dim iFascia As Integer
    Dim iPosto As Integer
    
    trovato = False
    iFascia = 1
    While iFascia < collFasce.count + 1 And Not trovato
        Set fascia = collFasce(iFascia)
        iPosto = 1
        While iPosto < fascia.collPosti.count + 1 And Not trovato
            If idP = fascia.collPosti(iPosto).idPosto Then
                trovato = True
            End If
            iPosto = iPosto + 1
        Wend
        
        iSequenza = 1
        While iSequenza < fascia.collSequenze.count + 1 And Not trovato
            Set sequenza = fascia.collSequenze(iSequenza)
            iPosto = 1
            While iPosto < sequenza.collPosti.count + 1 And Not trovato
                If idP = sequenza.collPosti(iPosto).idPosto Then
                    trovato = True
                End If
                iPosto = iPosto + 1
            Wend
            iSequenza = iSequenza + 1
        Wend
        iFascia = iFascia + 1
    Wend
    
    VerificaSePostoInFasciaSequenzaArea_old = trovato
End Function

'Public Function OttieniColorePostoDaIndice(idP As Long, indF As Long, indS As Long) As ColorePostoEnum
'    Dim fascia As classeFascia
'    Dim sequenza As classeSequenza
'    Dim trovato As Boolean
'    Dim iSequenza As Integer
'    Dim iFascia As Integer
'    Dim iPosto As Integer
'
'    OttieniColorePostoDaIndice = CP_NON_SPECIFICATO
'    trovato = False
'    iFascia = 1
'    While iFascia < collFasce.count + 1 And Not trovato
'        Set fascia = collFasce(iFascia)
'        iPosto = 1
'        While iPosto < fascia.collPosti.count + 1 And Not trovato
'            If idP = fascia.collPosti(iPosto) Then
'                trovato = True
'                If indF = idNessunElementoSelezionato Then
'                    OttieniColorePostoDaIndice = CP_GIALLO
'                Else
'                    If indF = fascia.indiceDiPreferibilita Then
'                        OttieniColorePostoDaIndice = CP_GIALLO
'                    Else
'                        OttieniColorePostoDaIndice = CP_GRIGIO
'                    End If
'                End If
'            End If
'            iPosto = iPosto + 1
'        Wend
'
'        iSequenza = 1
'        While iSequenza < fascia.collSequenze.count + 1 And Not trovato
'            Set sequenza = fascia.collSequenze(iSequenza)
'            iPosto = 1
'            While iPosto < sequenza.collPosti.count + 1 And Not trovato
'                If idP = sequenza.collPosti(iPosto) Then
'                    trovato = True
'                End If
'                iPosto = iPosto + 1
'            Wend
'            iSequenza = iSequenza + 1
'        Wend
'        iFascia = iFascia + 1
'    Wend
'
'End Function
'
Public Function VerificaSePostoInAreaCorrente_old(idP As Long) As Boolean
    Dim trovato As Boolean
    Dim iPosto As Integer
    
    trovato = False
    iPosto = 1
    While iPosto < collPosti.count + 1 And Not trovato
        If idP = collPosti(iPosto).idPosto Then
            trovato = True
        End If
        iPosto = iPosto + 1
    Wend
        
    VerificaSePostoInAreaCorrente_old = trovato
End Function

Public Function VerificaSePostoInAreaCorrente(idP As Long) As Boolean
    
On Error GoTo gestioneErrori
    
    VerificaSePostoInAreaCorrente = True
    Call collPosti.Item(ChiaveId(idP))

    Exit Function
gestioneErrori:
    VerificaSePostoInAreaCorrente = False
End Function

Public Function NumeroFasceDaCompletare() As Integer
    Dim fascia As classeFascia
    Dim count As Integer
    
    count = 0
    For Each fascia In collFasce
        If fascia.collPosti.count > 0 Then
            count = count + 1
        End If
    Next fascia
    NumeroFasceDaCompletare = count
End Function

Public Function VerificaSePostoInFasciaSequenzaArea(idP As Long) As Boolean
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    Dim posto As clsPosto
    
    On Error Resume Next
    
    VerificaSePostoInFasciaSequenzaArea = False
    For Each fascia In collFasce
        For Each sequenza In fascia.collSequenze
            Set posto = sequenza.collPosti.Item(ChiaveId(idP))
            If Not (posto Is Nothing) Then
                VerificaSePostoInFasciaSequenzaArea = True
                Exit Function
            End If
        Next sequenza
    Next fascia
End Function
'
'Public Sub EliminaSequenza(idFascia As Long, idSequenza As Long)
'    Dim f As classeFascia
'
'On Error Resume Next
'    Set f = collFasce.Item(ChiaveId(idFascia))
'    Call f.collSequenze.Remove(ChiaveId(idSequenza))
'End Sub


