VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneProdottoDurateRiservazioni 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "2"
   Begin VB.Frame fraDateDurate 
      Caption         =   "Periodi / scadenze durate riservazioni "
      Height          =   1635
      Left            =   120
      TabIndex        =   25
      Top             =   5460
      Width           =   8175
      Begin VB.TextBox txtMinutiTermine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7380
         MaxLength       =   2
         TabIndex        =   4
         Top             =   300
         Width           =   435
      End
      Begin VB.TextBox txtOraTermine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6780
         MaxLength       =   2
         TabIndex        =   3
         Top             =   300
         Width           =   435
      End
      Begin VB.TextBox txtMinutiDurata 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3960
         MaxLength       =   2
         TabIndex        =   6
         Top             =   720
         Width           =   435
      End
      Begin VB.TextBox txtOreDurata 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3360
         MaxLength       =   2
         TabIndex        =   5
         Top             =   720
         Width           =   435
      End
      Begin VB.TextBox txtMinutiDurataPeriodoScadenzaForzata 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3960
         MaxLength       =   2
         TabIndex        =   8
         Top             =   1140
         Width           =   435
      End
      Begin VB.TextBox txtOreDurataPeriodoScadenzaForzata 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3360
         MaxLength       =   2
         TabIndex        =   7
         Top             =   1140
         Width           =   435
      End
      Begin MSComCtl2.DTPicker dtpDataTermine 
         Height          =   315
         Left            =   3360
         TabIndex        =   2
         Top             =   300
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   67108865
         CurrentDate     =   37684.7534259259
      End
      Begin VB.Label lblOraTermine 
         Alignment       =   1  'Right Justify
         Caption         =   "ora (HH:MM)"
         Height          =   255
         Left            =   5760
         TabIndex        =   33
         Top             =   360
         Width           =   915
      End
      Begin VB.Label lblSeparatoreOreMinuti 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7260
         TabIndex        =   32
         Top             =   300
         Width           =   75
      End
      Begin VB.Label lblDataTermine 
         Alignment       =   1  'Right Justify
         Caption         =   "data"
         Height          =   255
         Left            =   2820
         TabIndex        =   31
         Top             =   360
         Width           =   435
      End
      Begin VB.Label lblScadenza 
         Caption         =   "Scadenza:"
         Height          =   255
         Left            =   1920
         TabIndex        =   30
         Top             =   360
         Width           =   915
      End
      Begin VB.Label lblDurataRiservazioni 
         Alignment       =   1  'Right Justify
         Caption         =   "Durata (HH:MM)"
         Height          =   255
         Left            =   2040
         TabIndex        =   29
         Top             =   780
         Width           =   1215
      End
      Begin VB.Label lblSeparatoreOreMinutiDurata 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3840
         TabIndex        =   28
         Top             =   720
         Width           =   75
      End
      Begin VB.Label lblDurataPeriodoScadenzaForzata 
         Alignment       =   1  'Right Justify
         Caption         =   "Durata periodo scadenza forzata (HH:MM)"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   1200
         Width           =   3015
      End
      Begin VB.Label lblSeparatoreOreMinutiDurataPeriodoScadenzaForzata 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3840
         TabIndex        =   26
         Top             =   1140
         Width           =   75
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   22
      Top             =   4380
      Width           =   2775
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   21
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   19
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   15
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   14
      Top             =   7680
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoDurateRiservazioni 
      Height          =   330
      Left            =   6600
      Top             =   180
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoDurateRiservazioni 
      Height          =   3435
      Left            =   120
      TabIndex        =   16
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6059
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   4140
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   23
      Top             =   4140
      Width           =   2775
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   20
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   18
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Durate Riservazioni Titoli"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoDurateRiservazioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private isProdottoTMaster As ValoreBooleanoEnum
Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
Private dataOraTermine As Date
Private durataRiservazioni As Long
Private durataPeriodoScadenzaForzata As Long
Private isProdottoAttivoSuTL As Boolean
Private rateo As Long
Private isRecordEditabile As Boolean
Private idClasseProdottoSelezionata As ClasseProdottoEnum
'Private emettiWarningVariazioneDateScadenza As Boolean
Private dataOraTermineOld As Date
Private durataRiservazioniOld As Long
Private durataPeriodoScadenzaForzataOld As Long

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    dgrConfigurazioneProdottoDurateRiservazioni.Caption = "CAUSALI RISERVAZIONE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoDurateRiservazioni.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtOraTermine.Text = ""
        txtMinutiTermine.Text = ""
        txtOreDurata.Text = ""
        txtMinutiDurata.Text = ""
        txtOreDurataPeriodoScadenzaForzata.Text = ""
        txtMinutiDurataPeriodoScadenzaForzata.Text = ""
        dtpDataTermine.Value = Null
        dtpDataTermine.Enabled = False
        txtOraTermine.Enabled = False
        txtMinutiTermine.Enabled = False
        txtOreDurata.Enabled = False
        txtMinutiDurata.Enabled = False
        txtOreDurataPeriodoScadenzaForzata.Enabled = False
        txtMinutiDurataPeriodoScadenzaForzata.Enabled = False
        lblDataTermine.Enabled = False
        lblOraTermine.Enabled = False
        lblScadenza.Enabled = False
        lblSeparatoreOreMinuti.Enabled = False
        lblOraTermine.Enabled = False
        lblDurataPeriodoScadenzaForzata.Enabled = False
        lblDurataRiservazioni.Enabled = False
        lblSeparatoreOreMinutiDurata.Enabled = False
        lblSeparatoreOreMinutiDurataPeriodoScadenzaForzata.Enabled = False
        fraDateDurate.Enabled = False
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoDurateRiservazioni.Enabled = False
        fraDateDurate.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        dtpDataTermine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblDataTermine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtOraTermine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And _
            Not (IsNull(dtpDataTermine.Value))
        lblOraTermine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And _
            Not (IsNull(dtpDataTermine.Value))
        lblSeparatoreOreMinuti.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And _
            Not (IsNull(dtpDataTermine.Value))
        lblScadenza.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtMinutiTermine.Enabled = (gestioneRecordGriglia = ASG_MODIFICA) And _
            Not (IsNull(dtpDataTermine.Value))
        txtOreDurata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblDurataRiservazioni.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtMinutiDurata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblSeparatoreOreMinutiDurata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtOreDurataPeriodoScadenzaForzata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblDurataPeriodoScadenzaForzata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        txtMinutiDurataPeriodoScadenzaForzata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        lblSeparatoreOreMinutiDurataPeriodoScadenzaForzata.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = _
            (Len(Trim(txtOraTermine.Text)) = 2 And Len(Trim(txtMinutiTermine.Text)) = 2 And Not IsNull(dtpDataTermine.Value)) Or _
            Len(Trim(txtMinutiDurata.Text)) > 0 Or Len(Trim(txtMinutiDurataPeriodoScadenzaForzata.Text)) > 0
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoDurateRiservazioni.Enabled = True
        dtpDataTermine.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtOraTermine.Text = ""
        txtMinutiTermine.Text = ""
        txtOreDurata.Text = ""
        txtMinutiDurata.Text = ""
        txtOreDurataPeriodoScadenzaForzata.Text = ""
        txtMinutiDurataPeriodoScadenzaForzata.Text = ""
        dtpDataTermine.Enabled = False
        dtpDataTermine.Value = Null
        txtOraTermine.Enabled = False
        txtMinutiTermine.Enabled = False
        txtOreDurata.Enabled = False
        txtMinutiDurata.Enabled = False
        txtOreDurataPeriodoScadenzaForzata.Enabled = False
        txtMinutiDurataPeriodoScadenzaForzata.Enabled = False
        lblDataTermine.Enabled = False
        lblOraTermine.Enabled = False
        lblScadenza.Enabled = False
        lblSeparatoreOreMinuti.Enabled = False
        lblOraTermine.Enabled = False
        lblDurataRiservazioni.Enabled = False
        lblDurataPeriodoScadenzaForzata.Enabled = False
        lblSeparatoreOreMinutiDurata.Enabled = False
        lblSeparatoreOreMinutiDurataPeriodoScadenzaForzata.Enabled = False
        fraDateDurate.Enabled = False
        cmdModifica.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
            cmdEsci.Enabled = True
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
    If isProdottoTMaster Then
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIsProdottoTMaster(b As ValoreBooleanoEnum)
    isProdottoTMaster = b
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato & _
        "; IDCAUSALERISERVAZIONE = " & idRecordSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If isRecordEditabile Then
                Select Case gestioneRecordGriglia
                    Case ASG_MODIFICA
                        If valoriCampiOK Then
                            If EmettiWarningVariazioneDateScadenza Then
                                Call frmMessaggio.Visualizza("AvvertimentoVariazioneDateScadenzaRiservazioni")
                            End If
                            If NessunaDurataRilevata(idProdottoSelezionato, idRecordSelezionato) Then
                                Call InserisciNellaBaseDati
                                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_CAUSALE_RISERVAZIONE, stringaNota, idProdottoSelezionato)
                            Else
                                Call AggiornaNellaBaseDati
                                Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_CAUSALE_RISERVAZIONE, stringaNota, idProdottoSelezionato)
                            End If
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneprodottoDurateRiservazioni_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoDurateRiservazioni_Init
                        End If
                    Case ASG_ELIMINA
                        Call frmMessaggio.Visualizza("AvvertimentoVariazioneDateScadenzaRiservazioni")
                        Call EliminaDallaBaseDati
                        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_CAUSALE_RISERVAZIONE, stringaNota, idProdottoSelezionato)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazioneprodottoDurateRiservazioni_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazioneProdottoDurateRiservazioni_Init
                End Select
            End If
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Private Function EmettiWarningVariazioneDateScadenza() As Boolean
    EmettiWarningVariazioneDateScadenza = False
    If dataOraTermine <> dataOraTermineOld Or _
        durataPeriodoScadenzaForzata <> durataPeriodoScadenzaForzataOld Or _
        durataRiservazioni <> durataRiservazioniOld Then
        EmettiWarningVariazioneDateScadenza = True
    End If
End Function

Private Function NessunaDurataRilevata(idP As Long, idC As Long) As Boolean
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim trovato As Boolean
    Dim durataR As Integer
    Dim durataP As Integer
    Dim dataOraS As Date
    
    Call ApriConnessioneBD
    
    trovato = False
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql1 = "SELECT DURATARISERVAZIONI," & _
'            " DURATAPERIODOSCADENZAFORZATA, DATAORASCADENZARISERVAZIONI" & _
'            " FROM PRODOTTO_CAUSALERISERVAZIONE" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL" & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND IDCAUSALERISERVAZIONE = " & idC
'    Else
        sql1 = "SELECT DURATARISERVAZIONI," & _
            " DURATAPERIODOSCADENZAFORZATA, DATAORASCADENZARISERVAZIONI" & _
            " FROM PRODOTTO_CAUSALERISERVAZIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDCAUSALERISERVAZIONE = " & idC
'    End If
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF And Not trovato
            durataR = IIf(IsNull(rec1("DURATARISERVAZIONI")), -1, rec1("DURATARISERVAZIONI"))
            durataP = IIf(IsNull(rec1("DURATAPERIODOSCADENZAFORZATA")), -1, rec1("DURATAPERIODOSCADENZAFORZATA"))
            dataOraS = IIf(IsNull(rec1("DATAORASCADENZARISERVAZIONI")), dataNulla, rec1("DATAORASCADENZARISERVAZIONI"))
            If (durataR = -1 And durataP = -1 And dataOraS = dataNulla) Then
                trovato = True
            End If
            rec1.MoveNext
        Wend
        NessunaDurataRilevata = trovato
    Else
        NessunaDurataRilevata = True
    End If
    rec1.Close
    
    Call ChiudiConnessioneBD
    
End Function

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Dim stringaNota As String

    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
'            Call Esci
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoDurateRiservazioni.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    dataOraTermineOld = dataOraTermine
    durataRiservazioniOld = durataRiservazioni
    durataPeriodoScadenzaForzataOld = durataPeriodoScadenzaForzata
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneProdottoDurateRiservazioni_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneprodottoDurateRiservazioni_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoDurateRiservazioni_Init
    Call dtpDataTermine_Init
'    Call CercaCausaleDefault
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoDurateRiservazioni.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneprodottoDurateRiservazioni_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneProdottoDurateRiservazioni
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        Sql = "SELECT CR.IDCAUSALERISERVAZIONE ID," & _
''            " CR.NOME ""Nome""," & _
''            " PCR.DATAORASCADENZARISERVAZIONI ""Scadenza""," & _
''            " DECODE(PCR.DURATARISERVAZIONI,NULL,'',LPAD(TRUNC(PCR.DURATARISERVAZIONI/60,0),2,0) || ':' || LPAD(MOD(PCR.DURATARISERVAZIONI,60),2,0)) ""Durata riservazioni""," & _
''            " DECODE(PCR.DURATAPERIODOSCADENZAFORZATA,NULL,'',LPAD(TRUNC(PCR.DURATAPERIODOSCADENZAFORZATA/60,0),2,0) || ':' || LPAD(MOD(PCR.DURATAPERIODOSCADENZAFORZATA,60),2,0)) ""Durata perido scadenza forzata""" & _
''            " FROM PRODOTTO_CAUSALERISERVAZIONE PCR, CAUSALERISERVAZIONE CR" & _
''            " WHERE CR.IDCAUSALERISERVAZIONE = PCR.IDCAUSALERISERVAZIONE(+)" & _
''            " AND IDPRODOTTO(+) = " & idProdottoSelezionato & _
''            " AND (PCR.IDTIPOSTATORECORD IS NULL" & _
''            " OR PCR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
''            " ORDER BY ""Nome"""
'        sql = "SELECT CR.IDCAUSALERISERVAZIONE ID, CR.NOME ""Nome"","
'        sql = sql & " PCR.DATAORASCADENZARISERVAZIONI ""Scadenza"","
'        sql = sql & " DECODE(PCR.DURATARISERVAZIONI,NULL,'',LPAD(TRUNC(PCR.DURATARISERVAZIONI/60,0),2,0) || ':' || LPAD(MOD(PCR.DURATARISERVAZIONI,60),2,0)) ""Durata riservazioni"","
'        sql = sql & " DECODE(PCR.DURATAPERIODOSCADENZAFORZATA,NULL,'',LPAD(TRUNC(PCR.DURATAPERIODOSCADENZAFORZATA/60,0),2,0) || ':' || LPAD(MOD(PCR.DURATAPERIODOSCADENZAFORZATA,60),2,0)) ""Durata perido scadenza forzata"""
'        sql = sql & " FROM "
'        sql = sql & " ("
'        sql = sql & " SELECT IDCAUSALERISERVAZIONE, DATAORASCADENZARISERVAZIONI, DURATARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA"
'        sql = sql & " FROM PRODOTTO_CAUSALERISERVAZIONE"
'        sql = sql & " WHERE IDPRODOTTO(+) = " & idProdottoSelezionato
'        sql = sql & " AND (IDTIPOSTATORECORD IS NULL OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " ) PCR, CAUSALERISERVAZIONE CR"
'        sql = sql & " WHERE CR.IDCAUSALERISERVAZIONE = PCR.IDCAUSALERISERVAZIONE(+)"
'        sql = sql & " ORDER BY ""Nome"""
'    Else
        sql = " SELECT CR.IDCAUSALERISERVAZIONE ID,"
        sql = sql & " CR.NOME ""Nome"","
        sql = sql & " PCR.DATAORASCADENZARISERVAZIONI ""Scadenza"","
        sql = sql & " DECODE(PCR.DURATARISERVAZIONI,NULL,'',LPAD(TRUNC(PCR.DURATARISERVAZIONI/60,0),2,0) || ':' || LPAD(MOD(PCR.DURATARISERVAZIONI,60),2,0)) ""Durata riservazioni"","
        sql = sql & " DECODE(PCR.DURATAPERIODOSCADENZAFORZATA,NULL,'',LPAD(TRUNC(PCR.DURATAPERIODOSCADENZAFORZATA/60,0),2,0) || ':' || LPAD(MOD(PCR.DURATAPERIODOSCADENZAFORZATA,60),2,0)) ""Durata perido scadenza forzata"""
        sql = sql & " FROM PRODOTTO_CAUSALERISERVAZIONE PCR, CAUSALERISERVAZIONE CR"
        sql = sql & " WHERE CR.IDCAUSALERISERVAZIONE = PCR.IDCAUSALERISERVAZIONE(+)"
        sql = sql & " AND IDPRODOTTO(+) = " & idProdottoSelezionato
        sql = sql & " ORDER BY ""Nome"""
'    End If
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    Set dgrConfigurazioneProdottoDurateRiservazioni.dataSource = d
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim condizioniSQL As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    isRecordEditabile = True
    SETAConnection.BeginTrans
    sql = "INSERT INTO PRODOTTO_CAUSALERISERVAZIONE (IDPRODOTTO, IDCAUSALERISERVAZIONE," & _
        " DURATARISERVAZIONI, DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA)" & _
        " VALUES (" & _
        idProdottoSelezionato & ", " & _
        idRecordSelezionato & ", " & _
        IIf(durataRiservazioni = 0, "NULL", durataRiservazioni) & ", " & _
        IIf(dataOraTermine = dataNulla, "NULL", SqlDateTimeValue(dataOraTermine)) & ", " & _
        IIf(durataPeriodoScadenzaForzata = 0, "NULL", durataPeriodoScadenzaForzata) & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            condizioniSQL = " AND IDCAUSALERISERVAZIONE = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_NUOVO, condizioniSQL)
'        End If
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT DATAORASCADENZARISERVAZIONI, DURATARISERVAZIONI,"
'        sql = sql & " DURATAPERIODOSCADENZAFORZATA,"
'        sql = sql & " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE"
'        sql = sql & " FROM PRODOTTO_CAUSALERISERVAZIONE"
'        sql = sql & " WHERE IDCAUSALERISERVAZIONE = " & idRecordSelezionato
'        sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
'    Else
        sql = "SELECT DATAORASCADENZARISERVAZIONI, DURATARISERVAZIONI,"
        sql = sql & " DURATAPERIODOSCADENZAFORZATA"
        sql = sql & " FROM PRODOTTO_CAUSALERISERVAZIONE"
        sql = sql & " WHERE IDCAUSALERISERVAZIONE = " & idRecordSelezionato
        sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        dataOraTermine = IIf(IsNull(rec("DATAORASCADENZARISERVAZIONI")), dataNulla, rec("DATAORASCADENZARISERVAZIONI"))
        durataRiservazioni = IIf(IsNull(rec("DURATARISERVAZIONI")), 0, rec("DURATARISERVAZIONI"))
        durataPeriodoScadenzaForzata = IIf(IsNull(rec("DURATAPERIODOSCADENZAFORZATA")), 0, rec("DURATAPERIODOSCADENZAFORZATA"))
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    Else
        dataOraTermine = dataNulla
        durataRiservazioni = 0
        durataPeriodoScadenzaForzata = 0
        tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim hDur As Integer
    Dim hSca As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtOraTermine.Text = ""
    txtMinutiTermine.Text = ""
    txtOreDurata.Text = ""
    txtMinutiDurata.Text = ""
    txtOreDurataPeriodoScadenzaForzata.Text = ""
    txtMinutiDurataPeriodoScadenzaForzata.Text = ""
    
    If dataOraTermine = dataNulla Then
        'Do Nothing
    Else
        dtpDataTermine.Value = dataOraTermine
        txtOraTermine.Text = StringaOraMinuti(CStr(Hour(dataOraTermine)))
        txtMinutiTermine.Text = StringaOraMinuti(CStr(Minute(dataOraTermine)))
    End If
    
    If durataRiservazioni > 0 Then
        txtOreDurata.Text = StringaOraMinuti(Int(durataRiservazioni / 60))
        hDur = durataRiservazioni Mod 60
        txtMinutiDurata.Text = StringaOraMinuti(CStr(hDur))
    Else
        txtOreDurata.Text = ""
        txtMinutiDurata.Text = ""
    End If
    
    If durataPeriodoScadenzaForzata > 0 Then
        txtOreDurataPeriodoScadenzaForzata.Text = StringaOraMinuti(Int(durataPeriodoScadenzaForzata / 60))
        hSca = durataPeriodoScadenzaForzata Mod 60
        txtMinutiDurataPeriodoScadenzaForzata.Text = StringaOraMinuti(CStr(hSca))
    Else
        txtOreDurataPeriodoScadenzaForzata.Text = ""
        txtMinutiDurataPeriodoScadenzaForzata.Text = ""
    End If
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim condizioniSQL As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE PRODOTTO_CAUSALERISERVAZIONE SET" & _
        " DATAORASCADENZARISERVAZIONI = " & IIf(dataOraTermine = dataNulla, "NULL", SqlDateTimeValue(dataOraTermine)) & "," & _
        " DURATARISERVAZIONI = " & IIf(durataRiservazioni = 0, "NULL", durataRiservazioni) & "," & _
        " DURATAPERIODOSCADENZAFORZATA = " & IIf(durataPeriodoScadenzaForzata = 0, "NULL", durataPeriodoScadenzaForzata) & _
        " WHERE IDCAUSALERISERVAZIONE = " & idRecordSelezionato & _
        " AND IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            condizioniSQL = " AND IDCAUSALERISERVAZIONE = " & idRecordSelezionato
''            Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_MODIFICATO, CondizioniSql)
'            Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_NUOVO, condizioniSQL)
'        End If
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim condizioniSQL As String

    Call ApriConnessioneBD
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM PRODOTTO_CAUSALERISERVAZIONE" & _
'                " WHERE IDCAUSALERISERVAZIONE = " & idRecordSelezionato & _
'                " AND IDPRODOTTO = " & idProdottoSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            condizioniSQL = " AND IDCAUSALERISERVAZIONE = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO, condizioniSQL)
'        End If
'    Else
        sql = "DELETE FROM PRODOTTO_CAUSALERISERVAZIONE" & _
            " WHERE IDCAUSALERISERVAZIONE = " & idRecordSelezionato & _
            " AND IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub dgrConfigurazioneProdottoDurateRiservazioni_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoDurateRiservazioni
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneSistemiDiEmissione
End Sub

Private Sub CaricaFormConfigurazioneSistemiDiEmissione()
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetRateo(rateo)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoSistemiDiEmissione.Init
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoStampeAggiuntive.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoStampeAggiuntive.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Private Function valoriCampiOK() As Boolean
    Dim data As Date
    Dim ora As Integer
    Dim minuti As Integer
    Dim oraDurata As Integer
    Dim minutiDurata As Integer
    Dim oraDurataScadenza As Integer
    Dim minutiDurataScadenza As Integer
    Dim listaNonConformitā As Collection

    valoriCampiOK = True
    
    Set listaNonConformitā = New Collection

    If IsNull(dtpDataTermine.Value) Then
        dataOraTermine = dataNulla
    Else
        data = FormatDateTime(dtpDataTermine.Value, vbShortDate)
        If txtOraTermine.Text <> "" Then
            If IsCampoOraCorretto(txtOraTermine) Then
                ora = CInt(Trim(txtOraTermine.Text))
            Else
'                Call frmMessaggio.Visualizza("ErroreFormatoOra", "Ora scadenza")
                valoriCampiOK = False
                Call listaNonConformitā.Add("- il valore immesso sul campo Ora scadenza deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiTermine.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiTermine) Then
                minuti = CInt(Trim(txtMinutiTermine.Text))
            Else
                valoriCampiOK = False
                Call listaNonConformitā.Add("- il valore immesso sul campo Minuti scadenza deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraTermine = FormatDateTime(data & " " & ora & ":" & minuti, vbGeneralDate)
    End If
    
    If txtOreDurata.Text <> "" Then
        If IsCampoInteroCorretto(txtOreDurata) Then
            oraDurata = CInt(Trim(txtOreDurata.Text))
        Else
            valoriCampiOK = False
            Call listaNonConformitā.Add("- il valore immesso sul campo Ora durata deve essere numerico di tipo intero e compreso tra 0 e 23;")
        End If
    End If
    If txtMinutiDurata.Text <> "" Then
        If IsCampoMinutiCorretto(txtMinutiDurata) Then
            minutiDurata = CInt(Trim(txtMinutiDurata.Text))
        Else
            valoriCampiOK = False
            Call listaNonConformitā.Add("- il valore immesso sul campo Minuti durata deve essere numerico di tipo intero e compreso tra 0 e 59;")
        End If
    End If
    durataRiservazioni = (oraDurata * 60) + minutiDurata
    
    If txtOreDurataPeriodoScadenzaForzata.Text <> "" Then
        If IsCampoInteroCorretto(txtOreDurataPeriodoScadenzaForzata) Then
            oraDurataScadenza = CInt(Trim(txtOreDurataPeriodoScadenzaForzata.Text))
        Else
            valoriCampiOK = False
'            Call frmMessaggio.Visualizza("ErroreFormatoOra", "Ora durata periodo scadenza forzata")
            Call listaNonConformitā.Add("- il valore immesso sul campo Ora durata periodo scadenza forzata deve essere numerico di tipo intero e compreso tra 0 e 23;")
        End If
    End If
    If txtMinutiDurataPeriodoScadenzaForzata.Text <> "" Then
        If IsCampoMinutiCorretto(txtMinutiDurataPeriodoScadenzaForzata) Then
            minutiDurataScadenza = CInt(Trim(txtMinutiDurataPeriodoScadenzaForzata.Text))
        Else
            valoriCampiOK = False
'            Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Minuti durata periodo scadenza forzata")
            Call listaNonConformitā.Add("- il valore immesso sul campo Minuti durata periodo scadenza forzata deve essere numerico di tipo intero e compreso tra 0 e 59;")
        End If
    End If
    durataPeriodoScadenzaForzata = (oraDurataScadenza * 60) + minutiDurataScadenza
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub dtpDataTermine_Init()
    dtpDataTermine.MinDate = dataNulla
End Sub

Private Sub dtpDataTermine_CHANGE()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraTermine_Change()
    If Not internalEvent Then
        If Len(txtOraTermine.Text) = 2 Then
            txtMinutiTermine.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiTermine_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOreDurata_Change()
    If Not internalEvent Then
        If Len(txtOreDurata.Text) = 2 Then
            txtMinutiDurata.SetFocus
        End If
    End If
End Sub

Private Sub txtMinutiDurata_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOreDurataPeriodoScadenzaForzata_Change()
    If Not internalEvent Then
        If Len(txtOreDurataPeriodoScadenzaForzata.Text) = 2 Then
            txtMinutiDurataPeriodoScadenzaForzata.SetFocus
        End If
    End If
End Sub

Private Sub txtMinutiDurataPeriodoScadenzaForzata_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdStagioneSelezionata(idS As Long)
    idStagioneSelezionata = idS
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

