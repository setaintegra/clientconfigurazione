VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSceltaProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8970
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8970
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAnnullaSelezioni 
      Caption         =   "Reset"
      Height          =   375
      Left            =   10800
      TabIndex        =   24
      Top             =   1200
      Width           =   975
   End
   Begin VB.ComboBox cmbUltimiProdotti 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmSceltaProdotto.frx":0000
      Left            =   2280
      List            =   "frmSceltaProdotto.frx":0007
      Style           =   2  'Dropdown List
      TabIndex        =   22
      Top             =   1680
      Width           =   8175
   End
   Begin VB.TextBox txtIdProdotto 
      Height          =   330
      Left            =   120
      TabIndex        =   21
      Top             =   1680
      Width           =   1695
   End
   Begin VB.ComboBox cmbTipoStatoProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmSceltaProdotto.frx":0029
      Left            =   8580
      List            =   "frmSceltaProdotto.frx":0030
      Style           =   2  'Dropdown List
      TabIndex        =   18
      Top             =   900
      Width           =   1935
   End
   Begin VB.ComboBox cmbMese 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmSceltaProdotto.frx":0036
      Left            =   6720
      List            =   "frmSceltaProdotto.frx":0061
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   900
      Width           =   1695
   End
   Begin VB.CommandButton cmdArchivia 
      Caption         =   "Archivia"
      Height          =   435
      Left            =   8640
      TabIndex        =   15
      Top             =   7920
      Width           =   1155
   End
   Begin VB.ComboBox cmbAnnoPrimaRappresentazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmSceltaProdotto.frx":00D8
      Left            =   4380
      List            =   "frmSceltaProdotto.frx":00DF
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   900
      Width           =   2175
   End
   Begin VB.CheckBox chkOrganizzazioniAttive 
      Caption         =   "solo organizzazioni attive"
      Height          =   195
      Left            =   1620
      TabIndex        =   12
      Top             =   660
      Width           =   2655
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Report"
      Height          =   435
      Left            =   7080
      TabIndex        =   11
      Top             =   7920
      Width           =   1155
   End
   Begin VB.CommandButton cmdConvalidaConfigura 
      Caption         =   "Convalida"
      Height          =   435
      Left            =   5820
      TabIndex        =   10
      Top             =   7920
      Width           =   1155
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   900
      Width           =   4095
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10560
      TabIndex        =   4
      Top             =   8280
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   5
      Top             =   7620
      Width           =   5295
      Begin VB.CommandButton cmdClona 
         Caption         =   "Clona"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcProdotto 
      Height          =   375
      Left            =   9180
      Top             =   120
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrProdottiConfigurati 
      Height          =   5235
      Left            =   120
      TabIndex        =   6
      Top             =   2160
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   9234
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblReportInPreparazione 
      Alignment       =   2  'Center
      Caption         =   "Report in preparazione ...Attendere ..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   5880
      TabIndex        =   25
      Top             =   8520
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.Label lblUltimiProdotti 
      Caption         =   "Ultimi prodotti"
      Height          =   255
      Left            =   2280
      TabIndex        =   23
      Top             =   1440
      Width           =   2535
   End
   Begin VB.Label lblIdProdotto 
      Caption         =   "Id prodotto maggiore di"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   1440
      Width           =   1695
   End
   Begin VB.Label lblStatoProdotto 
      Caption         =   "Tipo stato prodotto"
      Height          =   255
      Left            =   8580
      TabIndex        =   19
      Top             =   660
      Width           =   1635
   End
   Begin VB.Label lblMese 
      Caption         =   "Mese"
      Height          =   255
      Left            =   6720
      TabIndex        =   17
      Top             =   660
      Width           =   1635
   End
   Begin VB.Label lblAnnoPrimaRappresentazione 
      Caption         =   "Anno prima rappresentazione"
      Height          =   255
      Left            =   4380
      TabIndex        =   14
      Top             =   660
      Width           =   2175
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione del Prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   7335
   End
End
Attribute VB_Name = "frmSceltaProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idClasseProdottoSelezionato As Long
Private mostraSoloOrganizzazioniAttive As ValoreBooleanoEnum
Private annoSelezionato As Long
Private meseSelezionato As Long
Private idTipoStatoProdottoSelezionato As Long
Private idProdottoMinimo As Long

Private listaAnniDisponibili As Collection
Private listaTipoStatoProdottoDisponibile As Collection

Private internalEvent As Boolean

Private Sub chkOrganizzazioniAttive_Click()
    If Not internalEvent Then
        mostraSoloOrganizzazioniAttive = chkOrganizzazioniAttive.Value
        Call CaricaOrganizzazioni
    End If
End Sub

Private Sub CaricaOrganizzazioni()
    Dim sql As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE"
    If mostraSoloOrganizzazioniAttive = VB_VERO Then
        sql = sql & " WHERE IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVA
    End If
    sql = sql & " ORDER BY NOME"
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
        
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaUltimiProdotti()
    Dim sql As String
    Dim mousePointerOld As Integer
    Dim strIdProdotto As String
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    cmbUltimiProdotti.Clear
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione1", "")
    cmbUltimiProdotti.ItemData(0) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id1", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione2", "")
    cmbUltimiProdotti.ItemData(1) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id2", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione3", "")
    cmbUltimiProdotti.ItemData(2) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id3", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione4", "")
    cmbUltimiProdotti.ItemData(3) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id4", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione5", "")
    cmbUltimiProdotti.ItemData(4) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id5", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione6", "")
    cmbUltimiProdotti.ItemData(5) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id6", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione7", "")
    cmbUltimiProdotti.ItemData(6) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id7", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione8", "")
    cmbUltimiProdotti.ItemData(7) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id8", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione9", "")
    cmbUltimiProdotti.ItemData(8) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id9", ""))
    
    cmbUltimiProdotti.AddItem GetSetting(App.EXEName, "UltimiProdotti", "Descrizione10", "")
    cmbUltimiProdotti.ItemData(9) = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id10", ""))
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbAnnoPrimaRappresentazione_Click()
    Call cmbAnnoPrimaRappresentazione_Update
End Sub

Private Sub cmbMese_Update()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    idRecordSelezionato = idNessunElementoSelezionato
    meseSelezionato = Me.cmbMese.ListIndex + 1
    If meseSelezionato = 13 Then
        meseSelezionato = idTuttiGliElementiSelezionati
    End If
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbMese_Click()
    Call cmbMese_Update
End Sub

Private Sub cmbTipoStatoProdotto_Update()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    idRecordSelezionato = idNessunElementoSelezionato
    idTipoStatoProdottoSelezionato = Me.cmbTipoStatoProdotto.ItemData(Me.cmbTipoStatoProdotto.ListIndex)
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbTipoStatoProdotto_Click()
    Call cmbTipoStatoProdotto_Update
End Sub

Private Sub cmbUltimiProdotti_Click()
    Call ScegliProdottoInHistory
End Sub

Private Sub ScegliProdottoInHistory()
    idRecordSelezionato = cmbUltimiProdotti.ItemData(cmbUltimiProdotti.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnullaSelezioni_Click()
    Call AnnullaSelezioniRicerca
End Sub

Private Sub AnnullaSelezioniRicerca()

    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    annoSelezionato = Year(Now())
    Call AggiornaAbilitazioneControlli
    idRecordSelezionato = idNessunElementoSelezionato

    'default stato organizzazione
    Me.chkOrganizzazioniAttive.Value = 1
    mostraSoloOrganizzazioniAttive = VB_VERO

    'default anno corrente
    Call CreaListaAnniDisponibili
    Call CaricaValoriComboDaLista(cmbAnnoPrimaRappresentazione, listaAnniDisponibili)
    Me.cmbAnnoPrimaRappresentazione.ListIndex = getIdAnnoCorrenteDaSelezionare(Me.cmbAnnoPrimaRappresentazione, annoSelezionato)

    'default mese corrente
    meseSelezionato = Month(Now())
    Me.cmbMese.ListIndex = meseSelezionato - 1

    'default stato prodotto
    Call CreaListaTipoStatoProdottoDisponibile
    Call CaricaValoriComboDaLista(cmbTipoStatoProdotto, listaTipoStatoProdottoDisponibile)
    Me.cmbTipoStatoProdotto.ListIndex = getIdTipoStatoProdottoDaSelezionare(Me.cmbTipoStatoProdotto, "Attivo")
    
    txtIdProdotto.Text = ""

    Call CaricaOrganizzazioni
    Call CaricaUltimiProdotti
End Sub
Private Sub cmdClona_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CaricaFormClonazioneProdotto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = Me.MousePointer
    Me.MousePointer = vbHourglass
'    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
'    idRecordSelezionato = idNessunElementoSelezionato
'    Call CaricaValoriGriglia
'    Call AggiornaAbilitazioneControlli
    Call cmbOrganizzazione_Update
    
    Me.MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Update()
    
    idOrganizzazioneSelezionata = Me.cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmbAnnoPrimaRappresentazione_Update()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
        
    idRecordSelezionato = idNessunElementoSelezionato
    annoSelezionato = Me.cmbAnnoPrimaRappresentazione.ItemData(Me.cmbAnnoPrimaRappresentazione.ListIndex)
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub CreaListaAnniDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim anno As clsElementoLista
    Dim tuttiAnni As clsElementoLista
    
    Set listaAnniDisponibili = New Collection
    
    Call ApriConnessioneBD
    
    sql = " SELECT DISTINCT TO_CHAR(R.DATAORAINIZIO,'YYYY') ANNO"
    sql = sql & " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R"
    sql = sql & " WHERE P.IDPRODOTTO = PR.IDPRODOTTO"
    sql = sql & " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    'per l'anno di default � stato commentato il codice sottostante
'    If idOrganizzazioneSelezionata <> idNessunElementoSelezionato And _
'        idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
'        sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'    End If
    sql = sql & " ORDER BY ANNO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set anno = New clsElementoLista
            anno.idElementoLista = CInt(rec("ANNO"))
            anno.descrizioneElementoLista = CStr(rec("ANNO"))
            Call listaAnniDisponibili.Add(anno)
            rec.MoveNext
        Wend
    End If
    rec.Close
    Set tuttiAnni = New clsElementoLista
    tuttiAnni.idElementoLista = idTuttiGliElementiSelezionati
    tuttiAnni.descrizioneElementoLista = "<tutti>"
    Call listaAnniDisponibili.Add(tuttiAnni)
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CreaListaTipoStatoProdottoDisponibile()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim tipoStatoProdotto As clsElementoLista
    Dim tuttiTipoStatoProdotto As clsElementoLista
    
    Set listaTipoStatoProdottoDisponibile = New Collection
    
    Call ApriConnessioneBD
    
    sql = "SELECT * from TIPOSTATOPRODOTTO WHERE idtipostatoprodotto IN (1,2,3)"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoStatoProdotto = New clsElementoLista
            tipoStatoProdotto.idElementoLista = CInt(rec("IDTIPOSTATOPRODOTTO"))
            tipoStatoProdotto.descrizioneElementoLista = CStr(rec("NOME"))
            Call listaTipoStatoProdottoDisponibile.Add(tipoStatoProdotto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    Set tuttiTipoStatoProdotto = New clsElementoLista
    tuttiTipoStatoProdotto.idElementoLista = idTuttiGliElementiSelezionati
    tuttiTipoStatoProdotto.descrizioneElementoLista = "<tutti>"
    Call listaTipoStatoProdottoDisponibile.Add(tuttiTipoStatoProdotto)
    
    Call ChiudiConnessioneBD
    
End Sub

'
'Private Sub CaricaListaAnni()
'    Dim annoCorrente As Integer
'    Dim i As Integer
'
'    annoCorrente = CInt(Year(Now))
'    For i = 1 To 10
'
'    Next i
'End Sub
'
'Private Sub CaricaValoriComboDaLista(cmb As ComboBox, listaLabelElementi As Collection, listaIdElementi As Collection)
Private Sub CaricaValoriComboDaLista(cmb As ComboBox, listaElementi As Collection)
    Dim i As Integer
    Dim elemento As clsElementoLista
    
    Call cmb.Clear
    i = 1
'    For i = 1 To listaElementi.count
    For Each elemento In listaElementi
        cmb.AddItem elemento.descrizioneElementoLista
        cmb.ItemData(i - 1) = elemento.idElementoLista
        i = i + 1
'    Next i
    Next elemento

End Sub

Private Sub CaricaFormClonazioneProdotto()
    Dim rec As ADODB.Recordset
    
'    Call BloccaProdottoPerUtente(idRecordSelezionato)
    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    If Not isProdottoBloccatoDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    Else
        If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
            Set rec = adcProdotto.Recordset
            idOrganizzazioneSelezionata = rec("IDORGANIZZAZIONE")
        End If
        Call GetClasseProdottoRecordSelezionato
        Call frmClonazioneProdotto.SetIdProdottoSelezionato(idRecordSelezionato)
        Call frmClonazioneProdotto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
        Call frmClonazioneProdotto.SetIdClasseProdottoSelezionato(idClasseProdottoSelezionato)
        Call frmClonazioneProdotto.Init
        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
        Call AggiornaDataGrid
        Call dgrProdottiConfigurati_Init
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call ModificaProdotto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdElimina_Click()
    Call EliminaProdotto
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call NuovoProdotto
    
    MousePointer = mousePointerOld
End Sub

Private Sub NuovoProdotto()
    isProdottoBloccatoDaUtente = True
    Call frmInizialeProdotto.SetModalit�Form(A_NUOVO)
    Call frmInizialeProdotto.Init
    Call AggiornaDataGrid
    Call dgrProdottiConfigurati_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaProdotto()
    Call frmInizialeProdotto.SetModalit�Form(A_MODIFICA)
    Call frmInizialeProdotto.SetIdProdottoSelezionato(idRecordSelezionato)
'    Call BloccaProdottoPerUtente(idRecordSelezionato)
    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    If Not isProdottoBloccatoDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Call frmInizialeProdotto.Init
    Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    Call AggiornaDataGrid
    Call dgrProdottiConfigurati_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call CaricaUltimiProdotti
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub AggiornaAbilitazioneControlli()

    dgrProdottiConfigurati.Columns(0).Visible = False
    dgrProdottiConfigurati.Caption = "PRODOTTI CONFIGURATI"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdClona.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdReport.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    If idRecordSelezionato <> idNessunElementoSelezionato Then
        Select Case tipoStatoProdotto
            Case TSP_IN_CONFIGURAZIONE
                cmdConvalidaConfigura.Visible = True
                cmdConvalidaConfigura.Caption = "Convalida"
                cmdConvalidaConfigura.Enabled = True
                cmdArchivia.Visible = True
                cmdArchivia.Caption = "Archivia"
                cmdArchivia.Enabled = True
            Case TSP_ATTIVABILE
                cmdConvalidaConfigura.Visible = True
                cmdConvalidaConfigura.Caption = "Configura"
                cmdConvalidaConfigura.Enabled = True
                cmdArchivia.Visible = False
            Case TSP_ATTIVO
                cmdConvalidaConfigura.Visible = False
                cmdArchivia.Visible = False
            Case TSP_ARCHIVIATO
                cmdConvalidaConfigura.Visible = False
                cmdArchivia.Visible = False
            Case Else
        End Select
    Else
        cmdConvalidaConfigura.Visible = False
        cmdArchivia.Visible = False
    End If
End Sub

Private Sub AggiornaDataGrid()
    Call CaricaValoriGriglia
'    adcProdotto.Refresh
End Sub

Private Sub cmdReport_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call GeneraReport
    
    MousePointer = mousePointerOld
End Sub

Private Sub GeneraReport()
    Dim report As New clsReportProd
    Dim visualizzaStatoProtezioni As Boolean
    
    visualizzaStatoProtezioni = False
    Call frmMessaggio.Visualizza("VisualizzaStatoProtezioni")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        visualizzaStatoProtezioni = True
    End If
    lblReportInPreparazione.Visible = True
    Call report.PrelevaDati(idRecordSelezionato, visualizzaStatoProtezioni)
    lblReportInPreparazione.Visible = False
    
End Sub

Private Sub dgrProdottiConfigurati_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call GetStatoProdottoRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrProdottiConfigurati, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
End Sub

Private Sub GetStatoProdottoRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        tipoStatoProdotto = rec("TIPOSTATOENUM")
    Else
        tipoStatoProdotto = TSP_NON_SPECIFICATO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub GetClasseProdottoRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idClasseProdottoSelezionato = rec("CLASSE").Value
    Else
        idClasseProdottoSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init()
    Call AnnullaSelezioniRicerca
    Call Me.Show(vbModal)
End Sub

Private Function getIdAnnoCorrenteDaSelezionare(cmb As ComboBox, valoreDaConfrontare As Long) As Long
    Dim riga As Long
    Dim id As Long
    
    For id = 0 To cmb.ListCount
        If cmb.List(id) = CStr(valoreDaConfrontare) Then
            getIdAnnoCorrenteDaSelezionare = id
            Exit For
        End If
    Next id
End Function

Private Function getIdTipoStatoProdottoDaSelezionare(cmb As ComboBox, valoreDaConfrontare As String) As Long
    Dim riga As Long
    Dim id As Long
    
    For id = 0 To cmb.ListCount
        If cmb.List(id) = Trim(valoreDaConfrontare) Then
            getIdTipoStatoProdottoDaSelezionare = id
            Exit For
        End If
    Next id
End Function

Private Sub CaricaValoriGriglia()
    Call adcProdotto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrProdottiConfigurati_Init
End Sub

Private Sub adcProdotto_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Dim NumeroRicevAbilitateDellOrganizzazione As Long
    
    NumeroRicevAbilitateDellOrganizzazione = getNumRicAbilitateOrganizzazione(idOrganizzazioneSelezionata)
    
    Set d = adcProdotto
    
    sql = "SELECT P.IDPRODOTTO ID, P.NOME ""Nome"","
    sql = sql & " PI.NOME ""Pianta"", R.DATAORAINIZIO ""Data/ora inizio"","
    sql = sql & " TSP.NOME ""Stato"","
    sql = sql & " P.IDTIPOSTATOPRODOTTO TIPOSTATOENUM, P.IDCLASSEPRODOTTO CLASSE"
    sql = sql & " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP, ORGANIZZAZIONE O, PIANTA PI,"
    sql = sql & " ("
    sql = sql & " SELECT P.IDPRODOTTO, MIN(R.DATAORAINIZIO) DATAORAINIZIO"
    sql = sql & " FROM PRODOTTO P, RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE PR"
    sql = sql & " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
    sql = sql & " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " GROUP BY P.IDPRODOTTO"
    sql = sql & " UNION"
    sql = sql & " SELECT P.IDPRODOTTO, MIN(R.DATAORAINIZIO) DATAORAINIZIO"
    sql = sql & " FROM PRODOTTO P, RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE SR, SCELTARAPPRESENTAZIONE_RAPPR SR_R"
    sql = sql & " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND P.IDPRODOTTO = SR.IDPRODOTTO"
    sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = SR_R.IDSCELTARAPPRESENTAZIONE"
    sql = sql & " AND SR_R.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " AND P.IDCLASSEPRODOTTO = " & CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO
    sql = sql & " GROUP BY P.IDPRODOTTO"
    sql = sql & " ) R"
    sql = sql & " WHERE P.IDTIPOSTATOPRODOTTO = TSP.IDTIPOSTATOPRODOTTO"
    
    If idTipoStatoProdottoSelezionato <> idNessunElementoSelezionato And _
        idTipoStatoProdottoSelezionato <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND TSP.IDTIPOSTATOPRODOTTO = " & CStr(idTipoStatoProdottoSelezionato)
    ElseIf idTipoStatoProdottoSelezionato <> idNessunElementoSelezionato And _
        idTipoStatoProdottoSelezionato = idTuttiGliElementiSelezionati Then
        sql = sql & " AND TSP.IDTIPOSTATOPRODOTTO IN (1,2,3,6)"
    End If
    If txtIdProdotto.Text <> "" Then
        If IsNumeric(txtIdProdotto.Text) Then
            sql = sql & " AND P.IDPRODOTTO > " & txtIdProdotto.Text
        End If
    End If
    
    sql = sql & " AND P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE"
    sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND P.IDPIANTA = PI.IDPIANTA"
    sql = sql & " AND P.IDPRODOTTO = R.IDPRODOTTO(+)"
    If annoSelezionato <> idNessunElementoSelezionato And _
        annoSelezionato <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND TO_CHAR(R.DATAORAINIZIO,'YYYY') = '" & CStr(annoSelezionato) & "'"
    End If
    '+
    If meseSelezionato <> idNessunElementoSelezionato And _
        meseSelezionato <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND TO_CHAR(R.DATAORAINIZIO,'MM') = '" & CStr(IIf(meseSelezionato < 10, "0" & meseSelezionato, meseSelezionato)) & "'"
    ElseIf meseSelezionato <> idNessunElementoSelezionato And _
        meseSelezionato = idTuttiGliElementiSelezionati Then
        'non viene impostato il mese
    End If
    If idTipoStatoProdottoSelezionato <> idNessunElementoSelezionato And _
        idTipoStatoProdottoSelezionato <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND P.IDTIPOSTATOPRODOTTO = " & CStr(idTipoStatoProdottoSelezionato)
    ElseIf idTipoStatoProdottoSelezionato <> idNessunElementoSelezionato And _
        idTipoStatoProdottoSelezionato = idTuttiGliElementiSelezionati Then
        sql = sql & " AND P.IDTIPOSTATOPRODOTTO IN (1,2,3,6)"
    End If
    
    sql = sql & " ORDER BY TIPOSTATOENUM, ""Nome"""
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrProdottiConfigurati.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Function getNumRicAbilitateOrganizzazione(idOrganizzazione As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim NumRicev As Long
    
    Call ApriConnessioneBD

    sql = " SELECT COUNT(DISTINCT O.IDOPERATORE) ric_ab"
    sql = sql & " FROM operatore_organizzazione oo, operatore o, puntovendita pv, TERMINALE T"
    sql = sql & " WHERE oo.idorganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " AND oo.idoperatore = o.idoperatore"
    sql = sql & " AND o.abilitato = 1"
    sql = sql & " AND o.IDPUNTOVENDITA = pv.IDPUNTOVENDITA"
    sql = sql & " and pv.idpuntovendita = t.idpuntovendita"
    sql = sql & " AND (T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2 OR T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3)"
    sql = sql & " AND pv.IDTIPOPUNTOVENDITA = 2"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        NumRicev = CInt(rec("ric_ab"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    getNumRicAbilitateOrganizzazione = NumRicev
End Function

Private Sub dgrProdottiConfigurati_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrProdottiConfigurati
    g.ScrollBars = dbgAutomatic
    dimensioneGrid = g.Width - 100
    
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Visible = False
    g.Columns(6).Visible = False
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub EliminaProdotto()
    Dim prodottoEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    If Not isProdottoBloccatoDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Set listaTabelleCorrelate = New Collection
    Call frmInizialeProdotto.SetModalit�Form(A_ELIMINA)
    
    prodottoEliminabile = True
    If Not IsRecordEliminabile("IDPRODOTTO", "PREZZOTITOLOPRODOTTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PREZZOTITOLOPRODOTTO")
        prodottoEliminabile = False
    End If
    If idClasseProdottoSelezionato = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
        If Not IsRecordEliminabile("IDPRODOTTO", "SCELTARAPPRESENTAZIONE", CStr(idRecordSelezionato)) Then
            Call listaTabelleCorrelate.Add("SCELTARAPPRESENTAZIONE")
            prodottoEliminabile = False
        End If
    Else
        If Not IsRecordEliminabile("IDPRODOTTO", "PRODOTTO_RAPPRESENTAZIONE", CStr(idRecordSelezionato)) Then
            Call listaTabelleCorrelate.Add("PRODOTTO_RAPPRESENTAZIONE")
            prodottoEliminabile = False
        End If
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "TARIFFA", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("TARIFFA")
        prodottoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "TITOLO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("TITOLO")
        prodottoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "PROTEZIONEPOSTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PROTEZIONEPOSTO")
        prodottoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "PROTEZIONEAREA", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PROTEZIONEAREA")
        prodottoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "PERIODOCOMMERCIALE", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PERIODOCOMMERCIALE")
        prodottoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "RICHIESTASIGILLO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("RICHIESTASIGILLO")
        prodottoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPRODOTTO", "STAMPAAGGIUNTIVA", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("STAMPAAGGIUNTIVA")
        prodottoEliminabile = False
    End If
    
    If prodottoEliminabile Then
        Call frmInizialeProdotto.SetIdProdottoSelezionato(idRecordSelezionato)
        Call frmInizialeProdotto.Init
        Call AggiornaDataGrid
        Call dgrProdottiConfigurati_Init
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il prodotto selezionato", tabelleCorrelate)
        Call frmInizialeOperatore.SetOperatoreAbilitato(0)
    End If
    Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
    MousePointer = mousePointerOld
    
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
        
    Call cmb.Clear
        
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    'cmb.AddItem "<tutti>"
    'cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub cmdConvalidaConfigura_Click()
    Call ConvalidaConfigura
End Sub

Private Sub cmdArchivia_Click()
    Call Archivia
End Sub

Private Sub ConvalidaConfigura()
    Dim mousePointerOld As Integer
    Dim ccAttivit� As CCTipoAttivit�Enum
    
    If Not IsProdottoTDL(idRecordSelezionato) Then
        mousePointerOld = MousePointer
        MousePointer = vbHourglass
    '    Call BloccaProdottoPerUtente(idRecordSelezionato)
        Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
        If Not isProdottoBloccatoDaUtente Then
            Call frmMessaggio.Visualizza("NotificaRecordBloccato")
        Else
            If tipoStatoProdotto = TSP_IN_CONFIGURAZIONE Then
                Call AggiornaIDgenereSiaePredominante(idRecordSelezionato)
            End If
            Call AggiornaStatoProdottoInBaseDati(ccAttivit�)
            Call ScriviLog(ccAttivit�, CCDA_PRODOTTO, , "IDPRODOTTO = " & idRecordSelezionato)
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
    '        Call SbloccaProdottoPerUtente(idRecordSelezionato)
            Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
            Call AggiornaAbilitazioneControlli
        End If
        MousePointer = mousePointerOld
    End If
End Sub

Private Sub AggiornaIDgenereSiaePredominante(idRecordSelezionato As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Dim idGenereSiae As Long
    
    Call ApriConnessioneBD
    
    sql = "       SELECT MIN(IDGENERESIAE) IDGENERESIAEPREDOMINANTE "
    sql = sql & "       FROM "
    sql = sql & "             ( "
    sql = sql & "           SELECT IDPRODOTTO, IDGENERESIAE, MAX(INCIDENZA) MAX_INCIDENZA "
    sql = sql & "             FROM "
    sql = sql & "                   ( "
    sql = sql & "                   SELECT PR.IDPRODOTTO, SG.IDGENERESIAE, SUM(SG.INCIDENZA) INCIDENZA "
    sql = sql & "                   FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, SPETTACOLO_GENERESIAE SG "
    sql = sql & "                   WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE "
    sql = sql & "                   AND R.IDSPETTACOLO = SG.IDSPETTACOLO "
    sql = sql & "                   AND idprodotto = " & idRecordSelezionato
    sql = sql & "                   GROUP BY PR.IDPRODOTTO, SG.IDGENERESIAE "
    sql = sql & "                   ) "
    sql = sql & "             GROUP BY ROLLUP (IDPRODOTTO, IDGENERESIAE) "
    sql = sql & "             ) "
    sql = sql & "       GROUP BY IDPRODOTTO, MAX_INCIDENZA "
    sql = sql & "       HAVING IDPRODOTTO IS NOT NULL AND COUNT(*) = COUNT(IDGENERESIAE) + 1"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        idGenereSiae = rec("IDGENERESIAEPREDOMINANTE").Value
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    sql = "UPDATE prodotto SET idGenereSiaePredominante = " & idGenereSiae & " WHERE idprodotto = " & idRecordSelezionato
    SETAConnection.Execute sql, , adCmdText
    
End Sub

Private Sub Archivia()
    Dim mousePointerOld As Integer
    Dim ccAttivit� As CCTipoAttivit�Enum
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    If Not isProdottoBloccatoDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    Else
        Call ArchiviaProdottoInBaseDati
        Call ScriviLog(CCTA_ARCHIVIAZIONE, CCDA_PRODOTTO, , "IDPRODOTTO = " & idRecordSelezionato)
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
        Call AggiornaAbilitazioneControlli
    End If
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAttivaDisattiva_Click()
    Call AttivaDisattiva
End Sub

Private Sub AttivaDisattiva()
    Dim mousePointerOld As Integer
    Dim ccAttivit� As CCTipoAttivit�Enum
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
'    Call BloccaProdottoPerUtente(idRecordSelezionato)
    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
    If Not isProdottoBloccatoDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    Else
'        Select Case tipoStatoProdotto
'            Case TSP_ATTIVABILE
'                tipoStatoProdotto = TSP_ATTIVO
'                ccAttivit� = CCTA_ATTIVAZIONE
'            Case TSP_ATTIVO
'                tipoStatoProdotto = TSP_ATTIVABILE
'                ccAttivit� = CCTA_DISATTIVAZIONE
'            Case Else
'                'Do Nothing
'        End Select
'        Call AggiornaStatoProdottoInBaseDati
        Call AggiornaStatoProdottoInBaseDati(ccAttivit�)
        Call ScriviLog(ccAttivit�, CCDA_PRODOTTO, , "IDPRODOTTO = " & idRecordSelezionato)
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
'        Call SbloccaProdottoPerUtente(idRecordSelezionato)
        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
        Call AggiornaAbilitazioneControlli
    End If
    MousePointer = mousePointerOld
End Sub

Private Sub AggiornaStatoProdottoInBaseDati_old()
    Dim d As Adodc
    Dim rec As ADODB.Recordset
    
    Set d = adcProdotto
    Set rec = d.Recordset
    
    rec("TIPOSTATOENUM") = tipoStatoProdotto
    rec.Update
    Call adcProdotto_Init
    Call dgrProdottiConfigurati_Init
    
End Sub

Private Sub AggiornaStatoProdottoInBaseDati(ccAttivit� As CCTipoAttivit�Enum)
    Dim sql As String
    Dim recordAggiornati As Integer
    Dim internalEventOld As Boolean
    Dim risposta As VbMsgBoxResult
    Dim attiva As Boolean
    
    Select Case tipoStatoProdotto
        Case TSP_IN_CONFIGURAZIONE
            If IsProdottoAttivabile(idRecordSelezionato, True) Then
            
' se il generesiae predominante � calcio allora dovrebbe rientrare nel decreto
' non � incluso nei controlli precedenti in quanto � solo un warning
                If Not IsParametroDecretoSicurezzaOK(idRecordSelezionato) Then
                    risposta = MsgBox("Attenzione! Il genere SIAE � 'calcio' ma il prodotto non rientra nel prodotto sicurezza! Si vuole comunque procedere?", vbYesNo)
                    If risposta = vbYes Then
                        attiva = True
                    Else
                        attiva = False
                    End If
                Else
                    attiva = True
                End If
                
                If (attiva) Then
                    recordAggiornati = 0
                    sql = "UPDATE PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE & _
                        " WHERE IDPRODOTTO = " & idRecordSelezionato & _
                        " AND IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE
                    SETAConnection.Execute sql, recordAggiornati, adCmdText
                    If recordAggiornati = 1 Then
                        tipoStatoProdotto = TSP_ATTIVABILE
                        ccAttivit� = CCTA_CONVALIDA
                    End If
                End If
            End If
        Case TSP_ATTIVABILE
            recordAggiornati = 0
            sql = "UPDATE PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE & _
                " WHERE IDPRODOTTO = " & idRecordSelezionato & _
                " AND IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE
            SETAConnection.Execute sql, recordAggiornati, adCmdText
            If recordAggiornati = 1 Then
                tipoStatoProdotto = TSP_IN_CONFIGURAZIONE
                ccAttivit� = CCTA_CONFIGURAZIONE
            Else
                tipoStatoProdotto = TSP_ATTIVO
            End If
        Case Else
            'Do Nothing
    End Select
    
    internalEventOld = internalEvent
    internalEvent = True
    
'    Call adcProdotto.Refresh
    Call adcProdotto_Init
    Call dgrProdottiConfigurati_Init
    
    internalEvent = internalEventOld
End Sub

Private Sub ArchiviaProdottoInBaseDati()
    Dim sql As String
    Dim recordAggiornati As Integer
    Dim internalEventOld As Boolean
    
    Select Case tipoStatoProdotto
        Case TSP_IN_CONFIGURAZIONE
            recordAggiornati = 0
            sql = "UPDATE PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_ARCHIVIATO & _
                " WHERE IDPRODOTTO = " & idRecordSelezionato
            SETAConnection.Execute sql, recordAggiornati, adCmdText
            If recordAggiornati = 1 Then
                tipoStatoProdotto = TSP_ARCHIVIATO
            End If
        Case Else
            'Do Nothing
    End Select
    
    internalEventOld = internalEvent
    internalEvent = True
    
'    Call adcProdotto.Refresh
    Call adcProdotto_Init
    Call dgrProdottiConfigurati_Init
    
    internalEvent = internalEventOld
End Sub


