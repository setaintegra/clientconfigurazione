VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneVenue 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Venue"
   ClientHeight    =   8940
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca"
      Height          =   315
      Left            =   10680
      TabIndex        =   31
      Top             =   480
      Width           =   1155
   End
   Begin VB.TextBox txtCerca 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8880
      MaxLength       =   21
      TabIndex        =   30
      Top             =   480
      Width           =   1695
   End
   Begin VB.TextBox txtDescrizionePOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7800
      MaxLength       =   21
      TabIndex        =   10
      Top             =   7500
      Width           =   2415
   End
   Begin VB.ComboBox cmbComune 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4020
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   6840
      Width           =   3555
   End
   Begin VB.ComboBox cmbProvincia 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4020
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   6180
      Width           =   3555
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      MaxLength       =   30
      TabIndex        =   4
      Top             =   6180
      Width           =   3315
   End
   Begin VB.TextBox txtCodiceSiae 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10320
      MaxLength       =   13
      TabIndex        =   11
      Top             =   6180
      Width           =   1455
   End
   Begin VB.TextBox txtDescrizioneStampa 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7800
      MaxLength       =   20
      TabIndex        =   9
      Top             =   6840
      Width           =   2415
   End
   Begin VB.TextBox txtDescrizioneVideo 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7800
      MaxLength       =   20
      TabIndex        =   8
      Top             =   6180
      Width           =   2415
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   885
      Left            =   180
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   6840
      Width           =   3615
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   14
      Top             =   8340
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   16
      Top             =   7860
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   15
      Top             =   4860
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneVenue 
      Height          =   330
      Left            =   9720
      Top             =   4560
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneVenue 
      Height          =   3675
      Left            =   120
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   840
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6482
      _Version        =   393216
      AllowUpdate     =   0   'False
      BackColor       =   16777215
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCerca 
      Caption         =   "Codice SIAE"
      Height          =   255
      Left            =   7200
      TabIndex        =   29
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label lblDescrizionePOS 
      Caption         =   "Descr. POS"
      Height          =   255
      Left            =   7800
      TabIndex        =   28
      Top             =   7260
      Width           =   1395
   End
   Begin VB.Label lblComune 
      Caption         =   "Comune"
      Height          =   255
      Left            =   4020
      TabIndex        =   27
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Label lblProvincia 
      Caption         =   "Provincia (sigla)"
      Height          =   255
      Left            =   4020
      TabIndex        =   26
      Top             =   5940
      Width           =   1695
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   25
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblCodiceSiae 
      Caption         =   "Codice SIAE"
      Height          =   255
      Left            =   10320
      TabIndex        =   24
      Top             =   5940
      Width           =   1095
   End
   Begin VB.Label lblDescrizioneStampa 
      Caption         =   "Descr. stampa"
      Height          =   255
      Left            =   7800
      TabIndex        =   23
      Top             =   6600
      Width           =   1395
   End
   Begin VB.Label lblDescrizioneVideo 
      Caption         =   "Descr. video"
      Height          =   255
      Left            =   7800
      TabIndex        =   22
      Top             =   5940
      Width           =   1395
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   180
      TabIndex        =   21
      Top             =   6600
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   180
      TabIndex        =   20
      Top             =   5880
      Width           =   1695
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione del Venue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   180
      TabIndex        =   18
      Top             =   360
      Width           =   5775
   End
End
Attribute VB_Name = "frmConfigurazioneVenue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idTipoSupportoSelezionato As Long
Private SQLCaricamentoCombo As String
Private nomeRecordSelezionato As String
Private descrizioneRecordSelezionato As String
Private descrVideoRecordSelezionato As String
Private descrStampaRecordSelezionato As String
Private descrPOSRecordSelezionato As String
Private codiceSiae As String
Private idProvinciaSelezionata As Long
Private idComuneSelezionato As Long

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()
        
    dgrConfigurazioneVenue.Caption = "VENUE CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneVenue.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneVideo.Text = ""
        txtDescrizioneStampa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodiceSiae.Text = ""
        Call cmbProvincia.Clear
        Call cmbComune.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneVideo.Enabled = False
        txtDescrizioneStampa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodiceSiae.Enabled = False
        cmbProvincia.Enabled = False
        cmbComune.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneVideo.Enabled = False
        lblDescrizioneStampa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodiceSiae.Enabled = False
        lblProvincia.Enabled = False
        lblComune.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneVenue.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneStampa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneVideo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodiceSiae.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbComune.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And idProvinciaSelezionata <> idNessunElementoSelezionato)
        cmbProvincia.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblComune.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And idProvinciaSelezionata <> idNessunElementoSelezionato)
        lblProvincia.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneStampa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneVideo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodiceSiae.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                               Trim(txtCodiceSiae.Text) <> "" And _
                               idComuneSelezionato <> idNessunElementoSelezionato)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneVenue.Enabled = True
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneVideo.Text = ""
        txtDescrizioneStampa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodiceSiae.Text = ""
        Call cmbProvincia.Clear
        Call cmbComune.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneVideo.Enabled = False
        txtDescrizioneStampa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodiceSiae.Enabled = False
        cmbProvincia.Enabled = False
        cmbComune.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneVideo.Enabled = False
        lblDescrizioneStampa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodiceSiae.Enabled = False
        lblProvincia.Enabled = False
        lblComune.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
End Sub

Private Sub cmbProvincia_Click()
    idProvinciaSelezionata = cmbProvincia.ItemData(cmbProvincia.ListIndex)
    If Not internalEvent Then
        Call cmbProvincia_Update
    End If
End Sub

Private Sub cmbProvincia_Update()
    Dim sql As String
    
    Call cmbComune.Clear
    If idProvinciaSelezionata = idTuttiGliElementiSelezionati Then
        sql = "SELECT IDCOMUNE ID, NOME" & _
            " FROM COMUNE WHERE ATTUALE = 1 ORDER BY NOME"
    Else
        sql = "SELECT IDCOMUNE ID, NOME" & _
            " FROM COMUNE WHERE ATTUALE = 1 AND IDPROVINCIA = " & idProvinciaSelezionata & _
            " ORDER BY NOME"
    End If
    Call CaricaValoriCombo(cmbComune, sql, "NOME", False)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbComune_Click()
    idComuneSelezionato = cmbComune.ItemData(cmbComune.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCerca_Click()
    Call adcConfigurazioneVenue_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneVenue_Init
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    stringaNota = "IDVENUE = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_VENUE, , stringaNota)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneVenue_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneVenue_Init
    Else
        If ValoriCampiOK Then
            If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_VENUE, , stringaNota)
            ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_VENUE, , stringaNota)
            End If
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazioneVenue_Init
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
            Call dgrConfigurazioneVenue_Init
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sqlC As String
    Dim sqlP As String
    
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    sqlP = "SELECT IDPROVINCIA ID, SIGLA || ' - ' || NOME LABEL" & _
        " FROM PROVINCIA WHERE ATTUALE = 1 ORDER BY LABEL"
    sqlC = "SELECT IDCOMUNE ID, NOME" & _
        " FROM COMUNE WHERE ATTUALE = 1" & _
        " AND IDPROVINCIA = " & idProvinciaSelezionata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbProvincia, sqlP, "LABEL", True)
    Call CaricaValoriCombo(cmbComune, sqlC, "NOME", False)
    Call AssegnaValoriCampi
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sqlC As String
    Dim sqlP As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    sqlP = "SELECT IDPROVINCIA ID, SIGLA || ' - ' || NOME LABEL" & _
        " FROM PROVINCIA WHERE ATTUALE = 1 ORDER BY LABEL"
    sqlC = "SELECT IDCOMUNE ID, NOME" & _
        " FROM COMUNE WHERE ATTUALE = 1" & _
        " AND IDPROVINCIA = " & idProvinciaSelezionata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbProvincia, sqlP, "LABEL", True)
    Call CaricaValoriCombo(cmbComune, sqlC, "NOME", False)
    Call AssegnaValoriCampi
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    idComuneSelezionato = idNessunElementoSelezionato
    idProvinciaSelezionata = idNessunElementoSelezionato
    nomeRecordSelezionato = ""
    descrizioneRecordSelezionato = ""
    sql = "SELECT IDPROVINCIA ID, SIGLA || ' - ' || NOME LABEL" & _
        " FROM PROVINCIA WHERE ATTUALE = 1 ORDER BY LABEL"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriCombo(cmbProvincia, sql, "LABEL", True)
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sqlC As String
    Dim sqlP As String
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    sqlP = "SELECT IDPROVINCIA ID, SIGLA || ' - ' || NOME LABEL" & _
        " FROM PROVINCIA WHERE ATTUALE = 1 ORDER BY LABEL"
    sqlC = "SELECT IDCOMUNE ID, NOME" & _
        " FROM COMUNE WHERE ATTUALE = 1" & _
        " AND IDPROVINCIA = " & idProvinciaSelezionata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbProvincia, sqlP, "LABEL", True)
    Call CaricaValoriCombo(cmbComune, sqlC, "NOME", False)
    Call AssegnaValoriCampi
End Sub

Private Sub dgrConfigurazioneVenue_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    
    idProvinciaSelezionata = idNessunElementoSelezionato
    idComuneSelezionato = idNessunElementoSelezionato
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneVenue_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneVenue_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneVenue.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub adcConfigurazioneVenue_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneVenue
    
    sql = "SELECT IDVENUE ID, V.NOME ""Nome"", V.DESCRIZIONE ""Descrizione""," & _
        " DESCRIZIONEVIDEO ""Descr. Video"", DESCRIZIONESTAMPA ""Descr. Stampa""," & _
        " V.DESCRIZIONEPOS ""Descr. POS""," & _
        " CODICESIAE ""Cod. SIAE"", C.NOME ||' - '|| P.SIGLA ""Comune - Prov.""" & _
        " FROM VENUE V, COMUNE C, PROVINCIA P" & _
        " WHERE V.IDCOMUNE = C.IDCOMUNE(+)" & _
        " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
        " AND CODICESIAE LIKE '" & txtCerca.Text & "%'" & _
        " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    Set dgrConfigurazioneVenue.dataSource = d
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT V.NOME, V.DESCRIZIONE, DESCRIZIONEVIDEO, DESCRIZIONESTAMPA, CODICESIAE," & _
        " C.IDPROVINCIA, V.IDCOMUNE, V.DESCRIZIONEPOS" & _
        " FROM VENUE V, COMUNE C " & _
        " WHERE V.IDCOMUNE = C.IDCOMUNE(+)" & _
        " AND V.IDVENUE = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        descrVideoRecordSelezionato = IIf(IsNull(rec("DESCRIZIONEVIDEO")), "", rec("DESCRIZIONEVIDEO"))
        descrStampaRecordSelezionato = IIf(IsNull(rec("DESCRIZIONESTAMPA")), "", rec("DESCRIZIONESTAMPA"))
        descrPOSRecordSelezionato = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        idComuneSelezionato = IIf(IsNull(rec("IDCOMUNE")), idNessunElementoSelezionato, rec("IDCOMUNE"))
        idProvinciaSelezionata = IIf(IsNull(rec("IDPROVINCIA")), idNessunElementoSelezionato, rec("IDPROVINCIA"))
        codiceSiae = rec("CODICESIAE")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizioneStampa.Text = descrStampaRecordSelezionato
    txtDescrizioneVideo.Text = descrVideoRecordSelezionato
    txtDescrizionePOS.Text = descrPOSRecordSelezionato
    txtCodiceSiae.Text = codiceSiae
    
    Call SelezionaElementoSuCombo(cmbProvincia, idProvinciaSelezionata)
    Call SelezionaElementoSuCombo(cmbComune, idComuneSelezionato)

    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "UPDATE VENUE SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " DESCRIZIONEVIDEO = " & SqlStringValue(descrVideoRecordSelezionato) & "," & _
        " DESCRIZIONESTAMPA = " & SqlStringValue(descrStampaRecordSelezionato) & "," & _
        " DESCRIZIONEPOS = " & SqlStringValue(descrPOSRecordSelezionato) & "," & _
        " IDCOMUNE = " & idComuneSelezionato & "," & _
        " CODICESIAE = " & SqlStringValue(codiceSiae) & _
        " WHERE IDVENUE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub dgrConfigurazioneVenue_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneVenue
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 7
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneVenue.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
'                dgrConfigurazioneOrganizzazione.Row = i - 1
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

On Error Resume Next

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDVENUE <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("VENUE", "NOME = " & SqlStringValue(nomeRecordSelezionato), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrStampaRecordSelezionato = Trim(txtDescrizioneStampa.Text)
    If descrStampaRecordSelezionato <> "" Then
        If ViolataUnicitā("VENUE", "DESCRIZIONESTAMPA = " & SqlStringValue(descrStampaRecordSelezionato), "", _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore descrizione stampa = " & SqlStringValue(descrStampaRecordSelezionato) & _
                " č giā presente in DB;")
        End If
    End If
    descrVideoRecordSelezionato = Trim(txtDescrizioneVideo.Text)
    descrPOSRecordSelezionato = Trim(txtDescrizionePOS.Text)
    If descrVideoRecordSelezionato <> "" Then
        If ViolataUnicitā("VENUE", "DESCRIZIONEVIDEO = " & SqlStringValue(descrVideoRecordSelezionato), "", _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore descrizione video = " & SqlStringValue(descrVideoRecordSelezionato) & _
                " č giā presente in DB;")
        End If
    End If
    codiceSiae = Trim(txtCodiceSiae.Text)
    If Not IsNumeric(codiceSiae) Then
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiCodiceSIAE")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il codice SIAE deve essere una stringa di lunghezza non superiore a 13 caratteri composta di numeri (eventualmente preceduti da zeri);")
    End If
    If ViolataUnicitā("VENUE", "CODICESIAE = " & SqlStringValue(codiceSiae), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice SIAE = " & SqlStringValue(codiceSiae) & _
            " č giā presente in DB;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call ValorizzaCampiAutomaticamente
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub ValorizzaCampiAutomaticamente()
    txtDescrizioneVideo.Text = Left$(txtNome.Text, 20)
    txtDescrizioneStampa.Text = Left$(txtNome.Text, 20)
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizioneVideo_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizioneStampa_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCodiceSiae_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idNuovoVenue As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    idNuovoVenue = OttieniIdentificatoreDaSequenza("SQ_VENUE")
    
    sql = "INSERT INTO VENUE (IDVENUE, NOME, DESCRIZIONE, DESCRIZIONEPOS," & _
        " DESCRIZIONEVIDEO, DESCRIZIONESTAMPA, CODICESIAE, IDCOMUNE)" & _
        " VALUES (" & _
        idNuovoVenue & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        SqlStringValue(descrPOSRecordSelezionato) & ", " & _
        SqlStringValue(descrVideoRecordSelezionato) & ", " & _
        SqlStringValue(descrStampaRecordSelezionato) & ", " & _
        SqlStringValue(codiceSiae) & ", " & _
        idComuneSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoVenue)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim tabelleCorrelate As String
    Dim listaTabelleCorrelate As Collection
    Dim venueEliminabile As Boolean
    Dim n As Long

    Set listaTabelleCorrelate = New Collection
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    venueEliminabile = True
    If Not IsRecordEliminabile("IDVENUE", "VENUE_PIANTA", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("VENUE_PIANTA")
        venueEliminabile = False
    End If
    If Not IsRecordEliminabile("IDVENUE", "RAPPRESENTAZIONE", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("RAPPRESENTAZIONE")
        venueEliminabile = False
    End If

    If venueEliminabile Then
        sql = "DELETE FROM VENUE WHERE IDVENUE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il Venue selezionato", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, includiTutti As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If includiTutti Then
        If i <= 0 Then
            i = 1
        End If
        cmb.AddItem "<tutte>"
        cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

