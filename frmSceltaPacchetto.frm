VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSceltaPacchetto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVerificaCompletezza 
      Caption         =   "    Verifica completezza"
      Height          =   435
      Left            =   5820
      TabIndex        =   8
      Top             =   7920
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   1
      Top             =   7620
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdClona 
         Caption         =   "Clona"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   0
      Top             =   8100
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcOffertaPacchetto 
      Height          =   375
      Left            =   9180
      Top             =   120
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrOffertePacchettoConfigurate 
      Bindings        =   "frmSceltaPacchetto.frx":0000
      Height          =   6735
      Left            =   120
      TabIndex        =   6
      Top             =   660
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   11880
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione dell'Offerta pacchetto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   7335
   End
End
Attribute VB_Name = "frmSceltaPacchetto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idClasseProdottoSelezionato As Long
Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum

Private internalEvent As Boolean
'Private tipoStatoProdotto As TipoStatoProdottoEnum

Private Sub cmdClona_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
'    Call CaricaFormClonazioneProdotto
    
    MousePointer = mousePointerOld
End Sub
'
'Private Sub CaricaFormClonazioneProdotto()
'    Dim rec As ADODB.Recordset
'
''    Call BloccaProdottoPerUtente(idRecordSelezionato)
'    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
'    If Not isProdottoBloccatoDaUtente Then
'        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
'    Else
'        If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
'            Set rec = adcProdotto.Recordset
'            idOrganizzazioneSelezionata = rec("IDORGANIZZAZIONE")
'        End If
'        Call GetClasseProdottoRecordSelezionato
'        Call frmClonazioneProdotto.SetIdProdottoSelezionato(idRecordSelezionato)
'        Call frmClonazioneProdotto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'        Call frmClonazioneProdotto.SetIdClasseProdottoSelezionato(idClasseProdottoSelezionato)
'        Call frmClonazioneProdotto.Init
'        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
'        Call AggiornaDataGrid
'        Call dgrProdottiConfigurati_Init
'        Call SelezionaElementoSuGriglia(idRecordSelezionato)
'        Call AggiornaAbilitazioneControlli
'    End If
'End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call ModificaOffertaPacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call EliminaOffertaPacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call NuovaOffertaPacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub NuovaOffertaPacchetto()
    isOffertaPacchettoBloccataDaUtente = True
    Call frmInizialePacchetto.SetModalitāForm(A_NUOVO)
    Call frmInizialePacchetto.Init
    Call AggiornaDataGrid
    Call dgrOffertePacchettoConfigurate_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaOffertaPacchetto()
    Call frmInizialePacchetto.SetModalitāForm(A_MODIFICA)
    Call frmInizialePacchetto.SetIdOffertaPacchettoSelezionata(idRecordSelezionato)
    Call BloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idRecordSelezionato, isOffertaPacchettoBloccataDaUtente)
    If Not isOffertaPacchettoBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Call frmInizialePacchetto.Init
    Call SbloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idRecordSelezionato, isOffertaPacchettoBloccataDaUtente)
    Call AggiornaDataGrid
    Call dgrOffertePacchettoConfigurate_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub AggiornaAbilitazioneControlli()

'    dgrProdottiConfigurati.Columns(0).Visible = False
    dgrOffertePacchettoConfigurate.Caption = "OFFERTE PACCHETTO CONFIGURATE"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdVerificaCompletezza.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'    cmdClona.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdClona.Enabled = False
    If idRecordSelezionato <> idNessunElementoSelezionato Then
'        Select Case tipoStatoProdotto
'            Case TSP_IN_CONFIGURAZIONE
'                cmdVerificaCompletezza.Visible = True
'                cmdConvalidaConfigura.Caption = "Convalida"
'                cmdConvalidaConfigura.Enabled = True
'            Case TSP_ATTIVABILE
'                cmdConvalidaConfigura.Visible = True
'                cmdConvalidaConfigura.Caption = "Configura"
'                cmdConvalidaConfigura.Enabled = True
'            Case TSP_ATTIVO
'                cmdConvalidaConfigura.Visible = False
'            Case TSP_ARCHIVIATO
'                cmdConvalidaConfigura.Visible = False
'            Case Else
'        End Select
    Else
'        cmdConvalidaConfigura.Visible = False
    End If
End Sub

Private Sub AggiornaDataGrid()
    Call CaricaValoriGriglia
'    adcProdotto.Refresh
End Sub

Private Sub dgrOffertePacchettoConfigurate_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
'        Call GetStatoProdottoRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrOffertePacchettoConfigurate, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcOffertaPacchetto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub
'
'Private Sub GetStatoProdottoRecordSelezionato()
'    Dim rec As ADODB.Recordset
'
'    Set rec = adcProdotto.Recordset
'    If Not (rec.BOF) Then
'        If rec.EOF Then
'            rec.MoveFirst
'        End If
'        tipoStatoProdotto = rec("TIPOSTATOENUM")
'    Else
'        tipoStatoProdotto = TSP_NON_SPECIFICATO
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub GetClasseProdottoRecordSelezionato()
'    Dim rec As ADODB.Recordset
'
'    Set rec = adcProdotto.Recordset
'    If Not (rec.BOF) Then
'        If rec.EOF Then
'            rec.MoveFirst
'        End If
'        idClasseProdottoSelezionato = rec("CLASSE").Value
'    Else
'        idClasseProdottoSelezionato = idNessunElementoSelezionato
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub

Public Sub Init()
    idRecordSelezionato = idNessunElementoSelezionato
    Call AggiornaAbilitazioneControlli
'    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcOffertaPacchetto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrOffertePacchettoConfigurate_Init
End Sub

Private Sub adcOffertaPacchetto_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Set d = adcOffertaPacchetto
    
    sql = " SELECT IDOFFERTAPACCHETTO ID,"
    sql = sql & " O.NOME ""Nome"", O.DESCRIZIONE ""Descrizione"","
    sql = sql & " O.IDTIPOOFFERTAPACCHETTO IDTIPO, T.NOME ""Tipo"""
    sql = sql & " FROM OFFERTAPACCHETTO O, TIPOOFFERTAPACCHETTO T"
    sql = sql & " WHERE O.IDTIPOOFFERTAPACCHETTO = T.IDTIPOOFFERTAPACCHETTO"
    sql = sql & " ORDER BY ""Nome"""
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrOffertePacchettoConfigurate_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrOffertePacchettoConfigurate
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Visible = False
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcOffertaPacchetto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub EliminaOffertaPacchetto()
    Dim pacchettoEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    
    Call BloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idRecordSelezionato, isOffertaPacchettoBloccataDaUtente)
    If Not isOffertaPacchettoBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Set listaTabelleCorrelate = New Collection
    Call frmInizialePacchetto.SetModalitāForm(A_ELIMINA)
    
    pacchettoEliminabile = True
'    If Not IsRecordEliminabile("IDOFFERTAPACCHETTO", "SCELTAPRODOTTO", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("SCELTAPRODOTTO")
'        pacchettoEliminabile = False
'    End If
'    If Not IsRecordEliminabile("IDOFFERTAPACCHETTO", "TARIFFAPACCHETTO", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("TARIFFAPACCHETTO")
'        pacchettoEliminabile = False
'    End If
'    If Not IsRecordEliminabile("IDOFFERTAPACCHETTO", "AREAPACCHETTO", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("AREAPACCHETTO")
'        pacchettoEliminabile = False
'    End If
    If Not IsRecordEliminabile("IDOFFERTAPACCHETTO", "PACCHETTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PACCHETTO")
        pacchettoEliminabile = False
    End If
'    If Not IsRecordEliminabile("IDOFFERTAPACCHETTO", "OPERATORE_OFFERTAPACCHETTO", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("OPERATORE_OFFERTAPACCHETTO")
'        pacchettoEliminabile = False
'    End If
'    If Not IsRecordEliminabile("IDOFFERTAPACCHETTO", "OFFPACCHETTO_MODSCELTACOLLOCAZ", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("OFFPACCHETTO_MODSCELTACOLLOCAZ")
'        pacchettoEliminabile = False
'    End If
    
    If pacchettoEliminabile Then
        Call frmInizialePacchetto.SetIdOffertaPacchettoSelezionata(idRecordSelezionato)
        Call frmInizialePacchetto.Init
        Call AggiornaDataGrid
        Call dgrOffertePacchettoConfigurate_Init
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "L'Offerta pacchetto selezionata", tabelleCorrelate)
'        Call frmInizialeOperatore.SetOperatoreAbilitato(0)
    End If
    Call SbloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idRecordSelezionato, isOffertaPacchettoBloccataDaUtente)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
    
End Sub
'
'Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
'    Dim i As Integer
'
'    If id = idNessunElementoSelezionato Then
'        cmb.ListIndex = idNessunElementoSelezionato
'    Else
'        For i = 1 To cmb.ListCount
'            If id = cmb.ItemData(i - 1) Then
'                cmb.ListIndex = i - 1
'            End If
'        Next i
'    End If
'
'End Sub

Private Sub cmdVerificaCompletezza_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call VerificaCompletezzaOffertaPacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub VerificaCompletezzaOffertaPacchetto()
    Dim listaAttributiNonCompleti As Collection
    Dim nome As String
    
    nome = adcOffertaPacchetto.Recordset.Fields("Nome").Value
    Set listaAttributiNonCompleti = New Collection
    Call GetTipoOffertaPacchettoRecordSelezionato
    Call IsOffertaPacchettoCompleta(idRecordSelezionato, tipoOffertaPacchettoSelezionata, listaAttributiNonCompleti)
    If listaAttributiNonCompleti.count = 0 Then
        Call frmMessaggio.Visualizza("NotificaOffertaPacchettoCompleta", nome)
    Else
        Call frmMessaggio.Visualizza("AvvertimentoOffertaPacchettoIncompleta", "", _
            ArgomentoMessaggio(listaAttributiNonCompleti))
    End If
End Sub

Private Sub GetTipoOffertaPacchettoRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcOffertaPacchetto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        tipoOffertaPacchettoSelezionata = rec("IDTIPO").Value
    Else
        tipoOffertaPacchettoSelezionata = TOP_NON_SPECIFICATO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

