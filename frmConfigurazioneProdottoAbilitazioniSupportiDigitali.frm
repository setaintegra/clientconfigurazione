VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoAbilitazioniSupportiDigitali 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Abilitazioni supporti digitali per superarea"
   ClientHeight    =   11145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12975
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11145
   ScaleWidth      =   12975
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   8760
      TabIndex        =   23
      Top             =   10080
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   26
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   25
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   24
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   10080
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   22
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   21
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstSuperaree 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7410
      Left            =   180
      MultiSelect     =   2  'Extended
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   2460
      Width           =   3030
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9360
      TabIndex        =   12
      Top             =   360
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   11100
      TabIndex        =   11
      Top             =   360
      Width           =   1635
   End
   Begin VB.Frame frameSupporti 
      Caption         =   "Tipi supporti digitali consentiti per le superaree selezionate"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7695
      Left            =   3360
      TabIndex        =   0
      Top             =   2160
      Width           =   9495
      Begin VB.Frame frmPassbook 
         Caption         =   "Passbook"
         Height          =   735
         Left            =   7560
         TabIndex        =   29
         Top             =   3720
         Width           =   1695
         Begin VB.CheckBox chkPassbook 
            Caption         =   "Abilitato"
            Height          =   375
            Left            =   120
            TabIndex        =   30
            Top             =   240
            Width           =   1455
         End
      End
      Begin VB.ListBox lstClassiPuntiVenditaConObbligo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2700
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   720
         Width           =   3675
      End
      Begin VB.Frame frmTessereTifoso 
         Caption         =   "Tessere del Tifoso"
         Height          =   3855
         Left            =   120
         TabIndex        =   6
         Top             =   3720
         Width           =   3615
         Begin VB.ListBox lstTipiSupportiDigitaliTT 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2460
            Left            =   120
            Style           =   1  'Checkbox
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   1320
            Width           =   3675
         End
         Begin VB.OptionButton optTuttiTT 
            Caption         =   "Tutti"
            Height          =   195
            Left            =   240
            TabIndex        =   9
            Top             =   360
            Width           =   2175
         End
         Begin VB.OptionButton optNessunoTT 
            Caption         =   "Nessuno"
            Height          =   195
            Left            =   240
            TabIndex        =   8
            Top             =   720
            Width           =   2175
         End
         Begin VB.OptionButton optDaDettagliareTT 
            Caption         =   "Da dettagliare"
            Height          =   195
            Left            =   240
            TabIndex        =   7
            Top             =   1080
            Width           =   2175
         End
      End
      Begin VB.Frame frmCarteFidelizzazione 
         Caption         =   "Carte di Fidelizzazione"
         Height          =   3855
         Left            =   3840
         TabIndex        =   1
         Top             =   3720
         Width           =   3615
         Begin VB.OptionButton optDaDettagliareCF 
            Caption         =   "Da dettagliare"
            Height          =   195
            Left            =   240
            TabIndex        =   5
            Top             =   1080
            Width           =   2175
         End
         Begin VB.OptionButton optNessunoCF 
            Caption         =   "Nessuno"
            Height          =   195
            Left            =   240
            TabIndex        =   4
            Top             =   720
            Width           =   2175
         End
         Begin VB.OptionButton optTuttiCF 
            Caption         =   "Tutti"
            Height          =   195
            Left            =   240
            TabIndex        =   3
            Top             =   360
            Width           =   2175
         End
         Begin VB.ListBox lstTipiSupportiDigitaliCF 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2460
            Left            =   120
            Style           =   1  'Checkbox
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   1320
            Width           =   3675
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Classi punti vendita con obbligo di identificazione da supporto digitale"
         Height          =   315
         Left            =   120
         TabIndex        =   28
         Top             =   480
         Width           =   6075
      End
   End
   Begin VB.Label Label3 
      Caption         =   "L'informazione � utilizzata sia per la fase di identificazione che per l'emissione del titolo."
      Height          =   315
      Left            =   240
      TabIndex        =   19
      Top             =   1080
      Width           =   10635
   End
   Begin VB.Label lblSuperaree 
      Alignment       =   2  'Center
      Caption         =   "Superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   240
      TabIndex        =   18
      Top             =   2160
      Width           =   2955
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Abilitazioni supporti digitali per superarea"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   240
      Width           =   6375
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   9360
      TabIndex        =   16
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   11100
      TabIndex        =   15
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label Label2 
      Caption         =   "Attenzione: i tipi supporti considerati sono soltanto quelli permessi a livello di organizzazione."
      Height          =   315
      Left            =   240
      TabIndex        =   14
      Top             =   1440
      Width           =   10635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoAbilitazioniSupportiDigitali"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idClasseProdottoSelezionata As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private rateo As Long

Private gestioneExitCode As ExitCodeEnum
Private modalitaFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private idSuperAreaSelezionata As Long
Private stringaListaSuperareeSelezionate As String
Private quantitaSuperareeSelezionate As Long

'Variabili inizializzate all'apertura per evitare tante query
Private tutteTTperOrg As Boolean
Private tutteCFperOrg As Boolean
Private tuttiPBperOrg As Boolean
Private listaTTperOrg As Collection
Private listaCFperOrg As Collection

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    Select Case modalitaFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Successivo"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaInformazioniOrganizzazione
    Call CaricaValoriLst_Superaree
    Call CancellaSelezioni
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalitaFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    criteriSelezioneImpostati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitaFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Successivo()
    Call CaricaFormModalitaFornitura
End Sub

Private Sub CaricaFormModalitaFornitura()
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetRateo(rateo)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoModalitaFornitura.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoModalitaFornitura.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoModalitaFornitura.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoModalitaFornitura.Init
End Sub

Private Sub CaricaInformazioniOrganizzazione()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    lstSuperaree.Clear
    
    Call ApriConnessioneBD_ORA
    
    tutteTTperOrg = False
    tutteCFperOrg = False
    tuttiPBperOrg = False
    
    sql = "SELECT DISTINCT IDTIPOSUPPORTOSIAE" & _
        " FROM TSDIGCONSENTITOPERORG" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND IDTIPOSUPPORTOSIAE IS NOT NULL"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            If rec("IDTIPOSUPPORTOSIAE") = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
                tutteTTperOrg = True
            End If
            If rec("IDTIPOSUPPORTOSIAE") = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Then
                tutteCFperOrg = True
            End If
            If rec("IDTIPOSUPPORTOSIAE") = IDTIPOSUPPORTOSIAE_PASSBOOK Then
                tuttiPBperOrg = True
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Set listaTTperOrg = New Collection
    Set listaCFperOrg = New Collection
    
    sql = "SELECT TS.IDTIPOSUPPORTO, TS.IDTIPOSUPPORTOSIAE" & _
        " FROM TSDIGCONSENTITOPERORG TSDCPO, TIPOSUPPORTO TS" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND TSDCPO.IDTIPOSUPPORTOSIAE IS NULL" & _
        " AND TSDCPO.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            If rec("IDTIPOSUPPORTOSIAE") = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
                Call listaTTperOrg.Add(rec("IDTIPOSUPPORTOSIAE"))
            End If
            If rec("IDTIPOSUPPORTOSIAE") = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Then
                Call listaCFperOrg.Add(rec("IDTIPOSUPPORTOSIAE"))
            End If
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub CaricaValoriLst_Superaree()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    lstSuperaree.Clear
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT DISTINCT IDAREA ID, CODICE, NOME" & _
        " FROM AREA" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
        " ORDER BY CODICE"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstSuperaree.AddItem rec("NOME")
            lstSuperaree.ItemData(i) = rec("ID")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub InizializzaListaSuperareeSelezionate()
    Dim i As Long
    Dim idSuperarea As Long

    stringaListaSuperareeSelezionate = ""
    quantitaSuperareeSelezionate = 0
    For i = 0 To lstSuperaree.ListCount - 1
        If lstSuperaree.Selected(i) Then
            idSuperarea = lstSuperaree.ItemData(i)
            stringaListaSuperareeSelezionate = stringaListaSuperareeSelezionate & idSuperarea & ","
            quantitaSuperareeSelezionate = quantitaSuperareeSelezionate + 1
        End If
    Next i

    If stringaListaSuperareeSelezionate <> "" Then
        stringaListaSuperareeSelezionate = Left(stringaListaSuperareeSelezionate, Len(stringaListaSuperareeSelezionate) - 1)
    End If

End Sub

Private Sub CancellaSelezioni()
    Dim i As Long
    
    For i = 1 To lstClassiPuntiVenditaConObbligo.ListCount
        lstClassiPuntiVenditaConObbligo.Selected(i - 1) = False
    Next i
    lstClassiPuntiVenditaConObbligo.Enabled = False
    
    For i = 1 To lstSuperaree.ListCount
        lstSuperaree.Selected(i - 1) = False
    Next i
    
    optTuttiTT.Value = False
    optNessunoTT.Value = False
    optDaDettagliareTT.Value = False
    optTuttiCF.Value = False
    optNessunoCF.Value = False
    optDaDettagliareCF.Value = False
    chkPassbook.Value = VB_FALSO
    
    For i = 1 To lstTipiSupportiDigitaliTT.ListCount
        lstTipiSupportiDigitaliTT.Selected(i - 1) = False
    Next i
    
    For i = 1 To lstTipiSupportiDigitaliCF.ListCount
        lstTipiSupportiDigitaliCF.Selected(i - 1) = False
    Next i
    
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call InserisciNellaBaseDati
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub
'
'Private Sub listaSuperaree_Init()
'    Dim internalEventOld As Boolean
'    Dim i As Integer
'    Dim superarea As clsElementoLista
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    lstSuperaree.Clear
'
'    If Not (listaSuperaree Is Nothing) Then
'        i = 1
'        For Each superarea In listaSuperaree
'            lstSuperaree.AddItem superarea.descrizioneElementoLista
'            lstSuperaree.ItemData(i - 1) = superarea.idElementoLista
'            i = i + 1
'        Next superarea
'    End If
'
'    internalEvent = internalEventOld
'
'End Sub

' Tabella TSDIGOBBLIGOIDENTIFICAZIONE:
' - se non c'e' riga non esiste alcun obbligo
' - se c'e' una riga con IDCLASSEPUNTOVENDITA pari a NULL esiste l'obbligo di identificazione per TUTTE le classi punti vendita
' - se l'obbligo esiste solo per qualche classe di punti vendita devono esistere tante righe,
'   una per ogni classe punto vendita
Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idSuperarea As Long
    Dim idClassePuntoVendita As Long
    Dim idTipoSupporto As Long
    Dim i As Long
    Dim j As Long
    
    Call InizializzaListaSuperareeSelezionate
    
    If stringaListaSuperareeSelezionate = "" Then
        MsgBox "Nessuna superarea selezionata"
    Else
        Call ApriConnessioneBD_ORA

On Error GoTo errori_TitoliDigitali_InserisciNellaBaseDati

        ORADB.BeginTrans
        
        If lstSuperaree.ListCount > 0 Then
            sql = "DELETE FROM TSDIGOBBLIGOIDENTIFICAZIONE" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")"
            n = ORADB.ExecuteSQL(sql)
            
            If tutteLeCPVSelezionate() Then
                ' una sola riga per superarea con idCPV = NULL
                For i = 0 To lstSuperaree.ListCount - 1
                    If lstSuperaree.Selected(i) = True Then
                        idSuperarea = lstSuperaree.ItemData(i)
                        
                        sql = "INSERT INTO TSDIGOBBLIGOIDENTIFICAZIONE (IDPRODOTTO, IDSUPERAREA, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & idSuperarea & ", NULL)"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            Else
                ' una riga per superarea e CPV solo per le CPV selezionate
                For i = 0 To lstSuperaree.ListCount - 1
                    If lstSuperaree.Selected(i) = True Then
                        idSuperarea = lstSuperaree.ItemData(i)
                        
                        For j = 0 To lstClassiPuntiVenditaConObbligo.ListCount - 1
                            If lstClassiPuntiVenditaConObbligo.Selected(j) = True Then
                                idClassePuntoVendita = lstClassiPuntiVenditaConObbligo.ItemData(j)
                        
                                sql = "INSERT INTO TSDIGOBBLIGOIDENTIFICAZIONE (IDPRODOTTO, IDSUPERAREA, IDCLASSEPUNTOVENDITA)" & _
                                    " VALUES (" & idProdottoSelezionato & ", " & idSuperarea & ", " & idClassePuntoVendita & ")"
                                n = ORADB.ExecuteSQL(sql)
                            End If
                        Next j
                    End If
                Next i
            End If

        End If

        If optTuttiTT.Value = False And optNessunoTT.Value = False And optDaDettagliareTT.Value = False Then
             ' non � stato indicato nulla lascio le TT come stanno
        Else
            sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")" & _
                " AND IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO
            n = ORADB.ExecuteSQL(sql)
            
            sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")" & _
                " AND IDTIPOSUPPORTO IN (" & _
                " SELECT IDTIPOSUPPORTO FROM TIPOSUPPORTO WHERE IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO & _
                ")"
            n = ORADB.ExecuteSQL(sql)
        End If

        If optTuttiCF.Value = False And optNessunoCF.Value = False And optDaDettagliareCF.Value = False Then
             ' non � stato indicato nulla lascio le CF come stanno
        Else
            sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")" & _
                " AND IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE
            n = ORADB.ExecuteSQL(sql)
            
            sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")" & _
                " AND IDTIPOSUPPORTO IN (" & _
                " SELECT IDTIPOSUPPORTO FROM TIPOSUPPORTO WHERE IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE & _
                ")"
            n = ORADB.ExecuteSQL(sql)
        End If

        ' cancello comunque le info su passbook
        sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")" & _
            " AND IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_PASSBOOK
        n = ORADB.ExecuteSQL(sql)

        For i = 0 To lstSuperaree.ListCount - 1
            If lstSuperaree.Selected(i) = True Then
                idSuperarea = lstSuperaree.ItemData(i)

                If optTuttiTT.Value = True Then
                    sql = "INSERT INTO TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                        " SELECT TSDCPO.IDTIPOSUPPORTOSIAE, TSDCPO.IDTIPOSUPPORTO, " & idProdottoSelezionato & ", " & idSuperarea & _
                        " FROM TSDIGCONSENTITOPERORG TSDCPO, TIPOSUPPORTO TS" & _
                        " WHERE TSDCPO.idOrganizzazione = " & idOrganizzazioneSelezionata & _
                        " AND TSDCPO.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO(+)" & _
                        " AND (TSDCPO.IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO & " OR TS.IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO & ")"
                    n = ORADB.ExecuteSQL(sql)
                End If

                If optDaDettagliareTT.Value = True Then
                    For j = 0 To lstTipiSupportiDigitaliTT.ListCount - 1
                        If lstTipiSupportiDigitaliTT.Selected(j) = True Or optTuttiTT.Value = True Then
                            idTipoSupporto = lstTipiSupportiDigitaliTT.ItemData(j)
                            sql = "INSERT INTO TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                                " VALUES (NULL, " & idTipoSupporto & ", " & idProdottoSelezionato & ", " & idSuperarea & ")"
                            n = ORADB.ExecuteSQL(sql)
                        End If
                    Next j
                End If

                If optTuttiCF.Value = True Then
                    sql = "INSERT INTO TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                        " SELECT TSDCPO.IDTIPOSUPPORTOSIAE, TSDCPO.IDTIPOSUPPORTO, " & idProdottoSelezionato & ", " & idSuperarea & _
                        " FROM TSDIGCONSENTITOPERORG TSDCPO, TIPOSUPPORTO TS" & _
                        " WHERE TSDCPO.idOrganizzazione = " & idOrganizzazioneSelezionata & _
                        " AND TSDCPO.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO(+)" & _
                        " AND (TSDCPO.IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE & " OR TS.IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE & ")"
                    n = ORADB.ExecuteSQL(sql)
                End If

                If optDaDettagliareCF.Value = True Then
                    For j = 0 To lstTipiSupportiDigitaliCF.ListCount - 1
                        If lstTipiSupportiDigitaliCF.Selected(j) = True Or optTuttiCF.Value = True Then
                            idTipoSupporto = lstTipiSupportiDigitaliCF.ItemData(j)
                            sql = "INSERT INTO TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                                " VALUES (NULL, " & idTipoSupporto & ", " & idProdottoSelezionato & ", " & idSuperarea & ")"
                            n = ORADB.ExecuteSQL(sql)
                        End If
                    Next j
                End If
                
                If chkPassbook.Value = VB_VERO Then
                    sql = "INSERT INTO TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                        " VALUES (" & IDTIPOSUPPORTOSIAE_PASSBOOK & ", NULL, " & idProdottoSelezionato & ", " & idSuperarea & ")"
                    n = ORADB.ExecuteSQL(sql)
                End If

            End If
        Next i

        ORADB.CommitTrans

        Call ChiudiConnessioneBD_ORA
    End If

    Call AggiornaAbilitazioneControlli
    Call CancellaSelezioni

    Exit Sub

errori_TitoliDigitali_InserisciNellaBaseDati:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    Call CancellaSelezioni

End Sub

Private Sub lstSuperaree_Click()
    Dim numeroElementiSelezionati As Long
    
    numeroElementiSelezionati = lstSuperaree.SelCount
    
    If numeroElementiSelezionati = 1 Then
        idSuperAreaSelezionata = lstSuperaree.ItemData(lstSuperaree.ListIndex)
        Call SelezionaSuperArea
    ElseIf numeroElementiSelezionati > 1 Then         ' multiselezione
        idSuperAreaSelezionata = idNessunElementoSelezionato
        optTuttiTT.Value = False
        optNessunoTT.Value = False
        optDaDettagliareTT.Value = False
        lstTipiSupportiDigitaliTT.Clear
        optTuttiCF.Value = False
        optNessunoCF.Value = False
        optDaDettagliareCF.Value = False
        lstTipiSupportiDigitaliCF.Clear
        chkPassbook.Value = VB_FALSO
    End If
    
End Sub

Private Sub optDaDettagliareTT_Click()
    PopolaListaTipiSupporto (IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
    lstTipiSupportiDigitaliTT.Enabled = True
End Sub

Private Sub optNessunoTT_Click()
    PopolaListaTipiSupporto (IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
    lstTipiSupportiDigitaliTT.Enabled = False
End Sub

Private Sub optTuttiTT_Click()
    PopolaListaTipiSupporto (IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
    lstTipiSupportiDigitaliTT.Enabled = False
End Sub

Private Sub optDaDettagliareCF_Click()
    PopolaListaTipiSupporto (IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE)
    lstTipiSupportiDigitaliCF.Enabled = True
End Sub

Private Sub optNessunoCF_Click()
    PopolaListaTipiSupporto (IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE)
    lstTipiSupportiDigitaliCF.Enabled = False
End Sub

Private Sub optTuttiCF_Click()
    PopolaListaTipiSupporto (IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE)
    lstTipiSupportiDigitaliCF.Enabled = False
End Sub

Private Sub SelezionaSuperArea()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim tutteTT As Boolean
    Dim tutteCF As Boolean
    Dim tuttiPB As Boolean
    Dim numeroTTPresenti As Long
    Dim numeroCFPresenti As Long
    
    lstTipiSupportiDigitaliTT.Clear
    lstTipiSupportiDigitaliCF.Clear
    optNessunoTT.Value = True
    optNessunoCF.Value = True
    
    Call PopolaListaClassiPuntiVendita

    Call ApriConnessioneBD_ORA
    
' informazioni sull'obbligatoriet� del supporto digitale
    sql = "SELECT TSS.IDTIPOSUPPORTOSIAE IDTSS" & _
        " FROM TIPOSUPPORTOSIAE TSS, TSDIGCONSENTITOPERORG TSO, TSDIGCONSENTITOPERSUPERAREA TSC" & _
        " WHERE TSS.IDTIPOSUPPORTOSIAE IN (" & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE & ", " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO & ")" & _
        " AND TSS.IDTIPOSUPPORTOSIAE = TSO.IDTIPOSUPPORTOSIAE" & _
        " AND TSO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND TSS.IDTIPOSUPPORTOSIAE = TSC.IDTIPOSUPPORTOSIAE" & _
        " AND TSC.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND TSC.IDSUPERAREA = " & idSuperAreaSelezionata & _
        " AND TSC.IDTIPOSUPPORTO IS NULL"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("IDTSS") = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Then
                optTuttiCF.Value = True
                lstTipiSupportiDigitaliCF.Enabled = False
            End If
            If rec("IDTSS") = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
                optTuttiTT.Value = True
                lstTipiSupportiDigitaliTT.Enabled = False
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close

    tutteTT = VerificaTuttiTTS(IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
    tutteCF = VerificaTuttiTTS(IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE)
    tuttiPB = VerificaPassbook
    numeroTTPresenti = NumeroDettagliPresenti(IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
    numeroCFPresenti = NumeroDettagliPresenti(IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE)
    
    If tutteTTperOrg = False And listaTTperOrg.count = 0 Then
        optTuttiTT.Enabled = False
        optDaDettagliareTT.Enabled = False
    End If
    If tutteCFperOrg = False And listaCFperOrg.count = 0 Then
        optTuttiCF.Enabled = False
        optDaDettagliareCF.Enabled = False
    End If
    If tuttiPBperOrg = False Then
        chkPassbook.Enabled = False
    End If
    
    If tutteTT Or numeroTTPresenti > 0 And numeroTTPresenti = listaTTperOrg.count Then
        optTuttiTT.Value = True
        Call PopolaListaTipiSupporto(IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
        lstTipiSupportiDigitaliTT.Enabled = False
    ElseIf tutteTT = False And numeroTTPresenti = 0 Then
        optNessunoTT.Value = True
    Else
        optDaDettagliareTT.Value = True
    End If
    If tutteTT = False And numeroTTPresenti > 0 Then
        Call PopolaListaTipiSupporto(IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO)
    End If
    
    If tutteCF Or numeroCFPresenti > 0 And numeroCFPresenti = listaCFperOrg.count Then
        optTuttiCF.Value = True
    ElseIf tutteCF = False And numeroCFPresenti = 0 Then
        optNessunoCF.Value = True
    Else
        optDaDettagliareCF.Value = True
    End If
    If tutteCF = False And numeroCFPresenti > 0 Then
        Call PopolaListaTipiSupporto(IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE)
    End If
    
    If tuttiPB Then
        chkPassbook.Value = VB_VERO
    Else
        chkPassbook.Value = VB_FALSO
    End If
    
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub PopolaListaTipiSupporto(tipoSIAE As Integer)
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long

' considero solo 6 e 8
    If tipoSIAE = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Or tipoSIAE = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Then
        Call ApriConnessioneBD_ORA
        
        If tipoSIAE = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
            Call lstTipiSupportiDigitaliTT.Clear
        End If
        If tipoSIAE = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Then
            Call lstTipiSupportiDigitaliCF.Clear
        End If
                        
        sql = "SELECT TS.IDTIPOSUPPORTO IDTSS, TS.NOME NOME, TS.IDTIPOSUPPORTOSIAE IDTIPOSUPPORTOSIAE, TSC.IDTIPOSUPPORTO ABILITATO" & _
            " FROM TIPOSUPPORTO TS, TSDIGCONSENTITOPERORG TSO, TSDIGCONSENTITOPERSUPERAREA TSC" & _
            " WHERE TS.IDTIPOSUPPORTOSIAE = " & tipoSIAE & _
            " AND (TS.IDTIPOSUPPORTOSIAE = TSO.IDTIPOSUPPORTOSIAE OR TS.IDTIPOSUPPORTO = TSO.IDTIPOSUPPORTO)" & _
            " AND TSO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND TS.IDTIPOSUPPORTO = TSC.IDTIPOSUPPORTO(+)" & _
            " AND TSC.IDPRODOTTO(+) = " & idProdottoSelezionato & _
            " AND TSC.IDSUPERAREA(+) = " & idSuperAreaSelezionata & _
            " AND TSC.IDTIPOSUPPORTOSIAE(+) IS NULL"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 0
            While Not rec.EOF
                If tipoSIAE = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
                    If rec("IDTIPOSUPPORTOSIAE") = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
                        ' va aggiunto alla lista
                        Call lstTipiSupportiDigitaliTT.AddItem(rec("NOME"), i)
                        If optTuttiTT.Value = True Then
                            lstTipiSupportiDigitaliTT.Selected(i) = True
                        ElseIf optNessunoTT.Value = True Then
                            lstTipiSupportiDigitaliTT.Selected(i) = False
                        Else
                            If IsNull(rec("ABILITATO")) Then
                                lstTipiSupportiDigitaliTT.Selected(i) = False
                            Else
                                lstTipiSupportiDigitaliTT.Selected(i) = True
                            End If
                        End If
                        lstTipiSupportiDigitaliTT.ItemData(i) = rec("IDTSS")
                    End If
                Else
                    ' va aggiunto alla lista
                    Call lstTipiSupportiDigitaliCF.AddItem(rec("NOME"), i)
                    If optTuttiCF.Value = True Then
                        lstTipiSupportiDigitaliCF.Selected(i) = True
                    ElseIf optNessunoCF.Value = True Then
                        lstTipiSupportiDigitaliCF.Selected(i) = False
                    Else
                        If IsNull(rec("ABILITATO")) Then
                            lstTipiSupportiDigitaliCF.Selected(i) = False
                        Else
                            lstTipiSupportiDigitaliCF.Selected(i) = True
                        End If
                    End If
                    lstTipiSupportiDigitaliCF.ItemData(i) = rec("IDTSS")
                End If
    
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
    
        Call ChiudiConnessioneBD_ORA
    End If

End Sub

Private Sub PopolaListaClassiPuntiVendita()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim cont As Long

    lstClassiPuntiVenditaConObbligo.Clear
    lstClassiPuntiVenditaConObbligo.Enabled = True
    
    Call ApriConnessioneBD_ORA

    sql = "SELECT COUNT(*) CONT" & _
        " FROM TSDIGOBBLIGOIDENTIFICAZIONE OI" & _
        " WHERE OI.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND OI.IDSUPERAREA = " & idSuperAreaSelezionata & _
        " AND OI.IDCLASSEPUNTOVENDITA IS NULL"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            cont = rec("CONT")
            rec.MoveNext
        Wend
    End If
    rec.Close

    If cont = 1 Then
        ' obbligo per tutte le classi di punti vendita
        sql = "SELECT CPV.IDCLASSEPUNTOVENDITA IDCPV, CPV.NOME" & _
            " FROM CLASSEPUNTOVENDITA CPV" & _
            " ORDER BY CPV.IDCLASSEPUNTOVENDITA"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 0
            While Not rec.EOF
                Call lstClassiPuntiVenditaConObbligo.AddItem(rec("NOME"), i)
                lstClassiPuntiVenditaConObbligo.ItemData(i) = rec("IDCPV")
                lstClassiPuntiVenditaConObbligo.Selected(i) = True
                
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
    ElseIf cont = 0 Then
        sql = "SELECT CPV.IDCLASSEPUNTOVENDITA IDCPV, CPV.NOME, OI.IDCLASSEPUNTOVENDITA IDOI" & _
            " FROM CLASSEPUNTOVENDITA CPV, TSDIGOBBLIGOIDENTIFICAZIONE OI" & _
            " WHERE CPV.IDCLASSEPUNTOVENDITA = OI.IDCLASSEPUNTOVENDITA(+)" & _
            " AND OI.IDPRODOTTO(+) = " & idProdottoSelezionato & _
            " AND OI.IDSUPERAREA(+) = " & idSuperAreaSelezionata & _
            " ORDER BY CPV.IDCLASSEPUNTOVENDITA"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 0
            While Not rec.EOF
                Call lstClassiPuntiVenditaConObbligo.AddItem(rec("NOME"), i)
                lstClassiPuntiVenditaConObbligo.ItemData(i) = rec("IDCPV")
                
                If Not IsNull(rec("IDOI")) Then
                    lstClassiPuntiVenditaConObbligo.Selected(i) = True
                End If
    
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
    Else
        MsgBox "Errore nella configurazione delle obbligatorieta'"
    End If

    Call ChiudiConnessioneBD_ORA

End Sub

Private Function EsistonoDettagli(tipoSIAE As Integer) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Long

    Call ApriConnessioneBD_ORA

    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM TIPOSUPPORTO TS, TSDIGCONSENTITOPERORG TSO, TSDIGCONSENTITOPERSUPERAREA TSC" & _
        " WHERE TS.IDTIPOSUPPORTOSIAE = " & tipoSIAE & _
        " AND (TS.IDTIPOSUPPORTOSIAE = TSO.IDTIPOSUPPORTOSIAE OR TS.IDTIPOSUPPORTO = TSO.IDTIPOSUPPORTO)" & _
        " AND TSO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND TS.IDTIPOSUPPORTO = TSC.IDTIPOSUPPORTO" & _
        " AND TSC.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND TSC.IDSUPERAREA = " & idSuperAreaSelezionata & _
        " AND TSC.IDTIPOSUPPORTOSIAE IS NULL"

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        cont = rec("CONT")
        If cont = 0 Then
            EsistonoDettagli = False
        Else
            EsistonoDettagli = True
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
End Function

Private Function VerificaTuttiTTS(tipoSIAE As Integer) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Long

    Call ApriConnessioneBD_ORA

    sql = "SELECT COUNT(*) CONT" & _
        " FROM TSDIGCONSENTITOPERSUPERAREA" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDSUPERAREA = " & idSuperAreaSelezionata & _
        " AND IDTIPOSUPPORTOSIAE = " & tipoSIAE

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        cont = rec("CONT")
        If cont = 0 Then
            VerificaTuttiTTS = False
        Else
            VerificaTuttiTTS = True
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
    
End Function

Private Function VerificaPassbook() As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Long

    Call ApriConnessioneBD_ORA

    sql = "SELECT COUNT(*) CONT" & _
        " FROM TSDIGCONSENTITOPERSUPERAREA" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDSUPERAREA = " & idSuperAreaSelezionata & _
        " AND IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_PASSBOOK

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        cont = rec("CONT")
        If cont = 0 Then
            VerificaPassbook = False
        Else
            VerificaPassbook = True
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
    
End Function

Private Function NumeroDettagliPresenti(tipoSIAE As Integer) As Long
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Long

    Call ApriConnessioneBD_ORA

    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM TIPOSUPPORTO TS, TSDIGCONSENTITOPERORG TSO, TSDIGCONSENTITOPERSUPERAREA TSC" & _
        " WHERE TS.IDTIPOSUPPORTOSIAE = " & tipoSIAE & _
        " AND (TS.IDTIPOSUPPORTOSIAE = TSO.IDTIPOSUPPORTOSIAE OR TS.IDTIPOSUPPORTO = TSO.IDTIPOSUPPORTO)" & _
        " AND TSO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND TS.IDTIPOSUPPORTO = TSC.IDTIPOSUPPORTO" & _
        " AND TSC.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND TSC.IDSUPERAREA = " & idSuperAreaSelezionata & _
        " AND TSC.IDTIPOSUPPORTOSIAE IS NULL"

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        cont = rec("CONT")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
    
    NumeroDettagliPresenti = cont
End Function

Private Function tutteLeCPVSelezionate() As Boolean
    Dim i As Long
    
    i = 0
    tutteLeCPVSelezionate = True
    
    While i < lstClassiPuntiVenditaConObbligo.ListCount And tutteLeCPVSelezionate
        If lstClassiPuntiVenditaConObbligo.Selected(i) = False Then
            tutteLeCPVSelezionate = False
        End If
        i = i + 1
    Wend

End Function

Private Sub SelezionaTutteLeClassiPuntiVendita()
    Dim i As Long
    
    For i = 1 To lstClassiPuntiVenditaConObbligo.ListCount
        lstClassiPuntiVenditaConObbligo.Selected(i) = True
    Next i

End Sub
