VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazioneProdottoPrezzi 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "11"
   Begin VB.CommandButton cmdEliminaTuttiPrezzi 
      Caption         =   "Elimina"
      Height          =   315
      Left            =   4680
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   900
      Width           =   1035
   End
   Begin MSComctlLib.ProgressBar pgbAvanzamento 
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   7320
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   14
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraSceltaPeriodoCommerciale 
      Caption         =   "Scelta Periodo Commerciale"
      Height          =   795
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Width           =   4155
      Begin VB.CommandButton cmdCarica 
         Caption         =   "Carica"
         Height          =   315
         Left            =   2940
         TabIndex        =   1
         Top             =   300
         Width           =   1035
      End
      Begin VB.ComboBox cmbPeriodoCommerciale 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   300
         Width           =   2655
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   9
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   8
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   7
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoPrezzi 
      Height          =   330
      Left            =   240
      Top             =   6780
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoPrezzi 
      Height          =   5715
      Left            =   120
      TabIndex        =   15
      Top             =   1500
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10081
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      WrapCellPointer =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   "000;(000)"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   "000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   2
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblEliminaTuttiPrezzi 
      Caption         =   "tutti i prezzi del periodo commerciale"
      Height          =   255
      Left            =   5760
      TabIndex        =   18
      Top             =   960
      Width           =   2715
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   12
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   11
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Prezzi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   5415
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoPrezzi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private isProdottoTMaster As ValoreBooleanoEnum
Private IsProdottoTDL As ValoreBooleanoEnum

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idPeriodoCommercialeSelezionato As Long
Private idTipoSupportoSelezionato As Long
Private idLayoutSupportoSelezionato As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long

Private listaNomiTariffe As Collection
Private listaIdTariffe As Collection
Private listaIdTariffeOmaggio As Collection
Private listaNomiTariffeOmaggio As Collection
Private listaIdTariffeOmaggioConRiferimento As Collection
Private listaNomiTariffeOmaggioConRiferimento As Collection
Private listaIdTariffeServizio As Collection
Private listaNomiTariffeServizio As Collection
Private listaIdAppoggioPrezzi As Collection
Private listaNomeAppoggioPrezzi As Collection
Private listaPrevenditaAppoggioPrezzi As Collection
Private listaIdArea As Collection
Private listaNomeArea As Collection
Private isDefinitoAlmenoUnPrezzo As Boolean
Private listaCompletezzaPeriodiCommerciali As Collection
Private isProdottoAttivoSuTL As Boolean
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String
Private idSuperareaServizio As Long
Private idTariffaIngressoAbbonato As Long
Private listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente As Collection
Private listaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente As Collection

'Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private tipoArea As TipoAreaEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    dgrConfigurazioneProdottoPrezzi.Caption = "PREZZI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoPrezzi.Enabled = False
        dgrConfigurazioneProdottoPrezzi.AllowUpdate = False
        dgrConfigurazioneProdottoPrezzi.Columns(1).Locked = True
        lblEliminaTuttiPrezzi.Enabled = False
        cmbPeriodoCommerciale.Enabled = True
        cmdCarica.Enabled = (cmbPeriodoCommerciale.ListIndex <> idNessunElementoSelezionato)
        cmdEliminaTuttiPrezzi.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoPrezzi.Enabled = True
        dgrConfigurazioneProdottoPrezzi.AllowUpdate = True
        dgrConfigurazioneProdottoPrezzi.Columns(1).Locked = True
        lblEliminaTuttiPrezzi.Enabled = True
        cmbPeriodoCommerciale.Enabled = False
        cmdCarica.Enabled = False
        cmdEliminaTuttiPrezzi.Enabled = True
        cmdConferma.Enabled = True
        cmdAnnulla.Enabled = True
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoPrezzi.Enabled = False
        dgrConfigurazioneProdottoPrezzi.AllowUpdate = False
        dgrConfigurazioneProdottoPrezzi.Columns(1).Locked = True
        lblEliminaTuttiPrezzi.Enabled = False
        cmbPeriodoCommerciale.Enabled = True
        cmdCarica.Enabled = (cmbPeriodoCommerciale.ListIndex <> idNessunElementoSelezionato)
        cmdEliminaTuttiPrezzi.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)) And _
                                    (IsDefinitoAlmenoUnPrezzoPerPeriodoCommerciale)
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
    If isProdottoTMaster Or IsProdottoTDL Then
        cmdEliminaTuttiPrezzi.Enabled = False
        cmdConferma.Enabled = False
    End If
    
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIsProdottoTMaster(b As ValoreBooleanoEnum)
    isProdottoTMaster = b
End Sub

Public Sub SetIsProdottoTDL(b As ValoreBooleanoEnum)
    IsProdottoTDL = b
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoPrezzi_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call dgrConfigurazioneProdottoPrezzi_Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCarica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Carica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Carica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CreaTabellaAppoggioPrezzi
    Call adcConfigurazioneProdottoPrezzi_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoPrezzi_Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            Select Case gestioneRecordGriglia
                Case ASG_MODIFICA
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PREZZO, stringaNota, idProdottoSelezionato)
                    Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_PREZZO, stringaNota, idProdottoSelezionato)
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PREZZO, stringaNota, idProdottoSelezionato)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Case Else
                    'Do Nothing
            End Select
            Call IsDefinitoAlmenoUnPrezzoPerPeriodoCommerciale
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdEliminaTuttiPrezzi_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaTuttiPrezziPerPeriodoCommerciale
    
    MousePointer = mousePointerOld
End Sub

Private Sub EliminaTuttiPrezziPerPeriodoCommerciale()
    Dim sql As String
    Dim n As Long
    Dim condizioniSQL As String
    Dim stringaNota As String
    
    Call frmMessaggio.Visualizza("ConfermaEliminazionePrezzi")
    If frmMessaggio.exitCode = EC_CONFERMA Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''            If tipoStatoRecordSelezionato = TSR_NUOVO Then
''                sql = "DELETE FROM PREZZOTITOLOPRODOTTO" & _
''                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
''                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
''                SETAConnection.Execute sql, n, adCmdText
''            Else
''                condizioniSql = " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
''                Call AggiornaParametriSessioneSuRecord("PREZZOTITOLOPRODOTTO", "IDPERIODOCOMMERCIALE", idProdottoSelezionato, TSR_ELIMINATO, condizioniSql)
''            End If
'            sql = "DELETE FROM PREZZOTITOLOPRODOTTO" & _
'                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
'                " AND IDTIPOSTATORECORD = " & TSR_NUOVO
'            SETAConnection.Execute sql, n, adCmdText
'            condizioniSQL = " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
'            Call AggiornaParametriSessioneSuRecord("PREZZOTITOLOPRODOTTO", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO, condizioniSQL)
'        Else
            sql = "DELETE FROM PREZZOTITOLOPRODOTTO" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
        stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PREZZO, stringaNota, idProdottoSelezionato)
        Call CreaTabellaAppoggioPrezzi
        Call adcConfigurazioneProdottoPrezzi_Init
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneProdottoPrezzi_Init
        Call SetGestioneExitCode(EC_CONFERMA)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call EliminaTabellaAppoggioPrezzi
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioPrezzi
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioPrezzi
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoPrezzi.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("IDAREA") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub Successivo()
    Call CaricaFormAssociazioniTipiLayoutSupporto
    Call EliminaTabellaAppoggioPrezzi
End Sub

Private Sub CaricaFormAssociazioniTipiLayoutSupporto()
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetRateo(rateo)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.Init
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneProdottoPrezzi_ButtonClick(ByVal ColIndex As Integer)
    Dim b As Button
    
'    b.Style = tbrCheck
End Sub

Private Sub dgrConfigurazioneProdottoPrezzi_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Dim sql As String

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Set listaCompletezzaPeriodiCommerciali = New Collection
    Call AggiornaAbilitazioneControlli
    Call RilevaNomiTariffe
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT PC.IDPERIODOCOMMERCIALE AS ID, PC.NOME AS NOME," & _
'            " COUNT(PTP.IMPORTOBASE) AS CONTEGGIO" & _
'            " FROM PERIODOCOMMERCIALE PC, PREZZOTITOLOPRODOTTO PTP" & _
'            " WHERE PC.IDPERIODOCOMMERCIALE = PTP.IDPERIODOCOMMERCIALE(+)" & _
'            " AND PC.IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (PC.IDTIPOSTATORECORD IS NULL" & _
'            " OR PC.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " GROUP BY PC.IDPERIODOCOMMERCIALE, PC.NOME" & _
'            " ORDER BY NOME"
'    Else
        sql = "SELECT PC.IDPERIODOCOMMERCIALE AS ID, PC.NOME AS NOME," & _
            " COUNT(PTP.IMPORTOBASE) AS CONTEGGIO" & _
            " FROM PERIODOCOMMERCIALE PC, PREZZOTITOLOPRODOTTO PTP" & _
            " WHERE PC.IDPERIODOCOMMERCIALE = PTP.IDPERIODOCOMMERCIALE(+)" & _
            " AND PC.IDPRODOTTO = " & idProdottoSelezionato & _
            " GROUP BY PC.IDPERIODOCOMMERCIALE, PC.NOME" & _
            " ORDER BY NOME"
'    End If
    Call CaricaListaPeriodiCommerciali(cmbPeriodoCommerciale, sql, "")
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoPrezzi.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("IDAREA").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoPrezzi_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    Dim i As Integer
    Dim campoImportoBase As String
    Dim campoImportoPrevendita As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioPrezzi
        
    Set d = adcConfigurazioneProdottoPrezzi
    
    sql = "SELECT IDAREA, SUPERAREA"
    For i = 1 To listaIdTariffe.count
        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffe.Item(i), 29))
        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffe.Item(i), 29))
        sql = sql & ", " & _
            campoImportoBase & ", " & campoImportoPrevendita
    Next i
    For i = 1 To listaIdTariffeOmaggio.count
        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggio.Item(i), 29))
        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeOmaggio.Item(i), 29))
        sql = sql & ", " & _
            campoImportoBase & ", " & campoImportoPrevendita
    Next i
'    For i = 1 To listaIdTariffeOmaggioConRiferimento.count
'        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggioConRiferimento.Item(i), 29))
'        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeOmaggioConRiferimento.Item(i), 29))
'        sql = sql & ", " & _
'            campoImportoBase & ", " & campoImportoPrevendita
'    Next i
    For i = 1 To listaIdTariffeServizio.count
        campoImportoBase = StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeServizio.Item(i), 29))
        campoImportoPrevendita = StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeServizio.Item(i), 29))
        sql = sql & ", " & _
            campoImportoBase & ", " & campoImportoPrevendita
    Next i
    sql = sql & " FROM " & nomeTabellaTemporanea & _
        " ORDER BY SUPERAREA"
'    sql = sql & " FROM " & nomeTabellaTemporanea
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneProdottoPrezzi.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneProdottoPrezzi_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim i As Integer
    Dim j As Integer
'    Dim j2 As Integer
    Dim k As Integer
    Dim formato As New StdDataFormat
    Dim numeroColonne As Integer
    
    formato.Format = "###0.00"
    Set g = dgrConfigurazioneProdottoPrezzi
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroColonne = 2 * (listaIdTariffe.count + listaIdTariffeOmaggio.count + listaIdTariffeServizio.count) + 1
    g.Columns(0).Visible = False
    g.Columns(1).Width = 1500
    g.Columns(1).DividerStyle = dbgBlackLine
    For i = 2 To (listaIdTariffe.count * 2) Step 2
        g.Columns(i + 1).DividerStyle = dbgBlackLine
        g.Columns(i).DividerStyle = dbgLightGrayLine
        g.Columns(i + 1).Width = 600
        g.Columns(i + 1).Caption = "PREV"
        g.Columns(i).Width = 1500
        Set g.Columns(i + 1).DataFormat = formato
        Set g.Columns(i).DataFormat = formato
    Next i
    For j = i To i + (listaIdTariffeOmaggio.count * 2) - 1 Step 2
        g.Columns(j + 1).DividerStyle = dbgBlackLine
        g.Columns(j).DividerStyle = dbgLightGrayLine
        g.Columns(j + 1).Width = 600
        g.Columns(j + 1).Caption = "PREV"
        g.Columns(j).Width = 1500
        Set g.Columns(j + 1).DataFormat = formato
        Set g.Columns(j).DataFormat = formato
    Next j
'    For j2 = j To j + (listaIdTariffeOmaggioConRiferimento.count * 2) - 1 Step 2
'        g.Columns(j2 + 1).DividerStyle = dbgBlackLine
'        g.Columns(j2).DividerStyle = dbgLightGrayLine
'        g.Columns(j2 + 1).Width = 600
'        g.Columns(j2 + 1).Caption = "PREV"
'        g.Columns(j2).Width = 1500
'        Set g.Columns(j2 + 1).DataFormat = formato
'        Set g.Columns(j2).DataFormat = formato
'    Next j2
    For k = j To j + (listaIdTariffeServizio.count * 2) - 1 Step 2
        g.Columns(k + 1).DividerStyle = dbgBlackLine
        g.Columns(k).DividerStyle = dbgLightGrayLine
        g.Columns(k + 1).Width = 600
        g.Columns(k + 1).Caption = "PREV"
        g.Columns(k).Width = 1500
        Set g.Columns(k + 1).DataFormat = formato
        Set g.Columns(k).DataFormat = formato
    Next k
    
    g.MarqueeStyle = dbgHighlightCell
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Private Sub RilevaNomiTariffe()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim id As Long
    Dim nome As String
    Dim codice As String
    Dim label As String
    Dim idTipoTariffa As TipoTariffaEnum
    Dim idTariffaRiferimento As Long
    
    Call ApriConnessioneBD
          
    Set listaNomiTariffe = New Collection
    Set listaIdTariffe = New Collection
    Set listaIdTariffeOmaggio = New Collection
    Set listaNomiTariffeOmaggio = New Collection
    Set listaIdTariffeOmaggioConRiferimento = New Collection
    Set listaNomiTariffeOmaggioConRiferimento = New Collection
    Set listaIdTariffeServizio = New Collection
    Set listaNomiTariffeServizio = New Collection
        
    i = 0
    idTariffaIngressoAbbonato = idNessunElementoSelezionato
        
    sql = "SELECT IDTARIFFA, NOME, CODICE, IDTIPOTARIFFA, IDTARIFFA_RIFERIMENTO" & _
        " FROM TARIFFA" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " ORDER BY NLSSORT(CODICE, 'NLS_SORT=italian')"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            i = i + 1
            id = rec("IDTARIFFA")
            nome = rec("NOME")
            codice = rec("CODICE")
            idTipoTariffa = rec("IDTIPOTARIFFA")
            label = codice & " " & nome
            Select Case idTipoTariffa
                Case T_INTERI, T_RIDOTTI
                    Call listaIdTariffe.Add(id)
                    Call listaNomiTariffe.Add(label)
                Case T_INGRESSO_ABBONATO
                    idTariffaIngressoAbbonato = id
                Case T_OMAGGIO
                    If IsNull(rec("IDTARIFFA_RIFERIMENTO")) Then
                        Call listaIdTariffeOmaggio.Add(id)
                        Call listaNomiTariffeOmaggio.Add(label)
                    Else
                        Call listaIdTariffeOmaggioConRiferimento.Add(id)
                        Call listaNomiTariffeOmaggioConRiferimento.Add(label)
                    End If
                Case T_SERVIZI
                    Call listaIdTariffeServizio.Add(id)
                    Call listaNomiTariffeServizio.Add(label)
            End Select
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CreaTabellaAppoggioPrezzi()
    Dim sql As String
    Dim i As Integer
    Dim stringaSQL As String
    Dim tariffaSQL As String
    
    nomeTabellaTemporanea = CreaNomeTabellaTemporanea("TMP_PREZZI_")
    
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDAREA NUMBER(10), SUPERAREA VARCHAR2(50)"
    For i = 1 To listaIdTariffe.count
        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffe.Item(i), 29)) & " NUMBER(10,2)" & _
            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffe.Item(i), 29)) & " NUMBER (10,2)"
    Next i
    For i = 1 To listaIdTariffeOmaggio.count
        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggio.Item(i), 29)) & " NUMBER(10,2)" & _
            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeOmaggio.Item(i), 29)) & " NUMBER (10,2)"
    Next i
'    For i = 1 To listaIdTariffeOmaggioConRiferimento.count
'        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggioConRiferimento.Item(i), 29)) & " NUMBER(10,2)" & _
'            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeOmaggioConRiferimento.Item(i), 29)) & " NUMBER (10,2)"
'    Next i
    For i = 1 To listaIdTariffeServizio.count
        sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeServizio.Item(i), 29)) & " NUMBER(10,2)" & _
            ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeServizio.Item(i), 29)) & " NUMBER (10,2)"
    Next i
    sql = sql & ")"
    
    Call EliminaTabellaAppoggioPrezzi
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioPrezzi()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub cmbPeriodoCommerciale_Click()
    If Not internalEvent Then
        idPeriodoCommercialeSelezionato = cmbPeriodoCommerciale.ItemData(cmbPeriodoCommerciale.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPeriodiCommerciali.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoPeriodiCommerciali.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Function NumeroPeriodiCommercialiConfigurati() As Integer
    NumeroPeriodiCommercialiConfigurati = cmbPeriodoCommerciale.ListCount
End Function

Private Function IsDefinitoAlmenoUnPrezzoPerPeriodoCommerciale() As Boolean
    Dim i As Integer
    Dim associazioneParziale As Boolean
    
    associazioneParziale = True
    If listaCompletezzaPeriodiCommerciali.count = 0 Then
        associazioneParziale = False
    Else
        For i = 1 To listaCompletezzaPeriodiCommerciali.count
            associazioneParziale = associazioneParziale And _
                listaCompletezzaPeriodiCommerciali.Item(i)
        Next i
    End If
    IsDefinitoAlmenoUnPrezzoPerPeriodoCommerciale = associazioneParziale
End Function

Private Sub CaricaListaPeriodiCommerciali(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim chiave As String
    Dim isPresenteAlmenoUnPrezzo As Boolean
    Dim conteggio As Integer
    
    Call ApriConnessioneBD
    
    isPresenteAlmenoUnPrezzo = False
    conteggio = 0
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            chiave = ChiaveId(rec("ID").Value)
            conteggio = rec("CONTEGGIO")
            isPresenteAlmenoUnPrezzo = IIf(conteggio <= 0, False, True)
            Call listaCompletezzaPeriodiCommerciali.Add(isPresenteAlmenoUnPrezzo, chiave)
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Function IdPrezzoTitoloProdottoInDB(idTariffa As Long, idArea As Long, _
                                            importoBase As Long, importoPrevendita As Long, _
                                            idTipoTarSIAE As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idPTP As Long
    Dim tipoStatoRecord As TipoStatoRecordEnum
    
    sql = "SELECT IDPREZZOTITOLOPRODOTTO, IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA, IDTIPOTARIFFASIAE" & _
        " FROM PREZZOTITOLOPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDTARIFFA = " & idTariffa & _
        " AND IDAREA = " & idArea & _
        " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        importoBase = rec("IMPORTOBASE").Value
        importoPrevendita = rec("IMPORTOSERVIZIOPREVENDITA").Value
        idPTP = rec("IDPREZZOTITOLOPRODOTTO").Value
        idTipoTarSIAE = rec("IDTIPOTARIFFASIAE").Value
    Else
        idPTP = idNessunElementoSelezionato
        idTipoTarSIAE = idNessunElementoSelezionato
    End If
    rec.Close
    IdPrezzoTitoloProdottoInDB = idPTP
End Function

Private Sub PopolaTabellaAppoggioPrezzi()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim l As Integer
    Dim l2 As Integer
    Dim m As Integer
    Dim idArea As Long
    Dim campoNome As String
    Dim codice As String
    Dim label As String
    Dim idImporto As Long
    Dim campoImportoBase As String
    Dim campoImportoPrevendita As String
    Dim valori As String
    Dim valoriPrevendita As String
    Dim tipoSA As TipoAreaEnum
    
    Call ApriConnessioneBD
    
    Set listaIdArea = New Collection
    Set listaNomeArea = New Collection
    
    'A: la tabella viene pulita (solo se precedentemente popolata)
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, , adCmdText
    
    'B: si ricavano le superaree relative al prodotto corrente (e alla sua pianta), compresa la superarea di servizio
'    sql = "SELECT CODICE, IDAREA, NOME, IDTIPOAREA" & _
'        " FROM AREA" & _
'        " WHERE IDPIANTA = " & idPiantaSelezionata & _
'        " AND IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_SERVIZIO & ")" & _
'        " AND IDAREA NOT IN (SELECT IDAREA_PADRE FROM AREA WHERE IDTIPOAREA = " & TA_SUPERAREA & " AND" & _
'        " IDAREA_PADRE IS NOT NULL AND IDPIANTA = " & idPiantaSelezionata & ")" & _
'        " ORDER BY CODICE"
    sql = "SELECT CODICE, IDAREA, NOME, IDTIPOAREA" & _
        " FROM AREA" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA IN (" & TA_SUPERAREA_SERVIZIO & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
        " ORDER BY CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            tipoSA = rec("IDTIPOAREA")
            idArea = rec("IDAREA")
            campoNome = rec("NOME")
            codice = rec("CODICE")
            label = codice & " - " & campoNome
            If tipoSA = TA_SUPERAREA_SERVIZIO Then
                idSuperareaServizio = idArea
            Else
                Call listaIdArea.Add(idArea)
                Call listaNomeArea.Add(label)
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close

    'C: vengono caricati i dati (importi) per area, tariffa e prodotto
    For j = 1 To listaIdArea.count
        Set listaIdAppoggioPrezzi = New Collection
        Set listaNomeAppoggioPrezzi = New Collection
        Set listaPrevenditaAppoggioPrezzi = New Collection
        For i = 1 To listaIdTariffe.count
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "SELECT IMPORTOBASE, " & _
'                    " IMPORTOSERVIZIOPREVENDITA," & _
'                    " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
'                    " FROM PREZZOTITOLOPRODOTTO" & _
'                    " WHERE IDTARIFFA = " & listaIdTariffe.Item(i) & _
'                    " AND IDAREA = " & listaIdArea.Item(j) & _
'                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
'                    " AND IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND (IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                    " OR IDTIPOSTATORECORD IS NULL)"
'            Else
                sql = "SELECT IMPORTOBASE, " & _
                    " IMPORTOSERVIZIOPREVENDITA," & _
                    " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
                    " FROM PREZZOTITOLOPRODOTTO" & _
                    " WHERE IDTARIFFA = " & listaIdTariffe.Item(i) & _
                    " AND IDAREA = " & listaIdArea.Item(j) & _
                    " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
                    " AND IDPRODOTTO = " & idProdottoSelezionato
'            End If
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
                    idImporto = IIf(IsNull(rec("IDIM")), idNessunElementoSelezionato, rec("IDIM"))
                    rec.MoveNext
                Wend
                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
                Call listaIdAppoggioPrezzi.Add(idImporto)
            Else
                Call listaNomeAppoggioPrezzi.Add("")
                Call listaPrevenditaAppoggioPrezzi.Add("")
                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
            End If
            rec.Close
        Next i
        For i = 1 To listaIdTariffeOmaggio.count 'OMAGGIO
            sql = "SELECT IMPORTOBASE, " & _
                " IMPORTOSERVIZIOPREVENDITA," & _
                " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
                " FROM PREZZOTITOLOPRODOTTO" & _
                " WHERE IDTARIFFA = " & listaIdTariffeOmaggio.Item(i) & _
                " AND IDAREA = " & listaIdArea.Item(j) & _
                " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
                " AND IDPRODOTTO = " & idProdottoSelezionato
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
                    idImporto = IIf(IsNull(rec("IDIM")), idNessunElementoSelezionato, rec("IDIM"))
                    rec.MoveNext
                Wend
                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
                Call listaIdAppoggioPrezzi.Add(idImporto)
            Else
                Call listaNomeAppoggioPrezzi.Add("")
                Call listaPrevenditaAppoggioPrezzi.Add("")
                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
            End If
            rec.Close
        Next i
'        For i = 1 To listaIdTariffeOmaggioConRiferimento.count 'OMAGGIO con riferimento
'            sql = "SELECT IMPORTOBASE, " & _
'                " IMPORTOSERVIZIOPREVENDITA," & _
'                " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
'                " FROM PREZZOTITOLOPRODOTTO" & _
'                " WHERE IDTARIFFA = " & listaIdTariffeOmaggioConRiferimento.Item(i) & _
'                " AND IDAREA = " & listaIdArea.Item(j) & _
'                " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
'                " AND IDPRODOTTO = " & idProdottoSelezionato
'            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'            If Not (rec.BOF And rec.EOF) Then
'                rec.MoveFirst
'                While Not rec.EOF
'                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
'                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
'                    idImporto = IIf(IsNull(rec("IDIM")), idNessunElementoSelezionato, rec("IDIM"))
'                    rec.MoveNext
'                Wend
'                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
'                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
'                Call listaIdAppoggioPrezzi.Add(idImporto)
'            Else
'                Call listaNomeAppoggioPrezzi.Add("")
'                Call listaPrevenditaAppoggioPrezzi.Add("")
'                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
'            End If
'            rec.Close
'        Next i
        For i = 1 To listaIdTariffeServizio.count 'SERVIZIO
            sql = "SELECT IMPORTOBASE, " & _
                " IMPORTOSERVIZIOPREVENDITA," & _
                " IDPREZZOTITOLOPRODOTTO AS ""IDIM""" & _
                " FROM PREZZOTITOLOPRODOTTO" & _
                " WHERE IDTARIFFA = " & listaIdTariffeServizio.Item(i) & _
                " AND IDAREA = " & listaIdArea.Item(j) & _
                " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
                " AND IDPRODOTTO = " & idProdottoSelezionato
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    campoImportoBase = IIf(IsNull(rec("IMPORTOBASE")), "", CStr(rec("IMPORTOBASE") / 100))
                    campoImportoPrevendita = IIf(IsNull(rec("IMPORTOSERVIZIOPREVENDITA")), "", CStr(rec("IMPORTOSERVIZIOPREVENDITA") / 100))
                    idImporto = IIf(IsNull(rec("IDIM")), idNessunElementoSelezionato, rec("IDIM"))
                    rec.MoveNext
                Wend
                Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
                Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
                Call listaIdAppoggioPrezzi.Add(idImporto)
            Else
                Call listaNomeAppoggioPrezzi.Add("")
                Call listaPrevenditaAppoggioPrezzi.Add("")
                Call listaIdAppoggioPrezzi.Add(idNessunElementoSelezionato)
            End If
            rec.Close
        Next i
        
        'D: vengono inseriti i record precedentemente tirati sų
        Dim val1 As String
        Dim val2 As String
        valori = ""
        valoriPrevendita = ""
        If listaNomeAppoggioPrezzi.count > 0 Then
            sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDAREA, SUPERAREA"
            valori = listaIdArea.Item(j) & ", " & SqlStringValue(listaNomeArea.Item(j))
            For k = 1 To listaIdTariffe.count
                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffe.Item(k), 29)) & _
                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffe.Item(k), 29))
                If listaNomeAppoggioPrezzi.Item(k) = "" Then
                    val1 = "NULL"
                Else
                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k)))
                End If
                If listaPrevenditaAppoggioPrezzi.Item(k) = "" Then
                    val2 = "NULL"
                Else
                    val2 = StringaConPuntoDecimale(CDbl(listaPrevenditaAppoggioPrezzi.Item(k)))
                End If
                valori = valori & "," & val1 & "," & val2
            Next k
            For l = 1 To listaIdTariffeOmaggio.count
                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggio.Item(l), 29)) & _
                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeOmaggio.Item(l), 29))
                If listaNomeAppoggioPrezzi.Item(k - 1 + l) = "" Then
                    val1 = "NULL"
                Else
                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k - 1 + l)))
                End If
                If listaPrevenditaAppoggioPrezzi.Item(k - 1 + l) = "" Then
                    val2 = "NULL"
                Else
                    val2 = StringaConPuntoDecimale(CDbl(listaPrevenditaAppoggioPrezzi.Item(k - 1 + l)))
                End If
                valori = valori & "," & val1 & "," & val2
            Next l
'            For l2 = 1 To listaIdTariffeOmaggioConRiferimento.count
'                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeOmaggioConRiferimento.Item(l2), 29)) & _
'                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeOmaggioConRiferimento.Item(l2), 29))
'                If listaNomeAppoggioPrezzi.Item(k - 2 + l + l2) = "" Then
'                    val1 = "NULL"
'                Else
'                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k - 2 + l + l2)))
'                End If
'                If listaPrevenditaAppoggioPrezzi.Item(k - 2 + l + l2) = "" Then
'                    val2 = "NULL"
'                Else
'                    val2 = StringaConPuntoDecimale(CDbl(listaPrevenditaAppoggioPrezzi.Item(k - 2 + l + l2)))
'                End If
'                valori = valori & "," & val1 & "," & val2
'            Next l2
            For m = 1 To listaIdTariffeServizio.count
                sql = sql & ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata(listaNomiTariffeServizio.Item(m), 29)) & _
                    ", " & StringaAlfanumerica(StringaLunghezzaMaxFissata("PR_" & listaNomiTariffeServizio.Item(m), 29))
                If listaNomeAppoggioPrezzi.Item(k - 2 + l + m) = "" Then
                    val1 = "NULL"
                Else
                    val1 = StringaConPuntoDecimale(CDbl(listaNomeAppoggioPrezzi.Item(k - 2 + l + m)))
                End If
                If listaPrevenditaAppoggioPrezzi.Item(k - 2 + l + m) = "" Then
                    val2 = "NULL"
                Else
                    val2 = StringaConPuntoDecimale(CDbl(listaPrevenditaAppoggioPrezzi.Item(k - 2 + l + m)))
                End If
                valori = valori & "," & val1 & "," & val2
            Next m
            If Right$(valori, 2) = ", " Then valori = Left$(valori, Len(valori) - 2)
            sql = sql & ") VALUES (" & valori & ")"
            SETAConnection.Execute sql, , adCmdText
        End If
    Next j
    
    Call ChiudiConnessioneBD
    'Call pgbAvanzamento_Init(listaNomeArea.count * _
    '    (listaNomiTariffe.count + 1 + listaNomiTariffeOmaggio.count + listaNomiTariffeServizio.count) + 2)
    Call pgbAvanzamento_Init(listaIdArea.count * _
        (listaIdTariffe.count + listaIdTariffeOmaggio.count + listaIdTariffeServizio.count + 1) + listaIdTariffeServizio.count)
    'listaIdArea.count * (listaIdTariffe.count + listaIdTariffeOmaggio.count + listaIdTariffeServizio.count + 1) + listaIdTariffeServizio.count
End Sub

Private Sub RilevaValoriCampi(record As Long)
    Dim g As DataGrid
    Dim i As Integer
    Dim campoImportoBase As String
    Dim campoImportoPrevendita As String
    Dim a As Adodc
    Dim internalEventOld As Boolean
    
    Set g = dgrConfigurazioneProdottoPrezzi
    Set a = adcConfigurazioneProdottoPrezzi
    Set listaNomeAppoggioPrezzi = New Collection
    Set listaPrevenditaAppoggioPrezzi = New Collection
    
    internalEventOld = internalEvent
    internalEvent = True
    If a.Recordset.RecordCount > 0 Then
        Call a.Recordset.Move(record, adBookmarkFirst)
        For i = 2 To (a.Recordset.Fields.count - 1) Step 2
            campoImportoBase = ""
            If Not IsNull(a.Recordset(i)) Then
                campoImportoBase = CStr(a.Recordset(i).Value * 100)
            End If
            campoImportoPrevendita = ""
            If Not IsNull(a.Recordset(i + 1)) Then
                campoImportoPrevendita = CStr(a.Recordset(i + 1).Value * 100)
            End If
            isDefinitoAlmenoUnPrezzo = isDefinitoAlmenoUnPrezzo Or (campoImportoBase <> "")
            Call listaNomeAppoggioPrezzi.Add(campoImportoBase)
            Call listaPrevenditaAppoggioPrezzi.Add(campoImportoPrevendita)
        Next i
    End If
    internalEvent = internalEventOld
        
End Sub

Private Sub pgbAvanzamento_Update(pgbValue As Long)
    pgbAvanzamento.Value = pgbValue
End Sub

Private Sub pgbAvanzamento_Init(pgbMax As Long)
    pgbAvanzamento.Visible = True
    pgbAvanzamento.Value = 0
    pgbAvanzamento.max = pgbMax
End Sub

Private Function EsisteTitoloAssociatoAPrezzo(idPTP As Long, numTitoli As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    sql = "SELECT COUNT(IDSTATOTITOLO) CONT FROM STATOTITOLO"
    sql = sql & " WHERE IDPREZZOTITOLOPRODOTTO = " & idPTP
'    sql = sql & " AND IDTIPOSTATOTITOLO IN (2, 3, 4, 5, 6)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numTitoli = rec("CONT").Value
    rec.Close
    EsisteTitoloAssociatoAPrezzo = (numTitoli > 0)
End Function

Private Sub AggiornaPrezzoTitoloProdottoInDB(importoBase As Long, importoPrevendita As Long, idPrezzoTitoloProdotto As Long, idTipoTariffaSIAE As Long)
    Dim sql As String
    Dim n As Long
    Dim condizioniSQL As String
    
    If idTipoTariffaSIAE = idNessunElementoSelezionato Or idTipoTariffaSIAE = 0 Then
        sql = "UPDATE PREZZOTITOLOPRODOTTO SET IMPORTOBASE = " & importoBase & _
            ", IMPORTOSERVIZIOPREVENDITA = " & importoPrevendita & _
            ", IDTIPOTARIFFASIAE = " & listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente(1) & _
            " WHERE IDPREZZOTITOLOPRODOTTO = " & idPrezzoTitoloProdotto
        Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Remove(1)
    Else
        sql = "UPDATE PREZZOTITOLOPRODOTTO SET IMPORTOBASE = " & importoBase & _
            ", IMPORTOSERVIZIOPREVENDITA = " & importoPrevendita & _
            ", IDTIPOTARIFFASIAE = " & idTipoTariffaSIAE & _
            " WHERE IDPREZZOTITOLOPRODOTTO = " & idPrezzoTitoloProdotto
    End If
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            condizioniSQL = " AND IDTIPOSTATORECORD IS NULL"
'            Call AggiornaParametriSessioneSuRecord("PREZZOTITOLOPRODOTTO", "IDPREZZOTITOLOPRODOTTO", idPrezzoTitoloProdotto, TSR_MODIFICATO, condizioniSQL)
''        End If
'    End If
End Sub

Private Sub InserisciPrezzoTitoloProdottoInDB(importoBase As String, importoPrevendita As String, _
                                                idTariffa As Long, idArea As Long, idTipoTariffaSIAE As Long, _
                                                isTipoTariffaOmaggio As Boolean)
    Dim sql As String
    Dim idNuovoPrezzoTitoloProdotto As Long
    Dim idTTS As Long
    Dim n As Long
    
    idNuovoPrezzoTitoloProdotto = OttieniIdentificatoreDaSequenza("SQ_PREZZOTITOLOPRODOTTO")
    If idTipoTariffaSIAE = idNessunElementoSelezionato Or idTipoTariffaSIAE = 0 Then
        If isTipoTariffaOmaggio Then
            idTTS = listaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente(1)
            Call listaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente.Remove(1)
        Else
            idTTS = listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente(1)
            Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Remove(1)
        End If
    Else
        idTTS = idTipoTariffaSIAE
    End If
    sql = "INSERT INTO PREZZOTITOLOPRODOTTO (IDPREZZOTITOLOPRODOTTO," & _
        " IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA, IDPRODOTTO," & _
        " IDTARIFFA, IDAREA, IDTIPOTARIFFASIAE, IDPERIODOCOMMERCIALE)" & _
        " VALUES (" & _
        idNuovoPrezzoTitoloProdotto & ", " & _
        IIf(importoBase = "", 0, SostituisciVirgolaConPunto(importoBase)) & ", " & _
        IIf(importoPrevendita = "", 0, SostituisciVirgolaConPunto(importoPrevendita)) & ", " & _
        idProdottoSelezionato & ", " & _
        idTariffa & ", " & _
        idArea & ", " & _
        idTTS & ", " & _
        idPeriodoCommercialeSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("PREZZOTITOLOPRODOTTO", "IDPREZZOTITOLOPRODOTTO", idNuovoPrezzoTitoloProdotto, TSR_NUOVO)
'    End If
    
End Sub

Private Function IsPrezzoEliminabile(idPTP As Long, numTitoli As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
'    sql = "SELECT COUNT(*) CONT FROM STATOTITOLO" & _
'        " WHERE IDPREZZOTITOLOPRODOTTO = " & idPTP & _
'        " AND IDTIPOSTATOTITOLO IN (2, 3, 4, 5, 6)"
    sql = "SELECT COUNT(*) CONT FROM STATOTITOLO" & _
        " WHERE IDPREZZOTITOLOPRODOTTO = " & idPTP
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numTitoli = rec("CONT").Value
    rec.Close
    IsPrezzoEliminabile = (numTitoli = 0)
End Function

Private Sub EliminaPrezzoTitoloProdottoInDB(idPTP As Long)
    Dim sql As String
    Dim n As Long
    
    sql = "DELETE PREZZOTITOLOPRODOTTO" & _
        " WHERE IDPREZZOTITOLOPRODOTTO = " & idPTP
    SETAConnection.Execute sql, n, adCmdText
End Sub

Private Sub CaricaListaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente(idOrdine As Long, idPiantaSIAE As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idTTS As Long
    
    Call ApriConnessioneBD
    
    Set listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente = New Collection
    
    sql = "SELECT DISTINCT TTS.IDTIPOTARIFFASIAE"
    sql = sql & " FROM TIPOTARIFFASIAE TTS"
    sql = sql & " WHERE NOT EXISTS"
    sql = sql & " ("
    sql = sql & " SELECT IDTIPOTARIFFASIAE"
    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, PIANTASIAE_AREA_ORDINEDIPOSTO PAO, TARIFFA T"
    sql = sql & " WHERE PTP.IDAREA = PAO.IDAREA"
    sql = sql & " AND PAO.IDORDINEDIPOSTOSIAE = " & idOrdine
    sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
    sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " AND PTP.IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
    sql = sql & " AND IDPIANTASIAE = " & idPiantaSIAE
    sql = sql & " AND TTS.IDTIPOTARIFFASIAE = PTP.IDTIPOTARIFFASIAE"
    sql = sql & " )"
    sql = sql & " AND (TTS.CODICE LIKE 'R_' OR TTS.CODICE LIKE 'T_')"
    sql = sql & " AND TTS.CODICE <> 'TS'"
    sql = sql & " ORDER BY TTS.IDTIPOTARIFFASIAE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idTTS = rec("IDTIPOTARIFFASIAE")
            Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Add(idTTS, ChiaveId(idTTS))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CaricaListaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente(idOrdine As Long, idPiantaSIAE As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idTTS As Long
    
    Call ApriConnessioneBD
    
    Set listaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente = New Collection
    
    sql = "SELECT DISTINCT TTS.IDTIPOTARIFFASIAE"
    sql = sql & " FROM TIPOTARIFFASIAE TTS"
    sql = sql & " WHERE NOT EXISTS"
    sql = sql & " ("
    sql = sql & " SELECT IDTIPOTARIFFASIAE"
    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, PIANTASIAE_AREA_ORDINEDIPOSTO PAO, TARIFFA T"
    sql = sql & " WHERE PTP.IDAREA = PAO.IDAREA"
    sql = sql & " AND PAO.IDORDINEDIPOSTOSIAE = " & idOrdine
    sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
    sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " AND PTP.IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato
    sql = sql & " AND IDPIANTASIAE = " & idPiantaSIAE
    sql = sql & " AND TTS.IDTIPOTARIFFASIAE = PTP.IDTIPOTARIFFASIAE"
    sql = sql & " )"
    sql = sql & " AND TTS.CODICE LIKE 'O_'"
    sql = sql & " AND TTS.CODICE <> 'OX'"
    sql = sql & " ORDER BY TTS.IDTIPOTARIFFASIAE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idTTS = rec("IDTIPOTARIFFASIAE")
            Call listaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente.Add(idTTS, ChiaveId(idTTS))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Function RilevaIdOrdineDiPostoSIAECorrente(idArea As Long, idPiantaSIAE As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idOPS As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT IDORDINEDIPOSTOSIAE" & _
        " FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
        " WHERE IDAREA = " & idArea & _
        " AND IDPIANTASIAE = " & idPiantaSIAE
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        idOPS = rec("IDORDINEDIPOSTOSIAE")
    Else
        idOPS = idNessunElementoSelezionato
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    RilevaIdOrdineDiPostoSIAECorrente = idOPS
End Function

Private Function PrezzoMaxPerOrdineDiPostoSIAE(idOrdine, idPTPMax As Long, idPiantaSIAE As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim importo As Long
    
    Call ApriConnessioneBD
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT PTP.IDPREZZOTITOLOPRODOTTO IDPTPMAX, MAX(IMPORTOBASE) IMPORTO " & _
'            " FROM PREZZOTITOLOPRODOTTO PTP, PIANTASIAE_AREA_ORDINEDIPOSTO PAO" & _
'            " WHERE PTP.IDAREA = PAO.IDAREA" & _
'            " AND PAO.IDORDINEDIPOSTOSIAE = " & idOrdine & _
'            " AND PAO.IDPIANTASIAE = " & idPiantaSIAE & _
'            " AND PTP.IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (PTP.IDTIPOSTATORECORD IS NULL" & _
'            " OR PTP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " GROUP BY PTP.IDPREZZOTITOLOPRODOTTO" & _
'            " ORDER BY IMPORTO DESC"
'    Else
        sql = " SELECT PTP.IDPREZZOTITOLOPRODOTTO IDPTPMAX, MAX(IMPORTOBASE) IMPORTO " & _
            " FROM PREZZOTITOLOPRODOTTO PTP, PIANTASIAE_AREA_ORDINEDIPOSTO PAO" & _
            " WHERE PTP.IDAREA = PAO.IDAREA" & _
            " AND PAO.IDORDINEDIPOSTOSIAE = " & idOrdine & _
            " AND PAO.IDPIANTASIAE = " & idPiantaSIAE & _
            " AND PTP.IDPRODOTTO = " & idProdottoSelezionato & _
            " GROUP BY PTP.IDPREZZOTITOLOPRODOTTO" & _
            " ORDER BY IMPORTO DESC"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        importo = rec("IMPORTO")
        idPTPMax = rec("IDPTPMAX")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    PrezzoMaxPerOrdineDiPostoSIAE = importo
    
End Function

Private Sub AggiornaTipoTariffaSIAE(importoCorrente As Long, idOrdineDiPostoSIAE As Long, _
                                    idPiantaSIAE As Long, isPTPMax As Boolean)
    Dim sql As String
    Dim n As Integer
    Dim idTTS As Long
    Dim condizioniSQL As String
    
    If isPTPMax Then
        idTTS = IDTIPOTARIFFASIAE_INTERO
    Else
        idTTS = listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente(1)
        Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Remove(1)
    End If
    
    Call ApriConnessioneBD

    condizioniSQL = " AND IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
        " AND IDTIPOSTATORECORD IS NULL " & _
        " AND IDAREA IN (" & _
        " SELECT IDAREA FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
        " WHERE IDORDINEDIPOSTOSIAE = " & idOrdineDiPostoSIAE & _
        " AND IDPIANTASIAE = " & idPiantaSIAE & ")"
    sql = "UPDATE PREZZOTITOLOPRODOTTO SET IDTIPOTARIFFASIAE = " & idTTS & _
        " WHERE IMPORTOBASE = " & importoCorrente & _
        " AND IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDPERIODOCOMMERCIALE = " & idPeriodoCommercialeSelezionato & _
        " AND IDAREA IN (" & _
        " SELECT IDAREA FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
        " WHERE IDORDINEDIPOSTOSIAE = " & idOrdineDiPostoSIAE & _
        " AND IDPIANTASIAE = " & idPiantaSIAE & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("PREZZOTITOLOPRODOTTO", "IMPORTOBASE", importoCorrente, TSR_MODIFICATO, condizioniSQL)
''        End If
'    End If

    Call ChiudiConnessioneBD
    
End Sub

Private Function IsImportoPresenteInOrdineDiPostoSIAE(importo As Long, idOrdineDiPostoSIAE As Long, idTipoTariffaSIAE) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim trovato As Boolean
    
    trovato = False
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDPREZZOTITOLOPRODOTTO, IDTIPOTARIFFASIAE FROM PREZZOTITOLOPRODOTTO" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDAREA IN " & _
'            " (SELECT IDAREA FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
'            " WHERE IDORDINEDIPOSTOSIAE = " & idOrdineDiPostoSIAE & _
'            " AND IDPIANTASIAE IN " & _
'            " (SELECT IDPIANTASIAE FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & "))" & _
'            " AND (IDTIPOSTATORECORD IS NULL" & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND IMPORTOBASE = " & importo
'    Else
        sql = "SELECT IDPREZZOTITOLOPRODOTTO, IDTIPOTARIFFASIAE FROM PREZZOTITOLOPRODOTTO" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDAREA IN " & _
            " (SELECT IDAREA FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
            " WHERE IDORDINEDIPOSTOSIAE = " & idOrdineDiPostoSIAE & _
            " AND IDPIANTASIAE IN " & _
            " (SELECT IDPIANTASIAE FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & "))" & _
            " AND IMPORTOBASE = " & importo
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        trovato = True
        idTipoTariffaSIAE = rec("IDTIPOTARIFFASIAE")
    End If
    rec.Close
    
    IsImportoPresenteInOrdineDiPostoSIAE = trovato
End Function

Private Function IdPiantaSIAECorrente() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idP As Long
    
    sql = "SELECT IDPIANTASIAE FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        idP = rec("IDPIANTASIAE")
    End If
    rec.Close
    IdPiantaSIAECorrente = idP
End Function

Private Sub InserisciNellaBaseDati()
    Dim i As Integer
    Dim j As Integer
    Dim chiave As String
    Dim avanzamento As Long
    Dim importoBase As Long
    Dim importoPrevendita As Long
    Dim idPrezzoTitoloProdotto As Long
    Dim numeroStatiTitolo As Long
    Dim listaPrezziNonAggiornati As Collection
    Dim appoggioPrevendita As Long
    Dim appoggioPrezzo As Long
    Dim importo As String
    Dim importoMaxOld As Long
    Dim importoMaxNew As Long
    Dim prevendita As String
    Dim idOrdineDiPostoSIAE As Long
    Dim idPTPMax As Long
    Dim idTipoTariffaSIAECorrente As Long
    Dim idPiantaSIAE As Long
    Dim isImportoTrovato As Boolean
    Dim k As Long
    Dim k2 As Long
    Dim l As Long
    
    isDefinitoAlmenoUnPrezzo = False
    Set listaPrezziNonAggiornati = New Collection
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    idTipoTariffaSIAECorrente = idNessunElementoSelezionato
    idPTPMax = idNessunElementoSelezionato
    avanzamento = 0
    idPiantaSIAE = IdPiantaSIAECorrente 'in seguito si potrā pensare di non fare la query e portare idPiantaSIAE dal frmInizialeProdotto, o renedere finalmente tutte le cose che ora mi porto in giro variabili globali
'A: vengono inseriti nuovi record contenenti i valori prelevati dalla griglia
    For i = 1 To listaIdArea.count
        idOrdineDiPostoSIAE = RilevaIdOrdineDiPostoSIAECorrente(listaIdArea(i), idPiantaSIAE)
        If idOrdineDiPostoSIAE <> idNessunElementoSelezionato Then
            Call CaricaListaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente(idOrdineDiPostoSIAE, idPiantaSIAE)
            Call CaricaListaTipiTariffaSIAEOmaggioDisponibiliPerOrdineDiPostoCorrente(idOrdineDiPostoSIAE, idPiantaSIAE)
            Call RilevaValoriCampi(i - 1)
            For j = 1 To listaIdTariffe.count
                idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffe(j), listaIdArea(i), importoBase, importoPrevendita, idTipoTariffaSIAECorrente)
'                If (listaNomeAppoggioPrezzi.Item(j) <> "" Or listaPrevenditaAppoggioPrezzi(j) <> "") And _
'                    listaNomeAppoggioPrezzi.Item(j) <> "0" Then
                If (listaNomeAppoggioPrezzi(j) <> "" And listaNomeAppoggioPrezzi(j) <> "0") Or _
                    (listaPrevenditaAppoggioPrezzi(j) <> "" And listaPrevenditaAppoggioPrezzi(j) <> "0") Then
                    If idPrezzoTitoloProdotto <> idNessunElementoSelezionato Then 'se il prezzo č presente in DB
                        numeroStatiTitolo = 0
                        If EsisteTitoloAssociatoAPrezzo(idPrezzoTitoloProdotto, numeroStatiTitolo) Then
                            Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; stati titolo: " & numeroStatiTitolo)
                        Else
                            If CStr(importoBase) <> SostituisciVirgolaConPunto(listaNomeAppoggioPrezzi(j)) Or _
                                CStr(importoPrevendita) <> SostituisciVirgolaConPunto(listaPrevenditaAppoggioPrezzi(j)) Then
                                If SostituisciVirgolaConPunto(listaPrevenditaAppoggioPrezzi(j)) = "" Then
                                    appoggioPrevendita = 0
                                Else
                                    appoggioPrevendita = CLng(SostituisciVirgolaConPunto(listaPrevenditaAppoggioPrezzi(j)))
                                End If
                                If SostituisciVirgolaConPunto(listaNomeAppoggioPrezzi(j)) = "" Then
                                    appoggioPrezzo = 0
                                Else
                                    appoggioPrezzo = CLng(SostituisciVirgolaConPunto(listaNomeAppoggioPrezzi(j)))
                                End If
                                importoMaxOld = PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE)
                                If appoggioPrezzo <= importoMaxOld Then
                                    isImportoTrovato = IsImportoPresenteInOrdineDiPostoSIAE(appoggioPrezzo, idOrdineDiPostoSIAE, idTipoTariffaSIAECorrente)
                                    If isImportoTrovato Then
                                        Call AggiornaPrezzoTitoloProdottoInDB(CStr(appoggioPrezzo), appoggioPrevendita, idPrezzoTitoloProdotto, idTipoTariffaSIAECorrente)
                                        importoMaxNew = PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE) 'trova il max dopo quello eliminato
                                        Call AggiornaTipoTariffaSIAE(CStr(importoMaxNew), idOrdineDiPostoSIAE, idPiantaSIAE, True)
                                    ElseIf listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.count > 0 Then
                                        Call AggiornaPrezzoTitoloProdottoInDB(appoggioPrezzo, appoggioPrevendita, idPrezzoTitoloProdotto, idNessunElementoSelezionato)
                                        importoMaxNew = PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE) 'trova il max dopo quello eliminato
                                        Call AggiornaTipoTariffaSIAE(CStr(importoMaxNew), idOrdineDiPostoSIAE, idPiantaSIAE, True)
                                    Else
                                        Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; non ci sono tipi tariffa SIAE disponibili")
                                    End If
                                Else
                                    If Not EsisteTitoloAssociatoAPrezzo(idPTPMax, numeroStatiTitolo) Then
                                        Call AggiornaPrezzoTitoloProdottoInDB(appoggioPrezzo, appoggioPrevendita, idPrezzoTitoloProdotto, IDTIPOTARIFFASIAE_INTERO)
'                                        Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Add(idTipoTariffaSIAECorrente, ChiaveId(idTipoTariffaSIAECorrente))
                                        Call AggiungiTipoTariffaSIAEAListaDisponibili(idTipoTariffaSIAECorrente)
                                        Call AggiornaTipoTariffaSIAE(CStr(importoMaxOld), idOrdineDiPostoSIAE, idPiantaSIAE, False)
'                                        Call AggiornaTipoTariffaSIAE(CStr(appoggioPrezzo), idOrdineDiPostoSIAE, idPiantaSIAE, True)
                                    Else
                                        Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; prezzo superiore all'attuale I1 sul quale esistono titoli emessi")
                                    End If
                                End If
                            End If
                            'qui va comunque fatto il controllo per vedere se č cambiato I1
                        End If
                    Else
                        importo = IIf(listaNomeAppoggioPrezzi(j) = "", 0, SostituisciVirgolaConPunto(listaNomeAppoggioPrezzi(j)))
                        prevendita = IIf(listaPrevenditaAppoggioPrezzi(j) = "", 0, SostituisciVirgolaConPunto(listaPrevenditaAppoggioPrezzi(j)))
                        importoMaxOld = PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE)
                        If importoMaxOld = 0 Then
                            Call InserisciPrezzoTitoloProdottoInDB(importo, prevendita, CLng(listaIdTariffe(j)), CLng(listaIdArea(i)), IDTIPOTARIFFASIAE_INTERO, False)
                        Else
                            If importo <= importoMaxOld Then
                                isImportoTrovato = IsImportoPresenteInOrdineDiPostoSIAE(CLng(importo), idOrdineDiPostoSIAE, idTipoTariffaSIAECorrente)
                                If isImportoTrovato Then
                                    Call InserisciPrezzoTitoloProdottoInDB(importo, prevendita, CLng(listaIdTariffe(j)), CLng(listaIdArea(i)), idTipoTariffaSIAECorrente, False)
                                ElseIf listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.count > 0 Then
                                    Call InserisciPrezzoTitoloProdottoInDB(importo, prevendita, CLng(listaIdTariffe(j)), CLng(listaIdArea(i)), idNessunElementoSelezionato, False)
                                Else
                                    Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; non ci sono tipi tariffa SIAE disponibili")
                                End If
                            Else
                                If Not EsisteTitoloAssociatoAPrezzo(idPTPMax, numeroStatiTitolo) Then
                                    If listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.count > 0 Then
                                       Call InserisciPrezzoTitoloProdottoInDB(importo, prevendita, CLng(listaIdTariffe(j)), CLng(listaIdArea(i)), IDTIPOTARIFFASIAE_INTERO, False)
                                       Call AggiornaTipoTariffaSIAE(CStr(importoMaxOld), idOrdineDiPostoSIAE, idPiantaSIAE, False)
                                    Else
                                        Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; non ci sono tipi tariffa SIAE disponibili")
                                    End If
                                Else
                                    Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; prezzo superiore all'attuale I1 sul quale esistono titoli emessi")
                                End If
                            End If
                        End If
                    End If
                Else
                    'VERIFICARE SE CANCELLARE I PREZZI IN BD
                    If idPrezzoTitoloProdotto <> idNessunElementoSelezionato Then
                        numeroStatiTitolo = 0
                        If EsisteTitoloAssociatoAPrezzo(idPrezzoTitoloProdotto, numeroStatiTitolo) Then
                            Call listaPrezziNonAggiornati.Add("superarea: " & listaNomeArea(i) & "; tariffa: " & listaNomiTariffe(j) & "; stati titolo: " & numeroStatiTitolo)
                        Else
                            If importoBase < PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE) Then
                                Call EliminaPrezzoTitoloProdottoInDB(idPrezzoTitoloProdotto)
                            Else
                                Call EliminaPrezzoTitoloProdottoInDB(idPrezzoTitoloProdotto)
'                                Call PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE) 'trova il max dopo quello eliminato
'                                Call AggiornaTipoTariffaSIAE(CStr(importoBase), idOrdineDiPostoSIAE, idPiantaSIAE, True)
                                importoMaxNew = PrezzoMaxPerOrdineDiPostoSIAE(idOrdineDiPostoSIAE, idPTPMax, idPiantaSIAE) 'trova il max dopo quello eliminato
                                If importoMaxNew > 0 Then
                                    Call AggiornaTipoTariffaSIAE(CStr(importoMaxNew), idOrdineDiPostoSIAE, idPiantaSIAE, True)
                                End If
                            End If
'                            If Not IsTipoTariffaSIAEDisponibile(idTipoTariffaSIAECorrente) Then
'                                Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Add(idTipoTariffaSIAECorrente, ChiaveId(idTipoTariffaSIAECorrente))
'                            End If
                            Call AggiungiTipoTariffaSIAEAListaDisponibili(idTipoTariffaSIAECorrente)
                        End If
                    End If
                End If
                avanzamento = avanzamento + 1
                Call pgbAvanzamento_Update(avanzamento)
            Next j
            For k = 1 To listaIdTariffeOmaggio.count
                If listaNomeAppoggioPrezzi.Item(j - 1 + k) <> "" Or listaPrevenditaAppoggioPrezzi(j - 1 + k) <> "" Then
                    'controllo di preesistenza nel DB
                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeOmaggio(k), listaIdArea(i), importoBase, importoPrevendita, IDTIPOTARIFFASIAE_OMAGGIO)
                    If idPrezzoTitoloProdotto = idNessunElementoSelezionato Then
                        Call InserisciPrezzoTitoloProdottoInDB(0, 0, CLng(listaIdTariffeOmaggio(k)), listaIdArea(i), IDTIPOTARIFFASIAE_OMAGGIO, True)
                    End If
                Else
                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeOmaggio(k), listaIdArea(i), 0, 0, idNessunElementoSelezionato)
                    If idPrezzoTitoloProdotto <> idNessunElementoSelezionato Then
                        If Not EsisteTitoloAssociatoAPrezzo(idPrezzoTitoloProdotto, 0) Then
                            Call EliminaPrezzoTitoloProdottoInDB(idPrezzoTitoloProdotto)
                        End If
                    End If
                End If
                avanzamento = avanzamento + 1
                Call pgbAvanzamento_Update(avanzamento)
            Next k
'            For k2 = 1 To listaIdTariffeOmaggioConRiferimento.count
'                If listaNomeAppoggioPrezzi.Item(j - 1 + k + k2) <> "" Or listaPrevenditaAppoggioPrezzi(j - 1 + k + k2) <> "" Then
'                    'controllo di preesistenza nel DB
'                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeOmaggioConRiferimento(k2), listaIdArea(i), importoBase, importoPrevendita, IDTIPOTARIFFASIAE_OMAGGIO)
'                    If idPrezzoTitoloProdotto = idNessunElementoSelezionato Then
'                        Call InserisciPrezzoTitoloProdottoInDB(0, 0, CLng(listaIdTariffeOmaggioConRiferimento(k2)), listaIdArea(i), IDTIPOTARIFFASIAE_OMAGGIO, True)
'                    End If
'                Else
'                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeOmaggioConRiferimento(k2), listaIdArea(i), 0, 0, idNessunElementoSelezionato)
'                    If idPrezzoTitoloProdotto <> idNessunElementoSelezionato Then
'                        If Not EsisteTitoloAssociatoAPrezzo(idPrezzoTitoloProdotto, 0) Then
'                            Call EliminaPrezzoTitoloProdottoInDB(idPrezzoTitoloProdotto)
'                        End If
'                    End If
'                End If
'                avanzamento = avanzamento + 1
'                Call pgbAvanzamento_Update(avanzamento)
'            Next k2
            For l = 1 To listaIdTariffeServizio.count
                If listaNomeAppoggioPrezzi.Item(j - 1 + k - 1 + l) <> "" Or listaPrevenditaAppoggioPrezzi(j - 1 + k - 1 + l) <> "" Then
                    'inserire anche il controllo di preesistenza nel DB
                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeServizio(l), listaIdArea(i), importoBase, importoPrevendita, IDTIPOTARIFFASIAE_SERVIZIO)
                    If idPrezzoTitoloProdotto = idNessunElementoSelezionato Then
                        Call InserisciPrezzoTitoloProdottoInDB(0, 0, CLng(listaIdTariffeServizio(l)), listaIdArea(i), IDTIPOTARIFFASIAE_SERVIZIO, False)
                    End If
                Else
                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeServizio(l), listaIdArea(i), 0, 0, idNessunElementoSelezionato)
                    If idPrezzoTitoloProdotto <> idNessunElementoSelezionato Then
                        If Not EsisteTitoloAssociatoAPrezzo(idPrezzoTitoloProdotto, 0) Then
                            Call EliminaPrezzoTitoloProdottoInDB(idPrezzoTitoloProdotto)
                        End If
                    End If
                End If
                avanzamento = avanzamento + 1
                Call pgbAvanzamento_Update(avanzamento)
            Next l
'B: vengono inseriti i record relativi alla tariffa ingresso abbonato (prezzo 0 per tutte le superaree)
            If idTariffaIngressoAbbonato <> idNessunElementoSelezionato Then
                idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(idTariffaIngressoAbbonato, listaIdArea(i), 0, 0, idTipoTariffaSIAECorrente)
                If idPrezzoTitoloProdotto = idNessunElementoSelezionato Then
                    Call InserisciPrezzoTitoloProdottoInDB(0, 0, idTariffaIngressoAbbonato, listaIdArea(i), IDTIPOTARIFFASIAE_INGRESSO_ABBONATO, False)
                End If
            End If

' B1: vengono inseriti i prezzi relativi alle tariffe omaggio con riferimento
            For k2 = 1 To listaIdTariffeOmaggioConRiferimento.count
                If esistePrezzoPerTariffaRiferimento(listaIdTariffeOmaggioConRiferimento(k2), listaIdArea(i)) Then
                    'controllo di preesistenza nel DB
                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeOmaggioConRiferimento(k2), listaIdArea(i), importoBase, importoPrevendita, IDTIPOTARIFFASIAE_OMAGGIO)
                    If idPrezzoTitoloProdotto = idNessunElementoSelezionato Then
                        Call InserisciPrezzoTitoloProdottoInDB(0, 0, CLng(listaIdTariffeOmaggioConRiferimento(k2)), listaIdArea(i), idNessunElementoSelezionato, True)
                    End If
                Else
                    idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeOmaggioConRiferimento(k2), listaIdArea(i), 0, 0, idNessunElementoSelezionato)
                    If idPrezzoTitoloProdotto <> idNessunElementoSelezionato Then
                        If Not EsisteTitoloAssociatoAPrezzo(idPrezzoTitoloProdotto, 0) Then
                            Call EliminaPrezzoTitoloProdottoInDB(idPrezzoTitoloProdotto)
                        End If
                    End If
                End If
            Next k2

            avanzamento = avanzamento + 1
            Call pgbAvanzamento_Update(avanzamento)
        Else
            Call frmMessaggio.Visualizza("NotificaOrdineDiPostoSIAENonConfigurato", listaNomeArea(i))
        End If
    Next i
'C: vengono inseriti i record relativi alla superarea di servizio e alle tariffe di servizio
    For j = 1 To listaIdTariffeServizio.count
        idPrezzoTitoloProdotto = IdPrezzoTitoloProdottoInDB(listaIdTariffeServizio(j), idSuperareaServizio, 0, 0, IDTIPOTARIFFASIAE_SERVIZIO)
        If idPrezzoTitoloProdotto = idNessunElementoSelezionato Then
            Call InserisciPrezzoTitoloProdottoInDB(0, 0, CLng(listaIdTariffeServizio(j)), idSuperareaServizio, IDTIPOTARIFFASIAE_SERVIZIO, False)
        End If
        avanzamento = avanzamento + 1
        Call pgbAvanzamento_Update(avanzamento)
    Next j
    
    Call ChiudiConnessioneBD
    chiave = ChiaveId(idPeriodoCommercialeSelezionato)
    Call listaCompletezzaPeriodiCommerciali.Remove(chiave)
    Call listaCompletezzaPeriodiCommerciali.Add(isDefinitoAlmenoUnPrezzo, chiave)
    If listaPrezziNonAggiornati.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAggiornamentoPrezziNonEseguito", ArgomentoMessaggio(listaPrezziNonAggiornati))
    End If
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub AggiungiTipoTariffaSIAEAListaDisponibili(idTipoTariffaSIAE As Long)

On Error GoTo gestioneErrori
    Call listaTipiTariffaSIAEDisponibiliPerOrdineDiPostoCorrente.Add(idTipoTariffaSIAE, ChiaveId(idTipoTariffaSIAE))
    Exit Sub
gestioneErrori:
    Resume Next
End Sub

Public Function esistePrezzoPerTariffaRiferimento(idTariffa As Long, idArea As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP" & _
        " WHERE T.IDTARIFFA = " & idTariffa & _
        " AND T.IDTARIFFA_RIFERIMENTO = PTP.IDTARIFFA" & _
        " AND PTP.IDAREA = " & idArea
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        cont = rec("CONT")
    End If
    rec.Close
    esistePrezzoPerTariffaRiferimento = (cont = 1)
End Function

