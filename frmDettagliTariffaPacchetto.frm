VERSION 5.00
Begin VB.Form frmDettagliTariffaPacchetto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tariffe"
   ClientHeight    =   1335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   840
      TabIndex        =   2
      Top             =   780
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2520
      TabIndex        =   1
      Top             =   780
      Width           =   1035
   End
   Begin VB.ComboBox cmbTariffa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   300
      Width           =   4275
   End
End
Attribute VB_Name = "frmDettagliTariffaPacchetto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idTariffaSelezionata As Long
Private nomeTariffaSelezionata As String
Private codiceTariffaSelezionata As String
Private labelTariffaSelezionata As String
Private idProdottoSelezionato As Long
Private idOffertaPacchettoSelezionata As Long
Private listaPrezzi As Collection
Private isPrimaTariffaAssociataPerSceltaProdotto As Boolean
Private IdPrimaTariffaAssociataPerSceltaProdotto As Long
Private listaTariffeDisponibili As Collection
Private listaAreePacchetto As Collection

Private exitCode As ExitCodeEnum
Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum

Public Sub Init()
    Dim sql As String

'    Call cmbTariffa.Clear
'    idTariffaSelezionata = idNessunElementoSelezionato
'    sql = "SELECT IDTARIFFA ID, CODICE || ' - ' || NOME LABEL"
'    sql = sql & " FROM TARIFFA"
'    sql = sql & " WHERE IDPRODOTTO = " & idProdottoSelezionato
'    sql = sql & " AND IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
'    sql = sql & " ORDER BY LABEL"
'    Call CaricaValoriCombo(cmbTariffa, sql, "LABEL")
    Call CaricaListaTariffeDisponibili
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = cmbTariffa.ListIndex <> idNessunElementoSelezionato
End Sub

Private Sub cmbTariffa_Click()
    Call cmbTariffa_Update
End Sub

Private Sub cmbTariffa_Update()
    idTariffaSelezionata = cmbTariffa.ItemData(cmbTariffa.ListIndex)
    labelTariffaSelezionata = cmbTariffa.Text
    codiceTariffaSelezionata = Left(labelTariffaSelezionata, 1)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo1 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            descrizione = rec(NomeCampo1)
            cmb.AddItem descrizione
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Function GetIdTariffaSelezionata() As Long
    GetIdTariffaSelezionata = idTariffaSelezionata
End Function

Public Function GetLabelTariffaSelezionata() As String
    GetLabelTariffaSelezionata = labelTariffaSelezionata
End Function

Public Function GetCodiceTariffaSelezionata() As String
    GetCodiceTariffaSelezionata = codiceTariffaSelezionata
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetTipoOffertaPacchettoSelezionata(tipo As TipoOffertaPacchettoEnum)
    tipoOffertaPacchettoSelezionata = tipo
End Sub

Public Sub SetListaPrezzi(lista As Collection)
    Set listaPrezzi = lista
End Sub

Public Sub SetListaAreePacchetto(lista As Collection)
    Set listaAreePacchetto = lista
End Sub

Public Sub SetIsPrimaTariffaAssociataPerSceltaProdotto(statement As Boolean)
    isPrimaTariffaAssociataPerSceltaProdotto = statement
End Sub

Public Sub SetIdPrimaTariffaAssociataPerSceltaProdotto(idTariffa As Long)
    IdPrimaTariffaAssociataPerSceltaProdotto = idTariffa
End Sub

Public Sub SetIdOffertaPacchettoSelezionata(idOffertaPacchetto As Long)
    idOffertaPacchettoSelezionata = idOffertaPacchetto
End Sub

Private Sub CaricaListaTariffeDisponibili_old()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim prezzo As clsPrezzo
    Dim idAreaPacchetto As Long
    Dim areaPacchetto As clsElementoLista
    
    sql = ""
'    Set listaTariffeDisponibili = New Collection
    Select Case tipoOffertaPacchettoSelezionata
        Case TOP_PREZZO_RATEIZZABILE
            For Each prezzo In listaPrezzi
                idAreaPacchetto = IdAreaPacchettoDaIdArea(prezzo.idArea, idOffertaPacchettoSelezionata)
                If sql <> "" Then
                    sql = sql & " INTERSECT"
                End If
                sql = " SELECT DISTINCT T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL,"
                sql = sql & " COUNT(DISTINCT PTP.IMPORTOBASE) PREZZI,"
                sql = sql & " COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) PREVENDITE"
                sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, AREAPACCHETTO_AREA AA, TARIFFA T"
                sql = sql & " WHERE PTP.IDAREA = AA.IDAREA"
                sql = sql & " AND AA.IDAREAPACCHETTO = " & idAreaPacchetto
                sql = sql & " AND PTP.IDPRODOTTO = " & idProdottoSelezionato
                'sql = sql & " AND PTP.IDPERIODOCOMMERCIALE = " & BLABLA
                sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
                sql = sql & " AND PTP.IMPORTOBASE = " & prezzo.importoBase
                sql = sql & " AND PTP.IMPORTOSERVIZIOPREVENDITA = " & prezzo.importoServizioPrevendita
                sql = sql & " GROUP BY T.IDTARIFFA, T.CODICE, T.NOME"
                sql = sql & " HAVING (COUNT(DISTINCT PTP.IMPORTOBASE) = 1"
                sql = sql & " AND COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) = 1)"
            Next prezzo
            sql = sql & " ORDER BY LABEL"
        Case TOP_PREZZO_FISSO
            If IdPrimaTariffaAssociataPerSceltaProdotto = idNessunElementoSelezionato Then
                sql = " SELECT DISTINCT T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
                sql = sql & " FROM TARIFFA T"
                sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoSelezionato
                sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
                sql = sql & " ORDER BY LABEL"
            Else
                For Each prezzo In listaPrezzi
                    If sql <> "" Then
                        sql = sql & " UNION"
                    End If
                    sql = sql & " SELECT DISTINCT T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
                    sql = sql & " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP"
                    sql = sql & " WHERE PTP.IDTARIFFA = T.IDTARIFFA"
                    sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
                    sql = sql & " AND PTP.IDAREA = " & prezzo.idArea
            '        sql = sql & " AND PTP.IDPERIODOCOMMERCIALE = "
                    sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
                    sql = sql & " AND PTP.IMPORTOBASE = " & prezzo.importoBase
                    sql = sql & " AND PTP.IMPORTOSERVIZIOPREVENDITA = " & prezzo.importoServizioPrevendita
                Next prezzo
                sql = sql & " ORDER BY LABEL"
            End If
        Case TOP_PREZZO_VARIABILE
            For Each areaPacchetto In listaAreePacchetto
                If sql <> "" Then
                    sql = sql & " INTERSECT"
                End If
                sql = " SELECT DISTINCT T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL,"
                sql = sql & " COUNT(DISTINCT PTP.IMPORTOBASE) PREZZI,"
                sql = sql & " COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) PREVENDITE"
                sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, AREAPACCHETTO_AREA AA, TARIFFA T"
                sql = sql & " WHERE PTP.IDAREA = AA.IDAREA"
                sql = sql & " AND AA.IDAREAPACCHETTO = " & areaPacchetto.idElementoLista
                sql = sql & " AND PTP.IDPRODOTTO = " & idProdottoSelezionato
                sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
                sql = sql & " GROUP BY T.IDTARIFFA, T.CODICE, T.NOME"
                sql = sql & " HAVING (COUNT(DISTINCT PTP.IMPORTOBASE) = 1"
                sql = sql & " AND COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) = 1)"
            Next areaPacchetto
            sql = sql & " ORDER BY LABEL"
    End Select
    Call CaricaValoriCombo(cmbTariffa, sql, "LABEL")
End Sub

Private Sub CaricaValoriComboDaLista(cmb As ComboBox, listaElementi As Collection)
    Dim i As Integer
    Dim elemento As clsElementoLista
    
    Call cmbTariffa.Clear
    i = 1
    For Each elemento In listaElementi
        cmb.AddItem elemento.descrizioneElementoLista
        cmb.ItemData(i - 1) = elemento.idElementoLista
        i = i + 1
    Next elemento
            
End Sub

Private Sub CaricaListaTariffeDisponibili()
    Set listaTariffeDisponibili = New Collection
    Select Case tipoOffertaPacchettoSelezionata
        Case TOP_PREZZO_RATEIZZABILE
            Call CreaListaTariffeDisponibiliPerPrezziRateizzabili
        Case TOP_PREZZO_FISSO
            Call CreaListaTariffeDisponibiliPerPrezziFissi
        Case TOP_PREZZO_VARIABILE
            Call CreaListaTariffeDisponibiliPerPrezziVariabili
    End Select
    Call CaricaValoriComboDaLista(cmbTariffa, listaTariffeDisponibili)
End Sub

Public Sub CreaListaTariffeDisponibiliPerPrezziRateizzabili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim prezzo As clsPrezzo
    Dim lista As Collection
    Dim idAreaPacchetto As Long
    Dim tariffa As clsElementoLista
    Dim importoBase As String
    Dim importoServizioPrevendita As String
    
    Call ApriConnessioneBD
    
    sql = ""
    Set lista = New Collection
    For Each prezzo In listaPrezzi
        If prezzo.stringaImportoBase = "" Then
            importoBase = " IS NULL"
            importoServizioPrevendita = " IS NULL"
        Else
            importoBase = " = " & prezzo.importoBase
            importoServizioPrevendita = " = " & prezzo.importoServizioPrevendita
        End If
        idAreaPacchetto = IdAreaPacchettoDaIdArea(prezzo.idArea, idOffertaPacchettoSelezionata)
        If sql <> "" Then
            sql = sql & " INTERSECT"
        End If
        sql = sql & " SELECT DISTINCT "
        sql = sql & " T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
        sql = sql & " FROM TARIFFA T,"
'                   TARIFFE DEL PRODOTTO CON UN PREZZO UNICO NELL'AREA PACCHETTO "
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT T.IDTARIFFA,"
        sql = sql & " COUNT(DISTINCT PTP.IMPORTOBASE) IMPORTO,"
        sql = sql & " COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) PREVENDITA"
        sql = sql & " FROM TARIFFA T,"
        sql = sql & " PREZZOTITOLOPRODOTTO PTP,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT AA.IDAREA"
        sql = sql & " FROM AREAPACCHETTO_AREA AA, AREAPACCHETTO A"
        sql = sql & " WHERE AA.IDAREAPACCHETTO = A.IDAREAPACCHETTO"
        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " AND AA.IDAREAPACCHETTO = " & idAreaPacchetto
        sql = sql & " ) AP"
        sql = sql & " WHERE PTP.IDAREA = AP.IDAREA"
        sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
        sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
        sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
        sql = sql & " GROUP BY T.IDTARIFFA"
        sql = sql & " HAVING (COUNT(DISTINCT PTP.IMPORTOBASE) = 1"
        sql = sql & " AND COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) = 1)"
        sql = sql & " ) TP,"
'                   TARIFFE DEL PRODOTTO CON IL PREZZO GIUSTO "
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT T.IDTARIFFA"
        sql = sql & " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP"
        sql = sql & " WHERE PTP.IDTARIFFA = T.IDTARIFFA"
        sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
        sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
        sql = sql & " AND PTP.IMPORTOBASE " & importoBase
        sql = sql & " AND PTP.IMPORTOSERVIZIOPREVENDITA " & importoServizioPrevendita
        sql = sql & " ) P"
        sql = sql & " WHERE T.IDTARIFFA = TP.IDTARIFFA"
        sql = sql & " AND T.IDTARIFFA = P.IDTARIFFA"
    Next prezzo
    sql = sql & " ORDER BY LABEL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockBatchOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tariffa = New clsElementoLista
            tariffa.idElementoLista = rec("ID").Value
            tariffa.descrizioneElementoLista = rec("LABEL").Value
            Call lista.Add(tariffa, ChiaveId(tariffa.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Set listaTariffeDisponibili = lista
End Sub

Public Sub CreaListaTariffeDisponibiliPerPrezziFissi()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim lista As Collection
    Dim prezzo As clsPrezzo
    Dim idAreaPacchetto As Long
    Dim tariffa As clsElementoLista
    Dim areaPacchetto As clsElementoLista
    
    Call ApriConnessioneBD
    
    sql = ""
    Set lista = New Collection
    If IdPrimaTariffaAssociataPerSceltaProdotto = idNessunElementoSelezionato Then
'        sql = " SELECT DISTINCT T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
'        sql = sql & " FROM TARIFFA T"
'        sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoSelezionato
'        sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
'        sql = sql & " ORDER BY LABEL"
        For Each areaPacchetto In listaAreePacchetto
            If sql <> "" Then
                sql = sql & " INTERSECT"
            End If
            sql = sql & " SELECT DISTINCT "
            sql = sql & " T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
            sql = sql & " FROM TARIFFA T,"
    '                   TARIFFE DEL PRODOTTO CON UN PREZZO UNICO NELL'AREA PACCHETTO "
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT T.IDTARIFFA,"
            sql = sql & " COUNT(DISTINCT PTP.IMPORTOBASE) IMPORTO,"
            sql = sql & " COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) PREVENDITA"
            sql = sql & " FROM TARIFFA T,"
            sql = sql & " PREZZOTITOLOPRODOTTO PTP,"
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT AA.IDAREA"
            sql = sql & " FROM AREAPACCHETTO_AREA AA, AREAPACCHETTO A"
            sql = sql & " WHERE AA.IDAREAPACCHETTO = A.IDAREAPACCHETTO"
            sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
            sql = sql & " AND AA.IDAREAPACCHETTO = " & areaPacchetto.idElementoLista
            sql = sql & " AND AA.IDPRODOTTO = " & idProdottoSelezionato
            sql = sql & " ) AP"
            sql = sql & " WHERE PTP.IDAREA = AP.IDAREA"
            sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
            sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
            sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
            sql = sql & " GROUP BY T.IDTARIFFA"
            sql = sql & " HAVING (COUNT(DISTINCT PTP.IMPORTOBASE) = 1"
            sql = sql & " AND COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) = 1)"
            sql = sql & " ) TP"
            sql = sql & " WHERE T.IDTARIFFA = TP.IDTARIFFA"
        Next areaPacchetto
        sql = sql & " ORDER BY LABEL"
    Else
        For Each prezzo In listaPrezzi
            idAreaPacchetto = IdAreaPacchettoDaIdArea(prezzo.idArea, idOffertaPacchettoSelezionata)
            If sql <> "" Then
                sql = sql & " INTERSECT"
            End If
            sql = sql & " SELECT DISTINCT "
            sql = sql & " T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
            sql = sql & " FROM TARIFFA T,"
'                       TARIFFE DEL PRODOTTO CON UN PREZZO UNICO NELL'AREA PACCHETTO "
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT T.IDTARIFFA,"
            sql = sql & " COUNT(DISTINCT PTP.IMPORTOBASE) IMPORTO,"
            sql = sql & " COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) PREVENDITA"
            sql = sql & " FROM TARIFFA T,"
            sql = sql & " PREZZOTITOLOPRODOTTO PTP,"
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT AA.IDAREA"
            sql = sql & " FROM AREAPACCHETTO_AREA AA, AREAPACCHETTO A"
            sql = sql & " WHERE AA.IDAREAPACCHETTO = A.IDAREAPACCHETTO"
            sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
            sql = sql & " AND AA.IDAREAPACCHETTO = " & idAreaPacchetto
            sql = sql & " ) AP"
            sql = sql & " WHERE PTP.IDAREA = AP.IDAREA"
            sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
            sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
            sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
            sql = sql & " GROUP BY T.IDTARIFFA"
            sql = sql & " HAVING (COUNT(DISTINCT PTP.IMPORTOBASE) = 1"
            sql = sql & " AND COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) = 1)"
            sql = sql & " ) TP,"
'                       TARIFFE DEL PRODOTTO CON IL PREZZO GIUSTO "
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT T.IDTARIFFA"
            sql = sql & " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP"
            sql = sql & " WHERE PTP.IDTARIFFA = T.IDTARIFFA"
            sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
            sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
            sql = sql & " AND PTP.IMPORTOBASE = " & prezzo.importoBase
            sql = sql & " AND PTP.IMPORTOSERVIZIOPREVENDITA = " & prezzo.importoServizioPrevendita
            sql = sql & " ) P"
            sql = sql & " WHERE T.IDTARIFFA = TP.IDTARIFFA"
            sql = sql & " AND T.IDTARIFFA = P.IDTARIFFA"
        Next prezzo
        sql = sql & " ORDER BY LABEL"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockBatchOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tariffa = New clsElementoLista
            tariffa.idElementoLista = rec("ID").Value
            tariffa.descrizioneElementoLista = rec("LABEL").Value
            Call lista.Add(tariffa, ChiaveId(tariffa.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Set listaTariffeDisponibili = lista
End Sub

Public Sub CreaListaTariffeDisponibiliPerPrezziVariabili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim areaPacchetto As clsElementoLista
    Dim lista As Collection
    Dim tariffa As clsElementoLista
    
    Call ApriConnessioneBD
    
    sql = ""
    Set lista = New Collection
    For Each areaPacchetto In listaAreePacchetto
        If sql <> "" Then
            sql = sql & " INTERSECT"
        End If
        sql = sql & " SELECT DISTINCT "
        sql = sql & " T.IDTARIFFA ID, T.CODICE || ' - ' || T.NOME LABEL"
        sql = sql & " FROM TARIFFA T,"
'                   TARIFFE DEL PRODOTTO CON UN PREZZO UNICO NELL'AREA PACCHETTO "
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT T.IDTARIFFA,"
        sql = sql & " COUNT(DISTINCT PTP.IMPORTOBASE) IMPORTO,"
        sql = sql & " COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) PREVENDITA"
        sql = sql & " FROM TARIFFA T,"
        sql = sql & " PREZZOTITOLOPRODOTTO PTP,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT AA.IDAREA"
        sql = sql & " FROM AREAPACCHETTO_AREA AA, AREAPACCHETTO A"
        sql = sql & " WHERE AA.IDAREAPACCHETTO = A.IDAREAPACCHETTO"
        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " AND AA.IDAREAPACCHETTO = " & areaPacchetto.idElementoLista
        sql = sql & " AND AA.IDPRODOTTO = " & idProdottoSelezionato
        sql = sql & " ) AP"
        sql = sql & " WHERE PTP.IDAREA = AP.IDAREA"
        sql = sql & " AND PTP.IDTARIFFA = T.IDTARIFFA"
        sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
        sql = sql & " AND T.IDAMBITOAPPLICABILITATARIFFA = " & TAAT_OFFERTA_PACCHETTO
        sql = sql & " GROUP BY T.IDTARIFFA"
        sql = sql & " HAVING (COUNT(DISTINCT PTP.IMPORTOBASE) = 1"
        sql = sql & " AND COUNT(DISTINCT PTP.IMPORTOSERVIZIOPREVENDITA) = 1)"
        sql = sql & " ) TP"
        sql = sql & " WHERE T.IDTARIFFA = TP.IDTARIFFA"
    Next areaPacchetto
    sql = sql & " ORDER BY LABEL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockBatchOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tariffa = New clsElementoLista
            tariffa.idElementoLista = rec("ID").Value
            tariffa.descrizioneElementoLista = rec("LABEL").Value
            Call lista.Add(tariffa, ChiaveId(tariffa.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Set listaTariffeDisponibili = lista
End Sub

