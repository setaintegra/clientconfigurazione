VERSION 5.00
Begin VB.Form frmApriSessione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Apri"
   ClientHeight    =   4950
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   7680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdApri 
      Caption         =   "Apri"
      Default         =   -1  'True
      Height          =   315
      Left            =   180
      TabIndex        =   1
      Top             =   4560
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Annulla"
      Height          =   315
      Left            =   6420
      TabIndex        =   2
      Top             =   4560
      Width           =   1035
   End
   Begin VB.OptionButton optCompleta 
      Caption         =   "tutte"
      Height          =   255
      Left            =   4800
      TabIndex        =   17
      Top             =   1080
      Width           =   2355
   End
   Begin VB.OptionButton optRistretta 
      Caption         =   "aperte su NomeMacchina"
      Height          =   255
      Left            =   4800
      TabIndex        =   16
      Top             =   720
      Value           =   -1  'True
      Width           =   2775
   End
   Begin VB.Frame frameDettagli 
      Caption         =   "Dettagli sessione"
      Height          =   3015
      Left            =   180
      TabIndex        =   5
      Top             =   1440
      Width           =   7275
      Begin VB.TextBox txtDataOraCreazione 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   14
         Top             =   2400
         Width           =   5650
      End
      Begin VB.TextBox txtNomeMacchina 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   12
         Top             =   1920
         Width           =   5650
      End
      Begin VB.TextBox txtTipoSessione 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   10
         Top             =   1440
         Width           =   5650
      End
      Begin VB.TextBox txtDescrizione 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   1500
         MaxLength       =   255
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   720
         Width           =   5650
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   6
         Top             =   300
         Width           =   5650
      End
      Begin VB.Label lblDataOraCreazione 
         Caption         =   "Data ora creazione"
         Height          =   375
         Left            =   120
         TabIndex        =   15
         Top             =   2400
         Width           =   1395
      End
      Begin VB.Label lblNomeMacchina 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome macchina"
         Height          =   255
         Left            =   300
         TabIndex        =   13
         Top             =   1920
         Width           =   1155
      End
      Begin VB.Label lblTipoSessione 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo sessione"
         Height          =   315
         Left            =   480
         TabIndex        =   11
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   315
         Left            =   420
         TabIndex        =   9
         Top             =   780
         Width           =   1035
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   195
         Left            =   420
         TabIndex        =   7
         Top             =   360
         Width           =   1035
      End
   End
   Begin VB.ComboBox cmbSessione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   900
      Width           =   2775
   End
   Begin VB.Line Line4 
      BorderColor     =   &H00FFFFFF&
      X1              =   4560
      X2              =   4380
      Y1              =   1020
      Y2              =   1020
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000005&
      X1              =   4560
      X2              =   4560
      Y1              =   840
      Y2              =   1185
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000005&
      X1              =   4820
      X2              =   4550
      Y1              =   1200
      Y2              =   1200
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000005&
      X1              =   4800
      X2              =   4560
      Y1              =   840
      Y2              =   840
   End
   Begin VB.Label lblSottotitolo 
      Caption         =   "Visualizza le sessioni ..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4800
      TabIndex        =   18
      Top             =   360
      Width           =   2175
   End
   Begin VB.Label lblSessioniAperte 
      Caption         =   "Sessioni aperte"
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label lblTitolo 
      Caption         =   "Apri sessione di configurazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   4215
   End
End
Attribute VB_Name = "frmApriSessione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sessioneSelezionata As clsSessione 'Oggetto locale del form
Private filtroSessioni As TipoListaSessioni   'Variabile per l'inizializzazione della lista delle sessioni

Private internalEvent As Boolean

Private Sub AggiornaAbilitazioneControlli()
    optRistretta.Caption = "aperte su " & getNomeMacchina
    cmdApri.Enabled = sessioneSelezionata.id <> 0
End Sub

Private Sub frameDatiSessione_Clear()
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDataOraCreazione.Text = ""
    txtNomeMacchina.Text = ""
    txtTipoSessione.Text = ""
    
End Sub

Private Sub Annulla()
    Unload Me
End Sub

Private Sub CaricaComboSessione()
    Dim sql As String
    
    Call cmbSessione.Clear
        
    sql = " SELECT IDSESSIONECONFIGURAZIONE  ID, NOME FROM CC_SESSIONECONFIGURAZIONE " & _
          " WHERE IDSESSIONECONFIGURAZIONE>0 " & _
          " AND DATAORACONFERMA IS NULL "
          
    If filtroSessioni = TLS_LISTA_RISTRETTA Then
        sql = sql & "AND NOMEMACCHINA = " & SqlStringValue(getNomeMacchina) & _
              " ORDER BY NOME"
    Else
         sql = sql & " ORDER BY NOME"
    End If
    
    Call CaricaValoriCombo(cmbSessione, sql, "NOME")
    Call sessioneSelezionata_Update
    
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub filtroSessioni_Update()
    If optRistretta.Value Then
        filtroSessioni = TLS_LISTA_RISTRETTA
    Else
        filtroSessioni = TLS_LISTA_COMPLETA
    End If
End Sub

Public Sub Init()
    Set sessioneSelezionata = New clsSessione
    Call CaricaComboSessione
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub sessioneSelezionata_Update()
    If cmbSessione.ListIndex <> -1 Then
        sessioneSelezionata.id = cmbSessione.ItemData(cmbSessione.ListIndex)
        sessioneSelezionata.nome = cmbSessione.List(cmbSessione.ListIndex)
    End If
End Sub

Private Sub frameDatiSessione_Update()

    sessioneSelezionata.CaricaDallaBaseDatiTramiteId (sessioneSelezionata.id)

    txtNome.Text = sessioneSelezionata.nome
    txtDescrizione.Text = sessioneSelezionata.descrizione
    txtDataOraCreazione.Text = sessioneSelezionata.dataOraCreazione
    txtNomeMacchina.Text = sessioneSelezionata.NomeMacchina
    txtTipoSessione.Text = sessioneSelezionata.tipoSessione
    
End Sub

Private Sub cmbSessione_Click()
    If Not internalEvent Then
        sessioneSelezionata_Update
        frameDatiSessione_Update
        AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
        Call Annulla
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdApri_Click()
    Call Apri
End Sub

Private Sub Apri()
    idSessioneConfigurazioneCorrente = sessioneSelezionata.id
    tipoSessioneConfigurazione = sessioneSelezionata.idTipoSessione
    Call ScriviLog(CCTA_APERTURA_SESSIONE, CCDA_SESSIONE_CONFIGURAZIONE, , sessioneSelezionata.tipoSessione)
    Unload Me
End Sub

Private Sub optCompleta_Click()
    If Not internalEvent Then
        Dim mousePointerOld As Integer
        mousePointerOld = MousePointer
        MousePointer = vbHourglass
        
            Call filtroSessioni_Update
            Call CaricaComboSessione
            Call frameDatiSessione_Clear
            Call AggiornaAbilitazioneControlli
            
        MousePointer = mousePointerOld
    End If
End Sub

Private Sub optRistretta_Click()
    If Not internalEvent Then
        Dim mousePointerOld As Integer
        mousePointerOld = MousePointer
        MousePointer = vbHourglass
        
            Call filtroSessioni_Update
            Call CaricaComboSessione
            Call frameDatiSessione_Clear
            Call AggiornaAbilitazioneControlli
            
        MousePointer = mousePointerOld
    End If
End Sub


