VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPianta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idPianta As Long
Private nomePianta As String
Private descrizionePianta As String
Public collAree As New Collection

Public Sub inizializza(idP As Long, tipoG As TipoGrigliaEnum, Optional idA As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Dim area As classeArea
'    Dim FASCIA As classeFascia
'    Dim SEQUENZA As classeSequenza
    
On Error GoTo gestioneErrori
    
    idPianta = idP
    sql = "SELECT IDPIANTA, NOME, DESCRIZIONE FROM PIANTA WHERE IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    idPianta = rec("IDPIANTA")
    nomePianta = rec("NOME")
    descrizionePianta = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    rec.Close
    
'   aree
    Select Case tipoG
        Case TG_PICCOLI_IMPIANTI
'            sql = "SELECT IDAREA, NOME, DESCRIZIONE FROM AREA WHERE" & _
'                " IDPIANTA = " & idPianta & " AND IDAREA_PADRE NOT IN" & _
'                " (SELECT IDAREA FROM AREA WHERE IDAREA_PADRE IS NOT NULL)"
'            sql = "SELECT A1.IDAREA IDA, A1.NOME, A1.DESCRIZIONE" & _
'                " FROM AREA A1, AREA A2" & _
'                " WHERE A1.IDAREA_PADRE = A2.IDAREA" & _
'                " AND A1.IDPIANTA = " & idPianta & _
'                " AND A1.IDTIPOAREA IN (2, 3)"
            sql = "SELECT A1.IDAREA IDA, A1.NOME, A1.DESCRIZIONE"
            sql = sql & " FROM AREA A1"
            sql = sql & " WHERE A1.IDPIANTA = " & idPianta
            sql = sql & " AND A1.IDTIPOAREA IN (2, 3)"
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    Set area = New classeArea
                    area.idArea = rec("IDA")
                    Call area.inizializza(area.idArea)
                    Call collAree.Add(area, ChiaveId(area.idArea))
                    rec.MoveNext
                Wend
            End If
            rec.Close
        Case TG_GRANDI_IMPIANTI
            Set area = New classeArea
            Call area.inizializza(idA)
            Call collAree.Add(area, ChiaveId(area.idArea))
    End Select
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Public Function ottieniAreaDaIdArea_old(idA As Long) As classeArea
    Dim a As classeArea
    
    For Each a In collAree
        If a.idArea = idA Then
            Set ottieniAreaDaIdArea_old = a
        End If
    Next a
End Function

Public Function ottieniAreaDaIdArea(idA As Long) As classeArea
    Set ottieniAreaDaIdArea = collAree.Item(ChiaveId(idA))
End Function

