VERSION 5.00
Begin VB.Form frmInserimentoProdottoRappresentazione 
   Caption         =   "Inserimento rappresentazione"
   ClientHeight    =   6165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7530
   LinkTopic       =   "Form1"
   ScaleHeight     =   6165
   ScaleWidth      =   7530
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   465
      Left            =   5625
      TabIndex        =   11
      Top             =   5280
      Width           =   1140
   End
   Begin VB.Frame fraPulsanti 
      Height          =   915
      Left            =   150
      TabIndex        =   9
      Top             =   4980
      Width           =   1740
      Begin VB.CommandButton cmdAssociaRappresentazioneProdotto 
         Caption         =   "Inserisci"
         Height          =   390
         Left            =   225
         TabIndex        =   10
         Top             =   300
         Width           =   1290
      End
   End
   Begin VB.ComboBox cmbProdottoInConfigurazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   4485
      Width           =   3780
   End
   Begin VB.ComboBox cmbRappresentazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   3795
      Width           =   3780
   End
   Begin VB.ComboBox cmbProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   3045
      Width           =   3780
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   2370
      Width           =   3795
   End
   Begin VB.Label Label1 
      Height          =   615
      Left            =   75
      TabIndex        =   13
      Top             =   1080
      Width           =   7365
   End
   Begin VB.Label lblMessaggio 
      Caption         =   "Questa maschera � utilizzata per associare una rappresentazione ad un prodotto in configurazione."
      Height          =   615
      Left            =   75
      TabIndex        =   12
      Top             =   675
      Width           =   7365
   End
   Begin VB.Label lblProdottoInConfigurazione 
      Caption         =   "Prodotti in configurazione disponibili"
      Height          =   195
      Left            =   75
      TabIndex        =   8
      Top             =   4245
      Width           =   2775
   End
   Begin VB.Label lblRappresentazione 
      Caption         =   "Rappresentazioni"
      Height          =   195
      Left            =   75
      TabIndex        =   6
      Top             =   3555
      Width           =   1500
   End
   Begin VB.Label lblProdotto 
      Caption         =   "Abbonamenti attivi"
      Height          =   195
      Left            =   75
      TabIndex        =   4
      Top             =   2820
      Width           =   1500
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazioni"
      Height          =   255
      Left            =   75
      TabIndex        =   2
      Top             =   2070
      Width           =   1470
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione rappresentazione gi� esistente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   150
      TabIndex        =   0
      Top             =   150
      Width           =   5775
   End
End
Attribute VB_Name = "frmInserimentoProdottoRappresentazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idRappresentazioneSelezionata As Long
Private idProdottoInConfigurazioneSelezionato As Long

Private Sub cmbOrganizzazione_Click()
    Call cmbProdotto_Update
    Call cmbProdottoInConfigurazione_Update
End Sub

Private Sub cmbProdotto_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
    sql = "SELECT   P.idprodotto ID, P.nome NOME"
    sql = sql & " FROM prodotto P, tipostatoprodotto TSP"
    sql = sql & " WHERE TSP.idtipostatoprodotto = P.idtipostatoprodotto"
    sql = sql & " AND P.idorganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " AND TSP.idtipostatoprodotto = " & TSP_ATTIVO
    sql = sql & " AND P.idclasseprodotto = " & CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO
    sql = sql & " ORDER BY P.nome ASC"

    Call CaricaValoriCombo(cmbProdotto, sql, "NOME")
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbProdotto_Click()
    Call cmbRappresentazione_Update
End Sub

Private Sub cmbRappresentazione_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
    
    sql = "SELECT R.IDRAPPRESENTAZIONE ID, S.NOME || ' ' || R.DATAORAINIZIO NOME"
    sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, SPETTACOLO S"
    sql = sql & " Where PR.idProdotto = " & idProdottoSelezionato
    sql = sql & " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " AND R.IDSPETTACOLO = S.IDSPETTACOLO"
    sql = sql & " ORDER BY R.DATAORAINIZIO ASC"

    Call CaricaValoriCombo(cmbRappresentazione, sql, "NOME")
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmbProdottoInConfigurazione_Click()
    idProdottoInConfigurazioneSelezionato = cmbProdottoInConfigurazione.ItemData(cmbProdottoInConfigurazione.ListIndex)
    If cmbProdottoInConfigurazione.ListIndex > -1 And cmbRappresentazione.ListIndex > -1 Then
        idRecordSelezionato = 1
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli

End Sub

Private Sub cmbProdottoInConfigurazione_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
    sql = "SELECT P.IDPRODOTTO ID, P.NOME NOME"
    sql = sql & " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP"
    sql = sql & " Where TSP.IDTIPOSTATOPRODOTTO = p.IDTIPOSTATOPRODOTTO"
    sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND TSP.IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE
    sql = sql & " AND P.IDCLASSEPRODOTTO = " & CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO
    sql = sql & " UNION"
    sql = sql & " SELECT P.IDPRODOTTO ID, P.NOME NOME"
    sql = sql & " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP, PRODOTTO_RAPPRESENTAZIONE PR"
    sql = sql & " Where TSP.IDTIPOSTATOPRODOTTO = p.IDTIPOSTATOPRODOTTO"
    sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND TSP.IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE
    sql = sql & " AND P.IDCLASSEPRODOTTO = " & CPR_BIGLIETTERIA_ORDINARIA
    sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO (+)"
    sql = sql & " AND PR.IDPRODOTTO IS NULL"
    sql = sql & " ORDER BY NOME ASC"

    Call CaricaValoriCombo(cmbProdottoInConfigurazione, sql, "NOME")
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbRappresentazione_Click()
    idRappresentazioneSelezionata = cmbRappresentazione.ItemData(cmbRappresentazione.ListIndex)
    If cmbProdottoInConfigurazione.ListIndex > -1 And cmbRappresentazione.ListIndex > -1 Then
        idRecordSelezionato = 1
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmdAssociaRappresentazioneProdotto_Click()
    Dim sql As String
    Dim sql2 As String
    Dim n As Long
    Dim msg As String
    Dim esisteRecord As Boolean
    Dim rec As New ADODB.Recordset
    
    On Error GoTo gestioneErrori
    
    sql = " INSERT INTO PRODOTTO_RAPPRESENTAZIONE"
    sql = sql & " (IDPRODOTTO, IDRAPPRESENTAZIONE)"
    sql = sql & " VALUES( " & idProdottoInConfigurazioneSelezionato & " , " & idRappresentazioneSelezionata & " )"

    sql2 = "SELECT COUNT(IDPRODOTTO) NUMPROD FROM PRODOTTO_RAPPRESENTAZIONE"
    sql2 = sql2 & " WHERE IDPRODOTTO = " & idProdottoInConfigurazioneSelezionato
    sql2 = sql2 & " AND IDRAPPRESENTAZIONE = " & idRappresentazioneSelezionata
            
    Call ApriConnessioneBD
    rec.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
    esisteRecord = (rec("NUMPROD") > 0)
    
    rec.Close
    Call ChiudiConnessioneBD
    
    If Not esisteRecord Then
        Call ApriConnessioneBD
        SETAConnection.BeginTrans
        
        SETAConnection.Execute sql, n, adCmdText
        
        If (n = 1) Then
            Call frmMessaggio.Visualizza("ConfermaInserimentoAssociazioneRappresentazioneProdotto")
            
            If frmMessaggio.exitCode = EC_CONFERMA Then
                SETAConnection.CommitTrans
                MsgBox "Avvenuto inserimento", vbInformation, "Associazione"
            Else
                SETAConnection.RollbackTrans
                MsgBox "Operazione annullata", vbInformation, "Associazione"
            End If
        Else
            SETAConnection.RollbackTrans
            msg = "Errore nell'inserimento: sono stati inseriti " & n & " record!" & vbCrLf
            msg = msg & "Operazione annullata"
            MsgBox msg, vbCritical, "Attenzione"
        End If
        
        Call ChiudiConnessioneBD
    Else
        msg = "Esiste gi� l'associazione tra " & vbCrLf
        msg = msg & " il prodotto " & idProdottoInConfigurazioneSelezionato & vbCrLf
        msg = msg & " e la rappresentazione " & idRappresentazioneSelezionata
        MsgBox msg, vbCritical, "Attenzione"
    End If
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Public Sub Init()
    Dim sql As String

    sql = "SELECT   O.idorganizzazione ID, O.nome NOME"
    'sql = sql & " tipoStatoOrganizzazione.idTipoStatoOrganizzazione"
    sql = sql & " From organizzazione O, tipoStatoOrganizzazione TSO"
    sql = sql & " WHERE (    (TSO.idtipostatoorganizzazione ="
    sql = sql & " O.idTipoStatoOrganizzazione"
    sql = sql & " )"
    sql = sql & " AND (TSO.idtipostatoorganizzazione = " & TSO_ATTIVA & ")"
    sql = sql & " )"
    sql = sql & " ORDER BY O.nome ASC"

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idProdottoSelezionato = idNessunElementoSelezionato
    idRappresentazioneSelezionata = idNessunElementoSelezionato
    
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdAssociaRappresentazioneProdotto.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

