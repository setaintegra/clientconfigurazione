VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSceltaOperatore 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Operatore"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   900
      Width           =   4095
   End
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   4
      Top             =   7620
      Width           =   5295
      Begin VB.CommandButton cmdClona 
         Caption         =   "Clona"
         Height          =   435
         Left            =   2700
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   3
      Top             =   8100
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcOperatore 
      Height          =   375
      Left            =   9180
      Top             =   120
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrOperatoriConfigurati 
      Height          =   6075
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10716
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione dell'Operatore"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   7335
   End
End
Attribute VB_Name = "frmSceltaOperatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long

Private internalEvent As Boolean
Private exitCodeFormIniziale As ExitCodeEnum

Private Sub cmbOrganizzazione_Click()
    Call cmbOrganizzazione_Update
End Sub

Private Sub cmbOrganizzazione_Update()
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ModificaOperatore
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdClona_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CaricaFormClonazioneOperatore
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaOperatore
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call NuovoOperatore
    
    MousePointer = mousePointerOld
End Sub

Private Sub NuovoOperatore()
    Call frmInizialeOperatore.SetModalitāForm(A_NUOVO)
    Call frmInizialeOperatore.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaOperatore()
    Call frmInizialeOperatore.SetModalitāForm(A_MODIFICA)
    Call frmInizialeOperatore.SetIdOperatoreSelezionato(idRecordSelezionato)
    Call frmInizialeOperatore.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmInizialeOperatore.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormClonazioneOperatore()
    Call frmClonazioneOperatore.SetIdOperatoreSelezionato(idRecordSelezionato)
    Call frmClonazioneOperatore.Init
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaOperatore()
    Dim operatoreEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
        
    Set listaTabelleCorrelate = New Collection
    
    Call frmInizialeOperatore.SetModalitāForm(A_ELIMINA)
    
    operatoreEliminabile = True
    If Not IsRecordEliminabile("IDOPERATORECORRENTE", "SUPPORTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("SUPPORTO")
        operatoreEliminabile = False
    End If
    If Not IsRecordEliminabile("IDOPERATOREPRECEDENTE", "SUPPORTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("SUPPORTO")
        operatoreEliminabile = False
    End If
    If Not IsRecordEliminabile("IDOPERATORE", "OPERAZIONE", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("OPERAZIONE")
        operatoreEliminabile = False
    End If
    ' da controllar anche IDRICEVITORIA
'    If Not frmInizialeOperatore.RecordEliminabile("IDOPERATORE", "CAUSALEPROTEZIONE") Then
'        Call listaTabelleCorrelate.Add("CAUSALEPROTEZIONE")
'        operatoreEliminabile = False
'    End If

    If operatoreEliminabile Then
        Call frmInizialeOperatore.SetIdOperatoreSelezionato(idRecordSelezionato)
        Call frmInizialeOperatore.Init
'        Call AggiornaDataGrid
'        Call dgrOperatoriConfigurati_Init
        If exitCodeFormIniziale = EC_CONFERMA Then
            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
            Call CaricaValoriGriglia
'            Call SelezionaElementoSuGriglia(idRecordSelezionato)
        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaDisabilitazioneOperatore")
        Call frmInizialeOperatore.SetOperatoreAbilitato(0)
    End If
    
'    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Unload Me
End Sub
Private Sub AggiornaAbilitazioneControlli()

    dgrOperatoriConfigurati.Columns(0).Visible = False
    dgrOperatoriConfigurati.Caption = "OPERATORI CONFIGURATI"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdClona.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
End Sub

Private Sub dgrOperatoriConfigurati_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrOperatoriConfigurati, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcOperatore.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init()
    Dim sql As String
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE ORDER BY NOME"
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcOperatore_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call dgrOperatoriConfigurati_Init
End Sub

Private Sub adcOperatore_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcOperatore
    
    Select Case idOrganizzazioneSelezionata
        Case idTuttiGliElementiSelezionati
            sql = "SELECT DISTINCT OPERATORE.IDOPERATORE AS ID, OPERATORE.USERNAME AS ""Username""," & _
                " DECODE (ABILITATO, 0, 'NO', 1, 'SI') AS ""Abilitato""" & _
                " FROM OPERATORE WHERE" & _
                " OPERATORE.IDPERFEZIONATORE IS NULL" & _
                " ORDER BY OPERATORE.USERNAME"
        Case Else
            sql = "SELECT DISTINCT OP.IDOPERATORE AS ID, OP.USERNAME AS ""Username""," & _
                " DECODE (OP.ABILITATO, 0, 'NO', 1, 'SI') AS ""Abilitato""" & _
                " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP" & _
                " WHERE OP.IDPERFEZIONATORE IS NULL" & _
                " AND OP.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA" & _
                " AND OCP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " ORDER BY OP.USERNAME"
    End Select
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrOperatoriConfigurati.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrOperatoriConfigurati_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrOperatoriConfigurati
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi * 2)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcOperatore.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetExitCodeFormIniziale(exC As ExitCodeEnum)
    exitCodeFormIniziale = exC
End Sub

