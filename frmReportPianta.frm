VERSION 5.00
Begin VB.Form frmReportPianta 
   Caption         =   "Report pianta"
   ClientHeight    =   3720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8310
   LinkTopic       =   "Form1"
   ScaleHeight     =   3720
   ScaleWidth      =   8310
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbPianta 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1080
      Width           =   8025
   End
   Begin VB.ComboBox cmbVenue 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   360
      Width           =   8025
   End
   Begin VB.CommandButton cmdChiudi 
      Caption         =   "Chiudi"
      Height          =   495
      Left            =   3120
      TabIndex        =   1
      Top             =   2880
      Width           =   2055
   End
   Begin VB.CommandButton cmdCreaFileExcel 
      Caption         =   "Crea file Excel"
      Height          =   495
      Left            =   3120
      TabIndex        =   0
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label lblReportInPreparazione 
      Alignment       =   2  'Center
      Caption         =   "Report in preparazione ..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   2880
      TabIndex        =   6
      Top             =   2400
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.Label lblPianta 
      Caption         =   "Pianta"
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label lblVenue 
      Caption         =   "Venue"
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frmReportPianta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private idVenueSelezionato As Long
Private idPiantaSelezionata As Long
Private tipoSelezione As TipoSelezioneSuPiantaEnum

Private internalEvent As Boolean

Public Sub Init()
    Call CaricaComboVenue
    idPiantaSelezionata = idNessunElementoSelezionato
    Call Me.Show
End Sub

Private Sub CaricaComboVenue()
    Dim sql As String
    
    Call cmbVenue.Clear
    sql = "SELECT IDVENUE ID, NOME FROM VENUE ORDER BY NOME"
    Call CaricaValoriCombo3(cmbVenue, sql, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbVenue_Click()
    idVenueSelezionato = cmbVenue.ItemData(cmbVenue.ListIndex)
    Call CaricaComboPianta
End Sub

Private Sub CaricaComboPianta()
    Dim sql As String
    
    Call cmbPianta.Clear
    sql = "SELECT P.IDPIANTA ID, P.NOME AS NOME" & _
        " FROM PIANTA P, VENUE_PIANTA VP, GRIGLIAPOSTI GP" & _
        " WHERE VP.IDVENUE = " & idVenueSelezionato & _
        " AND VP.IDPIANTA = P.IDPIANTA" & _
        " AND P.IDPIANTA = GP.IDPIANTA(+)" & _
        " AND GP.IDPIANTA IS NULL" & _
        " ORDER BY P.NOME"
        
        Call CaricaValoriCombo3(cmbPianta, sql, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbPianta_Click()
    idPiantaSelezionata = cmbPianta.ItemData(cmbPianta.ListIndex)
End Sub

Private Sub cmdChiudi_Click()
    Unload Me
End Sub

Private Sub cmdCreaFileExcel_Click()
    Call CreaFileExcel
End Sub

Private Sub CreaFileExcel()
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim mousePointerOld As Integer
    Dim rSettore As Long
    Dim r As Long
    Dim c As Long
    Dim superareaCorrente As String
    Dim settoreCorrente As String
    Dim filaCorrente As String
    Dim dimensioneVerticale As Long
    Dim dimensioneOrizzontale As Long
    Dim coordinataVerticale As Long
    Dim superarea As String
    Dim settore As String
    Dim fila As String
    Dim posto As String
    Dim x As Long
    Dim y As Long
    Dim contatoreSuperaree As Long

    If idPiantaSelezionata = idNessunElementoSelezionato Then
        MsgBox "Nessuna pianta selezionata!"
    Else
    
        mousePointerOld = MousePointer
        MousePointer = vbHourglass
        
        lblReportInPreparazione.Visible = True
        
        sql = "SELECT SA.CODICE COD_SA, SA.NOME SA, A.CODICE || ' - ' || A.NOME SETT, NOMEFILA, NOMEPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE, MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE" & _
            " FROM AREA SA, AREA A, POSTO P, GRIGLIAPOSTI GP" & _
            " WHERE A.IDPIANTA = " & idPiantaSelezionata & _
            " AND A.IDAREA_PADRE = SA.IDAREA" & _
            " AND A.IDAREA = P.IDAREA" & _
            " AND A.IDAREA = GP.IDAREA" & _
            " ORDER BY SA.CODICE, A.CODICE, COORDINATAVERTICALE"
        
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet
        
        ' prime righe intestazione pianta ...... da capire ....
'        xlsSheet.Rows.Cells(1, 1) = "PRODOTTO"
        
        superareaCorrente = ""
        settoreCorrente = ""
        filaCorrente = ""
        contatoreSuperaree = 2

'        Cells(1, 1).Borders.LineStyle = xlContinuous
'        Cells(1, 3).Borders.LineStyle = xlEdgeBottom
'        Cells(1, 5).Borders.LineStyle = xlEdgeLeft
'        Cells(1, 7).Cells.Borders(xlEdgeLeft).LineStyle = xlContinuous

        Call ApriConnessioneBD
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF

                superarea = rec("COD_SA")
                If superarea <> superareaCorrente Then
                
                    If superareaCorrente <> "" Then
                        Call xlsSheet.Columns("C:ZZ").Select
                        Call xlsSheet.Columns("C:ZZ").EntireColumn.AutoFit
                    End If
        
'                    xlsDoc.Sheets.Add
'                    Set xlsSheet = xlsDoc.Sheets("Foglio" & contatoreSuperaree)
'                    xlsSheet.Select
'                    xlsSheet.Name = superarea
'                    xlsSheet.Move after:=xlsDoc.Sheets(contatoreSuperaree - 1)
                    xlsDoc.Sheets.Add
                    Set xlsSheet = xlsDoc.Sheets(contatoreSuperaree - 1)
                    xlsSheet.Select
                    xlsSheet.Move after:=xlsDoc.Sheets(contatoreSuperaree)
                    xlsSheet.Name = superarea
                    
                    xlsSheet.Rows.Cells(1, 1) = "Superarea: " & rec("SA")
                    
                    superareaCorrente = superarea
                    contatoreSuperaree = contatoreSuperaree + 1
                    
                    settoreCorrente = ""
                End If

                settore = rec("SETT")
                If settore <> settoreCorrente Then
                    If settoreCorrente = "" Then
                        rSettore = 3
                    Else
                        rSettore = rSettore + dimensioneVerticale + 2
                    End If
                    xlsSheet.Rows.Cells(rSettore, 1) = "Settore: " & settore

                    dimensioneVerticale = rec("MASSIMACOORDINATAVERTICALE")
                    dimensioneOrizzontale = rec("MASSIMACOORDINATAORIZZONTALE")
                    coordinataVerticale = -1

                    For c = 3 To 3 + dimensioneOrizzontale - 2
                        xlsSheet.Rows.Cells(rSettore, c).Cells.Borders(xlEdgeBottom).LineStyle = xlContinuous
                        xlsSheet.Rows.Cells(rSettore + dimensioneVerticale, c).Cells.Borders(xlEdgeTop).LineStyle = xlContinuous
                    Next c
                    For r = rSettore + 1 To rSettore + dimensioneVerticale - 1
                        xlsSheet.Rows.Cells(r, 2).Cells.Borders(xlEdgeRight).LineStyle = xlContinuous
                        xlsSheet.Rows.Cells(r, 2 + dimensioneOrizzontale).Cells.Borders(xlEdgeLeft).LineStyle = xlContinuous
                    Next r

                    settoreCorrente = settore
                End If

                ' per ora si considera una sola fila per coordinata verticale
                If coordinataVerticale <> rec("COORDINATAVERTICALE") Then
                    coordinataVerticale = rec("COORDINATAVERTICALE")
                    xlsSheet.Rows.Cells(rSettore + coordinataVerticale, 2) = rec("NOMEFILA")
                End If

                posto = rec("NOMEPOSTO")
                x = rec("COORDINATAORIZZONTALE")
                y = rec("COORDINATAVERTICALE")
                xlsSheet.Rows.Cells(rSettore + y, 2 + x) = posto

                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

        Call xlsSheet.Columns("C:ZZ").Select
        Call xlsSheet.Columns("C:ZZ").EntireColumn.AutoFit
        
'        xlsDoc.Sheets("Foglio1").Delete
'        xlsDoc.Sheets("Foglio2").Delete
'        xlsDoc.Sheets("Foglio3").Delete
        
        nomeFile = App.Path & "\" & idPiantaSelezionata & " - "
        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing
    
        MsgBox "Report salvato sul file: " & nomeFile
        lblReportInPreparazione.Visible = False
        
        MousePointer = mousePointerOld
        
    End If
    
End Sub

