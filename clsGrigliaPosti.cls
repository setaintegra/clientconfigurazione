VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGrigliaPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public listaPostiInGriglia As Collection
Public listaPostiEliminati As Collection
Public listaPostiCreati As Collection
Public listaPostiModificati As Collection

Public Sub GrigliaPosti_Init_OLD(tipoGriglia As TipoGrigliaEnum, idArea As Long, idPianta As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim x As Integer
    Dim Y As Integer
    Dim id As Long
    Dim idA As Long
    Dim i As Integer
    Dim j As Integer
    Dim nomeF As String
    Dim nomeP As String
    Dim nomeA As String
    Dim indice As Long
    Dim idSeq As Long
    Dim posto As clsPosto
    
    Call ApriConnessioneBD
    
    Set listaPostiInGriglia = New Collection
    Set listaPostiCreati = New Collection
    Set listaPostiEliminati = New Collection
    Set listaPostiModificati = New Collection
        
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            sql = "SELECT P.IDPOSTO, P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE,"
            sql = sql & " P.NOMEFILA, P.NOMEPOSTO, P.IDSEQUENZAPOSTI, A.IDAREA,"
            sql = sql & " A.INDICEDIPREFERIBILITA"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDAREA = " & idArea
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT DISTINCT P.IDPOSTO, P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE,"
            sql = sql & " P.NOMEFILA, P.NOMEPOSTO, P.IDSEQUENZAPOSTI, A.IDAREA,"
            sql = sql & " A.INDICEDIPREFERIBILITA, A.NOME NOMEAREA"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPianta
        Case Else
    End Select
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            id = rec("IDPOSTO")
            x = rec("COORDINATAORIZZONTALE")
            Y = rec("COORDINATAVERTICALE")
            nomeF = rec("NOMEFILA")
            nomeP = rec("NOMEPOSTO")
            indice = rec("INDICEDIPREFERIBILITA")
            idA = rec("IDAREA")
            If IsNull(rec("IDSEQUENZAPOSTI")) Then
                idSeq = idNessunElementoSelezionato
            Else
                idSeq = rec("IDSEQUENZAPOSTI")
            End If
            Set posto = New clsPosto
            If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                nomeA = rec("NOMEAREA")
                posto.nomeArea = nomeA
            End If
            posto.idPosto = id
            posto.idArea = idA
            posto.xPosto = x
            posto.yPosto = Y
            posto.nomeFila = nomeF
            posto.nomePosto = nomeP
            posto.idSequenzaPosti = idSeq
            posto.indiceDiPreferibilitÓArea = indice
            Call listaPostiInGriglia.Add(posto, ChiaveId(posto.idPosto))
            rec.MoveNext
        Loop
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
End Sub

Public Function OttieniPostoDaId_OLD(listaPosti As Collection, idPosto As Long) As clsPosto
    Dim p As clsPosto
    Dim i As Integer
    Dim trovato As Boolean
    
    i = 1
    trovato = False
    While i <= listaPosti.count And Not trovato
        Set p = listaPosti.Item(i)
        If idPosto = p.idPosto Then
            trovato = True
            Set OttieniPostoDaId_OLD = p
        End If
        i = i + 1
    Wend
'    If Not trovato Then
'        Set OttieniPostoDaId = Nothing
'    End If
End Function

Public Function OttieniPostoDaId(listaPosti As Collection, idPosto As Long) As clsPosto
    On Error GoTo gestioneErrori
    Set OttieniPostoDaId = listaPosti.Item(ChiaveId(idPosto))
    Exit Function
gestioneErrori:
    Set OttieniPostoDaId = Nothing
End Function

Public Sub EliminaElementoDaLista_OLD(lista As Collection, id As Long)
    Dim i As Integer
    Dim trovato As Boolean
    Dim p As clsPosto

    i = 1
    trovato = False
    While i <= lista.count And Not trovato
        Set p = lista(i)
        If id = p.idPosto Then
            trovato = True
            Call lista.Remove(i)
        End If
        i = i + 1
    Wend
End Sub

Public Sub EliminaElementoDaLista(lista As Collection, id As Long)
On Error Resume Next
    Call lista.Remove(ChiaveId(id))
End Sub

Public Function IsPostoDuplicato_OLD(posto As clsPosto) As Boolean
    Dim p As clsPosto
    Dim i As Integer
    Dim trovato As Boolean
    
    i = 1
    trovato = False
    While i < listaPostiInGriglia.count And Not trovato
        Set p = listaPostiInGriglia(i)
        If p.idArea = posto.idArea And p.nomeFila = posto.nomeFila And p.nomePosto = posto.nomePosto Then
            trovato = True
        End If
        i = i + 1
    Wend
    IsPostoDuplicato_OLD = trovato
End Function

Public Function IsPostoDuplicato(posto As clsPosto, listaPosti As Collection) As Boolean
    Dim p As clsPosto
    Dim i As Integer
    Dim trovato As Boolean
    
    i = 1
    trovato = False
    If Not (listaPosti Is Nothing) Then
        While i <= listaPosti.count And Not trovato
            Set p = listaPosti(i)
            If p.idArea = posto.idArea And p.nomeFila = posto.nomeFila And p.nomePosto = posto.nomePosto Then
                trovato = True
            End If
            i = i + 1
        Wend
    End If
    IsPostoDuplicato = trovato
End Function

Public Function ClonaListaPostiInGriglia() As Collection
    Dim p As clsPosto
    Dim lista As New Collection
        
    For Each p In listaPostiInGriglia
        Call lista.Add(p, ChiaveId(p.idPosto))
    Next p
    Set ClonaListaPostiInGriglia = lista
End Function

Public Function EsistonoPostiSenzaAttributi(lista As Collection) As Boolean
    Dim p As clsPosto
    Dim trovato As Boolean
    
    For Each p In lista
        If p.nomeFila = "" Or p.nomePosto = "" Or p.idArea = idNessunElementoSelezionato Then
            trovato = True
        End If
    Next p
    EsistonoPostiSenzaAttributi = trovato
End Function

Public Sub GrigliaPosti_Init(tipoGriglia As TipoGrigliaEnum, idArea As Long, idPianta As Long)
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim x As Integer
    Dim Y As Integer
    Dim id As Long
    Dim idA As Long
    Dim i As Integer
    Dim j As Integer
    Dim nomeF As String
    Dim nomeP As String
    Dim nomeA As String
    Dim indice As Long
    Dim idSeq As Long
    Dim posto As clsPosto
    
    Call ApriConnessioneBD_ORA
    
    ORADB.BeginTrans
    
    Set listaPostiInGriglia = New Collection
    Set listaPostiCreati = New Collection
    Set listaPostiEliminati = New Collection
    Set listaPostiModificati = New Collection
        
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            sql = "SELECT P.IDPOSTO, P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE,"
            sql = sql & " P.NOMEFILA, P.NOMEPOSTO, P.IDSEQUENZAPOSTI, A.IDAREA,"
            sql = sql & " A.INDICEDIPREFERIBILITA"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDAREA = " & idArea
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT DISTINCT P.IDPOSTO, P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE,"
            sql = sql & " P.NOMEFILA, P.NOMEPOSTO, P.IDSEQUENZAPOSTI, A.IDAREA,"
            sql = sql & " A.INDICEDIPREFERIBILITA, A.NOME NOMEAREA"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPianta
        Case Else
    End Select
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            id = rec("IDPOSTO")
            x = rec("COORDINATAORIZZONTALE")
            Y = rec("COORDINATAVERTICALE")
            nomeF = rec("NOMEFILA")
            nomeP = rec("NOMEPOSTO")
            indice = rec("INDICEDIPREFERIBILITA")
            idA = rec("IDAREA")
            If IsNull(rec("IDSEQUENZAPOSTI")) Then
                idSeq = idNessunElementoSelezionato
            Else
                idSeq = rec("IDSEQUENZAPOSTI")
            End If
            Set posto = New clsPosto
            If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                nomeA = rec("NOMEAREA")
                posto.nomeArea = nomeA
            End If
            posto.idPosto = id
            posto.idArea = idA
            posto.xPosto = x
            posto.yPosto = Y
            posto.nomeFila = nomeF
            posto.nomePosto = nomeP
            posto.idSequenzaPosti = idSeq
            posto.indiceDiPreferibilitÓArea = indice
            Call listaPostiInGriglia.Add(posto, ChiaveId(posto.idPosto))
            rec.MoveNext
        Loop
    End If
    rec.Close
    
    ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
End Sub


