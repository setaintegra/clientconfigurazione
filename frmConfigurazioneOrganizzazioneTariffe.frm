VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneOrganizzazioneTariffe 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   4140
      MultiSelect     =   2  'Extended
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   5760
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   7080
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   6660
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   6240
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   8220
      MultiSelect     =   2  'Extended
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5760
      Width           =   3555
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   20
      Top             =   5760
      Width           =   3795
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   19
      Top             =   5100
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizioneAlternativa 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   18
      Top             =   7140
      Width           =   3315
   End
   Begin VB.ComboBox cmbTipoTariffa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4200
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   5100
      Width           =   2475
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3600
      MaxLength       =   1
      TabIndex        =   16
      Top             =   5100
      Width           =   375
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   5820
      Width           =   435
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   13
      Top             =   240
      Width           =   3255
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   10
      Top             =   3720
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   7
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   6
      Top             =   8100
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneOrganizzazioneTariffe 
      Height          =   330
      Left            =   5880
      Top             =   120
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneOrganizzazioneTariffe 
      Height          =   2775
      Left            =   120
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   4895
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Tariffe standard selezionate"
      Height          =   195
      Left            =   8220
      TabIndex        =   32
      Top             =   5520
      Width           =   3555
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Tariffe standard disponibili"
      Height          =   195
      Left            =   4140
      TabIndex        =   31
      Top             =   5520
      Width           =   3555
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   30
      Top             =   5520
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   29
      Top             =   4860
      Width           =   1695
   End
   Begin VB.Label lblTipoTariffa 
      Caption         =   "Tipo Tariffa - Cod."
      Height          =   255
      Left            =   4200
      TabIndex        =   28
      Top             =   4860
      Width           =   1695
   End
   Begin VB.Label lblDescrizioneAlternativa 
      Caption         =   "Descrizione alternativa"
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   6900
      Width           =   1695
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   3600
      TabIndex        =   26
      Top             =   4860
      Width           =   555
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   14
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   12
      Top             =   3480
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   3480
      Width           =   1815
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Tariffe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   4815
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneTariffe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idCampoTariffaTL As Long
Private idTipoTariffa As Long
Private idStagioneSelezionata As Long
Private descrizioneRecordSelezionato As String
Private descrizioneAlternativaRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private codice As String
Private codiceTipoTariffa As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private isRecordEditabile As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private tipoTariffa As TipoOrigineTariffaEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazioneOrganizzazioneTariffe.Caption = "TARIFFE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            tipoTariffa = TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTariffe.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblCodice.Enabled = False
        lblTipoTariffa.Enabled = False
        cmdSelezionato.Enabled = True
        cmdDidsponibile.Enabled = True
        cmdSvuotaSelezionati.Enabled = True
        cmdSvuotaDisponibili.Enabled = True
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO And _
            tipoTariffa = TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTariffe.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbTipoTariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipoTariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                        Len(Trim(txtCodice.Text)) = 1 And _
                        cmbTipoTariffa.ListIndex <> idNessunElementoSelezionato)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            tipoTariffa <> TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTariffe.Enabled = False
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblCodice.Enabled = False
        lblTipoTariffa.Enabled = False
        cmdSelezionato.Enabled = True
        cmdDidsponibile.Enabled = True
        cmdSvuotaSelezionati.Enabled = True
        cmdSvuotaDisponibili.Enabled = True
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (lstSelezionati.ListCount > 0)
        cmdAnnulla.Enabled = True
        Select Case tipoTariffa
            Case TOT_STANDARD
                lblOperazioneInCorso.Caption = "Operazione in corso:"
                lblOperazione.Caption = "inserimento nuovo record"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            tipoTariffa = TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTariffe.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        Call lstSelezionati.Clear
        lstDisponibili.Enabled = True
        lstSelezionati.Enabled = True
        lblDisponibili.Enabled = True
        lblSelezionati.Enabled = True
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblCodice.Enabled = False
        lblTipoTariffa.Enabled = False
        cmdSelezionato.Enabled = True
        cmdDidsponibile.Enabled = True
        cmdSvuotaSelezionati.Enabled = True
        cmdSvuotaDisponibili.Enabled = True
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub cmbTipoTariffa_Click()
'    If Not internalEvent Then
        idTipoTariffa = cmbTipoTariffa.ItemData(cmbTipoTariffa.ListIndex)
'        Call CaricaTipiTariffaSiae
'    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call CaricaValoriLstDisponibili
    Call AggiornaAbilitazioneControlli
    Set listaSelezionati = New Collection
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
    "; IDTARIFFA = " & idRecordSelezionato

    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoOrganizzazione = TSO_ATTIVA Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If ValoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_TARIFFA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneTariffe_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazioneTariffe_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_TARIFFA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneTariffe_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazioneTariffe_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_ORGANIZZAZIONE, CCDA_TARIFFA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneTariffe_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazioneTariffe_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_TARIFFA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneTariffe_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneOrganizzazioneTariffe_Init
                    End Select
                    
                    Select Case tipoTariffa
                        Case TOT_STANDARD
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_TARIFFA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneTariffe_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneOrganizzazioneTariffe_Init
                    End Select
                End If
                
                Call CaricaValoriLstDisponibili
                Call AggiornaAbilitazioneControlli
                Set listaSelezionati = New Collection
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaDaSelezionatiADisponibili
End Sub

Private Sub SpostaDaSelezionatiADisponibili()
    Call SpostaInLstDisponibili
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_STANDARD)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idTariffa As Long
    Dim tariffa As clsElementoLista
    Dim chiaveTariffa As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idTariffa = lstSelezionati.ItemData(i - 1)
            chiaveTariffa = ChiaveId(idTariffa)
            Set tariffa = listaSelezionati.Item(chiaveTariffa)
            Call listaDisponibili.Add(tariffa, chiaveTariffa)
            Call listaSelezionati.Remove(chiaveTariffa)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sqlTipoTariffa As String
    
    sqlTipoTariffa = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipoTariffa, "COD")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sqlTipoTariffa As String
    Dim sqlTipoTariffaSiae As String
    
    sqlTipoTariffa = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipoTariffa, "COD")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sqlTipoTariffa As String
    
    isRecordEditabile = True
    sqlTipoTariffa = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipoTariffa, "COD")
    
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTariffa As String
    Dim tariffa As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    sql = "SELECT DISTINCT IDTARIFFA, NOME AS ""Nome"", DESCRIZIONE AS ""Descrizione""" & _
        " FROM TARIFFA WHERE" & _
        " IDORGANIZZAZIONE IS NULL AND" & _
        " IDPRODOTTO IS NULL" & _
        " ORDER BY ""Nome"""
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tariffa = New clsElementoLista
            tariffa.idElementoLista = rec("IDTARIFFA").Value
            tariffa.nomeElementoLista = rec("NOME")
            tariffa.descrizioneElementoLista = rec("NOME")
            chiaveTariffa = ChiaveId(tariffa.idElementoLista)
            Call listaDisponibili.Add(tariffa, chiaveTariffa)
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tariffa As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each tariffa In listaDisponibili
            lstDisponibili.AddItem tariffa.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = tariffa.idElementoLista
            i = i + 1
        Next tariffa
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tariffa As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each tariffa In listaSelezionati
            lstSelezionati.AddItem tariffa.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = tariffa.idElementoLista
            i = i + 1
        Next tariffa
    End If
           
    internalEvent = internalEventOld

End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Public Sub SetTipoTariffa(tipo As TipoOrigineTariffaEnum)
    tipoTariffa = tipo
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneOrganizzazioneTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sqlTipoTariffa As String
    
    sqlTipoTariffa = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME" & _
        " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipoTariffa, "COD")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaDaDisponibiliASelezionati
End Sub

Private Sub SpostaDaDisponibiliASelezionati()
    isRecordEditabile = True
    Call SpostaInLstSelezionati
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_STANDARD)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idTariffa As Long
    Dim tariffa As clsElementoLista
    Dim chiaveTariffa As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idTariffa = lstDisponibili.ItemData(i - 1)
            chiaveTariffa = ChiaveId(idTariffa)
            Set tariffa = listaDisponibili.Item(chiaveTariffa)
            Call listaSelezionati.Add(tariffa, chiaveTariffa)
            Call listaDisponibili.Remove(chiaveTariffa)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SpostaTuttiInDisponibili
End Sub

Private Sub SpostaTuttiInDisponibili()
    Call SvuotaSelezionati
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'    Call SetTipoTariffa(TOT_STANDARD_ORGANIZZAZIONE)
    Call SetTipoTariffa(TOT_STANDARD)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaSelezionati()
    Dim tariffa As clsElementoLista
    Dim chiaveTariffa As String
    
    For Each tariffa In listaSelezionati
        chiaveTariffa = ChiaveId(tariffa.idElementoLista)
        Call listaDisponibili.Add(tariffa, chiaveTariffa)
    Next tariffa
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub dgrConfigurazioneOrganizzazioneTariffe_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call adcConfigurazioneOrganizzazioneTariffe_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneOrganizzazioneTariffe_Init
    Call CaricaValoriLstDisponibili
    Set listaSelezionati = New Collection
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneOrganizzazioneTariffe.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneOrganizzazioneTariffe_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneOrganizzazioneTariffe
        
    sql = "SELECT T.IDTARIFFA AS ""ID"", T.NOME AS ""Nome""," & _
        " T.DESCRIZIONE AS ""Descrizione"", T.DESCRIZIONEALTERNATIVA AS ""Descr. altern.""," & _
        " T.IDTIPOTARIFFA AS IDTT," & _
        " T.CODICE AS ""Codice""," & _
        " TT.CODICE || ' - ' || TT.NOME AS ""Codice - Tipo""" & _
        " FROM TARIFFA T, TIPOTARIFFA TT WHERE" & _
        " T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA AND" & _
        " T.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & " AND" & _
        " T.IDPRODOTTO IS NULL" & _
        " ORDER BY ""Codice"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    Set dgrConfigurazioneOrganizzazioneTariffe.dataSource = d
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim max As Long
    Dim i As Integer
    Dim n As Long
    Dim idNuovaTariffa As Long
    Dim tariffa As clsElementoLista
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    Select Case tipoTariffa
        Case TOT_STANDARD
            For Each tariffa In listaSelezionati
                idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
                sql = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, CODICE," & _
                    " DESCRIZIONEALTERNATIVA, IDTIPOTARIFFA," & _
                    " IDPRODOTTO, IDORGANIZZAZIONE)" & _
                    " (SELECT " & _
                    idNuovaTariffa & "," & _
                    " NOME, DESCRIZIONE, CODICE, DESCRIZIONEALTERNATIVA, IDTIPOTARIFFA," & _
                    " NULL, " & _
                    idOrganizzazioneSelezionata & " FROM TARIFFA WHERE" & _
                    " IDTARIFFA = " & tariffa.idElementoLista & ")"
                SETAConnection.Execute sql, n, adCmdText
            Next tariffa
        Case Else
            idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
            sql = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, CODICE," & _
                " DESCRIZIONEALTERNATIVA, IDTIPOTARIFFA," & _
                " IDPRODOTTO, IDORGANIZZAZIONE)" & _
                " VALUES (" & _
                idNuovaTariffa & ", " & _
                SqlStringValue(nomeRecordSelezionato) & ", " & _
                SqlStringValue(descrizioneRecordSelezionato) & ", " & _
                SqlStringValue(codice) & ", " & _
                SqlStringValue(descrizioneAlternativaRecordSelezionato) & ", " & _
                idTipoTariffa & ", " & _
                "NULL, " & _
                idOrganizzazioneSelezionata & ")"
            SETAConnection.Execute sql, n, adCmdText
    End Select
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("TARIFFA", "IDTARIFFA", idNuovaTariffa, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaTariffa)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT T.IDTARIFFA ID, T.NOME NOME, T.CODICE COD," & _
'            " T.DESCRIZIONE DESCR, T.DESCRIZIONEALTERNATIVA DESCRALT," & _
'            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE," & _
'            " T.IDTIPOTARIFFA IDTT, TT.NOME TIPOTAR, TT.CODICE TIPOCOD" & _
'            " FROM TARIFFA T, TIPOTARIFFA TT" & _
'            " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA" & _
'            " AND T.IDTARIFFA = " & idRecordSelezionato
'    Else
        sql = "SELECT T.IDTARIFFA ID, T.NOME NOME, T.CODICE COD," & _
            " T.DESCRIZIONE DESCR, T.DESCRIZIONEALTERNATIVA DESCRALT," & _
            " T.IDTIPOTARIFFA IDTT, TT.NOME TIPOTAR, TT.CODICE TIPOCOD" & _
            " FROM TARIFFA T, TIPOTARIFFA TT" & _
            " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA" & _
            " AND T.IDTARIFFA = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCR")), "", rec("DESCR"))
        descrizioneAlternativaRecordSelezionato = IIf(IsNull(rec("DESCRALT")), "", rec("DESCRALT"))
        idTipoTariffa = rec("IDTT").Value
        codice = rec("COD")
        codiceTipoTariffa = rec("TIPOCOD")
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDescrizioneAlternativa.Text = ""
    txtCodice.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizioneAlternativa.Text = descrizioneAlternativaRecordSelezionato
    txtCodice.Text = codice
    Call SelezionaElementoSuCombo(cmbTipoTariffa, idTipoTariffa)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE TARIFFA SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " DESCRIZIONEALTERNATIVA = " & SqlStringValue(descrizioneAlternativaRecordSelezionato) & "," & _
        " CODICE = " & SqlStringValue(codice) & "," & _
        " IDPRODOTTO = NULL" & "," & _
        " IDTIPOTARIFFA = " & idTipoTariffa & "," & _
        " IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " WHERE IDTARIFFA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("TARIFFA", "IDTARIFFA", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazioneOrganizzazioneTariffe_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneOrganizzazioneTariffe
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtCodice_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String
    Dim tariffa As clsElementoLista

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTARIFFA <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    codice = Trim(txtCodice.Text)
    If tipoTariffa = TOT_STANDARD Then
        If Not (listaSelezionati Is Nothing) Then
            For Each tariffa In listaSelezionati
                If ViolataUnicitā("TARIFFA", "NOME = " & SqlStringValue(tariffa.descrizioneElementoLista), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
                    condizioneSql, "", "") Then
                    ValoriCampiOK = False
                    Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(tariffa.descrizioneElementoLista) & _
                        " č giā presente in DB per la stessa organizzazione;")
                End If
            Next tariffa
        End If
    Else
        If ViolataUnicitā("TARIFFA", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
                " č giā presente in DB per la stessa organizzazione;")
        End If
        If ViolataUnicitā("TARIFFA", "CODICE = " & SqlStringValue(codice), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore codice = " & SqlStringValue(codice) & _
                " č giā presente in DB per la stessa organizzazione;")
        End If
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrizioneAlternativaRecordSelezionato = Trim(txtDescrizioneAlternativa.Text)
    If IsNumeric(codice) Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo codice non deve essere numerico;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Dim i As Integer
'    Dim tariffa As clsElementoLista
'
'    Set listaCampiValoriUnici = New Collection
'
'    If tipoTariffa = TOT_STANDARD Then
'        If Not (listaSelezionati Is Nothing) Then
'            For Each tariffa In listaSelezionati
'                listaCampiValoriUnici.Add ("NOME = " & SqlStringValue(tariffa.nomeElementoLista))
'            Next tariffa
'        End If
'    Else
'        Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'        Call listaCampiValoriUnici.Add("CODICE = " & SqlStringValue(codice))
'    End If
'End Sub
'
Private Sub GetIdTipoTariffaSelezionata()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneOrganizzazioneTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        Call rec.Move(0)
        idTipoTariffa = rec("IDTT").Value
    Else
        idTipoTariffa = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String

    Call ApriConnessioneBD
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idRecordSelezionato
'            SETAConnection.Execute sql, , adCmdText
'        Else
'            Call AggiornaParametriSessioneSuRecord("TARIFFA", "IDTARIFFA", idRecordSelezionato, TSR_ELIMINATO)
'        End If
'    Else
        sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idRecordSelezionato
        SETAConnection.Execute sql, , adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Sub SvuotaDisponibili()
    Dim tariffa As clsElementoLista
    Dim chiaveTariffa As String
    
    isRecordEditabile = True
    For Each tariffa In listaDisponibili
        chiaveTariffa = ChiaveId(tariffa.idElementoLista)
        Call listaSelezionati.Add(tariffa, chiaveTariffa)
    Next tariffa
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub









