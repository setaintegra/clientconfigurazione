VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoSuperareeTipiMedium 
   Caption         =   "frmConfigurazioneProdottoSuperareeTipiMedium"
   ClientHeight    =   7110
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11565
   LinkTopic       =   "Form1"
   ScaleHeight     =   7110
   ScaleWidth      =   11565
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConfermaAssociazione 
      Caption         =   "Conferma associazione"
      Height          =   555
      Left            =   4560
      TabIndex        =   8
      Top             =   5040
      Width           =   3435
   End
   Begin VB.ListBox lstTipiSupporto 
      Height          =   3570
      Left            =   4560
      TabIndex        =   7
      Top             =   1080
      Width           =   3495
   End
   Begin VB.ListBox lstSuperaree 
      Height          =   4545
      Left            =   360
      TabIndex        =   6
      Top             =   1080
      Width           =   3495
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10200
      TabIndex        =   5
      Top             =   6480
      Width           =   1275
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9780
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8040
      TabIndex        =   0
      Top             =   240
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9780
      TabIndex        =   4
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8040
      TabIndex        =   3
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Gestione tipi anagrafiche"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   6375
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoSuperareeTipiMedium"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private internalEvent As Boolean
Private operazioniEffettuate As Boolean

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
End Sub

Public Sub Init(idPianta As Long, idProdotto As Long, nomeProdotto As String, idOrganizzazione As Long, nomeOrganizzazione As String)

    idPiantaSelezionata = idPianta
    idProdottoSelezionato = idProdotto
    nomeProdottoSelezionato = nomeProdotto
    idOrganizzazioneSelezionata = idOrganizzazione
    nomeOrganizzazioneSelezionata = nomeOrganizzazione
    
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLstSuperaree
    
    Call AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdCancella_Click()
    Dim mousePointerOld As Integer
    Dim risposta As VbMsgBoxResult
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConfermaAssociazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call AssociaSuperareaATipoSupporto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Dim risposta As VbMsgBoxResult
    
    If operazioniEffettuate Then
        risposta = MsgBox("Ci sono operazioni in sospeso non salvate: sei sicuro di voler uscire?", vbYesNo)
        If risposta = vbYes Then
            Unload Me
        End If
    Else
        Unload Me
    End If
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub CaricaValoriLstSuperaree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    Dim i As Integer
    
    lstSuperaree.Clear
    Call ApriConnessioneBD
    
    sql = "SELECT SA.IDAREA ID, SA.CODICE || ' - ' || SA.NOME NOME" & _
        " FROM AREA SA" & _
        " WHERE SA.IDTIPOAREA = " & TA_SUPERAREA_NUMERATA & _
        " AND SA.IDPIANTA = " & idPiantaSelezionata & _
        " ORDER BY SA.CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstSuperaree.AddItem rec("NOME")
            lstSuperaree.ItemData(i) = rec("ID")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CaricaValoriLstTipiSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    Dim i As Integer
    
    Call ApriConnessioneBD
    
    sql = "SELECT SA.IDAREA ID, SA.CODICE || ' - ' SA.NOME NOME" & _
        " FROM AREA SA" & _
        " WHERE SA.IDTIPOAREA = " & TA_SUPERAREA_NUMERATA & _
        " AND SA.IDPIANTA = " & idPiantaSelezionata & _
        " ORDER BY SA.CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstSuperaree.AddItem rec("NOME")
            lstSuperaree.ItemData(i) = rec("ID")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub lstSuperaree_Click()
    Dim idSuperareaSelezionata As Long
    Dim i As Integer
    
    For i = 0 To lstSuperaree.ListCount - 1
        If lstSuperaree.Selected(i) Then
            idSuperareaSelezionata = lstSuperaree.ItemData(i)
        End If
    Next i
    Call SelezionaSuperArea(idSuperareaSelezionata)
    
End Sub

Private Sub SelezionaSuperArea(idSuperareaSelezionata As Long)
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Integer
    
    lstTipiSupporto.Clear
    
    ' query errata
'    sql = "SELECT TS.IDTIPOSUPPORTO, TS.NOME, DECODE (STS.IDSUPERAREA, " & idSuperareaSelezionata & ", 1, 0) ASSOCIATA" & _
'            " FROM (" & _
'            " SELECT TS.IDTIPOSUPPORTO, TS.NOME" & _
'            " FROM TSDIGCONSENTITOPERORG TSCO" & _
'            " INNER JOIN TIPOSUPPORTO TS ON TSCO.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
'            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " UNION" & _
'            " SELECT TS.IDTIPOSUPPORTO, TS.NOME" & _
'            " FROM TSDIGCONSENTITOPERORG TSCO" & _
'            " INNER JOIN TIPOSUPPORTO TS ON TSCO.IDTIPOSUPPORTOSIAE = TS.IDTIPOSUPPORTOSIAE" & _
'            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            ") TS" & _
'            " LEFT OUTER JOIN SUPERAREA_TIPOSUPPORTODIGITALE STS ON TS.IDTIPOSUPPORTO = STS.IDTIPOSUPPORTODIGITALE" & _
'            " ORDER BY NOME"
    sql = "SELECT TS.IDTIPOSUPPORTO, TS.NOME, DECODE (STS.IDSUPERAREA, NULL, 0, 1) ASSOCIATA" & _
            " FROM (" & _
            " SELECT TS.IDTIPOSUPPORTO, TS.NOME" & _
            " FROM TSDIGCONSENTITOPERORG TSCO" & _
            " INNER JOIN TIPOSUPPORTO TS ON TSCO.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " UNION" & _
            " SELECT TS.IDTIPOSUPPORTO, TS.NOME" & _
            " FROM TSDIGCONSENTITOPERORG TSCO" & _
            " INNER JOIN TIPOSUPPORTO TS ON TSCO.IDTIPOSUPPORTOSIAE = TS.IDTIPOSUPPORTOSIAE" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            ") TS" & _
            " LEFT OUTER JOIN SUPERAREA_TIPOSUPPORTODIGITALE STS ON TS.IDTIPOSUPPORTO = STS.IDTIPOSUPPORTODIGITALE AND STS.IDSUPERAREA = " & idSuperareaSelezionata & _
            " ORDER BY NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstTipiSupporto.AddItem rec("NOME")
            lstTipiSupporto.ItemData(i - 1) = rec("IDTIPOSUPPORTO")
            
            If rec("ASSOCIATA") = 1 Then
                lstTipiSupporto.Selected(i - 1) = True
            Else
                lstTipiSupporto.Selected(i - 1) = False
            End If
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
End Sub

Private Sub AssociaSuperareaATipoSupporto()
    Dim i As Integer
    Dim idSuperareaSelezionata As Long
    Dim idTipoSupportoSelezionato As Long
    Dim sql As String
    Dim n As Integer
    
    i = 0
    idSuperareaSelezionata = idNessunElementoSelezionato
    While i < lstSuperaree.ListCount And idSuperareaSelezionata = idNessunElementoSelezionato
        If lstSuperaree.Selected(i) Then
            idSuperareaSelezionata = lstSuperaree.ItemData(i)
        End If
        i = i + 1
    Wend

    i = 0
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    While i < lstTipiSupporto.ListCount And idTipoSupportoSelezionato = idNessunElementoSelezionato
        If lstTipiSupporto.Selected(i) Then
            idTipoSupportoSelezionato = lstTipiSupporto.ItemData(i)
        End If
        i = i + 1
    Wend

    If idSuperareaSelezionata <> idNessunElementoSelezionato And idTipoSupportoSelezionato <> idNessunElementoSelezionato Then
        Call ApriConnessioneBD_ORA
        ORADB.BeginTrans
    
        ' cancellaConfigurazioneCorrente
        sql = "DELETE FROM SUPERAREA_TIPOSUPPORTODIGITALE" & _
                " WHERE IDSUPERAREA = " & idSuperareaSelezionata
        n = ORADB.ExecuteSQL(sql)
        
        sql = "INSERT INTO SUPERAREA_TIPOSUPPORTODIGITALE(IDSUPERAREA, IDTIPOSUPPORTODIGITALE)" & _
                " VALUES (" & idSuperareaSelezionata & ", " & idTipoSupportoSelezionato & ")"
        n = ORADB.ExecuteSQL(sql)
        
        ORADB.CommitTrans
        Call ChiudiConnessioneBD_ORA
    
    End If

End Sub

