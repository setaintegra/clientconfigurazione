VERSION 5.00
Begin VB.Form frmConfigurazionePacchettoOperatori 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVerificaOperatività 
      Caption         =   "Verifica operatività"
      Height          =   315
      Left            =   4260
      TabIndex        =   6
      Top             =   6540
      Width           =   3555
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   14
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   13
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   12
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3780
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1920
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Left            =   4260
      MultiSelect     =   2  'Extended
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1860
      Width           =   3555
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3780
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3060
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3780
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   4200
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3780
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   5340
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Left            =   180
      MultiSelect     =   2  'Extended
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1860
      Width           =   3555
   End
   Begin VB.Label lblNota 
      Caption         =   "NOTA: sono disponibili i soli operatori associati a tutti i prodotti dell'offerta pacchetto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   255
      Left            =   180
      TabIndex        =   19
      Top             =   1200
      Width           =   8175
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   18
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Diritti Operatore sull'Offerta pacchetto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   7095
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili - Abilitato"
      Height          =   195
      Left            =   180
      TabIndex        =   16
      Top             =   1620
      Width           =   3555
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati - Abilitato"
      Height          =   195
      Left            =   4260
      TabIndex        =   15
      Top             =   1620
      Width           =   3555
   End
End
Attribute VB_Name = "frmConfigurazionePacchettoOperatori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOffertaPacchettoSelezionata As Long
Private nomeOffertaPacchettoSelezionata As String
Private internalEvent As Boolean
Private listaDisponibili As Collection
Private listaSelezionati As Collection
'Private isRecordEditabile As Boolean
Private listaOperatoriNonOperativi As Collection

Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitàFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Public Sub SetIdOffertaPacchettoSelezionata(id As Long)
    idOffertaPacchettoSelezionata = id
End Sub

Public Sub SetNomeOffertaPacchettoSelezionata(nome As String)
    nomeOffertaPacchettoSelezionata = nome
End Sub

Public Sub SetTipoOffertaPacchettoSelezionata(tipo As TipoOffertaPacchettoEnum)
    tipoOffertaPacchettoSelezionata = tipo
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pacchetto"
    txtInfo1.Text = nomeOffertaPacchettoSelezionata
    txtInfo1.Enabled = False
    
    If Not (listaSelezionati Is Nothing) Then
        If listaSelezionati.count > 0 Then
            cmdVerificaOperatività.Enabled = True
        Else
            cmdVerificaOperatività.Enabled = False
        End If
    Else
        cmdVerificaOperatività.Enabled = False
    End If
    
    Select Case modalitàFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Fine"
            cmdSuccessivo.Enabled = True
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalitàForm(mf As AzioneEnum)
    modalitàFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
'    If tipoModalitàConfigurazione = TMC_TRANSAZIONALE Then
''        sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
''        sql = sql & " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO"
''        sql = sql & " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)"
''        sql = sql & " AND (OP.IDOPERATORE IS NULL OR OP.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")"
''        sql = sql & " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato
''        sql = sql & " AND O.ABILITATO = " & VB_VERO
''        sql = sql & " AND O.IDOPERATORE = OO.IDOPERATORE"
''        sql = sql & " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
''        sql = sql & " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA"
''        sql = sql & " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")"
''        sql = sql & " OR O.IDPUNTOVENDITA IS NULL)"
''        sql = sql & " ORDER BY USERNAME"
'    Else
        sql = " SELECT OPPROD.IDOPERATORE ID, O.USERNAME,"
        sql = sql & " DECODE(O.ABILITATO, 1, 'SI', 'NO') ABILITATO"
        sql = sql & " FROM OPERATORE_PRODOTTO OPPROD, OPERATORE O,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT OO.IDOPERATORE"
        sql = sql & " FROM OPERATORE_OFFERTAPACCHETTO OO"
        sql = sql & " WHERE OO.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ) OS,"
        sql = sql & " ("
        sql = sql & " SELECT SPP.IDPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP"
        sql = sql & " WHERE SP.IDSCELTAPRODOTTO = SPP.IDSCELTAPRODOTTO"
        sql = sql & " AND SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ) A"
        sql = sql & " WHERE A.IDPRODOTTO = OPPROD.IDPRODOTTO"
        sql = sql & " AND OPPROD.IDOPERATORE = O.IDOPERATORE"
        sql = sql & " AND OS.IDOPERATORE(+) = O.IDOPERATORE"
        sql = sql & " AND OS.IDOPERATORE IS NULL"
        sql = sql & " GROUP BY  OPPROD.IDOPERATORE, O.USERNAME, ABILITATO"
        sql = sql & " HAVING COUNT(OPPROD.IDOPERATORE) ="
        sql = sql & " ("
        sql = sql & " SELECT COUNT(SPP.IDPRODOTTO)"
        sql = sql & " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP"
        sql = sql & " WHERE SP.IDSCELTAPRODOTTO = SPP.IDSCELTAPRODOTTO"
        sql = sql & " AND SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ) "
        sql = sql & " ORDER BY O.USERNAME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME") & " - " & rec("ABILITATO")
            operatoreCorrente.idElementoLista = rec("ID").Value
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaDisponibili.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSelezionati = New Collection
'    If tipoModalitàConfigurazione = TMC_TRANSAZIONALE Then
''        sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
''        sql = sql & " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO"
''        sql = sql & " WHERE O.IDOPERATORE = OP.IDOPERATORE"
''        sql = sql & " AND OP.IDPRODOTTO = " & idProdottoSelezionato
''        sql = sql & " AND O.ABILITATO = " & VB_VERO
''        sql = sql & " AND O.IDOPERATORE = OO.IDOPERATORE"
''        sql = sql & " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
''        sql = sql & " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA"
''        sql = sql & " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")"
''        sql = sql & " OR O.IDPUNTOVENDITA IS NULL)"
''        sql = sql & " AND (OP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
''        sql = sql & " OR OP.IDTIPOSTATORECORD IS NULL)"
''        sql = sql & " ORDER BY USERNAME"
'    Else
        sql = " SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME,"
        sql = sql & " DECODE(O.ABILITATO, 1, 'SI', 'NO') ABILITATO"
        sql = sql & " FROM OPERATORE O, OPERATORE_OFFERTAPACCHETTO OO "
        sql = sql & " WHERE O.IDOPERATORE = OO.IDOPERATORE"
        sql = sql & " AND OO.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ORDER BY O.USERNAME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME") & " - " & rec("ABILITATO")
            operatoreCorrente.idElementoLista = rec("ID").Value
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaSelezionati.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstSelezionati_Init
        
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchettoSelezionata)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitàFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazionePacchettoTariffe.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazionePacchettoTariffe.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
'        isConfigurabile = True
'        If tipoStatoProdotto = TSP_ATTIVO Then
'            Call frmMessaggio.Visualizza("ConfermaEditabilitàProdottoAttivo")
'            If frmMessaggio.exitCode <> EC_CONFERMA Then
'                isConfigurabile = False
'            End If
'        End If
    If isOffertaPacchettoBloccataDaUtente Then
'        If isConfigurabile Then
'            If tipoModalitàConfigurazione = TMC_TRANSAZIONALE Then
'                Call SetGestioneExitCode(EC_CONFERMA)
'                Call AggiornaAbilitazioneControlli
'                Call InserisciNellaBaseDati
'                Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_OPERATORE, stringaNota, idOffertaPacchettoSelezionata)
'                Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'                Call AggiornaAbilitazioneControlli
'            Else
                Call SetGestioneExitCode(EC_CONFERMA)
                Call AggiornaAbilitazioneControlli
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_OPERATORE, stringaNota, idOffertaPacchettoSelezionata)
                Call SetGestioneExitCode(EC_NON_SPECIFICATO)
                Call AggiornaAbilitazioneControlli
'            End If
'        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitàCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
    Call AzionePercorsoGuidato(TNCP_FINE)
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear
    
    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each operatore In listaDisponibili
            lstDisponibili.AddItem operatore.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear
    
    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each operatore In listaSelezionati
            lstSelezionati.AddItem operatore.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SvuotaDisponibili()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaDisponibili
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaSelezionati.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaSelezionati()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaSelezionati
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaDisponibili.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idOperatore = lstSelezionati.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaSelezionati.Item(chiaveOperatore)
            Call listaDisponibili.Add(operatore, chiaveOperatore)
            Call listaSelezionati.Remove(chiaveOperatore)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idOperatore = lstDisponibili.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaDisponibili.Item(chiaveOperatore)
            Call listaSelezionati.Add(operatore, chiaveOperatore)
            Call listaDisponibili.Remove(chiaveOperatore)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
End Sub

Private Sub cmdVerificaOperatività_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call VerificaOperatività
    
    MousePointer = mousePointerOld
End Sub

Private Sub VerificaOperatività()
    Dim elencoNonOperatività As String
    
    Set listaOperatoriNonOperativi = New Collection
    If OperativitàSuOffertaPacchettoOK(idOffertaPacchettoSelezionata, listaSelezionati, listaOperatoriNonOperativi) Then
        Call frmMessaggio.Visualizza("NotificaOperativitàOperatoriOK")
    Else
        elencoNonOperatività = ArgomentoMessaggio(listaOperatoriNonOperativi)
        Call frmMessaggio.Visualizza("AvvertimentoOperativitàOperatoriKO ", elencoNonOperatività)
    End If
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim operatore As clsElementoLista
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    If Not (listaSelezionati Is Nothing) Then
'        If tipoModalitàConfigurazione = TMC_TRANSAZIONALE Then
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_OFFERTAPACCHETTO", "IDOFFERTAPACCHETTO", idOffertaPacchettoSelezionata, TSR_ELIMINATO)
'        Else
            sql = "DELETE FROM OPERATORE_OFFERTAPACCHETTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
            SETAConnection.Execute sql, n, adCmdText
'        End If
        
        For Each operatore In listaSelezionati
'            If tipoModalitàConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "INSERT INTO OPERATORE_OFFERTAPACCHETTO"
'                sql = sql & " (IDOFFERTAPACCHETTO, IDOPERATORE)"
'                sql = sql & " SELECT "
'                sql = sql & idOffertaPacchettoSelezionata & " IDOFFERTAPACCHETTO, "
'                sql = sql & operatore.idElementoLista & " IDOPERATORE"
'                sql = sql & " FROM DUAL"
'                sql = sql & " MINUS"
'                sql = sql & " SELECT IDOFFERTAPACCHETTO, IDOPERATORE"
'                sql = sql & " FROM OPERATORE_OFFERTAPACCHETTO"
'                sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'                sql = sql & " AND IDOPERATORE = " & operatore.idElementoLista
'                SETAConnection.Execute sql, n, adCmdText
'                condizioneSql = " AND IDOPERATORE = " & operatore.idElementoLista
'                Call AggiornaParametriSessioneSuRecord("OPERATORE_OFFERTAPACCHETTO", "IDOFFERTAPACCHETTO", idOffertaPacchettoSelezionata, TSR_NUOVO, condizioneSql)
'            Else
                sql = "INSERT INTO OPERATORE_OFFERTAPACCHETTO (IDOPERATORE, IDOFFERTAPACCHETTO)"
                sql = sql & " VALUES (" & operatore.idElementoLista & ", "
                sql = sql & idOffertaPacchettoSelezionata & ")"
                SETAConnection.Execute sql, n, adCmdText
'            End If
        Next operatore
    End If
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

