VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazionePiantaArea 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkVirtuale 
      Caption         =   "Virtuale"
      Height          =   375
      Left            =   4680
      TabIndex        =   37
      Top             =   7080
      Width           =   2535
   End
   Begin VB.TextBox txtDescrizionePOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4680
      MaxLength       =   12
      TabIndex        =   10
      Top             =   6540
      Width           =   2475
   End
   Begin VB.Frame fraAzioniSuGrigliaPosti 
      Caption         =   "Griglia posti "
      Height          =   915
      Left            =   6180
      TabIndex        =   35
      Top             =   4620
      Width           =   2775
      Begin VB.CommandButton cmdEditaGrigliaPosti 
         Caption         =   "Edita"
         Height          =   435
         Left            =   180
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdClonaGrigliaPosti 
         Caption         =   "Clona"
         Height          =   435
         Left            =   1440
         TabIndex        =   16
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizioneStampaIngresso 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7800
      MaxLength       =   10
      TabIndex        =   7
      Top             =   5880
      Width           =   1215
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   31
      Top             =   240
      Width           =   3255
   End
   Begin VB.TextBox txtDescrizioneAlternativa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4320
      MaxLength       =   30
      TabIndex        =   6
      Top             =   5880
      Width           =   3315
   End
   Begin VB.TextBox txtIndicePreferibilita 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9600
      MaxLength       =   4
      TabIndex        =   8
      Top             =   5880
      Width           =   615
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3600
      MaxLength       =   3
      TabIndex        =   5
      Top             =   5880
      Width           =   555
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   6540
      Width           =   4395
   End
   Begin VB.Frame fraAzioniSuGrigliaDati 
      Height          =   915
      Left            =   120
      TabIndex        =   23
      Top             =   4620
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   5880
      Width           =   3315
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10680
      TabIndex        =   19
      Top             =   8040
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePiantaArea 
      Height          =   330
      Left            =   6240
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePiantaArea 
      Height          =   3675
      Left            =   120
      TabIndex        =   21
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6482
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraArea 
      Caption         =   "Tipo Area"
      Height          =   1095
      Left            =   7500
      TabIndex        =   11
      Top             =   6480
      Width           =   2115
      Begin VB.TextBox txtCapienza 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1140
         MaxLength       =   6
         TabIndex        =   14
         Top             =   720
         Width           =   855
      End
      Begin VB.OptionButton optNonNumerata 
         Caption         =   "Non Numerata"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   480
         Width           =   1395
      End
      Begin VB.OptionButton optNumerata 
         Caption         =   "Numerata"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   1395
      End
      Begin VB.Label lblCapienza 
         Alignment       =   1  'Right Justify
         Caption         =   "Capienza"
         Height          =   255
         Left            =   300
         TabIndex        =   34
         Top             =   780
         Width           =   735
      End
   End
   Begin VB.Label lblDescrizionePOS 
      Caption         =   "Descrizione POS"
      Height          =   255
      Left            =   4680
      TabIndex        =   36
      Top             =   6300
      Width           =   1935
   End
   Begin VB.Label lblDescrizioneStampaIngresso 
      Caption         =   "Descr. stampa ingresso"
      Height          =   255
      Left            =   7800
      TabIndex        =   33
      Top             =   5640
      Width           =   1695
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   32
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIndicePreferibilita 
      Caption         =   "Indice di preferibilitā"
      Height          =   255
      Left            =   9600
      TabIndex        =   30
      Top             =   5640
      Width           =   1575
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   3600
      TabIndex        =   29
      Top             =   5640
      Width           =   555
   End
   Begin VB.Label lblDescrizioneAlternativa 
      Caption         =   "Descrizione alternativa"
      Height          =   255
      Left            =   4320
      TabIndex        =   28
      Top             =   5640
      Width           =   1935
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   27
      Top             =   4380
      Width           =   2775
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   6300
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   5640
      Width           =   1695
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   4380
      Width           =   1815
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Aree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazionePiantaArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idPiantaSelezionata As Long
Private descrizioneRecordSelezionato As String
Private descrizioneAlternativaRecordSelezionato As String
Private descrizionePOS As String
Private codiceRecordSelezionato As String
Private indicePreferibilita As Integer
Private descrizioneStampaIngresso As String
Private nomeRecordSelezionato As String
Private nomePiantaSelezionata As String
Private capienzaRecordSelezionato As Long
Private esisteGrigliaPostiAssociata As Boolean
'Private listaCampiValoriUnici As Collection
Private numeroVersioneArea As Long
Private isAttributiAreaModificati As Boolean
Private isAssociabileGrigliaClonata As Boolean
Private virtuale As ValoreBooleanoEnum

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private tipoArea As TipoAreaEnum
Private tipoGriglia As TipoGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pianta"
    txtInfo1.Text = nomePiantaSelezionata
    txtInfo1.Enabled = False
    fraAzioniSuGrigliaPosti.Visible = (tipoGriglia = TG_GRANDI_IMPIANTI And _
                               tipoArea = TA_AREA_NUMERATA)
    
    dgrConfigurazionePiantaArea.Caption = "AREE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaArea.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        txtIndicePreferibilita.Text = ""
        txtDescrizioneStampaIngresso.Text = ""
        txtCapienza.Text = ""
        optNumerata.Value = False
        optNonNumerata.Value = False
        
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        txtIndicePreferibilita.Enabled = False
        txtDescrizioneStampaIngresso.Enabled = False
        txtCapienza.Enabled = False
        fraArea.Enabled = False
        optNumerata.Enabled = False
        optNonNumerata.Enabled = False
        
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblCapienza.Enabled = False
        lblIndicePreferibilita.Enabled = False
        lblDescrizioneStampaIngresso.Enabled = False
        
        chkVirtuale.Enabled = False
        chkVirtuale.Value = False
        
        cmdEditaGrigliaPosti.Enabled = False
        cmdClonaGrigliaPosti.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaArea.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtIndicePreferibilita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneStampaIngresso.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        fraArea.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        optNumerata.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or _
                               gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE)
        optNonNumerata.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or _
                               gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE)
        txtCapienza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
                             optNonNumerata.Value = True)
        If (gestioneRecordGriglia <> ASG_ELIMINA And optNumerata.Value = True) Then
            txtCapienza.Text = ""
        End If
        lblCapienza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
                             optNonNumerata.Value = True)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblIndicePreferibilita.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneStampaIngresso.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdEditaGrigliaPosti.Enabled = (gestioneRecordGriglia = ASG_MODIFICA)
        cmdClonaGrigliaPosti.Enabled = (gestioneRecordGriglia = ASG_MODIFICA And _
            isAssociabileGrigliaClonata)
        chkVirtuale.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                               Trim(txtCodice.Text) <> "" And _
                               Trim(txtIndicePreferibilita.Text) <> "" And _
                               Trim(txtDescrizioneAlternativa.Text) <> "" And _
                               (optNumerata.Value = True Or (optNonNumerata.Value = True And _
                                                            Trim(txtCapienza.Text) <> "")))
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaArea.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        txtIndicePreferibilita.Text = ""
        txtDescrizioneStampaIngresso.Text = ""
        optNumerata.Value = False
        optNonNumerata.Value = False
        txtCapienza.Text = ""
        
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        txtIndicePreferibilita.Enabled = False
        txtDescrizioneStampaIngresso.Enabled = False
        fraArea.Enabled = False
        optNumerata.Enabled = False
        optNonNumerata.Enabled = False
        txtCapienza.Enabled = False
        lblCapienza.Enabled = False
        
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblIndicePreferibilita.Enabled = False
        lblDescrizioneStampaIngresso.Enabled = False
        
        chkVirtuale.Enabled = False
        
        cmdEditaGrigliaPosti.Enabled = False
        cmdClonaGrigliaPosti.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    End If
    
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetNomePiantaSelezionata(nome As String)
    nomePiantaSelezionata = nome
End Sub

Private Sub chkVirtuale_Click()
    If Not internalEvent Then
        Call chkVirtuale_Update
    End If
End Sub

Private Sub chkVirtuale_Update()
    virtuale = chkVirtuale.Value
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    If isAttributiAreaModificati Then
        numeroVersioneArea = numeroVersioneArea + 1
        Call AggiornaVersioneInBaseDati
    End If
    Call AggiornaAbilitazioneControlli
    Call SbloccaDominioPerUtente(CCDA_AREA, idRecordSelezionato, isAreaBloccataDaUtente)
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()

    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    numeroVersioneArea = numeroVersioneArea + 1
    If ValoriCampiOK Then
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA, CCDA_AREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazionePiantaArea_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazionePiantaArea_Init
            Case ASG_INSERISCI_DA_SELEZIONE
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA, CCDA_AREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazionePiantaArea_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazionePiantaArea_Init
            Case ASG_MODIFICA
                If isAreaBloccataDaUtente Then
                    Call AggiornaNellaBaseDati
                    Call SbloccaDominioPerUtente(CCDA_AREA, idRecordSelezionato, isAreaBloccataDaUtente)
                    Call ScriviLog(CCTA_MODIFICA, CCDA_PIANTA, CCDA_AREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazionePiantaArea_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazionePiantaArea_Init
                Else
                    Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", "l'area č in lavorazione da parte di un altro utente")
                End If
            Case ASG_ELIMINA
                If isAreaBloccataDaUtente Then
                    Call EliminaDallaBaseDati
                    Call SbloccaDominioPerUtente(CCDA_AREA, idRecordSelezionato, isAreaBloccataDaUtente)
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PIANTA, CCDA_AREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazionePiantaArea_Init
                    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                    Call dgrConfigurazionePiantaArea_Init
                Else
                    Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", "l'area č in lavorazione da parte di un altro utente")
                End If
        End Select
        Call frmInizialePianta.SetIsAttributiPiantaModificati(True)
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call BloccaDominioPerUtente(CCDA_AREA, idRecordSelezionato, isAreaBloccataDaUtente)
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    If isAttributiAreaModificati Then
        Call AggiornaVersioneInBaseDati
        Call frmInizialePianta.SetIsAttributiPiantaModificati(True)
    End If
    Call SbloccaDominioPerUtente(CCDA_AREA, idRecordSelezionato, isAreaBloccataDaUtente)
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    isAreaBloccataDaUtente = True
    isAssociabileGrigliaClonata = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    numeroVersioneArea = 0
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    isAreaBloccataDaUtente = True
    numeroVersioneArea = 0
    isAssociabileGrigliaClonata = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePiantaArea.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim rec As New ADODB.Recordset
    
    Set rec = adcConfigurazionePiantaArea.Recordset
    isAssociabileGrigliaClonata = (rec("POSTI").Value = 0)
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call BloccaDominioPerUtente(CCDA_AREA, idRecordSelezionato, isAreaBloccataDaUtente)
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazionePiantaArea_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    isAttributiAreaModificati = False
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazionePiantaArea_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePiantaArea_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePiantaArea.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePiantaArea_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazionePiantaArea
    
    sql = "SELECT A1.IDAREA ID, A1.NOME ""Nome"", A1.CODICE ""Codice"","
    sql = sql & " A1.DESCRIZIONE ""Descrizione"", A1.IDTIPOAREA, TA.NOME ""Tipo Area"","
    sql = sql & " COUNT(IDPOSTO) POSTI,"
    sql = sql & " COUNT(IDPOSTO)||' / -' AS ""Capienza (Posti/Ingr.)"", A2.CODICE || ' - ' || A2.NOME AS ""Superarea"","
    sql = sql & " A1.INDICEDIPREFERIBILITA AS ""Indice pref."""
    sql = sql & " FROM AREA A1, AREA A2, TIPOAREA TA, POSTO P"
    sql = sql & " WHERE (A1.IDAREA = P.IDAREA(+)) AND (A1.IDTIPOAREA = TA.IDTIPOAREA)"
    sql = sql & " AND A1.IDAREA_PADRE = A2.IDAREA(+)"
    sql = sql & " AND (A1.IDTIPOAREA = " & TA_AREA_NUMERATA & ") AND (A1.IDPIANTA = " & idPiantaSelezionata & ")"
    sql = sql & " GROUP BY A2.CODICE,A2.NOME,A1.IDAREA,A1.NOME,A1.CODICE,A1.DESCRIZIONE,A1.IDTIPOAREA,TA.NOME,A1.INDICEDIPREFERIBILITA"
    sql = sql & " UNION"
    sql = sql & " SELECT A1.IDAREA ID, A1.NOME ""Nome"", A1.CODICE ""Codice"","
    sql = sql & " A1.DESCRIZIONE ""Descrizione"", A1.IDTIPOAREA, TA.NOME ""Tipo Area"","
    sql = sql & " 0 POSTI,"
    sql = sql & " '- / '||A1.CAPIENZA AS ""(Capienza (Posti/Ingr.)"", A2.CODICE || ' - ' || A2.NOME AS ""Superarea"","
    sql = sql & " A1.INDICEDIPREFERIBILITA AS ""Indice pref."""
    sql = sql & " FROM AREA A1, AREA A2, TIPOAREA TA"
    sql = sql & " WHERE (A1.IDTIPOAREA = TA.IDTIPOAREA)"
    sql = sql & " AND A1.IDAREA_PADRE = A2.IDAREA(+)"
    sql = sql & " AND (A1.IDTIPOAREA = " & TA_AREA_NON_NUMERATA & ") AND (A1.IDPIANTA = " & idPiantaSelezionata & ")"
    sql = sql & " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazionePiantaArea.dataSource = d
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim idNuovaArea As Long
    Dim varco As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
'   INSERIMENTO IN TABELLA AREA
    idNuovaArea = OttieniIdentificatoreDaSequenza("SQ_AREA")
    sql = "INSERT INTO AREA (IDAREA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
    sql = sql & " CODICE, INDICEDIPREFERIBILITA, CAPIENZA, DESCRIZIONESTAMPAINGRESSO,"
    sql = sql & " IDPIANTA, IDTIPOAREA, IDORDINEDIPOSTOSIAE, IDAREA_PADRE, NUMEROVERSIONE, VIRTUALE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaArea & ", "
    sql = sql & SqlStringValue(nomeRecordSelezionato) & ", "
    sql = sql & SqlStringValue(descrizioneRecordSelezionato) & ", "
    sql = sql & SqlStringValue(descrizioneAlternativaRecordSelezionato) & ", "
    sql = sql & SqlStringValue(descrizionePOS) & ", "
    sql = sql & SqlStringValue(codiceRecordSelezionato) & ", "
    sql = sql & indicePreferibilita & ", "
    sql = sql & IIf(capienzaRecordSelezionato = valoreLongNullo, "NULL, ", capienzaRecordSelezionato & ", ")
    sql = sql & SqlStringValue(descrizioneStampaIngresso) & ", "
    sql = sql & idPiantaSelezionata & ", "
    sql = sql & tipoArea & ", "
    sql = sql & "NULL, "
    sql = sql & "NULL, "
    sql = sql & numeroVersioneArea & ", "
    sql = sql & IIf(virtuale, "1", "0") & ")"
    n = ORADB.ExecuteSQL(sql)
    
    Call SetIdRecordSelezionato(idNuovaArea)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori

    sql = "SELECT A.NOME, A.DESCRIZIONE, A.DESCRIZIONEALTERNATIVA, A.CAPIENZA,"
    sql = sql & " A.INDICEDIPREFERIBILITA, A.CODICE, A.IDTIPOAREA, A.NUMEROVERSIONE,"
    sql = sql & " A.DESCRIZIONESTAMPAINGRESSO, G.IDAREA IDA, A.DESCRIZIONEPOS, VIRTUALE"
    sql = sql & " FROM AREA A, GRIGLIAPOSTI G"
    sql = sql & " WHERE G.IDAREA(+) = A.IDAREA"
    sql = sql & " AND A.IDAREA = " & idRecordSelezionato
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        descrizioneAlternativaRecordSelezionato = rec("DESCRIZIONEALTERNATIVA")
        descrizionePOS = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        capienzaRecordSelezionato = IIf(IsNull(rec("CAPIENZA")), valoreLongNullo, rec("CAPIENZA"))
        indicePreferibilita = IIf(IsNull(rec("INDICEDIPREFERIBILITA")), valoreIntegerNullo, rec("INDICEDIPREFERIBILITA"))
        codiceRecordSelezionato = rec("CODICE")
        tipoArea = IIf(IsNull(rec("IDTIPOAREA")), "", rec("IDTIPOAREA"))
        esisteGrigliaPostiAssociata = IIf(IsNull(rec("IDA")), False, True)
        numeroVersioneArea = rec("NUMEROVERSIONE")
        descrizioneStampaIngresso = IIf(IsNull(rec("DESCRIZIONESTAMPAINGRESSO")), "", rec("DESCRIZIONESTAMPAINGRESSO"))
        virtuale = IIf(rec("VIRTUALE") = 1, VB_VERO, VB_FALSO)
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDescrizioneAlternativa.Text = ""
    txtDescrizionePOS.Text = ""
    txtCodice.Text = ""
    txtIndicePreferibilita.Text = ""
    txtCapienza.Text = ""
    txtIndicePreferibilita.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizioneAlternativa.Text = descrizioneAlternativaRecordSelezionato
    txtDescrizionePOS.Text = descrizionePOS
    txtDescrizioneStampaIngresso.Text = descrizioneStampaIngresso
    txtCodice.Text = codiceRecordSelezionato
    txtIndicePreferibilita.Text = IIf(indicePreferibilita = valoreIntegerNullo, "", indicePreferibilita)
    txtCapienza.Text = IIf(capienzaRecordSelezionato = valoreLongNullo, "", capienzaRecordSelezionato)
    Select Case tipoArea
    Case TA_AREA_NUMERATA
        optNumerata.Value = True
    Case TA_AREA_NON_NUMERATA
        optNonNumerata.Value = True
    Case Else
        'Do Nothing
    End Select
    chkVirtuale.Value = IIf(virtuale = VB_VERO, 1, 0)
    
    internalEvent = internalEventOld

End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDAREA <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrizionePOS = Trim(txtDescrizionePOS.Text)
    descrizioneAlternativaRecordSelezionato = Trim(txtDescrizioneAlternativa.Text)
    codiceRecordSelezionato = Trim(txtCodice.Text)
    If ViolataUnicitā("AREA", "CODICE = " & SqlStringValue(codiceRecordSelezionato), "IDPIANTA = " & idPiantaSelezionata, _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice = " & SqlStringValue(codiceRecordSelezionato) & _
            " č giā presente in DB per la stessa pianta;")
    End If
    descrizioneStampaIngresso = Trim(txtDescrizioneStampaIngresso.Text)
    
    If IsCampoInteroCorretto(txtIndicePreferibilita) Then
        indicePreferibilita = CInt(Trim(txtIndicePreferibilita.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo indice di preferibilitā deve essere numerico di tipo intero;")
    End If
    
    If txtCapienza.Text = "" Then
        capienzaRecordSelezionato = valoreLongNullo
    Else
        If IsCampoInteroCorretto(txtCapienza) Then
            capienzaRecordSelezionato = CLng(Trim(txtCapienza.Text))
        Else
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore immesso sul campo capienza deve essere numerico di tipo intero;")
        End If
    End If
    
    If optNumerata.Value = True Then
        tipoArea = TA_AREA_NUMERATA
    ElseIf optNonNumerata.Value = True Then
        tipoArea = TA_AREA_NON_NUMERATA
    Else
        tipoArea = TA_SUPERAREA
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim varco As clsElementoLista

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "UPDATE AREA SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & ","
    sql = sql & " DESCRIZIONEALTERNATIVA = " & SqlStringValue(descrizioneAlternativaRecordSelezionato) & ","
    sql = sql & " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOS) & ","
    sql = sql & " INDICEDIPREFERIBILITA = " & indicePreferibilita & ","
    sql = sql & " CODICE = " & SqlStringValue(codiceRecordSelezionato) & ","
    sql = sql & " CAPIENZA = " & IIf(capienzaRecordSelezionato = valoreLongNullo, "NULL", capienzaRecordSelezionato) & ","
    sql = sql & " IDPIANTA = " & idPiantaSelezionata & ","
    sql = sql & " IDTIPOAREA = " & tipoArea & ","
    sql = sql & " NUMEROVERSIONE = " & numeroVersioneArea & ","
    sql = sql & " DESCRIZIONESTAMPAINGRESSO = " & SqlStringValue(descrizioneStampaIngresso) & ","
    sql = sql & " VIRTUALE = " & IIf(virtuale = VB_VERO, "1", "0")
    sql = sql & " WHERE IDAREA = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim areaEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim numeroPostiCancellati As Long
'    Dim mousePointerOld As Integer
    Dim n As Long
    
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
    Call ApriConnessioneBD_ORA
    
    Set listaTabelleCorrelate = New Collection

    areaEliminabile = True
    If Not IsRecordEliminabile("IDAREA", "FASCIAPOSTI", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("FASCIAPOSTI")
        areaEliminabile = False
    End If
    
    If areaEliminabile Then
    
'        On Error GoTo gestioneErrori
        
'        SETAConnection.BeginTrans
        Call ORADB.BeginTrans
    
        sql = "DELETE FROM GRIGLIAPOSTI WHERE IDAREA = " & idRecordSelezionato
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
'        numeroPostiCancellati = CancellaRecord(SETAConnection, "POSTO", "IDAREA", idRecordSelezionato, "IDPOSTO")
        numeroPostiCancellati = CancellaRecord(ORADB, "POSTO", "IDAREA", idRecordSelezionato, "IDPOSTO")
        sql = "DELETE FROM CC_AREAUTILIZZATA WHERE IDAREA = " & idRecordSelezionato
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
        sql = "DELETE FROM AREA WHERE IDAREA = " & idRecordSelezionato
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
        
'        SETAConnection.CommitTrans
        Call ORADB.CommitTrans
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "L'area selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD_ORA
'    MousePointer = mousePointerOld
'    Exit Sub
'
'gestioneErrori:
''    SETAConnection.RollbackTrans
'    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazionePiantaArea_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePiantaArea
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 7
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Visible = False
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
    g.Columns(8).Width = (dimensioneGrid / numeroCampi)
    g.Columns(9).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub optNumerata_Click()
    If Not internalEvent Then
        tipoArea = TA_AREA_NUMERATA
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub optNonNumerata_Click()
    If Not internalEvent Then
        tipoArea = TA_AREA_NON_NUMERATA
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtCapienza_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizioneAlternativa_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call ValorizzaCodiceAutomaticamente
        Call ValorizzaDecsrizioneAlternativaAutomaticamente
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub ValorizzaCodiceAutomaticamente()
    txtCodice.Text = Left$(txtNome.Text, 3)
End Sub

Private Sub ValorizzaDecsrizioneAlternativaAutomaticamente()
    txtDescrizioneAlternativa.Text = Left$(txtNome.Text, 30)
End Sub

Private Sub txtCodice_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtIndicePreferibilita_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdEditaGrigliaPosti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CaricaFormInizialePiantaGrigliaPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaFormInizialePiantaGrigliaPosti()
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    Else
        Call frmInizialePiantaGrigliaPosti.SetTipoGriglia(TG_GRANDI_IMPIANTI)
        Call frmInizialePiantaGrigliaPosti.SetIdPiantaSelezionata(idPiantaSelezionata)
        Call frmInizialePiantaGrigliaPosti.SetIdAreaSelezionata(idRecordSelezionato)
        Call frmInizialePiantaGrigliaPosti.SetNomeAreaSelezionata(nomeRecordSelezionato)
        Call frmInizialePiantaGrigliaPosti.SetEsisteGrigliaPostiAssociata(esisteGrigliaPostiAssociata)
        Call frmInizialePiantaGrigliaPosti.Init
        If frmInizialePiantaGrigliaPosti.GetGestioneExitCode = EC_CONFERMA Then
            isAttributiAreaModificati = True
            Call Conferma
        End If
    End If
End Sub

Private Sub cmdClonaGrigliaPosti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ClonaGrigliaPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub ClonaGrigliaPosti()
    Dim idAreaMaster As Long
    Dim clonaFasceSequenze As Boolean
    
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    Else
'        If Not esisteGrigliaPostiAssociata Then
        Call CaricaFormDettagliGrigliePostiClonabili
        If frmDettagliGriglieClonabili.GetExitCode = EC_CONFERMA Then
            idAreaMaster = frmDettagliGriglieClonabili.GetIdAreaSelezionata
            clonaFasceSequenze = frmDettagliGriglieClonabili.GetClonaFasceSequenze
            If Not IsClonazioneGrigliaPostiOK(idRecordSelezionato, idAreaMaster, clonaFasceSequenze) Then
                Call frmMessaggio.Visualizza("ErroreClonazione")
            Else
                Call Conferma
            End If
        End If
'        Else
'            Call frmMessaggio.Visualizza("NotificaGrigliaPostiGiaEsistente")
'        End If
    End If
End Sub

Private Sub CaricaFormDettagliGrigliePostiClonabili()
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    Else
        Call frmDettagliGriglieClonabili.Init(idPiantaSelezionata)
        isAttributiAreaModificati = True
    End If
End Sub

Public Sub SetTipoGriglia(tipoG As TipoGrigliaEnum)
    tipoGriglia = tipoG
End Sub

Private Sub AggiornaVersioneInBaseDati_old()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    sql = "SELECT NUMEROVERSIONE FROM AREA WHERE IDAREA = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        rec("NUMEROVERSIONE") = numeroVersioneArea
    End If
    rec.Update
    rec.Close
    
    isAttributiAreaModificati = False
End Sub

Private Sub AggiornaVersioneInBaseDati()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql = "UPDATE AREA"
    sql = sql & " SET NUMEROVERSIONE = " & numeroVersioneArea
    sql = sql & " WHERE IDAREA = " & idRecordSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    isAttributiAreaModificati = False
End Sub

Public Sub SetIsAttributiAreaModificati(isAttrMod As Boolean)
    isAttributiAreaModificati = isAttrMod
End Sub
