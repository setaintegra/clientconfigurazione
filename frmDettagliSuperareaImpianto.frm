VERSION 5.00
Begin VB.Form frmDettagliSuperareaImpianto 
   Caption         =   "Colori"
   ClientHeight    =   8715
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   3225
   LinkTopic       =   "Form1"
   ScaleHeight     =   8715
   ScaleWidth      =   3225
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Colori disponibili"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7935
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   3015
      Begin VB.OptionButton optColore 
         Caption         =   "colore"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Visible         =   0   'False
         Width           =   2655
      End
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   8160
      Width           =   1275
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   435
      Left            =   1920
      TabIndex        =   0
      Top             =   8160
      Width           =   1155
   End
End
Attribute VB_Name = "frmDettagliSuperareaImpianto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idMappaImpianto As Long
Private collNomiSuperareeImpianto As Collection
Private stringaNomiSuperareeImpianto As String
Private coloreSelezionato As String

Public Sub Init()
    Dim a As classeArea
    Dim i As Long
    
    For i = 1 To NUMERO_COLORI
        Load optColore(i)
        optColore(i).Caption = colori(i, 1)
        optColore(i).Left = 50
        optColore(i).tOp = 300 * i
        optColore(i).Visible = True
        optColore(i).BackColor = CalcolaRGB(i, "")
    Next i
    
    stringaNomiSuperareeImpianto = ""
    For Each a In collNomiSuperareeImpianto
        stringaNomiSuperareeImpianto = stringaNomiSuperareeImpianto & "'" & a.nomeArea & "',"
    Next a
    stringaNomiSuperareeImpianto = stringaNomiSuperareeImpianto & "''"

    Call Me.Show(vbModal)
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call conferma
End Sub

Private Sub conferma()
    Dim sql As String
    Dim n As Long
    
    Call ORADB.BeginTrans

    sql = "UPDATE SUPERAREAIMPIANTO SET RGBCOLORE = '" & coloreSelezionato & "'" & _
            " WHERE IDMAPPAIMPIANTO = " & idMappaImpianto & _
            " AND NOME IN (" & stringaNomiSuperareeImpianto & ")"
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    Unload Me
End Sub

Public Sub SetIdMappaImpianto(idMI As Long)
    idMappaImpianto = idMI
End Sub

Public Sub SetCollNomiSuperareaImpianto(c As Collection)
    Set collNomiSuperareeImpianto = c
End Sub

Private Sub optColore_Click(Index As Integer)
    coloreSelezionato = optColore(Index).Caption
End Sub
