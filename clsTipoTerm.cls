VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTipoTerm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idTipoTerminale As Long
Public nomeTipoTerminale As String
Public collTipiOperazioneDisponibili As New Collection
Public collTipiOperazioneSelezionate As New Collection

Public Sub inizializza(idP As Long)  'lista degli iDTipoTerminale
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sql2 As String
    Dim rec2 As New ADODB.Recordset
    Dim i As Integer
    Dim numeroFasce As Integer
    Dim numeroTotaleSequenze As Integer
    
    Dim TipoOperazione As clsTipoOperaz
    
On Error GoTo gestioneErrori
    
'  TIPI OPERAZIONE DISPONIBILI
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT T.IDTIPOOPERAZIONE IDTIPOOPERAZIONE, T.NOME NOME"
'        sql = sql & " FROM TIPOOPERAZIONE T, PRODOTTO_TIPOTERMIN_TIPOOPERAZ PTT"
'        sql = sql & " WHERE T.IDTIPOOPERAZIONE = PTT.IDTIPOOPERAZIONE(+)"
'        sql = sql & " AND (PTT.IDTIPOOPERAZIONE IS NULL OR PTT.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")"
'        sql = sql & " AND PTT.IDPRODOTTO(+) = " & idP
'        sql = sql & " AND PTT.IDTIPOTERMINALE(+) = " & idTipoTerminale
'        sql = sql & " AND T.IDTIPOOPERAZIONE > 0 "
'        sql = sql & " ORDER BY NOME"
'    Else
        sql = "SELECT T.IDTIPOOPERAZIONE IDTIPOOPERAZIONE, T.NOME NOME"
        sql = sql & " FROM TIPOOPERAZIONE T, PRODOTTO_TIPOTERMIN_TIPOOPERAZ PTT"
        sql = sql & " WHERE T.IDTIPOOPERAZIONE = PTT.IDTIPOOPERAZIONE(+)"
        sql = sql & " AND PTT.IDTIPOOPERAZIONE IS NULL"
        sql = sql & " AND PTT.IDPRODOTTO(+) = " & idP
        sql = sql & " AND PTT.IDTIPOTERMINALE(+) = " & idTipoTerminale
        sql = sql & " AND T.IDTIPOOPERAZIONE > 0 "
        sql = sql & " ORDER BY NOME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set TipoOperazione = New clsTipoOperaz
            TipoOperazione.idTipoOperazione = rec("IDTIPOOPERAZIONE")
            TipoOperazione.nomeTipoOperazione = rec("NOME")
            TipoOperazione.descrizioneTipoOperazione = rec("NOME")
            TipoOperazione.dataOraInizio = dataNulla
            TipoOperazione.dataOraFine = dataNulla
            Call collTipiOperazioneDisponibili.Add(TipoOperazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
'  TIPI OPERAZIONE SELEZIONATI
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT T.IDTIPOOPERAZIONE IDTIPOOPERAZIONE, T.NOME NOME,"
'        sql = sql & " PTT.DATAORAINIZIOVALIDITA DATAORAINIZIOVALIDITA,"
'        sql = sql & " PTT.DATAORAFINEVALIDITA DATAORAFINEVALIDITA"
'        sql = sql & " FROM TIPOOPERAZIONE T, PRODOTTO_TIPOTERMIN_TIPOOPERAZ PTT"
'        sql = sql & " WHERE T.IDTIPOOPERAZIONE = PTT.IDTIPOOPERAZIONE"
'        sql = sql & " AND PTT.IDTIPOTERMINALE = " & idTipoTerminale
'        sql = sql & " AND PTT.IDPRODOTTO = " & idP
'        sql = sql & " AND (PTT.IDTIPOSTATORECORD IS NULL"
'        sql = sql & " OR PTT.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " ORDER BY NOME"
'    Else
        sql = "SELECT T.IDTIPOOPERAZIONE IDTIPOOPERAZIONE, T.NOME NOME,"
        sql = sql & " PTT.DATAORAINIZIOVALIDITA DATAORAINIZIOVALIDITA,"
        sql = sql & " PTT.DATAORAFINEVALIDITA DATAORAFINEVALIDITA"
        sql = sql & " FROM TIPOOPERAZIONE T, PRODOTTO_TIPOTERMIN_TIPOOPERAZ PTT"
        sql = sql & " WHERE T.IDTIPOOPERAZIONE = PTT.IDTIPOOPERAZIONE"
        sql = sql & " AND PTT.IDTIPOTERMINALE = " & idTipoTerminale
        sql = sql & " AND PTT.IDPRODOTTO = " & idP
        sql = sql & " ORDER BY NOME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set TipoOperazione = New clsTipoOperaz
            TipoOperazione.idTipoOperazione = rec("IDTIPOOPERAZIONE")
            TipoOperazione.nomeTipoOperazione = rec("NOME")
            If Not IsNull(rec("DATAORAINIZIOVALIDITA")) And Not IsNull(rec("DATAORAFINEVALIDITA")) Then
                TipoOperazione.dataOraInizio = rec("DATAORAINIZIOVALIDITA")
                TipoOperazione.dataOraFine = rec("DATAORAFINEVALIDITA")
                TipoOperazione.descrizioneTipoOperazione = TipoOperazione.nomeTipoOperazione & _
                    " - da: " & TipoOperazione.dataOraInizio & _
                    " a: " & TipoOperazione.dataOraFine
            Else
                TipoOperazione.dataOraInizio = dataNulla
                TipoOperazione.dataOraFine = dataNulla
                TipoOperazione.descrizioneTipoOperazione = TipoOperazione.nomeTipoOperazione & _
                    " - illimitata"
            End If
            Call collTipiOperazioneSelezionate.Add(TipoOperazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
   
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub


