VERSION 5.00
Begin VB.Form frmConfigurazioneOrganizzazioneTemplate 
   Caption         =   "Organizzazione"
   ClientHeight    =   5175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11940
   LinkTopic       =   "Form1"
   ScaleHeight     =   5175
   ScaleWidth      =   11940
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   10
      Top             =   4560
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   240
      TabIndex        =   7
      Top             =   4080
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1560
      Width           =   5235
   End
   Begin VB.CommandButton cmdSpostaTuttiASinistra 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2880
      Width           =   435
   End
   Begin VB.CommandButton cmdSpostaSelezioneASinistra 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2460
      Width           =   435
   End
   Begin VB.CommandButton cmdSpostaSelezioneADestra 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2040
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   6420
      MultiSelect     =   2  'Extended
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1560
      Width           =   5235
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   1
      Top             =   360
      Width           =   3255
   End
   Begin VB.CommandButton cmdSpostaTuttiADestra 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1620
      Width           =   435
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione dei Template home ticketing all'Organizzazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   240
      Width           =   7995
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati"
      Height          =   195
      Left            =   6420
      TabIndex        =   14
      Top             =   1320
      Width           =   5235
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   120
      TabIndex        =   13
      Top             =   1320
      Width           =   5175
   End
   Begin VB.Label lblTemplate 
      Alignment       =   2  'Center
      Caption         =   "LAYOUT HOME TICKETING"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   180
      TabIndex        =   12
      Top             =   960
      Width           =   11475
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   11
      Top             =   120
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
Private internalEvent As Boolean

Private listaDisponibili As Collection
Private listaSelezionati As Collection

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveHTLayout As String
    Dim HTLayoutCorrente As clsElementoLista
    Dim s As String
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    sql = "SELECT HTL.IDLAYOUTALFRESCO, HTL.DESCRIZIONE NOME" & _
        " FROM LAYOUTALFRESCO HTL, ORGANIZZAZIONE_LAYOUTALFRESCO OHTL" & _
        " WHERE HTL.IDLAYOUTALFRESCO = OHTL.IDLAYOUTALFRESCO(+)" & _
        " AND IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata & _
        " AND OHTL.IDLAYOUTALFRESCO IS NULL" & _
        " ORDER BY NOME"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set HTLayoutCorrente = New clsElementoLista
            HTLayoutCorrente.idElementoLista = rec("IDLAYOUTALFRESCO")
            HTLayoutCorrente.nomeElementoLista = rec("NOME")
            HTLayoutCorrente.descrizioneElementoLista = ""
            chiaveHTLayout = ChiaveId(HTLayoutCorrente.idElementoLista)
            Call listaDisponibili.Add(HTLayoutCorrente, chiaveHTLayout)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD

    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveHTLayout As String
    Dim HTLayoutCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSelezionati = New Collection
    
    sql = "SELECT DISTINCT HTL.IDLAYOUTALFRESCO, HTL.DESCRIZIONE NOME" & _
        " FROM LAYOUTALFRESCO HTL, ORGANIZZAZIONE_LAYOUTALFRESCO OHTL" & _
        " WHERE HTL.IDLAYOUTALFRESCO = OHTL.IDLAYOUTALFRESCO" & _
        " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY NOME"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set HTLayoutCorrente = New clsElementoLista
            HTLayoutCorrente.idElementoLista = rec("IDLAYOUTALFRESCO")
            HTLayoutCorrente.nomeElementoLista = rec("NOME")
            HTLayoutCorrente.descrizioneElementoLista = ""
            chiaveHTLayout = ChiaveId(HTLayoutCorrente.idElementoLista)
            Call listaSelezionati.Add(HTLayoutCorrente, chiaveHTLayout)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstSelezionati_Init
        
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim HTLayout As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each HTLayout In listaDisponibili
            lstDisponibili.AddItem HTLayout.nomeElementoLista & " - " & HTLayout.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = HTLayout.idElementoLista
            i = i + 1
        Next HTLayout
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim HTLayout As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each HTLayout In listaSelezionati
            lstSelezionati.AddItem HTLayout.nomeElementoLista & " - " & HTLayout.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = HTLayout.idElementoLista
            i = i + 1
        Next HTLayout
    End If
           
    internalEvent = internalEventOld

End Sub

Public Sub CaricaListe()
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Public Sub Init()
    Call AggiornaAbilitazioneControlli
    
    Call CaricaListe
    Call Me.Show(vbModal)
End Sub

Private Sub cmdSpostaTuttiADestra_Click()
    Call SpostaTuttiADestra
End Sub

Private Sub SpostaTuttiADestra()
    Dim HTLayout As clsElementoLista
    Dim chiaveHTLayout As String
    
    For Each HTLayout In listaDisponibili
        chiaveHTLayout = ChiaveId(HTLayout.idElementoLista)
        Call listaSelezionati.Add(HTLayout, chiaveHTLayout)
    Next HTLayout
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSpostaSelezioneADestra_Click()
    Call SpostaSelezioneADestra
End Sub

Private Sub SpostaSelezioneADestra()
    Dim i As Integer
    Dim idHTLayout As Long
    Dim HTLayout As clsElementoLista
    Dim chiaveHTLayout As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idHTLayout = lstDisponibili.ItemData(i - 1)
            chiaveHTLayout = ChiaveId(idHTLayout)
            Set HTLayout = listaDisponibili.Item(chiaveHTLayout)
            Call listaSelezionati.Add(HTLayout, chiaveHTLayout)
            Call listaDisponibili.Remove(chiaveHTLayout)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSpostaSelezioneASinistra_Click()
    Call SpostaSelezioneASinistra
End Sub

Private Sub SpostaSelezioneASinistra()
    Dim i As Integer
    Dim idHTLayout As Long
    Dim HTLayout As clsElementoLista
    Dim chiaveHTLayout As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idHTLayout = lstSelezionati.ItemData(i - 1)
            chiaveHTLayout = ChiaveId(idHTLayout)
            Set HTLayout = listaSelezionati.Item(chiaveHTLayout)
            Call listaDisponibili.Add(HTLayout, chiaveHTLayout)
            Call listaSelezionati.Remove(chiaveHTLayout)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSpostaTuttiASinistra_Click()
    Call SpostaTuttiASinistra
End Sub

Private Sub SpostaTuttiASinistra()
    Dim HTLayout As clsElementoLista
    Dim chiaveHTLayout As String
    
    For Each HTLayout In listaSelezionati
        chiaveHTLayout = ChiaveId(HTLayout.idElementoLista)
        Call listaDisponibili.Add(HTLayout, chiaveHTLayout)
    Next HTLayout
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub Annulla()
    Call CaricaListe
End Sub

Private Sub Conferma()
    Dim sql As String
    Dim i As Long
    Dim numeroRecord As Long
    Dim numeroElementi As Long
    
    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    
    sql = "DELETE FROM ORGANIZZAZIONE_LAYOUTALFRESCO WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    SETAConnection.Execute sql, numeroRecord, adCmdText
    
    numeroElementi = lstSelezionati.ListCount
    For i = 1 To numeroElementi
        sql = "INSERT INTO ORGANIZZAZIONE_LAYOUTALFRESCO (IDORGANIZZAZIONE, IDLAYOUTALFRESCO)" & _
            " VALUES (" & idOrganizzazioneSelezionata & ", " & lstSelezionati.ItemData(i - 1) & ")"
        SETAConnection.Execute sql, numeroRecord, adCmdText
    Next i
        
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD

    Call CaricaListe
End Sub

