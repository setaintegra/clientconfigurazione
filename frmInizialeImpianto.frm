VERSION 5.00
Begin VB.Form frmInizialeImpianto 
   Caption         =   "Impianto"
   ClientHeight    =   7440
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12060
   LinkTopic       =   "Form1"
   ScaleHeight     =   7440
   ScaleWidth      =   12060
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Scelta della pianta dalla quale copiare i settori e i posti"
      Height          =   1935
      Left            =   120
      TabIndex        =   11
      Top             =   3000
      Width           =   9855
      Begin VB.ComboBox cmbVenue 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   660
         Width           =   4755
      End
      Begin VB.ComboBox cmbPianta 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   1335
         Width           =   4740
      End
      Begin VB.Label lblVenue 
         Caption         =   "Venue"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   1470
      End
      Begin VB.Label lblPianta 
         Caption         =   "Pianta"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   1110
         Width           =   2025
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   855
      Left            =   10200
      TabIndex        =   9
      Top             =   600
      Width           =   1695
      Begin VB.CommandButton cmdMappe 
         Caption         =   "Mappe"
         Height          =   435
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   7
      Top             =   6240
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   540
      Width           =   9855
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1380
         MaxLength       =   30
         TabIndex        =   1
         Top             =   420
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1380
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   840
         Width           =   7635
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   180
         TabIndex        =   6
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   900
         Width           =   1095
      End
   End
   Begin VB.Label lblAvanzamento 
      Height          =   375
      Left            =   120
      TabIndex        =   16
      Top             =   5520
      Width           =   9855
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeImpianto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idImpiantoSelezionato As Long
Private nomeImpianto As String
Private descrizioneImpianto As String
Private idRecordSelezionato As Long
Private idVenueSelezionato As Long
Private idPiantaSelezionata As Long

Private internalEvent As Boolean

Private modalit�FormCorrente As AzioneEnum
Private exitCodeFormDettagli As ExitCodeEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init()

    Call Variabili_Init
    Call AggiornaAbilitazioneControlli
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            idImpiantoSelezionato = idNessunElementoSelezionato
            Call InizializzaVenue
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call AggiornaAbilitazioneControlli
    Call frmInizialeImpianto.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    nomeImpianto = ""
    descrizioneImpianto = ""
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdImpiantoSelezionato(id As Long)
    idImpiantoSelezionato = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Impianto"
            cmdMappe.Enabled = False
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Impianto configurato"
            lblVenue.Enabled = False
            cmbVenue.Enabled = False
            lblPianta.Enabled = False
            cmbPianta.Enabled = False
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Impianto configurato"
            lblVenue.Enabled = False
            cmbVenue.Enabled = False
            lblPianta.Enabled = False
            cmbPianta.Enabled = False
            cmdMappe.Enabled = False
    End Select

    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
        
    cmdConferma.Enabled = (Trim(txtNome.Text) <> "") And idPiantaSelezionata <> idNessunElementoSelezionato
End Sub

Private Sub cmdMappe_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraMappe
    
    MousePointer = mousePointerOld

End Sub

Private Sub ConfiguraMappe()
    Call frmConfigurazioneImpiantoMappa.SetIdImpiantoSelezionato(idImpiantoSelezionato)
    Call frmConfigurazioneImpiantoMappa.SetNomeImpiantoSelezionato(nomeImpianto)
    Call frmConfigurazioneImpiantoMappa.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneImpiantoMappa.Init
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbVenue_Click()
    Call cmbVenue_Update
End Sub

Private Sub InizializzaVenue()
    Dim sql As String

    sql = "SELECT IDVENUE ID, NOME"
    sql = sql & " FROM VENUE"
    sql = sql & " ORDER BY nome"
    
    Call CaricaValoriCombo2(cmbVenue, sql, "NOME", False)
End Sub

Private Sub cmbVenue_Update()
    Dim sql As String

    idVenueSelezionato = cmbVenue.ItemData(cmbVenue.ListIndex)
    
    sql = "SELECT P.IDPIANTA ID, P.NOME" & _
            " FROM VENUE_PIANTA VP, PIANTA P" & _
            " WHERE IDVENUE = " & idVenueSelezionato & _
            " AND VP.IDPIANTA = P.IDPIANTA" & _
            " ORDER BY P.NOME"
    
    cmbPianta.Clear
    Call CaricaValoriCombo2(cmbPianta, sql, "NOME", False)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbPianta_Click()
    Call cmbPianta_Update
End Sub

Private Sub cmbPianta_Update()

    idPiantaSelezionata = cmbPianta.ItemData(cmbPianta.ListIndex)
    
    If cmbPianta.ListIndex > -1 Then
        idRecordSelezionato = idPiantaSelezionata
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
        
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaImpianto.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As OraDynaset

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = "SELECT NOME, DESCRIZIONE"
    sql = sql & " FROM IMPIANTO"
    sql = sql & " WHERE IDIMPIANTO = " & idImpiantoSelezionato
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        nomeImpianto = rec("NOME")
        descrizioneImpianto = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeImpianto
    txtDescrizione.Text = descrizioneImpianto
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim isModificabile As Boolean
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            If valoriCampiOK = True Then
                Call InserisciNellaBaseDati
                Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                Call SetModalit�Form(A_MODIFICA)
                Call AggiornaAbilitazioneControlli
            End If
            Call Esci
        Case A_MODIFICA
            If valoriCampiOK = True Then
                Call AggiornaNellaBaseDati
                Call frmMessaggio.Visualizza("NotificaModificaDati")
                Call Esci
            End If
        Case A_ELIMINA
            Call EliminaDallaBaseDati
            Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
            Call Esci
    End Select
    Call frmSceltaPianta.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim idNuovoImpianto As Long
    Dim nomeFile As String
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
'   INSERIMENTO IN TABELLA PIANTA
    idNuovoImpianto = OttieniIdentificatoreDaSequenza("SQ_IMPIANTO")
    sql = "INSERT INTO IMPIANTO (IDIMPIANTO, NOME, DESCRIZIONE, IDVENUE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovoImpianto & ", "
    sql = sql & SqlStringValue(nomeImpianto) & ", "
    sql = sql & SqlStringValue(descrizioneImpianto) & ", "
    sql = sql & idVenueSelezionato & ")"
    n = ORADB.ExecuteSQL(sql)

'   CREAZIONE DEI SETTORI IMPIANTO A PARTIRE DALLA PIANTA SELEZIONATA
    sql = "INSERT INTO SETTOREIMPIANTO (IDSETTOREIMPIANTO, NOME, IDIMPIANTO, NUMERATOAPOSTOUNICO)" & _
            " SELECT SQ_SETTOREIMPIANTO.NEXTVAL, NOME, " & idNuovoImpianto & ", NUMERATAAPOSTOUNICO" & _
            " FROM AREA A" & _
            " WHERE A.IDPIANTA = " & idPiantaSelezionata & _
            " AND A.IDTIPOAREA = " & TA_AREA_NUMERATA
    n = ORADB.ExecuteSQL(sql)

'   CREAZIONE DEI POSTI IMPIANTO A PARTIRE DALLA PIANTA SELEZIONATA
    sql = "INSERT INTO POSTOIMPIANTO (IDPOSTOIMPIANTO, IDSETTOREIMPIANTO, NOMEFILA, NOMEPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE, ETICHETTAPOSTO)" & _
            " SELECT SQ_POSTOIMPIANTO.NEXTVAL, SI.IDSETTOREIMPIANTO, NOMEFILA, NOMEPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE, EP.ETICHETTA" & _
            " FROM AREA A, POSTO P, SETTOREIMPIANTO SI, PIANTA PT, ETICHETTAPOSTO EP" & _
            " WHERE A.IDPIANTA = " & idPiantaSelezionata & _
            " AND A.IDAREA = P.IDAREA" & _
            " AND A.IDPIANTA = PT.IDPIANTA" & _
            " AND SI.IDIMPIANTO = " & idNuovoImpianto & _
            " AND A.NOME = SI.NOME" & _
            " AND P.IDPOSTO = EP.IDPOSTO(+)"
    n = ORADB.ExecuteSQL(sql)

    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    nomeFile = EsportaDatiImpiantoSuExcel(idNuovoImpianto)
    MsgBox "L'impianto � stato creato. Il file con i dati per la creazione del file JSon e': " & nomeFile
    
    Call frmSceltaImpianto.SetIdRecordSelezionato(idNuovoImpianto)
    Call SetIdImpiantoSelezionato(idNuovoImpianto)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim venue As clsElementoLista
    Dim organizzazione As clsElementoLista

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori

'   AGGIORNAMENTO IN TABELLA PIANTA
    sql = "UPDATE IMPIANTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeImpianto) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneImpianto)
    sql = sql & " WHERE IDIMPIANTO = " & idImpiantoSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM POSTOIMPIANTO WHERE IDSETTOREIMPIANTO IN (SELECT IDSETTOREIMPIANTO FROM SETTOREIMPIANTO WHERE IDIMPIANTO = " & idImpiantoSelezionato & ")"
    n = ORADB.ExecuteSQL(sql)
    
    sql = "DELETE FROM SETTOREIMPIANTO WHERE IDIMPIANTO = " & idImpiantoSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    sql = "DELETE FROM IMPIANTO WHERE IDIMPIANTO = " & idImpiantoSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    Set listaNonConformit� = New Collection
    condizioneSql = ""
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_ELIMINA Then
        condizioneSql = "IDIMPIANTO <> " & idImpiantoSelezionato
    End If
    
    nomeImpianto = Trim(txtNome.Text)
    If ViolataUnicit�("IMPIANTO", "NOME = " & SqlStringValue(nomeImpianto), "", _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomeImpianto) & _
            " � gi� presente in DB;")
    End If
    descrizioneImpianto = Trim(txtDescrizione.Text)
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function

Private Function EsportaDatiImpiantoSuExcel(idImpianto As Long) As String
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim nomeFileBat As String
    Dim mousePointerOld As Integer
    Dim s As String
    Dim i As Long
    Dim qtaPosti As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    If idImpianto = idNessunElementoSelezionato Then
        MsgBox "Nessun impianto selezionato!"
    Else
        Call ApriConnessioneBD
        
        qtaPosti = 0
        sql = "SELECT COUNT(PI.IDPOSTOIMPIANTO) QTAPOSTI" & _
                " FROM SETTOREIMPIANTO SI, POSTOIMPIANTO PI" & _
                " WHERE SI.IDIMPIANTO = " & idImpianto & _
                " AND SI.IDSETTOREIMPIANTO = PI.IDSETTOREIMPIANTO"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                qtaPosti = rec("QTAPOSTI")
                rec.MoveNext
            Wend
        End If
        rec.Close
        lblAvanzamento.Caption = "Esportazione di " & qtaPosti & " posti ....."
        DoEvents
    
        nomeFile = App.Path & "\" & idImpianto & "-" & "-ElencoPostiImpianto-"

        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        sql = "SELECT SI.IDIMPIANTO, SI.IDSETTOREIMPIANTO, SI.NOME, SI.NUMERATOAPOSTOUNICO, PI.IDPOSTOIMPIANTO, PI.NOMEFILA, PI.NOMEPOSTO, PI.COORDINATAORIZZONTALE, PI.COORDINATAVERTICALE, PI.ETICHETTAPOSTO" & _
                " FROM SETTOREIMPIANTO SI, POSTOIMPIANTO PI" & _
                " WHERE SI.IDIMPIANTO = " & idImpianto & _
                " AND SI.IDSETTOREIMPIANTO = PI.IDSETTOREIMPIANTO" & _
                " ORDER BY SI.NOME, LPAD(NOMEFILA, 2, ' '), LPAD(NOMEPOSTO, 3, ' ')"
    
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet

        xlsSheet.Rows.Cells(1, 1) = "IDIMPIANTO"
        xlsSheet.Rows.Cells(1, 2) = "IDSETTOREIMPIANTO"
        xlsSheet.Rows.Cells(1, 3) = "NOME"
        xlsSheet.Rows.Cells(1, 4) = "NUMERATOAPOSTOUNICO"
        xlsSheet.Rows.Cells(1, 5) = "IDPOSTOIMPIANTO"
        xlsSheet.Rows.Cells(1, 6) = "NOMEFILA"
        xlsSheet.Rows.Cells(1, 7) = "NOMEPOSTO"
        xlsSheet.Rows.Cells(1, 8) = "COORDINATAORIZZONTALE"
        xlsSheet.Rows.Cells(1, 9) = "COORDINATAVERTICALE"
        xlsSheet.Rows.Cells(1, 10) = "ETICHETTAPOSTO"

        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 2
            While Not rec.EOF
                xlsSheet.Rows.Cells(i, 1) = rec("IDIMPIANTO")
                xlsSheet.Rows.Cells(i, 2) = rec("IDSETTOREIMPIANTO")
                xlsSheet.Rows.Cells(i, 3) = rec("NOME")
                xlsSheet.Rows.Cells(i, 4) = rec("IDPOSTOIMPIANTO")
                xlsSheet.Rows.Cells(i, 5) = rec("NOMEFILA")
                xlsSheet.Rows.Cells(i, 6) = rec("NOMEPOSTO")
                xlsSheet.Rows.Cells(i, 7) = rec("COORDINATAORIZZONTALE")
                xlsSheet.Rows.Cells(i, 8) = rec("COORDINATAVERTICALE")
                xlsSheet.Rows.Cells(i, 9) = rec("NUMERATOAPOSTOUNICO")
                xlsSheet.Rows.Cells(i, 10) = rec("ETICHETTAPOSTO")
                
                i = i + 1
                rec.MoveNext
                
                lblAvanzamento.Caption = "Posto " & i & " di " & qtaPosti
                DoEvents
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing

        MousePointer = mousePointerOld

    End If
    MousePointer = mousePointerOld
    
    EsportaDatiImpiantoSuExcel = nomeFile

End Function

