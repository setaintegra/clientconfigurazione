VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneProdottoTariffe 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   10485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13665
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10485
   ScaleWidth      =   13665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "4"
   Begin VB.CommandButton cmdSelezionaNessunTipo 
      Caption         =   "Nessuno"
      Height          =   315
      Left            =   12360
      TabIndex        =   73
      Top             =   7680
      Width           =   1215
   End
   Begin VB.CommandButton cmdSelezionaTuttiTipi 
      Caption         =   "Tutti"
      Height          =   315
      Left            =   11160
      TabIndex        =   72
      Top             =   7680
      Width           =   1215
   End
   Begin VB.CommandButton cmdSelezionaNessunaCategoria 
      Caption         =   "Nessuna"
      Height          =   315
      Left            =   9840
      TabIndex        =   71
      Top             =   7680
      Width           =   1215
   End
   Begin VB.CommandButton cmdSelezionaTutteCategorie 
      Caption         =   "Tutte"
      Height          =   315
      Left            =   8640
      TabIndex        =   70
      Top             =   7680
      Width           =   1215
   End
   Begin VB.CheckBox chkTitoloCorporateObbligatorio 
      Alignment       =   1  'Right Justify
      Caption         =   "Titolo corporate obbligatorio"
      Height          =   495
      Left            =   11880
      TabIndex        =   69
      Top             =   8280
      Width           =   1575
   End
   Begin VB.CheckBox chkIVAPreassoltaObbligatoria 
      Alignment       =   1  'Right Justify
      Caption         =   "IVA preassolta obbligatoria"
      Height          =   495
      Left            =   9840
      TabIndex        =   68
      Top             =   8280
      Width           =   1455
   End
   Begin VB.ListBox lstTipiSupportoDigitali 
      Height          =   2085
      Left            =   11160
      Style           =   1  'Checkbox
      TabIndex        =   66
      Top             =   5580
      Width           =   2355
   End
   Begin VB.ListBox lstCategorieCarteDiPagamento 
      Height          =   2085
      Left            =   8640
      Style           =   1  'Checkbox
      TabIndex        =   64
      Top             =   5580
      Width           =   2355
   End
   Begin VB.TextBox txtNumeroMassimoTitoliPerAcq 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   58
      Top             =   8280
      Width           =   615
   End
   Begin VB.CheckBox chkCambioUtilizzatoreApplicabile 
      Alignment       =   1  'Right Justify
      Caption         =   "Cambio utilizzatore applicabile"
      Height          =   495
      Left            =   2640
      TabIndex        =   57
      Top             =   8280
      Width           =   1695
   End
   Begin VB.CheckBox chkApplicabileAPostoConRinuncia 
      Alignment       =   1  'Right Justify
      Caption         =   "Applicabile a posto con rinuncia"
      Height          =   495
      Left            =   120
      TabIndex        =   13
      Top             =   8280
      Width           =   1815
   End
   Begin VB.CheckBox chkAggiungiTariffaOmaggio 
      Alignment       =   1  'Right Justify
      Caption         =   "Aggiungi tariffa omaggio"
      Height          =   255
      Left            =   3900
      TabIndex        =   12
      Top             =   9780
      Width           =   2055
   End
   Begin VB.ComboBox cmbClasseTariffaControlloAccessi 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2400
      Style           =   2  'Dropdown List
      TabIndex        =   55
      Top             =   4920
      Width           =   11115
   End
   Begin VB.TextBox txtDescrizionePOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   10
      TabIndex        =   11
      Top             =   7740
      Width           =   1215
   End
   Begin VB.ComboBox cmbAmbitoApplicabilit�Tariffa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9360
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   4440
      Width           =   4155
   End
   Begin VB.ComboBox cmbModalit�Emissione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   5580
      Width           =   2895
   End
   Begin VB.ListBox lstClassiPuntoVenditaDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   3180
      MultiSelect     =   2  'Extended
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   5580
      Width           =   2355
   End
   Begin VB.CommandButton cmdSvuotaClassiPuntoVenditaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   6180
      Width           =   435
   End
   Begin VB.CommandButton cmdClassePuntoVenditaDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   5880
      Width           =   435
   End
   Begin VB.CommandButton cmdClassePuntoVenditaSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5580
      Width           =   435
   End
   Begin VB.ListBox lstClassiPuntoVenditaSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   6060
      MultiSelect     =   2  'Extended
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   5580
      Width           =   2355
   End
   Begin VB.CommandButton cmdInserisciEliminaTariffaIngressoAbbonato 
      Caption         =   "..."
      Height          =   435
      Left            =   5820
      TabIndex        =   25
      Top             =   3480
      Width           =   1155
   End
   Begin VB.CommandButton cmdSvuotaTariffeOrganizzazioneDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   6780
      Width           =   435
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5280
      MaxLength       =   5
      TabIndex        =   5
      Top             =   4440
      Width           =   435
   End
   Begin VB.ComboBox cmbTipoTariffa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5880
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   4440
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizioneAlternativa 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   10
      Top             =   7080
      Width           =   2895
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   4440
      Width           =   4995
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   6240
      Width           =   2895
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   41
      Top             =   3180
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstTariffeOrganizzazioneSelezionate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   6060
      MultiSelect     =   2  'Extended
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   6840
      Width           =   2415
   End
   Begin VB.CommandButton cmdTariffaOrganizzazioneSelezionata 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   7080
      Width           =   435
   End
   Begin VB.CommandButton cmdTariffaOrganizzazioneDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   7380
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaTariffeOrganizzazioneSelezionate 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5580
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   7680
      Width           =   435
   End
   Begin VB.ListBox lstTariffeOrganizzazioneDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   3180
      MultiSelect     =   2  'Extended
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   6840
      Width           =   2355
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   9480
      TabIndex        =   38
      Top             =   9420
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   30
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   28
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   29
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9120
      TabIndex        =   36
      Top             =   240
      Width           =   2115
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   11400
      TabIndex        =   32
      Top             =   240
      Width           =   2115
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   31
      Top             =   9420
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   26
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   27
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoTariffe 
      Height          =   330
      Left            =   6360
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoTariffe 
      Bindings        =   "frmConfigurazioneProdottoTariffe.frx":0000
      Height          =   2235
      Left            =   120
      TabIndex        =   33
      Top             =   600
      Width           =   13395
      _ExtentX        =   23627
      _ExtentY        =   3942
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDataNascitaTitolareMinima 
      Height          =   315
      Left            =   3720
      TabIndex        =   60
      Top             =   8880
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   76808193
      CurrentDate     =   37607
   End
   Begin MSComCtl2.DTPicker dtpDataNascitaTitolareMassima 
      Height          =   315
      Left            =   10200
      TabIndex        =   62
      Top             =   8880
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   76808193
      CurrentDate     =   37607
   End
   Begin VB.Label lblTipiSupportoDigitali 
      Alignment       =   2  'Center
      Caption         =   "Tipi supporto digitali"
      Height          =   195
      Left            =   11160
      TabIndex        =   67
      Top             =   5340
      Width           =   2295
   End
   Begin VB.Label lblCategorieCarteDiPagamento 
      Alignment       =   2  'Center
      Caption         =   "Categorie carte di pagamento"
      Height          =   195
      Left            =   8640
      TabIndex        =   65
      Top             =   5340
      Width           =   2295
   End
   Begin VB.Label lblDataNascitaTitolareMassima 
      Caption         =   "Data nascita titolare massima (tariffe tipo UNDER)"
      Height          =   255
      Left            =   6240
      TabIndex        =   63
      Top             =   8880
      Width           =   3735
   End
   Begin VB.Label lblDataNascitaTitolareMinima 
      Caption         =   "Data nascita titolare minima (tariffe tipo OVER)"
      Height          =   255
      Left            =   120
      TabIndex        =   61
      Top             =   8880
      Width           =   3495
   End
   Begin VB.Label lblNumeroMassimoTitoliPerAcq 
      Alignment       =   1  'Right Justify
      Caption         =   "Numero massimo titoli per acquirente (solo se minore di quanto precisato sul prodotto)"
      Height          =   495
      Left            =   5160
      TabIndex        =   59
      Top             =   8280
      Width           =   3195
   End
   Begin VB.Label lblClasseTariffaControlloAccessi 
      Caption         =   "Classe per Controllo Accessi"
      Height          =   255
      Left            =   120
      TabIndex        =   56
      Top             =   4920
      Width           =   1995
   End
   Begin VB.Label lblDescrizionePOS 
      Caption         =   "Descrizione POS"
      Height          =   255
      Left            =   120
      TabIndex        =   54
      Top             =   7500
      Width           =   1275
   End
   Begin VB.Label lblAmbitoApplicabilit�Tariffa 
      Caption         =   "Ambito di applicabilit�"
      Height          =   255
      Left            =   9360
      TabIndex        =   53
      Top             =   4200
      Width           =   1635
   End
   Begin VB.Label lblModalit�Emissione 
      Caption         =   "Modalit� di emissione"
      Height          =   255
      Left            =   120
      TabIndex        =   52
      Top             =   5340
      Width           =   1515
   End
   Begin VB.Label lblClassiPVSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Classi punti vendita selezionati"
      Height          =   195
      Left            =   6060
      TabIndex        =   51
      Top             =   5340
      Width           =   2355
   End
   Begin VB.Label lblClassiPVDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Classi punti vendita disponibili"
      Height          =   195
      Left            =   3180
      TabIndex        =   50
      Top             =   5340
      Width           =   2355
   End
   Begin VB.Label lblTariffaIngressoAbbonato 
      Caption         =   "Tariffa ingresso abbonato"
      Height          =   195
      Left            =   7020
      TabIndex        =   49
      Top             =   3600
      Width           =   2235
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   5280
      TabIndex        =   48
      Top             =   4200
      Width           =   555
   End
   Begin VB.Label lblDescrizioneAlternativa 
      Caption         =   "Descrizione alternativa"
      Height          =   255
      Left            =   120
      TabIndex        =   47
      Top             =   6840
      Width           =   1695
   End
   Begin VB.Label lblTipoTariffa 
      Caption         =   "Tipo Tariffa - Codice"
      Height          =   255
      Left            =   5880
      TabIndex        =   46
      Top             =   4200
      Width           =   1515
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   45
      Top             =   4200
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   44
      Top             =   6000
      Width           =   1575
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   43
      Top             =   2940
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   42
      Top             =   2940
      Width           =   2775
   End
   Begin VB.Label lbTariffeOrganizzazioneDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Tariffe Organizzazione disponibili"
      Height          =   195
      Left            =   3180
      TabIndex        =   40
      Top             =   6600
      Width           =   2355
   End
   Begin VB.Label lblTariffeOrganizzazioneSelezionate 
      Alignment       =   2  'Center
      Caption         =   "Tariffe Organizzazione selezionate"
      Height          =   195
      Left            =   6060
      TabIndex        =   39
      Top             =   6600
      Width           =   2355
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   9120
      TabIndex        =   37
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   11400
      TabIndex        =   35
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Tariffe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   34
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoTariffe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private isProdottoTMaster As ValoreBooleanoEnum
Private IsProdottoTDL As ValoreBooleanoEnum

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idTipoTariffa As Long
Private idStagioneSelezionata As Long
Private idTipoTerminaleSelezionato As TipoTerminaleEnum
Private idClassePuntoVenditaSelezionato As TipoClassePuntoVenditaEnum
Private isRecordEditabile As Boolean
Private descrizioneRecordSelezionato As String
Private descrizioneAlternativaRecordSelezionato As String
Private descrizionePOSRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private nomeTipoTerminaleSelezionato As String
Private codiceTastoTipoTerminaleSelezionato As String
Private idTastoTipoTerminaleSelezionato As Long
Private codice As String
Private codiceTipoTariffa As String
Private codiceTipoTariffaSiae As String
Private rateo As Long
Private listaTariffeOrganizzazioneDisponibili As Collection
Private listaTariffeOrganizzazioneSelezionate As Collection
Private nomeTabellaTemporanea As String
Private listaAppoggioClassiPuntiVendita As Collection
Private listaTipiTerminaleDisponibili As Collection
Private listaTipiTerminaleSelezionati As Collection
Private listaClassiPuntoVenditaDisponibili As Collection
Private listaClassiPuntoVenditaSelezionati As Collection
Private isProdottoAttivoSuTL As Boolean
Private esisteTariffaIngressoAbbonato As Boolean
Private idTariffaIngressoAbbonato As Long
Private isProdottoCorrenteAdInvito As Boolean
Private isGestioneEccedenzeOmaggioCorrenteEvoluta As Boolean
Private idModalit�Emissione As Modalit�EmissioneEnum
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private idTipoAmbitoApplicabilt�Tariffa As TipoAmbitoApplicabilit�TariffaEnum
Private idClasseTariffaControlloAccessi As Long
Private applicabileAPostoConRinuncia As ValoreBooleanoEnum
Private cambioUtilizzatoreApplicabile As ValoreBooleanoEnum
Private gestioneTariffaOmaggioPermessa As ValoreBooleanoEnum
Private numeroMassimoTitoliPerAcq As Long
Private IVAPreassoltaObbligatoria As ValoreBooleanoEnum
Private titoloCorporateObbligatorio As ValoreBooleanoEnum
Private dataNascitaTitolareMinima As Date
Private dataNascitaTitolareMassima As Date
Private prodottoRientraDecretoSicurezza As ValoreBooleanoEnum
Private numeroMassimoTitoliPerAcqProdotto As Long
Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
'Private validaPerAccedere As ValoreBooleanoEnum
Private gestioneExitCode As ExitCodeEnum
Private tipoTariffa As TipoOrigineTariffaEnum
Private modalit�FormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
Dim pippo As Boolean

    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    dgrConfigurazioneProdottoTariffe.Caption = "TARIFFE CONFIGURATE"
    If esisteTariffaIngressoAbbonato Then
        cmdInserisciEliminaTariffaIngressoAbbonato.Caption = "Elimina"
    Else
        cmdInserisciEliminaTariffaIngressoAbbonato.Caption = "Inserisci"
    End If
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            tipoTariffa = TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoTariffe.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        Call cmbModalit�Emissione.Clear
        Call cmbAmbitoApplicabilit�Tariffa.Clear
        Call cmbClasseTariffaControlloAccessi.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        lstClassiPuntoVenditaDisponibili.Enabled = False
        lstClassiPuntoVenditaSelezionati.Enabled = False
        lstCategorieCarteDiPagamento.Enabled = False
        lstTipiSupportoDigitali.Enabled = False
        cmbTipoTariffa.Enabled = False
        cmbModalit�Emissione.Enabled = False
        cmbAmbitoApplicabilit�Tariffa.Enabled = False
        cmbClasseTariffaControlloAccessi.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblTipoTariffa.Enabled = False
        lblModalit�Emissione.Enabled = False
        lblAmbitoApplicabilit�Tariffa.Enabled = False
        lblClasseTariffaControlloAccessi.Enabled = False
'        lblTipiTerminaleDisponibili.Enabled = False
'        lblTipiTerminaleSelezionati.Enabled = False
        lblCategorieCarteDiPagamento.Enabled = False
        lblTipiSupportoDigitali.Enabled = False
'        chkValidaPerAccedere.Value = vbUnchecked
'        chkValidaPerAccedere.Enabled = False
        cmdTariffaOrganizzazioneSelezionata.Enabled = True
        cmdTariffaOrganizzazioneDisponibile.Enabled = True
        cmdSvuotaTariffeOrganizzazioneSelezionate.Enabled = True
        cmdSvuotaTariffeOrganizzazioneDisponibili.Enabled = True
        cmdClassePuntoVenditaDisponibile.Enabled = False
        cmdClassePuntoVenditaSelezionato.Enabled = False
        cmdSvuotaClassiPuntoVenditaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            idRecordSelezionato <> idTariffaIngressoAbbonato)
        cmdModifica.Enabled = idRecordSelezionato <> idNessunElementoSelezionato
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            idRecordSelezionato <> idTariffaIngressoAbbonato)
        cmdInserisciEliminaTariffaIngressoAbbonato.Enabled = True
        lblTariffaIngressoAbbonato.Enabled = True
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        chkAggiungiTariffaOmaggio.Enabled = False
        chkApplicabileAPostoConRinuncia.Enabled = False
        chkCambioUtilizzatoreApplicabile.Enabled = False
        lblNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Text = ""
        chkIVAPreassoltaObbligatoria.Enabled = False
        chkTitoloCorporateObbligatorio.Enabled = False
        lblDataNascitaTitolareMassima.Enabled = False
        dtpDataNascitaTitolareMassima.Enabled = False
        dtpDataNascitaTitolareMassima.Value = dataNulla
        lblDataNascitaTitolareMinima.Enabled = False
        dtpDataNascitaTitolareMinima.Enabled = False
        dtpDataNascitaTitolareMinima.Value = dataNulla
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO And _
            tipoTariffa = TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoTariffe.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbTipoTariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            Not isProdottoCorrenteAdInvito)
        cmbModalit�Emissione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbAmbitoApplicabilit�Tariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbClasseTariffaControlloAccessi.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipoTariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            Not isProdottoCorrenteAdInvito)
        lblModalit�Emissione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblAmbitoApplicabilit�Tariffa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblClasseTariffaControlloAccessi.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        chkValidaPerAccedere.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
'            idTipoTariffa <> TIPO_TARIFFA_SERVIZIO)
        lstClassiPuntoVenditaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstClassiPuntoVenditaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstCategorieCarteDiPagamento.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstTipiSupportoDigitali.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblClassiPVDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblClassiPVSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdClassePuntoVenditaDisponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdClassePuntoVenditaSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaClassiPuntoVenditaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        Call lstTariffeOrganizzazioneDisponibili.Clear
        Call lstTariffeOrganizzazioneSelezionate.Clear
        lstTariffeOrganizzazioneDisponibili.Enabled = False
        lstTariffeOrganizzazioneSelezionate.Enabled = False
        lbTariffeOrganizzazioneDisponibili.Enabled = False
        lblTariffeOrganizzazioneSelezionate.Enabled = False
        cmdTariffaOrganizzazioneSelezionata.Enabled = False
        cmdTariffaOrganizzazioneDisponibile.Enabled = False
        cmdSvuotaTariffeOrganizzazioneSelezionate.Enabled = False
        cmdSvuotaTariffeOrganizzazioneDisponibili.Enabled = False
        
        lblCategorieCarteDiPagamento.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipiSupportoDigitali.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdInserisciEliminaTariffaIngressoAbbonato.Enabled = False
        lblTariffaIngressoAbbonato.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> "" And _
            Len(Trim(txtCodice.Text)) <= 5 And _
            cmbTipoTariffa.ListIndex <> idNessunElementoSelezionato And _
            cmbAmbitoApplicabilit�Tariffa.ListIndex <> idNessunElementoSelezionato And _
            cmbModalit�Emissione.ListIndex <> idNessunElementoSelezionato
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
        pippo = (esisteTariffaOmaggioCollegata(idRecordSelezionato) = VB_FALSO)
        
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
'                chkAggiungiTariffaOmaggio.Enabled = prodottoPermetteGestioneTariffeOmaggio(idProdottoSelezionato) And (idTipoTariffa = TIPO_TARIFFA_INTERO Or idTipoTariffa = TIPO_TARIFFA_RIDOTTO)
                chkAggiungiTariffaOmaggio.Enabled = VB_FALSO
                chkCambioUtilizzatoreApplicabile = VB_VERO
                chkTitoloCorporateObbligatorio = VB_FALSO
                chkIVAPreassoltaObbligatoria = VB_FALSO
                chkApplicabileAPostoConRinuncia = VB_FALSO
            Case ASG_INSERISCI_DA_SELEZIONE
'                chkAggiungiTariffaOmaggio.Enabled = prodottoPermetteGestioneTariffeOmaggio(idProdottoSelezionato) And (idTipoTariffa = TIPO_TARIFFA_INTERO Or idTipoTariffa = TIPO_TARIFFA_RIDOTTO)
                chkAggiungiTariffaOmaggio.Enabled = VB_FALSO
            Case ASG_MODIFICA
'                chkAggiungiTariffaOmaggio.Enabled = prodottoPermetteGestioneTariffeOmaggio(idProdottoSelezionato) _
'                    And (idTipoTariffa = TIPO_TARIFFA_INTERO Or idTipoTariffa = TIPO_TARIFFA_RIDOTTO) _
'                    And (esisteTariffaOmaggioCollegata(idRecordSelezionato) = VB_FALSO)
                chkAggiungiTariffaOmaggio.Enabled = VB_FALSO
            Case ASG_ELIMINA
                chkAggiungiTariffaOmaggio.Enabled = VB_FALSO
            Case Else
                'Do Nothing
        End Select
        chkApplicabileAPostoConRinuncia.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkCambioUtilizzatoreApplicabile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNumeroMassimoTitoliPerAcq.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtNumeroMassimoTitoliPerAcq.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkIVAPreassoltaObbligatoria.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkTitoloCorporateObbligatorio.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDataNascitaTitolareMassima.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        dtpDataNascitaTitolareMassima.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDataNascitaTitolareMinima.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        dtpDataNascitaTitolareMinima.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            tipoTariffa <> TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoTariffe.Enabled = False
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        Call cmbModalit�Emissione.Clear
        Call cmbAmbitoApplicabilit�Tariffa.Clear
        Call cmbClasseTariffaControlloAccessi.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        cmbModalit�Emissione.Enabled = False
        cmbAmbitoApplicabilit�Tariffa.Enabled = False
        cmbClasseTariffaControlloAccessi.Enabled = False
        lstClassiPuntoVenditaDisponibili.Enabled = False
        lstClassiPuntoVenditaSelezionati.Enabled = False
        lstCategorieCarteDiPagamento.Enabled = False
        lstTipiSupportoDigitali.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblCodice.Enabled = False
        lblTipoTariffa.Enabled = False
        lblClassiPVDisponibili.Enabled = False
        lblClassiPVSelezionati.Enabled = False
        lblModalit�Emissione.Enabled = False
        lblAmbitoApplicabilit�Tariffa.Enabled = False
        lblClasseTariffaControlloAccessi.Enabled = False
'        chkValidaPerAccedere.Enabled = False
        cmdTariffaOrganizzazioneSelezionata.Enabled = True
        cmdTariffaOrganizzazioneDisponibile.Enabled = True
        cmdSvuotaTariffeOrganizzazioneSelezionate.Enabled = True
        cmdSvuotaTariffeOrganizzazioneDisponibili.Enabled = True
        cmdClassePuntoVenditaDisponibile.Enabled = False
        cmdClassePuntoVenditaSelezionato.Enabled = False
        cmdSvuotaClassiPuntoVenditaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdInserisciEliminaTariffaIngressoAbbonato.Enabled = False
        lblTariffaIngressoAbbonato.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (lstTariffeOrganizzazioneSelezionate.ListCount > 0)
        cmdAnnulla.Enabled = True
        Select Case tipoTariffa
            Case TOT_STANDARD_ORGANIZZAZIONE
                lblOperazioneInCorso.Caption = "Operazione in corso:"
                lblOperazione.Caption = "inserimento nuovo record"
            Case Else
                'Do Nothing
        End Select
        chkAggiungiTariffaOmaggio.Enabled = False
        chkApplicabileAPostoConRinuncia.Enabled = False
        chkCambioUtilizzatoreApplicabile.Enabled = False
        lblNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Text = ""
        chkIVAPreassoltaObbligatoria.Enabled = False
        chkTitoloCorporateObbligatorio.Enabled = False
        lblDataNascitaTitolareMassima.Enabled = False
        dtpDataNascitaTitolareMassima.Enabled = False
        dtpDataNascitaTitolareMassima.Value = dataNulla
        lblDataNascitaTitolareMinima.Enabled = False
        dtpDataNascitaTitolareMinima.Enabled = False
        dtpDataNascitaTitolareMinima.Value = dataNulla
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            tipoTariffa = TOT_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoTariffe.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        Call cmbTipoTariffa.Clear
        Call cmbModalit�Emissione.Clear
        Call cmbAmbitoApplicabilit�Tariffa.Clear
        Call cmbClasseTariffaControlloAccessi.Clear
        Call lstTariffeOrganizzazioneSelezionate.Clear
        Call lstClassiPuntoVenditaDisponibili.Clear
        Call lstClassiPuntoVenditaSelezionati.Clear
        Call lstCategorieCarteDiPagamento.Clear
        Call lstTipiSupportoDigitali.Clear
        lstTariffeOrganizzazioneDisponibili.Enabled = True
        lstTariffeOrganizzazioneSelezionate.Enabled = True
        lbTariffeOrganizzazioneDisponibili.Enabled = True
        lblTariffeOrganizzazioneSelezionate.Enabled = True
        lblCategorieCarteDiPagamento.Enabled = True
        lblTipiSupportoDigitali.Enabled = True
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        cmbTipoTariffa.Enabled = False
        cmbModalit�Emissione.Enabled = False
        cmbAmbitoApplicabilit�Tariffa.Enabled = False
        cmbClasseTariffaControlloAccessi.Enabled = False
        lstClassiPuntoVenditaDisponibili.Enabled = False
        lstClassiPuntoVenditaSelezionati.Enabled = False
        lstCategorieCarteDiPagamento.Enabled = False
        lstTipiSupportoDigitali.Enabled = False
'        chkValidaPerAccedere.Value = vbUnchecked
'        chkValidaPerAccedere.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblTipoTariffa.Enabled = False
        lblModalit�Emissione.Enabled = False
        lblAmbitoApplicabilit�Tariffa.Enabled = False
        lblClasseTariffaControlloAccessi.Enabled = False
        lblClassiPVDisponibili.Enabled = False
        lblClassiPVSelezionati.Enabled = False
        cmdTariffaOrganizzazioneSelezionata.Enabled = True
        cmdTariffaOrganizzazioneDisponibile.Enabled = True
        cmdSvuotaTariffeOrganizzazioneSelezionate.Enabled = True
        cmdSvuotaTariffeOrganizzazioneDisponibili.Enabled = True
        cmdClassePuntoVenditaDisponibile.Enabled = False
        cmdClassePuntoVenditaSelezionato.Enabled = False
        cmdSvuotaClassiPuntoVenditaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            idRecordSelezionato <> idTariffaIngressoAbbonato)
        cmdModifica.Enabled = idRecordSelezionato <> idNessunElementoSelezionato
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            idRecordSelezionato <> idTariffaIngressoAbbonato)
        cmdInserisciEliminaTariffaIngressoAbbonato.Enabled = True
        lblTariffaIngressoAbbonato.Enabled = True
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        chkAggiungiTariffaOmaggio.Enabled = False
        chkApplicabileAPostoConRinuncia.Enabled = False
        chkCambioUtilizzatoreApplicabile.Enabled = False
        lblNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Text = ""
        chkIVAPreassoltaObbligatoria.Enabled = False
        chkTitoloCorporateObbligatorio.Enabled = False
        lblDataNascitaTitolareMassima.Enabled = False
        dtpDataNascitaTitolareMassima.Enabled = False
        dtpDataNascitaTitolareMassima.Value = dataNulla
        lblDataNascitaTitolareMinima.Enabled = False
        dtpDataNascitaTitolareMinima.Enabled = False
        dtpDataNascitaTitolareMinima.Value = Null
        
    Else
        'Do Nothing
    End If
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)) And _
                                    (IsConfigurataAlmenoUnaTariffa)
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select

' in ogni caso se il prodotto non rientra nel decreto sicurezza disabilita i campi correlati
    If prodottoRientraDecretoSicurezza = VB_FALSO Then
        chkCambioUtilizzatoreApplicabile.Enabled = False
        lblNumeroMassimoTitoliPerAcq.Enabled = False
        txtNumeroMassimoTitoliPerAcq.Enabled = False
    End If

    chkAggiungiTariffaOmaggio.Visible = VB_FALSO
    
    If isProdottoTMaster Or IsProdottoTDL Then
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdInserisciEliminaTariffaIngressoAbbonato.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = False
    End If
    
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioClassiPuntiVendita
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call EliminaTabellaAppoggioClassiPuntiVendita
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub CaricaFormConfigurazionePeriodiCommerciali()
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetRateo(rateo)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetModalit�Form(A_NUOVO)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoPeriodiCommerciali.Init
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIsProdottoTMaster(b As ValoreBooleanoEnum)
    isProdottoTMaster = b
End Sub

Public Sub SetIsProdottoTDL(b As ValoreBooleanoEnum)
    IsProdottoTDL = b
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazionePeriodiCommerciali
    Call EliminaTabellaAppoggioClassiPuntiVendita
End Sub

Private Sub cmbModalit�Emissione_Click()
    If Not internalEvent Then
        idModalit�Emissione = cmbModalit�Emissione.ItemData(cmbModalit�Emissione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbAmbitoApplicabilit�Tariffa_Click()
    If Not internalEvent Then
        idTipoAmbitoApplicabilt�Tariffa = cmbAmbitoApplicabilit�Tariffa.ItemData(cmbAmbitoApplicabilit�Tariffa.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbClasseTariffaControlloAccessi_Click()
    If Not internalEvent Then
        idClasseTariffaControlloAccessi = cmbClasseTariffaControlloAccessi.ItemData(cmbClasseTariffaControlloAccessi.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbTipoTariffa_Click()
    If Not internalEvent Then
        idTipoTariffa = cmbTipoTariffa.ItemData(cmbTipoTariffa.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call CaricaValoriLstTariffeOrganizzazioneDisponibili
    Call AggiornaAbilitazioneControlli
    Set listaTariffeOrganizzazioneSelezionate = New Collection
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilit�ProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
    
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If valoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoTariffe_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoTariffe_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoTariffe_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoTariffe_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoTariffe_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoTariffe_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati(idRecordSelezionato)
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoTariffe_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneProdottoTariffe_Init
                    End Select
                    Select Case tipoTariffa
                        Case TOT_STANDARD_ORGANIZZAZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
                            Call SetTipoTariffa(TOT_NON_SPECIFICATO)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoTariffe_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneProdottoTariffe_Init
                    End Select
                End If
               
                Call CaricaValoriLstTariffeOrganizzazioneDisponibili
                Call AggiornaAbilitazioneControlli
                Set listaTariffeOrganizzazioneSelezionate = New Collection
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilit�Campi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdSelezionaTutteCategorie_Click()
    Call SelezionaListe(lstCategorieCarteDiPagamento, True)
End Sub

Private Sub cmdSelezionaNessunaCategoria_Click()
    Call SelezionaListe(lstCategorieCarteDiPagamento, False)
End Sub

Private Sub cmdSelezionaTuttiTipi_Click()
    Call SelezionaListe(lstTipiSupportoDigitali, True)
End Sub

Private Sub cmdSelezionaNessunTipo_Click()
    Call SelezionaListe(lstTipiSupportoDigitali, False)
End Sub

Private Sub SelezionaListe(lista As listBox, b As Boolean)
    Dim i As Long
    
    For i = 0 To lista.ListCount - 1
        lista.Selected(i) = b
    Next i
End Sub
                                
Private Sub cmdTariffaOrganizzazioneDisponibile_Click()
    Call SpostaDaSelezionatiADisponibili
End Sub

Private Sub SpostaDaSelezionatiADisponibili()
    Call SpostaInLstDisponibili
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_STANDARD_ORGANIZZAZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idTariffaOrg As Long
    Dim tariffaOrg As clsElementoLista
    Dim chiaveTariffaOrg As String
    
    For i = 1 To lstTariffeOrganizzazioneSelezionate.ListCount
        If lstTariffeOrganizzazioneSelezionate.Selected(i - 1) Then
            idTariffaOrg = lstTariffeOrganizzazioneSelezionate.ItemData(i - 1)
            chiaveTariffaOrg = ChiaveId(idTariffaOrg)
            Set tariffaOrg = listaTariffeOrganizzazioneSelezionate.Item(chiaveTariffaOrg)
            Call listaTariffeOrganizzazioneDisponibili.Add(tariffaOrg, chiaveTariffaOrg)
            Call listaTariffeOrganizzazioneSelezionate.Remove(chiaveTariffaOrg)
        End If
    Next i
    Call lstTariffeOrganizzazioneDisponibili_Init
    Call lstTariffeOrganizzazioneSelezionate_Init
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sqlTipo As String
    Dim sqlModalit� As String
    Dim sqlAmbito As String
    Dim sqlClasseControlloAccessi As String
    
    sqlTipo = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME"
    sqlTipo = sqlTipo & " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    sqlTipo = sqlTipo & " AND IDTIPOTARIFFA <> " & IDTIPOTARIFFA_INGRESSO_ABBONATO
    sqlModalit� = "SELECT P.IDMODALITAEMISSIONE ID, NOME"
    sqlModalit� = sqlModalit� & " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P"
    sqlModalit� = sqlModalit� & " WHERE P.IDMODALITAEMISSIONE = M.IDMODALITAEMISSIONE"
    sqlModalit� = sqlModalit� & " AND P.IDPRODOTTO = " & idProdottoSelezionato
    sqlModalit� = sqlModalit� & " ORDER BY NOME"
    sqlAmbito = "SELECT IDAMBITOAPPLICABILITATARIFFA ID, NOME"
    sqlAmbito = sqlAmbito & " FROM AMBITOAPPLICABILITATARIFFA"
    sqlAmbito = sqlAmbito & " WHERE IDAMBITOAPPLICABILITATARIFFA > 0"
    sqlClasseControlloAccessi = "SELECT IDCLASSETARIFFACONTROLLOACC ID, NOME || '-' || COMPORTAMENTO|| '-' || TIPOLOGIATITOLI AS NOME"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " FROM CLASSETARIFFACONTROLLOACC"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " WHERE IDCLASSETARIFFACONTROLLOACC > 0"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipo, "COD")
    Call CaricaValoriCombo(cmbModalit�Emissione, sqlModalit�, "NOME")
    Call CaricaValoriCombo(cmbAmbitoApplicabilit�Tariffa, sqlAmbito, "NOME")
    Call CaricaValoriCombo(cmbClasseTariffaControlloAccessi, sqlClasseControlloAccessi, "NOME")
    Call AssegnaValoriCampi
    Call CaricaValorilstClassiPuntoVenditaDisponibili
    Call CaricaValorilstClassiPuntoVenditaSelezionati
    Call lstCategorieCarteDiPagamento_Init
    Call lstTipiSupportoDigitali_Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalit�FormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioClassiPuntiVendita
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sqlTipo As String
    Dim sqlModalit� As String
    Dim sqlAmbito As String
    Dim sqlClasseControlloAccessi As String
    
    sqlTipo = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME"
    sqlTipo = sqlTipo & " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    sqlTipo = sqlTipo & " AND IDTIPOTARIFFA <> " & IDTIPOTARIFFA_INGRESSO_ABBONATO
    sqlTipo = sqlTipo & " ORDER BY CODICE"
    sqlModalit� = "SELECT P.IDMODALITAEMISSIONE ID, NOME"
    sqlModalit� = sqlModalit� & " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P"
    sqlModalit� = sqlModalit� & " WHERE P.IDMODALITAEMISSIONE = M.IDMODALITAEMISSIONE"
    sqlModalit� = sqlModalit� & " AND P.IDPRODOTTO = " & idProdottoSelezionato
    sqlModalit� = sqlModalit� & " ORDER BY NOME"
    sqlAmbito = "SELECT IDAMBITOAPPLICABILITATARIFFA ID, NOME"
    sqlAmbito = sqlAmbito & " FROM AMBITOAPPLICABILITATARIFFA"
    sqlAmbito = sqlAmbito & " WHERE IDAMBITOAPPLICABILITATARIFFA > 0"
    sqlClasseControlloAccessi = "SELECT IDCLASSETARIFFACONTROLLOACC ID, NOME || '-' || COMPORTAMENTO|| '-' || TIPOLOGIATITOLI AS NOME"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " FROM CLASSETARIFFACONTROLLOACC"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " WHERE IDCLASSETARIFFACONTROLLOACC > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipo, "COD")
    Call CaricaValoriCombo(cmbModalit�Emissione, sqlModalit�, "NOME")
    Call CaricaValoriCombo(cmbAmbitoApplicabilit�Tariffa, sqlAmbito, "NOME")
    Call CaricaValoriCombo(cmbClasseTariffaControlloAccessi, sqlClasseControlloAccessi, "NOME")
    Call AssegnaValoriCampi
    Call CaricaValorilstClassiPuntoVenditaDisponibili
    Call CaricaValorilstClassiPuntoVenditaSelezionati
    Call lstCategorieCarteDiPagamento_Init
    Call lstTipiSupportoDigitali_Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciEliminaTariffaIngressoAbbonato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciEliminaTariffaIngressoAbbonato
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciEliminaTariffaIngressoAbbonato()
    Dim sql As String
    Dim idNewTariffa As Long
    Dim stringaNota As String
    Dim n As Long

    stringaNota = "(TARIFFA INGRESSO ABBONATO); IDPRODOTTO = " & idProdottoSelezionato
    
    Call ApriConnessioneBD
    
    If esisteTariffaIngressoAbbonato Then
        Call EliminaDallaBaseDati(idTariffaIngressoAbbonato)
        If Not esisteTariffaIngressoAbbonato Then
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
        End If
    Else
        idNewTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
        sql = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, CODICE," & _
            " DESCRIZIONEALTERNATIVA, IDORGANIZZAZIONE," & _
            " IDTIPOTARIFFA, IDPRODOTTO)" & _
            " VALUES (" & _
            idNewTariffa & ", " & _
            "'INGRESSO ABBONATO', " & _
            "'INGRESSO ABBONATO', " & _
            "'9', " & _
            "'INGRESSO ABBONATO', " & _
            "NULL, " & _
            T_INGRESSO_ABBONATO & ", " & _
            idProdottoSelezionato & ")"
        SETAConnection.Execute sql, n, adCmdText
        sql = "INSERT INTO TARIFFA_TIPOTERMINALE (IDTARIFFA, IDTIPOTERMINALE, IDTASTOTIPOTERMINALE)" & _
            " VALUES (" & _
            idNewTariffa & ", " & _
            TT_TERMINALE_AVANZATO & ", " & _
            "NULL)"
        SETAConnection.Execute sql, n, adCmdText
'        If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = TSR_NUOVO
'            Call AggiornaParametriSessioneSuRecord("TARIFFA", "IDTARIFFA", idNewTariffa, TSR_NUOVO)
'            Call AggiornaParametriSessioneSuRecord("TARIFFA_TIPOTERMINALE", "IDTARIFFA", idNewTariffa, TSR_NUOVO)
'        End If
        
        idTariffaIngressoAbbonato = idNewTariffa
        esisteTariffaIngressoAbbonato = True
        Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_TARIFFA, stringaNota, idProdottoSelezionato)
    End If
    
    Call ChiudiConnessioneBD
    
'    Call adcConfigurazioneProdottoTariffe.Refresh
    Call adcConfigurazioneProdottoTariffe_Init
    Call dgrConfigurazioneProdottoTariffe_Init
    idRecordSelezionato = idNessunElementoSelezionato
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sqlTipo As String
    Dim sqlModalit� As String
    Dim sqlAmbito As String
    Dim sqlClasseControlloAccessi As String
    
    isRecordEditabile = True
    idTipoTariffa = idNessunElementoSelezionato
    sqlTipo = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME"
    sqlTipo = sqlTipo & " FROM TIPOTARIFFA WHERE IDTIPOTARIFFA > 0"
    sqlTipo = sqlTipo & " AND IDTIPOTARIFFA <> " & IDTIPOTARIFFA_INGRESSO_ABBONATO
    sqlTipo = sqlTipo & " ORDER BY CODICE"
    sqlModalit� = "SELECT P.IDMODALITAEMISSIONE ID, NOME"
    sqlModalit� = sqlModalit� & " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P"
    sqlModalit� = sqlModalit� & " WHERE P.IDMODALITAEMISSIONE = M.IDMODALITAEMISSIONE"
    sqlModalit� = sqlModalit� & " AND P.IDPRODOTTO = " & idProdottoSelezionato
    sqlModalit� = sqlModalit� & " ORDER BY NOME"
    sqlAmbito = "SELECT IDAMBITOAPPLICABILITATARIFFA ID, NOME"
    sqlAmbito = sqlAmbito & " FROM AMBITOAPPLICABILITATARIFFA"
    sqlAmbito = sqlAmbito & " WHERE IDAMBITOAPPLICABILITATARIFFA > 0"
    sqlClasseControlloAccessi = "SELECT IDCLASSETARIFFACONTROLLOACC ID, NOME || '-' || COMPORTAMENTO|| '-' || TIPOLOGIATITOLI AS NOME"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " FROM CLASSETARIFFACONTROLLOACC"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " WHERE IDCLASSETARIFFACONTROLLOACC > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipo, "COD")
    Call CaricaValoriCombo(cmbModalit�Emissione, sqlModalit�, "NOME")
    Call CaricaValoriCombo(cmbAmbitoApplicabilit�Tariffa, sqlAmbito, "NOME")
    Call CaricaValoriCombo(cmbClasseTariffaControlloAccessi, sqlClasseControlloAccessi, "NOME")
    If isProdottoCorrenteAdInvito Then
        Call SelezionaElementoSuCombo(cmbTipoTariffa, TIPO_TARIFFA_SERVIZIO)
    End If
    Call CaricaValorilstClassiPuntoVenditaDisponibili
    Call CaricaValorilstClassiPuntoVenditaSelezionati
    Call lstCategorieCarteDiPagamento_Init
    Call lstTipiSupportoDigitali_Init
End Sub

Private Sub CaricaValoriLstTariffeOrganizzazioneDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTariffaOrg As String
    Dim tariffaOrganizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTariffeOrganizzazioneDisponibili = New Collection
    
    sql = "SELECT TARIFFA.IDTARIFFA," & _
        " TARIFFA.NOME AS ""Nome""," & _
        " TARIFFA.DESCRIZIONE AS ""Descrizione""" & _
        " FROM TARIFFA WHERE" & _
        " (TARIFFA.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") AND" & _
        " TARIFFA.IDTARIFFA NOT IN" & _
        " (SELECT TARIFFA.IDTARIFFA FROM TARIFFA WHERE TARIFFA.IDPRODOTTO = " & idProdottoSelezionato & ")" & _
        " ORDER BY ""Nome"""
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tariffaOrganizzazioneCorrente = New clsElementoLista
            tariffaOrganizzazioneCorrente.nomeElementoLista = rec("NOME")
            tariffaOrganizzazioneCorrente.idElementoLista = rec("IDTARIFFA").Value
            tariffaOrganizzazioneCorrente.descrizioneElementoLista = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
            chiaveTariffaOrg = ChiaveId(tariffaOrganizzazioneCorrente.idElementoLista)
            Call listaTariffeOrganizzazioneDisponibili.Add(tariffaOrganizzazioneCorrente, chiaveTariffaOrg)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstTariffeOrganizzazioneDisponibili_Init

End Sub

Private Sub lstCategorieCarteDiPagamento_Init()
    Dim internalEventOld As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call ApriConnessioneBD
    lstCategorieCarteDiPagamento.Clear
    
    sql = "SELECT C.IDCATEGORIACARTADIPAGAMENTO, C.NOME AS ""Nome"", C.DESCRIZIONE AS ""Descrizione"", DECODE(TC.IDCATEGORIACARTADIPAGAMENTO, NULL, 0, 1) ABIL" & _
        " FROM CATEGORIACARTADIPAGAMENTO C, PRODOTTO P, ORG_GENERECARTADIPAGAMENTO OG," & _
        " (" & _
        " SELECT IDCATEGORIACARTADIPAGAMENTO" & _
        " FROM TAR_CATEGORIACARTADIPAGAMENTO" & _
        " WHERE IDTARIFFA = " & idRecordSelezionato & _
        " ) TC" & _
        " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND P.IDORGANIZZAZIONE = OG.IDORGANIZZAZIONE" & _
        " AND OG.IDGENERECARTADIPAGAMENTO = C.IDGENERECARTADIPAGAMENTO" & _
        " AND C.IDCATEGORIACARTADIPAGAMENTO = TC.IDCATEGORIACARTADIPAGAMENTO(+)" & _
        " ORDER BY C.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstCategorieCarteDiPagamento.AddItem rec("NOME")
            lstCategorieCarteDiPagamento.ItemData(i - 1) = rec("IDCATEGORIACARTADIPAGAMENTO")
            If (rec("ABIL") = 1) Then
                lstCategorieCarteDiPagamento.Selected(i - 1) = True
            End If
            
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD
           
    internalEvent = internalEventOld

End Sub

Private Sub lstTipiSupportoDigitali_Init()
    Dim internalEventOld As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call ApriConnessioneBD
    lstTipiSupportoDigitali.Clear
    
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.NOME AS ""Nome"", TS.DESCRIZIONE AS ""Descrizione"", DECODE(TTSD.IDTIPOSUPPORTO, NULL, 0, 1) ABIL" & _
        " FROM (" & _
        " SELECT DISTINCT TS.IDTIPOSUPPORTO" & _
        " FROM TIPOSUPPORTO TS, TSDIGCONSENTITOPERSUPERAREA TSDC" & _
        " WHERE TSDC.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND TSDC.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
        " UNION" & _
        " SELECT DISTINCT TS.IDTIPOSUPPORTO" & _
        " FROM TSDIGCONSENTITOPERSUPERAREA TSDCS, TSDIGCONSENTITOPERORG TSDCO, TIPOSUPPORTO TS" & _
        " WHERE TSDCS.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND TSDCS.IDTIPOSUPPORTOSIAE = TSDCO.IDTIPOSUPPORTOSIAE" & _
        " AND TSDCO.IDTIPOSUPPORTOSIAE = TS.IDTIPOSUPPORTOSIAE" & _
        " ) T," & _
        " (" & _
        " SELECT IDTIPOSUPPORTO" & _
        " FROM TARIFFA_TIPOSUPPORTODIGITALE" & _
        " WHERE IDTARIFFA = " & idRecordSelezionato & _
        " ) TTSD, TIPOSUPPORTO TS" & _
        " WHERE T.IDTIPOSUPPORTO = TTSD.IDTIPOSUPPORTO(+)" & _
        " AND T.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
        " ORDER BY TS.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstTipiSupportoDigitali.AddItem rec("NOME")
            lstTipiSupportoDigitali.ItemData(i - 1) = rec("IDTIPOSUPPORTO")
            If (rec("ABIL") = 1) Then
                lstTipiSupportoDigitali.Selected(i - 1) = True
            End If
            
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD
           
    internalEvent = internalEventOld

End Sub

Private Sub lstTariffeOrganizzazioneDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tariffaOrg As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTariffeOrganizzazioneDisponibili.Clear

    If Not (listaTariffeOrganizzazioneDisponibili Is Nothing) Then
        i = 1
        For Each tariffaOrg In listaTariffeOrganizzazioneDisponibili
            lstTariffeOrganizzazioneDisponibili.AddItem tariffaOrg.nomeElementoLista
            lstTariffeOrganizzazioneDisponibili.ItemData(i - 1) = tariffaOrg.idElementoLista
            i = i + 1
        Next tariffaOrg
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstTariffeOrganizzazioneSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tariffaOrg As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTariffeOrganizzazioneSelezionate.Clear

    If Not (listaTariffeOrganizzazioneSelezionate Is Nothing) Then
        i = 1
        For Each tariffaOrg In listaTariffeOrganizzazioneSelezionate
            lstTariffeOrganizzazioneSelezionate.AddItem tariffaOrg.nomeElementoLista
            lstTariffeOrganizzazioneSelezionate.ItemData(i - 1) = tariffaOrg.idElementoLista
            i = i + 1
        Next tariffaOrg
    End If
           
    internalEvent = internalEventOld

End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Public Sub SetTipoTariffa(tipo As TipoOrigineTariffaEnum)
    tipoTariffa = tipo
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sqlTipo As String
    Dim sqlModalit� As String
    Dim sqlAmbito As String
    Dim sqlClasseControlloAccessi As String
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    sqlTipo = "SELECT IDTIPOTARIFFA AS ID, CODICE || ' - ' || NOME AS NOME"
    sqlTipo = sqlTipo & " FROM TIPOTARIFFA"
    If idTipoTariffa = IDTIPOTARIFFA_INGRESSO_ABBONATO Then
        sqlTipo = sqlTipo & " WHERE IDTIPOTARIFFA = " & IDTIPOTARIFFA_INGRESSO_ABBONATO
    Else
        sqlTipo = sqlTipo & " WHERE IDTIPOTARIFFA > 0 AND IDTIPOTARIFFA <> " & IDTIPOTARIFFA_INGRESSO_ABBONATO
    End If
    sqlTipo = sqlTipo & " ORDER BY CODICE"
    sqlModalit� = "SELECT P.IDMODALITAEMISSIONE ID, NOME"
    sqlModalit� = sqlModalit� & " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P"
    sqlModalit� = sqlModalit� & " WHERE P.IDMODALITAEMISSIONE = M.IDMODALITAEMISSIONE"
    sqlModalit� = sqlModalit� & " AND P.IDPRODOTTO = " & idProdottoSelezionato
    sqlModalit� = sqlModalit� & " ORDER BY NOME"
    sqlAmbito = "SELECT IDAMBITOAPPLICABILITATARIFFA ID, NOME"
    sqlAmbito = sqlAmbito & " FROM AMBITOAPPLICABILITATARIFFA"
    sqlAmbito = sqlAmbito & " WHERE IDAMBITOAPPLICABILITATARIFFA > 0"
    sqlClasseControlloAccessi = "SELECT IDCLASSETARIFFACONTROLLOACC ID, NOME || '-' || COMPORTAMENTO|| '-' || TIPOLOGIATITOLI AS NOME"
    sqlClasseControlloAccessi = sqlClasseControlloAccessi & " FROM CLASSETARIFFACONTROLLOACC"
    Call CaricaValoriCombo(cmbTipoTariffa, sqlTipo, "COD")
    Call CaricaValoriCombo(cmbModalit�Emissione, sqlModalit�, "NOME")
    Call CaricaValoriCombo(cmbAmbitoApplicabilit�Tariffa, sqlAmbito, "NOME")
    Call CaricaValoriCombo(cmbClasseTariffaControlloAccessi, sqlClasseControlloAccessi, "NOME")
    Call AssegnaValoriCampi
    Call CaricaValorilstClassiPuntoVenditaDisponibili
    Call CaricaValorilstClassiPuntoVenditaSelezionati
    Call lstCategorieCarteDiPagamento_Init
    Call lstTipiSupportoDigitali_Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdTariffaOrganizzazioneSelezionata_Click()
    Call SpostaDaDisponibiliASelezionati
End Sub

Private Sub SpostaDaDisponibiliASelezionati()
    isRecordEditabile = True
    Call SpostaInLstSelezionati
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_STANDARD_ORGANIZZAZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idTariffaOrg As Long
    Dim tariffaOrg As clsElementoLista
    Dim chiaveTariffaOrg As String
    
    For i = 1 To lstTariffeOrganizzazioneDisponibili.ListCount
        If lstTariffeOrganizzazioneDisponibili.Selected(i - 1) Then
            idTariffaOrg = lstTariffeOrganizzazioneDisponibili.ItemData(i - 1)
            chiaveTariffaOrg = ChiaveId(idTariffaOrg)
            Set tariffaOrg = listaTariffeOrganizzazioneDisponibili.Item(chiaveTariffaOrg)
            Call listaTariffeOrganizzazioneSelezionate.Add(tariffaOrg, chiaveTariffaOrg)
            Call listaTariffeOrganizzazioneDisponibili.Remove(chiaveTariffaOrg)
        End If
    Next i
    Call lstTariffeOrganizzazioneDisponibili_Init
    Call lstTariffeOrganizzazioneSelezionate_Init
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaTariffeOrganizzazioneSelezionate_Click()
    Call SpostaTuttiInDisponibili
End Sub

Private Sub SpostaTuttiInDisponibili()
    Call SvuotaSelezionati
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_STANDARD_ORGANIZZAZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaTariffeOrganizzazioneDisponibili_Click()
    Call SpostaTuttiInSelezionati
End Sub

Private Sub SpostaTuttiInSelezionati()
    isRecordEditabile = True
    Call SvuotaDisponibili
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_STANDARD_ORGANIZZAZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaSelezionati()
    Dim tariffaOrg As clsElementoLista
    Dim chiaveTariffaOrg As String
    
    For Each tariffaOrg In listaTariffeOrganizzazioneSelezionate
        chiaveTariffaOrg = ChiaveId(tariffaOrg.idElementoLista)
        Call listaTariffeOrganizzazioneDisponibili.Add(tariffaOrg, chiaveTariffaOrg)
    Next tariffaOrg
    Set listaTariffeOrganizzazioneSelezionate = Nothing
    Set listaTariffeOrganizzazioneSelezionate = New Collection
    
    Call lstTariffeOrganizzazioneDisponibili_Init
    Call lstTariffeOrganizzazioneSelezionate_Init
End Sub

Private Sub dgrConfigurazioneProdottoTariffe_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    isProdottoCorrenteAdInvito = IsProdottoAdInvito(idProdottoSelezionato)
    gestioneTariffaOmaggioPermessa = prodottoPermetteGestioneTariffeOmaggio(idProdottoSelezionato)
    gestioneTariffaOmaggioPermessa = VB_FALSO
    isGestioneEccedenzeOmaggioCorrenteEvoluta = IsGestioneEccedenzeOmaggioEvoluta(idProdottoSelezionato)
    esisteTariffaIngressoAbbonato = IsDefinitaTariffaIngressoAbbonato(idTariffaIngressoAbbonato)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetTipoTariffa(TOT_NON_SPECIFICATO)
    Call CreaTabellaAppoggioClassiPuntiVendita
    Call adcConfigurazioneProdottoTariffe_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoTariffe_Init
    Call CaricaValoriLstTariffeOrganizzazioneDisponibili
    Set listaTariffeOrganizzazioneSelezionate = New Collection
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoTariffe.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoTariffe_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioClassiPuntiVendita

    Set d = adcConfigurazioneProdottoTariffe

    sql = "SELECT T.IDTARIFFA AS ""ID"", T.NOME AS ""Nome"","
    sql = sql & " T.DESCRIZIONEPOS AS ""DescrizionePOS"","
    sql = sql & " T.IDTIPOTARIFFA AS IDTT,"
    sql = sql & " T.CODICE AS ""Codice"","
    sql = sql & " TT.CODICE || ' - ' || TT.NOME AS ""Codice - Tipo"","
    sql = sql & " M.NOME ""Modalit� emissione"","
    sql = sql & " DECODE(CAMBIOUTILIZZATOREAPPLICABILE, 0, 'NO', 'SI') ""Cambio utilizzatore"","
    sql = sql & " TMP.NOME AS ""Classi punti vendita"""
    sql = sql & " FROM TARIFFA T, TIPOTARIFFA TT,"
    sql = sql & " MODALITAEMISSIONE M, " & nomeTabellaTemporanea & " TMP"
    sql = sql & " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA"
    sql = sql & " AND T.IDTARIFFA = TMP.IDTARIFFA(+)"
    sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " AND T.IDMODALITAEMISSIONE = M.IDMODALITAEMISSIONE"
'    sql = sql & " ORDER BY ""Codice"""
    sql = sql & " ORDER BY NLSSORT(""Codice"", 'NLS_SORT=italian')"
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim max As Long
    Dim i As Integer
    Dim idNuovaTariffa As Long
    Dim n As Long
    Dim tariffaOrg As clsElementoLista
    Dim classePuntoVendita As clsElementoLista
    Dim idMod As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA TARIFFA
    Select Case tipoTariffa
        Case TOT_STANDARD_ORGANIZZAZIONE
            If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
                idMod = ME_TITOLO_FRUIBILE
            ElseIf idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO Then
                idMod = ME_TITOLO_FRUIBILE
            ElseIf idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
                idMod = ME_TESSERA_NON_VALIDA_PER_ACCESSO
            End If
            For Each tariffaOrg In listaTariffeOrganizzazioneSelezionate
                idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
                sql = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, CODICE, DESCRIZIONEPOS,"
                sql = sql & " DESCRIZIONEALTERNATIVA, IDORGANIZZAZIONE, IDMODALITAEMISSIONE,"
                sql = sql & " IDPRODOTTO, IDTIPOTARIFFA)"
                sql = sql & " (SELECT "
                sql = sql & idNuovaTariffa & ","
                sql = sql & " NOME, DESCRIZIONE, CODICE, DESCRIZIONEPOS, DESCRIZIONEALTERNATIVA,"
                sql = sql & " NULL, "
                sql = sql & idMod & ","
                sql = sql & idProdottoSelezionato & ",IDTIPOTARIFFA FROM TARIFFA WHERE"
                sql = sql & " IDTARIFFA = " & tariffaOrg.idElementoLista & ")"
                SETAConnection.Execute sql, n, adCmdText
                
                sql = "INSERT INTO TARIFFA_TIPOTERMINALE"
                sql = sql & " (IDTARIFFA, IDTIPOTERMINALE, IDTASTOTIPOTERMINALE)"
                sql = sql & " VALUES "
                sql = sql & "(" & idNuovaTariffa & ", "
                sql = sql & TT_TERMINALE_AVANZATO & ", "
                sql = sql & "NULL)"
                SETAConnection.Execute sql, n, adCmdText
            
            Next tariffaOrg
        Case Else
            idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
            sql = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, CODICE, DESCRIZIONEPOS,"
            sql = sql & " DESCRIZIONEALTERNATIVA, IDORGANIZZAZIONE, IDMODALITAEMISSIONE,"
            sql = sql & " IDPRODOTTO, IDTIPOTARIFFA, IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC,"
            sql = sql & " APPLICABILEAPOSTOCONRINUNCIA,"
            sql = sql & " DATANASCITATITOLAREMINIMA, DATANASCITATITOLAREMASSIMA,"
            sql = sql & " CAMBIOUTILIZZATOREAPPLICABILE, NUMEROMASSIMOTITOLIPERACQ,"
            sql = sql & " IVAPREASSOLTAOBBLIGATORIA, TITOLOCORPORATEOBBLIGATORIO)"
            sql = sql & " VALUES ("
            sql = sql & idNuovaTariffa & ", "
            sql = sql & SqlStringValue(nomeRecordSelezionato) & ", "
            sql = sql & SqlStringValue(descrizioneRecordSelezionato) & ", "
            sql = sql & SqlStringValue(codice) & ", "
            sql = sql & SqlStringValue(descrizionePOSRecordSelezionato) & ", "
            sql = sql & SqlStringValue(descrizioneAlternativaRecordSelezionato) & ", "
            sql = sql & "NULL, "
            sql = sql & idModalit�Emissione & ", "
            sql = sql & idProdottoSelezionato & ", "
            sql = sql & idTipoTariffa & ", "
            sql = sql & idTipoAmbitoApplicabilt�Tariffa & ", "
            sql = sql & idClasseTariffaControlloAccessi & ", "
            sql = sql & applicabileAPostoConRinuncia & ", "
            sql = sql & IIf(dataNascitaTitolareMinima = dataNulla, "NULL", SqlDateValue(dataNascitaTitolareMinima)) & ", "
            sql = sql & IIf(dataNascitaTitolareMassima = dataNulla, "NULL", SqlDateValue(dataNascitaTitolareMassima)) & ", "
            sql = sql & cambioUtilizzatoreApplicabile & ", "
            sql = sql & IIf(numeroMassimoTitoliPerAcq = idNessunElementoSelezionato, "NULL", numeroMassimoTitoliPerAcq) & ", "
            sql = sql & IVAPreassoltaObbligatoria & ", "
            sql = sql & titoloCorporateObbligatorio & ")"
            SETAConnection.Execute sql, n, adCmdText

        '   INSERIMENTO IN TABELLA TARIFFA_TIPOTERMINALE
'            If Not (listaClassiPuntoVenditaSelezionati Is Nothing) Then
'                For Each tipoTerminale In listaClassiPuntoVenditaSelezionati
'                    sql = "INSERT INTO TARIFFA_TIPOTERMINALE"
'                    sql = sql & " (IDTARIFFA, IDTIPOTERMINALE, IDTASTOTIPOTERMINALE)"
'                    sql = sql & " VALUES "
'                    sql = sql & "(" & idNuovaTariffa & ", "
'                    sql = sql & tipoTerminale.idElementoLista & ", "
'                    sql = sql & IIf(tipoTerminale.idAttributoElementoLista = idNessunElementoSelezionato, "NULL", tipoTerminale.idAttributoElementoLista) & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                Next tipoTerminale
'            End If
            
        '   INSERIMENTO IN TABELLA CLASSEPV_TARIFFA
            If Not (listaClassiPuntoVenditaSelezionati Is Nothing) Then
                For Each classePuntoVendita In listaClassiPuntoVenditaSelezionati
                    sql = "INSERT INTO CLASSEPV_TARIFFA"
                    sql = sql & " (IDCLASSEPUNTOVENDITA, IDTARIFFA)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & classePuntoVendita.idElementoLista & ", "
                    sql = sql & idNuovaTariffa & ")"
                    SETAConnection.Execute sql, n, adCmdText
                Next classePuntoVendita
            End If
            
            If gestioneTariffaOmaggioPermessa And chkAggiungiTariffaOmaggio.Value = 1 Then
                Call InserisciTariffaOmaggioCollegata(idNuovaTariffa, SETAConnection)
            End If
            
        '   INSERIMENTO IN TABELLA TAR_CATEGORIACARTADIPAGAMENTO
            For i = 1 To lstCategorieCarteDiPagamento.ListCount
                If lstCategorieCarteDiPagamento.Selected(i - 1) = True Then
                    sql = "INSERT INTO TAR_CATEGORIACARTADIPAGAMENTO"
                    sql = sql & " (IDTARIFFA, IDCATEGORIACARTADIPAGAMENTO)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & idNuovaTariffa & ", "
                    sql = sql & lstCategorieCarteDiPagamento.ItemData(i - 1) & ")"
                    SETAConnection.Execute sql, n, adCmdText
                End If
            Next i
            
        '   INSERIMENTO IN TABELLA TARIFFA_TIPOSUPPORTODIGITALE
            For i = 1 To lstTipiSupportoDigitali.ListCount
                If lstTipiSupportoDigitali.Selected(i - 1) = True Then
                    sql = "INSERT INTO TARIFFA_TIPOSUPPORTODIGITALE"
                    sql = sql & " (IDTARIFFA, IDTIPOSUPPORTO)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & idNuovaTariffa & ", "
                    sql = sql & lstTipiSupportoDigitali.ItemData(i - 1) & ")"
                    SETAConnection.Execute sql, n, adCmdText
                End If
            Next i
            
    End Select
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaTariffa)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
    
    sql = "SELECT T.IDTARIFFA ID, T.NOME NOME, T.CODICE COD, T.IDTIPOTARIFFA IDTT,"
    sql = sql & " T.DESCRIZIONE DESCR, T.DESCRIZIONEALTERNATIVA DESCRALT,"
    sql = sql & " IDMODALITAEMISSIONE, T.DESCRIZIONEPOS,"
    sql = sql & " TT.NOME TIPOTAR, TT.CODICE TIPOCOD, IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC,"
    sql = sql & " APPLICABILEAPOSTOCONRINUNCIA,"
    sql = sql & " DATANASCITATITOLAREMINIMA, DATANASCITATITOLAREMASSIMA,"
    sql = sql & " CAMBIOUTILIZZATOREAPPLICABILE, NUMEROMASSIMOTITOLIPERACQ,"
    sql = sql & " IVAPREASSOLTAOBBLIGATORIA, TITOLOCORPORATEOBBLIGATORIO"
    sql = sql & " FROM TARIFFA T, TIPOTARIFFA TT"
    sql = sql & " WHERE T.IDTIPOTARIFFA = TT.IDTIPOTARIFFA"
    sql = sql & " AND T.IDTARIFFA = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCR")), "", rec("DESCR"))
        descrizioneAlternativaRecordSelezionato = IIf(IsNull(rec("DESCRALT")), "", rec("DESCRALT"))
        descrizionePOSRecordSelezionato = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        idTipoTariffa = rec("IDTT")
        codice = rec("COD")
        codiceTipoTariffa = rec("TIPOCOD")
'        validaPerAccedere = rec("FLAGACCESSO")
        idModalit�Emissione = rec("IDMODALITAEMISSIONE").Value
        idTipoAmbitoApplicabilt�Tariffa = rec("IDAMBITOAPPLICABILITATARIFFA").Value
        idClasseTariffaControlloAccessi = rec("IDCLASSETARIFFACONTROLLOACC").Value
        applicabileAPostoConRinuncia = rec("APPLICABILEAPOSTOCONRINUNCIA")
        dataNascitaTitolareMinima = IIf(IsNull(rec("DATANASCITATITOLAREMINIMA")), dataNulla, rec("DATANASCITATITOLAREMINIMA"))
        dataNascitaTitolareMassima = IIf(IsNull(rec("DATANASCITATITOLAREMASSIMA")), dataNulla, rec("DATANASCITATITOLAREMASSIMA"))
        cambioUtilizzatoreApplicabile = rec("CAMBIOUTILIZZATOREAPPLICABILE")
        numeroMassimoTitoliPerAcq = IIf(IsNull(rec("NUMEROMASSIMOTITOLIPERACQ")), idNessunElementoSelezionato, rec("NUMEROMASSIMOTITOLIPERACQ"))
        IVAPreassoltaObbligatoria = rec("IVAPREASSOLTAOBBLIGATORIA")
        titoloCorporateObbligatorio = rec("TITOLOCORPORATEOBBLIGATORIO")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDescrizioneAlternativa.Text = ""
    txtDescrizionePOS.Text = ""
    txtCodice.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizioneAlternativa.Text = descrizioneAlternativaRecordSelezionato
    txtDescrizionePOS.Text = descrizionePOSRecordSelezionato
    txtCodice.Text = codice
    chkApplicabileAPostoConRinuncia = applicabileAPostoConRinuncia
    If dataNascitaTitolareMinima <> dataNulla Then
        dtpDataNascitaTitolareMinima.Value = dataNascitaTitolareMinima
    Else
        dtpDataNascitaTitolareMinima.Value = Null
    End If
    If dataNascitaTitolareMassima <> dataNulla Then
        dtpDataNascitaTitolareMassima.Value = dataNascitaTitolareMassima
    Else
        dtpDataNascitaTitolareMassima.Value = Null
    End If
    chkCambioUtilizzatoreApplicabile = cambioUtilizzatoreApplicabile
    If numeroMassimoTitoliPerAcq <> idNessunElementoSelezionato Then
        txtNumeroMassimoTitoliPerAcq.Text = numeroMassimoTitoliPerAcq
    Else
        txtNumeroMassimoTitoliPerAcq.Text = ""
    End If
    chkIVAPreassoltaObbligatoria = IVAPreassoltaObbligatoria
    chkTitoloCorporateObbligatorio = titoloCorporateObbligatorio

    Call SelezionaElementoSuCombo(cmbTipoTariffa, idTipoTariffa)
    Call SelezionaElementoSuCombo(cmbModalit�Emissione, idModalit�Emissione)
    Call SelezionaElementoSuCombo(cmbAmbitoApplicabilit�Tariffa, idTipoAmbitoApplicabilt�Tariffa)
    Call SelezionaElementoSuCombo(cmbClasseTariffaControlloAccessi, idClasseTariffaControlloAccessi)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim classePuntoVendita As clsElementoLista
    Dim condizioniSQL As String
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   AGGIORNAMENTO TABELLA TARIFFA
    sql = "UPDATE TARIFFA SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & ","
    sql = sql & " DESCRIZIONEALTERNATIVA = " & SqlStringValue(descrizioneAlternativaRecordSelezionato) & ","
    sql = sql & " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOSRecordSelezionato) & ","
    sql = sql & " CODICE = '" & codice & "',"
    sql = sql & " IDPRODOTTO = " & idProdottoSelezionato & ","
    sql = sql & " IDMODALITAEMISSIONE = " & idModalit�Emissione & ","
    sql = sql & " IDTIPOTARIFFA = " & idTipoTariffa & ","
    sql = sql & " IDAMBITOAPPLICABILITATARIFFA = " & idTipoAmbitoApplicabilt�Tariffa & ","
    sql = sql & " IDCLASSETARIFFACONTROLLOACC = " & idClasseTariffaControlloAccessi & ","
    sql = sql & " IDORGANIZZAZIONE = NULL" & ","
    sql = sql & " APPLICABILEAPOSTOCONRINUNCIA = " & applicabileAPostoConRinuncia & ","
    sql = sql & " DATANASCITATITOLAREMINIMA = " & IIf(dataNascitaTitolareMinima = dataNulla, "NULL", SqlDateValue(dataNascitaTitolareMinima)) & ","
    sql = sql & " DATANASCITATITOLAREMASSIMA = " & IIf(dataNascitaTitolareMassima = dataNulla, "NULL", SqlDateValue(dataNascitaTitolareMassima)) & ","
    sql = sql & " CAMBIOUTILIZZATOREAPPLICABILE = " & cambioUtilizzatoreApplicabile & ","
    sql = sql & " NUMEROMASSIMOTITOLIPERACQ = " & IIf(numeroMassimoTitoliPerAcq = idNessunElementoSelezionato, "NULL", numeroMassimoTitoliPerAcq) & ","
    sql = sql & " IVAPREASSOLTAOBBLIGATORIA = " & IVAPreassoltaObbligatoria & ","
    sql = sql & " TITOLOCORPORATEOBBLIGATORIO = " & titoloCorporateObbligatorio
    sql = sql & " WHERE IDTARIFFA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
'   AGGIORNAMENTO TABELLA TARIFFA_TIPOTERMINALE
'    If Not (listaClassiPuntoVenditaSelezionati Is Nothing) Then
'            sql = "DELETE FROM TARIFFA_TIPOTERMINALE WHERE IDTARIFFA = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'        For Each tipoTerminale In listaClassiPuntoVenditaSelezionati
'            sql = "INSERT INTO TARIFFA_TIPOTERMINALE"
'            sql = sql & " (IDTARIFFA, IDTIPOTERMINALE, IDTASTOTIPOTERMINALE)"
'            sql = sql & " VALUES "
'            sql = sql & "(" & idRecordSelezionato & ", "
'            sql = sql & tipoTerminale.idElementoLista & ", "
'            sql = sql & IIf(tipoTerminale.idAttributoElementoLista = idNessunElementoSelezionato, "NULL", tipoTerminale.idAttributoElementoLista) & ")"
'            SETAConnection.Execute sql, n, adCmdText
'        Next tipoTerminale
'    End If
    
'   AGGIORNAMENTO TABELLA CLASSEPV_TARIFFA
    If Not (listaClassiPuntoVenditaSelezionati Is Nothing) Then
            sql = "DELETE FROM CLASSEPV_TARIFFA WHERE IDTARIFFA = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
        For Each classePuntoVendita In listaClassiPuntoVenditaSelezionati
            sql = "INSERT INTO CLASSEPV_TARIFFA"
            sql = sql & " (IDCLASSEPUNTOVENDITA, IDTARIFFA)"
            sql = sql & " VALUES "
            sql = sql & "(" & classePuntoVendita.idElementoLista & ", "
            sql = sql & idRecordSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next classePuntoVendita
    End If
    
'   AGGIORNAMENTO TABELLA TAR_CATEGORIACARTADIPAGAMENTO
    sql = "DELETE FROM TAR_CATEGORIACARTADIPAGAMENTO WHERE IDTARIFFA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    For i = 1 To lstCategorieCarteDiPagamento.ListCount
        If lstCategorieCarteDiPagamento.Selected(i - 1) = True Then
            sql = "INSERT INTO TAR_CATEGORIACARTADIPAGAMENTO"
            sql = sql & " (IDTARIFFA, IDCATEGORIACARTADIPAGAMENTO)"
            sql = sql & " VALUES "
            sql = sql & "(" & idRecordSelezionato & ", "
            sql = sql & lstCategorieCarteDiPagamento.ItemData(i - 1) & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
    Next i
    
'   AGGIORNAMENTO TABELLA TARIFFA_TIPOSUPPORTODIGITALE
    sql = "DELETE FROM TARIFFA_TIPOSUPPORTODIGITALE WHERE IDTARIFFA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    For i = 1 To lstTipiSupportoDigitali.ListCount
        If lstTipiSupportoDigitali.Selected(i - 1) = True Then
            sql = "INSERT INTO TARIFFA_TIPOSUPPORTODIGITALE"
            sql = sql & " (IDTARIFFA, IDTIPOSUPPORTO)"
            sql = sql & " VALUES "
            sql = sql & "(" & idRecordSelezionato & ", "
            sql = sql & lstTipiSupportoDigitali.ItemData(i - 1) & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
    Next i
    
    If gestioneTariffaOmaggioPermessa Then
        Call InserisciTariffaOmaggioCollegata(idRecordSelezionato, SETAConnection)
    End If
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazioneProdottoTariffe_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoTariffe
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 8
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Visible = False
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
    g.Columns(8).Width = (2 * dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub lstTariffeOrganizzazioneDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstTariffeOrganizzazioneDisponibili, lstTariffeOrganizzazioneDisponibili.Text)
End Sub

Private Sub lstTariffeOrganizzazioneSelezionate_Click()
    Call VisualizzaListBoxToolTip(lstTariffeOrganizzazioneSelezionate, lstTariffeOrganizzazioneSelezionate.Text)
End Sub

Private Sub lstClassiPuntoVenditaDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstClassiPuntoVenditaDisponibili, lstClassiPuntoVenditaDisponibili.Text)
End Sub

Private Sub lstClassiPuntoVenditaSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstClassiPuntoVenditaSelezionati, lstClassiPuntoVenditaSelezionati.Text)
End Sub

Private Sub txtCodice_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String
    Dim tariffa As clsElementoLista

    valoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformit� = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTARIFFA <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    codice = Trim(txtCodice.Text)
'    If IsNumeric(codice) Then
'        ValoriCampiOK = False
''        Call frmMessaggio.Visualizza("ErroreCodicetariffa")
'        Call listaNonConformit�.Add("- il valore immesso sul campo codice non deve essere numerico;")
'    End If
    If tipoTariffa = TOT_STANDARD_ORGANIZZAZIONE Then
        If Not (listaTariffeOrganizzazioneSelezionate Is Nothing) Then
            For Each tariffa In listaTariffeOrganizzazioneSelezionate
                If ViolataUnicit�("TARIFFA", "NOME = " & SqlStringValue(tariffa.nomeElementoLista), "IDPRODOTTO = " & idProdottoSelezionato, _
                    condizioneSql, "", "") Then
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(tariffa.nomeElementoLista) & _
                        " � gi� presente in DB per lo stesso Prodotto;")
                End If
            Next tariffa
        End If
    Else
        If ViolataUnicit�("TARIFFA", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDPRODOTTO = " & idProdottoSelezionato, _
            condizioneSql, "", "") Then
            valoriCampiOK = False
            Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
                " � gi� presente in DB per lo stesso Prodotto;")
        End If
        If ViolataUnicit�("TARIFFA", "CODICE = " & SqlStringValue(codice), "IDPRODOTTO = " & idProdottoSelezionato, _
            condizioneSql, "", "") Then
            valoriCampiOK = False
            Call listaNonConformit�.Add("- il valore codice = " & SqlStringValue(codice) & _
                " � gi� presente in DB per lo stesso Prodotto;")
        End If
    End If
    
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrizioneAlternativaRecordSelezionato = Trim(txtDescrizioneAlternativa.Text)
    descrizionePOSRecordSelezionato = Trim(txtDescrizionePOS.Text)
    applicabileAPostoConRinuncia = chkApplicabileAPostoConRinuncia.Value
    dataNascitaTitolareMinima = IIf(IsNull(dtpDataNascitaTitolareMinima.Value), dataNulla, FormatDateTime(dtpDataNascitaTitolareMinima, vbShortDate))
    dataNascitaTitolareMassima = IIf(IsNull(dtpDataNascitaTitolareMassima.Value), dataNulla, FormatDateTime(dtpDataNascitaTitolareMassima, vbShortDate))
    cambioUtilizzatoreApplicabile = chkCambioUtilizzatoreApplicabile.Value
    If txtNumeroMassimoTitoliPerAcq.Text <> "" Then
        numeroMassimoTitoliPerAcq = CLng(Trim(txtNumeroMassimoTitoliPerAcq.Text))
    Else
        numeroMassimoTitoliPerAcq = idNessunElementoSelezionato
    End If
    IVAPreassoltaObbligatoria = chkIVAPreassoltaObbligatoria.Value
    titoloCorporateObbligatorio = chkTitoloCorporateObbligatorio.Value
    
    If prodottoRientraDecretoSicurezza = VB_VERO Then
        If numeroMassimoTitoliPerAcq >= numeroMassimoTitoliPerAcqProdotto Then
            valoriCampiOK = False
            Call listaNonConformit�.Add("- il numero massimo di titoli per acquirente deve essere minore di quello consentito per il prodotto;")
        End If
    End If
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoCausaliProtezione.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoCausaliProtezione.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(ids As Long)
    idStagioneSelezionata = ids
End Sub

Private Function IsConfigurataAlmenoUnaTariffa() As Boolean
    Dim n As Integer
    
    n = adcConfigurazioneProdottoTariffe.Recordset.RecordCount
    If n = 0 Then
        IsConfigurataAlmenoUnaTariffa = False
    Else
        IsConfigurataAlmenoUnaTariffa = True
    End If
End Function

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(b As ValoreBooleanoEnum)
    prodottoRientraDecretoSicurezza = b
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMassimoTitoliPerAcqProdotto = n
End Sub

Private Sub GetIdTipoTariffaSelezionata()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        Call rec.Move(0)
        idTipoTariffa = rec("IDTT").Value
    Else
        idTipoTariffa = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati(idTar As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim listaTabelleCorrelate As Collection
    Dim tariffaEliminabile As Boolean
    Dim esistonoTitoliAssociati As Boolean
    Dim esistonoPrezziAssociati As Boolean
    Dim esistonoLayoutAssociati As Boolean
    Dim tabelleCorrelate As String
    Dim n As Long
    Dim idTariffaRiferimento As Long
    Dim risposta As VbMsgBoxResult

    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    
On Error GoTo errore_prodotto_tariffa_delete

    sql = "SELECT IDTARIFFA FROM TARIFFA WHERE IDTARIFFA_RIFERIMENTO = " & idTar
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            idTariffaRiferimento = rec("IDTARIFFA")
        Else
            idTariffaRiferimento = idNessunElementoSelezionato
        End If
    rec.Close
            
    Set listaTabelleCorrelate = New Collection
    tariffaEliminabile = True
    esistonoTitoliAssociati = False
    esistonoPrezziAssociati = False
    esistonoLayoutAssociati = False
    
    If idTar = idTariffaIngressoAbbonato Then
        If EsisteTitoloAssociatoATariffaIngressoAbbonato Then
            Call listaTabelleCorrelate.Add("TITOLO")
            tariffaEliminabile = False
            esisteTariffaIngressoAbbonato = True
            esistonoTitoliAssociati = True
        End If
    Else
        ' se � una tariffa di servizio bisogna togliere il prezzo sulla superarea di servizio
        If idTipoTariffa = T_SERVIZI Then
            sql = "DELETE FROM PREZZOTITOLOPRODOTTO"
            sql = sql & " WHERE IDPREZZOTITOLOPRODOTTO IN ("
            sql = sql & " SELECT IDPREZZOTITOLOPRODOTTO"
            sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, AREA A"
            sql = sql & " WHERE IDTARIFFA = " & idTar
            sql = sql & " AND PTP.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDTIPOAREA = " & TA_SUPERAREA_SERVIZIO & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
        
        If Not IsRecordEliminabile("IDTARIFFA", "PREZZOTITOLOPRODOTTO", CStr(idTar)) Then
            Call listaTabelleCorrelate.Add("PREZZOTITOLOPRODOTTO")
            tariffaEliminabile = False
            esistonoPrezziAssociati = True
        End If
    End If
    If Not IsRecordEliminabile("IDTARIFFA", "PREZZOTITOLOPRODOTTO", CStr(idTariffaRiferimento)) Then
        Call listaTabelleCorrelate.Add("PREZZOTITOLOPRODOTTO")
        tariffaEliminabile = False
        esistonoPrezziAssociati = True
    End If
    
    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTO", CStr(idTar)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
        tariffaEliminabile = False
        esistonoLayoutAssociati = True
    End If
    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTO", CStr(idTariffaRiferimento)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
        tariffaEliminabile = False
        esistonoLayoutAssociati = True
    End If
    
    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idTar)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTOCPV")
        tariffaEliminabile = False
        esistonoLayoutAssociati = True
    End If
    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idTariffaRiferimento)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTOCPV")
        tariffaEliminabile = False
        esistonoLayoutAssociati = True
    End If
    
    If tariffaEliminabile Then
        If idTar = idTariffaIngressoAbbonato Then
            sql = "DELETE FROM PREZZOTITOLOPRODOTTO WHERE IDTARIFFA = " & idTar
            SETAConnection.Execute sql, n, adCmdText
            esisteTariffaIngressoAbbonato = False
        End If
        sql = "DELETE FROM OPERATORE_TARIFFA WHERE IDTARIFFA = " & idTar
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE_TARIFFA WHERE IDTARIFFA = " & idTariffaRiferimento
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TARIFFA_TIPOTERMINALE WHERE IDTARIFFA = " & idTar
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TARIFFA_TIPOTERMINALE WHERE IDTARIFFA = " & idTariffaRiferimento
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM CLASSEPV_TARIFFA WHERE IDTARIFFA = " & idTar
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TAR_CATEGORIACARTADIPAGAMENTO WHERE IDTARIFFA = " & idTar
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TAR_CATEGORIACARTADIPAGAMENTO WHERE IDTARIFFA = " & idTariffaRiferimento
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TARIFFA_TIPOSUPPORTODIGITALE WHERE IDTARIFFA = " & idTar
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TARIFFA_TIPOSUPPORTODIGITALE WHERE IDTARIFFA = " & idTariffaRiferimento
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idTariffaRiferimento
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idTar
        SETAConnection.Execute sql, n, adCmdText
        
        SETAConnection.CommitTrans
    Else
        If Not esistonoTitoliAssociati And Not esistonoPrezziAssociati Then
        ' esistono solo layout!
            risposta = MsgBox("Esistono record collegati nella tabella UTILIZZOLAYOUTSUPPORTO collegati alla tariffa: eliminare anche questa informazione?", vbYesNo)
            If risposta = vbYes Then
                If idTar = idTariffaIngressoAbbonato Then
                    sql = "DELETE FROM PREZZOTITOLOPRODOTTO WHERE IDTARIFFA = " & idTar
                    SETAConnection.Execute sql, n, adCmdText
                    esisteTariffaIngressoAbbonato = False
                End If
                sql = "DELETE FROM OPERATORE_TARIFFA WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM OPERATORE_TARIFFA WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TARIFFA_TIPOTERMINALE WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TARIFFA_TIPOTERMINALE WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM CLASSEPV_TARIFFA WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TAR_CATEGORIACARTADIPAGAMENTO WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TAR_CATEGORIACARTADIPAGAMENTO WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TARIFFA_TIPOSUPPORTODIGITALE WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TARIFFA_TIPOSUPPORTODIGITALE WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTO WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTO WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idTariffaRiferimento
                SETAConnection.Execute sql, n, adCmdText
                sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idTar
                SETAConnection.Execute sql, n, adCmdText
                
                SETAConnection.CommitTrans
            Else
                SETAConnection.RollbackTrans
            End If
        Else
            SETAConnection.RollbackTrans
            
            tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
            Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Tariffa selezionata", tabelleCorrelate)
        End If
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub

errore_prodotto_tariffa_delete:
    SETAConnection.RollbackTrans

End Sub

Private Function EsisteTitoloAssociatoATariffaIngressoAbbonato() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numTitoli As Long
    
    Call ApriConnessioneBD
    
' perche' c'e' la condizione sul tipo stato titolo?
'    sql = "SELECT COUNT(*) CONT FROM STATOTITOLO ST, PREZZOTITOLOPRODOTTO PTP" & _
'        " WHERE ST.IDPREZZOTITOLOPRODOTTO = PTP.IDPREZZOTITOLOPRODOTTO" & _
'        " AND PTP.IDTARIFFA = " & idTariffaIngressoAbbonato & _
'        " AND IDTIPOSTATOTITOLO IN (2, 3, 4, 5, 6)"
    sql = "SELECT COUNT(*) CONT FROM STATOTITOLO ST, PREZZOTITOLOPRODOTTO PTP" & _
        " WHERE ST.IDPREZZOTITOLOPRODOTTO = PTP.IDPREZZOTITOLOPRODOTTO" & _
        " AND PTP.IDTARIFFA = " & idTariffaIngressoAbbonato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numTitoli = rec("CONT").Value
    rec.Close
    
    Call ChiudiConnessioneBD
    
    EsisteTitoloAssociatoATariffaIngressoAbbonato = (numTitoli > 0)
End Function

Private Sub SvuotaDisponibili()
    Dim tariffaOrg As clsElementoLista
    Dim chiaveTariffaOrg As String
    
    For Each tariffaOrg In listaTariffeOrganizzazioneDisponibili
        chiaveTariffaOrg = ChiaveId(tariffaOrg.idElementoLista)
        Call listaTariffeOrganizzazioneSelezionate.Add(tariffaOrg, chiaveTariffaOrg)
    Next tariffaOrg
    Set listaTariffeOrganizzazioneDisponibili = Nothing
    Set listaTariffeOrganizzazioneDisponibili = New Collection
    
    Call lstTariffeOrganizzazioneDisponibili_Init
    Call lstTariffeOrganizzazioneSelezionate_Init
End Sub

Private Function IsDefinitaTariffaIngressoAbbonato(idT As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim trovato As Boolean
    
    Call ApriConnessioneBD
    
    trovato = False
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDTARIFFA FROM TARIFFA" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDTIPOTARIFFA = " & IDTIPOTARIFFA_INGRESSO_ABBONATO & _
'            " AND (IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR IDTIPOSTATORECORD IS NULL)"
'    Else
        sql = "SELECT IDTARIFFA FROM TARIFFA" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDTIPOTARIFFA = " & IDTIPOTARIFFA_INGRESSO_ABBONATO
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            idT = rec("IDTARIFFA")
            trovato = True
        Else
            idT = idNessunElementoSelezionato
            trovato = False
        End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IsDefinitaTariffaIngressoAbbonato = trovato
End Function

Private Sub cmdTipoTerminaleDisponibile_Click()
    Call SpostaInlstClassiPuntoVenditaDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdTipoTerminaleSelezionato_Click()
    Call TipoTerminaleSelezionato
End Sub

Private Sub TipoTerminaleSelezionato()
    If lstClassiPuntoVenditaDisponibili.SelCount = 1 Then
        idTipoTerminaleSelezionato = lstClassiPuntoVenditaDisponibili.ItemData(lstClassiPuntoVenditaDisponibili.ListIndex)
        nomeTipoTerminaleSelezionato = lstClassiPuntoVenditaDisponibili.Text
        If idTipoTerminaleSelezionato = TT_TERMINALE_LOTTO Then
            Call CaricaFormDettagliTastoTipoTerminale
            If frmDettagliTastoTipoTerminale.GetExitCode = EC_CONFERMA Then
                codiceTastoTipoTerminaleSelezionato = frmDettagliTastoTipoTerminale.GetCodiceTastoTipoTerminaleSelezionato
                idTastoTipoTerminaleSelezionato = frmDettagliTastoTipoTerminale.GetIdTastoTipoTerminaleSelezionato
                Call SpostaInlstClassiPuntoVenditaSelezionati
            End If
        Else
            idTastoTipoTerminaleSelezionato = idNessunElementoSelezionato
            codiceTastoTipoTerminaleSelezionato = ""
            Call SpostaInlstClassiPuntoVenditaSelezionati
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormDettagliTastoTipoTerminale()
    Call frmDettagliTastoTipoTerminale.SetIdTariffaSelezionata(idRecordSelezionato)
    Call frmDettagliTastoTipoTerminale.SetIdTipoTerminaleSelezionato(idTipoTerminaleSelezionato)
    Call frmDettagliTastoTipoTerminale.SetNomeTipoTerminaleSelezionato(nomeTipoTerminaleSelezionato)
    Call frmDettagliTastoTipoTerminale.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmDettagliTastoTipoTerminale.SetRateoProdottoSelezionato(rateo)
    Call frmDettagliTastoTipoTerminale.Init
End Sub

Private Sub SpostaInlstClassiPuntoVenditaDisponibili()
    Dim i As Integer
    Dim idTipoTerminale As Long
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For i = 1 To lstClassiPuntoVenditaSelezionati.ListCount
        If lstClassiPuntoVenditaSelezionati.Selected(i - 1) Then
            idTipoTerminale = lstClassiPuntoVenditaSelezionati.ItemData(i - 1)
            chiaveTipoTerminale = ChiaveId(idTipoTerminale)
            Set tipoTerminale = listaClassiPuntoVenditaSelezionati.Item(chiaveTipoTerminale)
            Call listaClassiPuntoVenditaDisponibili.Add(tipoTerminale, chiaveTipoTerminale)
            Call listaClassiPuntoVenditaSelezionati.Remove(chiaveTipoTerminale)
        End If
    Next i
    Call lstClassiPuntoVenditaDisponibili_Init
    Call lstClassiPuntoVenditaSelezionati_Init
End Sub

Private Sub lstClassiPuntoVenditaDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim classePuntoVendita As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstClassiPuntoVenditaDisponibili.Clear

    If Not (listaClassiPuntoVenditaDisponibili Is Nothing) Then
        i = 1
        For Each classePuntoVendita In listaClassiPuntoVenditaDisponibili
            lstClassiPuntoVenditaDisponibili.AddItem classePuntoVendita.descrizioneElementoLista
            lstClassiPuntoVenditaDisponibili.ItemData(i - 1) = classePuntoVendita.idElementoLista
            i = i + 1
        Next classePuntoVendita
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstClassiPuntoVenditaSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim label As String
    Dim classePuntoVendita As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstClassiPuntoVenditaSelezionati.Clear

    If Not (listaClassiPuntoVenditaSelezionati Is Nothing) Then
        i = 1
        For Each classePuntoVendita In listaClassiPuntoVenditaSelezionati
            label = IIf(classePuntoVendita.codiceAttributoElementoLista = "", classePuntoVendita.descrizioneElementoLista, classePuntoVendita.descrizioneElementoLista & " - " & classePuntoVendita.codiceAttributoElementoLista)
            lstClassiPuntoVenditaSelezionati.AddItem label
            lstClassiPuntoVenditaSelezionati.ItemData(i - 1) = classePuntoVendita.idElementoLista
            i = i + 1
        Next classePuntoVendita
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValorilstClassiPuntoVenditaSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeClassePuntoVendita As String
    Dim idClassePuntoVendita As Long
    Dim chiaveClassePuntoVendita As String
    Dim classePuntoVenditaCorrente As clsElementoLista
    
    Set listaClassiPuntoVenditaSelezionati = New Collection
    
    If gestioneRecordGriglia <> ASG_INSERISCI_NUOVO Then
        Call ApriConnessioneBD

        sql = "SELECT CPV.IDCLASSEPUNTOVENDITA ID, CPV.ACRONIMO || ' - ' || CPV.NOME AS NOME" & _
            " FROM CLASSEPV_TARIFFA CPV_T, CLASSEPUNTOVENDITA CPV" & _
            " WHERE CPV_T.IDTARIFFA = " & idRecordSelezionato & _
            " AND CPV_T.IDCLASSEPUNTOVENDITA = CPV.IDCLASSEPUNTOVENDITA" & _
            " ORDER BY CPV.IDCLASSEPUNTOVENDITA"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set classePuntoVenditaCorrente = New clsElementoLista
                classePuntoVenditaCorrente.nomeElementoLista = rec("NOME")
                classePuntoVenditaCorrente.descrizioneElementoLista = rec("NOME")
                classePuntoVenditaCorrente.idElementoLista = rec("ID").Value
                chiaveClassePuntoVendita = ChiaveId(classePuntoVenditaCorrente.idElementoLista)
                Call listaClassiPuntoVenditaSelezionati.Add(classePuntoVenditaCorrente, chiaveClassePuntoVendita)
                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

    End If

    Call lstClassiPuntoVenditaSelezionati_Init
    
End Sub

Private Sub CaricaValorilstClassiPuntoVenditaDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveClassePuntoVendita As String
    Dim classePuntoVenditaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaClassiPuntoVenditaDisponibili = New Collection

    sql = "SELECT IDCLASSEPUNTOVENDITA ID, ACRONIMO || ' - ' || NOME AS NOME" & _
        " FROM CLASSEPUNTOVENDITA " & _
        " WHERE IDCLASSEPUNTOVENDITA NOT IN" & _
        " (SELECT IDCLASSEPUNTOVENDITA" & _
        " FROM CLASSEPV_TARIFFA" & _
        " WHERE IDTARIFFA = " & idRecordSelezionato & ")" & _
        " ORDER BY IDCLASSEPUNTOVENDITA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set classePuntoVenditaCorrente = New clsElementoLista
            classePuntoVenditaCorrente.nomeElementoLista = rec("NOME")
            classePuntoVenditaCorrente.descrizioneElementoLista = rec("NOME")
            classePuntoVenditaCorrente.idElementoLista = rec("ID").Value
            chiaveClassePuntoVendita = ChiaveId(classePuntoVenditaCorrente.idElementoLista)
            Call listaClassiPuntoVenditaDisponibili.Add(classePuntoVenditaCorrente, chiaveClassePuntoVendita)
            rec.MoveNext
        Wend
    End If
    rec.Close

    Call ChiudiConnessioneBD

    Call lstClassiPuntoVenditaDisponibili_Init
        
End Sub

Private Sub cmdSvuotaTipiTerminaleSelezionati_Click()
    Call SvuotaTipiTerminaleSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaTipiTerminaleSelezionati()
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For Each tipoTerminale In listaClassiPuntoVenditaSelezionati
        chiaveTipoTerminale = ChiaveId(tipoTerminale.idElementoLista)
        Call listaClassiPuntoVenditaDisponibili.Add(tipoTerminale, chiaveTipoTerminale)
    Next tipoTerminale
    Set listaClassiPuntoVenditaSelezionati = Nothing
    Set listaClassiPuntoVenditaSelezionati = New Collection
    
    Call lstClassiPuntoVenditaDisponibili_Init
    Call lstClassiPuntoVenditaSelezionati_Init
End Sub

Private Sub SpostaInlstClassiPuntoVenditaSelezionati()
    Dim i As Integer
    Dim idTipoTerminale As Long
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For i = 1 To lstClassiPuntoVenditaDisponibili.ListCount
        If lstClassiPuntoVenditaDisponibili.Selected(i - 1) Then
            idTipoTerminale = lstClassiPuntoVenditaDisponibili.ItemData(i - 1)
            chiaveTipoTerminale = ChiaveId(idTipoTerminale)
            Set tipoTerminale = listaClassiPuntoVenditaDisponibili.Item(chiaveTipoTerminale)
            tipoTerminale.idAttributoElementoLista = idTastoTipoTerminaleSelezionato
            tipoTerminale.codiceAttributoElementoLista = codiceTastoTipoTerminaleSelezionato
            Call listaClassiPuntoVenditaSelezionati.Add(tipoTerminale, chiaveTipoTerminale)
            Call listaClassiPuntoVenditaDisponibili.Remove(chiaveTipoTerminale)
        End If
    Next i
    Call lstClassiPuntoVenditaDisponibili_Init
    Call lstClassiPuntoVenditaSelezionati_Init
End Sub

Private Sub CreaTabellaAppoggioClassiPuntiVendita()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_CPV_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & " (IDTARIFFA NUMBER(10), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggioClassiPuntiVendita
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioClassiPuntiVendita()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdClasseProdottoSelezionata(idCP As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idCP
End Sub

Private Sub PopolaTabellaAppoggioClassiPuntiVendita()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idTariffa As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioClassiPuntiVendita = New Collection
    
    sql = "SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDTARIFFA").Value
            Call listaAppoggioClassiPuntiVendita.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioClassiPuntiVendita
        campoNome = ""
        sql = "SELECT CPV.IDCLASSEPUNTOVENDITA ID, CPV.ACRONIMO" & _
            " FROM CLASSEPV_TARIFFA CPV_T, CLASSEPUNTOVENDITA CPV" & _
            " WHERE CPV_T.IDCLASSEPUNTOVENDITA = CPV.IDCLASSEPUNTOVENDITA" & _
            " AND CPV_T.IDTARIFFA = " & recordTemporaneo.idElementoLista & _
            " ORDER BY CPV.IDCLASSEPUNTOVENDITA"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("ACRONIMO"), campoNome & "; " & rec("ACRONIMO"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati s�
    For Each recordTemporaneo In listaAppoggioClassiPuntiVendita
        idTariffa = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & _
            " VALUES (" & idTariffa & ", " & _
            SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdClassePuntoVenditaSelezionato_Click()
    Call ClassePuntoVenditaSelezionato
End Sub

Private Sub ClassePuntoVenditaSelezionato()
'    If lstClassiPuntoVenditaDisponibili.SelCount = 1 Then
'        idClassePuntoVenditaSelezionato = lstClassiPuntoVenditaDisponibili.ItemData(lstClassiPuntoVenditaDisponibili.ListIndex)
'        Call SpostaInlstClassiPuntoVenditaSelezionati
'    End If
    Dim i As Long
    
    If lstClassiPuntoVenditaDisponibili.ListCount > 0 Then
        Call SpostaInlstClassiPuntoVenditaSelezionati
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdClassePuntoVenditaDisponibile_Click()
    Call SpostaInlstClassiPuntoVenditaDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaClassiPuntoVenditaSelezionati_Click()
    Call SvuotaClassiPuntoVenditaSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaClassiPuntoVenditaSelezionati()
    Dim classePuntoVendita As clsElementoLista
    Dim chiaveClassePuntoVendita As String
    
    For Each classePuntoVendita In listaClassiPuntoVenditaSelezionati
        chiaveClassePuntoVendita = ChiaveId(classePuntoVendita.idElementoLista)
        Call listaClassiPuntoVenditaDisponibili.Add(classePuntoVendita, chiaveClassePuntoVendita)
    Next classePuntoVendita
    Set listaClassiPuntoVenditaSelezionati = Nothing
    Set listaClassiPuntoVenditaSelezionati = New Collection
    
    Call lstClassiPuntoVenditaDisponibili_Init
    Call lstClassiPuntoVenditaSelezionati_Init
End Sub

