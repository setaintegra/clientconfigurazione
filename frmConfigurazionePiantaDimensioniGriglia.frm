VERSION 5.00
Begin VB.Form frmConfigurazionePiantaDimensioniGrigliaPosti 
   Caption         =   "Dimensioni Griglia Posti"
   ClientHeight    =   2970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5595
   LinkTopic       =   "Form1"
   ScaleHeight     =   2970
   ScaleWidth      =   5595
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   5
      Top             =   1920
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtMassimaCoordinataVerticale 
      Height          =   405
      Left            =   3000
      TabIndex        =   4
      Top             =   1080
      Width           =   975
   End
   Begin VB.TextBox txtMassimaCoordinataOrizzontale 
      Height          =   405
      Left            =   3000
      TabIndex        =   2
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Massima coordinata verticale"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   2655
   End
   Begin VB.Label Label1 
      Caption         =   "Massima coordinata orizzontale"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   2655
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle dimensioni griglie posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7935
   End
End
Attribute VB_Name = "frmConfigurazionePiantaDimensioniGrigliaPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private tipoGriglia As TipoGrigliaEnum
Private idGrigliaPostiSelezionata As Long
Private massimaCoordinataOrizzontale As Long
Private massimaCoordinataVerticale As Long
Private dimensioneOrizzontalePosto As Long
Private dimensioneVerticalePosto As Long

Public Sub SetTipoGriglia(valore As TipoGrigliaEnum)
    tipoGriglia = valore
End Sub

Public Sub SetIdGrigliaPostiSelezionata(id As Long)
    idGrigliaPostiSelezionata = id
End Sub

Public Sub SetMassimaCoordinataOrizzontale(valore As Long)
    massimaCoordinataOrizzontale = valore
End Sub

Public Sub SetMassimaCoordinataVerticale(valore As Long)
    massimaCoordinataVerticale = valore
End Sub

Public Sub SetDimensioneOrizzontalePosto(valore As Long)
    dimensioneOrizzontalePosto = valore
End Sub

Public Sub SetDimensioneVerticalePosto(valore As Long)
    dimensioneVerticalePosto = valore
End Sub
Public Sub Init()
    txtMassimaCoordinataOrizzontale = massimaCoordinataOrizzontale
    txtMassimaCoordinataVerticale = massimaCoordinataVerticale
    Call Me.Show(vbModal)
End Sub

Private Sub cmdAnnulla_Click()
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Conferma
End Sub

Private Sub Conferma()
    Dim n As Long
    Dim sql As String
    
    If (massimaCoordinataOrizzontale <> CLng(txtMassimaCoordinataOrizzontale.Text) Or _
        massimaCoordinataVerticale <> CLng(txtMassimaCoordinataVerticale.Text)) Then
        If NuoveDimensioniGrigliaSonoCongruenti Then
        
            Call ApriConnessioneBD_ORA
            Call ORADB.BeginTrans
            
            sql = "UPDATE GRIGLIAPOSTI SET" & _
                " MASSIMACOORDINATAORIZZONTALE = " & CLng(txtMassimaCoordinataOrizzontale.Text) & _
                ", MASSIMACOORDINATAVERTICALE = " & CLng(txtMassimaCoordinataVerticale.Text) & _
                " WHERE IDGRIGLIAPOSTI = " & idGrigliaPostiSelezionata
            n = ORADB.ExecuteSQL(sql)
            If n = 1 Then
                Call ORADB.CommitTrans
            Else
                Call ORADB.Rollback
            End If
            Call ChiudiConnessioneBD_ORA
            
            Unload Me
        Else
            MsgBox "Dimensioni non accettabili"
        End If
        
    End If

End Sub

Private Function NuoveDimensioniGrigliaSonoCongruenti() As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim massimaCoordinataOrizzontaleDeiPosti As Long
    Dim massimaCoordinataVerticaleDeiPosti As Long
    
    NuoveDimensioniGrigliaSonoCongruenti = True
    
    Call ApriConnessioneBD_ORA
    
    If tipoGriglia = TG_GRANDI_IMPIANTI Then
        sql = "SELECT MAX(COORDINATAORIZZONTALE) MAX_O, MAX(COORDINATAVERTICALE) MAX_V" & _
            " FROM GRIGLIAPOSTI G, POSTO P " & _
            " WHERE G.IDGRIGLIAPOSTI = " & idGrigliaPostiSelezionata & _
            " AND G.IDAREA = P.IDAREA"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                massimaCoordinataOrizzontaleDeiPosti = IIf(IsNull(rec("MAX_O")), 0, rec("MAX_O"))
                massimaCoordinataVerticaleDeiPosti = IIf(IsNull(rec("MAX_V")), 0, rec("MAX_V"))
                rec.MoveNext
            Wend
        End If
        rec.Close
    Else
        sql = "SELECT MAX(COORDINATAORIZZONTALE) MAX_O, MAX(COORDINATAVERTICALE) MAX_V" & _
            " FROM GRIGLIAPOSTI G, AREA A, POSTO P " & _
            " WHERE G.IDGRIGLIAPOSTI = " & idGrigliaPostiSelezionata & _
            " AND G.IDPIANTA = A.IDPIANTA" & _
            " AND A.IDAREA = P.IDAREA"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                massimaCoordinataOrizzontaleDeiPosti = IIf(IsNull(rec("MAX_O")), 0, rec("MAX_O"))
                massimaCoordinataVerticaleDeiPosti = IIf(IsNull(rec("MAX_V")), 0, rec("MAX_V"))
                rec.MoveNext
            Wend
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD_ORA
    
    If CLng(txtMassimaCoordinataOrizzontale) < massimaCoordinataOrizzontaleDeiPosti + dimensioneOrizzontalePosto Then
        NuoveDimensioniGrigliaSonoCongruenti = False
    End If
    If CLng(txtMassimaCoordinataVerticale) < massimaCoordinataVerticaleDeiPosti + dimensioneVerticalePosto Then
        NuoveDimensioniGrigliaSonoCongruenti = False
    End If
    
End Function
