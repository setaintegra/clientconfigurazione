Attribute VB_Name = "FunzioniGlobali"
Option Explicit

Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public Declare Sub GetSystemTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)

Public Function SqlStringValue(valore As String) As String
    SqlStringValue = IIf(valore = VALORE_NULLO_TESTO, "NULL", "'" & StringaConApiciRaddoppiati(valore) & "'")
End Function

Public Sub ApriConnessioneBD_ORA()
    Dim i As Long
    
    i = 0
    If numeroConnessioniOracleVirtualmenteAperte = 0 Then
        Set ORASession = CreateObject("OracleInProcServer.XOraSession")
'        Set ORADB = ORASession.OpenDatabase("SETASVIL", "SETA_CC/SVILUPPOCC", 0&)
        Set ORADB = ORASession.OpenDatabase(NomeTNS, userID & "/" & passWord, 0&)
'        SETAConnection.Mode = adModeReadWrite
'        SETAConnection.CommandTimeout = TIME_OUT
        On Error GoTo gestioneErrori
        i = i + 1
'        SETAConnection.Open StringaDiConnessione
    End If
    
    numeroConnessioniOracleVirtualmenteAperte = numeroConnessioniOracleVirtualmenteAperte + 1
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreLogin", Err.Description)
    
End Sub

Public Sub ChiudiConnessioneBD_ORA()
    numeroConnessioniOracleVirtualmenteAperte = numeroConnessioniOracleVirtualmenteAperte - 1
    If numeroConnessioniOracleVirtualmenteAperte = 0 Then
'        SETAConnection.Close
        ORADB.Close
        Set ORASession = Nothing
    End If
End Sub

Public Sub ApriConnessioneBD()
    Dim i As Long
    Dim sql As String
    
    i = 0
    If numeroConnessioniVirtualmenteAperte = 0 Then
        SETAConnection.Mode = adModeReadWrite
        SETAConnection.CommandTimeout = TIME_OUT
        On Error GoTo gestioneErrori
        i = i + 1
        SETAConnection.Open StringaDiConnessione
    End If
    
    numeroConnessioniVirtualmenteAperte = numeroConnessioniVirtualmenteAperte + 1
    
'    sql = "alter session set optimizer_mode=rule"
'    SETAConnection.Execute sql, , adCmdText
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreLogin", Err.Description)
    
End Sub

Public Sub ChiudiConnessioneBD()
    numeroConnessioniVirtualmenteAperte = numeroConnessioniVirtualmenteAperte - 1
    If numeroConnessioniVirtualmenteAperte = 0 Then
        SETAConnection.Close
    End If
End Sub

Public Function SqlDateValue(dataDiRiferimento As Date) As String
    SqlDateValue = " TO_DATE('" & Format(dataDiRiferimento, "DD/MM/YYYY") & "','DD/MM/YYYY')"
End Function

Public Function SqlDateTimeValue(dataDiRiferimento As Date) As String
    SqlDateTimeValue = " TO_DATE('" & Format(dataDiRiferimento, "DD/MM/YYYY hh.mm.ss") & "','DD/MM/YYYY hh24.mi.ss')"
End Function

Public Function SqlTimeValue(oraDiRiferimento As Date) As String
    SqlTimeValue = " TO_DATE('" & Format(dataNulla + oraDiRiferimento, "DD/MM/YYYY hh.mm.ss") & "','DD/MM/YYYY hh24.mi.ss')"
End Function

Public Function SqlFormatOra(NomeCampo As String) As String
    SqlFormatOra = " TO_CHAR(" & NomeCampo & ",'HH24:MI:SS')"
End Function

Public Function SqlFormatData(NomeCampo As String) As String
    SqlFormatData = " TO_CHAR(" & NomeCampo & ",'DD/MM/YYYY')"
End Function

Public Function StringaConAccenti_old(sIn As String) As String
    Dim i As Integer
    Dim c As String
    Dim p As String
    Dim sout As String
    
    For i = 1 To Len(sIn)
        c = Mid(sIn, i, 1)
        If c = Chr(34) Or c = Chr(39) Then      'Chr(32) = " ; Chr(39) = '
            If i > 1 Then
                p = Mid(sIn, i - 1, 1)
            Else
                p = ""
            End If
            Select Case p
                Case "a", "A"
                    sout = Left(sout, i - 2)
                    sout = sout & Chr(192)
                Case "e", "E"
                    sout = Left(sout, i - 2)
                    sout = sout & Chr(200)
                    i = i + 1
                Case "i", "I"
                    sout = Left(sout, i - 2)
                    sout = sout & Chr(204)
                    i = i + 1
                Case "o", "O"
                    sout = Left(sout, i - 2)
                    sout = sout & Chr(210)
                    i = i + 1
                Case "u", "U"
                    sout = Left(sout, i - 2)
                    sout = sout & Chr(217)
                    i = i + 1
                Case Else
                    'Do Nothing
            End Select
        ElseIf c = Chr(32) Then         'Chr(32) = <spazio>
            sout = sout & Chr(95)       'Chr(95) = <underscore>
        ElseIf c = Chr(46) Then         'Chr(46) = .
            sout = sout & Chr(95)       'Chr(95) = <underscore>
        ElseIf c = Chr(38) Then         'Chr(38) = &
            sout = sout & "E"
        Else
            sout = sout & c
        End If
    Next i
    
    StringaConAccenti_old = sout
End Function

Function StringaDiConnessione() As String

'    StringaDiConnessione = "provider=" & _
'                            "MSDASQL.1;Persist Security Info=False;" & _
'                            "User ID=" & userID & ";Password=" & passWord & ";Data Source=" & dataSource & "; Extended Properties=" & _
'                            """DSN=" & dataSource & ";UID=" & userID & ";SERVER=" & server & ";"""
    StringaDiConnessione = "Provider=OraOLEDB.Oracle.1;Password=" & passWord & ";Persist Security Info=True;User ID=" & userID & ";Data Source=" & dataSource
    
End Function

Public Sub EliminaProdottoDallaBaseDati(idProdotto As Long)
    Dim sql As String
    Dim n As Long
    Dim tabelleCorrelateAProdotto As Collection
    Dim tabellaCorrelata As Variant
    
    Set tabelleCorrelateAProdotto = New Collection
    
    Call tabelleCorrelateAProdotto.Add("PREZZOTITOLOPRODOTTO")
'    Call tabelleCorrelateAProdotto.Add("PRODOTTO_RUOLODISERVIZIO")
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_CAUSALERISERVAZIONE")
    Call tabelleCorrelateAProdotto.Add("SELPOSTIMIGLIORI_CAUSPROTEZ")
    Call tabelleCorrelateAProdotto.Add("SELPOSTIMIGLIORI_MAXCONTIGUI")
    Call tabelleCorrelateAProdotto.Add("TARIFFA")
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_MODALITAEMISSIONE")
    Call tabelleCorrelateAProdotto.Add("TITOLO")
    Call tabelleCorrelateAProdotto.Add("PROTEZIONEPOSTO")
    Call tabelleCorrelateAProdotto.Add("PROTEZIONEAREA")
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_RAPPRESENTAZIONE")
    Call tabelleCorrelateAProdotto.Add("PERIODOCOMMERCIALE")
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_TIPOTERMIN_TIPOOPERAZ") ' LUIGI
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_CANALEDIVENDITA") ' LUIGI
    Call tabelleCorrelateAProdotto.Add("PROTEZIONEPOSTO") ' LUIGI
    Call tabelleCorrelateAProdotto.Add("OPERATORE_PRODOTTO") ' LUIGI
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_CHIAVEPRODOTTO")
    Call tabelleCorrelateAProdotto.Add("STAMPAAGGIUNTIVA")
    Call tabelleCorrelateAProdotto.Add("PRODOTTO_TIPOMODALITAFORNITURA")
    Call tabelleCorrelateAProdotto.Add("CLASSESUPERAREAPRODOTTO")
    Call tabelleCorrelateAProdotto.Add("UTILIZZOLAYOUTRICEVUTA")
    Call tabelleCorrelateAProdotto.Add("PRODOTTO")

    Call ApriConnessioneBD
    
    sql = "DELETE FROM TARIFFA_TIPOTERMINALE TT WHERE" & _
        " TT.IDTARIFFA IN" & _
        " (SELECT T.IDTARIFFA FROM TARIFFA T WHERE" & _
        " (T.IDPRODOTTO = " & idProdotto & "))"
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV UTS WHERE" & _
        " UTS.IDTARIFFA IN" & _
        " (SELECT T.IDTARIFFA FROM TARIFFA T WHERE" & _
        " (T.IDPRODOTTO = " & idProdotto & "))"
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM CLASSESUPERAREAPROD_SUPERAREA WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdotto & ")"
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdotto & ")"
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM PRODOTTO_RUOLO_TIPOANAGRAFICA WHERE IDPRODOTTO = " & idProdotto
    SETAConnection.Execute sql, n, adCmdText

    Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idProdotto, isProdottoBloccatoDaUtente)

    For Each tabellaCorrelata In tabelleCorrelateAProdotto
        sql = "DELETE FROM " & tabellaCorrelata & _
            " WHERE IDPRODOTTO = " & idProdotto
        SETAConnection.Execute sql, n, adCmdText
    Next tabellaCorrelata
    
    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, , "IDPRODOTTO = " & idProdotto)
    
    Call ChiudiConnessioneBD
    
End Sub

Public Function ChiaveId(id As Long) As String
    ChiaveId = "K" & CStr(id)
End Function

Public Function IdChiave(chiave As String) As Long
    IdChiave = CLng(Right$(chiave, Len(chiave) - 1))
End Function

Public Function IdChiaveMultipla(chiave As String, posizione As Integer) As Long
    Dim i As Integer
    Dim j As Integer
    Dim l As String
    Dim contatore As Integer
    Dim chiaveParziale As String
    
'    i = 1
'    chiaveParziale = ""
'    contatore = 1
'    l = Mid$(chiave, i, 1)
'    While Not contatore = posizione
'        contatore = contatore + 1
'        While Not l = "K"
'            chiaveParziale = chiaveParziale & l
'            i = i + 1
'            l = Mid$(chiave, i, 1)
'        Wend
'    Wend
'    i = 1
'    contatore = 0
'    l = Mid$(chiave, i, 1)
'    While Not contatore = posizione
'        chiaveParziale = ""
'        While Not l = "K"
'            chiaveParziale = chiaveParziale & l
'            i = i + 1
'            l = Mid$(chiave, i, 1)
'        Wend
'        contatore = contatore + 1
'        i = i + 1
'    Wend
'    IdChiaveMultipla = CLng(Right$(chiave, Len(chiave) - 1))

    contatore = 0
    For i = 1 To Len(chiave)
        l = Mid$(chiave, i, 1)
        If l = "K" Then
            contatore = contatore + 1
            If posizione = contatore Then
                chiaveParziale = ""
                j = i + 1
                l = Mid$(chiave, j, 1)
                While Not l = "K" And l <> ""
                    chiaveParziale = chiaveParziale & l
                    j = j + 1
                    l = Mid$(chiave, j, 1)
                Wend
            End If
        Else
        End If
    Next i
    
    
    
    IdChiaveMultipla = CLng(chiaveParziale)
End Function

Public Function IsInteroPositivo(campo As String) As Boolean
    Dim lunghezza As Integer
    Dim i As Integer
    Dim carattere As String
    
    lunghezza = Len(Trim(campo))
    For i = 1 To lunghezza
        carattere = Mid$(campo, i, 1)
        If Asc(carattere) < 48 And Asc(carattere) > 57 Then
            IsInteroPositivo = False
        Else
            IsInteroPositivo = True
        End If
    Next i

End Function

Public Function TutteSuperareeHannoFigli(idPianta As Long, elenco As String, conDettagli As Boolean) As Boolean
    Dim sql As String
    Dim sql1 As String
    Dim rec As New ADODB.Recordset
    Dim rec1 As New ADODB.Recordset
    Dim idSuperarea As Long
    Dim count As Long

    TutteSuperareeHannoFigli = True

    elenco = ""
    sql = "SELECT IDAREA, NOME, CODICE FROM AREA" & _
        " WHERE IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
        " AND IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idSuperarea = rec("IDAREA").Value
            sql1 = "SELECT COUNT(IDAREA) AS NUMEROFIGLI FROM AREA WHERE IDAREA_PADRE = " & idSuperarea
            rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
            count = rec1("NUMEROFIGLI").Value
            If count = 0 Then
                elenco = IIf(elenco = "", rec("CODICE") & " - " & rec("NOME"), elenco & ", " & rec("CODICE") & " - " & rec("NOME"))
                TutteSuperareeHannoFigli = False
                rec1.MoveNext
                If conDettagli = False Then
                    rec1.Close
                    rec.Close
                    Exit Function
                End If
            End If
            rec1.Close
            
            rec.MoveNext
        Wend
    End If
    rec.Close

End Function

Public Function TuttiVarchiAssegnati(idPianta As Long) As Boolean
Dim sql As String
Dim rec As New ADODB.Recordset
Dim numeroVarchiNonAssegnati As Long
Dim numeroAreeNonAssegnate As Long

' numero di varchi non assegnati a aree (numerate o non numerate) della pianta
'    sql = "SELECT COUNT(VARCO.IDVARCO) AS NUMEROVARCHINONASSEGNATI" & _
'    " FROM VARCO" & _
'    " WHERE VARCO.IDPIANTA = " & idPianta & _
'    " AND VARCO.IDVARCO NOT IN (" & _
'    " SELECT AREA_VARCO.IDVARCO" & _
'    " FROM AREA_VARCO, AREA" & _
'    " WHERE AREA.IDPIANTA = " & idPianta & _
'    " AND AREA.IDTIPOAREA <> " & TA_SUPERAREA & _
'    " AND AREA.IDAREA = AREA_VARCO.IDAREA" & _
'    ")"
    sql = "SELECT COUNT(V.IDVARCO) AS NUMEROVARCHINONASSEGNATI" & _
        " FROM VARCO V, AREA_VARCO A_V" & _
        " WHERE V.IDPIANTA = " & idPianta & _
        " AND V.IDVARCO = A_V.IDVARCO(+)" & _
        " AND A_V.IDVARCO IS NULL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numeroVarchiNonAssegnati = rec("NUMEROVARCHINONASSEGNATI").Value
    rec.Close

    If numeroVarchiNonAssegnati > 0 Then
        TuttiVarchiAssegnati = False
        Exit Function
    End If

    sql = "SELECT COUNT(AREA.IDAREA) AS NUMEROAREENONASSEGNATE" & _
        " FROM AREA A, AREA_VARCO A_V" & _
        " WHERE A.IDPIANTA = " & idPianta & _
        " AND A.IDTIPOAREA IN (" & TA_AREA_NUMERATA & ", " & TA_AREA_NON_NUMERATA & ")" & _
        " AND A.IDAREA = A_V.IDAREA(+)" & _
        " AND A_V.IDAREA IS NULL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numeroAreeNonAssegnate = rec("NUMEROAREENONASSEGNATE").Value
    rec.Close

    If numeroAreeNonAssegnate > 0 Then
        TuttiVarchiAssegnati = False
        Exit Function
    End If
    
    TuttiVarchiAssegnati = True

End Function

Public Function TutteAreeAssegnate(idPianta As Long, elencoAreeNonAssegnate As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSuperarea As Long
    Dim numeroAreeNonAssegnate As Long
    
    elencoAreeNonAssegnate = ""

    sql = "SELECT COUNT(IDAREA) AS NUMEROAREENONASSEGNATE" & _
        " FROM AREA" & _
        " WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA IN (" & TA_AREA_NUMERATA & ", " & TA_AREA_NON_NUMERATA & ")" & _
        " AND IDAREA_PADRE IS NULL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numeroAreeNonAssegnate = rec("NUMEROAREENONASSEGNATE").Value
    rec.Close

    If numeroAreeNonAssegnate > 0 Then
        TutteAreeAssegnate = False
        Exit Function
    End If
    
    TutteAreeAssegnate = True

End Function

Public Function EsistonoTutteLeGriglie(idPianta As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSuperarea As Long
    Dim numeroGrigliePiccoloImpianto As Long
    Dim numeroGriglieGrandeImpianto As Long
    Dim numeroAreeSenzaUnaGriglia As Long

    EsistonoTutteLeGriglie = True

' numero di griglie di un piccolo impianto
    sql = "SELECT COUNT(IDGRIGLIAPOSTI) AS NUMEROGRIGLIEPICCOLOIMPIANTO" & _
        " FROM GRIGLIAPOSTI" & _
        " WHERE IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numeroGrigliePiccoloImpianto = IIf(IsNull(rec("NUMEROGRIGLIEPICCOLOIMPIANTO")), 0, rec("NUMEROGRIGLIEPICCOLOIMPIANTO"))
    rec.Close

    If numeroGrigliePiccoloImpianto > 1 Then
        EsistonoTutteLeGriglie = False
        Exit Function
    End If
    
' numero di griglie di un grande impianto
    sql = "SELECT COUNT(G.IDGRIGLIAPOSTI) AS NUMEROGRIGLIEGRANDEIMPIANTO" & _
        " FROM GRIGLIAPOSTI G, AREA A" & _
        " WHERE G.IDAREA = A.IDAREA" & _
        " AND A.IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numeroGriglieGrandeImpianto = IIf(IsNull(rec("NUMEROGRIGLIEGRANDEIMPIANTO")), 0, rec("NUMEROGRIGLIEGRANDEIMPIANTO"))
    rec.Close
    
' esistono griglie per la pianta e per le aree
    If numeroGrigliePiccoloImpianto >= 1 And numeroGriglieGrandeImpianto >= 1 Then
        EsistonoTutteLeGriglie = False
        Exit Function
    End If
    
' griglia associata a ogni area
    If numeroGriglieGrandeImpianto > 0 Then
        sql = "SELECT COUNT(AREA.IDAREA) AS NUMEROAREESENZAUNAGRIGLIA" & _
            " FROM AREA" & _
            " WHERE (" & _
            " SELECT COUNT(GRIGLIAPOSTI.IDAREA) AS NUMEROGRIGLIEPERAREA " & _
            " FROM GRIGLIAPOSTI" & _
            " WHERE GRIGLIAPOSTI.IDAREA = AREA.IDAREA" & _
            " ) <> 1" & _
            " AND AREA.IDPIANTA = " & idPianta & _
            " AND AREA.IDTIPOAREA = " & TA_AREA_NUMERATA
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        numeroAreeSenzaUnaGriglia = rec("NUMEROAREESENZAUNAGRIGLIA").Value
        rec.Close
        
        If numeroAreeSenzaUnaGriglia > 0 Then
            EsistonoTutteLeGriglie = False
            Exit Function
        End If
    End If
End Function

Public Function EsisteClasseSuperareaPrincipale(idPianta As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numeroClassiSuperareaPrincipali As Long

    EsisteClasseSuperareaPrincipale = True

' numero di classesuperarea principale
    sql = "SELECT COUNT(IDCLASSESUPERAREA) AS CONT" & _
        " FROM CLASSESUPERAREA" & _
        " WHERE IDPIANTA = " & idPianta & _
        " AND CLASSEPRINCIPALE = 1"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    numeroClassiSuperareaPrincipali = rec("CONT")
    rec.Close

    If numeroClassiSuperareaPrincipali <> 1 Then
        EsisteClasseSuperareaPrincipale = False
    End If
    
End Function

Public Function TutteAreeNumerateHannoPosti(idPianta As Long, elencoAreeSenzaPosti As String) As Boolean
    Dim sql As String
    Dim sql1 As String
    Dim rec As New ADODB.Recordset
    Dim rec1 As New ADODB.Recordset
    Dim idAreaNumerata As Long
    Dim count As Long

    elencoAreeSenzaPosti = ""
    
    sql = "SELECT IDAREA FROM AREA"
    sql = sql & " WHERE IDTIPOAREA = " & TA_AREA_NUMERATA
    sql = sql & " AND IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idAreaNumerata = rec("IDAREA").Value
            
            sql1 = "SELECT COUNT(IDPOSTO) AS NUMEROPOSTI"
            sql1 = sql1 & " FROM POSTO"
            sql1 = sql1 & " WHERE POSTO.IDAREA = " & idAreaNumerata
            rec1.Open sql1, SETAConnection, adOpenForwardOnly, adLockOptimistic
'            If Not (rec1.BOF And rec1.EOF) Then
'                rec1.MoveFirst
'                While Not rec1.EOF
                    count = rec1("NUMEROPOSTI").Value
                    If count = 0 Then
                        TutteAreeNumerateHannoPosti = False
                        rec1.Close
                        rec.Close
                        Exit Function
                    End If
'                    rec1.MoveNext
'                Wend
'            End If
            rec1.Close
            
            rec.MoveNext
        Wend
    End If
    rec.Close

    TutteAreeNumerateHannoPosti = True

End Function
Public Function TutteAreeNonNumerateHannoCapienza(idPianta As Long, elencoAreeNonNumerateSenzaCapienza As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim capienza As Long
    Dim count As Long
    
    elencoAreeNonNumerateSenzaCapienza = ""

    sql = "SELECT CAPIENZA FROM AREA" & _
        " WHERE IDTIPOAREA = " & TA_AREA_NON_NUMERATA & _
        " AND IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            capienza = IIf(IsNull(rec("CAPIENZA")), 0, rec("CAPIENZA").Value)
            If capienza = 0 Then
                TutteAreeNonNumerateHannoCapienza = False
                rec.Close
                Exit Function
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close

    TutteAreeNonNumerateHannoCapienza = True

End Function
    
Public Function TuttiPostiAssegnatiAStrutturaPreferibilita(idPianta As Long, elencoAreeSenzaStrutturaPreferibilita As String, conDettagli As Boolean) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim capienza As Long
    Dim count1 As Long
    Dim count2 As Long

    elencoAreeSenzaStrutturaPreferibilita = ""
    
' prima conto i posti delle aree numerata della pianta:
    sql = "SELECT COUNT(*) AS COUNT1" & _
        " FROM POSTO P, AREA A" & _
        " WHERE A.IDPIANTA = " & idPianta & _
        " AND P.IDAREA = A.IDAREA" & _
        " AND A.IDTIPOAREA = " & TA_AREA_NUMERATA
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count1 = rec("COUNT1").Value
    rec.Close
    
' poi conto i posti della pianta che risalendo da sequenza e fascia arrivano alla stessa area
    sql = "SELECT COUNT(*) AS COUNT2" & _
        " FROM POSTO P, AREA A1, AREA A2, FASCIAPOSTI F, SEQUENZAPOSTI S" & _
        " WHERE A1.IDPIANTA = " & idPianta & _
        " AND P.IDAREA = A1.IDAREA" & _
        " AND A1.IDTIPOAREA = " & TA_AREA_NUMERATA & _
        " AND P.IDSEQUENZAPOSTI = S.IDSEQUENZAPOSTI" & _
        " AND S.IDFASCIAPOSTI = F.IDFASCIAPOSTI" & _
        " AND F.IDAREA = A2.IDAREA" & _
        " AND A1.IDAREA = A2.IDAREA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count2 = rec("COUNT2").Value
    rec.Close

' i due contatori devono essere gli stessi
    If count1 = count2 Then
        TuttiPostiAssegnatiAStrutturaPreferibilita = True
    Else
        TuttiPostiAssegnatiAStrutturaPreferibilita = False
        If conDettagli Then
            
            If count1 > count2 Then
                sql = "SELECT DISTINCT A.CODICE" & _
                    " FROM POSTO P, AREA A," & _
                    " (" & _
                    " SELECT P.IDPOSTO" & _
                    " FROM POSTO P, AREA A1, AREA A2, FASCIAPOSTI F, SEQUENZAPOSTI S" & _
                    " WHERE A1.IDPIANTA = " & idPianta & _
                    " AND P.IDAREA = A1.IDAREA" & _
                    " AND A1.IDTIPOAREA = " & TA_AREA_NUMERATA & _
                    " AND P.IDSEQUENZAPOSTI = S.IDSEQUENZAPOSTI" & _
                    " AND S.IDFASCIAPOSTI = F.IDFASCIAPOSTI" & _
                    " AND F.IDAREA = A2.IDAREA" & _
                    " AND A1.IDAREA = A2.IDAREA" & _
                    " ) P2" & _
                    " WHERE A.IDPIANTA = " & idPianta & _
                    " AND P.IDAREA = A.IDAREA" & _
                    " AND A.IDTIPOAREA = " & TA_AREA_NUMERATA & _
                    " AND P.IDPOSTO = P2.IDPOSTO(+)" & _
                    " AND P2.IDPOSTO IS NULL"
            Else ' forse non pu� succedere....
                sql = "SELECT A1.CODICE" & _
                    " FROM POSTO P, AREA A1, AREA A2, FASCIAPOSTI F, SEQUENZAPOSTI S," & _
                    " (" & _
                    " SELECT P.IDPOSTO" & _
                    " FROM POSTO P, AREA A" & _
                    " WHERE A.IDPIANTA = " & idPianta & _
                    " AND P.IDAREA = A.IDAREA" & _
                    " AND A.IDTIPOAREA = " & TA_AREA_NUMERATA & _
                    " )" & _
                    " WHERE A1.IDPIANTA = " & idPianta & _
                    " AND P.IDAREA = A1.IDAREA" & _
                    " AND A1.IDTIPOAREA = " & TA_AREA_NUMERATA & _
                    " AND P.IDSEQUENZAPOSTI = S.IDSEQUENZAPOSTI" & _
                    " AND S.IDFASCIAPOSTI = F.IDFASCIAPOSTI" & _
                    " AND F.IDAREA = A2.IDAREA" & _
                    " AND A1.IDAREA = A2.IDAREA" & _
                    " AND P.IDPOSTO = P2.IDPOSTO(+)" & _
                    " AND P2.IDPOSTO IS NULL"
            End If
            rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    elencoAreeSenzaStrutturaPreferibilita = IIf(elencoAreeSenzaStrutturaPreferibilita = "", rec("CODICE"), elencoAreeSenzaStrutturaPreferibilita & ", " & rec("CODICE"))
                    rec.MoveNext
                Wend
            End If
            rec.Close
        End If
    End If

End Function
Public Function StrutturaPreferibilitaSenzaElementiVuoti(idPianta As Long, elencoAreeConStruttureVuote As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim capienza As Long
    Dim count1 As Long
    Dim count2 As Long
    
    elencoAreeConStruttureVuote = ""

'conta le sequenze che hanno posti
    sql = "SELECT COUNT(S.IDSEQUENZAPOSTI) AS COUNT1" & _
        " FROM AREA A, FASCIAPOSTI F, SEQUENZAPOSTI S" & _
        " WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA = " & TA_AREA_NUMERATA & _
        " AND A.IDAREA = F.IDAREA" & _
        " AND F.IDFASCIAPOSTI = S.IDFASCIAPOSTI" & _
        " AND S.IDSEQUENZAPOSTI IN (SELECT DISTINCT IDSEQUENZAPOSTI FROM POSTO)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count1 = rec("COUNT1").Value
    rec.Close

'conta tutte le sequenze
    sql = "SELECT COUNT(S.IDSEQUENZAPOSTI) AS COUNT2" & _
        " FROM AREA A, FASCIAPOSTI F, SEQUENZAPOSTI S" & _
        " WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA = " & TA_AREA_NUMERATA & _
        " AND A.IDAREA = F.IDAREA" & _
        " AND F.IDFASCIAPOSTI = S.IDFASCIAPOSTI"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count2 = rec("COUNT2").Value
    rec.Close

' i due contatori devono essere gli stessi
    If count1 <> count2 Then
        StrutturaPreferibilitaSenzaElementiVuoti = False
        Exit Function
    End If

'conta le fasce che hanno sequenze
    sql = "SELECT COUNT(DISTINCT F.IDFASCIAPOSTI) AS COUNT1" & _
        " FROM AREA A, FASCIAPOSTI F, SEQUENZAPOSTI S" & _
        " WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA = " & TA_AREA_NUMERATA & _
        " AND A.IDAREA = F.IDAREA" & _
        " AND F.IDFASCIAPOSTI = S.IDFASCIAPOSTI"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count1 = rec("COUNT1").Value
    rec.Close
      
'conta tutte le fasce
    sql = "SELECT COUNT(F.IDFASCIAPOSTI) AS COUNT2" & _
        " FROM AREA A, FASCIAPOSTI F" & _
        " WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA = " & TA_AREA_NUMERATA & _
        " AND A.IDAREA = F.IDAREA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count2 = rec("COUNT2").Value
    rec.Close

' i due contatori devono essere gli stessi
    If count1 = count2 Then
        StrutturaPreferibilitaSenzaElementiVuoti = True
    Else
        StrutturaPreferibilitaSenzaElementiVuoti = False
    End If
End Function
    
Public Function IsPiantaCompleta(idPianta As Long, eseguiControlloCompleto As Boolean, verbose As Boolean, Optional conDettagli As Boolean = False) As Boolean
'Verifica che la pianta sia completa, ossia che:
'�   � associata ad almeno un venue
'�   ha una superarea di servizio
'�   ha almeno una superarea oltre a quella di servizio
'�   tutte le superaree presenti hanno figli
'�   tutti i varchi configurati sono assegnati a un'area - NON VA PIU' VERIFICATO!
'�   tutte le aree configurate sono assegnate a una superarea
'�   se si tratta di un piccolo impianto, esiste la griglia associata alla pianta
'�   se si tratta di un grande impianto, esiste una griglia per ogni area
'�   ogni area numerata ha almeno un posto
'�   ogni area non numerata ha capienza > 0 - MODIFICATO PER LE AREE A CAPIENZA ILLIMITATA!
'�   tutti i posti di un'numerata sono assegnati a una sequenza, poi a una fascia, poi all'area...
'�   non esistono fasce o sequenze vuote
'�   esiste almeno la classesuperarea principale

    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim count As Integer
    Dim nomePianta As String
    Dim capienzaIllimitata As ValoreBooleanoEnum
    Dim listaElementiIncompleti As Collection
    Dim parzialmenteCompleta As Boolean
    
    Dim elencoSuperareeSenzaFigli As String
    Dim elencoAreeNonAssegnate As String
    Dim elencoAreeSenzaPosti As String
    Dim elencoAreeNonNumerateSenzaCapienza As String
    Dim elencoAreeSenzaStrutturaPreferibilita As String
    Dim elencoAreeConStruttureVuote As String

    Set listaElementiIncompleti = New Collection
    parzialmenteCompleta = True
    
    nomePianta = ""
    sql = "SELECT NOME, CAPIENZAILLIMITATA FROM PIANTA WHERE IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            nomePianta = rec("NOME").Value
            capienzaIllimitata = IIf(rec("CAPIENZAILLIMITATA") = 0, VB_FALSO, VB_VERO)
            rec.MoveNext
        Wend
    End If
    rec.Close
    If nomePianta = "" Then
        If verbose Then
            Call listaElementiIncompleti.Add("- il nome � una stringa nulla;")
        End If
        parzialmenteCompleta = False
    End If

'�   � associata ad almeno un venue
    count = 0
    sql = "SELECT COUNT (IDVENUE) AS NUMEROVENUE FROM VENUE_PIANTA WHERE IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count = rec("NUMEROVENUE").Value
    rec.Close
    If count = 0 Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non ci sono venue associati;")
        End If
        parzialmenteCompleta = False
    End If
    
'�   ha una superarea di servizio
    count = 0
    sql = "SELECT COUNT (IDAREA) AS NUMEROSUPERAREESERVIZIO" & _
        " FROM AREA WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA = " & TA_SUPERAREA_SERVIZIO
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count = rec("NUMEROSUPERAREESERVIZIO").Value
    rec.Close
    If count <> 1 Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non c'� la superarea di servizio o ne esistono pi� di una;")
        End If
        parzialmenteCompleta = False
    End If
    
'�   ha almeno una superarea oltre a quella di servizio
    count = 0
    sql = "SELECT COUNT (IDAREA) AS NUMEROSUPERAREE" & _
        " FROM AREA WHERE IDPIANTA = " & idPianta & _
        " AND IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count = rec("NUMEROSUPERAREE").Value
    rec.Close
    If count = 0 Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non ci sono superaree;")
        End If
        parzialmenteCompleta = False
    End If
    
'�   tutte le superaree presenti hanno figli
    If TutteSuperareeHannoFigli(idPianta, elencoSuperareeSenzaFigli, conDettagli) = False Then
        If verbose Then
            If conDettagli Then
                Call listaElementiIncompleti.Add("- non tutte le superaree hanno figli: " & elencoSuperareeSenzaFigli & ";")
            Else
                Call listaElementiIncompleti.Add("- non tutte le superaree hanno figli;")
            End If
        End If
        parzialmenteCompleta = False
    End If
    
'�   tutti i varchi configurati sono assegnati a un'area (e il contrario!)
' LB20030609: QUESTO NON VA PIU' VERIFICATO
'    If TuttiVarchiAssegnati(idPianta) = False Then
'        If verbose Then
'            Call listaElementiIncompleti.Add("non tutti i varchi sono assegnati o viceversa;")
'        End If
''        IsPiantaCompleta = False
''        Exit Function
'        parzialmenteCompleta = False
'    End If
    
'�   tutte le aree configurate sono assegnate a una superarea (non di servizio)
    If TutteAreeAssegnate(idPianta, elencoAreeNonAssegnate) = False Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non tutte le aree sono assegnate a una superarea;")
        End If
        parzialmenteCompleta = False
    End If
    
'�   se si tratta di un piccolo impianto, esiste la griglia associata alla pianta
'�   se si tratta di un grande impianto, esiste una griglia per ogni area
    If EsistonoTutteLeGriglie(idPianta) = False Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non esistono le griglie di posti necessarie;")
        End If
        parzialmenteCompleta = False
    End If

'�   ogni area numerata ha almeno un posto
    If TutteAreeNumerateHannoPosti(idPianta, elencoAreeSenzaPosti) = False Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non esistono posti per tutte le aree numerate;")
        End If
        parzialmenteCompleta = False
    End If

'�   ogni area non numerata ha capienza > 0
    If capienzaIllimitata = VB_FALSO Then
        If TutteAreeNonNumerateHannoCapienza(idPianta, elencoAreeNonNumerateSenzaCapienza) = False Then
            If verbose Then
                Call listaElementiIncompleti.Add("- non tutte le aree non numerate hanno capienza maggiore di 0;")
            End If
            parzialmenteCompleta = False
        End If
    End If
    
    If eseguiControlloCompleto Then
    '�   tutti i posti di un'area numerata della pianta sono assegnati a una sequenza, poi a una fascia, poi all'area...
        If TuttiPostiAssegnatiAStrutturaPreferibilita(idPianta, elencoAreeSenzaStrutturaPreferibilita, conDettagli) = False Then
            If verbose Then
                If conDettagli Then
                    Call listaElementiIncompleti.Add("- non tutti i posti sono correttamente assegnati a una sequenza e/o la sequenza a una fascia: " & elencoAreeSenzaStrutturaPreferibilita & ";")
                Else
                    Call listaElementiIncompleti.Add("- non tutti i posti sono correttamente assegnati a una sequenza e/o la sequenza a una fascia;")
                End If
            End If
            parzialmenteCompleta = False
        End If
    
    '�   non esistono fasce o sequenze vuote
        If StrutturaPreferibilitaSenzaElementiVuoti(idPianta, elencoAreeConStruttureVuote) = False Then
            If verbose Then
                Call listaElementiIncompleti.Add("- esistono aree o fasce o sequenze vuote;")
            End If
            parzialmenteCompleta = False
        End If
    End If
    
'�   esiste almeno la classesuperarea principale
    If EsisteClasseSuperareaPrincipale(idPianta) = False Then
        If verbose Then
            Call listaElementiIncompleti.Add("- non esiste la classe superarea principale;")
        End If
        parzialmenteCompleta = False
    End If
    
    If verbose Then
        If Not parzialmenteCompleta Then
            Call frmMessaggio.Visualizza("AvvertimentoPiantaIncompleta", nomePianta, ArgomentoMessaggio(listaElementiIncompleti))
        End If
    End If
    
    IsPiantaCompleta = parzialmenteCompleta

End Function

Public Function IsFiscalitaModificabile(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim count As Long
    
    IsFiscalitaModificabile = True
    count = 0
    sql = "SELECT COUNT (IDTITOLO) AS QTATITOLI FROM TITOLO WHERE IDPRODOTTO = " & idProdotto

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count = rec("QTATITOLI").Value
    rec.Close
    If count > 0 Then
        IsFiscalitaModificabile = False
    End If

End Function

'Public Function StringaSingoliApici(sql As String) As String
'
'    StringaSingoliApici = "'" & sql & "'"
'
'End Function

Public Function ProssimoNomePosto(nomeP As String, st As Long) As String
    Dim a As String
    Dim l As Integer
    Dim parziale As Long
    Dim finale As String

    a = Mid$(Trim(UCase(nomeP)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        parziale = CLng(nomeP) + st
        finale = CStr(parziale)
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        parziale = CLng(Asc(a)) + st
        finale = Chr(parziale)
    End If
    
'    l = Len(finale)
'    Do While l < 3
'        finale = " " & finale
'        l = Len(finale)
'    Loop
    
    ProssimoNomePosto = finale

End Function

Public Function ProssimoNomeFila(nomeF As String, st As Long) As String
    Dim a As String
    Dim l As Integer
    Dim parziale As Long
    Dim finale As String

    a = Mid$(Trim(UCase(nomeF)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        parziale = CLng(nomeF) + st
        finale = CStr(parziale)
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        parziale = CLng(Asc(a)) + st
        finale = Chr(parziale)
    End If
    
'    l = Len(finale)
'    Do While l < 2
'        finale = " " & finale
'        l = Len(finale)
'    Loop
    
    ProssimoNomeFila = finale

End Function

Public Function Intero(c As Variant) As Integer
    Intero = Int(c + 0.5)
End Function

Public Function Min(val1 As Variant, val2 As Variant) As Variant
    Min = IIf(val1 < val2, val1, val2)
End Function

Public Function max(val1 As Variant, val2 As Variant) As Variant
    max = IIf(val1 > val2, val1, val2)
End Function

Public Function StringaOraMinuti(ora As String) As String
    Dim i As Integer
    Dim l As Integer
    
    l = Len(CStr(ora))
    Select Case l
        Case 0
            StringaOraMinuti = "00"
        Case 1
            StringaOraMinuti = "0" & ora
        Case 2
            StringaOraMinuti = ora
        Case 3
            StringaOraMinuti = ora
        Case 4
            StringaOraMinuti = ora
        Case 5
            StringaOraMinuti = ora
    End Select
    
End Function

Public Function ValoreCorrenteSequenza(nomeSequenza As String) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD

    sql = "SELECT " & nomeSequenza & ".CURRVAL FROM DUAL"
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    ValoreCorrenteSequenza = rec(0).Value
    rec.Close

    Call ChiudiConnessioneBD

End Function

Public Function OttieniIdentificatoreDaSequenza(nomeSequenza As String) As Long
    Dim rec As New ADODB.Recordset
    Dim sql As String

    nomeSequenza = nomeSchema & nomeSequenza
    sql = "SELECT " & nomeSequenza & ".NEXTVAL AS NEWID FROM DUAL"
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    OttieniIdentificatoreDaSequenza = rec("NEWID").Value
    rec.Close

End Function

Public Sub StampaTestoFormattato(t As String, nc As Byte, nr As Byte)
    
    't  - testo da formattare
    'nc - numero di colonne per pagina
    'nr - numero di righe per pagina
    
    Dim i As Long ' indice del carattere corrente
    Dim x As String ' carattere corrente
    
    Dim r As Byte ' riga corrente
    Dim c As Byte ' colonna corrente
    
    Dim lt As Long ' lunghezza del testo da formattare (numero di caratteri)
    Dim tf As String  ' testo formattato
    
    lt = Len(t)
    i = 1
    r = 1
    c = 1
    
    While i < lt
        x = Mid(t, i, 1)
        If x = Chr(13) Then
            i = i + 1 'skip chr(10)
            c = 1
            r = r + 1
            If r > nr Then
                Printer.Print tf
                Printer.NewPage
                r = 1
                tf = ""
            End If
        ElseIf x = " " And c > nc Then
            While x = " "
                i = i + 1
                x = Mid(t, i, 1)
            Wend
            x = Chr(13) & Chr(10) & x
            c = 1
            r = r + 1
            If r > nr Then
                Printer.Print tf
                Printer.NewPage
                r = 1
                tf = ""
            End If
        Else
            c = c + 1
        End If
        i = i + 1
        tf = tf & x
    Wend
    
    Printer.Print tf
    Printer.EndDoc

End Sub

Public Function listaDatiDuplicati(nomeTabella As String, _
                                   listaCampiValori As Collection, _
                                   Optional constr1 As Variant, _
                                   Optional constr2 As Variant, _
                                   Optional constr3 As Variant) As Collection
    Dim sql As String
    Dim nomeCampoValore As Variant
    Dim rec As New ADODB.Recordset
    Dim n As Integer
    
    Set listaDatiDuplicati = New Collection
    
    Call ApriConnessioneBD
    
    For Each nomeCampoValore In listaCampiValori
        sql = "SELECT COUNT(*) FROM " & nomeTabella & _
            " WHERE " & CStr(nomeCampoValore)
        If Not IsMissing(constr1) Then sql = sql & " AND " & constr1
        If Not IsMissing(constr2) Then sql = sql & " AND " & constr2
        If Not IsMissing(constr3) Then sql = sql & " AND " & constr3
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        n = rec("COUNT(*)")
        If n > 0 Then
            Call listaDatiDuplicati.Add(nomeCampoValore)
        End If
        rec.Close
    Next nomeCampoValore
    
    Call ChiudiConnessioneBD
    
End Function

Public Function ArgomentoMessaggio(listaC As Collection) As String
    Dim elemento As Variant
    Dim appo As String
    
    appo = ""
    For Each elemento In listaC
        appo = appo & elemento & Chr(13) & Chr(10)
    Next elemento
    ArgomentoMessaggio = appo
End Function

Public Function StringaMessaggioErroreOracle(str As String) As String
    Dim a As String
    Dim i As Integer
    Dim l As Integer
    
    i = 0
    l = Len(str)
    Do
       i = i + 1
       a = Mid$(str, i, 4)
    Loop While a <> "ORA-" And i < l
    StringaMessaggioErroreOracle = Right$(str, l - i + 1)
End Function

Public Function IsCampoOraCorretto(txtO As TextBox) As Boolean
    Dim a As String
    Dim b As String
    Dim c As String
    Dim i As Integer
    Dim l As Integer
    Dim appo As Boolean
    
    appo = True
    a = txtO.Text
    l = Len(a)
    appo = (l = 2)
    If l = 2 Then
        b = Mid$(a, 1, 1)
        If Asc(b) >= 48 And Asc(b) <= 57 Then
            c = Mid$(a, 2, 1)
            If Asc(c) >= 48 And Asc(c) <= 57 Then
                If CInt(a) >= 0 And CInt(a) <= 23 Then
                    appo = True
                Else
                    appo = False
                End If
            Else
                appo = False
            End If
        Else
            appo = False
        End If
    Else
        appo = False
    End If
    
    IsCampoOraCorretto = appo
End Function

Public Function IsCampoMinutiCorretto(txtO As TextBox) As Boolean
    Dim a As String
    Dim b As String
    Dim c As String
    Dim i As Integer
    Dim l As Integer
    Dim appo As Boolean
    
    appo = True
    a = txtO.Text
    l = Len(a)
    appo = (l = 2)
    If l = 2 Then
        b = Mid$(a, 1, 1)
        If Asc(b) >= 48 And Asc(b) <= 57 Then
            c = Mid$(a, 2, 1)
            If Asc(c) >= 48 And Asc(c) <= 57 Then
                If CInt(a) >= 0 And CInt(a) <= 59 Then
                    appo = True
                Else
                    appo = False
                End If
            Else
                appo = False
            End If
        Else
            appo = False
        End If
    Else
        appo = False
    End If
    
    IsCampoMinutiCorretto = appo
End Function

Public Function IsCampoNumericoCorretto_old(txtC As TextBox) As Boolean
    Dim a As String
    Dim b As String
    Dim i As Integer
    Dim l As Integer
    Dim appo As Boolean
    
    a = Trim(txtC.Text)
    l = Len(a)
    appo = True
    b = Mid$(a, 1, 1)
    
    If a <> "" Then
        If (Asc(b) >= 48 And Asc(b) <= 57) Or Asc(b) = 45 Then
            appo = appo And True
            For i = 2 To l
                b = Mid$(a, i, 1)
                If (Asc(b) >= 47 Or Asc(b) <= 57) Or Asc(b) = 46 Then
                    appo = appo And True
                Else
                    appo = appo And False
                End If
            Next i
        Else
            appo = appo And False
        End If
    Else
        appo = False
    End If
    IsCampoNumericoCorretto_old = appo
End Function

Public Function IsCampoNumericoCorretto(txtC As TextBox) As Boolean
    IsCampoNumericoCorretto = False
    If IsNumeric(Trim(txtC.Text)) Then IsCampoNumericoCorretto = True
End Function

Public Function IsCampoInteroCorretto(txtI As TextBox) As Boolean
    Dim a As String
    Dim b As String
    Dim i As Integer
    Dim l As Integer
    Dim appo As Boolean
    
    a = txtI.Text
    l = Len(a)
    appo = True
    b = Mid$(a, 1, 1)
    
    If (Asc(b) >= 48 And Asc(b) <= 57) Or (Asc(b) = 45) Then
        appo = True
        For i = 2 To l
            b = Mid$(a, i, 1)
            If Asc(b) >= 47 And Asc(b) <= 57 Then
                appo = appo And True
            Else
                appo = appo And False
            End If
        Next i
    Else
        appo = False
    End If
    
    IsCampoInteroCorretto = appo
End Function

Public Function IsCampoIncidenzaCorretto(txtI As TextBox) As Boolean
    Dim a As String
    Dim b As String
    Dim i As Integer
    Dim l As Integer
    Dim appo As Boolean
    
    a = txtI.Text
    l = Len(a)
    appo = True
    b = Mid$(a, 1, 1)
    
    If (Asc(b) >= 48 And Asc(b) <= 57) Or (Asc(b) = 45) Then
        appo = appo And True
        For i = 2 To l
            b = Mid$(a, i, 1)
            If Asc(b) >= 47 And Asc(b) <= 57 Then
                appo = appo And True
            Else
                appo = appo And False
            End If
        Next i
    Else
        appo = appo And False
    End If
    
    If appo = True And CInt(txtI.Text) >= 0 And CInt(txtI.Text) <= 100 Then
        IsCampoIncidenzaCorretto = True
    Else
        IsCampoIncidenzaCorretto = False
    End If
End Function

Public Function IsProdottoCompleto(idProdotto As Long, Optional verbose As Boolean) As Boolean
' Verifica che il prodotto sia completo, ossia che:
'�   rappresentazioni: deve esistere almeno una rappresentazione configurata
'�   tariffe: deve esistere almeno una tariffa configurata
'�   layout supporti: deve esistere almeno una layout configurato
'�   associazione layout supporti: tutte le aree devono avere un layout associato, in maniera diretta o ereditata; nell'interfaccia, la gerarchia di aree deve essere marcata con tutte icone verdi
'�   associazione tipi supporti: tutte le aree devono avere un tipo associato, in maniera diretta o ereditata; nell'interfaccia, la gerarchia di aree deve essere marcata con tutte icone verdi
'�   periodi commerciali: deve esistere almeno un periodo commerciale configurato
'�   prezzi: deve esistere almeno un prezzo per ogni periodo commerciale presente

Dim sql As String
Dim sql2 As String
Dim rec As New ADODB.Recordset
Dim rec2 As New ADODB.Recordset
Dim count As Integer
Dim nomeProdotto As String

    sql = "SELECT nome FROM prodotto WHERE idProdotto = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            nomeProdotto = rec("NOME").Value
            rec.MoveNext
        Wend
    End If
    rec.Close
    If nomeProdotto = "" Then
        If verbose Then
            MsgBox "Prodotto '" & nomeProdotto & "' incompleto: il nome � una stringa nulla"
        End If
        IsProdottoCompleto = False
        Exit Function
    End If

'�   tariffe: deve esistere almeno una tariffa configurata
    count = 0
    sql = "SELECT COUNT (*) AS numeroTariffe FROM tariffa WHERE idProdotto = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count = rec("numeroTariffe").Value
    rec.Close
    If count = 0 Then
        If verbose Then
            MsgBox "Prodotto '" & nomeProdotto & "' incompleto: non ci sono tariffe"
        End If
        IsProdottoCompleto = False
        Exit Function
    End If

'�   layout supporti: deve esistere almeno una layout configurato

'�   associazione layout supporti: tutte le aree devono avere un layout associato, in maniera diretta o ereditata; nell'interfaccia, la gerarchia di aree deve essere marcata con tutte icone verdi

'�   associazione tipi supporti: tutte le aree devono avere un tipo associato, in maniera diretta o ereditata; nell'interfaccia, la gerarchia di aree deve essere marcata con tutte icone verdi

'�   periodi commerciali: deve esistere almeno un periodo commerciale configurato
    count = 0
    sql = "SELECT COUNT (*) AS numeroPeriodiCommerciali FROM periodoCommerciale WHERE idProdotto = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    count = rec("numeroPeriodiCommerciali").Value
    rec.Close
    If count = 0 Then
        If verbose Then
            MsgBox "Prodotto '" & nomeProdotto & "' incompleto: non ci sono Periodi Commerciali"
        End If
        IsProdottoCompleto = False
        Exit Function
    End If

'�   prezzi: deve esistere almeno un prezzo per ogni periodo commerciale presente
    Dim idPC As Long
    Dim nomePC As String
    sql = "SELECT idPeriodoCommerciale, nome" & _
        " FROM periodoCommerciale " & _
        " WHERE idProdotto = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idPC = rec("idPeriodoCommerciale").Value
            nomePC = rec("nome").Value
            
            sql2 = "SELECT count(*) as numeroPrezzi" & _
                " FROM prezzoTitoloProdotto " & _
                " WHERE idPeriodoCommerciale = " & idPC
            rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
            count = rec2("numeroPrezzi").Value
            rec2.Close
            
            If count = 0 Then
                If verbose Then
                    MsgBox "Prodotto '" & nomeProdotto & "' incompleto: non esistono prezzi per il periodo commerciale " & nomePC
                End If
                IsProdottoCompleto = False
                Exit Function
            End If
            
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    IsProdottoCompleto = True

End Function

Public Function IsClonazioneProdottoOK(idVecchioProdotto As Long, _
                                        nomeNuovoProdotto As String, _
                                        descrizioneNuovoProdotto As String, _
                                        descrizioneAlternativaNuovoProdotto As String, _
                                        descrizionePOSNuovoProdotto As String, _
                                        codiceTerminaleLottoNuovoProdotto As String, _
                                        clonaRappresentazioni As ValoreBooleanoEnum, _
                                        idProdottoPrezzi As Long, _
                                        idProdottoClassiPuntiVendita As Long, _
                                        idProdottoCausaliProtezione As Long, _
                                        idProdottoProtezioniSuPosti As Long, _
                                        idProdottoMigliorPosto As Long, _
                                        idProdottoOperativita As Long) As Boolean

    Dim sql1 As String
    Dim sql2 As String
    Dim rec1 As New ADODB.Recordset
    Dim rec2 As New ADODB.Recordset
    Dim count As Integer
    Dim idNuovoProdotto As Long
    Dim righeAggiornate As Long
'    Dim coppiaId As clsCoppiaIdent
    Dim n As Long
    Dim url As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovoProdotto = OttieniIdentificatoreDaSequenza("SQ_PRODOTTO")
' inserimento in PRODOTTO
    sql1 = "INSERT INTO PRODOTTO(" & _
        " IDPRODOTTO, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, SIGILLOFISCALESUTITOLIVENDUTI, PROGRVENDITESUTITOLIVENDUTI, PROGRTIPOGRSUTITOLIVENDUTI," & _
        " CODICETERMINALELOTTO, IDSTAGIONE, IDCLASSEPRODOTTO, RATEO, IDPIANTASIAE, ALIQUOTAIVA, PROPAGAZIOPROTEZIONICONSENTITA, DATAINIZIOCONSEGNATITSTAMPSUCC, DATAFINECONSEGNATITSTAMPSUCC," & _
        " IDTIPOSTATOPRODOTTO, IDTIPOPRODOTTO, IDORGANIZZAZIONE, IDMANIFESTAZIONE, IDCONTRATTO," & _
        " DESCRIZIONESTAMPAVENUE, IDPIANTA, IDFORMATOSUPPORTO, RIENTRADECRETOSICUREZZA, NUMEROMASSIMOTITOLIPERACQ," & _
        " MODALITACONSTITSTAMPSUCC_ITA, MODALITACONSTITSTAMPSUCC_ING, SPEDIZIONETITOLIPERMESSA, NUMGIORNINECESSARIPERSPEDIZ," & _
        " GESTECCEDENZEOMAGGIOEVOLUTA, IDSITO, DESCRIZIONECATALOGOWEB, VISIBILESUCATALOGOWEB, VISIBILESUPORTALESPORT," & _
        " QUANTITAINGRESSIVENDIBILI, TITOLIDIGITALIABILITATI, RITIROTITOLIPERMESSO, IDLIVELLOGESTIONEANAGRAFICHE," & _
        " IDTIPOVENDITACANALEINTERNET, IDLIVELLOGESTIONECONTATTI, IDRUOLOILTUOABBONAMENTO, IDGENERESIAEPREDOMINANTE, IDCAUSALEUTILIZZOCREDITIITA, SPESACREDITOITAOBBLIGATORIA," & _
        " FISCALE, MINUTIANNULLAMENTOTITTRADIZ, MINUTIANNULLAMENTOFINEEVENTO, NUMEROMASSIMOCESSIONIRATEO" & _
        ") (SELECT " & _
        idNuovoProdotto & ", '" & StringaConApiciRaddoppiati(nomeNuovoProdotto) & "'," & IIf(IsNull(descrizioneNuovoProdotto), " NULL,", " '" & StringaConApiciRaddoppiati(descrizioneNuovoProdotto) & "',") & _
        " '" & StringaConApiciRaddoppiati(descrizioneAlternativaNuovoProdotto) & "', '" & StringaConApiciRaddoppiati(descrizionePOSNuovoProdotto) & "'," & _
        " SIGILLOFISCALESUTITOLIVENDUTI, PROGRVENDITESUTITOLIVENDUTI, PROGRTIPOGRSUTITOLIVENDUTI," & _
        " '" & StringaConApiciRaddoppiati(codiceTerminaleLottoNuovoProdotto) & "'," & _
        " IDSTAGIONE, IDCLASSEPRODOTTO, " & _
        IIf(clonaRappresentazioni = VB_FALSO, "0, ", "RATEO, ") & "IDPIANTASIAE, ALIQUOTAIVA," & _
        " PROPAGAZIOPROTEZIONICONSENTITA, DATAINIZIOCONSEGNATITSTAMPSUCC, DATAFINECONSEGNATITSTAMPSUCC," & _
        TSP_IN_CONFIGURAZIONE & ", IDTIPOPRODOTTO, IDORGANIZZAZIONE, IDMANIFESTAZIONE, IDCONTRATTO, DESCRIZIONESTAMPAVENUE, IDPIANTA," & _
        " IDFORMATOSUPPORTO, RIENTRADECRETOSICUREZZA, NUMEROMASSIMOTITOLIPERACQ, MODALITACONSTITSTAMPSUCC_ITA, MODALITACONSTITSTAMPSUCC_ING, SPEDIZIONETITOLIPERMESSA, NUMGIORNINECESSARIPERSPEDIZ, GESTECCEDENZEOMAGGIOEVOLUTA, IDSITO," & _
        " '" & StringaConApiciRaddoppiati(descrizioneNuovoProdotto) & "', VISIBILESUCATALOGOWEB, VISIBILESUPORTALESPORT," & _
        " QUANTITAINGRESSIVENDIBILI, TITOLIDIGITALIABILITATI, RITIROTITOLIPERMESSO, IDLIVELLOGESTIONEANAGRAFICHE," & _
        " IDTIPOVENDITACANALEINTERNET, IDLIVELLOGESTIONECONTATTI, IDRUOLOILTUOABBONAMENTO, IDGENERESIAEPREDOMINANTE, IDCAUSALEUTILIZZOCREDITIITA, SPESACREDITOITAOBBLIGATORIA," & _
        " FISCALE, MINUTIANNULLAMENTOTITTRADIZ, MINUTIANNULLAMENTOFINEEVENTO, NUMEROMASSIMOCESSIONIRATEO" & _
        " FROM PRODOTTO WHERE IDPRODOTTO=" & idVecchioProdotto & ")"
    SETAConnection.Execute sql1, righeAggiornate, adCmdText
    
    ' gestione dell'attributo URLVENDITASISTEMAESTERNO
    sql1 = "SELECT URLVENDITASISTEMAESTERNO FROM PRODOTTO WHERE IDPRODOTTO = " & idVecchioProdotto
    rec1.Open sql1, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            url = ""
            If Not IsNull(rec1("URLVENDITASISTEMAESTERNO")) Then
                url = rec1("URLVENDITASISTEMAESTERNO")
            End If
            If url <> "" Then
                If Mid(url, 1, 73) = "https://www.listicket.com/ticketing/acquisto/acquistoStep1.html?idEvento=" Then
                    sql2 = "UPDATE PRODOTTO SET URLVENDITASISTEMAESTERNO = 'https://www.listicket.com/ticketing/acquisto/acquistoStep1.html?idEvento=" & idNuovoProdotto & "'"
                    sql2 = sql2 & " WHERE IDPRODOTTO = " & idNuovoProdotto
                    SETAConnection.Execute sql2, righeAggiornate, adCmdText
                Else
                    sql2 = "UPDATE PRODOTTO SET URLVENDITASISTEMAESTERNO = ' & url & " '"
                    SETAConnection.Execute sql2, righeAggiornate, adCmdText
                End If
            End If
            rec1.MoveNext
        Wend
        rec1.Close
    End If
    
' inserimento in PRODOTTO_TIPOTERMIN_TIPOOPERAZ
'    sql1 = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ(IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)" & _
'        " (SELECT " & _
'        idNuovoProdotto & ", " & _
'        " IDTIPOTERMINALE, IDTIPOOPERAZIONE" & _
'        " FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO=" & idVecchioProdotto & ")"
'    SETAConnection.Execute sql1, righeAggiornate, adCmdText
    
' Classi punti vendita
    sql1 = "INSERT INTO CLASSESUPERAREAPRODOTTO (IDCLASSESUPERAREAPRODOTTO, IDCLASSESUPERAREA, IDPRODOTTO)" & _
        " SELECT SQ_CLASSESUPERAREAPRODOTTO.NEXTVAL, IDCLASSESUPERAREA, " & idNuovoProdotto & _
        " FROM CLASSESUPERAREAPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, righeAggiornate, adCmdText

    sql1 = "INSERT INTO CLASSESUPERAREAPROD_SUPERAREA (IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA)" & _
        " SELECT CSP_N.IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP_V, CLASSESUPERAREAPRODOTTO CSP_N, CLASSESUPERAREAPROD_SUPERAREA CSPS_V" & _
        " WHERE CSP_V.IDPRODOTTO = " & idVecchioProdotto & _
        " AND CSP_V.IDCLASSESUPERAREA = CSP_N.IDCLASSESUPERAREA" & _
        " AND CSP_N.IDPRODOTTO = " & idNuovoProdotto & _
        " AND CSP_V.IDCLASSESUPERAREAPRODOTTO = CSPS_V.IDCLASSESUPERAREAPRODOTTO"
    SETAConnection.Execute sql1, righeAggiornate, adCmdText

' si fanno dopo
'''''''    sql1 = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
'''''''        " SELECT CSP.IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA" & _
'''''''        " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCT, CLASSESUPERAREAPRODOTTO CSP" & _
'''''''        " WHERE CSP.IDPRODOTTO = " & idVecchioProdotto & _
'''''''        " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO"
'''''''    SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''''''
'''''''    sql1 = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
'''''''        " SELECT CSP_N.IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA" & _
'''''''        " FROM CLASSESUPERAREAPRODOTTO CSP_V, CLASSESAPROD_PUNTOVENDITA CP_V, CLASSESUPERAREAPRODOTTO CSP_N" & _
'''''''        " WHERE CSP_V.IDPRODOTTO = " & idVecchioProdotto & _
'''''''        " AND CSP_V.IDCLASSESUPERAREA = CSP_N.IDCLASSESUPERAREA" & _
'''''''        " AND CSP_N.IDPRODOTTO = " & idNuovoProdotto & _
'''''''        " AND CSP_V.IDCLASSESUPERAREAPRODOTTO = CP_V.IDCLASSESUPERAREAPRODOTTO"
'''''''    SETAConnection.Execute sql1, righeAggiornate, adCmdText

' inserimento in PRODOTTO_CANALEDIVENDITA
' Se non si inseriscono le date operazioni non vanno inizializzati i canali!
'    sql1 = "INSERT INTO PRODOTTO_CANALEDIVENDITA(" & _
'        " IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE" & _
'        ") (SELECT " & idNuovoProdotto & ", " & " IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE" & _
'        " FROM PRODOTTO_CANALEDIVENDITA WHERE IDPRODOTTO=" & idVecchioProdotto & ")"
'    SETAConnection.Execute sql1, righeAggiornate, adCmdText
    
' inserimento in STAMPAAGGIUNTIVA
    sql1 = "INSERT INTO STAMPAAGGIUNTIVA (IDSTAMPAAGGIUNTIVA, IDPRODOTTO, PROGRESSIVO, IDFORMATOSTAMPAAGGIUNTIVA, COORDINATAX, COORDINATAY, VALORE)" & _
        " SELECT " & nomeSchema & "SQ_STAMPAAGGIUNTIVA.NEXTVAL, " & idNuovoProdotto & ", " & " PROGRESSIVO, IDFORMATOSTAMPAAGGIUNTIVA, COORDINATAX, COORDINATAY, VALORE" & _
        " FROM STAMPAAGGIUNTIVA" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, righeAggiornate, adCmdText

' Ancora temporaneo: da aggiungere il flag!!!!
' inserimento in DURATARISERVAZIONE (cio� in PRODOTTO_CAUSALERISERVAZIONE)
    sql1 = "INSERT INTO PRODOTTO_CAUSALERISERVAZIONE (DURATARISERVAZIONI, DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA, IDPRODOTTO, IDCAUSALERISERVAZIONE)" & _
        " SELECT DURATARISERVAZIONI, DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA," & idNuovoProdotto & ", IDCAUSALERISERVAZIONE" & _
        " FROM PRODOTTO_CAUSALERISERVAZIONE" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, righeAggiornate, adCmdText

    If clonaRappresentazioni = VB_VERO Then
        Call IsImportazioneRappresentazioniOK(idVecchioProdotto, idNuovoProdotto)
        End If
    
' importazione di prezzi, periodi comm. e tariffe dal prodotto scelto
    If idProdottoPrezzi <> idNessunElementoSelezionato Then
        sql1 = "INSERT INTO PRODOTTO_MODALITAEMISSIONE (IDPRODOTTO, IDMODALITAEMISSIONE)" & _
            " SELECT " & idNuovoProdotto & ", " & _
            " IDMODALITAEMISSIONE" & _
            " FROM PRODOTTO_MODALITAEMISSIONE" & _
            " WHERE IDPRODOTTO = " & idVecchioProdotto
        SETAConnection.Execute sql1, righeAggiornate, adCmdText
        Call IsImportazioneTariffePeriodiPrezziOK(idNuovoProdotto, idProdottoPrezzi)
    End If
    
' importazione delle causali di protezione dal prodotto scelto
    If idProdottoCausaliProtezione <> idNessunElementoSelezionato Then
        sql1 = ""
        n = 0
        sql1 = "INSERT INTO OPERATORE_CAUSALEPROTEZIONE (IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)" & _
            " SELECT IDOPERATORE, IDCAUSALEPROTEZIONE, " & idNuovoProdotto & _
            " FROM OPERATORE_CAUSALEPROTEZIONE WHERE" & _
            " IDPRODOTTO = " & idProdottoCausaliProtezione
        SETAConnection.Execute sql1, n, adCmdText
    
        sql1 = ""
        n = 0
        sql1 = "INSERT INTO CLASSEPV_CAUSALEPROTEZIONE (IDCLASSEPUNTOVENDITA, IDCAUSALEPROTEZIONE, IDPRODOTTO)" & _
            " SELECT IDCLASSEPUNTOVENDITA, IDCAUSALEPROTEZIONE, " & idNuovoProdotto & _
            " FROM CLASSEPV_CAUSALEPROTEZIONE WHERE" & _
            " IDPRODOTTO = " & idProdottoCausaliProtezione
        SETAConnection.Execute sql1, n, adCmdText
    End If
    
' importazione delle protezioni su posto dal prodotto scelto
    If idProdottoProtezioniSuPosti <> idNessunElementoSelezionato Then
        Call IsImportazioneProtezioniSuPostiOK(idNuovoProdotto, idProdottoProtezioniSuPosti)
    End If

' importazione dei diritti dell'operatore dal prodotto scelto
    If idProdottoOperativita <> idNessunElementoSelezionato Then
        n = 0
        sql1 = "INSERT INTO OPERATORE_TIPOOPERAZIONE (IDOPERATORE," & _
            " IDTIPOOPERAZIONE, IDPRODOTTO)" & _
            " (SELECT IDOPERATORE, IDTIPOOPERAZIONE, " & _
            idNuovoProdotto & " FROM OPERATORE_TIPOOPERAZIONE WHERE" & _
            " IDPRODOTTO = " & idProdottoOperativita & ")"
        SETAConnection.Execute sql1, n, adCmdText
    End If
    
' importazione della composizione delle classi punto vendita
    If idProdottoClassiPuntiVendita <> idNessunElementoSelezionato Then
        sql1 = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
            " SELECT CSP_N.IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP_V, CLASSESAPROD_PUNTOVENDITA CP_V, CLASSESUPERAREAPRODOTTO CSP_N" & _
            " WHERE CSP_V.IDPRODOTTO = " & idVecchioProdotto & _
            " AND CSP_V.IDCLASSESUPERAREA = CSP_N.IDCLASSESUPERAREA" & _
            " AND CSP_N.IDPRODOTTO = " & idNuovoProdotto & _
            " AND CSP_V.IDCLASSESUPERAREAPRODOTTO = CP_V.IDCLASSESUPERAREAPRODOTTO"
        SETAConnection.Execute sql1, righeAggiornate, adCmdText
    End If

' importazione delle operativita'; tutte con periodo illimitato
    If idProdottoOperativita <> idNessunElementoSelezionato Then
        sql1 = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
            " SELECT CSP_N.IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, " & SqlDateTimeValue(dataInferioreMinima) & ", " & SqlDateTimeValue(dataSuperioreMassima) & _
            " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCT, CLASSESUPERAREAPRODOTTO CSP_V, CLASSESUPERAREAPRODOTTO CSP_N" & _
            " WHERE CSP_V.IDPRODOTTO = " & idVecchioProdotto & _
            " AND CSP_V.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
            " AND CSP_V.IDCLASSESUPERAREA = CSP_N.IDCLASSESUPERAREA" & _
            " AND CSP_N.IDPRODOTTO = " & idNuovoProdotto
        SETAConnection.Execute sql1, righeAggiornate, adCmdText
    End If
    
' clonazione delle causali protezione per tipo terminale dal prodotto master
    If idProdottoMigliorPosto <> idNessunElementoSelezionato Then
        n = 0
        sql1 = "INSERT INTO SELPOSTIMIGL_CAUSPROTEZ_CPV (IDPRODOTTO, IDCAUSALEPROTEZIONE, IDCLASSEPUNTOVENDITA)" & _
            " SELECT " & idNuovoProdotto & ", IDCAUSALEPROTEZIONE, IDCLASSEPUNTOVENDITA" & _
            " FROM SELPOSTIMIGL_CAUSPROTEZ_CPV WHERE" & _
            " IDPRODOTTO = " & idProdottoMigliorPosto
        SETAConnection.Execute sql1, n, adCmdText

' clonazione del numero max di posti contigui dal prodotto master
        n = 0
        sql1 = "INSERT INTO SELPOSTIMIGLIORI_MAXCONTIGUI (IDPRODOTTO, IDAREA, NUMEROMASSIMOPOSTICONTIGUI)" & _
            " SELECT " & idNuovoProdotto & ", IDAREA, NUMEROMASSIMOPOSTICONTIGUI" & _
            " FROM SELPOSTIMIGLIORI_MAXCONTIGUI WHERE" & _
            " IDPRODOTTO = " & idProdottoMigliorPosto
        SETAConnection.Execute sql1, n, adCmdText
    End If

' clonazione delle chiavi prodotto dal prodotto master
    n = 0
    sql1 = "INSERT INTO PRODOTTO_CHIAVEPRODOTTO (IDPRODOTTO, IDCHIAVEPRODOTTO)" & _
        " SELECT " & idNuovoProdotto & ", IDCHIAVEPRODOTTO" & _
        " FROM PRODOTTO_CHIAVEPRODOTTO WHERE" & _
        " IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText

' clonazione delle modalita di fornitura titoli dal prodotto master
    n = 0
    sql1 = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
        " SELECT " & idNuovoProdotto & ", IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA" & _
        " FROM PRODOTTO_TIPOMODALITAFORNITURA" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
' clonazione delle abilitazioni per supporti digitali
    n = 0
    sql1 = "INSERT INTO TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
        " SELECT IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, " & idNuovoProdotto & ", IDSUPERAREA" & _
        " FROM TSDIGCONSENTITOPERSUPERAREA" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
' clonazione le informazioni sulla obbligatorieta' della identificazione con supporti digitali
    n = 0
    sql1 = "INSERT INTO TSDIGOBBLIGOIDENTIFICAZIONE (IDPRODOTTO, IDSUPERAREA, IDCLASSEPUNTOVENDITA)" & _
        " SELECT " & idNuovoProdotto & ", IDSUPERAREA, IDCLASSEPUNTOVENDITA" & _
        " FROM TSDIGOBBLIGOIDENTIFICAZIONE" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText

' clonazione delle abilitazioni per template home ticketing
    n = 0
    sql1 = "INSERT INTO UTILIZZOTEMPLATE (IDPRODOTTO, IDSUPERAREA, IDTEMPLATE)" & _
        " SELECT " & idNuovoProdotto & ", IDSUPERAREA, IDTEMPLATE" & _
        " FROM UTILIZZOTEMPLATE" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
' clonazione dei layout per ricevute supporti digitali
    n = 0
    sql1 = "INSERT INTO UTILIZZOLAYOUTRICEVUTA (IDPRODOTTO, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
        " SELECT " & idNuovoProdotto & ", IDTIPOSUPPORTO, IDLAYOUTSUPPORTO" & _
        " FROM UTILIZZOLAYOUTRICEVUTA" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
' clonazione dei layout per spedizione email
    n = 0
    sql1 = "INSERT INTO PRODOTTO_HTLAYOUT (IDPRODOTTO, IDHTLAYOUT_ITA, IDHTLAYOUT_ING)" & _
        " SELECT " & idNuovoProdotto & ", IDHTLAYOUT_ITA, IDHTLAYOUT_ING" & _
        " FROM PRODOTTO_HTLAYOUT" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
' clonazione dei layout per layout Alfresco
    n = 0
    sql1 = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
        " SELECT " & idNuovoProdotto & ", IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI" & _
        " FROM PRODOTTO_LAYOUTALFRESCO" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
' clonazione TIPO STAMPANTE
    n = 0
    sql1 = "INSERT INTO PROD_TERM_TIPOSTAMPANTE (IDPRODOTTO, IDTERMINALE, IDTIPOSTAMPANTE)" & _
        " SELECT " & idNuovoProdotto & ", IDTERMINALE, IDTIPOSTAMPANTE" & _
        " FROM PROD_TERM_TIPOSTAMPANTE" & _
        " WHERE IDPRODOTTO = " & idVecchioProdotto
    SETAConnection.Execute sql1, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD

    Call ScriviLog(CCTA_CLONAZIONE, CCDA_PRODOTTO, CCDA_PRODOTTO, "IDPRODOTTO MASTER = " & idVecchioProdotto, idNuovoProdotto)

    IsClonazioneProdottoOK = True
    
    Exit Function
    
gestioneErrori:
    SETAConnection.RollbackTrans
    IsClonazioneProdottoOK = False
'    Call frmMessaggio.Visualizza("ClonazioneProdottoKO")
End Function

Public Function IsClonazioneOperatoreOK(idVecchioOperatore As Long, _
                                        usernameNuovoOperatore As String, _
                                        passwordNuovoOperatore As String) As Boolean

    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idNuovoOperatore As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovoOperatore = OttieniIdentificatoreDaSequenza("SQ_OPERATORE")

    'OPERATORE
    sql = "INSERT INTO OPERATORE(IDOPERATORE, USERNAME, CRYPTO_HASH, ABILITATO" & _
        ",IDPUNTOVENDITA, NUOVECOMUNICAZIONI, IDPERFEZIONATORE, DATAULTIMAMODIFICAPASSWORD" & _
        ", ABILITATOREPORTINTERNET, IDPUNTOVENDITAELETTIVO, EMAIL)" & _
        " (SELECT " & idNuovoOperatore & ", " & SqlStringValue(usernameNuovoOperatore) & _
        ", SETA.GET_HASH(" & SqlStringValue(passwordNuovoOperatore) & "), ABILITATO" & _
        ", IDPUNTOVENDITA, NUOVECOMUNICAZIONI, IDPERFEZIONATORE, SYSDATE" & _
        ", ABILITATOREPORTINTERNET, IDPUNTOVENDITAELETTIVO, EMAIL" & _
        " FROM OPERATORE" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore & ")"
    SETAConnection.Execute sql, , adCmdText
    
    'OPERATORE_CAUSALEPROTEZIONE
    sql = "INSERT INTO OPERATORE_CAUSALEPROTEZIONE(IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)" & _
        " SELECT " & idNuovoOperatore & ", IDCAUSALEPROTEZIONE, IDPRODOTTO" & _
        " FROM OPERATORE_CAUSALEPROTEZIONE" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText
    
    'OPERATORE_OFFERTAPACCHETTO
    sql = "INSERT INTO OPERATORE_OFFERTAPACCHETTO(IDOPERATORE, IDOFFERTAPACCHETTO)" & _
        " SELECT " & idNuovoOperatore & ", IDOFFERTAPACCHETTO" & _
        " FROM OPERATORE_OFFERTAPACCHETTO" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText

    'OPERATORE_ORGANIZZAZIONE
    sql = "INSERT INTO OPERATORE_ORGANIZZAZIONE(IDOPERATORE, IDORGANIZZAZIONE)" & _
        " SELECT " & idNuovoOperatore & ", IDORGANIZZAZIONE" & _
        " FROM OPERATORE_ORGANIZZAZIONE" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText

    'OPERATORE_PRODOTTO
    sql = "INSERT INTO OPERATORE_PRODOTTO(IDOPERATORE, IDPRODOTTO)" & _
        " SELECT " & idNuovoOperatore & ", IDPRODOTTO" & _
        " FROM OPERATORE_PRODOTTO" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText
    
    'OPERATORE_TARIFFA
    sql = "INSERT INTO OPERATORE_TARIFFA(IDOPERATORE, IDTARIFFA)" & _
        " SELECT " & idNuovoOperatore & ", IDTARIFFA" & _
        " FROM OPERATORE_TARIFFA" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText
    
    'OPERATORE_TIPOOPERAZIONE
    sql = "INSERT INTO OPERATORE_TIPOOPERAZIONE(IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO)" & _
        " SELECT " & idNuovoOperatore & ", IDTIPOOPERAZIONE, IDPRODOTTO" & _
        " FROM OPERATORE_TIPOOPERAZIONE" & _
        " WHERE IDOPERATORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText
    
    'OPERATORE_OPERATORE 1
    sql = "INSERT INTO OPERATORE_OPERATORE(IDOPERATORECONTROLLORE, IDOPERATORECONTROLLATO)" & _
        " SELECT " & idNuovoOperatore & ", IDOPERATORECONTROLLATO" & _
        " FROM OPERATORE_OPERATORE" & _
        " WHERE IDOPERATORECONTROLLORE = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText
    
    'OPERATORE_OPERATORE 2
    sql = "INSERT INTO OPERATORE_OPERATORE(IDOPERATORECONTROLLORE, IDOPERATORECONTROLLATO)" & _
        " SELECT IDOPERATORECONTROLLORE, " & idNuovoOperatore & _
        " FROM OPERATORE_OPERATORE" & _
        " WHERE IDOPERATORECONTROLLATO = " & idVecchioOperatore
    SETAConnection.Execute sql, , adCmdText

    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD

    Call ScriviLog(CCTA_CLONAZIONE, CCDA_OPERATORE, CCDA_OPERATORE, "IDOPERATORE MASTER = " & idVecchioOperatore, idNuovoOperatore)
    IsClonazioneOperatoreOK = True
    
    Exit Function
    
gestioneErrori:
    SETAConnection.RollbackTrans
    IsClonazioneOperatoreOK = False
End Function

Public Function IsClonazionePuntoVenditaOK(idVecchioPuntoVendita As Long, _
                                        nomeNuovo As String, _
                                        idTipoPuntoVenditaNuovo As Long, _
                                        descrizioneNuova As String, _
                                        codiceSAPNuovo As String, _
                                        indirizzoNuovo As String, _
                                        civicoNuovo As String, _
                                        capNuovo As String, _
                                        cittaNuova As String, _
                                        provinciaNuova As String, _
                                        telefonoNuovo As String, _
                                        faxNuovo As String, _
                                        responsabileNuovo As String, _
                                        eMailNuova As String) As Boolean

    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idNuovoPuntoVendita As Long
    Dim numeroRigheAggiornate As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovoPuntoVendita = OttieniIdentificatoreDaSequenza("SQ_PUNTOVENDITA")

    'PUNTOVENDITA
    sql = "INSERT INTO PUNTOVENDITA(IDPUNTOVENDITA, NOME, DESCRIZIONE, INDIRIZZO, CIVICO," & _
        " CAP, PROVINCIA, CITTA, TELEFONO, FAX, EMAIL, RESPONSABILE, CODICESAP, IDTIPOPUNTOVENDITA)" & _
        " VALUES (" & idNuovoPuntoVendita & ", '" & nomeNuovo & "', '" & descrizioneNuova & _
        "', '" & indirizzoNuovo & "', '" & civicoNuovo & "', '" & capNuovo & "', '" & provinciaNuova & _
        "', '" & cittaNuova & "', '" & telefonoNuovo & "', '" & faxNuovo & "', '" & eMailNuova & _
        "', '" & responsabileNuovo & "', '" & codiceSAPNuovo & "', " & idTipoPuntoVenditaNuovo & ")"
    SETAConnection.Execute sql, numeroRigheAggiornate, adCmdText
    
    sql = "INSERT INTO ORGANIZ_CLASSEPV_PUNTOVENDITA (IDORGANIZZAZIONE, IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA)" & _
        " SELECT IDORGANIZZAZIONE, IDCLASSEPUNTOVENDITA, " & idNuovoPuntoVendita & _
        " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
        " WHERE IDPUNTOVENDITA = " & idVecchioPuntoVendita
    SETAConnection.Execute sql, numeroRigheAggiornate, adCmdText

    sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
        " SELECT IDCLASSESUPERAREAPRODOTTO, " & idNuovoPuntoVendita & _
        " FROM CLASSESAPROD_PUNTOVENDITA" & _
        " WHERE IDPUNTOVENDITA = " & idVecchioPuntoVendita
    SETAConnection.Execute sql, numeroRigheAggiornate, adCmdText

    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD

    Call ScriviLog(CCTA_CLONAZIONE, CCDA_PUNTO_VENDITA, CCDA_PUNTO_VENDITA, "IDPUNTOVENDITA MASTER = " & idVecchioPuntoVendita, idNuovoPuntoVendita)
    IsClonazionePuntoVenditaOK = True
    
    Exit Function
    
gestioneErrori:
    SETAConnection.RollbackTrans
    IsClonazionePuntoVenditaOK = False
End Function

Public Function IsImportazioneRappresentazioniOK(idProdottoMaster As Long, idProdottoCorrente As Long) As Boolean
    Dim sql As String
    Dim sql1 As String
    Dim sql2 As String
    Dim rec As New ADODB.Recordset
    Dim idScelta As Long
    Dim idNuovaScelta As Long
    Dim nomeScelta As String
    Dim descrizioneScelta As String
    Dim n As Long
    
'INSERIMENTO IN RAPPRESENTAZIONE
    sql = "INSERT INTO PRODOTTO_RAPPRESENTAZIONE(" & _
        " IDPRODOTTO," & _
        " IDRAPPRESENTAZIONE" & _
        ") (SELECT " & _
       idProdottoCorrente & ", " & _
        " IDRAPPRESENTAZIONE" & _
        " FROM PRODOTTO_RAPPRESENTAZIONE WHERE IDPRODOTTO = " & idProdottoMaster & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        Call AggiornaParametriSessioneSuRecord("PRODOTTO_RAPPRESENTAZIONE", "IDPRODOTTO", idProdottoCorrente, TSR_NUOVO)
'    End If
    
'INSERIMENTO IN SCELTARAPPRESENTAZIONE
    sql = "SELECT IDSCELTARAPPRESENTAZIONE, NOME, DESCRIZIONE, IDPRODOTTO" & _
        " FROM SCELTARAPPRESENTAZIONE" & _
        " WHERE IDPRODOTTO = " & idProdottoMaster
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idScelta = rec("IDSCELTARAPPRESENTAZIONE").Value
            nomeScelta = IIf(IsNull(rec("NOME")), VALORE_NULLO_TESTO, rec("NOME"))
            descrizioneScelta = IIf(IsNull(rec("DESCRIZIONE")), VALORE_NULLO_TESTO, rec("DESCRIZIONE"))
            idNuovaScelta = OttieniIdentificatoreDaSequenza("SQ_SCELTARAPPRESENTAZIONE")
            sql1 = "INSERT INTO SCELTARAPPRESENTAZIONE (IDSCELTARAPPRESENTAZIONE," & _
                " NOME, DESCRIZIONE, IDPRODOTTO)" & _
                " VALUES (" & idNuovaScelta & ", " & _
                SqlStringValue(nomeScelta) & ", " & SqlStringValue(descrizioneScelta) & ", " & _
                idProdottoCorrente & ")"
            SETAConnection.Execute sql1, n, adCmdText
'            If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'                Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE", "IDPRODOTTO", idProdottoCorrente, TSR_NUOVO)
'            End If
'           INSERIMENTO IN SCELTARAPPRESENTAZIONE_RAPPR
            sql2 = "INSERT INTO SCELTARAPPRESENTAZIONE_RAPPR (IDSCELTARAPPRESENTAZIONE," & _
                " IDRAPPRESENTAZIONE)" & _
                " SELECT " & idNuovaScelta & ", IDRAPPRESENTAZIONE" & _
                " FROM SCELTARAPPRESENTAZIONE_RAPPR" & _
                " WHERE IDSCELTARAPPRESENTAZIONE = " & idScelta
            SETAConnection.Execute sql2, n, adCmdText
'            If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'                Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idNuovaScelta, TSR_NUOVO)
'            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Function

Public Function IsImportazioneProtezioniSuPostiOK(idNuovoProdotto As Long, idProdottoProtezioniSuPosti As Long) As Boolean
    Dim sql As String
    Dim n As Long
    
    n = 0
    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO," & _
        " IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
        " (SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, IDPOSTO, " & _
        idNuovoProdotto & ", IDCAUSALEPROTEZIONE" & _
        " FROM PROTEZIONEPOSTO WHERE" & _
        " IDPRODOTTO = " & idProdottoProtezioniSuPosti & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        Call AggiornaParametriSessioneSuRecord("PROTEZIONEPOSTO", "IDPRODOTTO", idNuovoProdotto, TSR_NUOVO)
'    End If
    IsImportazioneProtezioniSuPostiOK = True
End Function
'''
'''Public Function IsClonazionePiantaOK_OLD(idVecchiaPianta As Long, _
'''                                        nomeNuovaPianta As String, _
'''                                        descrizioneNuovaPianta As String, _
'''                                        listaOrganizzazioni As Collection, _
'''                                        clonaFasceSequenze As ValoreBooleanoEnum) As Boolean
'''    Dim sql1 As String
'''    Dim sql2 As String
'''    Dim rec1 As New ADODB.Recordset
'''    Dim rec2 As New ADODB.Recordset
'''    Dim count As Integer
'''    Dim idNuovaPianta As Long
'''    Dim righeAggiornate As Long
'''    Dim coppiaId As clsCoppiaIdent
'''    Dim listaCoppieIdentificatoriAree As New Collection
'''    Dim listaCoppieIdentificatoriFasce As New Collection
'''    Dim listaCoppieIdentificatoriSequenze As New Collection
''''    Dim listaCoppieIdentificatoriVarchi As New Collection
'''    Dim idVecchiaArea As Long
'''    Dim idNuovaArea As Long
'''    Dim idNuovaFascia As Long
'''    Dim idNuovaSequenza As Long
'''    Dim idNuovoPosto As Long
''''    Dim idNuovoVarco As Long
'''    Dim i As Long
'''    Dim id As Long
'''    Dim organizzazione As clsElementoLista
'''
'''    idNuovaPianta = OttieniIdentificatoreDaSequenza("SQ_PIANTA")
'''
'''' inserimento in PIANTA
'''    sql1 = "INSERT INTO PIANTA (IDPIANTA, NOME, DESCRIZIONE, NUMEROVERSIONE)" & _
'''        " VALUES (" & _
'''        idNuovaPianta & ", " & _
'''        " '" & StringaConApiciRaddoppiati(nomeNuovaPianta) & "'," & _
'''        IIf(IsNull(descrizioneNuovaPianta), " NULL, ", " '" & StringaConApiciRaddoppiati(descrizioneNuovaPianta) & "', ") & _
'''        1 & ")"
'''    SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''' inserimento in ORGANIZZAZIONE_PIANTA
'''    For Each organizzazione In listaOrganizzazioni
'''        sql1 = "INSERT INTO ORGANIZZAZIONE_PIANTA(" & _
'''            " IDORGANIZZAZIONE," & _
'''            " IDPIANTA," & _
'''            " CODICEPIANTA" & _
'''            ") VALUES( " & _
'''            organizzazione.idElementoLista & ", " & _
'''            idNuovaPianta & ", " & _
'''            organizzazione.codiceElementoLista & ")"
'''        SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''    Next organizzazione
'''
'''' inserimento in VENUE_PIANTA
'''    sql1 = "INSERT INTO VENUE_PIANTA(" & _
'''        " IDVENUE," & _
'''        " IDPIANTA" & _
'''        ") (SELECT " & _
'''        " IDVENUE," & _
'''        idNuovaPianta & _
'''        " FROM VENUE_PIANTA WHERE IDPIANTA=" & idVecchiaPianta & ")"
'''    SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''' inserimento GRIGLIAPOSTI; questa istruzione viene eseguita solo per piccoli impianti
'''' in quanto per grandi impianti la condizione where non � mai soddisfatta
'''    sql1 = "INSERT INTO GRIGLIAPOSTI(" & _
'''        " IDGRIGLIAPOSTI," & _
'''        " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE," & _
'''        " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO, IDAREA," & _
'''        " IDPIANTA" & _
'''        ") (SELECT " & _
'''        " SQ_GRIGLIAPOSTI.NEXTVAL," & _
'''        " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE," & _
'''        " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO, IDAREA," & _
'''       idNuovaPianta & _
'''        " FROM GRIGLIAPOSTI WHERE IDPIANTA=" & idVecchiaPianta & ")"
'''    SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
''''' inserimento VARCO
''''    sql1 = "SELECT IDVARCO, NOME, DESCRIZIONE" & _
''''        " FROM VARCO WHERE IDPIANTA = " & idVecchiaPianta
''''    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
''''    If Not (rec1.BOF And rec1.EOF) Then
''''        rec1.MoveFirst
''''        While Not rec1.EOF
''''            idNuovoVarco = OttieniIdentificatoreDaSequenza("SQ_VARCO")
''''            Set coppiaId = New clsCoppiaIdent
''''            coppiaId.idVecchio = rec1("IDVARCO")
''''            coppiaId.idNuovo = idNuovoVarco
''''            Call listaCoppieIdentificatoriVarchi.Add(coppiaId)
''''            sql1 = "INSERT INTO VARCO(" & _
''''                " IDVARCO," & _
''''                " NOME, DESCRIZIONE," & _
''''                " IDPIANTA" & _
''''                ") VALUES (" & _
''''                idNuovoVarco & "," & _
''''                DefinisciCampoStringa(rec1("NOME")) & ", " & _
''''                DefinisciCampoStringa(rec1("DESCRIZIONE")) & ", " & _
''''                idNuovaPianta & ")"
''''            SETAConnection.Execute sql1, righeAggiornate, adCmdText
''''            rec1.MoveNext
''''        Wend
''''    End If
''''    rec1.Close
'''
'''' inserimento superaree e aree
'''    sql1 = "SELECT IDAREA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONESTAMPAINGRESSO," & _
'''        " CODICE, INDICEDIPREFERIBILITA, CAPIENZA, IDTIPOAREA, IDORDINEDIPOSTOSIAE, IDAREA_PADRE" & _
'''        " FROM AREA WHERE IDPIANTA = " & idVecchiaPianta
'''    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'''    If Not (rec1.BOF And rec1.EOF) Then
'''        rec1.MoveFirst
'''        While Not rec1.EOF
'''            idNuovaArea = OttieniIdentificatoreDaSequenza("SQ_AREA")
'''            Set coppiaId = New clsCoppiaIdent
'''            coppiaId.idVecchio = rec1("IDAREA")
'''            coppiaId.idNuovo = idNuovaArea
'''            Call listaCoppieIdentificatoriAree.Add(coppiaId)
'''
'''            ' conviene inserire senza l'indicazione dell'area padre e aggiornare dopo aver inserito tutto
'''            sql1 = "INSERT INTO AREA(" & _
'''                " IDAREA," & _
'''                " NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONESTAMPAINGRESSO," & _
'''                " CODICE, INDICEDIPREFERIBILITA, CAPIENZA," & _
'''                " IDPIANTA," & _
'''                " IDTIPOAREA, IDORDINEDIPOSTOSIAE, IDAREA_PADRE, NUMEROVERSIONE" & _
'''                ") VALUES (" & _
'''                idNuovaArea & "," & _
'''                DefinisciCampoStringa(rec1("NOME")) & ", " & _
'''                DefinisciCampoStringa(rec1("DESCRIZIONE")) & ", " & _
'''                DefinisciCampoStringa(rec1("DESCRIZIONEALTERNATIVA")) & ", " & _
'''                DefinisciCampoStringa(rec1("DESCRIZIONESTAMPAINGRESSO")) & ", " & _
'''                DefinisciCampoStringa(rec1("CODICE")) & ", " & _
'''                IIf(IsNull(rec1("INDICEDIPREFERIBILITA")), "NULL", rec1("INDICEDIPREFERIBILITA")) & ", " & _
'''                IIf(IsNull(rec1("CAPIENZA")), "NULL", rec1("CAPIENZA")) & ", " & _
'''                idNuovaPianta & "," & _
'''                rec1("IDTIPOAREA") & ", " & _
'''                IIf(IsNull(rec1("IDORDINEDIPOSTOSIAE")), "NULL,", rec1("IDORDINEDIPOSTOSIAE") & ", ") & _
'''                IIf(IsNull(rec1("IDAREA_PADRE")), "NULL, ", rec1("IDAREA_PADRE") & ", ") & _
'''                1 & ")"
'''            SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''            rec1.MoveNext
'''        Wend
'''    End If
'''    rec1.Close
'''
'''' aggiornamenti AREA.AREA_PADRE, inserimento in GRIGLIAPOSTI
'''' scorrendo la lista delle coppie di id
'''    For i = 1 To listaCoppieIdentificatoriAree.count
'''        idVecchiaArea = listaCoppieIdentificatoriAree(i).idVecchio
'''        idNuovaArea = listaCoppieIdentificatoriAree(i).idNuovo
'''        sql1 = "UPDATE AREA" & _
'''            " SET IDAREA_PADRE=" & idNuovaArea & _
'''            " WHERE " & _
'''            " IDPIANTA=" & idNuovaPianta & _
'''            " AND IDAREA_PADRE=" & idVecchiaArea
'''        SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''' questa istruzione vale per i grandi impianti
'''        sql1 = "INSERT INTO GRIGLIAPOSTI(" & _
'''            " IDGRIGLIAPOSTI," & _
'''            " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE," & _
'''            " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO," & _
'''            " IDAREA," & _
'''            " IDPIANTA" & _
'''            ") (SELECT " & _
'''            " SQ_GRIGLIAPOSTI.NEXTVAL," & _
'''            " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE," & _
'''            " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO," & _
'''           idNuovaArea & ", " & _
'''            " IDPIANTA" & _
'''            " FROM GRIGLIAPOSTI WHERE IDAREA=" & idVecchiaArea & ")"
'''        SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''    Next i
'''
''''' inserimento in AREA_VARCO
''''    sql1 = "SELECT AREA_VARCO.IDVARCO, AREA_VARCO.IDAREA" & _
''''        " FROM AREA_VARCO, AREA" & _
''''        " WHERE AREA_VARCO.IDAREA=AREA.IDAREA AND AREA.IDPIANTA = " & idVecchiaPianta
''''    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
''''    If Not (rec1.BOF And rec1.EOF) Then
''''        rec1.MoveFirst
''''        While Not rec1.EOF
''''            id = rec1("IDVARCO")
''''            idNuovoVarco = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriVarchi, rec1("IDVARCO"))
''''            idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
''''            sql1 = "INSERT INTO AREA_VARCO(" & _
''''                " IDVARCO," & _
''''                " IDAREA" & _
''''                ") VALUES (" & _
''''                idNuovoVarco & "," & _
''''                idNuovaArea & ")"
''''            SETAConnection.Execute sql1, righeAggiornate, adCmdText
''''
''''            rec1.MoveNext
''''        Wend
''''    End If
''''    rec1.Close
'''
'''    If clonaFasceSequenze = VB_VERO Then
'''' aggiornamenti FASCIAPOSTI
'''        sql1 = "SELECT IDFASCIAPOSTI, FASCIAPOSTI.IDAREA, FASCIAPOSTI.INDICEDIPREFERIBILITA" & _
'''            " FROM FASCIAPOSTI, AREA" & _
'''            " WHERE FASCIAPOSTI.IDAREA=AREA.IDAREA AND AREA.IDPIANTA = " & idVecchiaPianta
'''        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'''        If Not (rec1.BOF And rec1.EOF) Then
'''            rec1.MoveFirst
'''            While Not rec1.EOF
'''                idNuovaFascia = OttieniIdentificatoreDaSequenza("SQ_FASCIAPOSTI")
'''                Set coppiaId = New clsCoppiaIdent
'''                coppiaId.idVecchio = rec1("IDFASCIAPOSTI")
'''                coppiaId.idNuovo = idNuovaFascia
'''                Call listaCoppieIdentificatoriFasce.Add(coppiaId)
'''
'''                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
'''                sql1 = "INSERT INTO FASCIAPOSTI(" & _
'''                    " IDFASCIAPOSTI," & _
'''                    " IDAREA," & _
'''                    " INDICEDIPREFERIBILITA" & _
'''                    ") VALUES (" & _
'''                    idNuovaFascia & "," & _
'''                    idNuovaArea & "," & _
'''                    rec1("INDICEDIPREFERIBILITA") & ")"
'''                SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''                rec1.MoveNext
'''            Wend
'''        End If
'''        rec1.Close
'''
'''' aggiornamenti SEQUENZAPOSTI
'''        sql1 = "SELECT IDSEQUENZAPOSTI, SEQUENZAPOSTI.IDFASCIAPOSTI, SEQUENZAPOSTI.ORDINEINFASCIA" & _
'''            " FROM SEQUENZAPOSTI, FASCIAPOSTI, AREA" & _
'''            " WHERE SEQUENZAPOSTI.IDFASCIAPOSTI = FASCIAPOSTI.IDFASCIAPOSTI" & _
'''            " AND FASCIAPOSTI.IDAREA=AREA.IDAREA AND AREA.IDPIANTA = " & idVecchiaPianta
'''        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'''        If Not (rec1.BOF And rec1.EOF) Then
'''            rec1.MoveFirst
'''            While Not rec1.EOF
'''                idNuovaSequenza = OttieniIdentificatoreDaSequenza("SQ_SEQUENZAPOSTI")
'''                Set coppiaId = New clsCoppiaIdent
'''                coppiaId.idVecchio = rec1("IDSEQUENZAPOSTI")
'''                coppiaId.idNuovo = idNuovaSequenza
'''                Call listaCoppieIdentificatoriSequenze.Add(coppiaId)
'''
'''                idNuovaFascia = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriFasce, rec1("IDFASCIAPOSTI"))
'''                sql1 = "INSERT INTO SEQUENZAPOSTI(" & _
'''                    " IDSEQUENZAPOSTI," & _
'''                    " IDFASCIAPOSTI," & _
'''                    " ORDINEINFASCIA" & _
'''                    ") VALUES (" & _
'''                    idNuovaSequenza & "," & _
'''                    idNuovaFascia & "," & _
'''                    rec1("ORDINEINFASCIA") & ")"
'''                SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''                rec1.MoveNext
'''            Wend
'''        End If
'''        rec1.Close
'''
'''' aggiornamenti POSTO
'''        sql1 = "SELECT IDPOSTO, NOMEFILA, NOMEPOSTO," & _
'''            " COORDINATAORIZZONTALE, COORDINATAVERTICALE, POSTO.IDAREA," & _
'''            " IDSEQUENZAPOSTI, ORDINEINSEQUENZA" & _
'''            " FROM POSTO, AREA" & _
'''            " WHERE POSTO.IDAREA=AREA.IDAREA AND AREA.IDPIANTA = " & idVecchiaPianta
'''        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'''        If Not (rec1.BOF And rec1.EOF) Then
'''            rec1.MoveFirst
'''            While Not rec1.EOF
'''                idNuovoPosto = OttieniIdentificatoreDaSequenza("SQ_POSTO")
'''                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
'''                If IsNull(rec1("IDSEQUENZAPOSTI")) Then
'''                    idNuovaSequenza = idNessunElementoSelezionato
'''                Else
'''                    idNuovaSequenza = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriSequenze, rec1("IDSEQUENZAPOSTI"))
'''                End If
'''                sql1 = "INSERT INTO POSTO(" & _
'''                    " IDPOSTO," & _
'''                    " NOMEFILA, NOMEPOSTO," & _
'''                    " COORDINATAORIZZONTALE, COORDINATAVERTICALE," & _
'''                    " IDAREA," & _
'''                    " IDSEQUENZAPOSTI," & _
'''                    " ORDINEINSEQUENZA" & _
'''                    ") VALUES (" & _
'''                    idNuovoPosto & "," & _
'''                    DefinisciCampoStringa(rec1("NOMEFILA")) & ", " & _
'''                    DefinisciCampoStringa(rec1("NOMEPOSTO")) & ", " & _
'''                    rec1("COORDINATAORIZZONTALE") & ", " & _
'''                    rec1("COORDINATAVERTICALE") & ", " & _
'''                    idNuovaArea & "," & _
'''                    IIf(idNuovaSequenza = idNessunElementoSelezionato, "NULL,", idNuovaSequenza & ", ") & _
'''                    IIf(idNuovaSequenza = idNessunElementoSelezionato, "NULL)", rec1("ORDINEINSEQUENZA") & ")")
'''                SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''                rec1.MoveNext
'''            Wend
'''        End If
'''        rec1.Close
'''
'''    Else '    clonaFasceSequenze FALSE
'''' inserisco soltanto i posti senza riferimenti a sequenze
'''        sql1 = "SELECT IDPOSTO, NOMEFILA, NOMEPOSTO," & _
'''            " COORDINATAORIZZONTALE, COORDINATAVERTICALE, POSTO.IDAREA AS POSTOIDAREA" & _
'''            " FROM POSTO, AREA" & _
'''            " WHERE POSTO.IDAREA=AREA.IDAREA AND AREA.IDPIANTA = " & idVecchiaPianta
'''        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'''        If Not (rec1.BOF And rec1.EOF) Then
'''            rec1.MoveFirst
'''            While Not rec1.EOF
'''                idNuovoPosto = OttieniIdentificatoreDaSequenza("SQ_POSTO")
'''                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("POSTOIDAREA"))
'''                sql1 = "INSERT INTO POSTO(" & _
'''                    " IDPOSTO," & _
'''                    " NOMEFILA, NOMEPOSTO," & _
'''                    " COORDINATAORIZZONTALE, COORDINATAVERTICALE, IDAREA," & _
'''                    " IDSEQUENZAPOSTI, ORDINEINSEQUENZA" & _
'''                    ") VALUES (" & _
'''                    idNuovoPosto & "," & _
'''                    DefinisciCampoStringa(rec1("NOMEFILA")) & ", " & _
'''                    DefinisciCampoStringa(rec1("NOMEPOSTO")) & ", " & _
'''                    rec1("COORDINATAORIZZONTALE") & ", " & _
'''                    rec1("COORDINATAVERTICALE") & ", " & _
'''                    idNuovaArea & "," & _
'''                    " NULL, NULL)"
'''                SETAConnection.Execute sql1, righeAggiornate, adCmdText
'''
'''                rec1.MoveNext
'''            Wend
'''        End If
'''        rec1.Close
'''
'''    End If
'''
'''    Call ScriviLog(CCTA_CLONAZIONE, CCDA_PIANTA, , "IDPIANTA MASTER = " & idVecchiaArea, idNuovaPianta)
'''
'''    IsClonazionePiantaOK_OLD = True
'''End Function

Private Function OttieniNuovoIdNellaLista(lista As Collection, vecchioId As Long) As Long
Dim i As Integer
Dim trovato As Boolean

    i = 1
    trovato = False
    
    OttieniNuovoIdNellaLista = idNessunElementoSelezionato
    While i <= lista.count And Not trovato
        If lista(i).idVecchio = vecchioId Then
            trovato = True
            OttieniNuovoIdNellaLista = lista(i).idNuovo
        End If
        i = i + 1
    Wend

End Function

Public Function IsSimboloSuPiantaCorretto(s As String) As Boolean
    If (Asc(s) >= 65 And Asc(s) <= 90) Or _
       (Asc(s) >= 49 And Asc(s) <= 57) Then
        IsSimboloSuPiantaCorretto = True
    Else
        IsSimboloSuPiantaCorretto = False
    End If
End Function

Public Function StringaConApiciRaddoppiati(sIn As String) As String
    Dim i As Integer
    Dim c As String
    Dim sout As String
    
    sout = ""
    For i = 1 To Len(sIn)
        c = Mid(sIn, i, 1)
        sout = sout & c
'        If c = Chr(34) Or c = Chr(39) Then
        If c = Chr(39) Then
            sout = sout & c
        End If
    Next i
    
    StringaConApiciRaddoppiati = sout
End Function

Public Function SostituisciVirgolaConPunto(sIn As String) As String
    Dim l As Integer
    Dim i As Integer
    Dim c As String
    Dim sout As String
    
    If sIn <> "" Then
        sout = ""
        l = Len(sIn)
        For i = 1 To l
            c = Mid$(sIn, i, 1)
            If c = Chr(44) Then
                sout = sout & Chr(46)
            Else
                sout = sout & c
            End If
        Next i
    Else
        sout = sIn
    End If
    SostituisciVirgolaConPunto = sout
End Function

Public Function IsImportazioneTariffePeriodiPrezziOK(idProdottoCorrente As Long, idProdottoMaster As Long) As Boolean
    Dim sql1 As String
    Dim sql2 As String
    Dim rec1 As New ADODB.Recordset
    Dim count As Integer
    Dim righeAggiornate As Long
    Dim coppiaId As clsCoppiaIdent
    Dim listaCoppieIdentificatoriTariffe As New Collection
    Dim listaCoppieIdentificatoriPeriodiCommerciali As New Collection
    Dim idNuovaTariffa As Long
    Dim idNuovoPeriodoCommerciale As Long
    Dim idNuovoPrezzo As Long
    Dim n As Long
    
' inserimento in TARIFFA
    sql1 = "SELECT IDTARIFFA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
    sql1 = sql1 & " IDTIPOTARIFFA, IDORGANIZZAZIONE, CODICE, IDMODALITAEMISSIONE,"
    sql1 = sql1 & " IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC, APPLICABILEAPOSTOCONRINUNCIA,"
    sql1 = sql1 & " DATANASCITATITOLAREMINIMA, DATANASCITATITOLAREMASSIMA, CAMBIOUTILIZZATOREAPPLICABILE, NUMEROMASSIMOTITOLIPERACQ,"
    sql1 = sql1 & " IDPROMOZIONE, IVAPREASSOLTAOBBLIGATORIA, TITOLOCORPORATEOBBLIGATORIO"
    sql1 = sql1 & " FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoMaster
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
            Set coppiaId = New clsCoppiaIdent
            coppiaId.idVecchio = rec1("IDTARIFFA").Value
            coppiaId.idNuovo = idNuovaTariffa
            Call listaCoppieIdentificatoriTariffe.Add(coppiaId)
            
            sql1 = "INSERT INTO TARIFFA("
            sql1 = sql1 & " IDTARIFFA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
            sql1 = sql1 & " IDTIPOTARIFFA, IDORGANIZZAZIONE, IDPRODOTTO, IDMODALITAEMISSIONE,"
            sql1 = sql1 & " CODICE, IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC, APPLICABILEAPOSTOCONRINUNCIA,"
            sql1 = sql1 & " DATANASCITATITOLAREMINIMA, DATANASCITATITOLAREMASSIMA, CAMBIOUTILIZZATOREAPPLICABILE, NUMEROMASSIMOTITOLIPERACQ,"
            sql1 = sql1 & " IDPROMOZIONE, IVAPREASSOLTAOBBLIGATORIA, TITOLOCORPORATEOBBLIGATORIO"
            sql1 = sql1 & ") VALUES ("
            sql1 = sql1 & idNuovaTariffa & ","
            sql1 = sql1 & " '" & rec1("NOME") & "', "
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONE")), "NULL", " '" & rec1("DESCRIZIONE") & "'") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONEALTERNATIVA")), "NULL", " '" & rec1("DESCRIZIONEALTERNATIVA") & "'") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONEPOS")), "NULL", " '" & rec1("DESCRIZIONEPOS") & "'") & ", "
            sql1 = sql1 & rec1("IDTIPOTARIFFA") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("IDORGANIZZAZIONE")), "NULL", rec1("IDORGANIZZAZIONE")) & ", "
            sql1 = sql1 & idProdottoCorrente & ", "
            sql1 = sql1 & rec1("IDMODALITAEMISSIONE") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("CODICE")), "NULL)", " '" & rec1("CODICE") & "', ")
            sql1 = sql1 & rec1("IDAMBITOAPPLICABILITATARIFFA") & ", "
            sql1 = sql1 & rec1("IDCLASSETARIFFACONTROLLOACC") & ", "
            sql1 = sql1 & rec1("APPLICABILEAPOSTOCONRINUNCIA") & ", "
            
            If (IsNull(rec1("DATANASCITATITOLAREMINIMA"))) Then
                sql1 = sql1 & "NULL" & ", "
            Else
                sql1 = sql1 & SqlDateValue(rec1("DATANASCITATITOLAREMINIMA")) & ", "
            End If
            If (IsNull(rec1("DATANASCITATITOLAREMASSIMA"))) Then
                sql1 = sql1 & "NULL" & ", "
            Else
                sql1 = sql1 & SqlDateValue(rec1("DATANASCITATITOLAREMASSIMA")) & ", "
            End If

            sql1 = sql1 & rec1("CAMBIOUTILIZZATOREAPPLICABILE") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("NUMEROMASSIMOTITOLIPERACQ")), "NULL", rec1("NUMEROMASSIMOTITOLIPERACQ")) & ", "
            sql1 = sql1 & IIf(IsNull(rec1("IDPROMOZIONE")), "NULL", rec1("IDPROMOZIONE")) & ", "
            sql1 = sql1 & rec1("IVAPREASSOLTAOBBLIGATORIA") & ", "
            sql1 = sql1 & rec1("TITOLOCORPORATEOBBLIGATORIO") & ")"
            
            SETAConnection.Execute sql1, righeAggiornate, adCmdText
            
            ' inserimento in OPERATORE_TARIFFA
            n = 0
            sql2 = "INSERT INTO OPERATORE_TARIFFA (IDOPERATORE, IDTARIFFA)"
            sql2 = sql2 & " (SELECT IDOPERATORE, "
            sql2 = sql2 & idNuovaTariffa & " FROM OPERATORE_TARIFFA WHERE"
            sql2 = sql2 & " IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText
            
            ' inserimento in TARIFFA_TIPOTERMINALE
            n = 0
            sql2 = "INSERT INTO TARIFFA_TIPOTERMINALE (IDTIPOTERMINALE, IDTASTOTIPOTERMINALE,"
            sql2 = sql2 & " IDTARIFFA) (SELECT IDTIPOTERMINALE, IDTASTOTIPOTERMINALE, "
            sql2 = sql2 & idNuovaTariffa & " FROM TARIFFA_TIPOTERMINALE WHERE"
            sql2 = sql2 & " IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            ' inserimento in CLASSEPV_TARIFFA
            n = 0
            sql2 = "INSERT INTO CLASSEPV_TARIFFA (IDCLASSEPUNTOVENDITA, IDTARIFFA)"
            sql2 = sql2 & "(SELECT IDCLASSEPUNTOVENDITA, " & idNuovaTariffa
            sql2 = sql2 & " FROM CLASSEPV_TARIFFA"
            sql2 = sql2 & " WHERE IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            ' inserimento in TAR_CATEGORIACARTADIPAGAMENTO
            n = 0
            sql2 = "INSERT INTO TAR_CATEGORIACARTADIPAGAMENTO (IDTARIFFA, IDCATEGORIACARTADIPAGAMENTO)"
            sql2 = sql2 & " (SELECT " & idNuovaTariffa & ", IDCATEGORIACARTADIPAGAMENTO"
            sql2 = sql2 & " FROM TAR_CATEGORIACARTADIPAGAMENTO"
            sql2 = sql2 & " WHERE IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            ' inserimento in TARIFFA_TIPOSUPPORTODIGITALE
            n = 0
            sql2 = "INSERT INTO TARIFFA_TIPOSUPPORTODIGITALE (IDTARIFFA, IDTIPOSUPPORTO)"
            sql2 = sql2 & " (SELECT " & idNuovaTariffa & ", IDTIPOSUPPORTO"
            sql2 = sql2 & " FROM TARIFFA_TIPOSUPPORTODIGITALE"
            sql2 = sql2 & " WHERE IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
' agggiorna tariffe_riferimento
    sql1 = "SELECT IDTARIFFA, IDTARIFFA_RIFERIMENTO"
    sql1 = sql1 & " FROM TARIFFA"
    sql1 = sql1 & " WHERE IDPRODOTTO = " & idProdottoMaster
    sql1 = sql1 & " AND IDTARIFFA_RIFERIMENTO IS NOT NULL"
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
'            If IsNull(rec1("IDTARIFFA_RIFERIMENTO")) Then
'                sql1 = sql1 & "NULL)"
'            Else
'                sql1 = sql1 & OttieniIdentificatoreDaSequenza(rec1("IDTARIFFA_RIFERIMENTO")) & ")"
'            End If
            n = 0
            sql2 = "UPDATE TARIFFA SET IDTARIFFA_RIFERIMENTO = " & OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA_RIFERIMENTO"))
            sql2 = sql2 & " WHERE IDTARIFFA= " & OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA"))
            SETAConnection.Execute sql2, n, adCmdText
            
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
' inserimento in PERIODOCOMMERCIALE
    sql1 = "SELECT IDPERIODOCOMMERCIALE,"
    sql1 = sql1 & " NOME, DESCRIZIONE,"
    sql1 = sql1 & " DATAORAINIZIO, DATAORAFINEPREVENDITA, DATAORAFINE"
    sql1 = sql1 & " FROM PERIODOCOMMERCIALE WHERE IDPRODOTTO = " & idProdottoMaster
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovoPeriodoCommerciale = OttieniIdentificatoreDaSequenza("SQ_PERIODOCOMMERCIALE")
            Set coppiaId = New clsCoppiaIdent
            coppiaId.idVecchio = rec1("IDPERIODOCOMMERCIALE").Value
            coppiaId.idNuovo = idNuovoPeriodoCommerciale
            Call listaCoppieIdentificatoriPeriodiCommerciali.Add(coppiaId)
            
            sql1 = "INSERT INTO PERIODOCOMMERCIALE("
            sql1 = sql1 & " IDPERIODOCOMMERCIALE,"
            sql1 = sql1 & " NOME, DESCRIZIONE,"
            sql1 = sql1 & " DATAORAINIZIO, DATAORAFINEPREVENDITA, DATAORAFINE, IDPRODOTTO"
            sql1 = sql1 & ") VALUES ("
            sql1 = sql1 & idNuovoPeriodoCommerciale & ","
            sql1 = sql1 & " '" & rec1("NOME") & "',"
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONE")), "NULL", " '" & rec1("DESCRIZIONE") & "'") & ", "
            sql1 = sql1 & SqlDateTimeValue(rec1("DATAORAINIZIO")) & ","
            sql1 = sql1 & SqlDateTimeValue(rec1("DATAORAFINEPREVENDITA")) & ","
            sql1 = sql1 & SqlDateTimeValue(rec1("DATAORAFINE")) & ","
            sql1 = sql1 & idProdottoCorrente & ")"
            SETAConnection.Execute sql1, righeAggiornate, adCmdText

            rec1.MoveNext
        Wend
        rec1.Close

' inserimento in PREZZOTITOLOPRODOTTO
        sql1 = "SELECT"
        sql1 = sql1 & " IDPREZZOTITOLOPRODOTTO,"
        sql1 = sql1 & " IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
        sql1 = sql1 & " IDTARIFFA,"
        sql1 = sql1 & " IDAREA,"
        sql1 = sql1 & " IDTIPOTARIFFASIAE,"
        sql1 = sql1 & " IDPERIODOCOMMERCIALE"
        sql1 = sql1 & " FROM PREZZOTITOLOPRODOTTO WHERE IDPRODOTTO = " & idProdottoMaster
        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovoPrezzo = OttieniIdentificatoreDaSequenza("SQ_PREZZOTITOLOPRODOTTO")
                idNuovaTariffa = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA"))
                idNuovoPeriodoCommerciale = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriPeriodiCommerciali, rec1("IDPERIODOCOMMERCIALE"))
                
                sql1 = "INSERT INTO PREZZOTITOLOPRODOTTO("
                sql1 = sql1 & " IDPREZZOTITOLOPRODOTTO,"
                sql1 = sql1 & " IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
                sql1 = sql1 & " IDPRODOTTO,"
                sql1 = sql1 & " IDTARIFFA,"
                sql1 = sql1 & " IDAREA,"
                sql1 = sql1 & " IDTIPOTARIFFASIAE,"
                sql1 = sql1 & " IDPERIODOCOMMERCIALE"
                sql1 = sql1 & ") VALUES ("
                sql1 = sql1 & idNuovoPrezzo & ","
                sql1 = sql1 & StringaConPuntoDecimale(rec1("IMPORTOBASE")) & ", "
                sql1 = sql1 & StringaConPuntoDecimale(rec1("IMPORTOSERVIZIOPREVENDITA")) & ", "
                sql1 = sql1 & idProdottoCorrente & ", "
                sql1 = sql1 & idNuovaTariffa & ", "
                sql1 = sql1 & rec1("IDAREA") & ", "
                sql1 = sql1 & rec1("IDTIPOTARIFFASIAE") & ", "
                sql1 = sql1 & idNuovoPeriodoCommerciale & ")"
                SETAConnection.Execute sql1, righeAggiornate, adCmdText
'                If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'                    Call AggiornaParametriSessioneSuRecord("PREZZOTITOLOPRODOTTO", "IDTARIFFA", idNuovaTariffa, TSR_NUOVO)
'                End If
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
    End If

' inserimento in UTILIZZOLAYOUTSUPPORTOCPV (IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO, IDTIPOTERMINALE)
    sql1 = "SELECT"
    sql1 = sql1 & " ULS.IDAREA, ULS.IDTARIFFA, ULS.IDTIPOSUPPORTO, ULS.IDLAYOUTSUPPORTO"
    sql1 = sql1 & " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T"
    sql1 = sql1 & " WHERE ULS.IDTARIFFA=T.IDTARIFFA AND T.IDPRODOTTO = " & idProdottoMaster
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovaTariffa = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA"))

            sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV("
            sql1 = sql1 & " IDAREA,"
            sql1 = sql1 & " IDTARIFFA,"
            sql1 = sql1 & " IDTIPOSUPPORTO,"
            sql1 = sql1 & " IDLAYOUTSUPPORTO"
            sql1 = sql1 & ") VALUES ("
            sql1 = sql1 & rec1("IDAREA") & ", "
            sql1 = sql1 & idNuovaTariffa & ", "
            sql1 = sql1 & rec1("IDTIPOSUPPORTO") & ", "
            sql1 = sql1 & rec1("IDLAYOUTSUPPORTO") & ")"
            SETAConnection.Execute sql1, righeAggiornate, adCmdText
'            If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'                Call AggiornaParametriSessioneSuRecord("UTILIZZOLAYOUTSUPPORTOCPV", "IDTARIFFA", idNuovaTariffa, TSR_NUOVO)
'            End If

            rec1.MoveNext
        Wend
    End If
    rec1.Close

    IsImportazioneTariffePeriodiPrezziOK = True
End Function

Private Function EsisteAlmenoUnaRappresentazione(idProdotto As Long, idClasseProdotto As ClasseProdottoEnum) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim idSceltaRappresentazione As Long
    Dim associazioneOK As Boolean
    
    associazioneOK = True
    If idClasseProdotto = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
        sql = "SELECT DISTINCT IDSCELTARAPPRESENTAZIONE"
        sql = sql & " FROM SCELTARAPPRESENTAZIONE"
        sql = sql & " WHERE IDPRODOTTO = " & idProdotto
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            While Not rec.EOF
                idSceltaRappresentazione = rec("IDSCELTARAPPRESENTAZIONE").Value
                sql1 = "SELECT COUNT(IDRAPPRESENTAZIONE) CONT"
                sql1 = sql1 & " FROM SCELTARAPPRESENTAZIONE_RAPPR"
                sql1 = sql1 & " WHERE IDSCELTARAPPRESENTAZIONE = " & idSceltaRappresentazione
                rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
                associazioneOK = associazioneOK And (rec1("CONT").Value > 0)
                rec1.Close
                rec.MoveNext
            Wend
        Else
            associazioneOK = False
        End If
        rec.Close
    Else
        sql = "SELECT COUNT(IDRAPPRESENTAZIONE) CONT"
        sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE WHERE IDPRODOTTO = " & idProdotto
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        associazioneOK = (rec("CONT").Value > 0)
        rec.Close
    End If
    EsisteAlmenoUnaRappresentazione = associazioneOK
End Function

Public Function IsProdottoAttivabile(idProd As Long, Optional verbose As Boolean) As Boolean
    Dim rec1 As New ADODB.Recordset
    Dim sql1 As String
    Dim rec2 As New ADODB.Recordset
    Dim sql2 As String
    Dim rec3 As New ADODB.Recordset
    Dim sql3 As String
    Dim rec4 As New ADODB.Recordset
    Dim sql4 As String
    Dim dataOraAttuale As Date
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim idTipoTerm As Long
    Dim idTipoOper As Long
    Dim idTipoTarLotto As Long
    Dim idPerComm As Long
    Dim idArea As Long
    Dim numeroPeriodi As Integer
    Dim associazioneRappresentazioniOK As Boolean
    Dim esistePeriodoCommerciale As Boolean
    Dim esistePrezzo As Boolean
    Dim esistePrezzoTL As Boolean
    Dim esisteTipoLayoutSupporto As Boolean
    Dim associazioneCompleta As Boolean
    Dim isProdottoScaduto As Boolean
    Dim isProdottoSolamenteScaduto As Boolean
    Dim trovato As Boolean
    Dim requisitiMinimi As Boolean
    Dim listaAttributiNonAttivabili As Collection
    Dim idPianta As Long
    Dim idPiantaSIAE As Long
    Dim isPiantaOK As Boolean
    Dim isPiantaSIAEOK As Boolean
    Dim isTipoTariffeSIAEOK As Boolean
    Dim isFiscalitaGenereSIAEOK As Boolean
    Dim isModConsegnaOK As Boolean
    Dim isDurateRisOK As Boolean
    Dim isVenditaInternetTitDigOK As Boolean
    Dim isVenditaSenzaStampaInternetOK As Boolean
    Dim isTariffaSupportoDigitaleOK As Boolean
    Dim isRitiroOK As Boolean
    Dim isCommissioniOK As Boolean
    Dim isLayoutVendOK As Boolean
    Dim areInteriRidottiOK As Boolean
    Dim areSuperareeMediumTypeOK As Boolean
    Dim isAssociatoContratto As Boolean
    Dim rateoNonNullo As Boolean
    Dim esisteOrgTipoLayoutErrato As Boolean
    Dim listaOrgTipoLayoutErrati As Collection
    Dim idClasseProdotto As ClasseProdottoEnum
    Dim listaTariffe As String
    
'   il prodotto � attivabile se:
'   a) esiste almeno una rappresentazione/scelta rappr. AND a1) rateo>0 AND a2) � sssociato un contratto
'   b) la prima rappresentazione non deve essere finita (requisito aggirabile previo avviso all'operatore)
'   c) IsPiantaCompleta = True
'   c1) il prodotto � spedibile o ritirabile
'   TipoOperazione = Protezione: valgono a, b, c
'   TipoOperazione = Riservazione or Vendita: valgono a, b, c e inoltre:
'   d) esiste almeno un periodo commerciale presente o futuro
'   e) esiste almeno un prezzo per periodo commerciale presente o futuro
'   f) se l'operaz. � abilitata su TL, allora vale e) per tariffa TL.
'   TipoOperazione = Stampa: valgono a, b, c e inoltre:
'   g) ogni area ha associato layout e tipo supporto per il relativo terminale
    
'   h) fiscalita': deve essere congruente il genere siae con il flag 'fiscale'

    Set listaAttributiNonAttivabili = New Collection
    
    Call ApriConnessioneBD
    
    rateoNonNullo = False
    sql1 = "SELECT IDPIANTA, IDPIANTASIAE, RATEO, IDCONTRATTO, IDCLASSEPRODOTTO" & _
        " FROM PRODOTTO WHERE IDPRODOTTO = " & idProd
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
        idPianta = rec1("IDPIANTA").Value
        idPiantaSIAE = rec1("IDPIANTASIAE").Value
        idClasseProdotto = rec1("IDCLASSEPRODOTTO").Value
        rateoNonNullo = (rec1("RATEO") > 0)
        If IsNull(rec1("IDCONTRATTO")) Then
            isAssociatoContratto = False
        Else
            isAssociatoContratto = True
        End If
    rec1.Close
    
'   a) esiste almeno una rappresentazione/scelta rappresentazione
    associazioneRappresentazioniOK = EsisteAlmenoUnaRappresentazione(idProd, idClasseProdotto)
    If Not associazioneRappresentazioniOK Then
        If idClasseProdotto = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
            Call listaAttributiNonAttivabili.Add("- non � associata alcuna scelta rappresentazione o almeno una scelta rappresentazione non ha alcuna rappresentazione associata;")
        Else
            Call listaAttributiNonAttivabili.Add("- non � associata alcuna rappresentazione;")
        End If
    End If
    
'    a1) rateo>0
    If Not rateoNonNullo Then
        Call listaAttributiNonAttivabili.Add("- il rateo � zero;")
    End If
    
'    a2) � associato un contratto
    If Not isAssociatoContratto Then
        Call listaAttributiNonAttivabili.Add("- non � associato alcun contratto;")
    End If
    
'   b) la prima rappresentazione non deve essere finita
' situazione gestita diversamente con un avviso all'operatore
    isProdottoScaduto = True
    If associazioneRappresentazioniOK Then
        isProdottoScaduto = PrimaRappresentazioneFinita(idProd, idClasseProdotto)
        If isProdottoScaduto Then
            Call listaAttributiNonAttivabili.Add("- il prodotto � nello stato scaduto (� terminata la prima rappresentazione o l'ultima rappresentazione della prima scelta rappresentazioni);")
        End If
    Else
        isProdottoScaduto = False
    End If
    
'   d) esiste almeno un periodo commerciale presente o futuro
    trovato = False
    numeroPeriodi = 0
    esistePrezzo = True
    esistePrezzoTL = True
    sql1 = "SELECT IDPERIODOCOMMERCIALE, DATAORAINIZIO, DATAORAFINE, SYSDATE ADESSO" & _
        " FROM PERIODOCOMMERCIALE WHERE IDPRODOTTO = " & idProd
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            dataOraInizio = rec1("DATAORAINIZIO")
            dataOraFine = rec1("DATAORAFINE")
            dataOraAttuale = rec1("ADESSO")
            trovato = (dataOraAttuale > dataOraInizio And dataOraAttuale < dataOraFine) Or _
                (dataOraAttuale < dataOraInizio)
'           e) esiste almeno un prezzo per periodo commerciale presente o futuro
'           f) se l'operaz. � abilitata su TL, allora vale e) per tariffa TL.
            If trovato Then
                numeroPeriodi = numeroPeriodi + 1
                idPerComm = rec1("IDPERIODOCOMMERCIALE")
'                sql2 = "SELECT IMPORTOBASE, IDTIPOTARIFFALOTTO" & _
'                    " FROM PREZZOTITOLOPRODOTTO, TARIFFA WHERE" & _
'                    " (PREZZOTITOLOPRODOTTO.IDTARIFFA = TARIFFA.IDTARIFFA) AND" & _
'                    " (PREZZOTITOLOPRODOTTO.IDPRODOTTO = " & idProd & ") AND" & _
'                    " PREZZOTITOLOPRODOTTO.IDPERIODOCOMMERCIALE = " & idPerComm
'                rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
'                If Not (rec2.BOF And rec2.EOF) Then
'                    rec2.MoveFirst
'                    esistePrezzo = True
'                    esistePrezzoTL = False
'                    While Not rec2.EOF And Not esistePrezzoTL
'                        If Not IsNull(rec2("IDTIPOTARIFFALOTTO")) Then
'                            esistePrezzoTL = True
'                        End If
'                        rec2.MoveNext
'                    Wend
'                    If Not esistePrezzoTL Then
'                        Call listaAttributiNonAttivabili.Add("non sono configurati prezzi relativi a tariffe Lotto per almeno un periodo commerciale;")
'                    End If
'                Else
'                    esistePrezzo = False
'                    esistePrezzoTL = False
'                    Call listaAttributiNonAttivabili.Add("- non � configurato alcun prezzo per almeno un periodo commerciale;")
'                End If
'                rec2.Close
            Else
                Call listaAttributiNonAttivabili.Add("- non � configurato alcun periodo commerciale presente o futuro;")
            End If
            rec1.MoveNext
        Wend
    Else
        Call listaAttributiNonAttivabili.Add("- non esiste alcun periodo commerciale presente o futuro;")
    End If
    rec1.Close
    esistePeriodoCommerciale = (numeroPeriodi > 0)
    
''   g) ogni area ha associato layout e tipo supporto per il relativo terminale.
''   NOTA: PER ORA � COSI', DA RIVEDERE
    associazioneCompleta = True
    
    isPiantaOK = IsPiantaCompleta(idPianta, True, False)
    If Not isPiantaOK Then
        Call listaAttributiNonAttivabili.Add("- la pianta non � completa;")
    End If
    
    isPiantaSIAEOK = IsPiantaSIAECompleta(idPiantaSIAE, idPianta)
    If Not isPiantaSIAEOK Then
        Call listaAttributiNonAttivabili.Add("- la pianta SIAE non � completa;")
    End If
    
    requisitiMinimi = associazioneRappresentazioniOK And _
                        rateoNonNullo And _
                        Not isProdottoScaduto And _
                        isPiantaOK And _
                        isAssociatoContratto And _
                        isPiantaSIAEOK
    isProdottoSolamenteScaduto = associazioneRappresentazioniOK And _
                        rateoNonNullo And _
                        isProdottoScaduto And _
                        isPiantaOK And _
                        isAssociatoContratto And _
                        isPiantaSIAEOK
    sql1 = "SELECT * FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO = " & idProd
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idTipoOper = rec1("IDTIPOOPERAZIONE")
            idTipoTerm = rec1("IDTIPOTERMINALE")
            Select Case idTipoOper
                Case TO_PROTEZIONE
                Case TO_RISERVAZIONE, TO_ANNULLAMENTO_RISERVAZIONE, TO_VENDITA, TO_ANNULLAMENTO_VENDITA
                    requisitiMinimi = requisitiMinimi And esistePeriodoCommerciale And esistePrezzo
                    isProdottoSolamenteScaduto = isProdottoSolamenteScaduto And esistePeriodoCommerciale And esistePrezzo
                    If idTipoTerm = TT_TERMINALE_LOTTO Then
                        requisitiMinimi = requisitiMinimi And esistePrezzoTL
                        isProdottoSolamenteScaduto = isProdottoSolamenteScaduto And esistePrezzoTL
                    End If
                Case TO_STAMPA, TO_ANNULLAMENTO_STAMPA
                    requisitiMinimi = requisitiMinimi And associazioneCompleta
                    isProdottoSolamenteScaduto = isProdottoSolamenteScaduto And associazioneCompleta
                Case Else
            End Select
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
    Set listaOrgTipoLayoutErrati = New Collection
    esisteOrgTipoLayoutErrato = False
'    sql1 = "SELECT DISTINCT A.CODICE AREACOD, LS.CODICE LAYCOD, TS.CODICE TIPOCOD" & _
'        " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, PRODOTTO P," & _
'        " AREA A, LAYOUTSUPPORTO LS, TIPOSUPPORTO TS" & _
'        " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
'        " AND ULS.IDAREA = A.IDAREA" & _
'        " AND ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO" & _
'        " AND ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
'        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
'        " AND P.IDPRODOTTO = " & idProd & _
'        " AND (P.IDORGANIZZAZIONE, ULS.IDLAYOUTSUPPORTO, ULS.IDTIPOSUPPORTO) NOT IN " & _
'        " (SELECT IDORGANIZZAZIONE, IDLAYOUTSUPPORTO, IDTIPOSUPPORTO" & _
'        " FROM ORGANIZ_TIPOSUP_LAYOUTSUP)"
    sql1 = "SELECT DISTINCT A.CODICE AREACOD, LS.CODICE LAYCOD, TS.CODICE TIPOCOD" & _
        " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, PRODOTTO P," & _
        " AREA A, LAYOUTSUPPORTO LS, TIPOSUPPORTO TS" & _
        " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
        " AND ULS.IDAREA = A.IDAREA" & _
        " AND ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO" & _
        " AND ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDPRODOTTO = " & idProd & _
        " AND NOT EXISTS " & _
        " (SELECT IDORGANIZZAZIONE, IDLAYOUTSUPPORTO, IDTIPOSUPPORTO" & _
        " FROM ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
        " WHERE OTL.IDORGANIZZAZIONE = P.IDORGANIZZAZIONE " & _
        " AND OTL.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO" & _
        " AND OTL.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO)"
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        esisteOrgTipoLayoutErrato = True
        rec1.MoveFirst
        While Not rec1.EOF
            Call listaOrgTipoLayoutErrati.Add("    area " & rec1("AREACOD") & ", tipo " & rec1("TIPOCOD") & ", layout " & rec1("LAYCOD") & ";")
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    If esisteOrgTipoLayoutErrato Then
        Call listaAttributiNonAttivabili.Add("- esistono associazioni Tipi/Layout Supporto non pi� associate all'Organizzazione:" & Chr(13) & Chr(10) & _
            ArgomentoMessaggio(listaOrgTipoLayoutErrati))
    End If
    requisitiMinimi = requisitiMinimi And Not esisteOrgTipoLayoutErrato
    
'   h) fiscalita': deve essere congruente il genere siae con il flag 'fiscale'
    isFiscalitaGenereSIAEOK = IsFiscalitaGenereSIAECongruenti(idProd)
    If Not isFiscalitaGenereSIAEOK Then
        Call listaAttributiNonAttivabili.Add("- la fiscalita' non � congruente con il genere SIAE;")
    End If
    requisitiMinimi = requisitiMinimi And isFiscalitaGenereSIAEOK
    
'   deve esistere un solo tipo tariffa siae pari ad I1 per ordine di posto siae
    isTipoTariffeSIAEOK = IsAssociazioneTariffeSIAEOrdiniDiPostoSIAECorretta(idProd, idPiantaSIAE)
    If Not isTipoTariffeSIAEOK Then
        Call listaAttributiNonAttivabili.Add("- l'associazione tra tipi tariffa SIAE e ordini di posto SIAE non � corretta;")
    End If
    requisitiMinimi = requisitiMinimi And isTipoTariffeSIAEOK
    
'   il prodotto � spedibile o ritirabile
    isModConsegnaOK = IsModalitaConsegnaTitoliOK(idProd)
    If Not isModConsegnaOK Then
        Call listaAttributiNonAttivabili.Add("- non � configurata alcuna modalit� di consegna dei titoli (spedibile o ritirabile);")
    End If
    requisitiMinimi = requisitiMinimi And isModConsegnaOK

' Altri controlli richiesti da LIS a fine 2012

'   in caso di vendite WEB controllare le durate riservazioni
    isDurateRisOK = AreDurateRiservazioniOK(idProd)
    If Not isDurateRisOK Then
        Call listaAttributiNonAttivabili.Add("- � configurata la vendita Internet ma non sono configurate le durate riservazioni;")
    End If
    requisitiMinimi = requisitiMinimi And isDurateRisOK

    Call ChiudiConnessioneBD
    
'   in caso di vendite WEB con titoli digitali controllare l'operazione di vendita contestuale
    isVenditaInternetTitDigOK = IsDateVenditaInternetTitDigOK(idProd)
    If Not isVenditaInternetTitDigOK Then
        Call listaAttributiNonAttivabili.Add("- � configurata la vendita Internet per titoli digitali ma non e' configurata il tipo operazione vendita contestuale;")
    End If
    requisitiMinimi = requisitiMinimi And isVenditaInternetTitDigOK
'   meglio controllare anche il resto...
'   in caso di vendite WEB con spedizione, ritiro o HT controllare l'operazione di vendita successiva
    isVenditaSenzaStampaInternetOK = IsDateVenditaSenzaStampaInternetTitDigOK(idProd)
    If Not isVenditaSenzaStampaInternetOK Then
        Call listaAttributiNonAttivabili.Add("- � configurata la vendita (ritiro, spedizione o HT) per la classe Internet ma non e' configurata il tipo operazione vendita senza stampa;")
    End If
    requisitiMinimi = requisitiMinimi And isVenditaSenzaStampaInternetOK

'   in caso di vendite WEB con titoli digitali controllare l'operazione di vendita contestuale
    listaTariffe = ""
    isTariffaSupportoDigitaleOK = isTariffaRichiedeSupportoDigitaleOK(idProd, listaTariffe)
    If Not isTariffaSupportoDigitaleOK Then
        Call listaAttributiNonAttivabili.Add("- per le tariffe: " & listaTariffe & " non sono abilitati tutti i supporti digitali necessari;")
    End If
    requisitiMinimi = requisitiMinimi And isTariffaSupportoDigitaleOK
    
'   in caso di ritiro devono essere valorizzati i campi per la modalit� di consegna
    isRitiroOK = IsDescrizioneConsegnaOK(idProd)
    If Not isRitiroOK Then
        Call listaAttributiNonAttivabili.Add("- � configurato il ritiro ma non sono configurate le modalita' di consegna;")
    End If
    requisitiMinimi = requisitiMinimi And isRitiroOK

'   in caso di vendita web devono essere valorizzate le commissioni
    isCommissioniOK = IsCommissioniWEBOK(idProd)
    If Not isCommissioniOK Then
        Call listaAttributiNonAttivabili.Add("- � configurata la vendita WEB ma non sono configurate le relative commissioni;")
    End If
    requisitiMinimi = requisitiMinimi And isCommissioniOK

'   in caso di vendita deve essere valorizzato il layout per l'invio di email
    isLayoutVendOK = IsLayoutVenditaOK(idProd)
    If Not isLayoutVendOK Then
        Call listaAttributiNonAttivabili.Add("- � configurata la vendita ma non e' configurato il layout per l'invio di email;")
    End If
    requisitiMinimi = requisitiMinimi And isLayoutVendOK

'   verifica che non ci siano ridotti maggiori di interi (forse e' gia' garantito dalla maschera dei prezzi....)
    areInteriRidottiOK = AreInteriRidottiCongruentiOK(idProd)
    If Not areInteriRidottiOK Then
        Call listaAttributiNonAttivabili.Add("- esistono ridotti di importo maggiore di interi nella stessa superarea;")
    End If
    requisitiMinimi = requisitiMinimi And areInteriRidottiOK

    If idClasseProdotto = CPR_MEDIUM Then
        ' per prodotti di classe medium verifica che tutte le superaree siano collegate a un tipo supporto
        areSuperareeMediumTypeOK = AreSuperareeCollegateAMediumType(idProd)
        If Not areSuperareeMediumTypeOK Then
            Call listaAttributiNonAttivabili.Add("- esistono superaree non collegate a tipi di supporto;")
        End If
        requisitiMinimi = requisitiMinimi And areSuperareeMediumTypeOK
        
        ' verifica che i tipi supporto siano univoci ????
        
        ' verifica che i tipi supporto collegati alle superaree e all'utilizzo dei layout siano congrui ????
        
    End If
    
' Fine controlli

    If requisitiMinimi = False Then
'    If requisitiMinimi = False And Not isProdottoSolamenteScaduto Then
        If verbose = True Then
            Call frmMessaggio.Visualizza("AvvertimentoNonAttivabilit�Prodotto", ArgomentoMessaggio(listaAttributiNonAttivabili))
        End If
    End If
    
    IsProdottoAttivabile = requisitiMinimi

'   b) la prima rappresentazione non deve essere finita
' situazione gestita diversamente con un avviso all'operatore
    If isProdottoSolamenteScaduto Then
        Call frmMessaggio.Visualizza("ConfermaAttivazioneProdottoScaduto")
        If frmMessaggio.exitCode = EC_CONFERMA Then
            IsProdottoAttivabile = True
        End If
    End If
    
End Function

Private Function PrimaRappresentazioneFinita(idProdotto As Long, idClasseProdotto As ClasseProdottoEnum) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim dataOraScadenza As Date
    Dim dataOraAttuale As Date
    
    If idClasseProdotto = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
        sql = "SELECT MIN(SCADENZASCELTA) DATAORASCADENZA,"
        sql = sql & " SYSDATE DATAORAATTUALE"
        sql = sql & " FROM"
        sql = sql & " ("
        sql = sql & " SELECT S.IDSCELTARAPPRESENTAZIONE, "
        sql = sql & " MAX(R.DATAORAINIZIO + (R.DURATAINMINUTI/1440)) SCADENZASCELTA"
        sql = sql & " FROM RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE S,"
        sql = sql & " SCELTARAPPRESENTAZIONE_RAPPR SR"
        sql = sql & " WHERE R.IDRAPPRESENTAZIONE = SR.IDRAPPRESENTAZIONE"
        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = S.IDSCELTARAPPRESENTAZIONE"
        sql = sql & " AND S.IDPRODOTTO = " & idProdotto
        sql = sql & " GROUP BY S.IDSCELTARAPPRESENTAZIONE"
        sql = sql & " )"
    Else
        sql = "SELECT MIN(R.DATAORAINIZIO + (R.DURATAINMINUTI/1440)) DATAORASCADENZA,"
        sql = sql & " SYSDATE DATAORAATTUALE"
        sql = sql & " FROM RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE PR"
        sql = sql & " WHERE PR.IDPRODOTTO = " & idProdotto
        sql = sql & " AND R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        dataOraScadenza = rec("DATAORASCADENZA")
        dataOraAttuale = rec("DATAORAATTUALE")
    End If
    rec.Close
    PrimaRappresentazioneFinita = (dataOraAttuale > dataOraScadenza)
End Function

Private Function IsFiscalitaGenereSIAECongruenti(idProdotto As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    sql = "SELECT COUNT(*) CONT" & _
        " FROM PRODOTTO P" & _
        " INNER JOIN PRODOTTO_RAPPRESENTAZIONE PR ON P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " INNER JOIN RAPPRESENTAZIONE R ON PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " INNER JOIN SPETTACOLO_GENERESIAE SG ON R.IDSPETTACOLO = SG.IDSPETTACOLO" & _
        " WHERE P.IDPRODOTTO = " & idProdotto & _
        " AND (P.FISCALE = 0 AND SG.IDGENERESIAE > 0 OR P.FISCALE = 1 AND SG.IDGENERESIAE = 0)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    IsFiscalitaGenereSIAECongruenti = (rec("CONT").Value = 0)
    rec.Close

End Function

Private Function IsAssociazioneTariffeSIAEOrdiniDiPostoSIAECorretta(idProdotto As Long, idPiantaSIAE As Long)
    Dim sql As String
    Dim verificaParzialeOk As Boolean
    Dim rec As New ADODB.Recordset
    
'   TUTTI GLI OMAGGI E SERVIZI SETA CORRISPONDONO A OMAGGI E SERVIZI SIAE, non � pi� cos� con le nuove tariffe omaggio
''    sql = "SELECT COUNT(DISTINCT PTP.IDPREZZOTITOLOPRODOTTO) CONT" & _
''        " FROM TARIFFA T, TIPOTARIFFASIAE TTS, PREZZOTITOLOPRODOTTO PTP, PRODOTTO P" & _
''        " WHERE " & _
''        " ((T.IDTIPOTARIFFA = " & T_OMAGGIO & " AND TTS.IDTIPOTARIFFASIAE <> " & IDTIPOTARIFFASIAE_OMAGGIO & ") OR" & _
''        " (T.IDTIPOTARIFFA = " & T_SERVIZI & " AND TTS.IDTIPOTARIFFASIAE <> " & IDTIPOTARIFFASIAE_SERVIZIO & "))" & _
''        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
''        " AND P.IDPIANTASIAE = " & idPiantaSIAE & _
''        " AND T.IDTARIFFA = PTP.IDTARIFFA" & _
''        " AND PTP.IDTIPOTARIFFASIAE = TTS.IDTIPOTARIFFASIAE" & _
''        " AND T.IDPRODOTTO = " & idProdotto
'    sql = "SELECT COUNT(DISTINCT PTP.IDPREZZOTITOLOPRODOTTO) CONT" & _
'        " FROM TARIFFA T, TIPOTARIFFASIAE TTS, PREZZOTITOLOPRODOTTO PTP, PRODOTTO P " & _
'        " WHERE  ((T.IDTIPOTARIFFA = " & T_OMAGGIO & " AND TTS.IDTIPOTARIFFASIAE <> " & IDTIPOTARIFFASIAE_OMAGGIO & ") OR " & _
'        " (T.IDTIPOTARIFFA = " & T_SERVIZI & " AND TTS.IDTIPOTARIFFASIAE NOT IN (" & IDTIPOTARIFFASIAE_SERVIZIO & ", " & IDTIPOTARIFFASIAE_INVITO & "))) " & _
'        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
'        " AND P.IDPIANTASIAE = " & idPiantaSIAE & _
'        " AND T.IDTARIFFA = PTP.IDTARIFFA" & _
'        " AND PTP.IDTIPOTARIFFASIAE = TTS.IDTIPOTARIFFASIAE" & _
'        " AND T.IDPRODOTTO = " & idProdotto
    sql = "SELECT COUNT(DISTINCT PTP.IDPREZZOTITOLOPRODOTTO) CONT" & _
        " FROM TARIFFA T, TIPOTARIFFASIAE TTS, PREZZOTITOLOPRODOTTO PTP, PRODOTTO P " & _
        " WHERE ((T.IDTIPOTARIFFA = " & T_OMAGGIO & " AND TTS.CODICE NOT LIKE 'O%') OR" & _
        " (T.IDTIPOTARIFFA = " & T_SERVIZI & " AND TTS.IDTIPOTARIFFASIAE NOT IN (" & IDTIPOTARIFFASIAE_SERVIZIO & ", " & IDTIPOTARIFFASIAE_INVITO & "))) " & _
        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDPIANTASIAE = " & idPiantaSIAE & _
        " AND T.IDTARIFFA = PTP.IDTARIFFA" & _
        " AND PTP.IDTIPOTARIFFASIAE = TTS.IDTIPOTARIFFASIAE" & _
        " AND T.IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    verificaParzialeOk = (rec("CONT").Value = 0)
    rec.Close

'   ESISTE UN SOLO TIPOTARIFFA SIAE = I1 PER ORDINE DI POSTO SIAE (O NON NE ESISTE NESSUNO)
    sql = "SELECT PAO.IDORDINEDIPOSTOSIAE ORD, COUNT(DISTINCT PTP.IMPORTOBASE) CONT" & _
        " FROM TARIFFA T, TIPOTARIFFASIAE TTS, PREZZOTITOLOPRODOTTO PTP," & _
        " PIANTASIAE_AREA_ORDINEDIPOSTO PAO, PRODOTTO P" & _
        " WHERE PAO.IDAREA = PTP.IDAREA" & _
        " AND PTP.IMPORTOBASE > 0" & _
        " AND TTS.IDTIPOTARIFFASIAE = " & IDTIPOTARIFFASIAE_INTERO & _
        " AND T.IDTARIFFA = PTP.IDTARIFFA" & _
        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDPIANTASIAE = " & idPiantaSIAE & _
        " AND PAO.IDPIANTASIAE = P.IDPIANTASIAE" & _
        " AND PTP.IDTIPOTARIFFASIAE = TTS.IDTIPOTARIFFASIAE" & _
        " AND T.IDPRODOTTO = " & idProdotto & _
        " GROUP BY PAO.IDORDINEDIPOSTOSIAE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            verificaParzialeOk = verificaParzialeOk And (rec("CONT").Value <= 1)
            rec.MoveNext
        Wend
    Else 'SI VERIFICA SE NON � DEFINITO ALCUN PREZZO > 0 (CIO� PER I PRODOTTI AD INVITO)
'        verificaParzialeOk = False
        verificaParzialeOk = True
    End If
    rec.Close

    IsAssociazioneTariffeSIAEOrdiniDiPostoSIAECorretta = verificaParzialeOk
End Function

Private Function IsDefinitoPeriodoCommercialePerTariffaTipoTerminale(idPeriodoCommerciale As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim esistePrezzo As Boolean
    Dim esistePrezzoTL As Boolean
    Dim esistePrezzoTW As Boolean
    
'    sql = "SELECT IMPORTOBASE, IDTIPOTARIFFALOTTO" & _
'        " FROM PREZZOTITOLOPRODOTTO, TARIFFA WHERE" & _
'        " (PREZZOTITOLOPRODOTTO.IDTARIFFA = TARIFFA.IDTARIFFA) AND" & _
'        " (PREZZOTITOLOPRODOTTO.IDPRODOTTO = " & idProd & ") AND" & _
'        " PREZZOTITOLOPRODOTTO.IDPERIODOCOMMERCIALE = " & idPerComm
    sql = "SELECT PTP.IMPORTOBASE, TTT.IDTIPOTERMINALE IDTT"
    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, TARIFFA T, TARIFFA_TIPOTERMINALE TTT"
    sql = sql & " WHERE PTP.IDTARIFFA = T.IDTARIFFA"
    sql = sql & " AND PTP.IDPERIODOCOMMERCIALE = " & idPeriodoCommerciale
    sql = sql & " AND T.IDTARIFFA = TTT.IDTARIFFA(+)"
    sql = sql & " AND TTT.IDTASTOTIPOTERMINALE IN (" & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        esistePrezzo = True
        esistePrezzoTL = False
        esistePrezzoTW = False
'        While Not rec.EOF And Not esistePrezzoTL
'            If Not IsNull(rec("IDTIPOTARIFFALOTTO")) Then
'                esistePrezzoTL = True
'            End If
'            rec.MoveNext
'        Wend
        Select Case rec("IDTT")
            Case TT_TERMINALE_LOTTO
                esistePrezzoTL = True
            Case TT_TERMINALE_WEB
                esistePrezzoTW = True
        End Select
        If Not esistePrezzoTL Then
'            Call listaAttributiNonAttivabili.Add("non sono configurati prezzi relativi a tariffe Lotto per almeno un periodo commerciale;")
        End If
    Else
        esistePrezzo = False
        esistePrezzoTL = False
'        Call listaAttributiNonAttivabili.Add("- non � configurato alcun prezzo per almeno un periodo commerciale;")
    End If
    rec.Close
End Function

Private Function IsPiantaSIAECompleta(idPiantaSIAE As Long, idPianta As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont1 As Long
    Dim cont2 As Long
    
    'NUMERO SUPERAREE DI 1� LIVELLO DELLA PIANTA (ESCLUSA QUELLA DI SERVIZIO)
'    sql = "SELECT COUNT(DISTINCT A1.IDAREA) CONT1" & _
'        " FROM AREA A1, AREA A2" & _
'        " WHERE A1.IDAREA = A2.IDAREA_PADRE" & _
'        " AND A2.IDTIPOAREA IN (" & TA_AREA_NON_NUMERATA & ", " & TA_AREA_NUMERATA & ")" & _
'        " AND A1.IDTIPOAREA = " & TA_SUPERAREA & _
'        " AND A1.IDPIANTA = " & idPianta
    sql = "SELECT COUNT(DISTINCT IDAREA) CONT1" & _
        " FROM AREA" & _
        " WHERE IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
        " AND IDPIANTA = " & idPianta
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    cont1 = rec("CONT1")
    rec.Close
        
    'NUMERO SUPERAREE incluse nella pianta SIAE
    sql = "SELECT COUNT(IDAREA) CONT2 FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
        " WHERE IDPIANTASIAE = " & idPiantaSIAE
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    cont2 = rec("CONT2")
    rec.Close
    
    IsPiantaSIAECompleta = (cont1 = cont2)
End Function

Private Function IsModalitaConsegnaTitoliOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim spedizioneTitoliPermessa As Long
    Dim ritiroTitoliPermesso As Long
    
    sql = "SELECT SPEDIZIONETITOLIPERMESSA, RITIROTITOLIPERMESSO" & _
        " FROM PRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    spedizioneTitoliPermessa = rec("SPEDIZIONETITOLIPERMESSA")
    ritiroTitoliPermesso = rec("RITIROTITOLIPERMESSO")
    rec.Close
        
    IsModalitaConsegnaTitoliOK = (spedizioneTitoliPermessa = 1 Or ritiroTitoliPermesso = 1)
End Function

Private Function AreDurateRiservazioniOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    Dim durataWeb As Long
    Dim durataWebProrogata As Long
    
    AreDurateRiservazioniOK = True
    durataWeb = 0
    durataWebProrogata = 0
    
    sql = "SELECT COUNT(*) CONT" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO_CLASSEPV_TIPOOPERAZ PCT" & _
            " WHERE CSP.IDPRODOTTO = " & idProdotto & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
            " AND PCT.IDTIPOOPERAZIONE = " & TO_RISERVAZIONE & _
            " AND PCT.IDCLASSEPUNTOVENDITA = 6"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close
        
    If cont > 0 Then
        sql = "SELECT IDCAUSALERISERVAZIONE, DURATARISERVAZIONI" & _
                " FROM PRODOTTO_CAUSALERISERVAZIONE" & _
                " WHERE IDPRODOTTO = " & idProdotto & _
                " AND IDCAUSALERISERVAZIONE IN (4, 5)"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                If rec("IDCAUSALERISERVAZIONE") = 4 Then
                    durataWeb = rec("DURATARISERVAZIONI")
                End If
                If rec("IDCAUSALERISERVAZIONE") = 5 Then
                    durataWebProrogata = rec("DURATARISERVAZIONI")
                End If
                rec.MoveNext
            Wend
            rec.Close
            AreDurateRiservazioniOK = (durataWeb > 0 And durataWebProrogata > 0)
        End If
    End If
    
End Function

Private Function IsDateVenditaInternetTitDigOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    Dim cont2 As Long
    Dim durataWeb As Long
    Dim durataWebProrogata As Long
    
    IsDateVenditaInternetTitDigOK = True
    durataWeb = 0
    durataWebProrogata = 0
    
    sql = "SELECT COUNT(*) CONT" & _
            " FROM PRODOTTO_TIPOMODALITAFORNITURA" & _
            " WHERE IDPRODOTTO = " & idProdotto & _
            " AND IDTIPOMODALITAFORNITURATITOLI = " & TMFT_TITOLI_DIGITALI & _
            " AND (IDCLASSEPUNTOVENDITA = 6 OR IDCLASSEPUNTOVENDITA IS NULL)"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close
        
    If cont > 0 Then
        sql = "SELECT COUNT(*) CONT" & _
                " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO_CLASSEPV_TIPOOPERAZ PCT" & _
                " WHERE IDPRODOTTO = " & idProdotto & _
                " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
                " AND PCT.IDTIPOOPERAZIONE = " & TO_VENDITA & _
                " AND PCT.IDCLASSEPUNTOVENDITA = 6"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        rec.MoveFirst
        cont2 = rec("CONT")
        rec.Close
        IsDateVenditaInternetTitDigOK = (cont2 > 0)
    End If
    
End Function

Private Function IsDateVenditaSenzaStampaInternetTitDigOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    Dim cont2 As Long
    Dim durataWeb As Long
    Dim durataWebProrogata As Long
    
    IsDateVenditaSenzaStampaInternetTitDigOK = True
    durataWeb = 0
    durataWebProrogata = 0
    
    sql = "SELECT COUNT(*) CONT" & _
            " FROM PRODOTTO_TIPOMODALITAFORNITURA" & _
            " WHERE IDPRODOTTO = " & idProdotto & _
            " AND IDTIPOMODALITAFORNITURATITOLI IN (" & TMFT_RITIRO & ", " & TMFT_SPEDIZIONE & ", " & TMFT_HOME_TICKETING & ")" & _
            " AND (IDCLASSEPUNTOVENDITA = 6 OR IDCLASSEPUNTOVENDITA IS NULL)"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close
        
    If cont > 0 Then
        sql = "SELECT COUNT(*) CONT" & _
                " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO_CLASSEPV_TIPOOPERAZ PCT" & _
                " WHERE IDPRODOTTO = " & idProdotto & _
                " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
                " AND PCT.IDTIPOOPERAZIONE = " & TO_VENDITA_SENZA_STAMPA & _
                " AND PCT.IDCLASSEPUNTOVENDITA = 6"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        rec.MoveFirst
        cont2 = rec("CONT")
        rec.Close
        IsDateVenditaSenzaStampaInternetTitDigOK = (cont2 > 0)
    End If
    
End Function

Private Function isTariffaRichiedeSupportoDigitaleOK(idProdotto As Long, ByRef listaTariffe As String) As Boolean
    Dim sql As String
    Dim sql2 As String
    Dim rec As New ADODB.Recordset
    Dim rec2 As New ADODB.Recordset
    Dim cont As Long
    
    isTariffaRichiedeSupportoDigitaleOK = True
    
    sql = "SELECT TF.IDTARIFFA, TT.IDTIPOSUPPORTO, TF.NOME NOMETARIFFA" & _
            " FROM TARIFFA TF, TARIFFA_TIPOSUPPORTODIGITALE TT" & _
            " WHERE TF.IDPRODOTTO = " & idProdotto & _
            " AND TF.IDTARIFFA = TT.IDTARIFFA"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
        
            sql2 = "SELECT COUNT(*) CONT" & _
                        " FROM PREZZOTITOLOPRODOTTO PTP, AREA SA," & _
                        " (" & _
                        " SELECT IDSUPERAREA" & _
                        " FROM TSDIGCONSENTITOPERSUPERAREA TSDCPSA,TIPOSUPPORTO TS" & _
                        " WHERE TSDCPSA.IDPRODOTTO = " & idProdotto & _
                        " AND TS.IDTIPOSUPPORTO = " & rec("IDTIPOSUPPORTO") & _
                        " AND (TSDCPSA.IDTIPOSUPPORTO = " & rec("IDTIPOSUPPORTO") & " OR TS.IDTIPOSUPPORTOSIAE = TSDCPSA.IDTIPOSUPPORTOSIAE)" & _
                        " ) TS" & _
                        " WHERE PTP.IDTARIFFA = " & rec("IDTARIFFA") & _
                        " AND PTP.IDAREA = SA.IDAREA" & _
                        " AND SA.IDTIPOAREA <> " & TA_SUPERAREA_SERVIZIO & _
                        " AND PTP.IDAREA = TS.IDSUPERAREA(+)" & _
                        " AND TS.IDSUPERAREA IS NULL"
            rec2.Open sql2, SETAConnection, adOpenForwardOnly, adLockOptimistic
            rec2.MoveFirst
            cont = rec2("CONT")
            rec2.Close
            If cont > 0 Then
                isTariffaRichiedeSupportoDigitaleOK = False
                If listaTariffe = "" Then
                    listaTariffe = rec("NOMETARIFFA")
                Else
                    listaTariffe = listaTariffe + ", " + rec("NOMETARIFFA")
                End If
            End If

            rec.MoveNext
        Wend
    End If

End Function

Private Function IsDescrizioneConsegnaOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    IsDescrizioneConsegnaOK = True
    
    sql = "SELECT COUNT(*) CONT" & _
            " FROM PRODOTTO_TIPOMODALITAFORNITURA" & _
            " WHERE IDPRODOTTO = " & idProdotto & _
            " AND IDTIPOMODALITAFORNITURATITOLI = " & TMFT_RITIRO
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close
        
    If cont > 0 Then
        sql = "SELECT MODALITACONSTITSTAMPSUCC_ITA, MODALITACONSTITSTAMPSUCC_ING" & _
                " FROM PRODOTTO" & _
                " WHERE IDPRODOTTO = " & idProdotto
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        rec.MoveFirst
        If IsNull(rec("MODALITACONSTITSTAMPSUCC_ITA")) Or IsNull(rec("MODALITACONSTITSTAMPSUCC_ING")) Then
            IsDescrizioneConsegnaOK = False
        End If
        rec.Close
    End If
    
End Function

Private Function IsCommissioniWEBOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    IsCommissioniWEBOK = True
    
    sql = "SELECT COUNT(*) CONT" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO_CLASSEPV_TIPOOPERAZ PCT" & _
            " WHERE IDPRODOTTO = " & idProdotto & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
            " AND (PCT.IDTIPOOPERAZIONE = 3 or PCT.IDTIPOOPERAZIONE = 11)" & _
            " AND PCT.IDCLASSEPUNTOVENDITA = 6"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close

    If (cont > 0) Then
        sql = "SELECT COUNT(*) CONT" & _
                " FROM SETA_REP.COMMISSIONE C, PRODOTTO P" & _
                " WHERE P.IDPRODOTTO = " & idProdotto & _
                " AND P.IDORGANIZZAZIONE = C.IDORGANIZZAZIONE" & _
                " AND SYSDATE BETWEEN TO_DATE(DATAINIZIO, 'YYYYMMDD') AND TO_DATE(DATAFINE, 'YYYYMMDD')"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        rec.MoveFirst
        cont = rec("CONT")
        rec.Close
            
        If cont = 0 Then
            IsCommissioniWEBOK = False
        End If
    End If
    
End Function

Private Function IsLayoutVenditaOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    IsLayoutVenditaOK = False

    sql = "SELECT COUNT(*) CONT" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO_CLASSEPV_TIPOOPERAZ PCT" & _
            " WHERE IDPRODOTTO = " & idProdotto & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
            " AND (PCT.IDTIPOOPERAZIONE = 3 or PCT.IDTIPOOPERAZIONE = 11)" & _
            " AND PCT.IDCLASSEPUNTOVENDITA = 6"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close

    If cont = 0 Then
        IsLayoutVenditaOK = True
    End If
    
    If cont > 0 Then
        sql = "SELECT IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING" & _
                " FROM PRODOTTO_LAYOUTALFRESCO" & _
                " WHERE IDPRODOTTO = " & idProdotto
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
                If Not IsNull(rec("IDLAYOUTALFRESCO_ITA")) And Not IsNull(rec("IDLAYOUTALFRESCO_ING")) Then
                    IsLayoutVenditaOK = True
                End If
            rec.Close
        End If
    End If
    
End Function

Private Function AreInteriRidottiCongruentiOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    AreInteriRidottiCongruentiOK = True
    
    sql = "SELECT COUNT(*) CONT" & _
            " FROM TARIFFA TI, TARIFFA TR, PREZZOTITOLOPRODOTTO PTPI, PREZZOTITOLOPRODOTTO PTPR" & _
            " WHERE TI.IDPRODOTTO = " & idProdotto & _
            " AND TR.IDPRODOTTO = " & idProdotto & _
            " AND PTPI.IDTIPOTARIFFASIAE = 1" & _
            " AND TR.IDTIPOTARIFFA = " & T_RIDOTTI & _
            " AND TI.IDTARIFFA = PTPI.IDTARIFFA" & _
            " AND TR.IDTARIFFA = PTPR.IDTARIFFA" & _
            " AND PTPI.IDAREA = PTPR.IDAREA" & _
            " AND PTPR.IMPORTOBASE > PTPI.IMPORTOBASE"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("CONT")
    rec.Close
        
    If cont > 0 Then
        AreInteriRidottiCongruentiOK = False
    End If
    
End Function

Private Function AreSuperareeCollegateAMediumType(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    AreSuperareeCollegateAMediumType = True
    
    sql = "SELECT COUNT(*) QTA" & _
            " FROM (" & _
            " SELECT P.IDPRODOTTO, P.NOME, A.IDAREA, A.NOME, COUNT(STSD.IDTIPOSUPPORTODIGITALE) QTA" & _
            " FROM PRODOTTO P" & _
            " INNER JOIN AREA A ON P.IDPIANTA = A.IDPIANTA" & _
            " LEFT OUTER JOIN SUPERAREA_TIPOSUPPORTODIGITALE STSD ON A.IDAREA = STSD.IDSUPERAREA" & _
            " WHERE P.IDPRODOTTO = " & idProdotto & _
            " AND A.IDTIPOAREA = " & TA_SUPERAREA_NUMERATA & _
            " GROUP BY P.IDPRODOTTO, P.NOME, A.IDAREA, A.NOME" & _
            " HAVING COUNT(STSD.IDTIPOSUPPORTODIGITALE) <> 1" & _
            ")"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    cont = rec("QTA")
    rec.Close
        
    If cont > 0 Then
        AreSuperareeCollegateAMediumType = False
    End If

End Function

Public Function IsParametroDecretoSicurezzaOK(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idGenereSIAEPredominante As Long
    Dim rientraDecretoSicurezza As Long
    
    IsParametroDecretoSicurezzaOK = True
    
    sql = "SELECT IDGENERESIAEPREDOMINANTE, RIENTRADECRETOSICUREZZA" & _
        " FROM PRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    rec.MoveFirst
    idGenereSIAEPredominante = rec("IDGENERESIAEPREDOMINANTE")
    rientraDecretoSicurezza = rec("RIENTRADECRETOSICUREZZA")
    rec.Close

    If idGenereSIAEPredominante = IDGENERESIAE_CALCIO_AB_INTERN Or idGenereSIAEPredominante = IDGENERESIAE_CALCIO_C_INF Then
        If rientraDecretoSicurezza = 0 Then
            IsParametroDecretoSicurezzaOK = False
        End If
    End If
End Function

Public Function idAreaPadre(idAreaFiglia As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    sql = "SELECT AREA.IDAREA_PADRE IDP FROM AREA WHERE" & _
        " AREA.IDAREA = " & idAreaFiglia
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        id = IIf(IsNull(rec("IDP")), idNessunElementoSelezionato, rec("IDP"))
    End If
    rec.Close
    idAreaPadre = id
End Function

Public Function StringaConPuntoDecimale(nin As Double) As String
    Dim sIn As String
    Dim sout As String
    Dim c As String
    Dim l As Integer
    Dim i As Integer
    
    sIn = CStr(nin)
    l = Len(sIn)
    For i = 1 To l
        c = Mid$(sIn, i, 1)
        If c = Chr(44) Then
            sout = sout & Chr(46)
        Else
            sout = sout & c
        End If
    Next i
    StringaConPuntoDecimale = sout
End Function

Public Function VisualizzaDataGridToolTip(dgr As DataGrid, testo As String)
    dgr.ToolTipText = testo
End Function

Public Function VisualizzaListBoxToolTip(lst As listBox, testo As String)
    lst.ToolTipText = testo
End Function

Public Function VisualizzaComboBoxToolTip(cmb As ComboBox, testo As String)
    cmb.ToolTipText = testo
End Function

Public Function DefinisciCampoStringa(f As ADODB.Field) As String
    If IsNull(f) Then
        DefinisciCampoStringa = "NULL"
    Else
        DefinisciCampoStringa = "'" & StringaConApiciRaddoppiati(f.Value) & "'"
    End If
End Function

Public Function DefinisciCampoStringaORADB(f As OraField) As String
    If IsNull(f) Then
        DefinisciCampoStringaORADB = "NULL"
    Else
        DefinisciCampoStringaORADB = "'" & StringaConApiciRaddoppiati(f.Value) & "'"
    End If
End Function

Public Function DefinisciCampoFloat(f As ADODB.Field) As String
    If IsNull(f) Then
        DefinisciCampoFloat = "NULL"
    Else
        DefinisciCampoFloat = StringaConPuntoDecimale(f.Value)
    End If
End Function

Public Function StringaLunghezzaMaxFissata(sIn As String, lungh As Integer) As String
    
    StringaLunghezzaMaxFissata = Left$(sIn, lungh)
End Function

Public Function StringaLunghezzaMaxFissataAllineataADestra(sIn As String, lungh As Integer) As String
    
    StringaLunghezzaMaxFissataAllineataADestra = Right$(sIn, lungh)
End Function

Public Function StringaAlfanumerica(sIn As String) As String
    Dim i As Integer
    Dim c As String
    Dim p As String
    Dim sout As String
    Dim l As Long
    Dim appo As String
    
    sout = ""
    l = Len(sIn)
'    If IsNumeric(Left$(sIn, 1)) Then
'        sIn = Right$(sIn, l - 2) 'se il primo carattere � un numero, si tolgono i primi due caratteri (es.: 9_ingresso_abbonati diventa ingresso_abbonati)
'    End If
    If IsNumeric(Left$(sIn, 1)) Then
        appo = "Z" & sIn
        sIn = appo
    End If
    For i = 1 To Len(sIn)
        c = Mid(sIn, i, 1)
        If c >= Chr(Asc("a")) And c <= Chr(Asc("z")) Or _
            c >= Chr(Asc("A")) And c <= Chr(Asc("Z")) Or _
            c >= Chr(Asc("0")) And c <= Chr(Asc("9")) Then
            sout = sout & c
        Else
            sout = sout & "_"
        End If
    Next i
    
    StringaAlfanumerica = sout
End Function

Public Function IsUltimoNomeFilaCorretto(nome1F As String, numeroF As Integer, st As Integer) As Boolean
    Dim a As String
    Dim l As Integer
    Dim ultimoNumeroFila As Long
    Dim valoreFunzione As Boolean

    valoreFunzione = True
    a = Mid$(Trim(UCase(nome1F)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        ultimoNumeroFila = CLng(nome1F) + (numeroF - 1) * st
        If ultimoNumeroFila > 99 Then
            valoreFunzione = False
        End If
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        ultimoNumeroFila = CLng(Asc(a)) + (numeroF - 1) * st
        If ultimoNumeroFila > 90 Then
            valoreFunzione = False
        End If
    End If
    IsUltimoNomeFilaCorretto = valoreFunzione
End Function

Public Function IsUltimoNomePostoCorretto(nome1P As String, numeroP As Integer, st As Integer, nomiPostoLunghiPermessi As Boolean) As Boolean
    Dim a As String
    Dim l As Integer
    Dim ultimoNumeroPosto As Long
    Dim valoreFunzione As Boolean

    valoreFunzione = True
    a = Mid$(Trim(UCase(nome1P)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        ultimoNumeroPosto = CLng(nome1P) + (numeroP - 1) * st
        If nomiPostoLunghiPermessi Then
            If ultimoNumeroPosto > 9999 Then
                valoreFunzione = False
            End If
        Else
            If ultimoNumeroPosto > 999 Then
                valoreFunzione = False
            End If
        End If
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        ultimoNumeroPosto = CLng(Asc(a)) + (numeroP - 1) * st
        If nomiPostoLunghiPermessi Then
            If ultimoNumeroPosto > 900 Then
                valoreFunzione = False
            End If
        Else
            If ultimoNumeroPosto > 90 Then   'perche' non 99?
                valoreFunzione = False
            End If
        End If
    End If
    IsUltimoNomePostoCorretto = valoreFunzione
End Function

Public Function IsProdottoTDL(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset

    IsProdottoTDL = False
    
    ' 27 aprile: Commentata in attesa della versione TDL
'    Call ApriConnessioneBD
'
'    sql = "SELECT COUNT(*) CONT FROM TDLPRODOTTO WHERE IDPRODOTTO = " & idProdotto
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If rec("CONT") = 0 Then
'        IsProdottoTDL = False
'    Else
'        IsProdottoTDL = True
'    End If
'    Call ChiudiConnessioneBD
End Function

Public Function IsRecordEliminabile(NomeCampo As String, tabellaCorrelata As String, nomeRecord As String, Optional condizione As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
    sql = "SELECT * FROM " & tabellaCorrelata & " WHERE " & _
          NomeCampo & " = " & nomeRecord
    If Not IsMissing(condizione) Then sql = sql & condizione
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        IsRecordEliminabile = False
    Else
        IsRecordEliminabile = True
    End If
    Call ChiudiConnessioneBD
End Function

Public Function IsProdottoEditabile_old(idP As Long, causaNonEditabilita As String) As Boolean
    Dim isInConfigurazione As Boolean
    Dim strDescr As String
    
    isInConfigurazione = (tipoStatoProdotto = TSP_IN_CONFIGURAZIONE)
    If Not isInConfigurazione Then causaNonEditabilita = "il prodotto non � nello stato 'in configurazione'"
    If Not isProdottoBloccatoDaUtente Then
        strDescr = "il prodotto � in lavorazione da parte di un altro utente"
        causaNonEditabilita = IIf(causaNonEditabilita = "", strDescr, "" & Chr(10) & Chr(13) & strDescr)
    End If
    
    IsProdottoEditabile_old = isInConfigurazione And isProdottoBloccatoDaUtente
End Function

Public Function IsProdottoEditabile(idP As Long, causaNonEditabilita As String) As Boolean
    Dim isAttivabile As Boolean
    Dim strDescr As String
    
    isAttivabile = (tipoStatoProdotto = TSP_ATTIVABILE)
    If isAttivabile Then causaNonEditabilita = "il prodotto � nello stato 'Attivabile'; per modificare i dati configurati portarlo allo stato 'in Configurazione'."
    If Not isProdottoBloccatoDaUtente Then
        strDescr = "il prodotto � in lavorazione da parte di un altro utente"
        causaNonEditabilita = IIf(causaNonEditabilita = "", strDescr, "" & Chr(10) & Chr(13) & strDescr)
    End If
    
    IsProdottoEditabile = Not isAttivabile And isProdottoBloccatoDaUtente
End Function

'Public Sub EliminaPianta(idPianta As Long)
'    Dim sql As String
'
'    sql = "delete from posto where idarea in (select idarea from area where idpianta = " & idPianta & ");" & _
'        "delete from sequenzaposti where idfasciaposti in (select idfasciaposti from fasciaposti where idarea in (select distinct idarea from area where idpianta = " & idPianta & "));" & _
'        "delete from fasciaposti where idarea in (select distinct idarea from area where idpianta = " & idPianta & ");" & _
'        "delete from grigliaposti where idarea in (select idarea from area where idpianta = " & idPianta & ") or idpianta = " & idPianta & ";" & _
'        "delete from area_varco where idarea in (select idarea from area where idpianta = " & idPianta & ");" & _
'        "delete from varco where idpianta = " & idPianta & ";" & _
'        "delete from area where idpianta = " & idPianta & ";" & _
'        "delete from organizzazione_pianta where idpianta = " & idPianta & ";" & _
'        "delete from venue_pianta where idpianta = " & idPianta & ";" & _
'        "delete from pianta where idpianta = " & idPianta & ";" & _
'        "commit;"
'    SETAConnection.Execute sql, , adCmdText
'End Sub

''chiamata della API
Public Function getNomeMacchina() As String
    Dim lunghezza As Long
    Dim BufNome As String * 256
    Dim NomeMacchina As String

    lunghezza = 256
    Call GetComputerName(BufNome, lunghezza)
    NomeMacchina = Left(BufNome, lunghezza)

    getNomeMacchina = Replace(NomeMacchina, "-", "_")
End Function

Public Sub ScriviLog(idTipoAtt As CCTipoAttivit�Enum, idDomAtt As CCDominioAttivit�Enum, _
                    Optional idSubDomAtt As CCDominioAttivit�Enum, _
                    Optional nota As String, _
                    Optional idRecordLavorato As Long)
    Dim sql As String
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    
    If IsMissing(nota) Then
        nota = "NULL"
    Else
        nota = "'" & getNomeMacchina & " - " & StringaConApiciRaddoppiati(nota) & "'"
    End If
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        sql = "INSERT INTO CC_LOG (DATAORA, IDTIPOATTIVITA, IDDOMINIOATTIVITA," & _
'            " IDSOTTODOMINIOATTIVITA, UTENTE, IDSESSIONECONFIGURAZIONE, IDRECORDLAVORATO, NOTE)" & _
'            " VALUES (SYSDATE, " & _
'            idTipoAtt & ", " & _
'            idDomAtt & ", " & _
'            idSubDomAtt & ", '" & _
'            StringaConApiciRaddoppiati(userID) & "', " & _
'            IIf(idSessioneConfigurazioneCorrente = idNessunElementoSelezionato, "NULL", idSessioneConfigurazioneCorrente) & ", " & _
'            idRecordLavorato & ", " & _
'            nota & ")"
'    Else
        sql = "INSERT INTO CC_LOG (DATAORA, IDTIPOATTIVITA, IDDOMINIOATTIVITA," & _
            " IDSOTTODOMINIOATTIVITA, UTENTE, NOTE)" & _
            " VALUES (SYSDATE, " & _
            idTipoAtt & ", " & _
            idDomAtt & ", " & _
            idSubDomAtt & ", '" & _
            StringaConApiciRaddoppiati(userID) & "', " & _
            nota & ")"
'    End If
    SETAConnection.Execute sql, , adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreScritturaLog")
End Sub

Public Sub BloccaDominioPerUtente(nomeDom As CCDominioAttivit�Enum, _
                                    idRec As Long, _
                                    isDominioBloccatoDaUtente As Boolean)
'L'ULTIMO PARAMETRO E' isProdottoBloccato OPPURE isAreaBloccata PASSATI ByRef.
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim rec1 As New ADODB.Recordset
    Dim sql1 As String
    Dim sqlTrans As String
    Dim numRec As Integer
    Dim dataOraAttuale As Date
    Dim nomeTab As String
    Dim nomeId As String
    
    Select Case nomeDom
        Case CCDA_AREA, CCDA_SUPERAREA
            nomeTab = "CC_AREAUTILIZZATA"
            nomeId = "IDAREA"
        Case CCDA_PRODOTTO
            nomeTab = "CC_PRODOTTOUTILIZZATO"
            nomeId = "IDPRODOTTO"
        Case CCDA_ORGANIZZAZIONE
            nomeTab = "CC_ORGANIZZAZIONEUTILIZZATA"
            nomeId = "IDORGANIZZAZIONE"
        Case CCDA_PIANTA
            nomeTab = "CC_PIANTAUTILIZZATA"
            nomeId = "IDPIANTA"
        Case CCDA_OFFERTA_PACCHETTO
            nomeTab = "CC_OFFERTAPACCHETTOUTILIZZATA"
            nomeId = "IDOFFERTAPACCHETTO"
        Case Else
    End Select
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    isDominioBloccatoDaUtente = False
    SETAConnection.BeginTrans
    sqlTrans = "LOCK TABLE " & nomeTab & " IN EXCLUSIVE MODE"
    SETAConnection.Execute sqlTrans, , adCmdText
    
    Call BloccaTabella(nomeTab, nomeId, idRec, isDominioBloccatoDaUtente)
'    dataOraAttuale = FormatDateTime(Now, vbGeneralDate)
''    sql = "SELECT COUNT(*) NUMREC FROM " & nomeTab & _
''        " WHERE " & nomeId & " = " & idRec & _
''        " AND UTENTE <> '" & StringaConApiciRaddoppiati(userID) & "'"
'    sql = "SELECT COUNT(*) NUMREC FROM " & nomeTab & _
'        " WHERE " & nomeId & " = " & idRec & _
'        " AND UTENTE <> '" & StringaConApiciRaddoppiati(getNomeMacchina) & "'"
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    numRec = rec("NUMREC")
'    If numRec = 0 Then
''        sql1 = "INSERT INTO " & nomeTab & " (DATAORA, " & nomeId & ", UTENTE)" & _
''            " VALUES (" & SqlDateTimeValue(dataOraAttuale) & ", " & _
''            idRec & ", '" & StringaConApiciRaddoppiati(userID) & "')"
'        sql1 = "INSERT INTO " & nomeTab & " (DATAORA, " & nomeId & ", UTENTE)" & _
'            " VALUES (" & SqlDateTimeValue(dataOraAttuale) & ", " & _
'            idRec & ", '" & StringaConApiciRaddoppiati(getNomeMacchina) & "')"
'        SETAConnection.Execute sql1, , adCmdText
'        isDominioBloccatoDaUtente = True
'    End If
'    rec.Close
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
End Sub

Public Sub BloccaTabella(nomeTab As String, nomeId As String, idRec As Long, isLocked As Boolean)
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim sql1 As String
    Dim numRec As Integer

    sql = "SELECT COUNT(*) NUMREC FROM " & nomeTab & _
        " WHERE " & nomeId & " = " & idRec & _
        " AND UTENTE <> '" & StringaConApiciRaddoppiati(getNomeMacchina) & "'"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numRec = rec("NUMREC")
    If numRec = 0 Then
        sql1 = "INSERT INTO " & nomeTab & " (DATAORA, " & nomeId & ", UTENTE)" & _
            " VALUES (SYSDATE, " & _
            idRec & ", '" & StringaConApiciRaddoppiati(getNomeMacchina) & "')"
        SETAConnection.Execute sql1, , adCmdText
'        isDominioBloccatoDaUtente = True
        isLocked = True
    End If
    rec.Close
End Sub

Public Sub SbloccaDominioPerUtente(nomeDom As CCDominioAttivit�Enum, _
                                    idRec As Long, _
                                    isDominioBloccatoDaUtente As Boolean)
'L'ULTIMO PARAMETRO E' isProdottoBloccato OPPURE isAreaBloccata PASSATI ByRef.
    Dim sql As String
    Dim nomeTab As String
    Dim nomeId As String
    Dim n As Long
    
    Select Case nomeDom
        Case CCDA_AREA, CCDA_SUPERAREA
            nomeTab = "CC_AREAUTILIZZATA"
            nomeId = "IDAREA"
        Case CCDA_PRODOTTO
            nomeTab = "CC_PRODOTTOUTILIZZATO"
            nomeId = "IDPRODOTTO"
        Case CCDA_ORGANIZZAZIONE
            nomeTab = "CC_ORGANIZZAZIONEUTILIZZATA"
            nomeId = "IDORGANIZZAZIONE"
        Case CCDA_PIANTA
            nomeTab = "CC_PIANTAUTILIZZATA"
            nomeId = "IDPIANTA"
        Case CCDA_OFFERTA_PACCHETTO
            nomeTab = "CC_OFFERTAPACCHETTOUTILIZZATA"
            nomeId = "IDOFFERTAPACCHETTO"
        Case Else
    End Select
    
    Call ApriConnessioneBD
    
'VERSIONE A: ALL'USCITA VIENE CANCELLATO UN SOLO RECORD
'(MA, CHE SUCCEDE SE PRIMA DI USCIRE HO INSERITO E POI MODIFICATO UN'AREA?
'MAGARI NON LA STESSA AREA?)
    sql = "DELETE FROM " & nomeTab
    sql = sql & " WHERE " & nomeId & " = " & idRec
    sql = sql & " AND UTENTE = '" & StringaConApiciRaddoppiati(getNomeMacchina) & "'"
'VERSIONE 2: ALL'USCITA VENGONO CANCELLATI TUTTI I RECORD RELATIVI A QUELL'UTENTE
'    sql = "DELETE FROM " & nomeTab & _
'        " WHERE UTENTE = '" & StringaConApiciRaddoppiati(getNomeMacchina) & "'"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    isDominioBloccatoDaUtente = False
End Sub

Public Sub VisualizzaLog(Optional filtro As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    If IsMissing(filtro) Then filtro = ""
    sql = "SELECT L.DATAORA, L.UTENTE, TA.NOME ATTIVITA, DA1.NOME DOMINIO, DA2.NOME SUBDOMINIO, L.NOTE " & _
       " FROM CC_LOG L," & _
       " CC_TIPOATTIVITA TA," & _
       " CC_DOMINIOATTIVITA DA1," & _
       " CC_DOMINIOATTIVITA DA2" & _
       " WHERE L.IDTIPOATTIVITA = TA.IDTIPOATTIVITA" & _
       " AND L.IDDOMINIOATTIVITA = DA1.IDDOMINIOATTIVITA" & _
       " AND L.IDSOTTODOMINIOATTIVITA = DA2.IDDOMINIOATTIVITA" & _
       filtro & _
       " ORDER BY DATAORA DESC"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    
End Sub

'        sql = "DELETE FROM POSTO WHERE IDAREA = " & idRecordSelezionato
' cancella da una tabella
' la condizione deve essere del tipo "where id=...", ossia basata su un long
' e deve essere fornito un ulteriore id che serva per poter frammentare la query
' Esempio:
'   nomeTabella: POSTO
'   nomeCampoCondizione: IDAREA
'   nomeCampoIntervallo: IDPOSTO
' non deve fare una transazione, in quanto deve essere fatta da chi chiama!
Public Function CancellaRecord_old(conn As ADODB.Connection, nomeTabella As String, nomeCampoCondizione As String, valoreCampoCondizione As Long, nomeCampoIntervallo As String) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim minimo As Long
    Dim massimo As Long
    Dim primoId As Long
    Dim numeroRecordCancellati As Long
    Dim numeroTotaleRecordCancellati As Long
    
    sql = "SELECT MIN(" & nomeCampoIntervallo & ") AS MINIMO,"
    sql = sql & " MAX(" & nomeCampoIntervallo & ") AS MASSIMO"
    sql = sql & " FROM " & nomeTabella
    sql = sql & " WHERE " & nomeCampoCondizione & " = " & valoreCampoCondizione
    rec.Open sql, conn, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        minimo = IIf(IsNull(rec("MINIMO")), 1, rec("MINIMO"))
        massimo = IIf(IsNull(rec("MASSIMO")), 0, rec("MASSIMO"))
    End If
    
    ' adesso cancella 100 righe per volta
    primoId = minimo
    numeroTotaleRecordCancellati = 0
    While primoId <= massimo
        sql = "DELETE FROM " & nomeTabella
        sql = sql & " WHERE " & nomeCampoCondizione & " = " & valoreCampoCondizione
        sql = sql & " AND " & nomeCampoIntervallo & " BETWEEN " & primoId & " AND " & primoId + 100
        conn.Execute sql, numeroRecordCancellati, adCmdText
        
        numeroTotaleRecordCancellati = numeroTotaleRecordCancellati + numeroRecordCancellati
        primoId = primoId + 100
    Wend
    
    CancellaRecord_old = numeroTotaleRecordCancellati
End Function

'Public Function NumeroRappresentazioniAssociateAProdotto(idProdotto As Long) As Long
'    Dim rec As New ADODB.Recordset
'    Dim sql As String
'    Dim cont As Long
'
'    sql = "SELECT COUNT(*) NUMERO FROM PRODOTTO_RAPPRESENTAZIONE" & _
'        " WHERE IDPRODOTTO = " & idProdotto
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    cont = rec("NUMERO")
'    rec.Close
'    NumeroRappresentazioniAssociateAProdotto = cont
'End Function

Public Function ProdottiCorrelandiBloccati(idRappresentazione As Long, idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim isLocked  As Boolean
    Dim bloccati  As Boolean
    Dim idP As Long
    Dim utente As String
    
    utente = getNomeMacchina
    bloccati = True
    sql = " SELECT P.IDPRODOTTO IDPROD FROM" & _
        " PRODOTTO P," & _
        " (SELECT IDPRODOTTOB IDPRODOTTO FROM PRODOTTO_PRODOTTO WHERE IDPRODOTTOA = " & idProdotto & ") PC," & _
        " (SELECT IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE where IDRAPPRESENTAZIONE = " & idRappresentazione & ") PR" & _
        " WHERE P.IDPRODOTTO = PC.IDPRODOTTO(+) AND PC.IDPRODOTTO IS NULL" & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idP = rec("IDPROD")
            isLocked = False
            Call BloccaTabella("CC_PRODOTTOUTILIZZATO", "IDPRODOTTO", idP, isLocked)
            bloccati = bloccati And isLocked
            rec.MoveNext
        Wend
    End If
    rec.Close
    ProdottiCorrelandiBloccati = bloccati
End Function

Public Function ElencoProdottiCorrelandi(idProdotto As Long, idRappresentazione As Long) As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elenco As String
    Dim idProd As Long
    
    elenco = ""
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
''NOTA: LA VISTA PRODOTTO_PRODOTTO NON � TRANSAZIONALE; APPENA RESA TRANSAZIONALE
''LA QUERY QUI SOTTO VA MODIFICATA RENDENDO TRANSAZIONALI ANCHE LE SOTTOQUERY
'        sql = "SELECT P.IDPRODOTTO IDPROD" & _
'            " FROM PRODOTTO P," & _
'            " (SELECT IDPRODOTTOB IDPRODOTTO FROM PRODOTTO_PRODOTTO WHERE IDPRODOTTOA = " & idProdotto & ") PC," & _
'            " (SELECT IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE WHERE IDRAPPRESENTAZIONE = " & idRappresentazione & ") PR" & _
'            " WHERE P.IDPRODOTTO = PC.IDPRODOTTO(+) AND PC.IDPRODOTTO IS NULL" & _
'            " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
'            " AND (P.IDTIPOSTATORECORD IS NULL" & _
'            " OR P.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT P.IDPRODOTTO IDPROD" & _
            " FROM PRODOTTO P," & _
            " (SELECT IDPRODOTTOB IDPRODOTTO FROM PRODOTTO_PRODOTTO WHERE IDPRODOTTOA = " & idProdotto & ") PC," & _
            " (SELECT IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE WHERE IDRAPPRESENTAZIONE = " & idRappresentazione & ") PR" & _
            " WHERE P.IDPRODOTTO = PC.IDPRODOTTO(+) AND PC.IDPRODOTTO IS NULL" & _
            " AND P.IDPRODOTTO = PR.IDPRODOTTO"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idProd = rec("IDPROD")
            elenco = IIf(elenco = "", "(" & idProd, elenco & ", " & idProd)
            rec.MoveNext
        Wend
        elenco = elenco & ")"
    End If
    rec.Close
    ElencoProdottiCorrelandi = elenco
End Function

Public Sub SbloccaProdottiCorrelandi(utente As String, idProdotto As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idP As Long
    
'    sql = "SELECT IDPRODOTTO" & _
'        " FROM PRODOTTO_RAPPRESENTAZIONE" & _
'        " WHERE IDRAPPRESENTAZIONE = " & idRappr & _
'        " AND IDPRODOTTO <> " & idProdottoCorrente
    sql = "SELECT IDPRODOTTO FROM CC_PRODOTTOUTILIZZATO" & _
        " WHERE UTENTE = '" & utente & "'" & _
        " AND IDPRODOTTO <> " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idP = rec("IDPRODOTTO")
            Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idP, True)
            rec.MoveNext
        Wend
    End If
    rec.Close
End Sub

Public Function IsOrganizzazioneEditabile(idO As Long, causaNonEditabilita As String) As Boolean
    Dim isAttivabile As Boolean
    Dim strDescr As String
    
    isAttivabile = (tipoStatoOrganizzazione = TSO_ATTIVABILE)
    If isAttivabile Then causaNonEditabilita = "l'organizzazione � nello stato 'Attivabile'; per modificare i dati configurati portarla allo stato 'in Configurazione'."
    If Not isOrganizzazioneBloccataDaUtente Then
        strDescr = "l'organizzazione � in lavorazione da parte di un altro utente"
        causaNonEditabilita = IIf(causaNonEditabilita = "", strDescr, "" & Chr(10) & Chr(13) & strDescr)
    End If
    
    IsOrganizzazioneEditabile = Not isAttivabile And isOrganizzazioneBloccataDaUtente
End Function

Public Function IsOrganizzazioneEditabile_OLD(idO As Long, causaNonEditabilita As String) As Boolean
    Dim isInConfigurazione As Boolean
'    Dim isInLavorazione As Boolean
    Dim strDescr As String
    
    isInConfigurazione = (tipoStatoOrganizzazione = TSO_IN_CONFIGURAZIONE)
    If Not isInConfigurazione Then causaNonEditabilita = "l'organizzazione non � nello stato 'in configurazione'"
    If Not isOrganizzazioneBloccataDaUtente Then
        strDescr = "l'organizzazione � in lavorazione da parte di un altro utente"
        causaNonEditabilita = IIf(causaNonEditabilita = "", strDescr, "" & Chr(10) & Chr(13) & strDescr)
    End If
    
    IsOrganizzazioneEditabile_OLD = isInConfigurazione And isOrganizzazioneBloccataDaUtente
End Function

Public Function CompletaNomeConSpazi(sIn As String, lunghezza As Integer) As String
    Dim c As String
    
    c = Trim(sIn)
    c = "    " & sIn
    c = Right$(c, lunghezza)
    CompletaNomeConSpazi = c
End Function

Private Function tempoTrascorsoInMillisecondi(tempoIniziale As SYSTEMTIME, tempoFinale As SYSTEMTIME) As Long
    Dim tempoInizialeInMillsec As Long
    Dim tempoFinaleInMillsec As Long
    Dim lngSecondiIniziali As Long
    Dim lngSecondiFinali As Long
    Dim tempoTrascorso As Long

    lngSecondiIniziali = CLng(tempoIniziale.wSecond)
    lngSecondiFinali = CLng(tempoFinale.wSecond)
    
    tempoInizialeInMillsec = ((lngSecondiIniziali * 1000) + tempoIniziale.wMilliseconds)
    tempoFinaleInMillsec = ((lngSecondiFinali * 1000) + tempoFinale.wMilliseconds)
    tempoTrascorsoInMillisecondi = tempoFinaleInMillsec - tempoInizialeInMillsec
End Function

Public Sub RipristinaConnessione()
    Dim sql As String
    
    sql = "SELECT SYSDATE FROM DUAL"
    SETAConnection.Execute sql, , adCmdText
End Sub

Public Function ValoreCampo_string(RS As Recordset, NomeCampo As String) As String
    ValoreCampo_string = IIf(IsNull(RS(NomeCampo)), VALORE_NULLO_TESTO, RS(NomeCampo))
End Function

Public Sub AggiornaParametriSessioneSuRecord(nomeTabella As String, _
                                            NomeCampo As String, _
                                            idRecord As Long, _
                                            tipoStatoRecord As TipoStatoRecordEnum, _
                                            Optional condizioniSQL As String)
    Dim sql As String
    Dim n As Long

    sql = "UPDATE " & nomeTabella & " SET" & _
        " IDTIPOSTATORECORD = " & tipoStatoRecord & "," & _
        " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
        " WHERE " & NomeCampo & " = " & idRecord
    If Not IsMissing(condizioniSQL) Then sql = sql & condizioniSQL
    SETAConnection.Execute sql, n, adCmdText
End Sub
'
'Public Sub AggiornaParametriSessionePerListaProdotti(listaProdotti As Collection, _
'                                            tipoStatoRecord As TipoStatoRecordEnum)
'    Dim sql As String
'    Dim n As Long
'
'    sql = "UPDATE PRODOTTO SET" & _
'        " IDTIPOSTATORECORD = " & tipoStatoRecord & "," & _
'        " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'        " WHERE IDPRODOTTO IN (" & idRecord
'    SETAConnection.Execute sql, n, adCmdText
'End Sub

Public Function SqlStringTableName(sIn As String) As String
    SqlStringTableName = Left(sIn, 30)
End Function

Public Function IsProdottoAdInvito(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim incidenza As Integer
    
    Call ApriConnessioneBD
    
'   bisognerebbe anche verificare che sia l'unica rappresentazione del prodotto
    sql = "SELECT INCIDENZA " & _
        " FROM PRODOTTO_RAPPRESENTAZIONE PR, SPETTACOLO_GENERESIAE SG, RAPPRESENTAZIONE R" & _
        " WHERE R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE" & _
        " AND PR.IDPRODOTTO = " & idProdotto & _
        " AND R.IDSPETTACOLO = SG.IDSPETTACOLO" & _
        " AND SG.IDGENERESIAE = " & IDGENERESIAE_INVITO
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        incidenza = rec("INCIDENZA").Value
    Else
        incidenza = 0
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IsProdottoAdInvito = (incidenza = 100)
End Function

Public Function IsGestioneEccedenzeOmaggioEvoluta(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim gestioneEvoluta As Integer
    
    Call ApriConnessioneBD
    
    sql = "SELECT GESTECCEDENZEOMAGGIOEVOLUTA " & _
        " FROM PRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        gestioneEvoluta = rec("GESTECCEDENZEOMAGGIOEVOLUTA").Value
    Else
        gestioneEvoluta = 0
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IsGestioneEccedenzeOmaggioEvoluta = (gestioneEvoluta = 1)
End Function

Public Function TipoStatoProdottoSchemaReale(idProdotto As Long) As TipoStatoProdottoEnum
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
    TipoStatoProdottoSchemaReale = TSP_NON_SPECIFICATO
    sql = "SELECT IDTIPOSTATOPRODOTTO FROM " & nomeSchema & ".PRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        TipoStatoProdottoSchemaReale = rec("IDTIPOSTATOPRODOTTO").Value
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Function

Public Function TipoStatoProdottoAggiornatoSchemaReale(idProdotto As Long, stato As TipoStatoProdottoEnum) As TipoStatoProdottoEnum
    Dim sql As String
    Dim recordAggiornati As Integer
    
    TipoStatoProdottoAggiornatoSchemaReale = TSP_NON_SPECIFICATO
    Select Case stato
        Case TSP_IN_CONFIGURAZIONE
            recordAggiornati = 0
            sql = "UPDATE " & nomeSchema & ".PRODOTTO" & _
                " SET IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE & _
                " WHERE IDPRODOTTO = " & idProdotto & _
                " AND IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE
            SETAConnection.Execute sql, recordAggiornati, adCmdText
            If recordAggiornati = 1 Then
                TipoStatoProdottoAggiornatoSchemaReale = TSP_IN_CONFIGURAZIONE
            End If
        Case TSP_ATTIVABILE
            recordAggiornati = 0
            sql = "UPDATE " & nomeSchema & ".PRODOTTO" & _
                " SET IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE & _
                " WHERE IDPRODOTTO = " & idProdotto & _
                " AND IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE
            SETAConnection.Execute sql, recordAggiornati, adCmdText
            If recordAggiornati = 1 Then
                TipoStatoProdottoAggiornatoSchemaReale = TSP_ATTIVABILE
            End If
        Case Else
            'Do Nothing
    End Select
End Function

Public Function TipoStatoOrganizzazioneAggiornatoSchemaReale(idOrganizzazione As Long, stato As TipoStatoOrganizzazioneEnum) As TipoStatoOrganizzazioneEnum
    Dim sql As String
    Dim recordAggiornati As Integer
    
    TipoStatoOrganizzazioneAggiornatoSchemaReale = TSO_NON_SPECIFICATO
    Select Case stato
        Case TSO_IN_CONFIGURAZIONE
            recordAggiornati = 0
            sql = "UPDATE " & nomeSchema & ".ORGANIZZAZIONE" & _
                " SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazione & _
                " AND IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVABILE
            SETAConnection.Execute sql, recordAggiornati, adCmdText
            If recordAggiornati = 1 Then
                TipoStatoOrganizzazioneAggiornatoSchemaReale = TSO_IN_CONFIGURAZIONE
            End If
        Case TSO_ATTIVABILE
            recordAggiornati = 0
            sql = "UPDATE " & nomeSchema & ".ORGANIZZAZIONE" & _
                " SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVABILE & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazione & _
                " AND IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE
            SETAConnection.Execute sql, recordAggiornati, adCmdText
            If recordAggiornati = 1 Then
                TipoStatoOrganizzazioneAggiornatoSchemaReale = TSO_ATTIVABILE
            End If
        Case Else
            'Do Nothing
    End Select
End Function

Public Function IsClonazioneGrigliaPostiOK(idAreaCorrente As Long, idAreaMaster As Long, clonaFasceSequenze As Boolean) As Boolean
    Dim sql As String
    Dim sql1 As String
    Dim rec As OraDynaset
    Dim rec1 As OraDynaset
    Dim sql2 As String
    Dim n As Long
    Dim coppiaId As clsCoppiaIdent
    Dim idNuovaFascia As Long
    Dim idNuovaSequenza As Long
    Dim listaCoppieIdentificatoriFasce As Collection
    Dim listaCoppieIdentificatoriSequenze As Collection
    Dim righeAggiornate As Long
    Dim idNuovoPosto As Long
    Dim qtaEtichette As Long
    
    Call ApriConnessioneBD_ORA
    
    On Error GoTo gestioneErrori
    
    Call ORADB.BeginTrans
    sql = "INSERT INTO GRIGLIAPOSTI("
    sql = sql & " IDGRIGLIAPOSTI,"
    sql = sql & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
    sql = sql & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO,"
    sql = sql & " IDAREA,"
    sql = sql & " IDPIANTA"
    sql = sql & ") (SELECT "
    sql = sql & " SQ_GRIGLIAPOSTI.NEXTVAL,"
    sql = sql & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
    sql = sql & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO,"
    sql = sql & idAreaCorrente & ","
    sql = sql & " IDPIANTA"
    sql = sql & " FROM GRIGLIAPOSTI WHERE IDAREA=" & idAreaMaster & ")"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    
    If clonaFasceSequenze Then
        Set listaCoppieIdentificatoriFasce = New Collection
        Set listaCoppieIdentificatoriSequenze = New Collection
' aggiornamenti FASCIAPOSTI
        sql1 = "SELECT IDFASCIAPOSTI, INDICEDIPREFERIBILITA"
        sql1 = sql1 & " FROM FASCIAPOSTI"
        sql1 = sql1 & " WHERE IDAREA = " & idAreaMaster
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovaFascia = OttieniIdentificatoreDaSequenza("SQ_FASCIAPOSTI")
                Set coppiaId = New clsCoppiaIdent
                coppiaId.idVecchio = rec1("IDFASCIAPOSTI")
                coppiaId.idNuovo = idNuovaFascia
                Call listaCoppieIdentificatoriFasce.Add(coppiaId)
                
'                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
                sql2 = "INSERT INTO FASCIAPOSTI("
                sql2 = sql2 & " IDFASCIAPOSTI,"
                sql2 = sql2 & " IDAREA,"
                sql2 = sql2 & " INDICEDIPREFERIBILITA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovaFascia & ","
                sql2 = sql2 & idAreaCorrente & ","
                sql2 = sql2 & rec1("INDICEDIPREFERIBILITA") & ")"
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
        
' aggiornamenti SEQUENZAPOSTI
        sql1 = "SELECT IDSEQUENZAPOSTI, S.IDFASCIAPOSTI, S.ORDINEINFASCIA"
        sql1 = sql1 & " FROM SEQUENZAPOSTI S, FASCIAPOSTI F"
        sql1 = sql1 & " WHERE S.IDFASCIAPOSTI = F.IDFASCIAPOSTI"
        sql1 = sql1 & " AND F.IDAREA = " & idAreaMaster
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovaSequenza = OttieniIdentificatoreDaSequenza("SQ_SEQUENZAPOSTI")
                Set coppiaId = New clsCoppiaIdent
                coppiaId.idVecchio = rec1("IDSEQUENZAPOSTI")
                coppiaId.idNuovo = idNuovaSequenza
                Call listaCoppieIdentificatoriSequenze.Add(coppiaId)
                
                idNuovaFascia = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriFasce, rec1("IDFASCIAPOSTI"))
                sql2 = "INSERT INTO SEQUENZAPOSTI("
                sql2 = sql2 & " IDSEQUENZAPOSTI,"
                sql2 = sql2 & " IDFASCIAPOSTI,"
                sql2 = sql2 & " ORDINEINFASCIA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovaSequenza & ","
                sql2 = sql2 & idNuovaFascia & ","
                sql2 = sql2 & rec1("ORDINEINFASCIA") & ")"
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close

' aggiornamenti POSTO
        sql1 = "SELECT P.IDPOSTO, P.NOMEFILA, P.NOMEPOSTO,"
        sql1 = sql1 & " P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE, P.IDAREA,"
        sql1 = sql1 & " P.IDSEQUENZAPOSTI, P.ORDINEINSEQUENZA, E.ETICHETTA"
        sql1 = sql1 & " FROM POSTO P, ETICHETTAPOSTO E"
        sql1 = sql1 & " WHERE P.IDAREA = " & idAreaMaster
        sql1 = sql1 & " AND P.IDPOSTO = E.IDPOSTO(+)"
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovoPosto = OttieniIdentificatoreDaSequenza("SQ_POSTO")
'                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
                If IsNull(rec1("IDSEQUENZAPOSTI")) Then
                    idNuovaSequenza = idNessunElementoSelezionato
                Else
                    idNuovaSequenza = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriSequenze, rec1("IDSEQUENZAPOSTI"))
                End If
                sql2 = "INSERT INTO POSTO("
                sql2 = sql2 & " IDPOSTO,"
                sql2 = sql2 & " NOMEFILA, NOMEPOSTO,"
                sql2 = sql2 & " COORDINATAORIZZONTALE, COORDINATAVERTICALE,"
                sql2 = sql2 & " IDAREA,"
                sql2 = sql2 & " IDSEQUENZAPOSTI,"
                sql2 = sql2 & " ORDINEINSEQUENZA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovoPosto & ","
                sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOMEFILA")) & ", "
                sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOMEPOSTO")) & ", "
                sql2 = sql2 & rec1("COORDINATAORIZZONTALE") & ", "
                sql2 = sql2 & rec1("COORDINATAVERTICALE") & ", "
                sql2 = sql2 & idAreaCorrente & ","
                sql2 = sql2 & IIf(idNuovaSequenza = idNessunElementoSelezionato, "NULL,", idNuovaSequenza & ", ")
                sql2 = sql2 & IIf(idNuovaSequenza = idNessunElementoSelezionato, "NULL)", rec1("ORDINEINSEQUENZA") & ")")
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                If Not IsNull(rec1("ETICHETTA")) Then
                    sql2 = "INSERT INTO ETICHETTAPOSTO (IDPOSTO, ETICHETTA) VALUES ("
                    sql2 = sql2 & idNuovoPosto & ", "
                    sql2 = sql2 & DefinisciCampoStringaORADB(rec1("ETICHETTA")) & ")"
                    righeAggiornate = ORADB.ExecuteSQL(sql2)
                End If
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
    Else
'INSERISCO I SOLI POSTI
        sql = "INSERT INTO POSTO ("
        sql = sql & " IDPOSTO,"
        sql = sql & " NOMEFILA, NOMEPOSTO,"
        sql = sql & " COORDINATAORIZZONTALE, COORDINATAVERTICALE,"
        sql = sql & " IDAREA,"
        sql = sql & " IDSEQUENZAPOSTI,"
        sql = sql & " ORDINEINSEQUENZA"
        sql = sql & ") SELECT "
        sql = sql & nomeSchema & "SQ_POSTO.NEXTVAL,"
        sql = sql & " NOMEFILA, NOMEPOSTO,"
        sql = sql & " COORDINATAORIZZONTALE, COORDINATAVERTICALE,"
        sql = sql & idAreaCorrente & ","
        sql = sql & " NULL,"
        sql = sql & " NULL"
        sql = sql & " FROM POSTO P"
        sql = sql & " WHERE P.IDAREA = " & idAreaMaster
        righeAggiornate = ORADB.ExecuteSQL(sql)
        
        sql = "SELECT COUNT(*) QTA"
        sql = sql & " FROM POSTO P, ETICHETTAPOSTO EP"
        sql = sql & " WHERE P.IDAREA = " & idAreaMaster
        sql = sql & " AND P.IDPOSTO = EP.IDPOSTO"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            qtaEtichette = rec("QTA")
        End If
        rec.Close
        
        If qtaEtichette > 0 Then
            MsgBox "Etichette sui posti non clonate...."
        End If
        
    End If
    
    Call ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    IsClonazioneGrigliaPostiOK = True
    
    Exit Function
    
gestioneErrori:
    ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    IsClonazioneGrigliaPostiOK = False
End Function


Public Function StatoAttualeProdottoSuSchemaProduzione(idProdotto As Long) As TipoStatoProdottoEnum
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    sql = "UPDATE " & nomeSchema & "PRODOTTO" & _
        " SET IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE & _
        " WHERE IDPRODOTTO = " & idProdotto & _
        " AND IDTIPOSTATOPRODOTTO IN (" & TSP_IN_CONFIGURAZIONE & _
        ", " & TSP_ATTIVABILE & ")"
    SETAConnection.Execute sql, n, adCmdText
    If n = 1 Then
        SETAConnection.CommitTrans
        StatoAttualeProdottoSuSchemaProduzione = TSP_IN_CONFIGURAZIONE
    Else
        SETAConnection.RollbackTrans
        StatoAttualeProdottoSuSchemaProduzione = TSP_NON_SPECIFICATO
    End If
    
    Call ChiudiConnessioneBD
    
End Function
'
'Public Function StatoAttualeOrganizzazioneSuSchemaProduzione(idOrganizzazione As Long) As TipoStatoOrganizzazioneEnum
'    Dim sql As String
'    Dim n As Long
'
'    Call ApriConnessioneBD
'
'    SETAConnection.BeginTrans
'    sql = "UPDATE " & nomeSchema & "ORGANIZZAZIONE" & _
'        " SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE & _
'        " WHERE IDORGANIZZAZIONE = " & idOrganizzazione & _
'        " AND IDTIPOSTATOORGANIZZAZIONE IN (" & TSO_IN_CONFIGURAZIONE & _
'        ", " & TSO_ATTIVABILE & ")"
'    SETAConnection.Execute sql, n, adCmdText
'    If n = 1 Then
'        SETAConnection.CommitTrans
'        StatoAttualeOrganizzazioneSuSchemaProduzione = TSO_IN_CONFIGURAZIONE
'    Else
'        SETAConnection.RollbackTrans
'        StatoAttualeOrganizzazioneSuSchemaProduzione = TSO_NON_SPECIFICATO
'    End If
'
'    Call ChiudiConnessioneBD
'
'End Function

Private Function ListaPostiConTitoliEmessi(idPeriodoCommerciale) As Collection
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim lista As Collection
    
    Set lista = New Collection
    
    sql = "SELECT DISTINCT ST.IDPREZZOTITOLOPRODOTTO IDP" & _
        " FROM TITOLO T, STATOTITOLO ST, PREZZOTITOLOPRODOTTO PTP" & _
        " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
        " AND PTP.IDPREZZOTITOLOPRODOTTO = ST.IDPREZZOTITOLOPRODOTTO(+)" & _
        " AND PTP.IDPERIODOCOMMERCIALE = " & idPeriodoCommerciale
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Call lista.Add(rec("IDP").Value, ChiaveId(CLng(rec("IDP").Value)))
            rec.MoveNext
        Wend
    End If
    rec.Close
    Set ListaPostiConTitoliEmessi = lista
    Set lista = Nothing
End Function

Public Function IsSpettacoloCompleto(idSpettacolo As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim somma As Integer
    Dim cont As Integer
    Dim listaCaratteristicheMancanti As Collection
    
    Call ApriConnessioneBD
    
    cont = 0
    somma = 0
    Set listaCaratteristicheMancanti = New Collection
    
    sql = "SELECT COUNT(IDRAPPRESENTAZIONE) CONT" & _
        " FROM RAPPRESENTAZIONE WHERE IDSPETTACOLO = " & idSpettacolo
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    If cont < 1 Then
        Call listaCaratteristicheMancanti.Add("- non esiste alcuna rappresentazione;")
    End If
    
    sql = "SELECT SUM(INCIDENZA) SOMMA" & _
        " FROM SPETTACOLO_GENERESIAE" & _
        " WHERE IDSPETTACOLO = " & idSpettacolo
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        If Not IsNull(rec("SOMMA")) Then
            somma = rec("SOMMA").Value
        Else
            somma = 0
        End If
    End If
    rec.Close
    If somma < 100 Then
        Call listaCaratteristicheMancanti.Add("- non � specificato il genere SIAE;")
    End If
    
    Call ChiudiConnessioneBD
    
    If (somma = 100 And cont > 0) Then
        IsSpettacoloCompleto = True
    Else
        IsSpettacoloCompleto = False
        Call frmMessaggio.Visualizza("AvvertimentoSpettacoloNonCompleto", ArgomentoMessaggio(listaCaratteristicheMancanti))
    End If
    
End Function

Public Function IsClonazioneOrganizzazioneOK(idVecchiaOrganizzazione As Long, _
                                        nomeNuovaOrganizzazione As String, _
                                        descrizioneNuovaOrganizzazione As String, _
                                        codiceTerminaleLottoNuovaOrganizzazione As String, _
                                        codiceFiscalePartitaIVA As String, _
                                        codiceFiscaleSocieta As String, _
                                        idOrganizzazioneTipiLayout As Long, _
                                        idOrganizzazioneTariffa As Long, _
                                        idOrganizzazionePuntiVendita As Long, _
                                        idOrganizzazioneVenditoriEsterni As Long, _
                                        idOrganizzazioneOperatori As Long, _
                                        idOrganizzazioneCausaliProtezione As Long, _
                                        idOrganizzazioneLayoutAlfresco As Long, _
                                        inserisciInformazioniSistemistiche As Boolean) As Boolean

    Dim sql1 As String
    Dim sql2 As String
    Dim rec1 As New ADODB.Recordset
    Dim rec2 As New ADODB.Recordset
    Dim count As Integer
    Dim idNuovaOrganizzazione As Long
    Dim idNuovaModalit� As Long
    Dim righeAggiornate As Long
    Dim coppiaId As clsCoppiaIdent
    Dim listaCoppieIdentificatoriModalit� As New Collection
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovaOrganizzazione = OttieniIdentificatoreDaSequenza("SQ_ORGANIZZAZIONE")
' inserimento in ORGANIZZAZIONE
    sql1 = "INSERT INTO ORGANIZZAZIONE(" & _
        " IDORGANIZZAZIONE, NOME, DESCRIZIONE, CODICETERMINALELOTTO, INDIRIZZO, CIVICO, LOCALITA, CAP, COMUNE," & _
        " TELEFONO, FAX, EMAIL, RESPONSABILE, CODICEFISCALE, CODICEFISCALESOCIETA, IDTIPOORGANIZZAZIONE, RAGIONESOCIALE, " & _
        " IDTIPOSTATOORGANIZZAZIONE, NUMGIORNICONSERVAZIONEPRODOTTI" & _
        ") (SELECT " & _
        idNuovaOrganizzazione & ", " & _
        SqlStringValue(nomeNuovaOrganizzazione) & ", " & _
        SqlStringValue(descrizioneNuovaOrganizzazione) & ", " & _
        SqlStringValue(codiceTerminaleLottoNuovaOrganizzazione) & ", " & _
        " NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " & _
        SqlStringValue(codiceFiscalePartitaIVA) & "," & _
        SqlStringValue(codiceFiscaleSocieta) & "," & _
        " IDTIPOORGANIZZAZIONE, " & _
        " NULL, " & _
        TSO_IN_CONFIGURAZIONE & _
        ", NUMGIORNICONSERVAZIONEPRODOTTI " & _
        " FROM ORGANIZZAZIONE WHERE IDORGANIZZAZIONE=" & idVecchiaOrganizzazione & ")"
    SETAConnection.Execute sql1, righeAggiornate, adCmdText
    
' importazione di TIPI / LAYOUT dall' organizzazione scelta
    If idOrganizzazioneTipiLayout <> idNessunElementoSelezionato Then
        sql1 = "INSERT INTO ORGANIZ_TIPOSUP_LAYOUTSUP(" & _
            " IDORGANIZZAZIONE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO" & _
            ") (SELECT " & _
            idNuovaOrganizzazione & ", " & _
            " IDTIPOSUPPORTO, IDLAYOUTSUPPORTO" & _
            " FROM ORGANIZ_TIPOSUP_LAYOUTSUP WHERE IDORGANIZZAZIONE = " & idOrganizzazioneTipiLayout & ")"
        SETAConnection.Execute sql1, righeAggiornate, adCmdText
'        If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'            Call AggiornaParametriSessioneSuRecord("ORGANIZ_TIPOSUP_LAYOUTSUP", "IDORGANIZZAZIONE", idNuovaOrganizzazione, TSR_NUOVO)
'        End If
    End If
    
' clonazione degli OPERATORI per tipo terminale dall' organizzazione master
'''    If idOrganizzazioneOperatori <> idNessunElementoSelezionato Then
'''        n = 0
'''        sql1 = "INSERT INTO OPERATORE_ORGANIZZAZIONE (IDORGANIZZAZIONE," & _
'''            " IDOPERATORE)" & _
'''            " (SELECT " & idNuovaOrganizzazione & ", " & _
'''            " IDOPERATORE" & _
'''            " FROM OPERATORE_ORGANIZZAZIONE WHERE" & _
'''            " IDORGANIZZAZIONE = " & idOrganizzazioneOperatori & ")"
'''        SETAConnection.Execute sql1, n, adCmdText
'''    End If
    
' importazione delle CAUSALI DI PROTEZIONE dall' organizzazione scelta
    If idOrganizzazioneCausaliProtezione <> idNessunElementoSelezionato Then
        sql1 = ""
        n = 0
        sql1 = "INSERT INTO CAUSALEPROTEZIONE (IDCAUSALEPROTEZIONE, NOME, DESCRIZIONE, " & _
            " SIMBOLOSUPIANTA, PROPAGABILEAPRODOTTICORRELATI , IDOPERATORE, IDORGANIZZAZIONE)" & _
            " (SELECT " & nomeSchema & "SQ_CAUSALEPROTEZIONE.NEXTVAL," & _
            " NOME, DESCRIZIONE ,SIMBOLOSUPIANTA, " & _
            "PROPAGABILEAPRODOTTICORRELATI, IDOPERATORE, " & _
            idNuovaOrganizzazione & " FROM CAUSALEPROTEZIONE WHERE" & _
            " IDORGANIZZAZIONE = " & idOrganizzazioneCausaliProtezione & ")"
        SETAConnection.Execute sql1, n, adCmdText
    End If
    
' importazione dei LAYOUT ALFRESCO dall' organizzazione scelta
    If idOrganizzazioneLayoutAlfresco <> idNessunElementoSelezionato Then
        sql1 = ""
        n = 0
        sql1 = "INSERT INTO ORGANIZZAZIONE_LAYOUTALFRESCO (IDORGANIZZAZIONE, IDLAYOUTALFRESCO)" & _
            " SELECT " & idNuovaOrganizzazione & ", IDLAYOUTALFRESCO" & _
            " FROM ORGANIZZAZIONE_LAYOUTALFRESCO" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneLayoutAlfresco
        SETAConnection.Execute sql1, n, adCmdText
    End If
    
' importazione delle MODALITA DI PAGAMENTO dall' organizzazione master
    n = 0
    sql1 = "SELECT IDMODALITADIPAGAMENTO, NOME," & _
        " DESCRIZIONE, IDORGANIZZAZIONE, IVAPREASSOLTA" & _
        " FROM MODALITADIPAGAMENTO" & _
        " WHERE IDORGANIZZAZIONE = " & idVecchiaOrganizzazione
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovaModalit� = OttieniIdentificatoreDaSequenza("SQ_MODALITADIPAGAMENTO")
            Set coppiaId = New clsCoppiaIdent
            coppiaId.idVecchio = rec1("IDMODALITADIPAGAMENTO").Value
            coppiaId.idNuovo = idNuovaModalit�
            sql2 = "INSERT INTO MODALITADIPAGAMENTO (IDMODALITADIPAGAMENTO, NOME," & _
                " DESCRIZIONE, IDORGANIZZAZIONE, IVAPREASSOLTA)" & _
                " (SELECT " & _
                idNuovaModalit� & "," & _
                " NOME, DESCRIZIONE, " & _
                idNuovaOrganizzazione & "," & _
                " IVAPREASSOLTA FROM MODALITADIPAGAMENTO" & _
                " WHERE IDMODALITADIPAGAMENTO = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText
            
            sql2 = "INSERT INTO ORGANIZZAZIONE_MODPAG_TIPOTERM (IDMODALITADIPAGAMENTO," & _
                " IDTIPOTERMINALE, IDORGANIZZAZIONE)" & _
                " (SELECT " & coppiaId.idNuovo & "," & _
                " IDTIPOTERMINALE, " & _
                idNuovaOrganizzazione & " FROM ORGANIZZAZIONE_MODPAG_TIPOTERM" & _
                " WHERE IDORGANIZZAZIONE = " & idVecchiaOrganizzazione & _
                " AND IDMODALITADIPAGAMENTO = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
' importazione delle TARIFFE dall' organizzazione scelta
    If idOrganizzazioneTariffa <> idNessunElementoSelezionato Then
        n = 0
        sql1 = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, IDPRODOTTO, CODICE," & _
            " IDTIPOTARIFFA, IDMODALITAEMISSIONE, IDORGANIZZAZIONE)" & _
            " (SELECT " & nomeSchema & "SQ_TARIFFA.NEXTVAL, " & _
            " NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, IDPRODOTTO, CODICE," & _
            " IDTIPOTARIFFA, IDMODALITAEMISSIONE, " & idNuovaOrganizzazione & " FROM TARIFFA WHERE" & _
            " IDORGANIZZAZIONE = " & idOrganizzazioneTariffa & ")"
        SETAConnection.Execute sql1, n, adCmdText
    End If

' clonazione dei PUNTI VENDITA per tipo terminale dall' organizzazione master
'''    If idOrganizzazionePuntiVendita <> idNessunElementoSelezionato Then
'''        n = 0
'''        sql1 = "INSERT INTO ORGANIZZAZIONE_PUNTOVENDITA (IDORGANIZZAZIONE," & _
'''            " IDPUNTOVENDITA)" & _
'''            " (SELECT " & idNuovaOrganizzazione & ", " & _
'''            " IDPUNTOVENDITA" & _
'''            " FROM ORGANIZZAZIONE_PUNTOVENDITA WHERE" & _
'''            " IDORGANIZZAZIONE = " & idOrganizzazionePuntiVendita & ")"
'''        SETAConnection.Execute sql1, n, adCmdText
'''    End If
    
' clonazione dei PUNTI VENDITA per classe punto vendita dall' organizzazione master
    If idOrganizzazionePuntiVendita <> idNessunElementoSelezionato Then
        n = 0
        sql1 = "INSERT INTO ORGANIZ_CLASSEPV_PUNTOVENDITA (IDORGANIZZAZIONE, IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA)" & _
            " (" & _
            " SELECT " & idNuovaOrganizzazione & ", IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA" & _
            " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazionePuntiVendita & _
            ")"
        SETAConnection.Execute sql1, n, adCmdText
        
        n = 0
        sql1 = "INSERT INTO OPERATOREMANAGER (IDORGANIZZAZIONE, IDOPERATORE)" & _
            " (" & _
            " SELECT " & idNuovaOrganizzazione & ", IDOPERATORE" & _
            " FROM OPERATOREMANAGER" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazionePuntiVendita & _
            ")"
        SETAConnection.Execute sql1, n, adCmdText
    End If

' clonazione dei VENDITORI ESTERNI per tipo terminale dall' organizzazione master
    If idOrganizzazioneVenditoriEsterni <> idNessunElementoSelezionato Then
        n = 0
        sql1 = "INSERT INTO ORGANIZZAZIONE_VENDITOREESTERN (IDORGANIZZAZIONE," & _
            " IDVENDITOREESTERNO, UTILIZZATOPERTITOLIOSPITE)" & _
            " (SELECT " & idNuovaOrganizzazione & ", " & _
            " IDVENDITOREESTERNO, UTILIZZATOPERTITOLIOSPITE" & _
            " FROM ORGANIZZAZIONE_VENDITOREESTERN WHERE" & _
            " IDORGANIZZAZIONE = " & idOrganizzazioneVenditoriEsterni & ")"
        SETAConnection.Execute sql1, n, adCmdText
    End If
    
'' clonazione dei RUOLI DI SERVIZIO per tipo terminale dall' organizzazione master
'    n = 0
'    sql1 = "INSERT INTO RUOLODISERVIZIO (IDRUOLODISERVIZIO, IDORGANIZZAZIONE," & _
'        " NOME, DESCRIZIONE, COLLOCAZIONEOBBLIGATORIA)" & _
'        " (SELECT " & nomeSchema & "SQ_RUOLODISERVIZIO.NEXTVAL, " & _
'        idNuovaOrganizzazione & ", " & _
'        " NOME, DESCRIZIONE, COLLOCAZIONEOBBLIGATORIA" & _
'        " FROM RUOLODISERVIZIO WHERE" & _
'        " IDORGANIZZAZIONE = " & idVecchiaOrganizzazione & ")"
'    SETAConnection.Execute sql1, n, adCmdText
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        Call AggiornaParametriSessioneSuRecord("RUOLODISERVIZIO", "IDORGANIZZAZIONE", idNuovaOrganizzazione, TSR_NUOVO)
'    End If

    If inserisciInformazioniSistemistiche Then
        Call InserisciInformazioniSistemistichePerNuovaOrganizzazione(idNuovaOrganizzazione)
    End If
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD

    Call ScriviLog(CCTA_CLONAZIONE, CCDA_ORGANIZZAZIONE, CCDA_ORGANIZZAZIONE, "IDORGANIZZAZIONE MASTER = " & idVecchiaOrganizzazione, idNuovaOrganizzazione)

    IsClonazioneOrganizzazioneOK = True
    
    Exit Function
    
gestioneErrori:
    SETAConnection.RollbackTrans
    IsClonazioneOrganizzazioneOK = False
'    Call frmMessaggio.Visualizza("ClonazioneProdottoKO")
End Function

Public Function NomeFileLog() As String
    NomeFileLog = "LogClientConfigurazione_20" & _
        CompletaStringaConZeri(CStr(Year(Now)), 2) & "_" & _
        CompletaStringaConZeri(CStr(Month(Now)), 2) & "_" & _
        CompletaStringaConZeri(CStr(Day(Now)), 2) & "_ore_" & _
        CompletaStringaConZeri(CStr(Hour(Now)), 2) & "_" & _
        CompletaStringaConZeri(CStr(Minute(Now)), 2) & "_" & _
        CompletaStringaConZeri(CStr(Second(Now)), 2)
End Function

Public Function CompletaStringaConZeri(sIn As String, lunghezza As Integer) As String
    Dim c As String
    
    c = Trim(sIn)
    c = "0000000000" & sIn
    c = Right$(c, lunghezza)
    CompletaStringaConZeri = c
End Function

Public Function ElencoDaLista(lista As Collection) As String
    Dim i As Integer
    Dim parziale As String
    
    parziale = ""
    If lista.count > 0 Then
        parziale = lista.Item(1)
        For i = 2 To lista.count
            parziale = parziale & ", " & lista.Item(i)
        Next i
    End If
    ElencoDaLista = parziale
End Function

Public Function ElencoDaListaElementi(lista As Collection, campo As CampoElementoListaEnum) As String
    Dim e As clsElementoLista
    Dim parziale As String
    Dim l As Integer
    
    If lista.count > 0 Then
        For Each e In lista
            Select Case campo
                Case CEL_ID_ELEMENTO_LISTA
                    parziale = parziale & e.idElementoLista & ", "
                Case CEL_CODICE_ELEMENTO_LISTA
                    parziale = parziale & e.codiceElementoLista & ", "
                Case CEL_NOME_ELEMENTO_LISTA
                    parziale = parziale & e.nomeElementoLista & ", "
                Case CEL_DESCRIZIONE_ELEMENTO_LISTA
                    parziale = parziale & e.descrizioneElementoLista & ", "
                Case CEL_ID_ATTRIBUTO_ELEMENTO_LISTA
                    parziale = parziale & e.idAttributoElementoLista & ", "
                Case CEL_CODICE_ATTRIBUTO_ELEMENTO_LISTA
                    parziale = parziale & e.codiceAttributoElementoLista & ", "
                Case CEL_NOME_ATTRIBUTO_ELEMENTO_LISTA
                    parziale = parziale & e.nomeAttributoElementoLista & ", "
                Case CEL_DESCRIZIONE_ATTRIBUTO_ELEMENTO_LISTA
                    parziale = parziale & e.descrizioneAttributoElementoLista & ", "
            End Select
        Next e
    Else
        parziale = CStr(idNessunElementoSelezionato) & ", "
    End If
    l = Len(parziale)
    parziale = Left$(parziale, l - 2) 'si toglie l'ultima virgola
    ElencoDaListaElementi = parziale
End Function

Public Function IdTariffaCorrispondente(idTariffaMaster As Long, idProdottoSlave) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    Call ApriConnessioneBD
    
    id = idNessunElementoSelezionato
    sql = "SELECT TS.IDTARIFFA ID FROM TARIFFA TM," & _
        " (" & _
        " SELECT IDTARIFFA, CODICE, IDTIPOTARIFFA, IDMODALITAEMISSIONE" & _
        " FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSlave & _
        " ) TS" & _
        " WHERE TM.IDTARIFFA = " & idTariffaMaster & _
        " AND TM.CODICE = TS.CODICE" & _
        " AND TM.IDTIPOTARIFFA = TS.IDTIPOTARIFFA" & _
        " AND TM.IDMODALITAEMISSIONE = TS.IDMODALITAEMISSIONE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        id = rec("ID")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IdTariffaCorrispondente = id
End Function

Public Function IdPiantaAssociataAProdotto(idProdotto As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    Call ApriConnessioneBD
    
    id = idNessunElementoSelezionato
    sql = "SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        id = rec("IDPIANTA").Value
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IdPiantaAssociataAProdotto = id
End Function

Public Sub Adodc_Init(adc As Adodc, sqlConfigurazioneDiretta As String, sqlConfigurazioneTransazionale As String, internalEvent As Boolean)
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
'    Call PopolaTabellaAppoggioLayoutTipiSupporto: queste chiamate vanno fatte prima
    
    Set d = adc
    
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        sql = sqlConfigurazioneTransazionale
'    Else
        sql = sqlConfigurazioneDiretta
'    End If
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
'    Call AggiornaAbilitazioneControlli: queste chiamate vanno fatte dopo
End Sub

Public Sub DataGrid_Init(dgr As DataGrid, numeroCampi As Integer, listaCampiVisibili As Collection)
    Dim g As DataGrid
    Dim dimensioneGrid As Long
'    Dim numeroCampi As Integer
    Dim i As Integer
    
    Set g = dgr
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
'    numeroCampi = 7
'    g.Columns(0).Visible = False
'    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(4).Visible = False
'    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(8).Width = (dimensioneGrid / numeroCampi)
    
    For i = 0 To listaCampiVisibili.count - 1 'lista di booleani che indicano la visibilit� dell'i-esimo campo
        If listaCampiVisibili.Item(i + 1) = True Then
            g.Columns(i).Width = (dimensioneGrid / numeroCampi)
        Else
            g.Columns(i).Visible = False
        End If
    Next i
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Public Sub DataGrid_Update(id As Long, rec As ADODB.Recordset, internalEvent As Boolean)
'    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
'    Set rec = adcConfigurazioneProdottoTariffe.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Public Sub ComboBox_Update(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Public Sub ComboBox_Init(cmb As ComboBox, sql As String, NomeCampo As String)
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
'    Call ApriConnessioneBD
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
'    Call ChiudiConnessioneBD
            
End Sub

Public Function IdRecordSelezionato_Update(rec As ADODB.Recordset) As Long
'    Dim rec As ADODB.Recordset
    
'    Set rec = adcConfigurazioneProdottoTariffe.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        IdRecordSelezionato_Update = rec("ID").Value
    Else
        IdRecordSelezionato_Update = idNessunElementoSelezionato
    End If
    'Call AggiornaAbilitazioneControlli: queste chiamate vanno fatte dopo
End Function

Public Function ViolataUnicit�(nomeTabella As String, condizioneCampo As String, _
                               sqlConstraint1 As String, _
                               sqlConstraint2 As String, _
                               sqlConstraint3 As String, _
                               sqlConstraint4 As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Integer
    
    Call ApriConnessioneBD
    
    n = 0
    sql = "SELECT COUNT(*) CONT FROM " & nomeTabella
    sql = sql & " WHERE " & CStr(condizioneCampo)
    If Not sqlConstraint1 = "" Then sql = sql & " AND " & sqlConstraint1
    If Not sqlConstraint2 = "" Then sql = sql & " AND " & sqlConstraint2
    If Not sqlConstraint3 = "" Then sql = sql & " AND " & sqlConstraint3
    If Not sqlConstraint4 = "" Then sql = sql & " AND " & sqlConstraint4
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    n = rec("CONT").Value
    rec.Close
    
    Call ChiudiConnessioneBD
    
    ViolataUnicit� = (n > 0)
End Function

Public Sub EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchetto As Long)
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM AREAPACCHETTO_AREA"
    sql = sql & " WHERE IDAREAPACCHETTO IN"
    sql = sql & " (SELECT DISTINCT IDAREAPACCHETTO"
    sql = sql & " FROM AREAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM TARIFFAPACCHETTO_TARIFFA"
    sql = sql & " WHERE IDTARIFFAPACCHETTO IN"
    sql = sql & " (SELECT DISTINCT IDTARIFFAPACCHETTO"
    sql = sql & " FROM TARIFFAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM SCELTAPRODOTTO_PRODOTTO"
    sql = sql & " WHERE IDSCELTAPRODOTTO IN"
    sql = sql & " (SELECT DISTINCT IDSCELTAPRODOTTO"
    sql = sql & " FROM SCELTAPRODOTTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM AREAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM TARIFFAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM SCELTAPRODOTTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM OPERATORE_OFFERTAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM OFFERTAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OFFERTA_PACCHETTO, , "IDOFFERTAPACCHETTO = " & idOffertaPacchetto)
    
    Call ChiudiConnessioneBD
    
    Exit Sub
gestioneErrori:
    SETAConnection.RollbackTrans
End Sub

Public Function OttieniCodiceDaLabel(label As String) As String
    Dim a As String
    Dim c As String
    Dim i As Integer
    
    i = 1
    a = ""
    c = ""
    While a <> "-"
        c = c & a
        a = Mid$(label, i, 1)
        i = i + 1
    Wend
    OttieniCodiceDaLabel = c
End Function

Public Function IsOffertaPacchettoCompleta(idOffertaPacchetto As Long, tipoOffertaPacchetto As TipoOffertaPacchettoEnum, listaAttributiNonCompleti As Collection) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim isAssociatoAlmenoUnProdotto As Boolean
    Dim tuttiProdottiAssociatiAttivabili As Boolean
    Dim esistePeriodoCommerciale As Boolean
    Dim associazioneAreeOk As Boolean
    Dim associazioneTariffeOk As Boolean
    Dim associazioneTipiLayoutOk As Boolean
    Dim associazionePrezziOk As Boolean
    Dim idProdotto As Long
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim cont As Integer
    
    'L'offerta pacchetto � completa quando:
    'a. esiste almeno un prodotto componente;
    'b. ogni prodotto componente � attivabile (e non scaduto);
    'c. l'intersezione di tutti i periodi commerciali dei prodotti delle scelte prodotto � non nulla (cio� il max delle date/ore inizio deve essere minore del min delle date/ora fine);
    'd. area pacchetto:
    ' d1. prezzo rateizzabile o fisso: per ogni pianta sulla quale sono definiti i prodotti associati alle scelte prodotto almeno una superarea � associata a ogni area pacchetto;
    ' d2. prezzo variabile: per ogni prodotto associato alle scelte prodotto almeno una superarea � associata a ogni area pacchetto;
    'e. tariffa pacchetto: ad ogni tariffa pacchetto deve essere associata una e una sola tariffa per prodotto;
    'f. tipi/layout supporto(?);
    'g. prezzi(?);
    
    Call ApriConnessioneBD
    
    'a.
    cont = 0
    isAssociatoAlmenoUnProdotto = False
    sql = " SELECT COUNT(DISTINCT SP.IDPRODOTTO) CONT"
    sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
    sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    isAssociatoAlmenoUnProdotto = (cont > 0)
    If Not isAssociatoAlmenoUnProdotto Then
        Call listaAttributiNonCompleti.Add("- non � associato alcun prodotto;")
    End If
    
    'b.
    cont = 0
    tuttiProdottiAssociatiAttivabili = False
    sql = " SELECT COUNT(DISTINCT SP.IDPRODOTTO) CONT"
    sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP, PRODOTTO P"
    sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
    sql = sql & " AND SP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDTIPOSTATOPRODOTTO NOT IN (2, 3)"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    tuttiProdottiAssociatiAttivabili = (cont = 0)
    If Not tuttiProdottiAssociatiAttivabili Then
        Call listaAttributiNonCompleti.Add("- non tutti i prodotti componenti sono in stato Attivabile o Attivo;")
    End If

    'c.
    dataOraInizio = dataNulla
    dataOraFine = dataNulla
    esistePeriodoCommerciale = True
    sql = " SELECT MAX(DATAORAINIZIO) INIZIO, MIN(DATAORAFINE) FINE"
    sql = sql & " FROM PERIODOCOMMERCIALE P, SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
    sql = sql & " WHERE P.IDPRODOTTO = SP.IDPRODOTTO"
    sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        If Not (IsNull(rec("INIZIO")) Or IsNull(rec("FINE"))) Then
            dataOraInizio = rec("INIZIO").Value
            dataOraFine = rec("FINE").Value
        End If
    End If
    rec.Close
    If dataOraInizio >= dataOraFine Then
        esistePeriodoCommerciale = True
        Call listaAttributiNonCompleti.Add("- non si pu� definire alcun periodo commerciale " & _
            "(cio� il max. delle date/ore inizio � maggiore del min. delle date/ora fine);")
    End If
    
    'd.
    Dim cont1 As Integer
    Dim cont2 As Integer
    
    cont = 0
    cont1 = 0
    cont2 = 0
    If tipoOffertaPacchetto = TOP_PREZZO_RATEIZZABILE Or tipoOffertaPacchetto = TOP_PREZZO_FISSO Then
        'PIANTE CON AREE ASSOCIATE AD AREE PACCHETTO DEL PACCHETTO
        sql = " SELECT COUNT(DISTINCT A.IDPIANTA) CONT1"
        sql = sql & " FROM AREAPACCHETTO AP, AREAPACCHETTO_AREA APA, AREA A"
        sql = sql & " WHERE AP.IDAREAPACCHETTO = APA.IDAREAPACCHETTO"
        sql = sql & " AND APA.IDAREA = A.IDAREA"
        sql = sql & " AND AP.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            cont1 = rec("CONT1").Value
        End If
        rec.Close
        'PIANTE ASSOCIATE A PRODOTTI DEL PACCHETTO
        sql = " SELECT COUNT(DISTINCT P.IDPIANTA) CONT2"
        sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP, PRODOTTO P"
        sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
        sql = sql & " AND SP.IDPRODOTTO = P.IDPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            cont2 = rec("CONT2").Value
        End If
        rec.Close
        associazioneAreeOk = (cont1 = cont2)
    ElseIf tipoOffertaPacchetto = TOP_PREZZO_VARIABILE Then
        'PRODOTTI CON NESSUNA AREA ASSOCIATA AD AREE PACCHETTO
        sql = " SELECT COUNT(P.IDPRODOTTO) CONT"
        sql = sql & " FROM "
        sql = sql & " ("
        sql = sql & " SELECT AA.IDAREA, AA.IDPRODOTTO"
        sql = sql & " FROM AREAPACCHETTO A, AREAPACCHETTO_AREA AA"
        sql = sql & " WHERE A.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) A," 'ASSOCIAZIONI AREA-PRODOTTO PER OFFERTA PACCHETTO DATA
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT SP.IDPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
        sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) P" 'PRODOTTI DELL'OFFERTA PACCHETTO DATA
        sql = sql & " WHERE P.IDPRODOTTO = A.IDPRODOTTO(+)"
        sql = sql & " AND A.IDPRODOTTO IS NULL"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            cont = rec("CONT").Value
        End If
        rec.Close
        associazioneAreeOk = (cont = 0)
    End If
    If Not associazioneAreeOk Then
        Call listaAttributiNonCompleti.Add("- alcuni prodotti/piante non hanno aree associate ad alcuna area pacchetto;")
    End If
    
    'e.
    cont = 0
    'PRODOTTI CON NESSUNA TARIFFA ASSOCIATA A TARIFFE PACCHETTO
    sql = " SELECT COUNT(P.IDPRODOTTO) CONT"
    sql = sql & " FROM"
    sql = sql & " ("
    sql = sql & " SELECT TT.IDTARIFFA, TT.IDPRODOTTO"
    sql = sql & " FROM TARIFFAPACCHETTO T, TARIFFAPACCHETTO_TARIFFA TT"
    sql = sql & " WHERE T.IDTARIFFAPACCHETTO = TT.IDTARIFFAPACCHETTO"
    sql = sql & " AND T.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    sql = sql & " ) T,"  'ASSOCIAZIONI TARIFFA-PRODOTTO PER OFFERTA PACCHETTO DATA
    sql = sql & " ("
    sql = sql & " SELECT DISTINCT SP.IDPRODOTTO"
    sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
    sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    sql = sql & " ) P" 'PRODOTTI DELL'OFFERTA PACCHETTO DATA
    sql = sql & " WHERE P.IDPRODOTTO = T.IDPRODOTTO(+)"
    sql = sql & " AND T.IDPRODOTTO IS NULL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        cont = rec("CONT").Value
    End If
    rec.Close
    associazioneTariffeOk = (cont = 0)
    If Not associazioneTariffeOk Then
        Call listaAttributiNonCompleti.Add("- alcuni prodotti non hanno tariffe associate ad alcuna tariffa pacchetto;")
    End If
    
    'f.
    associazioneTipiLayoutOk = True
    If associazioneAreeOk And associazioneTariffeOk Then
        sql = " SELECT DISTINCT P.IDPRODOTTO, T.IDTARIFFA, A.IDAREA, ULS.IDTIPOSUPPORTO, ULS.IDLAYOUTSUPPORTO"
        sql = sql & " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS,"
        sql = sql & " ("
        sql = sql & " SELECT AA.IDAREA, AA.IDPRODOTTO"
        sql = sql & " FROM AREAPACCHETTO A, AREAPACCHETTO_AREA AA"
        sql = sql & " WHERE A.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) A,"
        sql = sql & " ("
        sql = sql & " SELECT TT.IDTARIFFA, TT.IDPRODOTTO"
        sql = sql & " FROM TARIFFAPACCHETTO T, TARIFFAPACCHETTO_TARIFFA TT"
        sql = sql & " WHERE T.IDTARIFFAPACCHETTO = TT.IDTARIFFAPACCHETTO"
        sql = sql & " AND T.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) T,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT SP.IDPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
        sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) P"
        sql = sql & " WHERE P.IDPRODOTTO = T.IDPRODOTTO"
        sql = sql & " AND P.IDPRODOTTO = A.IDPRODOTTO"
        sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA(+)"
        sql = sql & " AND ULS.IDAREA = A.IDAREA(+)"
        sql = sql & " AND ULS.IDTARIFFA IS NULL"
        sql = sql & " AND ULS.IDAREA IS NULL"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            associazioneTipiLayoutOk = False
        End If
        rec.Close
    Else
        associazioneTipiLayoutOk = False
    End If
    If Not associazioneTipiLayoutOk Then
        Call listaAttributiNonCompleti.Add("- per alcuni prodotti del pacchetto non � definita l'associazione tra tipi supporto e layout supporto;")
    End If
    
    'g.
    associazionePrezziOk = True
    If associazioneAreeOk And associazioneTariffeOk Then
        sql = " SELECT DISTINCT P.IDPRODOTTO, T.IDTARIFFA, A.IDAREA, ULS.IDTIPOSUPPORTO, ULS.IDLAYOUTSUPPORTO"
        sql = sql & " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS,"
        sql = sql & " ("
        sql = sql & " SELECT AA.IDAREA, AA.IDPRODOTTO"
        sql = sql & " FROM AREAPACCHETTO A, AREAPACCHETTO_AREA AA"
        sql = sql & " WHERE A.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) A,"
        sql = sql & " ("
        sql = sql & " SELECT TT.IDTARIFFA, TT.IDPRODOTTO"
        sql = sql & " FROM TARIFFAPACCHETTO T, TARIFFAPACCHETTO_TARIFFA TT"
        sql = sql & " WHERE T.IDTARIFFAPACCHETTO = TT.IDTARIFFAPACCHETTO"
        sql = sql & " AND T.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) T,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT SP.IDPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
        sql = sql & " WHERE S.IDSCELTAPRODOTTO = SP.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
        sql = sql & " ) P"
        sql = sql & " WHERE P.IDPRODOTTO = T.IDPRODOTTO"
        sql = sql & " AND P.IDPRODOTTO = A.IDPRODOTTO"
        sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA(+)"
        sql = sql & " AND ULS.IDAREA = A.IDAREA(+)"
        sql = sql & " AND ULS.IDTARIFFA IS NULL"
        sql = sql & " AND ULS.IDAREA IS NULL"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            associazionePrezziOk = False
        End If
        rec.Close
    Else
        associazionePrezziOk = False
    End If
    If Not associazionePrezziOk Then
        Call listaAttributiNonCompleti.Add("- per alcuni prodotti del pacchetto non sono definiti i prezzi corrispondenti ad aree pacchetto e tariffe pacchetto;")
    End If
    
    Call ChiudiConnessioneBD
    
    IsOffertaPacchettoCompleta = _
        (isAssociatoAlmenoUnProdotto And tuttiProdottiAssociatiAttivabili) And _
        esistePeriodoCommerciale And _
        associazioneAreeOk And _
        associazioneTariffeOk And _
        associazioneTipiLayoutOk And _
        associazionePrezziOk
End Function

Public Function NomeTNS() As String
    Const NOME_SERVER As Integer = 48
    
    NomeTNS = CStr(SETAConnection.Properties.Item(NOME_SERVER).Value)
End Function

Public Function NumeroProdottiAssociatiAdOffertaPacchetto(idOffertaPacchetto As Long) As Integer
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Integer
    
    Call ApriConnessioneBD
    
    cont = 0
    sql = " SELECT COUNT(DISTINCT SP.IDPRODOTTO) CONT"
    sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP"
    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    NumeroProdottiAssociatiAdOffertaPacchetto = cont
End Function

Public Function IdAreaPacchettoDaIdArea(idArea As Long, idOffertaPacchetto As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    id = idNessunElementoSelezionato
    sql = " SELECT DISTINCT A.IDAREAPACCHETTO ID"
    sql = sql & " FROM AREAPACCHETTO_AREA AA, AREAPACCHETTO A"
    sql = sql & " WHERE A.IDAREAPACCHETTO = AA.IDAREAPACCHETTO"
    sql = sql & " AND AA.IDAREA = " & idArea
    sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        id = rec("ID").Value
    End If
    rec.Close
    
    IdAreaPacchettoDaIdArea = id
End Function

Public Function Operativit�SuOffertaPacchettoOK(idOffertaPacchetto As Long, listaOperatoriSelezionati As Collection, listaOperatoriNonOperativi As Collection) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim operatore As clsElementoLista
    Dim prodotto As clsElementoLista
    Dim listaProdotti As Collection
    Dim associatoAProdotto As Boolean
    Dim associatoATipoOperazione As Boolean
    Dim associatoATariffa As Boolean
    
    Call ApriConnessioneBD
    
    Set listaProdotti = ListaProdottiAssociatiAdOffertaPacchetto(idOffertaPacchetto)
    For Each operatore In listaOperatoriSelezionati
        For Each prodotto In listaProdotti
            associatoAProdotto = True
            sql = "SELECT IDOPERATORE, IDPRODOTTO"
            sql = sql & " FROM OPERATORE_PRODOTTO"
            sql = sql & " WHERE IDOPERATORE = " & operatore.idElementoLista
            sql = sql & " AND IDPRODOTTO = " & prodotto.idElementoLista
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.EOF And rec.BOF) Then
                rec.MoveFirst
                If IsNull(rec("IDOPERATORE")) Then associatoAProdotto = False
            Else
                associatoAProdotto = False
            End If
            rec.Close
            If Not associatoAProdotto Then
                Call listaOperatoriNonOperativi.Add(" - operatore " & operatore.nomeElementoLista & _
                    ": non associato al prodotto " & prodotto.nomeElementoLista)
            End If
            
            associatoATipoOperazione = True
            sql = "SELECT OT.IDOPERATORE, OT.IDTIPOOPERAZIONE"
            sql = sql & " FROM OPERATORE_TIPOOPERAZIONE OT"
            sql = sql & " WHERE OT.IDPRODOTTO = " & prodotto.idElementoLista
            sql = sql & " AND OT.IDOPERATORE = " & operatore.idElementoLista
            sql = sql & " AND OT.IDTIPOOPERAZIONE IN ("
            sql = sql & TO_VENDITA & ", "
            sql = sql & TO_ANNULLAMENTO_VENDITA & ", "
            sql = sql & TO_GESTIONE_OFFERTE_PACCHETTO
            sql = sql & " )"
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.EOF And rec.BOF) Then
                rec.MoveFirst
                If IsNull(rec("IDOPERATORE")) Then associatoATipoOperazione = False
            Else
                associatoATipoOperazione = False
            End If
            rec.Close
            If Not associatoATipoOperazione Then
                Call listaOperatoriNonOperativi.Add(" - operatore " & operatore.nomeElementoLista & _
                    ": non associato ad almeno una delle operazioni necessarie del prodotto " & prodotto.nomeElementoLista)
            End If
            
            associatoATariffa = True
            sql = "SELECT OT.IDOPERATORE, OT.IDTARIFFA"
            sql = sql & " FROM OPERATORE_TARIFFA OT, TARIFFA T"
            sql = sql & " WHERE OT.IDTARIFFA = T.IDTARIFFA"
            sql = sql & " AND T.IDPRODOTTO = " & prodotto.idElementoLista
            sql = sql & " AND OT.IDOPERATORE = " & operatore.idElementoLista
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.EOF And rec.BOF) Then
                rec.MoveFirst
                If IsNull(rec("IDOPERATORE")) Then associatoATariffa = False
            Else
                associatoATariffa = False
            End If
            rec.Close
            If Not associatoATariffa Then
                Call listaOperatoriNonOperativi.Add(" - operatore " & operatore.nomeElementoLista & _
                    ": non associato ad alcuna tariffa del prodotto " & prodotto.nomeElementoLista)
            End If
        Next prodotto
        
    Next operatore
    
    Call ChiudiConnessioneBD
    
    Operativit�SuOffertaPacchettoOK = (listaOperatoriNonOperativi.count = 0)
End Function

Public Function ListaProdottiAssociatiAdOffertaPacchetto(idOffertaPacchetto As Long) As Collection
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim lista As Collection
    Dim prodotto As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set lista = New Collection
    sql = " SELECT DISTINCT P.IDPRODOTTO, P.NOME"
    sql = sql & " FROM SCELTAPRODOTTO S, SCELTAPRODOTTO_PRODOTTO SP, PRODOTTO P"
    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    sql = sql & " AND SP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchetto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodotto = New clsElementoLista
            prodotto.idElementoLista = rec("IDPRODOTTO").Value
            prodotto.nomeElementoLista = rec("NOME").Value
            Call lista.Add(prodotto, ChiaveId(prodotto.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Set ListaProdottiAssociatiAdOffertaPacchetto = lista
End Function

Public Function esistePiantaInClonazione() As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Integer
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(*) CONT FROM CC_PIANTAINCLONAZIONE"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            cont = rec("CONT").Value
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
    
    If cont = 0 Then
        esistePiantaInClonazione = False
    Else
        esistePiantaInClonazione = True
    End If
    
End Function

Public Function IsClonazionePiantaOK(idVecchiaPianta As Long, _
                                        nomeNuovaPianta As String, _
                                        descrizioneNuovaPianta As String, _
                                        listaOrganizzazioni As Collection, _
                                        clonaFasceSequenze As ValoreBooleanoEnum) As Boolean
    Dim sql1 As String
    Dim sql2 As String
    Dim rec1 As OraDynaset
    Dim idNuovaPianta As Long
    Dim righeAggiornate As Long
    Dim coppiaId As clsCoppiaIdent
    Dim listaCoppieIdentificatoriAree As New Collection
    Dim listaCoppieIdentificatoriFasce As New Collection
    Dim listaCoppieIdentificatoriSequenze As New Collection
    Dim idVecchiaArea As Long
    Dim idNuovaArea As Long
    Dim idNuovaFascia As Long
    Dim idNuovaSequenza As Long
    Dim idNuovoPosto As Long
    Dim i As Long
    Dim organizzazione As clsElementoLista

    idNuovaPianta = OttieniIdentificatoreDaSequenza("SQ_PIANTA")
    
    Call ApriConnessioneBD_ORA
    
' inserimento dei dati di clonazione
    Call ORADB.BeginTrans
    
    sql1 = "INSERT INTO CC_PIANTAINCLONAZIONE (IDVECCHIAPIANTA, DATAORAINIZIOCLONAZIONE)"
    sql1 = sql1 & " VALUES ("
    sql1 = sql1 & idVecchiaPianta & ", "
    sql1 = sql1 & "SYSDATE)"
    righeAggiornate = ORADB.ExecuteSQL(sql1)
    
    ORADB.CommitTrans
    
' inizio transazione per la clonazione

    Call ORADB.BeginTrans
    
' inserimento in PIANTA
    sql1 = "INSERT INTO CL_PIANTA (IDPIANTA, NOME, DESCRIZIONE, NUMEROVERSIONE, IDTIPOSTATOPIANTA)"
    sql1 = sql1 & " VALUES ("
    sql1 = sql1 & idNuovaPianta & ", "
    sql1 = sql1 & " '" & StringaConApiciRaddoppiati(nomeNuovaPianta) & "',"
    sql1 = sql1 & IIf(IsNull(descrizioneNuovaPianta), " NULL, ", " '" & StringaConApiciRaddoppiati(descrizioneNuovaPianta) & "', ")
    sql1 = sql1 & 1 & ", " & TSPT_ATTIVA & ")"
    righeAggiornate = ORADB.ExecuteSQL(sql1)

' aggiornamento CC_PIANTAINCLONAZIONE
    sql1 = "UPDATE CC_PIANTAINCLONAZIONE SET IDPIANTACLONATA = " & idNuovaPianta
    sql1 = sql1 & " WHERE IDVECCHIAPIANTA = " & idVecchiaPianta
    righeAggiornate = ORADB.ExecuteSQL(sql1)

' inserimento in ORGANIZZAZIONE_PIANTA
    For Each organizzazione In listaOrganizzazioni
        sql1 = "INSERT INTO CL_ORGANIZZAZIONE_PIANTA (IDORGANIZZAZIONE, IDPIANTA, CODICEPIANTA)"
        sql1 = sql1 & " VALUES ( "
        sql1 = sql1 & organizzazione.idElementoLista & ", "
        sql1 = sql1 & idNuovaPianta & ", '"
        sql1 = sql1 & organizzazione.codiceElementoLista & "')"
        righeAggiornate = ORADB.ExecuteSQL(sql1)
    Next organizzazione
    
' inserimento in VENUE_PIANTA
    sql1 = "INSERT INTO CL_VENUE_PIANTA (IDVENUE, IDPIANTA)"
    sql1 = sql1 & " SELECT IDVENUE, " & idNuovaPianta
    sql1 = sql1 & " FROM VENUE_PIANTA"
    sql1 = sql1 & " WHERE IDPIANTA=" & idVecchiaPianta
    righeAggiornate = ORADB.ExecuteSQL(sql1)

' inserimento GRIGLIAPOSTI; questa istruzione viene eseguita solo per piccoli impianti
' in quanto per grandi impianti la condizione where non � mai soddisfatta
    sql1 = "INSERT INTO CL_GRIGLIAPOSTI("
    sql1 = sql1 & " IDGRIGLIAPOSTI,"
    sql1 = sql1 & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
    sql1 = sql1 & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO, IDAREA,"
    sql1 = sql1 & " IDPIANTA"
    sql1 = sql1 & ") (SELECT "
    sql1 = sql1 & " SQ_GRIGLIAPOSTI.NEXTVAL,"
    sql1 = sql1 & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
    sql1 = sql1 & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO, IDAREA,"
    sql1 = sql1 & idNuovaPianta
    sql1 = sql1 & " FROM GRIGLIAPOSTI WHERE IDPIANTA=" & idVecchiaPianta & ")"
    righeAggiornate = ORADB.ExecuteSQL(sql1)

' inserimento superaree e aree
    sql1 = "SELECT IDAREA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, DESCRIZIONESTAMPAINGRESSO,"
    sql1 = sql1 & " CODICE, INDICEDIPREFERIBILITA, CAPIENZA, SOGLIA1, SOGLIA2, IDTIPOAREA, IDORDINEDIPOSTOSIAE, IDAREA_PADRE, VIRTUALE"
    sql1 = sql1 & " FROM AREA WHERE IDPIANTA = " & idVecchiaPianta
    Set rec1 = ORADB.CreateDynaset(sql1, 0&)
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovaArea = OttieniIdentificatoreDaSequenza("SQ_AREA")
            Set coppiaId = New clsCoppiaIdent
            coppiaId.idVecchio = rec1("IDAREA")
            coppiaId.idNuovo = idNuovaArea
            Call listaCoppieIdentificatoriAree.Add(coppiaId)
            
            ' conviene inserire senza l'indicazione dell'area padre e aggiornare dopo aver inserito tutto
            sql2 = "INSERT INTO CL_AREA("
            sql2 = sql2 & " IDAREA,"
            sql2 = sql2 & " NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, DESCRIZIONESTAMPAINGRESSO,"
            sql2 = sql2 & " CODICE, INDICEDIPREFERIBILITA, CAPIENZA, SOGLIA1, SOGLIA2,"
            sql2 = sql2 & " IDPIANTA,"
            sql2 = sql2 & " IDTIPOAREA, IDORDINEDIPOSTOSIAE, IDAREA_PADRE, VIRTUALE, NUMEROVERSIONE"
            sql2 = sql2 & ") VALUES ("
            sql2 = sql2 & idNuovaArea & ","
            sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOME")) & ", "
            sql2 = sql2 & DefinisciCampoStringaORADB(rec1("DESCRIZIONE")) & ", "
            sql2 = sql2 & DefinisciCampoStringaORADB(rec1("DESCRIZIONEALTERNATIVA")) & ", "
            sql2 = sql2 & DefinisciCampoStringaORADB(rec1("DESCRIZIONEPOS")) & ", "
            sql2 = sql2 & DefinisciCampoStringaORADB(rec1("DESCRIZIONESTAMPAINGRESSO")) & ", "
            sql2 = sql2 & DefinisciCampoStringaORADB(rec1("CODICE")) & ", "
            sql2 = sql2 & IIf(IsNull(rec1("INDICEDIPREFERIBILITA")), "NULL", rec1("INDICEDIPREFERIBILITA")) & ", "
            sql2 = sql2 & IIf(IsNull(rec1("CAPIENZA")), "NULL", rec1("CAPIENZA")) & ", "
            sql2 = sql2 & IIf(IsNull(rec1("SOGLIA1")), "NULL", rec1("SOGLIA1")) & ", "
            sql2 = sql2 & IIf(IsNull(rec1("SOGLIA2")), "NULL", rec1("SOGLIA2")) & ", "
            sql2 = sql2 & idNuovaPianta & ","
            sql2 = sql2 & rec1("IDTIPOAREA") & ", "
            sql2 = sql2 & IIf(IsNull(rec1("IDORDINEDIPOSTOSIAE")), "NULL,", rec1("IDORDINEDIPOSTOSIAE") & ", ")
            sql2 = sql2 & IIf(IsNull(rec1("IDAREA_PADRE")), "NULL, ", rec1("IDAREA_PADRE") & ", ")
            sql2 = sql2 & IIf(IsNull(rec1("VIRTUALE")), "NULL, ", rec1("VIRTUALE") & ", ")
            sql2 = sql2 & 1 & ")"
            righeAggiornate = ORADB.ExecuteSQL(sql2)

            rec1.MoveNext
        Wend
    End If
    rec1.Close

' aggiornamenti AREA.AREA_PADRE, inserimento in GRIGLIAPOSTI
' scorrendo la lista delle coppie di id
    For i = 1 To listaCoppieIdentificatoriAree.count
        idVecchiaArea = listaCoppieIdentificatoriAree(i).idVecchio
        idNuovaArea = listaCoppieIdentificatoriAree(i).idNuovo
        sql1 = "UPDATE CL_AREA"
        sql1 = sql1 & " SET IDAREA_PADRE = " & idNuovaArea
        sql1 = sql1 & " WHERE"
        sql1 = sql1 & " IDPIANTA = " & idNuovaPianta
        sql1 = sql1 & " AND IDAREA_PADRE = " & idVecchiaArea
        righeAggiornate = ORADB.ExecuteSQL(sql1)
        
' questa istruzione vale per i grandi impianti
        sql1 = "INSERT INTO CL_GRIGLIAPOSTI("
        sql1 = sql1 & " IDGRIGLIAPOSTI,"
        sql1 = sql1 & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
        sql1 = sql1 & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO,"
        sql1 = sql1 & " IDAREA,"
        sql1 = sql1 & " IDPIANTA"
        sql1 = sql1 & ") (SELECT "
        sql1 = sql1 & " SQ_GRIGLIAPOSTI.NEXTVAL,"
        sql1 = sql1 & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
        sql1 = sql1 & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO,"
        sql1 = sql1 & idNuovaArea & ", "
        sql1 = sql1 & " IDPIANTA"
        sql1 = sql1 & " FROM GRIGLIAPOSTI WHERE IDAREA = " & idVecchiaArea & ")"
        righeAggiornate = ORADB.ExecuteSQL(sql1)
    Next i

' inserimento classi di superaree
    sql1 = "INSERT INTO CL_CLASSESUPERAREA (IDCLASSESUPERAREA, NOME, DESCRIZIONE, IDPIANTA, CLASSEPRINCIPALE)"
    sql1 = sql1 & "SELECT SQ_CLASSESUPERAREA.NEXTVAL, NOME, DESCRIZIONE, " & idNuovaPianta & ", CLASSEPRINCIPALE"
    sql1 = sql1 & " FROM CLASSESUPERAREA"
    sql1 = sql1 & " WHERE IDPIANTA = " & idVecchiaPianta
    righeAggiornate = ORADB.ExecuteSQL(sql1)

    If clonaFasceSequenze = VB_VERO Then
' aggiornamenti FASCIAPOSTI
        sql1 = "SELECT IDFASCIAPOSTI, F.IDAREA, F.INDICEDIPREFERIBILITA"
        sql1 = sql1 & " FROM FASCIAPOSTI F, AREA A"
        sql1 = sql1 & " WHERE F.IDAREA = A.IDAREA"
        sql1 = sql1 & " AND A.IDPIANTA = " & idVecchiaPianta
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovaFascia = OttieniIdentificatoreDaSequenza("SQ_FASCIAPOSTI")
                Set coppiaId = New clsCoppiaIdent
                coppiaId.idVecchio = rec1("IDFASCIAPOSTI")
                coppiaId.idNuovo = idNuovaFascia
                Call listaCoppieIdentificatoriFasce.Add(coppiaId)
                
                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
                sql2 = "INSERT INTO CL_FASCIAPOSTI("
                sql2 = sql2 & " IDFASCIAPOSTI,"
                sql2 = sql2 & " IDAREA,"
                sql2 = sql2 & " INDICEDIPREFERIBILITA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovaFascia & ","
                sql2 = sql2 & idNuovaArea & ","
                sql2 = sql2 & rec1("INDICEDIPREFERIBILITA") & ")"
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
        
' aggiornamenti SEQUENZAPOSTI
        sql1 = "SELECT IDSEQUENZAPOSTI, S.IDFASCIAPOSTI, S.ORDINEINFASCIA"
        sql1 = sql1 & " FROM SEQUENZAPOSTI S, FASCIAPOSTI F, AREA A"
        sql1 = sql1 & " WHERE S.IDFASCIAPOSTI = F.IDFASCIAPOSTI"
        sql1 = sql1 & " AND F.IDAREA = A.IDAREA"
        sql1 = sql1 & " AND A.IDPIANTA = " & idVecchiaPianta
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovaSequenza = OttieniIdentificatoreDaSequenza("SQ_SEQUENZAPOSTI")
                Set coppiaId = New clsCoppiaIdent
                coppiaId.idVecchio = rec1("IDSEQUENZAPOSTI")
                coppiaId.idNuovo = idNuovaSequenza
                Call listaCoppieIdentificatoriSequenze.Add(coppiaId)
                
                idNuovaFascia = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriFasce, rec1("IDFASCIAPOSTI"))
                sql2 = "INSERT INTO CL_SEQUENZAPOSTI("
                sql2 = sql2 & " IDSEQUENZAPOSTI,"
                sql2 = sql2 & " IDFASCIAPOSTI,"
                sql2 = sql2 & " ORDINEINFASCIA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovaSequenza & ","
                sql2 = sql2 & idNuovaFascia & ","
                sql2 = sql2 & rec1("ORDINEINFASCIA") & ")"
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close

' aggiornamenti POSTO
        sql1 = "SELECT P.IDPOSTO, P.NOMEFILA, P.NOMEPOSTO,"
        sql1 = sql1 & " P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE, P.IDAREA,"
        sql1 = sql1 & " P.IDSEQUENZAPOSTI, P.ORDINEINSEQUENZA, E.ETICHETTA"
        sql1 = sql1 & " FROM POSTO P, AREA A, ETICHETTAPOSTO E"
        sql1 = sql1 & " WHERE P.IDAREA = A.IDAREA"
        sql1 = sql1 & " AND A.IDPIANTA = " & idVecchiaPianta
        sql1 = sql1 & " AND P.IDPOSTO = E.IDPOSTO(+)"
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovoPosto = OttieniIdentificatoreDaSequenza("SQ_POSTO")
                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("IDAREA"))
                If IsNull(rec1("IDSEQUENZAPOSTI")) Then
                    idNuovaSequenza = idNessunElementoSelezionato
                Else
                    idNuovaSequenza = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriSequenze, rec1("IDSEQUENZAPOSTI"))
                End If
                sql2 = "INSERT INTO CL_POSTO("
                sql2 = sql2 & " IDPOSTO,"
                sql2 = sql2 & " NOMEFILA, NOMEPOSTO,"
                sql2 = sql2 & " COORDINATAORIZZONTALE, COORDINATAVERTICALE,"
                sql2 = sql2 & " IDAREA,"
                sql2 = sql2 & " IDSEQUENZAPOSTI,"
                sql2 = sql2 & " ORDINEINSEQUENZA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovoPosto & ","
                sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOMEFILA")) & ", "
                sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOMEPOSTO")) & ", "
                sql2 = sql2 & rec1("COORDINATAORIZZONTALE") & ", "
                sql2 = sql2 & rec1("COORDINATAVERTICALE") & ", "
                sql2 = sql2 & idNuovaArea & ","
                sql2 = sql2 & IIf(idNuovaSequenza = idNessunElementoSelezionato, "NULL,", idNuovaSequenza & ", ")
                sql2 = sql2 & IIf(idNuovaSequenza = idNessunElementoSelezionato, "NULL)", rec1("ORDINEINSEQUENZA") & ")")
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                If Not IsNull(rec1("ETICHETTA")) Then
                    sql2 = "INSERT INTO CL_ETICHETTAPOSTO (IDPOSTO, ETICHETTA) VALUES ("
                    sql2 = sql2 & idNuovoPosto & ", "
                    sql2 = sql2 & DefinisciCampoStringaORADB(rec1("ETICHETTA")) & ")"
                    righeAggiornate = ORADB.ExecuteSQL(sql2)
                End If
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
        
    Else '    clonaFasceSequenze FALSE
' inserisco soltanto i posti senza riferimenti a sequenze
        sql1 = "SELECT P.IDPOSTO, P.NOMEFILA, P.NOMEPOSTO,"
        sql1 = sql1 & " P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE, P.IDAREA POSTOIDAREA, E.ETICHETTA"
        sql1 = sql1 & " FROM POSTO P, AREA A, ETICHETTAPOSTO E"
        sql1 = sql1 & " WHERE P.IDAREA = A.IDAREA"
        sql1 = sql1 & " AND A.IDPIANTA = " & idVecchiaPianta
        sql1 = sql1 & " AND P.IDPOSTO = E.IDPOSTO(+)"
        Set rec1 = ORADB.CreateDynaset(sql1, 0&)
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovoPosto = OttieniIdentificatoreDaSequenza("SQ_POSTO")
                idNuovaArea = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriAree, rec1("POSTOIDAREA"))
                sql2 = "INSERT INTO CL_POSTO("
                sql2 = sql2 & " IDPOSTO,"
                sql2 = sql2 & " NOMEFILA, NOMEPOSTO,"
                sql2 = sql2 & " COORDINATAORIZZONTALE, COORDINATAVERTICALE, IDAREA,"
                sql2 = sql2 & " IDSEQUENZAPOSTI, ORDINEINSEQUENZA"
                sql2 = sql2 & ") VALUES ("
                sql2 = sql2 & idNuovoPosto & ","
                sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOMEFILA")) & ", "
                sql2 = sql2 & DefinisciCampoStringaORADB(rec1("NOMEPOSTO")) & ", "
                sql2 = sql2 & rec1("COORDINATAORIZZONTALE") & ", "
                sql2 = sql2 & rec1("COORDINATAVERTICALE") & ", "
                sql2 = sql2 & idNuovaArea & ","
                sql2 = sql2 & " NULL, NULL)"
                righeAggiornate = ORADB.ExecuteSQL(sql2)
    
                If Not IsNull(rec1("ETICHETTA")) Then
                    sql2 = "INSERT INTO CL_ETICHETTAPOSTO (IDPOSTO, ETICHETTA) VALUES ("
                    sql2 = sql2 & idNuovoPosto & ", "
                    sql2 = sql2 & DefinisciCampoStringaORADB(rec1("ETICHETTA")) & ")"
                    righeAggiornate = ORADB.ExecuteSQL(sql2)
                End If
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
    
    End If
    
' aggiornamento CC_PIANTAINCLONAZIONE
    sql1 = "UPDATE CC_PIANTAINCLONAZIONE SET DATAORAFINECLONAZIONE = SYSDATE"
    sql1 = sql1 & " WHERE IDVECCHIAPIANTA = " & idVecchiaPianta
    righeAggiornate = ORADB.ExecuteSQL(sql1)

    Call ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA
    
    Call ScriviLog(CCTA_CLONAZIONE, CCDA_PIANTA, , "IDPIANTA MASTER = " & idVecchiaArea, idNuovaPianta)

    IsClonazionePiantaOK = True
End Function

Public Function CancellaRecord(conn As OraDatabase, nomeTabella As String, nomeCampoCondizione As String, valoreCampoCondizione As Long, nomeCampoIntervallo As String) As Long
    Dim sql As String
    Dim rec As OraDynaset
    Dim minimo As Long
    Dim massimo As Long
    Dim primoId As Long
    Dim numeroRecordCancellati As Long
    Dim numeroTotaleRecordCancellati As Long
    
    sql = "SELECT MIN(" & nomeCampoIntervallo & ") AS MINIMO,"
    sql = sql & " MAX(" & nomeCampoIntervallo & ") AS MASSIMO"
    sql = sql & " FROM " & nomeTabella
    sql = sql & " WHERE " & nomeCampoCondizione & " = " & valoreCampoCondizione
    Set rec = conn.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        minimo = IIf(IsNull(rec("MINIMO")), 1, rec("MINIMO"))
        massimo = IIf(IsNull(rec("MASSIMO")), 0, rec("MASSIMO"))
    End If
    
    ' adesso cancella 100 righe per volta
    primoId = minimo
    numeroTotaleRecordCancellati = 0
    While primoId <= massimo
        sql = "DELETE FROM " & nomeTabella
        sql = sql & " WHERE " & nomeCampoCondizione & " = " & valoreCampoCondizione
        sql = sql & " AND " & nomeCampoIntervallo & " BETWEEN " & primoId & " AND " & primoId + 100
        numeroRecordCancellati = conn.ExecuteSQL(sql)
        
        numeroTotaleRecordCancellati = numeroTotaleRecordCancellati + numeroRecordCancellati
        primoId = primoId + 100
    Wend
    
    CancellaRecord = numeroTotaleRecordCancellati
End Function

Public Function PI_GRECO() As Double
    PI_GRECO = 4 * Atn(1)
End Function

Public Sub InserisciInformazioniSistemistichePerNuovaOrganizzazione(idOrganizzazione As Long)
    Dim sql As String
    Dim n As Long
    
    sql = " INSERT INTO SERVENTEORGANIZZAZIONE ("
    sql = sql & " IDORGANIZZAZIONE, NUMEROTHREAD_MINIMO, NUMEROTHREAD_MASSIMO, "
    sql = sql & " NUMEROTHREAD_INCREMENTO, IDTIPOCONNESSIONEDATABASE, DURATAMAXCONNESSIONIINATTIVE, "
    sql = sql & " NUMCONNESSIONIDBMS_MINIMO, NUMCONNESSIONIDBMS_MASSIMO, NUMCONNESSIONIDBMS_INCREMENTO, "
    sql = sql & " NUMPROCESSIDBMS_MINIMO, NUMPROCESSIDBMS_MASSIMO, NUMPROCESSIDBMS_INCREMENTO)"
    sql = sql & " VALUES (" & idOrganizzazione & ","
    sql = sql & " 5, 20, 1, 2, 300, 1, 10, 1, NULL, NULL, NULL)"
    SETAConnection.Execute sql, n, adCmdText
    sql = " INSERT INTO SERVENTEORGANIZZAZIONE_TIPOTER ("
    sql = sql & " IDORGANIZZAZIONE, IDTIPOTERMINALE, RAPPORTO_NUMTHREAD_NUMSESSIONI) "
    sql = sql & " VALUES (" & idOrganizzazione & ", 1, 0.5)"
    SETAConnection.Execute sql, n, adCmdText
    sql = " INSERT INTO SERVENTEORGANIZZAZIONE_TIPOTER ("
    sql = sql & " IDORGANIZZAZIONE, IDTIPOTERMINALE, RAPPORTO_NUMTHREAD_NUMSESSIONI) "
    sql = sql & " VALUES (" & idOrganizzazione & ", 2, 1)"
    SETAConnection.Execute sql, n, adCmdText
    sql = " INSERT INTO SERVENTEORGANIZZAZIONE_TIPOTER ("
    sql = sql & " IDORGANIZZAZIONE, IDTIPOTERMINALE, RAPPORTO_NUMTHREAD_NUMSESSIONI) "
    sql = sql & " VALUES (" & idOrganizzazione & ", 3, 0.2)"
    SETAConnection.Execute sql, n, adCmdText
End Sub

Public Function IsElementoPresenteInLista(lista As Collection, idElemento As Long) As Boolean
    Dim elemento As Variant
    
On Error GoTo gestioneErrori

    IsElementoPresenteInLista = True
    elemento = lista.Item(ChiaveId(idElemento))
    
    Exit Function
    
gestioneErrori:
    IsElementoPresenteInLista = False
End Function

Public Sub EliminaUtilizzatoriRappresentazioni(utilizzatoriEliminati As Long)
    Dim sql As String
    Dim n As Long
    
    sql = "DELETE FROM UTILIZZATORE"
    SETAConnection.Execute sql, n, adCmdText
    utilizzatoriEliminati = n
End Sub

Public Sub InserisciTariffaOmaggioCollegata(idTariffa As Long, conn As ADODB.Connection)
    Dim sql As String
    Dim sql2 As String
    Dim sqlInsert As String
    Dim rec As New ADODB.Recordset
    Dim rec2 As New ADODB.Recordset
    Dim idNuovaTariffa As Long
    Dim numeroRigheInserite As Long

'    sql = "SELECT IDTARIFFA, NOME, DESCRIZIONE, CODICE, IDPRODOTTO, DESCRIZIONEPOS," & _
'        " DESCRIZIONEALTERNATIVA, IDORGANIZZAZIONE, IDMODALITAEMISSIONE," & _
'        " IDPRODOTTO, IDTIPOTARIFFA, IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC" & _
'        " FROM TARIFFA" & _
'        " WHERE IDTARIFFA = " & idTariffa
'    rec.Open sql, conn, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        If rec("IDTIPOTARIFFA") = T_INTERI Or rec("IDTIPOTARIFFA") = T_RIDOTTI Then
'            idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
'
'            sql2 = "INSERT INTO TARIFFA (IDTARIFFA, NOME, DESCRIZIONE, CODICE, DESCRIZIONEPOS,"
'            sql2 = sql2 & " DESCRIZIONEALTERNATIVA, IDORGANIZZAZIONE, IDMODALITAEMISSIONE,"
'            sql2 = sql2 & " IDPRODOTTO, IDTIPOTARIFFA, IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC,"
'            sql2 = sql2 & " IDTARIFFA_RIFERIMENTO)"
'            sql2 = sql2 & " VALUES ("
'            sql2 = sql2 & idNuovaTariffa & ", "
'            sql2 = sql2 & SqlStringValue(Left("O_" & rec("NOME"), 30)) & ", "
'            sql2 = sql2 & SqlStringValue(Left("O_" & rec("DESCRIZIONE"), 255)) & ", "
'            sql2 = sql2 & SqlStringValue(Left("O_" & rec("CODICE"), 10)) & ", "
'            If IsNull(rec("DESCRIZIONEPOS")) Then
'                sql2 = sql2 & "NULL, "
'            Else
'                sql2 = sql2 & SqlStringValue(Left("O_" & rec("DESCRIZIONEPOS"), 10)) & ", "
'            End If
'            sql2 = sql2 & SqlStringValue(Left("O_" & rec("DESCRIZIONEALTERNATIVA"), 30)) & ", "
'            sql2 = sql2 & "NULL, "
'            sql2 = sql2 & rec("IDMODALITAEMISSIONE") & ", "
'            sql2 = sql2 & rec("IDPRODOTTO") & ", "
'            sql2 = sql2 & T_OMAGGIO & ", "
'            sql2 = sql2 & rec("IDAMBITOAPPLICABILITATARIFFA") & ", "
'            sql2 = sql2 & rec("IDCLASSETARIFFACONTROLLOACC") & ", "
'            sql2 = sql2 & idTariffa & ")"
'            conn.Execute sql2, numeroRigheInserite, adCmdText
'            If numeroRigheInserite <> 1 Then
'                MsgBox "Errore nell'inserimento della tariffa omaggio"
'            Else
'                sql = "INSERT INTO TARIFFA_TIPOTERMINALE (IDTARIFFA, IDTIPOTERMINALE, IDTASTOTIPOTERMINALE)" & _
'                    " SELECT " & idNuovaTariffa & ", IDTIPOTERMINALE, NULL" & _
'                    " FROM TARIFFA_TIPOTERMINALE " & _
'                    " WHERE IDTARIFFA = " & idTariffa
'                    conn.Execute sql, numeroRigheInserite, adCmdText
'            End If
'        End If
'    End If
'    rec.Close
End Sub

Public Sub CaricaValoriCombo1(cmb As ComboBox, strSQL As String, NomeCampo As String, idRecordSelezionato As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("CODICE")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If idRecordSelezionato <> idTuttiGliElementiSelezionati Then
        If i <= 0 Then
            i = 1
        Else
            'Do Nothing
        End If
        cmb.AddItem "<nessuno>"
        cmb.ItemData(i - 1) = idNessunElementoSelezionato
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub CaricaValoriCombo2(ByRef cmb As ComboBox, strSQL As String, NomeCampo As String, elencaValoriEstesi As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    If elencaValoriEstesi Then
        If i <= 0 Then
            i = 1
        End If
        cmb.AddItem "<nuovo...>"
        cmb.ItemData(i - 1) = idAggiungiNuovo
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub CaricaValoriCombo3(ByRef cmb As ComboBox, strSQL As String, NomeCampo As String, numeroMassimoElementi As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
On Error GoTo gestioneErroreCaricaValoriCombo
    cmb.Clear
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            If i <= numeroMassimoElementi Then
                cmb.AddItem rec("NOME")
                cmb.ItemData(i - 1) = rec("ID").Value
                i = i + 1
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Sub
    
gestioneErroreCaricaValoriCombo:
    MsgBox "Errore nel caricamento della casella combinata: " & Err.Description
End Sub

Public Sub CaricaValoriComboConOpzioneTutti(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub CaricaValoriComboConOpzioneTuttiIniziale(cmb As ComboBox, strSQL As String, NomeCampo As String, sTutti As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    cmb.AddItem sTutti
    cmb.ItemData(0) = idTuttiGliElementiSelezionati
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 2
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub CaricaValoriComboDaLista(ByRef cmb As ComboBox, listaElementi As Collection)
    Dim i As Integer
    Dim e As clsElementoLista
    
    i = 1
    For Each e In listaElementi
        cmb.AddItem e.descrizioneElementoLista
        cmb.ItemData(i - 1) = e.idElementoLista
        i = i + 1
    Next e
End Sub

Public Sub SelezionaElementoSuCombo(ByRef cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
End Sub

Public Sub SelezionaElementoSuGriglia(ByRef r As ADODB.Recordset, id As Long, Optional ByRef internalEvent As Boolean = False)
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    If Not (r.BOF And r.EOF) Then
        r.MoveFirst
        Do While Not r.EOF
            If id = r("IDTARIFFA") Then
                Exit Do
            End If
            r.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
End Sub

Public Sub ListaStringhe_Aggiungi(ByRef lista As String, valoreDaAggiungere As String, separatore As String, Optional aggiungiSeGiaPresente As Boolean = False)
    Dim pos As Long
    
    pos = InStr(0, lista, valoreDaAggiungere, vbTextCompare)
    
    If pos <> 0 Then
        If aggiungiSeGiaPresente Then
            lista = lista + separatore + valoreDaAggiungere
        End If
    Else
        lista = lista + separatore + valoreDaAggiungere
    End If
End Sub

Public Function CollezioneToStringa(lista As Collection, carattereSeparatoreInOutput As String) As String
    Dim primoItem As Boolean
    Dim Item As Variant
    Dim stringa As String
    
    If Not IsNull(lista) Then
        primoItem = True
        
        For Each Item In lista
            If primoItem Then
                primoItem = False
            Else
                stringa = stringa & carattereSeparatoreInOutput
            End If
            
            stringa = stringa & Item
        Next Item
    End If
    
    CollezioneToStringa = stringa
End Function

Public Function SelezionatiSuListBoxToStringa(listBox As listBox, carattereSeparatoreInOutput As String) As String
    Dim primoItem As Boolean
    Dim i As Long
    Dim Item As Variant
    Dim stringa As String
    
    stringa = ""
    If Not IsNull(listBox) Then
        primoItem = True
        
        For i = 0 To listBox.ListCount - 1
            If listBox.Selected(i) Then
                If primoItem Then
                    primoItem = False
                Else
                    stringa = stringa & carattereSeparatoreInOutput
                End If
                stringa = stringa & listBox.ItemData(i)
            End If
        Next i
    End If
    
    SelezionatiSuListBoxToStringa = stringa
End Function

Public Sub ListBox_SelezioneSingola(ByRef lstBox As listBox, selectedItem As Integer)
    Dim listItem As Variant
    
    For listItem = 0 To lstBox.ListCount - 1
        lstBox.Selected(listItem) = (selectedItem = listItem)
    Next
    
End Sub

Public Function prodottoPermetteGestioneTariffeOmaggio(idProdotto As Long) As ValoreBooleanoEnum
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    prodottoPermetteGestioneTariffeOmaggio = VB_FALSO
    sql = "SELECT GESTECCEDENZEOMAGGIOEVOLUTA FROM PRODOTTO"
    sql = sql & " WHERE IDPRODOTTO=" & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        prodottoPermetteGestioneTariffeOmaggio = rec("GESTECCEDENZEOMAGGIOEVOLUTA").Value
    End If
    rec.Close
End Function

Public Function esisteTariffaOmaggioCollegata(idTariffa As Long) As ValoreBooleanoEnum
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    esisteTariffaOmaggioCollegata = VB_FALSO
    sql = "SELECT IDTARIFFA FROM TARIFFA"
    sql = sql & " WHERE IDTARIFFA_RIFERIMENTO=" & idTariffa
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        esisteTariffaOmaggioCollegata = VB_VERO
    End If
    rec.Close
End Function

Public Function IdTariffaOmaggioAssociata(idTariffa As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    IdTariffaOmaggioAssociata = idNessunElementoSelezionato
    sql = "SELECT IDTARIFFA FROM TARIFFA"
    sql = sql & " WHERE IDTARIFFA_RIFERIMENTO=" & idTariffa
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        IdTariffaOmaggioAssociata = rec("IDTARIFFA")
    End If
    rec.Close
End Function

Public Sub inserisciDefaultTipiOperazione(idProdotto As Long, rientraInDecretoSicurezza As ValoreBooleanoEnum)
    Dim sql As String
    Dim n As Long
    
    SETAConnection.BeginTrans
    
' Cancellazione
'    sql = "DELETE FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO = " & idProdotto
'    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ WHERE IDPRODOTTO = " & idProdotto
    SETAConnection.Execute sql, n, adCmdText

' Sede organizzatore (Terminale Avanzato)
'    sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'    sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_AVANZATO & ", IDTIPOOPERAZIONE"
'    sql = sql & " FROM TIPOOPERAZIONE"
'    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'    sql = sql & "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29"
'    sql = sql & ")"
'    SETAConnection.Execute sql, n, adCmdText
    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
    sql = sql & " SELECT " & idProdotto & ", " & TCPV_SEDE_ORGANIZZATORE & ", IDTIPOOPERAZIONE, " & SqlDateTimeValue(dataInferioreMinima) & ", " & SqlDateTimeValue(dataSuperioreMassima)
    sql = sql & " FROM TIPOOPERAZIONE"
    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
    sql = sql & "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29"
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText

' Impianto (Terminale Avanzato)
    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
    sql = sql & " SELECT " & idProdotto & ", " & TCPV_IMPIANTO & ", IDTIPOOPERAZIONE, " & SqlDateTimeValue(dataInferioreMinima) & ", " & SqlDateTimeValue(dataSuperioreMassima)
    sql = sql & " FROM TIPOOPERAZIONE"
    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
    sql = sql & "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29"
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText

' Altri PV organizzatore (Terminale Avanzato)
    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
    sql = sql & " SELECT " & idProdotto & ", " & TCPV_ALTRI_PUNTI_VENDITA_ORGANIZZATORE & ", IDTIPOOPERAZIONE, " & SqlDateTimeValue(dataInferioreMinima) & ", " & SqlDateTimeValue(dataSuperioreMassima)
    sql = sql & " FROM TIPOOPERAZIONE"
    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
    sql = sql & "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29"
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText

' Affiliati (Terminale Avanzato)
    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
    sql = sql & " SELECT " & idProdotto & ", " & TCPV_PUNTI_VENDITA_AFFILIATI & ", IDTIPOOPERAZIONE, " & SqlDateTimeValue(dataInferioreMinima) & ", " & SqlDateTimeValue(dataSuperioreMassima)
    sql = sql & " FROM TIPOOPERAZIONE"
    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
    sql = sql & "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29"
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText

' Terminale POS
'    sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'    sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_POS & ", IDTIPOOPERAZIONE"
'    sql = sql & " FROM TIPOOPERAZIONE"
'    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'    sql = sql & TO_VENDITA & ","
'    sql = sql & TO_ANNULLAMENTO_VENDITA
'    sql = sql & ")"
'    SETAConnection.Execute sql, n, adCmdText

' Terminale WEB
'    sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'    sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_WEB & ", IDTIPOOPERAZIONE"
'    sql = sql & " FROM TIPOOPERAZIONE"
'    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'    sql = sql & TO_RISERVAZIONE & ","
'    sql = sql & TO_ANNULLAMENTO_RISERVAZIONE & ","
'    sql = sql & TO_VENDITA_SENZA_STAMPA & ","
'    sql = sql & TO_GESTIONE_ORDINE
'    If rientraInDecretoSicurezza Then
'        sql = sql & ","
'        sql = sql & TO_CAMBIO_UTILIZZATORE & ","
'        sql = sql & TO_PERFEZIONAMENTO
'    End If
'    sql = sql & ")"
'    SETAConnection.Execute sql, n, adCmdText

' Terminale TOTEM
'    sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'    sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_TOTEM & ", IDTIPOOPERAZIONE"
'    sql = sql & " FROM TIPOOPERAZIONE"
'    sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'    sql = sql & TO_RISERVAZIONE & ","
'    sql = sql & TO_ANNULLAMENTO_RISERVAZIONE & ","
'    sql = sql & TO_VENDITA_SENZA_STAMPA & ","
'    sql = sql & TO_GESTIONE_ORDINE
'    sql = sql & ")"
'    SETAConnection.Execute sql, n, adCmdText

    SETAConnection.CommitTrans

End Sub
'
'Public Sub inserisciDefaultTipiOperazionePerTipoTerminale(idProdotto As Long, rientraInDecretoSicurezza As ValoreBooleanoEnum, idTipoTerminale As Long)
'    Dim sql As String
'    Dim n As Long
'
'    SETAConnection.BeginTrans
'
'' Cancellazione
'    sql = "DELETE FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ"
'    sql = sql + " WHERE IDPRODOTTO = " & idProdotto
'    sql = sql + " AND IDTIPOTERMINALE = " & idTipoTerminale
'    SETAConnection.Execute sql, n, adCmdText
'
'' Terminale Avanzato
'    If idTipoTerminale = TT_TERMINALE_AVANZATO Then
'        sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'        sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_AVANZATO & ", IDTIPOOPERAZIONE"
'        sql = sql & " FROM TIPOOPERAZIONE"
'        sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'        sql = sql & "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35"
'        sql = sql & ")"
'        SETAConnection.Execute sql, n, adCmdText
'    End If
'
'' Terminale WEB
'    If idTipoTerminale = TT_TERMINALE_WEB Then
'        sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'        sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_WEB & ", IDTIPOOPERAZIONE"
'        sql = sql & " FROM TIPOOPERAZIONE"
'        sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'        sql = sql & TO_RISERVAZIONE & ","
'        sql = sql & TO_ANNULLAMENTO_RISERVAZIONE & ","
'        sql = sql & TO_VENDITA_SENZA_STAMPA & ","
'        sql = sql & TO_GESTIONE_ORDINE
'        If rientraInDecretoSicurezza Then
'            sql = sql & ","
'            sql = sql & TO_CAMBIO_UTILIZZATORE & ","
'            sql = sql & TO_PERFEZIONAMENTO
'        End If
'        sql = sql & ")"
'        SETAConnection.Execute sql, n, adCmdText
'    End If
'
'' Terminale POS
'    If idTipoTerminale = TT_TERMINALE_POS Then
'        sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'        sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_POS & ", IDTIPOOPERAZIONE"
'        sql = sql & " FROM TIPOOPERAZIONE"
'        sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'        sql = sql & TO_VENDITA & ","
'        sql = sql & TO_ANNULLAMENTO_VENDITA
'        sql = sql & ")"
'        SETAConnection.Execute sql, n, adCmdText
'    End If
'
'' Terminale TOTEM
'    If idTipoTerminale = TT_TERMINALE_TOTEM Then
'        sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE, IDTIPOOPERAZIONE)"
'        sql = sql & " SELECT " & idProdotto & ", " & TT_TERMINALE_TOTEM & ", IDTIPOOPERAZIONE"
'        sql = sql & " FROM TIPOOPERAZIONE"
'        sql = sql & " WHERE IDTIPOOPERAZIONE IN ("
'        sql = sql & TO_RISERVAZIONE & ","
'        sql = sql & TO_ANNULLAMENTO_RISERVAZIONE & ","
'        sql = sql & TO_VENDITA_SENZA_STAMPA & ","
'        sql = sql & TO_GESTIONE_ORDINE
'        sql = sql & ")"
'        SETAConnection.Execute sql, n, adCmdText
'    End If
'
'    SETAConnection.CommitTrans
'
'End Sub

Public Function isProdottoCalendario(idProdotto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idClasseProdotto As Long
    
    isProdottoCalendario = False

    sql = "SELECT IDCLASSEPRODOTTO FROM PRODOTTO WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idClasseProdotto = rec("IDCLASSEPRODOTTO")
            rec.MoveNext
        Wend
    End If
    rec.Close

    If idClasseProdotto = CPR_GIORNALIERO Or idClasseProdotto = CPR_TESSERA Or idClasseProdotto = CPR_OPEN Then
        isProdottoCalendario = True
    Else
        isProdottoCalendario = False
    End If
End Function

Public Function ContaRecord(sqlIn As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    ContaRecord = -1
    
    sql = "SELECT COUNT(*) AS CONT FROM (" & sqlIn & ")"
    Call ApriConnessioneBD
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        ContaRecord = rec("CONT").Value
    End If
    rec.Close
    
End Function

Public Function LeggiIdUltimoProdottoDaRegistro(s As String) As Long
    If s = "" Then
        LeggiIdUltimoProdottoDaRegistro = idNessunElementoSelezionato
    ElseIf Not IsNumeric(CLng(Trim(s))) Then
        LeggiIdUltimoProdottoDaRegistro = idNessunElementoSelezionato
    Else
        LeggiIdUltimoProdottoDaRegistro = CLng(Trim(s))
    End If
End Function

Public Sub PopolaLista(lst As listBox, sql As String)
    Dim rec As OraDynaset
    Dim i As Long
    
    lst.Clear
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lst.AddItem(rec("NOME"), i)
            lst.ItemData(i) = rec("ID")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

' utilizza sempre i campi ID e NOME
Public Sub CaricaValoriLista(lst As listBox, strSQL As String)
    Dim rec As OraDynaset
    Dim i As Long
    
    Call ApriConnessioneBD_ORA

    Set rec = ORADB.CreateDynaset(strSQL, 0&)
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            lst.AddItem rec("NOME")
            lst.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
        
End Sub

Public Function lPad(sIn As String, lunghezzaComplessiva As Long, char As String) As String
    Dim i As Integer
    Dim sout As String
    
    sout = sIn
    For i = Len(sIn) + 1 To lunghezzaComplessiva
        sout = sout & char
    Next i
    lPad = sout
End Function

Public Function classePuntoVenditaConDirittiOperatoreGestibili(idClassePuntoVendita As Long) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT DIRITTIOPERATOREGESTIBILI" & _
        " FROM CLASSEPUNTOVENDITA" & _
        " WHERE IDCLASSEPUNTOVENDITA = " & idClassePuntoVendita

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("DIRITTIOPERATOREGESTIBILI") = 0 Then
            classePuntoVenditaConDirittiOperatoreGestibili = False
        Else
            classePuntoVenditaConDirittiOperatoreGestibili = True
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
        
End Function

Public Function tipoOperazioneAncoraNonAttiva(idClasseSuperareaProdotto As Long, idTipoOperazione As Long, idClassePuntoVendita As Long) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    
    tipoOperazioneAncoraNonAttiva = True
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT DATAORAINIZIOVALIDITA - SYSDATE AS DIFF" & _
        " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCTO" & _
        " WHERE PCTO.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdotto & _
        " AND PCTO.IDCLASSEPUNTOVENDITA = " & idClassePuntoVendita & _
        " AND PCTO.IDTIPOOPERAZIONE = " & idTipoOperazione

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("DIFF") < 0 Then
            tipoOperazioneAncoraNonAttiva = False
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
        
End Function

Public Function getIdPuntoVendita(nomePV As String) As Long
    Dim sql As String
    Dim rec As OraDynaset

    getIdPuntoVendita = idNessunElementoSelezionato

    sql = "SELECT IDPUNTOVENDITA AS ID"
    sql = sql + " FROM PUNTOVENDITA"
    sql = sql + " WHERE NOME LIKE '" & nomePV & "%'"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If rec.EOF = False Then
        rec.MoveFirst
        getIdPuntoVendita = rec("ID")
    End If
    rec.Close

End Function

Public Function getIdTerminale(codiceTerminale As String) As Long
    Dim sql As String
    Dim rec As OraDynaset

    getIdTerminale = idNessunElementoSelezionato

    sql = "SELECT IDTERMINALE AS ID"
    sql = sql + " FROM TERMINALE"
    sql = sql + " WHERE CODICE = '" & codiceTerminale & "'"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If rec.EOF = False Then
        rec.MoveFirst
        getIdTerminale = rec("ID")
    End If
    rec.Close

End Function

Public Function CalcolaRGB(indice As Long, colore As String) As Long
    Dim r As String
    Dim g As String
    Dim b As String
    Dim i As Long
    
    If colore = "" Then
        i = indice
    Else
        If colore = COLORE_GIALLO_1 Then
            i = GIALLO_1
        ElseIf colore = COLORE_GIALLO_2 Then
            i = GIALLO_2
        ElseIf colore = COLORE_GIALLO_3 Then
            i = GIALLO_3
        ElseIf colore = COLORE_GIALLO_4 Then
            i = GIALLO_4
        ElseIf colore = COLORE_ARANCIO_1 Then
            i = ARANCIO_1
        ElseIf colore = COLORE_ROSA_1 Then
            i = ROSA_1
        ElseIf colore = COLORE_ROSA_2 Then
            i = ROSA_2
        ElseIf colore = COLORE_ROSA_3 Then
            i = ROSA_3
        ElseIf colore = COLORE_LEGNO_1 Then
            i = LEGNO_1
        ElseIf colore = COLORE_LEGNO_2 Then
            i = LEGNO_2
        ElseIf colore = COLORE_LEGNO_3 Then
            i = LEGNO_3
        ElseIf colore = COLORE_ACQUA_1 Then
            i = ACQUA_1
        ElseIf colore = COLORE_ACQUA_2 Then
            i = ACQUA_2
        ElseIf colore = COLORE_ACQUA_3 Then
            i = ACQUA_3
        ElseIf colore = COLORE_BLU_1 Then
            i = BLU_1
        ElseIf colore = COLORE_BLU_2 Then
            i = BLU_2
        ElseIf colore = COLORE_BLU_3 Then
            i = BLU_3
        ElseIf colore = COLORE_VIOLA_1 Then
            i = VIOLA_1
        ElseIf colore = COLORE_VIOLA_2 Then
            i = VIOLA_2
        ElseIf colore = COLORE_VIOLA_3 Then
            i = VIOLA_3
        ElseIf colore = COLORE_FUXIA_1 Then
            i = FUXIA_1
        ElseIf colore = COLORE_VERDE_1 Then
            i = VERDE_1
        ElseIf colore = COLORE_VERDE_2 Then
            i = VERDE_2
        ElseIf colore = COLORE_VERDE_3 Then
            i = VERDE_3
        Else
            i = 0
        End If
    End If
    
    If i >= 1 And i <= NUMERO_COLORI Then
        r = "&H" & Mid(colori(i, 2), 1, 2)
        g = "&H" & Mid(colori(i, 2), 3, 2)
        b = "&H" & Mid(colori(i, 2), 5, 2)
        CalcolaRGB = RGB(Val(r), Val(g), Val(b))
    Else
        CalcolaRGB = RGB(Val("&HFF"), Val("&HFF"), Val("&HFF"))
    End If
    
End Function

Public Sub AggiornaIDgenereSiaePredominante(idRecordSelezionato As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Dim idGenereSiae As Long
    
    Call ApriConnessioneBD
    
    sql = "       SELECT MIN(IDGENERESIAE) IDGENERESIAEPREDOMINANTE "
    sql = sql & "       FROM "
    sql = sql & "             ( "
    sql = sql & "           SELECT IDPRODOTTO, IDGENERESIAE, MAX(INCIDENZA) MAX_INCIDENZA "
    sql = sql & "             FROM "
    sql = sql & "                   ( "
    sql = sql & "                   SELECT PR.IDPRODOTTO, SG.IDGENERESIAE, SUM(SG.INCIDENZA) INCIDENZA "
    sql = sql & "                   FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, SPETTACOLO_GENERESIAE SG "
    sql = sql & "                   WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE "
    sql = sql & "                   AND R.IDSPETTACOLO = SG.IDSPETTACOLO "
    sql = sql & "                   AND idprodotto = " & idRecordSelezionato
    sql = sql & "                   GROUP BY PR.IDPRODOTTO, SG.IDGENERESIAE "
    sql = sql & "                   ) "
    sql = sql & "             GROUP BY ROLLUP (IDPRODOTTO, IDGENERESIAE) "
    sql = sql & "             ) "
    sql = sql & "       GROUP BY IDPRODOTTO, MAX_INCIDENZA "
    sql = sql & "       HAVING IDPRODOTTO IS NOT NULL AND COUNT(*) = COUNT(IDGENERESIAE) + 1"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        idGenereSiae = rec("IDGENERESIAEPREDOMINANTE").Value
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    sql = "UPDATE prodotto SET idGenereSiaePredominante = " & idGenereSiae & " WHERE idprodotto = " & idRecordSelezionato
    SETAConnection.Execute sql, , adCmdText
    
End Sub

Public Function CreaNomeTabellaTemporanea(radice As String) As String
    Call Randomize
    CreaNomeTabellaTemporanea = SqlStringTableName(radice & Year(Now) & Month(Now) & Day(Now) & Hour(Now) & Minute(Now) & Second(Now) & "_" & CInt(Int((1000 * Rnd()) + 1)))
End Function

Public Function generaStringaCasuale(lunghezza As Integer) As String
    Dim i As Integer
    Dim s As String
    Dim c As Integer
    
    s = ""
    For i = 1 To lunghezza
        Randomize
        
        ' 26 � il numero delle lettere + 10 cifre
        c = Int(36 * Rnd)
        ' 65 il codice ascii di A
        If c > 9 Then
            s = s & Chr(c - 10 + 65)
        Else
            s = s + Chr(c + 48)
        End If
    Next i

    generaStringaCasuale = s

End Function
