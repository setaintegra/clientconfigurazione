VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmConfermaSessione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Conferma"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   8955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   3900
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Annulla"
      Height          =   315
      Left            =   7860
      TabIndex        =   1
      Top             =   3900
      Width           =   1035
   End
   Begin MSDataGridLib.DataGrid dgrOggettiDaAggiornare 
      Height          =   2715
      Left            =   60
      TabIndex        =   4
      Top             =   1080
      Width           =   8835
      _ExtentX        =   15584
      _ExtentY        =   4789
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "TitoloDataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc adcOggettiDaAggiornare 
      Height          =   330
      Left            =   2880
      Top             =   3900
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblSottoTitolo 
      Caption         =   "descrittoreSessione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   420
      Width           =   8715
   End
   Begin VB.Label lblTitolo 
      Caption         =   "Conferma la sessione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   60
      Width           =   6075
   End
End
Attribute VB_Name = "frmConfermaSessione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sessioneCorrente As clsSessione 'Oggetto locale del form
Private ListaTabelleDaAggiornare_Insert As New Collection
Private ListaTabelleDaAggiornare_Delete As New Collection
Private internalEvent As Boolean
Private tabellaCorrente As String
Private tuttoOK As Boolean
Private listaPrezziDaNonAggiornare As Collection

Private Sub AggiornaAbilitazioneControlli()
    If tipoSessioneConfigurazione = TSC_PRODOTTO Then
        dgrOggettiDaAggiornare.Caption = "PRODOTTI DA AGGIORNARE"
'        cmdConferma.Enabled = adcOggettiDaAggiornare.Recordset.RecordCount > 0
    ElseIf tipoSessioneConfigurazione = TSC_ORGANIZZAZIONE Then
        dgrOggettiDaAggiornare.Caption = "ORGANIZZAZIONI DA AGGIORNARE"
'        cmdConferma.Enabled = adcOggettiDaAggiornare.Recordset.RecordCount > 0
    End If
    If (StatoProdottiAttivi Or StatoOrganizzazioniAttive) Then
        cmdConferma.Enabled = False
        Call frmMessaggio.Visualizza("AvvertimentoServertiOrganizzazioniOProdottiAttivi")
    End If
    cmdConferma.Enabled = (tipoSessioneConfigurazione <> TSC_NON_SPECIFICATO)
End Sub

Private Sub Annulla()
    Unload Me
End Sub

Private Sub Conferma()
    Call ListeTabelleDaAggiornare_Init
    If ListaTabelleDaAggiornare_Insert.count = 0 And ListaTabelleDaAggiornare_Delete.count = 0 Then
        Call frmMessaggio.Visualizza("ConfermaSessioneVuota")
        tuttoOK = (frmMessaggio.exitCode = EC_CONFERMA)
    Else
        Call TrasferisciDatiInSeta_Integra
    End If
    If tuttoOK Then
        Call sessioneCorrente.Conferma(idSessioneConfigurazioneCorrente)
        Call adcOggettiDaAggiornare_Init
        Set sessioneCorrente = Nothing
        tipoSessioneConfigurazione = TSC_NON_SPECIFICATO
        idSessioneConfigurazioneCorrente = idNessunElementoSelezionato
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub DeleteInSeta_Integra()
    Dim sql As String
    Dim n As Long
    
'    Eliminazione record in SETA_INTEGRA
    sql = "DELETE FROM " & nomeSchema & tabellaCorrente & _
        "  WHERE (" & StringaChiaviPrimarie & ") IN " & _
        " (SELECT " & StringaChiaviPrimarie & _
        "  FROM  " & tabellaCorrente & _
        "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_ELIMINATO & _
        "  AND   " & tabellaCorrente & ".IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & " )"
    SETAConnection.Execute sql, n, adCmdText
    
'    Eliminazione record in SETA_CCT
    sql = "DELETE FROM " & tabellaCorrente & _
        "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_ELIMINATO & _
        "  AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
    SETAConnection.Execute sql, n, adCmdText
    
End Sub

Private Sub DescrittoreSessione_init()
    lblSottoTitolo.Caption = sessioneCorrente.nome & ", di tipo " & sessioneCorrente.tipoSessione & _
                            ", creata su " & sessioneCorrente.NomeMacchina & ", il " & sessioneCorrente.dataOraCreazione

End Sub

Private Sub EliminaSessioneCorrente()
    Call sessioneCorrente.Conferma(idSessioneConfigurazioneCorrente)
End Sub

Private Function EUnaTabellaDiRelazione() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    EUnaTabellaDiRelazione = True
    
    sql = "SELECT Cname FROM Col WHERE Tname = '" & UCase(tabellaCorrente) & "'"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
    rec.MoveFirst
        While Not (rec.EOF)
            If (rec("CName") = "ID" & UCase(tabellaCorrente)) Then
                EUnaTabellaDiRelazione = False
            End If
        rec.MoveNext
        Wend
    End If
    rec.Close

End Function

Private Function StringaChiaviPrimarie() As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaDaEscludere As String
    
    sql = " SELECT DISTINCT USER_CONS_COLUMNS.COLUMN_NAME COLONNA" & _
        " FROM USER_CONS_COLUMNS,USER_CONSTRAINTS" & _
        " WHERE USER_CONS_COLUMNS.CONSTRAINT_NAME= USER_CONSTRAINTS.CONSTRAINT_NAME" & _
        " AND USER_CONSTRAINTS.TABLE_NAME='" & UCase(tabellaCorrente) & "'" & _
        " AND USER_CONSTRAINTS.CONSTRAINT_TYPE = 'P'"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
    rec.MoveFirst
        While Not (rec.EOF)
            StringaChiaviPrimarie = StringaChiaviPrimarie & ", " & rec("COLONNA")
        rec.MoveNext
        Wend
    End If
    rec.Close
    StringaChiaviPrimarie = StringaFormattata(StringaChiaviPrimarie)

End Function

Private Function StringaNomeCampi() As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaDaEscludere As String
    
    sql = "SELECT Cname FROM Col WHERE Tname = '" & UCase(tabellaCorrente) & "'"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
    rec.MoveFirst
        While Not (rec.EOF)
            stringaDaEscludere = rec("CName")
            If ((stringaDaEscludere <> "IDSESSIONECONFIGURAZIONE") And (stringaDaEscludere <> "IDTIPOSTATORECORD")) Then
                StringaNomeCampi = StringaNomeCampi & ", " & rec("Cname")
            End If
        rec.MoveNext
        Wend
    End If
    rec.Close
    StringaNomeCampi = StringaFormattata(StringaNomeCampi)
End Function

Private Sub UpdateInSeta_Integra()
    Dim stringaSelect As String
    Dim stringaJoin As String
    Dim stringaSet As String
    Dim stringaFrom As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaDaEscludere As String
    Dim n As Long
    Dim i As Long
    Dim listaNomeCampi As New Collection
    Dim NomeCampo As Variant
    
    Set listaNomeCampi = New Collection
    
    If Not EUnaTabellaDiRelazione Then
        sql = "SELECT Cname FROM Col WHERE Tname = '" & UCase(tabellaCorrente) & "'"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
            While Not (rec.EOF)
                stringaDaEscludere = rec("CName")
                If ((stringaDaEscludere <> "IDSESSIONECONFIGURAZIONE") And (stringaDaEscludere <> "IDTIPOSTATORECORD")) Then
                    NomeCampo = rec("Cname")
                    listaNomeCampi.Add NomeCampo
                End If
            rec.MoveNext
            Wend
        End If
        rec.Close
    
        For i = 1 To listaNomeCampi.count
            stringaSelect = stringaSelect & _
                ", T1." & listaNomeCampi.Item(i) & _
                "  T1_nomeCampo" & i & ", " & _
                "  T2." & listaNomeCampi.Item(i) & "  " & _
                "  T2_nomeCampo" & i
            stringaSet = stringaSet & _
                ", T1_nomeCampo" & i & " = T2_nomeCampo" & i
        Next i
        
        stringaSelect = StringaFormattata(stringaSelect)
        stringaSet = StringaFormattata(stringaSet)
        stringaJoin = "T1.id" & tabellaCorrente & " = T2.id" & tabellaCorrente
        stringaFrom = nomeSchema & tabellaCorrente & " T1, " & tabellaCorrente & " T2"
        
        sql = "UPDATE " & _
            "( SELECT " & stringaSelect & _
            "  FROM   " & stringaFrom & _
            "  WHERE  " & stringaJoin & _
            "  AND    T2.IDTIPOSTATORECORD = " & TSR_MODIFICATO & _
            "  AND    T2.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & ")" & _
            "  SET    " & stringaSet
        SETAConnection.Execute sql, n, adCmdText
        
        ''Eliminazione flags in seta_cct
        sql = "UPDATE " & tabellaCorrente & _
            "  SET " & tabellaCorrente & ".IDTIPOSTATORECORD = NULL ," & _
            "  IDSESSIONECONFIGURAZIONE = NULL " & _
            "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_MODIFICATO & _
            "  AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
        SETAConnection.Execute sql, n, adCmdText
    End If
End Sub

Private Function StringaFormattata(stringa As String) As String
Dim lughezzaStringa As Integer
    lughezzaStringa = Len(stringa)
    If Left(stringa, 1) = "," Then
        StringaFormattata = Right(stringa, lughezzaStringa - 1)
    End If
End Function

Public Sub Init()
    Set sessioneCorrente = New clsSessione
    sessioneCorrente.CaricaDallaBaseDatiTramiteId (idSessioneConfigurazioneCorrente)
    tuttoOK = True
    Call DescrittoreSessione_init
    Call adcOggettiDaAggiornare_Init
    Call dgrOggettiDaAggiornare_init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub dgrOggettiDaAggiornare_init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrOggettiDaAggiornare
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Width = (dimensioneGrid / numeroCampi)
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub adcOggettiDaAggiornare_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    Dim sqlOrganizzazioni As String
    Dim sqlLayOutSupporto As String
    Dim sqlOperatore As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcOggettiDaAggiornare
'   NOTA: ma le query sullo stato del prodotto/organizzazione non dovrebbero
'   essere fatte sullo schema SETA_INTEGRA invece che su SETA_CCT?
    If tipoSessioneConfigurazione = TSC_PRODOTTO Then
        sql = "SELECT PRODOTTO ""Prodotto"", DESCRIZIONE ""Descrizione"", TIPO_STATO ""Tipo stato""" & _
            " FROM (SELECT PRODOTTO.NOME PRODOTTO, PRODOTTO.DESCRIZIONE DESCRIZIONE,  TIPOSTATOPRODOTTO.NOME TIPO_STATO" & _
            " FROM CC_LOG, PRODOTTO, TIPOSTATOPRODOTTO,CC_SESSIONECONFIGURAZIONE" & _
            " WHERE CC_LOG.IDRECORDLAVORATO = PRODOTTO.IDPRODOTTO" & _
            " AND PRODOTTO.IDTIPOSTATOPRODOTTO = TIPOSTATOPRODOTTO.IDTIPOSTATOPRODOTTO" & _
            " AND CC_LOG.IDSESSIONECONFIGURAZIONE = CC_SESSIONECONFIGURAZIONE.IDSESSIONECONFIGURAZIONE" & _
            " AND CC_LOG.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
            " AND IDDOMINIOATTIVITA = " & CCDA_PRODOTTO & " AND DATAORACONFERMA IS NULL" & _
            " UNION" & _
            " SELECT PRODOTTO.NOME PRODOTTO, PRODOTTO.DESCRIZIONE DESCRIZIONE,  TIPOSTATOPRODOTTO.NOME TIPO_STATO" & _
            " FROM CC_LOG, PRODOTTO, TIPOSTATOPRODOTTO,CC_SESSIONECONFIGURAZIONE" & _
            " WHERE CC_LOG.IDRECORDLAVORATO = PRODOTTO.IDSTAGIONE" & _
            " AND PRODOTTO.IDTIPOSTATOPRODOTTO = TIPOSTATOPRODOTTO.IDTIPOSTATOPRODOTTO" & _
            " AND CC_LOG.IDSESSIONECONFIGURAZIONE = CC_SESSIONECONFIGURAZIONE.IDSESSIONECONFIGURAZIONE" & _
            " AND CC_LOG.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
            " AND IDDOMINIOATTIVITA = " & CCDA_STAGIONE & " AND DATAORACONFERMA IS NULL) PRODOTTI" & _
            " ORDER BY TIPO_STATO "
        d.ConnectionString = StringaDiConnessione
        d.RecordSource = sql
        d.Refresh
    
    ElseIf tipoSessioneConfigurazione = TSC_ORGANIZZAZIONE Then
        
        sqlOrganizzazioni = "SELECT ORGANIZZAZIONE.NOME ORGANIZZAZIONE, ORGANIZZAZIONE.DESCRIZIONE DESCRIZIONE,  TIPOSTATOORGANIZZAZIONE.NOME TIPO_STATO" & _
             " FROM CC_LOG, ORGANIZZAZIONE, TIPOSTATOORGANIZZAZIONE,CC_SESSIONECONFIGURAZIONE " & _
             " WHERE CC_LOG.IDRECORDLAVORATO = ORGANIZZAZIONE.IDORGANIZZAZIONE" & _
             " AND CC_LOG.IDSESSIONECONFIGURAZIONE=CC_SESSIONECONFIGURAZIONE.IDSESSIONECONFIGURAZIONE" & _
             " AND ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE = TIPOSTATOORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE" & _
             " AND CC_LOG.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
             " AND IDDOMINIOATTIVITA = " & CCDA_ORGANIZZAZIONE & " AND DATAORACONFERMA IS NULL"
             
         sqlLayOutSupporto = "SELECT ORGANIZZAZIONE.NOME ORGANIZZAZIONE, ORGANIZZAZIONE.DESCRIZIONE DESCRIZIONE," & _
             " TIPOSTATOORGANIZZAZIONE.NOME TIPO_STATO " & _
             " FROM CC_LOG, ORGANIZZAZIONE, TIPOSTATOORGANIZZAZIONE, ORGANIZ_TIPOSUP_LAYOUTSUP,CC_SESSIONECONFIGURAZIONE " & _
             " WHERE ORGANIZZAZIONE.IDORGANIZZAZIONE = ORGANIZ_TIPOSUP_LAYOUTSUP.IDORGANIZZAZIONE" & _
             " AND CC_LOG.IDRECORDLAVORATO = ORGANIZ_TIPOSUP_LAYOUTSUP.IDLAYOUTSUPPORTO" & _
             " AND CC_LOG.IDSESSIONECONFIGURAZIONE=CC_SESSIONECONFIGURAZIONE.IDSESSIONECONFIGURAZIONE" & _
             " AND ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE = TIPOSTATOORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE  " & _
             " AND CC_LOG.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
             " AND IDDOMINIOATTIVITA =" & CCDA_LAYOUT_SUPPORTO & " AND DATAORACONFERMA IS NULL"

         sqlOperatore = "SELECT ORGANIZZAZIONE.NOME ORGANIZZAZIONE, ORGANIZZAZIONE.DESCRIZIONE DESCRIZIONE," & _
             " TIPOSTATOORGANIZZAZIONE.NOME TIPO_STATO " & _
             " FROM CC_LOG, ORGANIZZAZIONE, TIPOSTATOORGANIZZAZIONE, OPERATORE_ORGANIZZAZIONE,CC_SESSIONECONFIGURAZIONE " & _
             " WHERE ORGANIZZAZIONE.IDORGANIZZAZIONE = OPERATORE_ORGANIZZAZIONE.IDORGANIZZAZIONE" & _
             " AND CC_LOG.IDRECORDLAVORATO = OPERATORE_ORGANIZZAZIONE.IDOPERATORE" & _
             " AND CC_LOG.IDSESSIONECONFIGURAZIONE=CC_SESSIONECONFIGURAZIONE.IDSESSIONECONFIGURAZIONE" & _
             " AND ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE = TIPOSTATOORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE" & _
             " AND CC_LOG.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
             " AND IDDOMINIOATTIVITA =" & CCDA_OPERATORE & " AND DATAORACONFERMA IS NULL"
             
         sql = "SELECT ORGANIZZAZIONE ""Organizzazione"", DESCRIZIONE ""Descrizione"", TIPO_STATO ""Tipo stato""" & _
             " FROM (" & _
             sqlOrganizzazioni & _
             " UNION " & _
             sqlLayOutSupporto & _
             " UNION " & _
             sqlOperatore & ") ORGANIZZAZIONI" & _
             " ORDER BY TIPO_STATO "
                
        d.ConnectionString = StringaDiConnessione
        d.RecordSource = sql
        d.Refresh
        
        Set dgrOggettiDaAggiornare.dataSource = d
    
    End If
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub ListeTabelleDaAggiornare_Init()
    Dim sqlRecordLavorati As String
    Dim recordLavorati As New ADODB.Recordset
    Dim sottoDominioCorrente As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim tabellaDaAggiornare As clsSchemaAggiorn
    Const IDTIPOATTIVITA_CLONAZIONE  As Integer = 6

    Set ListaTabelleDaAggiornare_Delete = New Collection
    Set ListaTabelleDaAggiornare_Insert = New Collection

    ApriConnessioneBD
    
'    sqlRecordLavorati = "(SELECT DISTINCT  IDSOTTODOMINIOATTIVITA" & _
'        " FROM CC_LOG" & _
'        " WHERE IDSESSIONECONFIGURAZIONE=" & idSessioneConfigurazioneCorrente & ")"
    
    'NOTA1: questa query dovrebbe:
    '   a. prelevare tutti gli IDSOTTODOMINIOATTIVITA se IDTIPOATTIVITA = 6 (cio� clonazione);
    '   b. fare quello che faceva la query sopra in tutti gli altri casi
    'NOTA2: � stata provate con IDTIPOSESSIONECONFIGURAZIONE = 1 (cio� prodotto)
    sqlRecordLavorati = "(SELECT DISTINCT IDSOTTODOMINIOATTIVITA" & _
        " FROM CC_LOG" & _
        " WHERE IDTIPOATTIVITA <> " & IDTIPOATTIVITA_CLONAZIONE & _
        " AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
        " UNION" & _
        " SELECT DISTINCT C.IDSOTTODOMINIOATTIVITA IDSOTTODOMINIOATTIVITA" & _
        " FROM " & _
        " (" & _
        " SELECT IDSOTTODOMINIOATTIVITA FROM CC_SCHEMAAGGIORNAMENTO" & _
        " WHERE IDTIPOSESSIONECONFIGURAZIONE = " & tipoSessioneConfigurazione & _
        " ORDER BY ORDINEESECUZIONE" & _
        " ) C, CC_LOG L" & _
        " WHERE L.IDTIPOATTIVITA = " & IDTIPOATTIVITA_CLONAZIONE & _
        " AND L.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & ")"

    'CREAZIONE LISTA ORDINATA IN SENSO CRESCENTE PER INSERT
    sql = "SELECT NOMETABELLA, ORDINEESECUZIONE" & _
        " FROM CC_SCHEMAAGGIORNAMENTO" & _
        " WHERE IDSOTTODOMINIOATTIVITA IN " & sqlRecordLavorati & _
        " AND IDTIPOSESSIONECONFIGURAZIONE=" & tipoSessioneConfigurazione & _
        " ORDER BY ORDINEESECUZIONE ASC"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tabellaDaAggiornare = New clsSchemaAggiorn
            tabellaDaAggiornare.nomeTabella = rec("NOMETABELLA")
            tabellaDaAggiornare.ordineEsecuzione = rec("ORDINEESECUZIONE")
            tabellaDaAggiornare.idTipoDominioAttivita = sottoDominioCorrente
            ListaTabelleDaAggiornare_Insert.Add tabellaDaAggiornare
            rec.MoveNext
        Wend
    End If
    rec.Close
    'CREAZIONE LISTA ORDINATA IN SENSO DECRESCENTE PER DELETE
    sql = "SELECT NOMETABELLA, ORDINEESECUZIONE" & _
        " FROM CC_SCHEMAAGGIORNAMENTO" & _
        " WHERE IDSOTTODOMINIOATTIVITA IN " & sqlRecordLavorati & _
        " AND IDTIPOSESSIONECONFIGURAZIONE=" & tipoSessioneConfigurazione & _
        " ORDER BY ORDINEESECUZIONE DESC"

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tabellaDaAggiornare = New clsSchemaAggiorn
            tabellaDaAggiornare.nomeTabella = rec("NOMETABELLA")
            tabellaDaAggiornare.idTipoDominioAttivita = sottoDominioCorrente
            tabellaDaAggiornare.ordineEsecuzione = rec("ORDINEESECUZIONE")
            ListaTabelleDaAggiornare_Delete.Add tabellaDaAggiornare
            rec.MoveNext
        Wend
    End If
    rec.Close
   
    ChiudiConnessioneBD
    
End Sub

Private Function StatoProdottiAttivi() As Boolean
    Dim sql As String
    Dim d As Adodc
    Dim rec As New ADODB.Recordset

    sql = " SELECT COUNT (*) NumProdottiAttivi " & _
        "  FROM (SELECT   IDPRODOTTO, PRODOTTO.IDTIPOSTATOPRODOTTO ID" & _
        "  FROM CC_LOG, PRODOTTO, TIPOSTATOPRODOTTO   " & _
        "  WHERE CC_LOG.IDRECORDLAVORATO = PRODOTTO.IDPRODOTTO   " & _
        "  AND PRODOTTO.IDTIPOSTATOPRODOTTO=TIPOSTATOPRODOTTO.IDTIPOSTATOPRODOTTO  " & _
        "  AND CC_LOG.IDSESSIONECONFIGURAZIONE=" & idSessioneConfigurazioneCorrente & _
        "  AND IDDOMINIOATTIVITA =" & CCDA_PRODOTTO & _
        "  UNION    " & _
        "  SELECT   IDPRODOTTO, PRODOTTO.IDTIPOSTATOPRODOTTO ID" & _
        "  FROM CC_LOG, PRODOTTO, TIPOSTATOPRODOTTO   " & _
        "  WHERE CC_LOG.IDRECORDLAVORATO = PRODOTTO.IDSTAGIONE   " & _
        "  AND PRODOTTO.IDTIPOSTATOPRODOTTO=TIPOSTATOPRODOTTO.IDTIPOSTATOPRODOTTO  " & _
        "  AND CC_LOG.IDSESSIONECONFIGURAZIONE=" & idSessioneConfigurazioneCorrente & _
        "  AND IDDOMINIOATTIVITA = " & CCDA_STAGIONE & ") STATOPRODOTTOATTIVO" & _
        "  WHERE STATOPRODOTTOATTIVO.ID =" & TSP_ATTIVO

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        StatoProdottiAttivi = (rec("NumProdottiAttivi") > 0)
    Else
        StatoProdottiAttivi = False
    End If
    rec.Close

End Function

Private Function StatoOrganizzazioniAttive() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sqlOperatore As String
    Dim sqlLayOutSupporto As String
    Dim sqlOrganizzazione As String
    
    sqlOrganizzazione = "SELECT   ORGANIZZAZIONE.IDORGANIZZAZIONE, ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE ID" & _
        "  FROM CC_LOG, ORGANIZZAZIONE, TIPOSTATOORGANIZZAZIONE" & _
        "  WHERE CC_LOG.IDRECORDLAVORATO = ORGANIZZAZIONE.IDORGANIZZAZIONE" & _
        "  AND ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE = TIPOSTATOORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE" & _
        "  AND CC_LOG.IDSESSIONECONFIGURAZIONE=" & idSessioneConfigurazioneCorrente & _
        "  AND IDDOMINIOATTIVITA =" & CCDA_ORGANIZZAZIONE
           
     sqlLayOutSupporto = "  SELECT   ORGANIZZAZIONE.IDORGANIZZAZIONE, ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE ID" & _
        "  FROM CC_LOG, ORGANIZZAZIONE, TIPOSTATOORGANIZZAZIONE, ORGANIZ_TIPOSUP_LAYOUTSUP" & _
        "  WHERE ORGANIZZAZIONE.IDORGANIZZAZIONE=ORGANIZ_TIPOSUP_LAYOUTSUP.IDORGANIZZAZIONE" & _
        "  AND CC_LOG.IDRECORDLAVORATO = ORGANIZ_TIPOSUP_LAYOUTSUP.IDLAYOUTSUPPORTO" & _
        "  AND ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE = TIPOSTATOORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE  " & _
        "  AND CC_LOG.IDSESSIONECONFIGURAZIONE=" & idSessioneConfigurazioneCorrente & _
        "  AND IDDOMINIOATTIVITA =" & CCDA_LAYOUT_SUPPORTO
        
    sqlOperatore = "SELECT ORGANIZZAZIONE.IDORGANIZZAZIONE, ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE ID " & _
        "  FROM CC_LOG, ORGANIZZAZIONE, TIPOSTATOORGANIZZAZIONE, OPERATORE_ORGANIZZAZIONE" & _
        "  WHERE ORGANIZZAZIONE.IDORGANIZZAZIONE = OPERATORE_ORGANIZZAZIONE.IDORGANIZZAZIONE" & _
        "  AND CC_LOG.IDRECORDLAVORATO = OPERATORE_ORGANIZZAZIONE.IDOPERATORE" & _
        "  AND ORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE = TIPOSTATOORGANIZZAZIONE.IDTIPOSTATOORGANIZZAZIONE" & _
        "  AND CC_LOG.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
        "  AND IDDOMINIOATTIVITA =" & CCDA_OPERATORE
    
    sql = " SELECT COUNT (*) NumOrganizzazioniAttive " & _
        " FROM ( " & _
        sqlOrganizzazione & _
        " UNION " & _
        sqlLayOutSupporto & _
        " UNION " & _
        sqlOperatore & _
        " ) STATOORGANIZZAZIONEATTIVA" & _
        "  WHERE STATOORGANIZZAZIONEATTIVA.ID =" & TSO_ATTIVA
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    
    If Not (rec.BOF And rec.EOF) Then
        StatoOrganizzazioniAttive = (rec("NumOrganizzazioniAttive") > 0)
    Else
        StatoOrganizzazioniAttive = False
    End If
    
    rec.Close

End Function

Private Sub TrasferisciDatiInSeta_Integra()
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim tabellaDaAggiornare As New clsSchemaAggiorn
 
    ApriConnessioneBD
    SETAConnection.BeginTrans
On Error GoTo gestioneErrori
    
    '''****INSERT******
    For Each tabellaDaAggiornare In ListaTabelleDaAggiornare_Insert
        tabellaCorrente = tabellaDaAggiornare.nomeTabella
        If tabellaCorrente = "PREZZOTITOLOPRODOTTO" Then
            Call CercaPrezziNonAggiornabili
            If listaPrezziDaNonAggiornare.count > 0 Then
                Call AggiornaPrezziSuSchemaSeta_CCT
            End If
        End If
        Call InsertInSeta_Integra
        Call UpdateInSeta_Integra
    Next tabellaDaAggiornare
    
    '''****DELETE******
    For Each tabellaDaAggiornare In ListaTabelleDaAggiornare_Delete
        tabellaCorrente = tabellaDaAggiornare.nomeTabella
        Call DeleteInSeta_Integra
    Next tabellaDaAggiornare

    SETAConnection.CommitTrans
    ChiudiConnessioneBD
    
    Exit Sub
gestioneErrori:
    frmMessaggio.Visualizza "ErroreGenericoDB", Err.Description
    SETAConnection.RollbackTrans
    tuttoOK = False
End Sub

Private Sub InsertInSeta_Integra()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    
    If EUnaTabellaDiRelazione Then
        Call DeleteTabellaDiRelazione
    Else
        sql = "INSERT INTO " & nomeSchema & tabellaCorrente & " (" & StringaNomeCampi & ")" & _
            " SELECT " & StringaNomeCampi & _
            " FROM " & tabellaCorrente & _
            " WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
            " AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
        SETAConnection.Execute sql, n, adCmdText
        
        ''Eliminazione flags in seta_cct
        sql = "UPDATE " & tabellaCorrente & _
            "  SET " & tabellaCorrente & ".IDTIPOSTATORECORD = NULL ," & _
            "  IDSESSIONECONFIGURAZIONE = NULL " & _
            "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
            "  AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
        SETAConnection.Execute sql, n, adCmdText
    End If
    
End Sub

Private Sub DeleteTabellaDiRelazione_OLD()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    
'   VECCHIA VERSIONE; DA' PROBLEMI
    sql = "DELETE FROM " & nomeSchema & tabellaCorrente & _
        "  WHERE (" & StringaChiaviPrimarie & ") IN " & _
        " (SELECT " & StringaChiaviPrimarie & _
        "  FROM  " & tabellaCorrente & _
        "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
        "  AND   " & tabellaCorrente & ".IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & " )"
    
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "INSERT INTO " & nomeSchema & tabellaCorrente & " (" & StringaNomeCampi & ")" & _
        " SELECT " & StringaNomeCampi & _
        " FROM " & tabellaCorrente & _
        " WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
        " AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
        
    SETAConnection.Execute sql, n, adCmdText
    
    ''Eliminazione flags in seta_cct
    sql = "UPDATE " & tabellaCorrente & _
        "  SET " & tabellaCorrente & ".IDTIPOSTATORECORD = NULL ," & _
        "  IDSESSIONECONFIGURAZIONE = NULL " & _
        "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
        "  AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
    
    SETAConnection.Execute sql, n, adCmdText
    
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub DeleteTabellaDiRelazione()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    
    'eliminazione tutti i records in seta_integra
    sql = "DELETE FROM " & nomeSchema & tabellaCorrente & _
        "  WHERE (" & StringaChiaviPrimarie & ") IN " & _
        " (SELECT " & StringaChiaviPrimarie & _
        "  FROM  " & tabellaCorrente & _
        "  WHERE " & tabellaCorrente & ".IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & " )"
    SETAConnection.Execute sql, n, adCmdText
    
    'inserimento records marcati N in seta_integra
    sql = "INSERT INTO " & nomeSchema & tabellaCorrente & " (" & StringaNomeCampi & ")" & _
        " SELECT " & StringaNomeCampi & _
        " FROM " & tabellaCorrente & _
        " WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
        " AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
    SETAConnection.Execute sql, n, adCmdText
    
    'eliminazione records marcati E in seta_cct
    sql = "DELETE FROM " & tabellaCorrente & _
        "  WHERE (" & StringaChiaviPrimarie & ") IN " & _
        " (SELECT " & StringaChiaviPrimarie & _
        "  FROM  " & tabellaCorrente & _
        "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_ELIMINATO & _
        " AND " & tabellaCorrente & ".IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & " )"
    SETAConnection.Execute sql, n, adCmdText
    
    ''Eliminazione flags marcati N in seta_cct
    sql = "UPDATE " & tabellaCorrente & _
        "  SET " & tabellaCorrente & ".IDTIPOSTATORECORD = NULL ," & _
        "  IDSESSIONECONFIGURAZIONE = NULL " & _
        "  WHERE " & tabellaCorrente & ".IDTIPOSTATORECORD = " & TSR_NUOVO & _
        "  AND IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
    SETAConnection.Execute sql, n, adCmdText
    
End Sub

Private Sub CercaPrezziNonAggiornabili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim idTipoTariffaSIAE_old As Long
    Dim idTipoTariffaSIAE_new As Long
    Dim esistonoTitoliEmessi As Boolean
'    Dim listaPrezziDaNonAggiornare As Collection
    Dim idPrezzoTitoloProdotto As Long
    Dim idOrdineDiPostoSIAE As Long
    Dim idPeriodoCommerciale As Long
    Dim idPTP As Long
    Dim prezzo As clsElementoLista
    
'MODIFICO PTP SU SETA_CCT ANDANDO A VEDERE I TITOLI EMESSI SU SETA_INTEGRA;
'QUANDO HO FATTO TUTTE LE CONSIDERAZIONI E HO MESSO A POSTO DECENTEMENTE
'PTP SU SETA_CCT, FACCIO LAVORARE NORMALMENMTE I METODI DI TrasferisciDatiInSeta_Integra
'NOTA: LA QUERY INIZIALE E' DIVERSA DALLE ALTRE USATE PER CREARE LA LISTA; UNIFICARE
    
    Set listaPrezziDaNonAggiornare = New Collection
    
'    sql = "SELECT PTP.IDPREZZOTITOLOPRODOTTO ID, "
'    sql = sql & " PTP.IDPERIODOCOMMERCIALE, AREATAR.IDORDINEDIPOSTOSIAE IDORD, "
'    sql = sql & " PTP.IMPORTOBASE IMPO, PTP.IMPORTOSERVIZIOPREVENDITA PREV, "
'    sql = sql & " PTP.IDTIPOTARIFFASIAE IDTIPOTARSIAE, INTPTP.IDTIPOTARIFFASIAE PREC,"
'    sql = sql & " AREATAR.IDAREA IDA, AREATAR.IDTARIFFA IDTAR,"
'    sql = sql & " PTP.IDSESSIONECONFIGURAZIONE, PTP.IDTIPOSTATORECORD,"
'    sql = sql & " COUNT(ST.IDSTATOTITOLO) TITOLI"
'    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, STATOTITOLO ST, "
'    sql = sql & " ("
'    sql = sql & " SELECT DISTINCT A.IDAREA IDAREA, IDTIPOAREA, IDTARIFFA, IDTIPOTARIFFA, "
'    sql = sql & " PAO.IDORDINEDIPOSTOSIAE IDORDINEDIPOSTOSIAE, PAO.IDPIANTASIAE IDPIANTASIAE"
'    sql = sql & " FROM AREA A, TARIFFA T, PIANTASIAE_AREA_ORDINEDIPOSTO PAO, PRODOTTO P"
'    sql = sql & " WHERE PAO.IDAREA(+) = A.IDAREA "
'    sql = sql & " AND P.IDPIANTASIAE = PAO.IDPIANTASIAE"
'    sql = sql & " AND T.IDPRODOTTO = " & idProdotto
'    sql = sql & " AND A.IDPIANTA = P.IDPIANTA"
'    sql = sql & " AND A.IDTIPOAREA IN (4,5,6)"
'    sql = sql & " ) AREATAR,"
'    sql = sql & " SETA_INTEGRA.PREZZOTITOLOPRODOTTO INTPTP"
'    sql = sql & " WHERE PTP.IDAREA = AREATAR.IDAREA"
'    sql = sql & " AND PTP.IDTARIFFA(+) = AREATAR.IDTARIFFA"
'    sql = sql & " AND ST.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO"
'    sql = sql & " AND ST.IDTIPOSTATOTITOLO(+) <> 1 -- da rivedere "
'    sql = sql & " AND INTPTP.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO"
'    sql = sql & " AND PTP.IDTIPOSTATORECORD IS NOT NULL"
'    sql = sql & " GROUP BY PTP.IDPREZZOTITOLOPRODOTTO, "
'    sql = sql & " PTP.IDPERIODOCOMMERCIALE, AREATAR.IDORDINEDIPOSTOSIAE, "
'    sql = sql & " PTP.IMPORTOBASE, PTP.IMPORTOSERVIZIOPREVENDITA, "
'    sql = sql & " PTP.IDTIPOTARIFFASIAE, INTPTP.IDTIPOTARIFFASIAE,"
'    sql = sql & " AREATAR.IDAREA, AREATAR.IDTARIFFA,"
'    sql = sql & " PTP.IDSESSIONECONFIGURAZIONE, PTP.IDTIPOSTATORECORD"
'    sql = sql & " ORDER BY PTP.IDPERIODOCOMMERCIALE, IDORD, IMPO DESC"

    sql = "SELECT PTP.IDPREZZOTITOLOPRODOTTO ID, P.IDPRODOTTO, PTP.IDPERIODOCOMMERCIALE IDPER, PAO.IDORDINEDIPOSTOSIAE IDORD, "
    sql = sql & " PTP.IMPORTOBASE IMPO, PTP.IMPORTOSERVIZIOPREVENDITA PREV, A.CODICE SCOD, A.NOME SNOME, "
    sql = sql & " TAR.CODICE TCOD, TAR.NOME TNOME, PTP.IDTIPOTARIFFASIAE IDTTS_NEW, INTPTP.IDTIPOTARIFFASIAE IDTTS_OLD,COUNT(T.IDTITOLO) CONT  "
    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, TITOLO T, STATOTITOLO ST,  SETA_INTEGRA.PREZZOTITOLOPRODOTTO INTPTP, "
    sql = sql & " AREA A, TARIFFA TAR, PIANTASIAE_AREA_ORDINEDIPOSTO PAO, PRODOTTO P "
    sql = sql & " WHERE PTP.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
    sql = sql & " AND T.IDSTATOTITOLOCORRENTE(+) = ST.IDSTATOTITOLO "
    sql = sql & " AND ST.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO  "
    sql = sql & " AND INTPTP.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO  "
    sql = sql & " AND A.IDAREA = PTP.IDAREA "
    sql = sql & " AND PTP.IDTARIFFA = TAR.IDTARIFFA "
    sql = sql & " AND PTP.IDAREA = PAO.IDAREA(+)"
    sql = sql & " AND PTP.IDPRODOTTO = P.IDPRODOTTO "
    sql = sql & " AND P.IDPIANTASIAE = PAO.IDPIANTASIAE"
    sql = sql & " GROUP BY PTP.IDPREZZOTITOLOPRODOTTO, P.IDPRODOTTO, PTP.IDPERIODOCOMMERCIALE,  "
    sql = sql & " PAO.IDORDINEDIPOSTOSIAE, PTP.IMPORTOBASE, PTP.IMPORTOSERVIZIOPREVENDITA, "
    sql = sql & " A.CODICE, A.NOME, TAR.CODICE, TAR.NOME, PTP.IDTIPOTARIFFASIAE, INTPTP.IDTIPOTARIFFASIAE "
    sql = sql & " UNION"
    sql = sql & " SELECT PTP.IDPREZZOTITOLOPRODOTTO ID, P.IDPRODOTTO, PTP.IDPERIODOCOMMERCIALE IDPER,  NULL IDORD,"
    sql = sql & " PTP.IMPORTOBASE IMPO, PTP.IMPORTOSERVIZIOPREVENDITA PREV, A.CODICE SCOD, A.NOME SNOME, "
    sql = sql & " TAR.CODICE TCOD, TAR.NOME TNOME, 17 IDTTS_NEW, INTPTP.IDTIPOTARIFFASIAE IDTTS_OLD,COUNT(T.IDTITOLO) CONT  "
    sql = sql & " FROM PREZZOTITOLOPRODOTTO PTP, TITOLO T, STATOTITOLO ST,  SETA_INTEGRA.PREZZOTITOLOPRODOTTO INTPTP, "
    sql = sql & " AREA A, TARIFFA TAR, PRODOTTO P "
    sql = sql & " WHERE PTP.IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
    sql = sql & " AND PTP.IDTIPOTARIFFASIAE = 17"
    sql = sql & " AND T.IDSTATOTITOLOCORRENTE(+) = ST.IDSTATOTITOLO "
    sql = sql & " AND ST.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO  "
    sql = sql & " AND INTPTP.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO  "
    sql = sql & " AND A.IDAREA = PTP.IDAREA "
    sql = sql & " AND PTP.IDTARIFFA = TAR.IDTARIFFA "
    sql = sql & " AND PTP.IDPRODOTTO = P.IDPRODOTTO "
    sql = sql & " GROUP BY PTP.IDPREZZOTITOLOPRODOTTO, P.IDPRODOTTO, PTP.IDPERIODOCOMMERCIALE,  "
    sql = sql & " PTP.IMPORTOBASE, PTP.IMPORTOSERVIZIOPREVENDITA, "
    sql = sql & " A.CODICE, A.NOME, TAR.CODICE, TAR.NOME, INTPTP.IDTIPOTARIFFASIAE "
    sql = sql & " ORDER BY IDPER, IDORD, IMPO DESC"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prezzo = New clsElementoLista
            prezzo.idElementoLista = rec("ID").Value
            prezzo.descrizioneElementoLista = "superarea: " & CStr(rec("SCOD").Value) & " - " & CStr(rec("SNOME").Value) & _
             "; tariffa: " & CStr(rec("TCOD").Value) & " - " & CStr(rec("TNOME").Value)
            'se il ptp � gi� nella listaPrezziDaNonAggiornare, vai direttamente al rec.MoveNext
            idOrdineDiPostoSIAE = IIf(IsNull(rec("IDORD")), idNessunElementoSelezionato, rec("IDORD").Value)
            idPeriodoCommerciale = rec("IDPER").Value
            idTipoTariffaSIAE_old = IIf(IsNull(rec("IDTTS_OLD")), idNessunElementoSelezionato, rec("IDTTS_OLD").Value)
            idTipoTariffaSIAE_new = IIf(IsNull(rec("IDTTS_NEW")), idNessunElementoSelezionato, rec("IDTTS_NEW").Value)
            esistonoTitoliEmessi = (rec("CONT").Value > 0)
            If Not esistonoTitoliEmessi Then
                If idTipoTariffaSIAE_old <> idNessunElementoSelezionato Then
                    If idTipoTariffaSIAE_old <> idTipoTariffaSIAE_new Then
                        'il tipo tariffa siae � cambiato
                        'ricerca dei record con tipo tariffa siae modificato in seguito a questo
                        'sono quelli che sullo schema SETA_INTEGRA
                        'hanno idTipoTariffaSIAE = idTipoTariffaSIAE_new
                        'sullo stesso ordine di posto(?)
                        Call CercaPrezziModificati(idPeriodoCommerciale, idTipoTariffaSIAE_new, idOrdineDiPostoSIAE)
                    Else
                        'il tipo tariffa siae NON � cambiato; su CCT non deve cambiare nulla
                        'Do Nothing
                    End If
                Else 'il prezzo � nuovo
                    'ricerca dei record con tipo tariffa siae modificato in seguito a questo
                    'sono quelli che sullo schema SETA_INTEGRA
                    'hanno idTipoTariffaSIAE = idTipoTariffaSIAE_new
                    'sullo stesso ordine di posto(?)
                    Call CercaPrezziModificati(idPeriodoCommerciale, idTipoTariffaSIAE_new, idOrdineDiPostoSIAE)
                End If
            Else
                If idTipoTariffaSIAE_new = IDTIPOTARIFFASIAE_INTERO Then
                    Call CercaPrezziModificati(idPeriodoCommerciale, idTipoTariffaSIAE_new, idOrdineDiPostoSIAE)
                Else
                    'Do Nothing
                End If
                Call listaPrezziDaNonAggiornare.Add(prezzo, ChiaveId(prezzo.idElementoLista))
                'ricerca dei record con tipo tariffa siae modificato in seguito a questo
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
End Sub

Private Sub CercaPrezziModificati(idPeriodoCommerciale As Long, idTipoTariffaSIAE As Long, idOrdideDiPostoSIAE As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim prezzo As clsElementoLista
    
    sql = "SELECT PTP.IDPREZZOTITOLOPRODOTTO ID,"
    sql = sql & " PTP.IMPORTOBASE IMPO, PTP.IMPORTOSERVIZIOPREVENDITA PREV,"
    sql = sql & " A.CODICE SCOD, A.NOME SNOME, TAR.CODICE TCOD, TAR.NOME TNOME,"
    sql = sql & " COUNT(T.IDTITOLO) CONT "
    sql = sql & " FROM SETA_INTEGRA.PREZZOTITOLOPRODOTTO PTP, TITOLO T, STATOTITOLO ST, "
    sql = sql & " AREA A, TARIFFA TAR, PIANTASIAE_AREA_ORDINEDIPOSTO PAO, PRODOTTO P"
    sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE(+) = ST.IDSTATOTITOLO"
    sql = sql & " AND ST.IDPREZZOTITOLOPRODOTTO(+) = PTP.IDPREZZOTITOLOPRODOTTO "
    sql = sql & " AND A.IDAREA = PTP.IDAREA"
    sql = sql & " AND PTP.IDTARIFFA = TAR.IDTARIFFA"
    sql = sql & " AND PTP.IDAREA = PAO.IDAREA(+)"
    sql = sql & " AND PTP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDPIANTASIAE = PAO.IDPIANTASIAE"
    sql = sql & " AND PTP.IDPERIODOCOMMERCIALE = " & idPeriodoCommerciale
    sql = sql & " AND PTP.IDTIPOTARIFFASIAE = " & idTipoTariffaSIAE
    If idOrdideDiPostoSIAE = idNessunElementoSelezionato Then
        sql = sql & " AND PAO.IDORDINEDIPOSTOSIAE IS NULL"
    Else
        sql = sql & " AND PAO.IDORDINEDIPOSTOSIAE = " & idOrdideDiPostoSIAE
    End If
    sql = sql & " GROUP BY PTP.IDPREZZOTITOLOPRODOTTO,"
    sql = sql & " PTP.IMPORTOBASE, PTP.IMPORTOSERVIZIOPREVENDITA,"
    sql = sql & " A.CODICE, A.NOME, TAR.CODICE, TAR.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("CONT").Value > 0 Then
                Set prezzo = New clsElementoLista
                prezzo.idElementoLista = rec("ID").Value
                prezzo.descrizioneElementoLista = "superarea: " & CStr(rec("SCOD").Value) & " - " & CStr(rec("SNOME").Value) & _
                 "; tariffa: " & CStr(rec("TCOD").Value) & " - " & CStr(rec("TNOME").Value)
                Call listaPrezziDaNonAggiornare.Add(prezzo, ChiaveId(prezzo.idElementoLista))
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub AggiornaPrezziSuSchemaSeta_CCT()
    Dim sql As String
    Dim n As Long
    
    sql = "UPDATE PREZZOTITOLOPRODOTTO SET IDTIPOSTATORECORD = NULL," & _
        " IDSESSIONECONFIGURAZIONE = NULL" & _
        " WHERE IDPREZZOTITOLOPRODOTTO IN " & ElencoPrezziNonAggiornabili
    SETAConnection.Execute sql, n, adCmdText
    
End Sub

Private Function ElencoPrezziNonAggiornabili() As String
    Dim i As Integer
    Dim elencoParziale As String
    
    elencoParziale = "(" & listaPrezziDaNonAggiornare.Item(1)
    For i = 1 To listaPrezziDaNonAggiornare.count
        elencoParziale = elencoParziale & ", " & listaPrezziDaNonAggiornare.Item(i)
    Next i
    elencoParziale = elencoParziale & ")"
    ElencoPrezziNonAggiornabili = elencoParziale
End Function
