Tipi di messaggio
	ERRORE
	AVVERTIMENTO
	NOTIFICA
	RICHIESTACONFERMA
	RICHIESTARIPETIZIONE




[MESSAGGIO] ConfermaAttivazioneProdottoScaduto RICHIESTACONFERMA
PRODOTTO SCADUTO.

Il prodotto � nello stato scaduto (� terminata la prima rappresentazione); 
si vuole comunque rendere attivabile il prodotto?





[MESSAGGIO] ConfermaEliminazioneUtilizzatori RICHIESTACONFERMA
RAPPRESENTAZIONI SCADUTE.

Le seguenti rappresentazioni hanno utilizzatori associati e sono scadute (sono passati 7 giorni dalla loro fine): 

Spettacolo - Inizio - Durata (min.) - Scadenza
�1


si desidera eliminare gli utenti associati a tali rappresentazioni?






[MESSAGGIO] ConfermaInserimentoInformazioniSistemistiche RICHIESTACONFERMA
INSERIMENTO INFORMAZIONI SISTEMISTICHE

Per poter essere utilizzabile l'Organizzazione necessita di alcune informazioni sistemistiche; 
si desidera inserire in base dati tali informazioni sistemistiche standard?






[MESSAGGIO] VisualizzaStatoProtezioni RICHIESTACONFERMA
REPORT DEI DATI DI CONFIGURAZIONE DEL PRODOTTO

Verranno visualizzati i dati di configurazione del prodotto selezionato, tra i quali lo stato delle protezioni su posto.
Tale informazione potrebbe appesantire la produzione del report e potrebbe essere necessario attendere alcuni minuti.
 
Premere [Conferma] per generare il report completo dello stato sulle protezioni, [Annulla] per generare il report senza tale informazione.





[MESSAGGIO] ConfermaSessioneVuota RICHIESTACONFERMA
SESSIONE VUOTA.

Nella sessione di configurazione che si intende confermare non � stata apportata alcuna modifica alla base dati.
Si desidera confermare comunque la sessione?




[MESSAGGIO] ConfermaValoreAliquotaIVAImpostato RICHIESTACONFERMA
ALIQUOTA IVA.

Il campo Aliquota IVA va valorizzato solo nei casi in cui non sar� utilizzata la procedura standard per il calcolo dell'IVA;
si vuole confermare il valore impostato?




[MESSAGGIO] ConfermaEliminazionePrezzi RICHIESTACONFERMA
ELIMINAZIONE PREZZI.

Tutti i prezzi del prodotto e del periodo commerciale corrente saranno eliminati dal DB

Premere [Conferma] per procedere all'eliminazione, altrimenti [Annulla].




[MESSAGGIO] ConfermaRimozioneProtezioni RICHIESTACONFERMA
RIMOZIONE PROTEZIONI.

Si � sicuri di voler eliminare le protezioni presenti sul prodotto corrente?




[MESSAGGIO] ConfermaEditabilit�ProdottoAttivo RICHIESTACONFERMA

Il prodotto � nello stato 'Attivo'; la modifica dei dati configurati potrebbe generare problemi a livello di sistema.

Premere [Conferma] per modificare comunque i dati, [Annulla] per lasciare i valori attuali.




[MESSAGGIO] ConfermaEditabilit�OrganizzazioneAttiva RICHIESTACONFERMA

L'organizzazione � nello stato 'Attivo'; la modifica dei dati configurati potrebbe generare problemi a livello di sistema.

Premere [Conferma] per modificare comunque i dati, [Annulla] per lasciare i valori attuali.




[MESSAGGIO] ConfermaRateoNonCorretto RICHIESTACONFERMA
VALORE RATEO NON CORRETTO.

Il rateo inserito non corrisponde al numero delle rappresentazioni/scelte rappresentazioni configurate per il prodotto ( �1 rappr./scelta rappr.). 

Premere [Conferma] per accettare il rateo inserito, [Annulla] per tornare alla configurazione.




[MESSAGGIO] ConfermaCapienzaNonCorretta RICHIESTACONFERMA
VALORE CAPIENZA NON CORRETTO.

La capienza inserita non corrisponde al numero dei posti presenti nelle aree della superarea ( �1 posti ). 

Premere [Conferma] per accettare la capienza inserita, [Annulla] per tornare alla configurazione.




[MESSAGGIO] ConfermaModificaLayoutProdotti RICHIESTACONFERMA

Tutti i prodotti selezionati verranno associati al layout impostato.

Premere [Conferma] per procedere con la modifica, altrimenti [Annulla].




[MESSAGGIO] AvvertimentoRateoNonCorretto AVVERTIMENTO
VALORE RATEO NON CORRETTO.

Il numero di rappresentazioni inserite non corrisponde al rateo presente in base dati.




[MESSAGGIO] AvvertimentoVariazioneDateScadenzaRiservazioni AVVERTIMENTO
ATTENZIONE.

Le date di scadenza delle riservazioni sono state variate: affinch� tale variazione abbia effetto sulle riservazioni sar� necessario riavviare il servente organizzazione.




[MESSAGGIO] AvvertimentoOperativit�OperatoriKO AVVERTIMENTO
AVVERTIMENTO.

I seguenti operatori selezionati non possiedono una piena operativit� su tutti i prodotti dell'offerta pacchetto:
(NOTA: per 'operazioni necessarie del prodotto' si intendono le operazioni di Vendita, Annullamento Vendita e Gestione Offerta Pacchetto)
�1
Sar� pertanto necessario modificare i prodotti indicati per poter operare sull'offerta pacchetto corrente.




[MESSAGGIO] AvvertimentoSpettacoloNonCompleto AVVERTIMENTO
SPETTACOLO NON COMPLETO.

Lo spettacolo corrente non � completo per i seguenti motivi:
�1





[MESSAGGIO] ErroreDuplicazioneDatiDB ERRORE
VIOLAZIONE DELLA RESTRIZIONE DI UNICITA'.

I seguenti valori sono gi� presenti in Base Dati:
�1




[MESSAGGIO] ErroreDuplicazioneDatiDBConstraint ERRORE
VIOLAZIONE DELLA RESTRIZIONE DI UNICITA'.

I seguenti valori sono gi� presenti in Base Dati per �2:
�1




[MESSAGGIO] ErroreDuplicazioneCodiceConstraint ERRORE
VIOLAZIONE DELLA RESTRIZIONE DI UNICITA'.

Il codice Pianta associato all'organizzazione deve essere univoco;
il valore �1 risulta essere gi� presente tra le piante associate 
all'Organizzazione �2




[MESSAGGIO] ErroreScritturaLog ERRORE
ERRORE NELLA SCRITTURA DEL FILE DI LOG.

Non � stato possibile aggiornare il file di Log con l'ultima operazione eseguita.





[MESSAGGIO] ErroreGenericoDB ERRORE
ERRORE DI ACCESSO AL DB.

Il tentativo di aggiornare la Base Dati ha causato il seguente errore:

�1

Contattare l'Assistenza.




[MESSAGGIO] ErroreLogin ERRORE
ERRORE LOGIN.

Il tentativo di connessione al DB � fallito:
�1

Contattare l'Assistenza.




[MESSAGGIO] ErroreInconsistenzaDB ERRORE
ERRORE INCONSISTENZA DB.

E' stata riscontrata un'inconsistenza nei dati presenti in Base Dati.
Contattare l'Assistenza.





[MESSAGGIO] ErroreDuplicazionePosto ERRORE
ERRORE DUPLICAZIONE POSTO.

I seguenti posti non sono stati disegnati in quanto gi� presenti per l'area �1:

�2





[MESSAGGIO] ErroreSovrapposizionePosto ERRORE
ERRORE DUPLICAZIONE POSTO.

I seguenti posti non sono stati disegnati in quanto si sovrappongono, totalmente o parzialmente, ad altri gi� presenti sull'area �1:

�2





[MESSAGGIO] ErroreFormatoPercentuale ERRORE
ERRORE NEL FORMATO DEI DATI

Il campo �1 deve essere numerico e compreso tra 0 e 100





[MESSAGGIO] ErroreFormatoDatiIntero ERRORE
ERRORE NEL FORMATO DEI DATI

Il campo �1 deve essere numerico di tipo intero �2





[MESSAGGIO] ErroreEliminazioneModalit�Emissione ERRORE
ERRORE NEL FORMATO DEI DATI

La modalit� di emissione selezionata non pu� essere eliminata in quanto in uso da parte di alcune tariffe del prodotto





[MESSAGGIO] ErroreSimboloSuPianta ERRORE
ERRORE NEL FORMATO DEL SIMBOLO SU PIANTA

Il simbolo su pianta pu� essere:
- una lettera dell'alfabeto;
- un numero compreso tra 1 e 9.



[MESSAGGIO] ErroreCodicetariffa ERRORE
ERRORE NEL FORMATO DEL CODICE TARIFFA

Il codice tariffa non deve essere numerico




[MESSAGGIO] ErroreFormatoDatiIndiceDiPreferibilita ERRORE
ERRORE NEL FORMATO DELL'INDICE DI PREFERIBILITA'

L'indice di preferibilit� deve essere numerico di tipo intero e maggiore di zero




[MESSAGGIO] ErroreFormatoDatiCapienza ERRORE
ERRORE NEL FORMATO DELLA CAPIENZA

La capienza deve essere maggiore di zero



[MESSAGGIO] ErroreFormatoDatiOrdine ERRORE
ERRORE NEL FORMATO DELL'ORDINE DELLA SEQUENZA IN FASCIA

L'ordine della sequenza in fascia deve essere numerico di tipo intero e maggiore di zero




[MESSAGGIO] ErroreFormatoDatiCodiceSIAE ERRORE
ERRORE NEL FORMATO DEL CODICE SIAE DEL VENUE

Il codice SIAE del Venue deve essere una stringa di lunghezza non superiore a 13 caratteri composta di numeri (eventualmente preceduti da zeri)



[MESSAGGIO] ConfermaUscitaConFascieIncomplete RICHIESTACONFERMA
TROVATE FASCE INCOMPLETE

�1 tra le fasce configurate hanno ancora dei posti da associare a sequenze; tale stato non verr� salvato nella base dati, ma verranno memorizzate soltanto le sequenze definite e le fasce con almeno una sequenza.

Premere [Conferma] per uscire in ogni caso, altrimenti premere [Annulla].





[MESSAGGIO] ConfermaAbbandonoPercorsoGuidato RICHIESTACONFERMA
ABBANDONO PERCORSO GUIDATO

Si sta abbandonando il percorso guidato; tutte le caratteristiche configurate andranno perse.

Premere [Conferma] per abbandonare in ogni caso, altrimenti premere [Annulla].





[MESSAGGIO] ConfermaSalvataggioAssociazioniArea RICHIESTACONFERMA
SALVATAGGIO ASSOCIAZIONI CONFIGURATE PER L'AREA

Deselezionando l'area �1, le informazioni eventualmente configurate andranno scaricate dall memoria cache.
Si intendono salvare le informazioni relative all'area nella base dati?

Premere [Conferma] per salvare in base dati, premere [Annulla] per abbandonare le modifiche fatte all'area.






[MESSAGGIO] ConfermaEliminazioneAssociazioni RICHIESTACONFERMA
ELIMINAZIONE ASSOCIAZIONI IN BASE DATI

Saranno eliminate dalla base dati TUTTE le associazioni presenti per l'area �1.

Premere [Conferma] per proseguire, altrimenti premere [Annulla].






[MESSAGGIO] ConfermaProseguimentoConfigurazioneProdotto RICHIESTACONFERMA
IMPORTAZIONE FALLITA

L'importazione di tariffe / periodi commerciali / prezzi dal prodotto �1 � fallita; � tuttavia possibile proseguire nella configurazione del nuovo prodotto

Premere [Conferma] per proseguire la configurazione del nuovo prodotto, 
altrimenti premere [Annulla] per interrompere il percorso guidato.





[MESSAGGIO] ErroreFormatoDatiLong ERRORE
ERRORE NEL FORMATO DEI DATI

Il campo �1 deve essere numerico di tipo intero lungo
(range: Da -2.147.483.648 a 2.147.483.647)




[MESSAGGIO] ErroreRangeCampoIncidenza ERRORE
ERRORE NEL FORMATO/RANGE DEL CAMPO INCIDENZA

Il campo Incidenza deve essere numerico di tipo intero e compreso tra 0 e 100




[MESSAGGIO] ErroreFormatoDatiDouble ERRORE
ERRORE NEL FORMATO DEI DATI

Il campo �1 deve essere numerico con due cifre dopo la virgola



[MESSAGGIO] ErroreSuccessioneDate ERRORE
ERRORE NELLA SUCCESSIONE DELLE DATE

La data �1 deve essere precedente alla data �2




[MESSAGGIO] ErroreIntervalloDate ERRORE
ERRORE NELL'INTERVALLO DELLE DATE

La data �1 deve essere compresa tra la data �2 e la data �3, ed eventualmente coincidere con una di esse




[MESSAGGIO] ErroreInversioneDate ERRORE
ERRORE NELL'INTERVALLO DELLE DATE

La data ora �1 deve essere minore della data ora �2.




[MESSAGGIO] ErroreClonazione ERRORE
ERRORE NELL'OPERAZIONE DI CLONAZIONE

L'operazione � stata interrotta a causa di problemi riscontrati nella clonazione;
contattare l'assistenza.

Inizio processo: �1
Interruzione processo: �2




[MESSAGGIO] ErroreAggiornamentoGruppoProdotti ERRORE
ERRORE AGGIORNAMENTO GRUPPO PRODOTTI

Non � stato possibile aggiornare le seguenti caratteristiche sul gruppo di prodotti selezionato:
�1

contattare l'assistenza.




[MESSAGGIO] NotificaNuovoInserimento NOTIFICA
NOTIFICA NUOVO INSERIMENTO

I dati presenti nei campi del form sono stati inseriti in base dati




[MESSAGGIO] NotificaAggiornamentoGruppoProdotti NOTIFICA
NOTIFICA AGGIORNAMENTO GRUPPO PRODOTTI

Il gruppo di prodotti selezionato � stato correttamente aggiornato.




[MESSAGGIO] NotificaPiantaCompleta NOTIFICA
NOTIFICA COMPLETEZZA PIANTA

La pianta selezionata risulta completa in tutte le sue parti.





[MESSAGGIO] NotificaOperativit�OperatoriOK NOTIFICA
NOTIFICA

Gli operatori selezionati sono pienamente operativi su tutti i prodotti associati all'offerta pacchetto.






[MESSAGGIO] NotificaGrigliaPostiGiaEsistente NOTIFICA
NOTIFICA

L'area selezionata ha gi� una griglia posti associata; per poter clonare una griglia posti da un'altra area bisogna prima eliminarla.





[MESSAGGIO] NotificaOffertaPacchettoCompleta NOTIFICA
NOTIFICA COMPLETEZZA OFFERTA PACCHETTO

L'offerta pacchetto �1 risulta completa in tutte le sue parti.




[MESSAGGIO] NotificaOperazioneNonEseguibile NOTIFICA
NOTIFICA OPERAZIONE NON ESEGUIBILE

Non � possibile portare allo stato 'In Configurazione' il prodotto sullo schema SETA_INTEGRA e quindi l'operazione corrente non pu� essere eseguita.






[MESSAGGIO] NotificaInserimentoProtezioni NOTIFICA
REPORT INSERIMENTO PROTEZIONI

prodotto: �1

protezioni preesistenti: �2
protezioni cancellate: �3
protezioni sovrascritte: �4
protezioni solo aggiunte: �5
protezioni aggiunte e sovrascritte: �6
totale protezioni attualmente presenti: �7





[MESSAGGIO] VisualizzazioneStatoProtezioni NOTIFICA
VISUALIZZAZIONE STATO PROTEZIONI

prodotto: �1 

Sono attualmente presenti �2 protezioni.

DETTAGLIO
(Superarea; Area; Simbolo su pianta; Stato posti; Numero protezioni)
�3




[MESSAGGIO] NotificaMancatoInserimentoProtezioni NOTIFICA
NOTIFICA MANCATO INSERIMENTO PROTEZIONI

Il prodotto master non ha alcuna protezione sui posti per i criteri di scelta impostati.
Non � stata inserita alcuna protezione




[MESSAGGIO] NotificaRimozioneProtezioni NOTIFICA
NOTIFICA RIMOZIONE PROTEZIONI

Sono state rimosse le protezioni sul prodotto corrente




[MESSAGGIO] NotificaMancataRimozioneProtezioni NOTIFICA
NOTIFICA MANCATA RIMOZIONE PROTEZIONI

Non � presente alcuna protezione sul prodotto corrente




[MESSAGGIO] NotificaOrdineDiPostoSIAENonConfigurato NOTIFICA
NOTIFICA MANCATO AGGIORNAMENTO

La pianta SIAE scelta non � completa, cio� non esistono gli ordini di posto SIAE per tutte le superaree della pianta.
Non � stato possibile aggiornare i prezzi della superarea �1




[MESSAGGIO] NotificaApplicazioneGiaLanciata NOTIFICA

Su questo terminale � gi� attiva un'istanza dell'applicazione Client di Configurazione e non sar� pertanto possibile lanciarne un'altra





[MESSAGGIO] NotificaEsistenzaPostiSenzaAttributi NOTIFICA

Nella griglia esistono dei posti senza nome Fila e/o senza nome Posto e/o non assegnati ad un'area (posti di colore rosso).
Non sar� possibile salvare in Base Dati finch� tali posti non saranno correttamente configurati oppure eliminati.





[MESSAGGIO] NotificaLayoutNonEliminabile NOTIFICA
NOTIFICA LAYOUT NON ELIMINABILE

L'area selezionata ha ereditato il suo Layout; esso non � pertanto eliminabile





[MESSAGGIO] NotificaPrezziNonModificabili NOTIFICA
NOTIFICA PREZZI NON MODIFICABILI

Per il prodotto �1 sono stati emessi dei titoli e pertanto non � possibile modificare i prezzi.





[MESSAGGIO] NotificaPostiNonDisegnabili NOTIFICA
NOTIFICA POSTI NON DISEGNABILI

Non � possibile disegnare i posti in quanto uscirebbero dall'area di disegno




[MESSAGGIO] NotificaNomiNonCorretti NOTIFICA
NOTIFICA NOMI NON CORRETTI

Non � possibile disegnare i posti in quanto i nomi scelti per le file e/o per i posti non rispettano i formati stabiliti




[MESSAGGIO] NotificaPostiSovrapposti NOTIFICA
NOTIFICA POSTI SOVRAPPOSTI

I posti selezionati sono stati disegnati sopra a dei posti gi� esistenti e vanno dunque spostati o eliminati.




[MESSAGGIO] NotificaPostiNonRiposizionabili NOTIFICA
NOTIFICA POSTI NON SPOSTABILI

Non � possibile spostare i posti selezionati in quanto uscirebbero dall'area di disegno oppure si sovrapporrebbero parzialmente o totalmente a posti gi� presenti sulla griglia




[MESSAGGIO] NotificaTipoSupportoNonEliminabile NOTIFICA
NOTIFICA TIPOSUPPORTO NON ELIMINABILE

L'area selezionata ha ereditato il suo Tipo Supporto; esso non � pertanto eliminabile




[MESSAGGIO] NotificaClonazioneProdotto NOTIFICA
NOTIFICA CLONAZIONE PRODOTTO

E' stato creato il prodotto �1 clonando le caratteristiche di configurazione del prodotto �2.

Inizio processo: �3
Fine processo: �4





[MESSAGGIO] NotificaClonazioneOrganizzazione NOTIFICA
NOTIFICA CLONAZIONE ORGANIZZAZIONE

E' stata creata l'organizzazione �1 clonando le caratteristiche di configurazione dell'organizzazione �2.

Inizio processo: �3
Fine processo: �4




[MESSAGGIO] NotificaClonazionePianta NOTIFICA
NOTIFICA CLONAZIONE PIANTA

E' stata creata la pianta �1 clonando le caratteristiche di configurazione della pianta �2.

Inizio processo: �3
Fine processo: �4




[MESSAGGIO] NotificaClonazioneOperatore NOTIFICA
NOTIFICA CLONAZIONE OPERATORE

E' stato creato l'operatore �1 clonando le caratteristiche di configurazione dell'operatore �2.

Inizio processo: �3
Fine processo: �4



[MESSAGGIO] NotificaClonazionePuntoVendita NOTIFICA
NOTIFICA CLONAZIONE PUNTO VENDITA

E' stato creato il punto vendita �1 clonando le caratteristiche di configurazione del punto vendita �2.

Inizio processo: �3
Fine processo: �4




[MESSAGGIO] NotificaNonEditabilit�PiantaSIAE RICHIESTACONFERMA
NOTA: MESSAGGIO TEMPORANEO

La pianta SIAE � gi� associata ai prodotti:
�1
La modifica dell'associazione delle aree agli ordini di posto SIAE � dunque potenzialmente pericolosa.

Premere [Conferma] per modificare comunque, altrimenti premere [Annulla].



[MESSAGGIO] NotificaModificaDati NOTIFICA
NOTIFICA MODIFICA IN BASE DATI

Le modifiche apportate ai dati visualizzati sono state salvate in base dati



[MESSAGGIO] NotificaEliminazioneDati NOTIFICA
NOTIFICA ELIMINAZIONE IN BASE DATI

Il record relativo ai dati visualizzati � stato eliminato dalla base dati



[MESSAGGIO] NotificaRecordNonEliminabile NOTIFICA
�1 non � eliminabile; 
record correlati in: 
�2




[MESSAGGIO] NotificaEliminazioneGenereCartaLIS NOTIFICA
Il genere non � eliminabile in quanto esistono categorie associate




[MESSAGGIO] NotificaPostiNonEliminabili NOTIFICA
I seguenti posti non sono eliminabili in quanto su di essi risultano titoli emessi o protezioni attive: 
�1




[MESSAGGIO] NotificaPostiNonModificabili NOTIFICA
I seguenti posti non sono modificabili in quanto su di essi risultano titoli emessi o protezioni attive: 
�1




[MESSAGGIO] NotificaNumeroMaxPeriodiCommerciali NOTIFICA
Non si possono inserire pi� di 2 Periodi Commerciali per Prodotto




[MESSAGGIO] NotificaNonEditabilit�Campi NOTIFICA
NOTIFICA DI NON EDITABILITA' CAMPI

I campi del form non sono editabili in quanto
�1
le modifiche apportate non saranno perci� salvate in base dati.




[MESSAGGIO] AvvertimentoModalit�EmissioneMancante AVVERTIMENTO
AVVERTIMENTO DI NON COMPLETEZZA

E' necessario associare al prodotto almeno una modalit� di emissione.




[MESSAGGIO] NotificaAggiornamentoPrezziNonEseguito NOTIFICA
NOTIFICA DI NON AGGIORNABILITA' DEI PREZZI

I seguenti prezzi non sono stati aggiornati in quanto sono risultati dei titoli emessi:
�1





[MESSAGGIO] NotificaAssociazioneRappresentazioniNonEseguita NOTIFICA
NOTIFICA DI NON ASSOCIABILITA' DELLE RAPPRESENTAZIONI

Le seguenti rappresentazioni non sono state associate al prodotto in quanto alcuni prodotti a cui sono associate non si trovano nello stato 'in configurazione' e/o sono risultati dei titoli emessi sugli stessi posti per il prodotto selezionato e per alcuni dei prodotti correlati e/o alcuni dei prodotti correlati sono in configurazione da parte di un altro utente:
�1



[MESSAGGIO] NotificaAssociazioneRappresentazioniNonEseguitaPerProdottiBloccati NOTIFICA
NOTIFICA DI NON ASSOCIABILITA' DELLE RAPPRESENTAZIONI

Le seguenti rappresentazioni non sono state associate al prodotto in quanto alcuni prodotti alcuni dei prodotti a cui sono associate sono in configurazione da parte di un altro utente:
�1




[MESSAGGIO] NotificaAggiornamentoRappresentazioneNonEseguita NOTIFICA
NOTIFICA DI NON AGGIORNABILITA' DELLA RAPPRESENTAZIONE

La rappresentazione selezionata non � aggiornabile in quanto alcuni prodotti correlati tramite essa non si trovano nello stato 'in configurazione' e/o alcuni dei prodotti correlati sono in configurazione da parte di un altro utente.






[MESSAGGIO] NotificaRecordBloccato NOTIFICA
RECORD IN LAVORAZIONE DA PARTE DI UN ALTRO UTENTE

Non � possibile accedere alle funzionalit� di modifica per il record selezionato in quanto in lavorazione da parte di un altro utente.





[MESSAGGIO] ConfermaAggiornamentoListaProdotti RICHIESTACONFERMA
CONFERMA DELL'AGGIORNAMENTO DEI PRODOTTI SELEZIONATI

I prodotti seguenti si trovano nello stato 'Attivo' e pertanto la loro modifica potrebbe causare disallineamenti tra il DB e i serventi:
�1

I prodotti seguenti si trovano nello stato 'Attivabile' e vanno prima portati nello stato 'In configurazione':
�2

I prodotti seguenti si trovano nello stato 'In configurazione' ma non sono modificabili in quanto in lavorazione da parte di altri utenti:
�3

I prodotti seguenti si trovano in altri stati e non sono pertanto modificabili:
�4

Tutti gli altri prodotti selezionati si trovano nello stato 'In configurazione' e saranno comunque aggiornati.

Premere [Conferma] per modificare anche i prodotti nello stato 'Attivo', altrimenti premere [Annulla].





[MESSAGGIO] NotificaDisabilitazioneOperatore NOTIFICA
NOTIFICA DISABILITAZIONE OPERATORE

L'operatore selezionato ha eseguito operazioni sui titoli o gli � stato assegnato un lotto titoli e pertanto non � eliminabile; esso sar� disabilitato



[MESSAGGIO] AvvertimentoPiantaSenzaAree AVVERTIMENTO
AVVERTIMENTO PIANTA SENZA AREE

La pianta configurata non contiene alcuna area; 
� necessario associare almeno un'area per poter disegnare la griglia




[MESSAGGIO] AvvertimentoNonAttivabilit�Prodotto AVVERTIMENTO
AVVERTIMENTO NON ATTIVABILITA' PRODOTTO

Il prodotto selezionato non � attivabile per i seguenti motivi:
�1





[MESSAGGIO] AvvertimentoPiantaIncompleta AVVERTIMENTO
AVVERTIMENTO NON COMPLETEZZA PIANTA

La pianta �1 risulta incompleta per i seguenti motivi:
�2

Sar� pertanto necessario modificare la pianta, altrimenti non sar� possibile rendere attivabili prodotti su di essa





[MESSAGGIO] AvvertimentoOffertaPacchettoIncompleta AVVERTIMENTO
AVVERTIMENTO NON COMPLETEZZA OFFERTA PACCHETTO

L'offerta pacchetto �1 risulta incompleta per i seguenti motivi:
�2

Sar� pertanto necessario modificarla, altrimenti non sar� possibile emettere titoli.




[MESSAGGIO] AvvertimentoSuperamentoLimitePosti RICHIESTACONFERMA
AVVERTIMENTO SUPERAMENTO LIMITE POSTI

Il numero posti � maggiore del massimo; i posti verranno disegnati sovrapposti
Premere [Conferma] per creare i posti in ogni caso, altrimenti premere [Annulla].




[MESSAGGIO] AvvertimentoSuperamentoLimiteFile RICHIESTACONFERMA
AVVERTIMENTO SUPERAMENTO LIMITE FILE

Il numero di file � maggiore del massimo; i posti verranno disegnati sovrapposti
Premere [Conferma] per creare i posti in ogni caso, altrimenti premere [Annulla].




[MESSAGGIO] AvvertimentoPiantaSenzaOrganizzazioneVenue AVVERTIMENTO
AVVERTIMENTO PIANTA SENZA ORGANIZZAZIONE/VENUE

Per la pianta configurata non � stata scelta alcuna Organizzazione e/o Venue; 
� necessario associare almeno un'Organizzazione e un Venue per poter disegnare la griglia




[MESSAGGIO] AvvertimentoIndicePreferibilitaGiaPresentePerArea AVVERTIMENTO
AVVERTIMENTO INDICE DI PREFERIBILITA' GIA' PRESENTE 

E' gi� stata definita una fascia con indice di preferibilit� �1 per l'area �2.




[MESSAGGIO] AvvertimentoIndicePreferibilitaGiaPresentePerFascia AVVERTIMENTO
AVVERTIMENTO ORDINE GIA' PRESENTE 

E' gi� stata definita una sequenza con indice di preferibilit� �1 per la fascia �2.




[MESSAGGIO] ErroreFormatoOra ERRORE
ERRORE NEL FORMATO DEL CAMPO ORA.

Il campo �1 non � nel formato corretto; controllare:
a) che il valore inserito sia numerico intero;
b) che il valore inserito sia compreso tra 00 e 23 (estremi inclusi)



[MESSAGGIO] ErroreFormatoMinuti ERRORE
ERRORE NEL FORMATO DEL CAMPO MINUTI.

Il campo �1 non � nel formato corretto; controllare:
a) che il valore inserito sia numerico intero;
b) che il valore inserito sia compreso tra 00 e 59 (estremi inclusi)


[MESSAGGIO] SessioneDuplicata AVVERTIMENTO
SESSIONE GIA' PRESENTE NELLA BASE DATI

La sessione che si � tentato di inserire risulta gi� presente in base dati. L'inserimento non � stato effettuato.

[MESSAGGIO] AvvertimentoServertiOrganizzazioniOProdottiAttivi AVVERTIMENTO

Prima di confermare le modifiche relative alla sessione corrente, disattivare tutti i serventi organizzazione o prodotti coinvolti.


[MESSAGGIO] AvvertimentoProdottiAttivi AVVERTIMENTO

Prima di confermare le modifiche relative alla sessione corrente, disattivare tutti prodotti coinvolti.



[MESSAGGIO] ErroreNonConformit�Campi ERRORE
ERRORE DI NON CONFORMITA' DEI CAMPI.

Sono stati rilevati i seguenti errori nei valori immessi nei campi:
�1



[MESSAGGIO] ConfermaValori RICHIESTACONFERMA
VALORI DA CONFERMARE.

I seguenti valori richiedono conferma:
�1

Premere [Conferma] per accettare i valori inseriti, [Annulla] per tornare alla configurazione.





[MESSAGGIO] ConfermaInserimentoAssociazioneRappresentazioneProdotto RICHIESTACONFERMA
INSERIMENTO ASSOCIAZIONE PRODOTTO RAPPRESENTAZIONE IN BASE DATI

Sar� aggiunta in base dati l'associazione tra il Prodotto (in configurazione) e Rappresentazione selezionati. L'operazione potrebbe essere pericolosa.
�1.

Premere [Conferma] per proseguire, altrimenti premere [Annulla].





[MESSAGGIO] ConfermaModificaDataRappresentazione RICHIESTACONFERMA
MODIFICA DATA RAPPRESENTAZIONE IN BASE DATI

Saranno modificate in base dati la data - ora inizio e la durata di una Rappresentazione selezionata. L'operazione potrebbe essere pericolosa.
�1.

Premere [Conferma] per proseguire, altrimenti premere [Annulla].





[MESSAGGIO] ConfermaModificaTipoSupportoProdotti RICHIESTACONFERMA

Tutti i prodotti selezionati verranno associati al tipo supporto impostato.

Premere [Conferma] per procedere con la modifica, altrimenti [Annulla].





[MESSAGGIO] ConfermaSbloccoProdotto RICHIESTACONFERMA

Il prodotto selezionato verr� sbloccato.

Premere [Conferma] per procedere con lo sblocco, altrimenti [Annulla].

