VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoProtezioni 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   37
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   40
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   39
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   38
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   34
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   36
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   35
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ComboBox cmbProdottoMaster 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   33
      Top             =   2640
      Width           =   3585
   End
   Begin VB.Frame fraTipoOperazione 
      Caption         =   "Esegui:"
      Height          =   1275
      Left            =   240
      TabIndex        =   30
      Top             =   840
      Width           =   2895
      Begin VB.OptionButton optVisualizza 
         Caption         =   "visualizzazione stato protezioni"
         Height          =   255
         Left            =   180
         TabIndex        =   46
         Top             =   900
         Width           =   2535
      End
      Begin VB.OptionButton optInserisci 
         Caption         =   "inserimento protezioni"
         Height          =   255
         Left            =   180
         TabIndex        =   32
         Top             =   300
         Width           =   2115
      End
      Begin VB.OptionButton optRimuovi 
         Caption         =   "rimozione protezioni"
         Height          =   255
         Left            =   180
         TabIndex        =   31
         Top             =   600
         Width           =   2115
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   405
      Left            =   8460
      TabIndex        =   29
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   405
      Left            =   10200
      TabIndex        =   28
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraProdottoCorrente 
      Caption         =   "Prodotto di destinazione (prodotto corrente)"
      Height          =   4215
      Left            =   6660
      TabIndex        =   18
      Top             =   3060
      Width           =   5175
      Begin VB.CheckBox chkInserisciSoloSuPostiLiberi 
         Caption         =   "inserisci protezioni solo su posti liberi"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   3720
         Width           =   3855
      End
      Begin VB.Frame fraImportaSuProdottoCorrente 
         Caption         =   "Step 2 - Importa le protezioni:"
         Height          =   1395
         Left            =   240
         TabIndex        =   23
         Top             =   2100
         Width           =   4695
         Begin VB.OptionButton optImportaAggiungendoESovrascrivendo 
            Caption         =   "aggiungendo e sovrascrivendo le protezioni in comune"
            Height          =   195
            Left            =   180
            TabIndex        =   26
            Top             =   960
            Width           =   4335
         End
         Begin VB.OptionButton optImportaSoloAggiungendo 
            Caption         =   "solo aggiungendo"
            Height          =   195
            Left            =   180
            TabIndex        =   25
            Top             =   360
            Width           =   2535
         End
         Begin VB.OptionButton optImportaSovrascrivendo 
            Caption         =   "sovrascrivendo le protezioni in comune"
            Height          =   195
            Left            =   180
            TabIndex        =   24
            Top             =   660
            Width           =   4155
         End
      End
      Begin VB.Frame fraProtezioniDaCancellare 
         Caption         =   "Step 1 - Protezioni presenti da cancellare:"
         Height          =   1395
         Left            =   240
         TabIndex        =   19
         Top             =   540
         Width           =   4695
         Begin VB.OptionButton optCancellaSoloCorrispondenti 
            Caption         =   "solo le protezioni in comune con il prodotto origine"
            Height          =   195
            Left            =   180
            TabIndex        =   22
            Top             =   660
            Width           =   4275
         End
         Begin VB.OptionButton optCancellaTutte 
            Caption         =   "tutte"
            Height          =   195
            Left            =   180
            TabIndex        =   21
            Top             =   360
            Width           =   1275
         End
         Begin VB.OptionButton optCancellaNessuna 
            Caption         =   "nessuna"
            Height          =   195
            Left            =   180
            TabIndex        =   20
            Top             =   960
            Width           =   1215
         End
      End
   End
   Begin VB.ListBox lstCausaliSuMaster 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   180
      Style           =   1  'Checkbox
      TabIndex        =   17
      Top             =   3300
      Width           =   4635
   End
   Begin VB.Frame fraSelezioneProtezioni 
      Caption         =   "Seleziona protezioni da:"
      Height          =   1755
      Left            =   180
      TabIndex        =   2
      Top             =   5520
      Width           =   4635
      Begin VB.Frame fraPostiLiberi 
         Height          =   315
         Left            =   1680
         TabIndex        =   9
         Top             =   600
         Width           =   1695
         Begin VB.OptionButton optPostiLiberiCorrelati 
            Height          =   195
            Left            =   1380
            TabIndex        =   11
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optPostiLiberiMaster 
            Height          =   195
            Left            =   60
            TabIndex        =   10
            Top             =   120
            Width           =   195
         End
      End
      Begin VB.Frame fraPostiRiservati 
         Height          =   315
         Left            =   1680
         TabIndex        =   6
         Top             =   900
         Width           =   1695
         Begin VB.OptionButton optPostiRiservatiMaster 
            Height          =   195
            Left            =   60
            TabIndex        =   8
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optPostiRiservatiCorrelati 
            Height          =   195
            Left            =   1380
            TabIndex        =   7
            Top             =   120
            Width           =   195
         End
      End
      Begin VB.Frame fraPostiVenduti 
         Height          =   315
         Left            =   1680
         TabIndex        =   3
         Top             =   1200
         Width           =   1695
         Begin VB.OptionButton optPostiVendutiCorrelati 
            Height          =   195
            Left            =   1380
            TabIndex        =   5
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optPostiVendutiMaster 
            Height          =   195
            Left            =   60
            TabIndex        =   4
            Top             =   120
            Width           =   195
         End
      End
      Begin VB.Label lblPostiLiberi 
         Alignment       =   1  'Right Justify
         Caption         =   "posti liberi"
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   1155
      End
      Begin VB.Label lblPostiRiservati 
         Alignment       =   1  'Right Justify
         Caption         =   "posti riservati"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   1020
         Width           =   1155
      End
      Begin VB.Label lblPostiVenduti 
         Alignment       =   1  'Right Justify
         Caption         =   "posti venduti"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   1320
         Width           =   1155
      End
      Begin VB.Label lblSoloMaster 
         Alignment       =   2  'Center
         Caption         =   "origine"
         Height          =   195
         Left            =   1440
         TabIndex        =   13
         Top             =   360
         Width           =   795
      End
      Begin VB.Label lblCorrelati 
         Alignment       =   2  'Center
         Caption         =   "origine + abb. correlati"
         Height          =   195
         Left            =   2340
         TabIndex        =   12
         Top             =   360
         Width           =   1875
      End
   End
   Begin VB.CommandButton cmdSelezionaTutti 
      Caption         =   "Seleziona tutti"
      Height          =   315
      Left            =   180
      TabIndex        =   1
      Top             =   5040
      Width           =   2295
   End
   Begin VB.CommandButton cmdDeselezionaTutti 
      Caption         =   "Deseleziona tutti"
      Height          =   315
      Left            =   2520
      TabIndex        =   0
      Top             =   5040
      Width           =   2295
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Impostazione delle Protezioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   45
      Top             =   120
      Width           =   5415
   End
   Begin VB.Label lblProdottoMaster 
      Caption         =   "Prodotto origine"
      Height          =   195
      Left            =   180
      TabIndex        =   44
      Top             =   2400
      Width           =   1695
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   43
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   42
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblCausaliSuMaster 
      Caption         =   "Causali di protezione presenti sul prodotto origine (simbolo - nome)"
      Height          =   195
      Left            =   180
      TabIndex        =   41
      Top             =   3060
      Width           =   4635
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000001&
      X1              =   5760
      X2              =   5760
      Y1              =   2640
      Y2              =   7260
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoProtezioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idProdottoMasterSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private selezionaPostiLiberiMaster As Boolean
Private selezionaPostiRiservatiMaster As Boolean
Private selezionaPostiVendutiMaster As Boolean
Private selezionaPostiLiberiCorrelati As Boolean
Private selezionaPostiRiservatiCorrelati As Boolean
Private selezionaPostiVendutiCorrelati As Boolean
Private cancellaTutteProtezioni As Boolean
Private cancellaSoloProtezioniCorrispondenti As Boolean
Private cancellaNessunaProtezione As Boolean
Private importaSoloAggiungendo As Boolean
Private importaSovrascrivendoCorrispondenti As Boolean
Private importaAggiungendoESovrascrivendo As Boolean
Private inserisciProtezioniSoloSuPostiLiberi As ValoreBooleanoEnum
Private isProdottoAttivoSuTL As Boolean
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long

Private internalEvent As Boolean
Private listaCausaliProtezioneMasterSelezionate As Collection
Private numeroProtezioniPreesistenti As Long
Private numeroProtezioniCancellate As Long
Private numeroProtezioniSovrascritte As Long
Private numeroProtezioniSoloAggiunte As Long
Private numeroProtezioniAggiunteESovrascritte As Long

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private tipoArea As TipoAreaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum
Private TipoOperazione As TipoOperazioneImpostazioneProtezioniEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetIdProdottoMasterSelezionato(idM As Long)
    idProdottoMasterSelezionato = idM
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    fraPostiLiberi.BorderStyle = vbBSNone
    fraPostiRiservati.BorderStyle = vbBSNone
    fraPostiVenduti.BorderStyle = vbBSNone
    optInserisci.Enabled = True
    optRimuovi.Enabled = True
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            TipoOperazione = TOIP_NON_SPECIFICATO) Then
        Call cmbProdottoMaster.Clear
        fraSelezioneProtezioni.Enabled = False
        cmbProdottoMaster.Enabled = False
        lblProdottoMaster.Enabled = False
        lblPostiLiberi.Enabled = False
        lblPostiRiservati.Enabled = False
        lblPostiVenduti.Enabled = False
        lblSoloMaster.Enabled = False
        lblCorrelati.Enabled = False
        fraProdottoCorrente.Enabled = False
        fraProtezioniDaCancellare.Enabled = False
        optCancellaTutte.Enabled = False
        optCancellaSoloCorrispondenti.Enabled = False
        optCancellaNessuna.Enabled = False
        fraImportaSuProdottoCorrente.Enabled = False
        optImportaSoloAggiungendo.Enabled = False
        optImportaSovrascrivendo.Enabled = False
        optImportaAggiungendoESovrascrivendo.Enabled = False
        chkInserisciSoloSuPostiLiberi.Enabled = False
        lstCausaliSuMaster.Enabled = False
        lblCausaliSuMaster.Enabled = False
        cmdSelezionaTutti.Enabled = False
        cmdDeselezionaTutti.Enabled = False
        fraTipoOperazione.Enabled = True
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            TipoOperazione <> TOIP_NON_SPECIFICATO) Then
        fraSelezioneProtezioni.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        cmbProdottoMaster.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblProdottoMaster.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblPostiLiberi.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblPostiRiservati.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblPostiVenduti.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblSoloMaster.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblCorrelati.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        fraProdottoCorrente.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        fraProtezioniDaCancellare.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        optCancellaTutte.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        optCancellaSoloCorrispondenti.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        optCancellaNessuna.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        fraImportaSuProdottoCorrente.Enabled = (TipoOperazione = TOIP_INSERIMENTO And _
            (cancellaTutteProtezioni Or cancellaSoloProtezioniCorrispondenti Or cancellaNessunaProtezione))
        optImportaSoloAggiungendo.Enabled = (TipoOperazione = TOIP_INSERIMENTO And _
            (cancellaTutteProtezioni Or cancellaSoloProtezioniCorrispondenti Or cancellaNessunaProtezione))
        optImportaSovrascrivendo.Enabled = (TipoOperazione = TOIP_INSERIMENTO And _
            (cancellaNessunaProtezione))
        optImportaAggiungendoESovrascrivendo.Enabled = (TipoOperazione = TOIP_INSERIMENTO And _
            (cancellaNessunaProtezione))
        chkInserisciSoloSuPostiLiberi.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lstCausaliSuMaster.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        lblCausaliSuMaster.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        cmdSelezionaTutti.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        cmdDeselezionaTutti.Enabled = (TipoOperazione = TOIP_INSERIMENTO)
        fraTipoOperazione.Enabled = True
        cmdConferma.Enabled = TipoOperazione = TOIP_RIMOZIONE Or _
            TOIP_VISUALIZZAZIONE Or _
            (TipoOperazione = TOIP_INSERIMENTO And _
            idProdottoMasterSelezionato <> idNessunElementoSelezionato And _
            listaCausaliProtezioneMasterSelezionate.count > 0 And _
            (selezionaPostiLiberiMaster = True Or selezionaPostiLiberiCorrelati = True Or _
            selezionaPostiRiservatiMaster = True Or selezionaPostiVendutiCorrelati = True Or _
            selezionaPostiVendutiMaster = True Or selezionaPostiVendutiCorrelati = True) And _
            (cancellaTutteProtezioni Or cancellaSoloProtezioniCorrispondenti Or cancellaNessunaProtezione) And _
            (importaSoloAggiungendo Or importaSovrascrivendoCorrispondenti Or importaAggiungendoESovrascrivendo))
        cmdAnnulla.Enabled = True
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            TipoOperazione = TOIP_NON_SPECIFICATO) Then
        Call cmbProdottoMaster.Clear
        fraSelezioneProtezioni.Enabled = False
        cmbProdottoMaster.Enabled = False
        lblProdottoMaster.Enabled = False
        optInserisci.Enabled = True
        lblPostiRiservati.Enabled = False
        lblPostiVenduti.Enabled = False
        lblSoloMaster.Enabled = False
        lblCorrelati.Enabled = False
        fraProdottoCorrente.Enabled = False
        fraProtezioniDaCancellare.Enabled = False
        optCancellaTutte.Enabled = False
        optCancellaSoloCorrispondenti.Enabled = False
        optCancellaNessuna.Enabled = False
        fraImportaSuProdottoCorrente.Enabled = False
        optImportaSoloAggiungendo.Enabled = False
        optImportaSovrascrivendo.Enabled = False
        optImportaAggiungendoESovrascrivendo.Enabled = False
        chkInserisciSoloSuPostiLiberi.Enabled = False
        lstCausaliSuMaster.Enabled = False
        lblCausaliSuMaster.Enabled = False
        cmdSelezionaTutti.Enabled = False
        cmdDeselezionaTutti.Enabled = False
        fraTipoOperazione.Enabled = True
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = True
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    TipoOperazione = TOIP_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()

    Set listaCausaliProtezioneMasterSelezionate = New Collection
    Call SetIdProdottoMasterSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneTipoOperazione(TOIP_NON_SPECIFICATO)
    Call SelezioneProtezioni_Init
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Public Sub SetGestioneTipoOperazione(tOp As TipoOperazioneImpostazioneProtezioniEnum)
    TipoOperazione = tOp
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True
    
    optPostiLiberiMaster.Value = selezionaPostiLiberiMaster
    optPostiRiservatiMaster.Value = selezionaPostiRiservatiMaster
    optPostiVendutiMaster.Value = selezionaPostiVendutiMaster
    optPostiLiberiCorrelati.Value = selezionaPostiLiberiCorrelati
    optPostiRiservatiCorrelati.Value = selezionaPostiRiservatiCorrelati
    optPostiVendutiCorrelati.Value = selezionaPostiVendutiCorrelati
    optCancellaTutte.Value = cancellaTutteProtezioni
    optCancellaSoloCorrispondenti.Value = cancellaSoloProtezioniCorrispondenti
    optCancellaNessuna.Value = cancellaNessunaProtezione
    optImportaSoloAggiungendo.Value = importaSoloAggiungendo
    optImportaSovrascrivendo.Value = importaSovrascrivendoCorrispondenti
    optImportaAggiungendoESovrascrivendo.Value = importaAggiungendoESovrascrivendo
    chkInserisciSoloSuPostiLiberi.Value = inserisciProtezioniSoloSuPostiLiberi
    Select Case TipoOperazione
        Case TOIP_INSERIMENTO
            optInserisci.Value = True
        Case TOIP_RIMOZIONE
            optRimuovi.Value = True
        Case TOIP_VISUALIZZAZIONE
            optVisualizza.Value = True
        Case Else
            optInserisci.Value = False
            optRimuovi.Value = False
            optVisualizza.Value = False
    End Select
    Call SelezionaElementoSuCombo(cmbProdottoMaster, idProdottoMasterSelezionato)
        
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub chkInserisciSoloSuPostiLiberi_Click()
    If Not internalEvent Then
        inserisciProtezioniSoloSuPostiLiberi = chkInserisciSoloSuPostiLiberi.Value
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbProdottoMaster_Click()
    If Not internalEvent Then
        idProdottoMasterSelezionato = cmbProdottoMaster.ItemData(cmbProdottoMaster.ListIndex)
        Call CaricaListaCausaliProtezioneMaster
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub CaricaListaCausaliProtezioneMaster()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idCausale As Long
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD
    
    i = 1
    Call lstCausaliSuMaster.Clear
    sql = "SELECT DISTINCT P.IDCAUSALEPROTEZIONE ID, SIMBOLOSUPIANTA, NOME" & _
        " FROM CAUSALEPROTEZIONE C, PROTEZIONEPOSTO P" & _
        " WHERE C.IDCAUSALEPROTEZIONE = P.IDCAUSALEPROTEZIONE" & _
        " AND P.IDPRODOTTO = " & idProdottoMasterSelezionato & _
        " ORDER BY SIMBOLOSUPIANTA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idCausale = rec("ID").Value
            descrizione = rec("SIMBOLOSUPIANTA").Value & " - " & rec("NOME").Value
            Call lstCausaliSuMaster.AddItem(descrizione)
            lstCausaliSuMaster.ItemData(i - 1) = idCausale
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub


Private Sub optInserisci_Click()
    If Not internalEvent Then
        Call Inserisci
    End If
End Sub

Private Sub Inserisci()
    TipoOperazione = TOIP_INSERIMENTO
    Call CaricaValoriComboProdottiMaster
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPostiLiberiCorrelati_Click()
    If Not internalEvent Then
        selezionaPostiLiberiMaster = False
        selezionaPostiLiberiCorrelati = True
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPostiLiberiMaster_Click()
    If Not internalEvent Then
        selezionaPostiLiberiMaster = True
        selezionaPostiLiberiCorrelati = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPostiRiservatiCorrelati_Click()
    If Not internalEvent Then
        selezionaPostiRiservatiMaster = False
        selezionaPostiRiservatiCorrelati = True
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPostiRiservatiMaster_Click()
    If Not internalEvent Then
        selezionaPostiRiservatiMaster = True
        selezionaPostiRiservatiCorrelati = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPostiVendutiCorrelati_Click()
    If Not internalEvent Then
        selezionaPostiVendutiMaster = False
        selezionaPostiVendutiCorrelati = True
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPostiVendutiMaster_Click()
    If Not internalEvent Then
        selezionaPostiVendutiMaster = True
        selezionaPostiVendutiCorrelati = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optRimuovi_Click()
    If Not internalEvent Then
        Call Rimuovi
    End If
End Sub

Private Sub Rimuovi()
    TipoOperazione = TOIP_RIMOZIONE
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optVisualizza_Click()
    If Not internalEvent Then
        Call Visualizza
    End If
End Sub

Private Sub Visualizza()
    TipoOperazione = TOIP_VISUALIZZAZIONE
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriComboProdottiMaster()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME FROM PRODOTTO" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND IDPIANTA = " & idPiantaSelezionata & _
        " AND IDPRODOTTO <> " & idProdottoSelezionato & _
        " ORDER BY NOME"
    Call cmbProdottoMaster.Clear
    Call CaricaValoriCombo(cmbProdottoMaster, sql, "NOME")
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetIdProdottoMasterSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneTipoOperazione(TOIP_NON_SPECIFICATO)
    Call SelezioneProtezioni_Init
    Call lstCausaliSuMaster.Clear
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                If StatoAttualeProdottoSuSchemaProduzione(idProdottoSelezionato) = TSP_IN_CONFIGURAZIONE Or modalitāFormCorrente = A_NUOVO Then
'                    Select Case TipoOperazione
'                        Case TOIP_INSERIMENTO
'                            Call InserisciProtezioni
'                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PROTEZIONI, stringaNota, idProdottoSelezionato)
'                        Case TOIP_RIMOZIONE
'                            Call RimuoviProtezioni
'                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PROTEZIONI, stringaNota, idProdottoSelezionato)
'                        Case TOIP_VISUALIZZAZIONE
'                            Call visualizzaStatoProtezioni
'                        Case Else
'                            'Do Nothing
'                    End Select
'                Else
'                    Call frmMessaggio.Visualizza("NotificaOperazioneNonEseguibile")
'                End If
'            Else
                Select Case TipoOperazione
                    Case TOIP_INSERIMENTO
                        Call InserisciProtezioni
                        Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PROTEZIONI, stringaNota, idProdottoSelezionato)
                    Case TOIP_RIMOZIONE
                        Call RimuoviProtezioni
                        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PROTEZIONI, stringaNota, idProdottoSelezionato)
                    Case TOIP_VISUALIZZAZIONE
                        Call visualizzaStatoProtezioni
                    Case Else
                        'Do Nothing
                End Select
'            End If
            Call SetGestioneTipoOperazione(TOIP_NON_SPECIFICATO)
            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
            Call SelezioneProtezioni_Init
            Call AssegnaValoriCampi
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub RimuoviProtezioni()
    Dim sql As String
    Dim n As Long
    
    Call frmMessaggio.Visualizza("ConfermaRimozioneProtezioni")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        DoEvents
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''           NOTA: QUESTA GESTIONE E' UN PO' DIVERSA DALLE ALTRE; VEDERE IN SEGUITO DI UNIFICARE
''           1. CANCELLARE SU CCT TUTTI I RECORD CONTRASSEGNATI COME NUOVI
'            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND IDTIPOSTATORECORD = " & TSR_NUOVO
'            SETAConnection.Execute sql, n, adCmdText
''           2. MARCARE COME ELIMINATI SU CCT TUTTI I RECORD RIMASTI
'            Call AggiornaParametriSessioneSuRecord("PROTEZIONEPOSTO", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO)
'        Else
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato
            SETAConnection.Execute sql, n, adCmdText
            If n > 1 Then
                Call frmMessaggio.Visualizza("NotificaRimozioneProtezioni")
            Else
                Call frmMessaggio.Visualizza("NotificaMancataRimozioneProtezioni")
            End If
'        End If
    End If
End Sub

Private Function ElencoCausaliProtezioneSelezionate() As String
    Dim i As Integer
    Dim elenco As String
    
    elenco = "(" & listaCausaliProtezioneMasterSelezionate.Item(1)
    For i = 2 To listaCausaliProtezioneMasterSelezionate.count
        elenco = elenco & ", " & listaCausaliProtezioneMasterSelezionate.Item(i)
    Next i
    elenco = elenco & ")"
    ElencoCausaliProtezioneSelezionate = elenco
End Function

Private Sub Successivo()
    Call CaricaFormConfigurazioneProdottoPostiMigliori
End Sub

Private Sub CaricaFormConfigurazioneProdottoPostiMigliori()
    Call frmConfigurazioneProdottoPostiMigliori.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoPostiMigliori.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetRateo(rateo)
    Call frmConfigurazioneProdottoPostiMigliori.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoPostiMigliori.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoPostiMigliori.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoPostiMigliori.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoPostiMigliori.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoPostiMigliori.Init
End Sub

Private Sub SelezioneProtezioni_Init()
    selezionaPostiLiberiMaster = False
    selezionaPostiRiservatiMaster = False
    selezionaPostiVendutiMaster = False
    selezionaPostiLiberiCorrelati = False
    selezionaPostiRiservatiCorrelati = False
    selezionaPostiVendutiCorrelati = False
    cancellaTutteProtezioni = False
    cancellaSoloProtezioniCorrispondenti = False
    cancellaNessunaProtezione = False
    importaSoloAggiungendo = False
    importaSovrascrivendoCorrispondenti = False
    importaAggiungendoESovrascrivendo = False
    inserisciProtezioniSoloSuPostiLiberi = VB_FALSO
    Set listaCausaliProtezioneMasterSelezionate = Nothing
    Set listaCausaliProtezioneMasterSelezionate = New Collection
End Sub
    
Private Sub lstCausaliSuMaster_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaCausaleProtezione
    End If
End Sub

Private Sub SelezionaDeselezionaCausaleProtezione()
    Dim idCausale As Long
    Dim descrizioneCausale As String
    
    If lstCausaliSuMaster.ListIndex <> -1 Then
        idCausale = lstCausaliSuMaster.ItemData(lstCausaliSuMaster.ListIndex)
        descrizioneCausale = lstCausaliSuMaster.Text
        If lstCausaliSuMaster.Selected(lstCausaliSuMaster.ListIndex) Then
            Call listaCausaliProtezioneMasterSelezionate.Add(idCausale, ChiaveId(idCausale))
        Else
            Call listaCausaliProtezioneMasterSelezionate.Remove(ChiaveId(idCausale))
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optCancellaTutte_Click()
    If Not internalEvent Then
        cancellaTutteProtezioni = True
        cancellaSoloProtezioniCorrispondenti = False
        cancellaNessunaProtezione = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optCancellaSoloCorrispondenti_Click()
    If Not internalEvent Then
        cancellaTutteProtezioni = False
        cancellaSoloProtezioniCorrispondenti = True
        cancellaNessunaProtezione = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optCancellaNessuna_Click()
    If Not internalEvent Then
        cancellaTutteProtezioni = False
        cancellaSoloProtezioniCorrispondenti = False
        cancellaNessunaProtezione = True
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optImportaSoloAggiungendo_Click()
    If Not internalEvent Then
        importaSoloAggiungendo = True
        importaSovrascrivendoCorrispondenti = False
        importaAggiungendoESovrascrivendo = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optImportaSovrascrivendo_Click()
    If Not internalEvent Then
        importaSoloAggiungendo = False
        importaSovrascrivendoCorrispondenti = True
        importaAggiungendoESovrascrivendo = False
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optImportaAggiungendoESovrascrivendo_Click()
    If Not internalEvent Then
        importaSoloAggiungendo = False
        importaSovrascrivendoCorrispondenti = False
        importaAggiungendoESovrascrivendo = True
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Function SqlPostiConTitoliEmessi(idProdotto As Long, elencoTipiStatoTitolo As String, importaAncheDaCorrelati As Boolean) As String
    Dim sql As String
    
    If importaAncheDaCorrelati Then
        sql = "(SELECT DISTINCT T.IDPOSTO " & _
            " FROM TITOLO T, STATOTITOLO ST, " & _
            " (" & _
            " SELECT DISTINCT IDPRODOTTO FROM PRODOTTO_PRODOTTO PP, PRODOTTO P" & _
            " WHERE IDPRODOTTOB = IDPRODOTTO" & _
            " AND IDPRODOTTOA = " & idProdotto & _
            " AND  P.RATEO > 1" & _
            " UNION" & _
            " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDPRODOTTO = " & idProdotto & _
            " ) P" & _
            " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
            " AND ST.IDTIPOSTATOTITOLO IN " & elencoTipiStatoTitolo & _
            " AND T.IDPRODOTTO = P.IDPRODOTTO)"
    Else
        sql = "(SELECT T.IDPOSTO FROM TITOLO T, STATOTITOLO ST" & _
            " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
            " AND ST.IDTIPOSTATOTITOLO IN " & elencoTipiStatoTitolo & _
            " AND IDPRODOTTO = " & idProdotto & ")"
    End If
    SqlPostiConTitoliEmessi = sql
End Function

Private Function SqlPostiConProtezione(idProdotto As Long) As String
    SqlPostiConProtezione = _
        "(SELECT DISTINCT IDPOSTO FROM PROTEZIONEPOSTO" & _
        " WHERE IDPRODOTTO = " & idProdotto & ")"
End Function
''
''Private Sub InserisciProtezioni_old()
''    Dim sql As String
''    Dim n As Long
''    Dim i As Integer
''    Dim protezioniInserite As Long
''    Dim protezioniCancellate As Long
''
''    Call ApriConnessioneBD
''
''    n = 0
''    protezioniInserite = 0
''    protezioniCancellate = 0
''    sql = ""
'''--------------------------------------------------------------------------------------------------------------------------------------------------
''    If cancellaTutteProtezioni Then
''        sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato
''        SETAConnection.Execute sql, n, adCmdText
''        protezioniCancellate = protezioniCancellate + n
''    End If
'''--------------------------------------------------------------------------------------------------------------------------------------------------
''    If selezionaPostiLiberiMaster Or selezionaPostiLiberiCorrelati Then
''        If cancellaSoloProtezioniCorrispondenti Then
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
''                " AND T.IDPOSTO IS NULL" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''        End If
''        If importaSoloAggiungendo Then
''            'INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
''                    " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
''                    " AND TM.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
''                    " AND TS.IDPOSTO IS NULL " & _
''                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
''                    " AND PROT.IDPOSTO IS NULL " & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
''                    " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
''                    " AND TM.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
''                    " AND PROT.IDPOSTO IS NULL " & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        ElseIf importaSovrascrivendoCorrispondenti Then
''            'PRIMO: ELIMINAZIONE CORRISPONDENTI
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
''                " AND T.IDPOSTO IS NULL" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''            'SECONDO: INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
''                    " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
''                    " AND T1.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
''                    " AND T2.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = S.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
''                    " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
''                    " AND T.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = S.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        ElseIf importaAggiungendoESovrascrivendo Then
''            'PRIMO: ELIMINAZIONE CORRISPONDENTI
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
''                " AND T.IDPOSTO IS NULL" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''            'SECONDO: INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1, " & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
''                    " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
''                    " AND T1.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
''                    " AND T2.IDPOSTO IS NULL" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T" & _
''                    " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
''                    " AND T.IDPOSTO IS NULL" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        End If
''    End If
'''-------------------------------------------------------------------------------------------------------------------------------------------------
''    If selezionaPostiRiservatiMaster Or selezionaPostiRiservatiCorrelati Then
''        If cancellaSoloProtezioniCorrispondenti Then
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''        End If
''        If importaSoloAggiungendo Then
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
''                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
''                    " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
''                    " AND TS.IDPOSTO IS NULL " & _
''                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
''                    " AND PROT.IDPOSTO IS NULL " & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
''                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
''                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
''                    " AND PROT.IDPOSTO IS NULL " & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        ElseIf importaSovrascrivendoCorrispondenti Then
''            'PRIMO: ELIMINAZIONE CORRISPONDENTI
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''            'SECONDO: INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
''                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
''                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
''                    " AND T2.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = S.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
''                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                    " AND PP.IDPOSTO = S.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        ElseIf importaAggiungendoESovrascrivendo Then
''            'PRIMO: ELIMINAZIONE CORRISPONDENTI
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''            'SECONDO: INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1, " & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
''                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
''                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
''                    " AND T2.IDPOSTO IS NULL" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T" & _
''                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        End If
''    End If
'''----------------------------------------------------------------------------------------------------------------------------------------------------------
''    If selezionaPostiVendutiMaster Or selezionaPostiVendutiCorrelati Then
''        If cancellaSoloProtezioniCorrispondenti Then
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                " AND PP.IDPRODOTTO = " & idProdottoSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''        End If
''        If importaSoloAggiungendo Then
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
''                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
''                    " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
''                    " AND TS.IDPOSTO IS NULL " & _
''                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
''                    " AND PROT.IDPOSTO IS NULL " & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
''                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
''                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
''                    " AND PROT.IDPOSTO IS NULL " & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        ElseIf importaSovrascrivendoCorrispondenti Then
''            'PRIMO: ELIMINAZIONE CORRISPONDENTI
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''            'SECONDO: INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
''                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
''                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
''                    " AND T2.IDPOSTO IS NULL" & _
''                    " AND PP.IDPOSTO = S.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T," & _
''                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
''                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                    " AND PP.IDPOSTO = S.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        ElseIf importaAggiungendoESovrascrivendo Then
''            'PRIMO: ELIMINAZIONE CORRISPONDENTI
''            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPOSTO IN" & _
''                " (SELECT DISTINCT PP.IDPOSTO" & _
''                " FROM PROTEZIONEPOSTO PP, " & _
''                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
''                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")" & _
''                " AND IDPRODOTTO = " & idProdottoSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniCancellate = protezioniCancellate + n
''            'SECONDO: INSERIMENTO PROTEZIONI
''            If inserisciProtezioniSoloSuPostiLiberi Then
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1, " & _
''                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
''                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
''                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
''                    " AND T2.IDPOSTO IS NULL" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            Else
''                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
''                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
''                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
''                    " FROM PROTEZIONEPOSTO PP, " & _
''                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T" & _
''                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
''                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
''                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
''            End If
''            SETAConnection.Execute sql, n, adCmdText
''            protezioniInserite = protezioniInserite + n
''        End If
''    End If
'''-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
''    protezioniInserite = protezioniInserite - protezioniCancellate
'''    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'''        tipoStatoRecordSelezionato = TSR_NUOVO
'''        Call AggiornaParametriSessioneSuRecord("PROTEZIONEPOSTO", "IDPRODOTTO", idProdottoSelezionato, TSR_NUOVO)
'''    End If
''
''    If protezioniInserite > 0 Then
''        Call frmMessaggio.Visualizza("NotificaInserimentoProtezioni", protezioniInserite)
''    Else
''        Call frmMessaggio.Visualizza("NotificaMancatoInserimentoProtezioni")
''    End If
''
''    Call ChiudiConnessioneBD
''
''End Sub

Private Sub cmdSelezionaTutti_Click()
    Call SelezionaTutteLeCausaliProtezione
End Sub

Private Sub cmdDeselezionaTutti_Click()
    Call DeselezionaTutteLeCausaliProtezione
End Sub

Private Sub SelezionaTutteLeCausaliProtezione()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idCausale As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set listaCausaliProtezioneMasterSelezionate = Nothing
    Set listaCausaliProtezioneMasterSelezionate = New Collection
    For i = 1 To lstCausaliSuMaster.ListCount
        lstCausaliSuMaster.Selected(i - 1) = True
        idCausale = lstCausaliSuMaster.ItemData(i - 1)
        Call listaCausaliProtezioneMasterSelezionate.Add(idCausale, ChiaveId(idCausale))
    Next i
    
    internalEvent = internalEventOld
End Sub

Private Sub DeselezionaTutteLeCausaliProtezione()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idArea As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstCausaliSuMaster.ListCount
        lstCausaliSuMaster.Selected(i - 1) = False
    Next i
    Set listaCausaliProtezioneMasterSelezionate = Nothing
    Set listaCausaliProtezioneMasterSelezionate = New Collection
    
    internalEvent = internalEventOld
End Sub
'
'Private Function StatoAttualeProdottoSuSchemaProduzione() As TipoStatoProdottoEnum
'    Dim sql As String
'    Dim n As Long
'
'    '1. controllare che su SETA_INTEGRA lo stato sia 'In Configurazione' o 'Attivabile'
'    '2. se si verifica 1., porre lo stato 'In Configurazione'
'    '3. se non si verifica 1., mostrare un messaggio esplicativo.
'
'    Call ApriConnessioneBD
'
'    SETAConnection.BeginTrans
'    sql = "UPDATE " & nomeSchema & "PRODOTTO" & _
'        " SET IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE & _
'        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'        " AND IDTIPOSTATOPRODOTTO IN (" & TSP_IN_CONFIGURAZIONE & _
'        ", " & TSP_ATTIVABILE & ")"
'    SETAConnection.Execute sql, n, adCmdText
'    If n = 1 Then
'        SETAConnection.CommitTrans
'        StatoAttualeProdottoSuSchemaProduzione = TSP_IN_CONFIGURAZIONE
'    Else
'        SETAConnection.RollbackTrans
'        StatoAttualeProdottoSuSchemaProduzione = TSP_NON_SPECIFICATO
'    End If
'
'    Call ChiudiConnessioneBD
'
'End Function

Private Sub InserisciProtezioni()
    Dim sql As String
    Dim n As Long
    Dim i As Integer
    Dim condizioniSQL As String
    
    numeroProtezioniPreesistenti = NumeroProtezioniSuProdotto(idProdottoSelezionato)
    
    Call ApriConnessioneBD
    
    n = 0
    numeroProtezioniAggiunteESovrascritte = 0
    numeroProtezioniSoloAggiunte = 0
    numeroProtezioniSovrascritte = 0
    numeroProtezioniCancellate = 0
    sql = ""
    condizioniSQL = ""
'--------------------------------------------------------------------------------------------------------------------------------------------------
'NOTA: L'AGGIORNAMENTO DEI PARAMETRI DELLA SESSIONE A SEGUITO DELLE INSERT E' PROFONDAMENTE FUOURI
'STANDARD, CIOE' VIENE FATTA SENZA CHIAMARE LA FUNZIONE AGGIORNAPARAMETRI...; QUESTO PERCHE'
'IN QUESTO CASO LE QUERY SONO TROPPO COMPLICATE PER POTER PASSARE LE CONDIZIONI COME PARAMETRO
'AL METODO AGGIORNAPARAMETRI...
'NOTA2: ALLA FINE, PER UNIFORMITA', ANCHE LE DELETE VENGONO FATTE FUORI STANDARD, COSA DI PER SE'
'NON NECESSARIA MA CHE RISOLVE IL PROBLEMA DEI CONTEGGI DEI RECORD ELIMINATI
'--------------------------------------------------------------------------------------------------------------------------------------------------
    If cancellaTutteProtezioni Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND IDTIPOSTATORECORD = " & TSR_NUOVO
'            SETAConnection.Execute sql, n, adCmdText
'            numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            sql = "UPDATE PROTEZIONEPOSTO" & _
'                " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                condizioniSQL
'            SETAConnection.Execute sql, n, adCmdText
'            numeroProtezioniCancellate = numeroProtezioniCancellate + n
'        Else
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniCancellate = numeroProtezioniCancellate + n
'        End If
    End If
'--------------------------------------------------------------------------------------------------------------------------------------------------
    If selezionaPostiLiberiMaster Or selezionaPostiLiberiCorrelati Then
        If cancellaSoloProtezioniCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                " AND T.IDPOSTO IS NULL" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'                " AND IDPRODOTTO = " & idProdottoSelezionato
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO" & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
'                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
'                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO" & _
                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            End If
        End If
        If importaSoloAggiungendo Then
            'INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                condizioniSQL = ""
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
'                        " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
'                        " AND TM.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
'                        " AND TS.IDPOSTO IS NULL " & _
'                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
'                        " AND PROT.IDPOSTO IS NULL " & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
                        " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
                        " AND TM.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
                        " AND TS.IDPOSTO IS NULL " & _
                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                        " AND PROT.IDPOSTO IS NULL " & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
'                        " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
'                        " AND TM.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
'                        " AND PROT.IDPOSTO IS NULL " & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
                        " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
                        " AND TM.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                        " AND PROT.IDPOSTO IS NULL " & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSoloAggiunte = numeroProtezioniSoloAggiunte + n
        ElseIf importaSovrascrivendoCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                " AND T.IDPOSTO IS NULL" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
'                protezioniCancellate = protezioniCancellate + n
'            End If
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
'                        " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
'                        " AND T1.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
'                        " AND T2.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = S.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                        " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
                        " AND T1.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                        " AND T2.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = S.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
'                        " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
'                        " AND T.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = S.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                        " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                        " AND T.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = S.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSovrascritte = numeroProtezioniSovrascritte + n
        ElseIf importaAggiungendoESovrascrivendo Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                    " AND T.IDPOSTO IS NULL" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_ELIMINATO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
'                protezioniCancellate = protezioniCancellate + n
'            End If
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1, " & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
'                        " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
'                        " AND T1.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
'                        " AND T2.IDPOSTO IS NULL" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1, " & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                        " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
                        " AND T1.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                        " AND T2.IDPOSTO IS NULL" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T" & _
'                        " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
'                        " AND T.IDPOSTO IS NULL" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T" & _
                        " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                        " AND T.IDPOSTO IS NULL" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniAggiunteESovrascritte = numeroProtezioniAggiunteESovrascritte + n
        End If
    End If
'-------------------------------------------------------------------------------------------------------------------------------------------------
    If selezionaPostiRiservatiMaster Or selezionaPostiRiservatiCorrelati Then
        If cancellaSoloProtezioniCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
'                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
'                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            End If
        End If
        If importaSoloAggiungendo Then
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
'                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
'                        " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
'                        " AND TS.IDPOSTO IS NULL " & _
'                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
'                        " AND PROT.IDPOSTO IS NULL " & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                        " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
                        " AND TS.IDPOSTO IS NULL " & _
                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                        " AND PROT.IDPOSTO IS NULL " & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
'                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
'                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
'                        " AND PROT.IDPOSTO IS NULL " & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                        " AND PROT.IDPOSTO IS NULL " & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSoloAggiunte = numeroProtezioniSoloAggiunte + n
        ElseIf importaSovrascrivendoCorrispondenti Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
'                protezioniCancellate = protezioniCancellate + n
'            End If
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
'                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
'                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
'                        " AND T2.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = S.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                        " AND T2.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = S.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
'                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
'                        " AND PP.IDPOSTO = S.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                        " AND PP.IDPOSTO = S.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSovrascritte = numeroProtezioniSovrascritte + n
        ElseIf importaAggiungendoESovrascrivendo Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
'                protezioniCancellate = protezioniCancellate + n
'            End If
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1, " & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
'                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
'                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
'                        " AND T2.IDPOSTO IS NULL" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1, " & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                        " AND T2.IDPOSTO IS NULL" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T" & _
'                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T" & _
                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniAggiunteESovrascritte = numeroProtezioniAggiunteESovrascritte + n
        End If
    End If
'----------------------------------------------------------------------------------------------------------------------------------------------------------
    If selezionaPostiVendutiMaster Or selezionaPostiVendutiCorrelati Then
        If cancellaSoloProtezioniCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
'                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
'                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
                numeroProtezioniCancellate = numeroProtezioniCancellate + n
'            End If
        End If
        If importaSoloAggiungendo Then
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
'                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
'                        " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
'                        " AND TS.IDPOSTO IS NULL " & _
'                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
'                        " AND PROT.IDPOSTO IS NULL " & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                        " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
                        " AND TS.IDPOSTO IS NULL " & _
                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                        " AND PROT.IDPOSTO IS NULL " & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
'                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
'                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
'                        " AND PROT.IDPOSTO IS NULL " & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
                        " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                        " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                        " AND PROT.IDPOSTO IS NULL " & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSoloAggiunte = numeroProtezioniSoloAggiunte + n
        ElseIf importaSovrascrivendoCorrispondenti Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
'                protezioniCancellate = protezioniCancellate + n
'            End If
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
'                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
'                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
'                        " AND T2.IDPOSTO IS NULL" & _
'                        " AND PP.IDPOSTO = S.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                        " AND T2.IDPOSTO IS NULL" & _
                        " AND PP.IDPOSTO = S.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T," & _
'                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
'                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
'                        " AND PP.IDPOSTO = S.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T," & _
                        SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                        " AND PP.IDPOSTO = S.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSovrascritte = numeroProtezioniSovrascritte + n
        ElseIf importaAggiungendoESovrascrivendo Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN " & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    " AND IDTIPOSTATORECORD = " & TSR_NUOVO & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'                sql = "UPDATE PROTEZIONEPOSTO" & _
'                    " SET IDTIPOSTATORECORD = " & TSR_ELIMINATO & "," & _
'                    " IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'                    condizioniSQL
'                SETAConnection.Execute sql, n, adCmdText
''                protezioniCancellate = protezioniCancellate + n
'            Else
                sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
                SETAConnection.Execute sql, n, adCmdText
'                protezioniCancellate = protezioniCancellate + n
'            End If
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1, " & _
'                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
'                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
'                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
'                        " AND T2.IDPOSTO IS NULL" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1, " & _
                        SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                        " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                        " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                        " AND T2.IDPOSTO IS NULL" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            Else
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE," & _
'                        " IDSESSIONECONFIGURAZIONE, IDTIPOSTATORECORD)" & _
'                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
'                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE," & _
'                        idSessioneConfigurazioneCorrente & ", " & TSR_NUOVO & _
'                        " FROM PROTEZIONEPOSTO PP, " & _
'                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T" & _
'                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
'                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
'                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                Else
                    sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                        " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                        idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                        " FROM PROTEZIONEPOSTO PP, " & _
                        SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T" & _
                        " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                        " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                        " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
'                End If
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniAggiunteESovrascritte = numeroProtezioniAggiunteESovrascritte + n
        End If
    End If
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    Call VisualizzaReportInserimentoProtezioni
    
    Call ChiudiConnessioneBD
    
End Sub

Private Function NumeroProtezioniSuProdotto(idProdotto As Long) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(IDPROTEZIONEPOSTO) CONT" & _
        " FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    rec.MoveFirst
    NumeroProtezioniSuProdotto = CLng(rec("CONT").Value)
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Function

Private Sub visualizzaStatoProtezioni()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim listaDettagliProtezioni As Collection
    Dim elencoDettagliProtezioni As String
    Dim recordDettagli As String
    Dim numeroProtezioniTotali As Long
    
    Set listaDettagliProtezioni = New Collection
    sql = "SELECT DISTINCT SUP.CODICE ||' - '|| SUP.NOME SUPERAREA,"
    sql = sql & " A.CODICE ||' - '|| A.NOME AREA, "
    sql = sql & " CP.SIMBOLOSUPIANTA ||' - '|| CP.NOME SIMBOLO,"
    sql = sql & " STATOPOSTO.STATO STATO,"
    sql = sql & " COUNT(DISTINCT PP.IDPROTEZIONEPOSTO) PROTEZIONI"
    sql = sql & " FROM PROTEZIONEPOSTO PP, CAUSALEPROTEZIONE CP, AREA A, AREA SUP,"
    sql = sql & " ("
    sql = sql & " SELECT DISTINCT P.IDPOSTO IDPOSTO, P.IDAREA IDAREA, T.IDTITOLO IDTITOLO, ST.IDTIPOSTATOTITOLO IDTIPOSTATOTITOLO,"
    sql = sql & " DECODE (ST.IDTIPOSTATOTITOLO,1,'RISERVATO',2,'VENDUTO',3,'VENDUTO',4,'VENDUTO',5,'VENDUTO','LIBERO') STATO"
    sql = sql & " FROM AREA A, POSTO P, TITOLO T, STATOTITOLO ST "
    sql = sql & " WHERE T.IDPOSTO(+) = P.IDPOSTO"
    sql = sql & " AND P.IDAREA = A.IDAREA"
    sql = sql & " AND A.IDPIANTA = " & idPiantaSelezionata
    sql = sql & " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO(+)"
    sql = sql & " AND T.IDPRODOTTO(+) = " & idProdottoSelezionato
    sql = sql & " MINUS"
    sql = sql & " SELECT DISTINCT P.IDPOSTO IDPOSTO, P.IDAREA IDAREA, T.IDTITOLO IDTITOLO, ST.IDTIPOSTATOTITOLO IDTIPOSTATOTITOLO,"
    sql = sql & " DECODE (ST.IDTIPOSTATOTITOLO,1,'RISERVATO',2,'VENDUTO',3,'VENDUTO',4,'VENDUTO',5,'VENDUTO','LIBERO') STATO"
    sql = sql & " FROM AREA A, POSTO P, TITOLO T, STATOTITOLO ST,"
    sql = sql & " ("
    sql = sql & " SELECT T.IDPOSTO IDPOSTO,  COUNT(T.IDTITOLO) TITOLI"
    sql = sql & " FROM TIPOSTATOTITOLO TST, TITOLO T, STATOTITOLO ST "
    sql = sql & " WHERE ST.IDTIPOSTATOTITOLO = TST.IDTIPOSTATOTITOLO"
    sql = sql & " AND ST.IDSTATOTITOLO = T.IDSTATOTITOLOCORRENTE  "
    sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " GROUP BY IDPOSTO"
    sql = sql & " HAVING COUNT(T.IDTITOLO) > 1"
    sql = sql & " ) DOPPI"
    sql = sql & " WHERE T.IDPOSTO = P.IDPOSTO"
    sql = sql & " AND P.IDPOSTO = DOPPI.IDPOSTO"
    sql = sql & " AND P.IDAREA = A.IDAREA"
    sql = sql & " AND A.IDPIANTA = " & idPiantaSelezionata
    sql = sql & " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
    sql = sql & " AND ST.IDTIPOSTATOTITOLO = 6"
    sql = sql & " AND ST.IDTIPOSTATOTITOLO = 7"
    sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " ) STATOPOSTO"
    sql = sql & " WHERE PP.IDCAUSALEPROTEZIONE = CP.IDCAUSALEPROTEZIONE "
    sql = sql & " AND PP.IDPOSTO(+) = STATOPOSTO.IDPOSTO"
    sql = sql & " AND STATOPOSTO.IDAREA = A.IDAREA"
    sql = sql & " AND A.IDAREA_PADRE = SUP.IDAREA"
    sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " GROUP BY CP.SIMBOLOSUPIANTA ||' - '|| CP.NOME,"
    sql = sql & " SUP.CODICE ||' - '|| SUP.NOME,"
    sql = sql & " A.CODICE ||' - '|| A.NOME,"
    sql = sql & " STATOPOSTO.STATO"
    sql = sql & " ORDER BY SUPERAREA, AREA, SIMBOLO, STATOPOSTO.STATO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            recordDettagli = ""
            recordDettagli = recordDettagli & CStr(rec("SUPERAREA").Value) & "; "
            recordDettagli = recordDettagli & CStr(rec("AREA").Value) & "; "
            recordDettagli = recordDettagli & CStr(rec("SIMBOLO").Value) & "; "
            recordDettagli = recordDettagli & CStr(rec("STATO").Value) & "; "
            recordDettagli = recordDettagli & CStr(rec("PROTEZIONI").Value)
            Call listaDettagliProtezioni.Add(recordDettagli)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    numeroProtezioniTotali = NumeroProtezioniSuProdotto(idProdottoSelezionato)
    elencoDettagliProtezioni = ArgomentoMessaggio(listaDettagliProtezioni)
    Call frmMessaggio.Visualizza("VisualizzazioneStatoProtezioni", _
        nomeProdottoSelezionato, _
        numeroProtezioniTotali, _
        elencoDettagliProtezioni)
End Sub

Private Sub VisualizzaReportInserimentoProtezioni()
    Dim elencoDettagliProtezioni As String
    Dim numeroProtezioniTotali As Long

    numeroProtezioniTotali = NumeroProtezioniSuProdotto(idProdottoSelezionato)
    elencoDettagliProtezioni = ""
    Call frmMessaggio.Visualizza("NotificaInserimentoProtezioni", _
        nomeProdottoSelezionato, _
        numeroProtezioniPreesistenti, _
        numeroProtezioniCancellate, _
        numeroProtezioniSovrascritte, _
        numeroProtezioniSoloAggiunte, _
        numeroProtezioniAggiunteESovrascritte, _
        numeroProtezioniTotali)
End Sub

Private Sub InserisciProtezioni_nonTransazionale()
    Dim sql As String
    Dim n As Long
    Dim i As Integer
    Dim condizioniSQL As String
    
    numeroProtezioniPreesistenti = NumeroProtezioniSuProdotto(idProdottoSelezionato)
    
    Call ApriConnessioneBD
    
    n = 0
    numeroProtezioniAggiunteESovrascritte = 0
    numeroProtezioniSoloAggiunte = 0
    numeroProtezioniSovrascritte = 0
    numeroProtezioniCancellate = 0
    sql = ""
    condizioniSQL = ""
    
    If cancellaTutteProtezioni Then
        sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        numeroProtezioniCancellate = numeroProtezioniCancellate + n
    End If
'--------------------------------------------------------------------------------------------------------------------------------------------------
    If selezionaPostiLiberiMaster Or selezionaPostiLiberiCorrelati Then
        If cancellaSoloProtezioniCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                " AND T.IDPOSTO IS NULL" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
'                " AND IDPRODOTTO = " & idProdottoSelezionato
            sql = "DELETE FROM PROTEZIONEPOSTO" & _
                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniCancellate = numeroProtezioniCancellate + n
        End If
        If importaSoloAggiungendo Then
            'INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                condizioniSQL = ""
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
                    " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
                    " AND TM.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
                    " AND TS.IDPOSTO IS NULL " & _
                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                    " AND PROT.IDPOSTO IS NULL " & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " TM," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
                    " WHERE PP.IDPOSTO = TM.IDPOSTO(+)" & _
                    " AND TM.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                    " AND PROT.IDPOSTO IS NULL " & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSoloAggiunte = numeroProtezioniSoloAggiunte + n
        ElseIf importaSovrascrivendoCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                " AND T.IDPOSTO IS NULL" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                    " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
                    " AND T1.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                    " AND T2.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = S.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                    " AND T.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = S.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSovrascritte = numeroProtezioniSovrascritte + n
        ElseIf importaAggiungendoESovrascrivendo Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                    " AND T.IDPOSTO IS NULL" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T1, " & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                    " WHERE PP.IDPOSTO = T1.IDPOSTO(+)" & _
                    " AND T1.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                    " AND T2.IDPOSTO IS NULL" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1,2,3,4,5)", selezionaPostiLiberiCorrelati) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO(+)" & _
                    " AND T.IDPOSTO IS NULL" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniAggiunteESovrascritte = numeroProtezioniAggiunteESovrascritte + n
        End If
    End If
'-------------------------------------------------------------------------------------------------------------------------------------------------
    If selezionaPostiRiservatiMaster Or selezionaPostiRiservatiCorrelati Then
        If cancellaSoloProtezioniCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniCancellate = numeroProtezioniCancellate + n
        End If
        If importaSoloAggiungendo Then
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                    " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
                    " AND TS.IDPOSTO IS NULL " & _
                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                    " AND PROT.IDPOSTO IS NULL " & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " TM," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                    " AND PROT.IDPOSTO IS NULL " & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSoloAggiunte = numeroProtezioniSoloAggiunte + n
        ElseIf importaSovrascrivendoCorrispondenti Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                " (SELECT DISTINCT PP.IDPOSTO" & _
                " FROM PROTEZIONEPOSTO PP, " & _
                SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
                " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                    " AND T2.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = S.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDPOSTO = S.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSovrascritte = numeroProtezioniSovrascritte + n
        ElseIf importaAggiungendoESovrascrivendo Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T1, " & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                    " AND T2.IDPOSTO IS NULL" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(1)", selezionaPostiRiservatiCorrelati) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniAggiunteESovrascritte = numeroProtezioniAggiunteESovrascritte + n
        End If
    End If
'----------------------------------------------------------------------------------------------------------------------------------------------------------
    If selezionaPostiVendutiMaster Or selezionaPostiVendutiCorrelati Then
        If cancellaSoloProtezioniCorrispondenti Then
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniCancellate = numeroProtezioniCancellate + n
        End If
        If importaSoloAggiungendo Then
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT," & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " TS" & _
                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                    " AND PP.IDPOSTO = TS.IDPOSTO(+)" & _
                    " AND TS.IDPOSTO IS NULL " & _
                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                    " AND PROT.IDPOSTO IS NULL " & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " TM," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " PROT" & _
                    " WHERE PP.IDPOSTO = TM.IDPOSTO" & _
                    " AND PP.IDPOSTO = PROT.IDPOSTO(+)" & _
                    " AND PROT.IDPOSTO IS NULL " & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSoloAggiunte = numeroProtezioniSoloAggiunte + n
        ElseIf importaSovrascrivendoCorrispondenti Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN" & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                    " AND T2.IDPOSTO IS NULL" & _
                    " AND PP.IDPOSTO = S.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T," & _
                    SqlPostiConProtezione(idProdottoSelezionato) & " S" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDPOSTO = S.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniSovrascritte = numeroProtezioniSovrascritte + n
        ElseIf importaAggiungendoESovrascrivendo Then
            'PRIMO: ELIMINAZIONE CORRISPONDENTI
            condizioniSQL = " AND IDPOSTO IN " & _
                    " (SELECT DISTINCT PP.IDPOSTO" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", False) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato & ")"
            sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                condizioniSQL
            SETAConnection.Execute sql, n, adCmdText
            'SECONDO: INSERIMENTO PROTEZIONI
            If inserisciProtezioniSoloSuPostiLiberi Then
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T1, " & _
                    SqlPostiConTitoliEmessi(idProdottoSelezionato, "(1,2,3,4,5)", False) & " T2" & _
                    " WHERE PP.IDPOSTO = T1.IDPOSTO" & _
                    " AND PP.IDPOSTO = T2.IDPOSTO(+)" & _
                    " AND T2.IDPOSTO IS NULL" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            Else
                sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                    " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & _
                    idProdottoSelezionato & ", PP.IDCAUSALEPROTEZIONE" & _
                    " FROM PROTEZIONEPOSTO PP, " & _
                    SqlPostiConTitoliEmessi(idProdottoMasterSelezionato, "(2,3,4,5)", selezionaPostiVendutiCorrelati) & " T" & _
                    " WHERE PP.IDPOSTO = T.IDPOSTO" & _
                    " AND PP.IDCAUSALEPROTEZIONE IN " & ElencoCausaliProtezioneSelezionate & _
                    " AND PP.IDPRODOTTO = " & idProdottoMasterSelezionato
            End If
            SETAConnection.Execute sql, n, adCmdText
            numeroProtezioniAggiunteESovrascritte = numeroProtezioniAggiunteESovrascritte + n
        End If
    End If
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    Call VisualizzaReportInserimentoProtezioni
    
    Call ChiudiConnessioneBD
    
End Sub



