VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazionePacchettoOrdiniScelteProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lvwConfigurazioneOrdiniScelteProdotto 
      Height          =   3555
      Left            =   120
      TabIndex        =   14
      Top             =   660
      Width           =   11235
      _ExtentX        =   19817
      _ExtentY        =   6271
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.CommandButton cmdGiu 
      Height          =   855
      Left            =   11400
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2460
      Width           =   435
   End
   Begin VB.CommandButton cmdSu 
      Height          =   855
      Left            =   11400
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1560
      Width           =   435
   End
   Begin VB.CommandButton cmdUltimo 
      Height          =   855
      Left            =   11400
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3360
      Width           =   435
   End
   Begin VB.CommandButton cmdPrimo 
      Height          =   855
      Left            =   11400
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   660
      Width           =   435
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   10
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   9
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dell'Ordine delle scelte prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   6735
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   12
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazionePacchettoOrdiniScelteProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOffertaPacchettoSelezionata As Long
Private isRecordEditabile As Boolean
Private nomeOffertaPacchettoSelezionata As String
Private nomeTabellaTemporanea As String
Private listaAppoggioProdotto As Collection

Private tipoSpostamentoRecordInGriglia As TipoSpostamentoRecordInGrigliaEnum
Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pacchetto"
    txtInfo1.Text = nomeOffertaPacchettoSelezionata
    txtInfo1.Enabled = False
    
'    dgrConfigurazioneOrdiniScelteProdotto.Caption = "SCELTE PRODOTTO CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        cmdConferma.Enabled = True
        cmdAnnulla.Enabled = True
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioProdotti
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchettoSelezionata)
        Call EliminaTabellaAppoggioProdotti
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub CaricaFormConfigurazioneAreePacchetto()
    Call frmConfigurazionePacchettoAree.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoAree.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoAree.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoAree.SetNumeroProdottiAssociati(numeroProdottiAssociati)
    Call frmConfigurazionePacchettoAree.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoAree.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazionePacchettoAree.Init
End Sub

Public Sub SetIdOffertaPacchettoSelezionata(id As Long)
    idOffertaPacchettoSelezionata = id
End Sub

Public Sub SetNomeOffertaPacchettoSelezionata(nome As String)
    nomeOffertaPacchettoSelezionata = nome
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneAreePacchetto
    Call EliminaTabellaAppoggioProdotti
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim causaNonEditabilita As String

    stringaNota = "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    causaNonEditabilita = ""
    If isOffertaPacchettoBloccataDaUtente Then
        Call AggiornaNellaBaseDati
        Call ScriviLog(CCTA_MODIFICA, CCDA_OFFERTA_PACCHETTO, CCDA_AREA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call ListaScelteProdottoConfigurate_Init
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioProdotti
            Unload Me
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim l As ListView
    Dim i As listItem
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set l = lvwConfigurazioneOrdiniScelteProdotto
    If id <> idNessunElementoSelezionato Then
        Set i = l.ListItems(ChiaveId(id))
        i.Selected = True
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call GetIdRecordSelezionato
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdPrimo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    tipoSpostamentoRecordInGriglia = TSRIG_PRIMO
    Call SpostaRecordInGriglia
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSu_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    tipoSpostamentoRecordInGriglia = TSRIG_SU
    Call SpostaRecordInGriglia
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdGiu_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    tipoSpostamentoRecordInGriglia = TSRIG_GIU
    Call SpostaRecordInGriglia
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdUltimo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    tipoSpostamentoRecordInGriglia = TSRIG_ULTIMO
    Call SpostaRecordInGriglia
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Public Sub Init()
    tipoSpostamentoRecordInGriglia = TSRIG_NON_SPECIFICATO
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioProdotti
    Call lvwConfigurazioneOrdiniScelteProdotto_Init
    Call ListaScelteProdottoConfigurate_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim l As ListView
    Dim i As listItem
    
    Set l = lvwConfigurazioneOrdiniScelteProdotto
    Set i = l.selectedItem
    If Not (i Is Nothing) Then
        idRecordSelezionato = IdChiave(i.Key)
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub lvwConfigurazioneOrdiniScelteProdotto_Init()
    Dim l As ListView
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set l = lvwConfigurazioneOrdiniScelteProdotto
    l.View = lvwReport
    l.Gridlines = True
    l.HideColumnHeaders = False
    l.HideSelection = False
    l.FullRowSelect = True
    dimensioneGrid = l.Width - 100
    numeroCampi = 3
    l.ColumnHeaders.Add(1) = "Ordine"
    l.ColumnHeaders(1).Width = (dimensioneGrid / numeroCampi)
    l.ColumnHeaders.Add(2) = "Prodotti"
    l.ColumnHeaders(2).Width = (dimensioneGrid / numeroCampi)
    l.ColumnHeaders.Add(3) = "Selezionabili"
    l.ColumnHeaders(3).Width = (dimensioneGrid / numeroCampi)
    
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetTipoOffertaPacchettoSelezionata(tipo As TipoOffertaPacchettoEnum)
    tipoOffertaPacchettoSelezionata = tipo
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazionePacchettoScelteProdotto.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazionePacchettoScelteProdotto.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
    End Select
End Sub

Private Sub CreaTabellaAppoggioProdotti()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_PACCH_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDSCELTAPRODOTTO NUMBER(10), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggioProdotti
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioProdotti()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioProdotti()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSceltaProdotto As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioProdotto = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT IDSCELTAPRODOTTO"
'        sql = sql & " FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (IDTIPOSTATORECORD IS NULL"
'        sql = sql & " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = " SELECT IDSCELTAPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDSCELTAPRODOTTO").Value
            Call listaAppoggioProdotto.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioProdotto
        campoNome = ""
        sql = " SELECT DISTINCT P.IDPRODOTTO IDPRODOTTO, P.NOME NOMEPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, PRODOTTO P"
        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO "
        sql = sql & " AND SP.IDSCELTAPRODOTTO = " & recordTemporaneo.idElementoLista
        sql = sql & " ORDER BY IDPRODOTTO, NOMEPRODOTTO"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("NOMEPRODOTTO"), campoNome & "; " & rec("NOMEPRODOTTO"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioProdotto
        idSceltaProdotto = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = " INSERT INTO " & nomeTabellaTemporanea & " (IDSCELTAPRODOTTO, NOME)"
        sql = sql & " VALUES (" & idSceltaProdotto & ", "
        sql = sql & SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim l As ListView
    Dim i As listItem
    Dim j As Integer
    Dim n As Long

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    
    'VIENE CAMBIATO IL SEGNO ALL'ORDINE, SENNO' POI NON SARA' POSSIBILE FARE
    'GLI UPDATE PER VIA DEL VINCOLO DI UNICITA'
    sql = " UPDATE SCELTAPRODOTTO"
    sql = sql & " SET ORDINE = -ORDINE"
    sql = sql & " WHERE  IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    SETAConnection.Execute sql, n, adCmdText
    Set l = lvwConfigurazioneOrdiniScelteProdotto
    For j = 1 To l.ListItems.count
        Set i = l.ListItems(j)
        sql = " UPDATE SCELTAPRODOTTO"
        sql = sql & " SET ORDINE = " & i.Index
        sql = sql & " WHERE IDSCELTAPRODOTTO = " & IdChiave(i.Key)
        SETAConnection.Execute sql, n, adCmdText
    Next j
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD

    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub ListaScelteProdottoConfigurate_Init()
    Dim internalEventOld As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim l As ListView
    
    internalEventOld = internalEvent
    internalEvent = True

    Call PopolaTabellaAppoggioProdotti

    Set l = lvwConfigurazioneOrdiniScelteProdotto
    Call l.ListItems.Clear
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT S.IDSCELTAPRODOTTO ID,"
'        sql = sql & " S.ORDINE ""Ordine"", TMP.NOME ""Prodotti"","
'        sql = sql & " S.NUMEROPRODOTTISELEZIONABILI ""Selezionabili"""
'        sql = sql & " FROM SCELTAPRODOTTO S, " & nomeTabellaTemporanea & " TMP"
'        sql = sql & " WHERE S.IDSCELTAPRODOTTO = TMP.IDSCELTAPRODOTTO"
'        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (S.IDTIPOSTATORECORD IS NULL"
'        sql = sql & " OR S.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " ORDER BY ""Ordine"""
'    Else
        sql = "SELECT S.IDSCELTAPRODOTTO ID,"
        sql = sql & " S.ORDINE ""Ordine"", TMP.NOME ""Prodotti"","
        sql = sql & " S.NUMEROPRODOTTISELEZIONABILI ""Selezionabili"""
        sql = sql & " FROM SCELTAPRODOTTO S, " & nomeTabellaTemporanea & " TMP"
        sql = sql & " WHERE S.IDSCELTAPRODOTTO = TMP.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ORDER BY ""Ordine"""
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Dim i As listItem
            Set i = l.ListItems.Add(, ChiaveId(rec("ID")), rec("Ordine"))
            i.SubItems(1) = rec("Prodotti")
            i.SubItems(2) = rec("Selezionabili")
            
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lvwConfigurazioneOrdiniScelteProdotto_ItemClick(ByVal Item As MSComctlLib.listItem)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Private Sub SpostaRecordInGriglia()
    Dim l As ListView
    Dim i As listItem
    Dim ordine As Integer
    Dim prodotti As String
    Dim selezionabili As Integer
    Dim posizionePrecedente As Integer
    Dim posizioneSuccessiva As Integer
    
    Call Modifica
    Set l = lvwConfigurazioneOrdiniScelteProdotto
    Set i = l.selectedItem
    ordine = CInt(i.Text)
    prodotti = i.SubItems(1)
    selezionabili = i.SubItems(2)
    posizionePrecedente = i.Index
    posizioneSuccessiva = posizionePrecedente
    Select Case tipoSpostamentoRecordInGriglia
        Case TSRIG_PRIMO
            If posizionePrecedente > 1 Then posizioneSuccessiva = 1
        Case TSRIG_SU
            If posizionePrecedente > 1 Then posizioneSuccessiva = posizionePrecedente - 1
        Case TSRIG_GIU
            If posizionePrecedente < l.ListItems.count Then posizioneSuccessiva = posizionePrecedente + 1
        Case TSRIG_ULTIMO
            If posizionePrecedente < l.ListItems.count Then posizioneSuccessiva = l.ListItems.count
    End Select
    Call l.ListItems.Remove(posizionePrecedente)
    Set i = l.ListItems.Add(posizioneSuccessiva, ChiaveId(idRecordSelezionato), ordine)
    i.SubItems(1) = prodotti
    i.SubItems(2) = selezionabili
    i.Selected = True
End Sub


