VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmGestioneStatoProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gestione Stato Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   2
      Top             =   8160
      Width           =   1155
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   960
      Width           =   4095
   End
   Begin VB.CommandButton cmdConvalidaConfigura 
      Caption         =   "Convalida"
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   7560
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcProdotto 
      Height          =   375
      Left            =   4440
      Top             =   900
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrProdottiConfigurati 
      Height          =   6075
      Left            =   120
      TabIndex        =   3
      Top             =   1380
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10716
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Gestione dello stato del Prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   180
      Width           =   7335
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   1695
   End
End
Attribute VB_Name = "frmGestioneStatoProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long

Private internalEvent As Boolean
Private statoModificabile As ValoreBooleanoEnum

Private Sub cmbOrganizzazione_Click()
    Call cmbOrganizzazione_Update
End Sub

Private Sub cmbOrganizzazione_Update()
    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub AggiornaAbilitazioneControlli()

    dgrProdottiConfigurati.Columns(0).Visible = False
    dgrProdottiConfigurati.Caption = "PRODOTTI CONFIGURATI"
    cmdConvalidaConfigura.Enabled = False
    If idRecordSelezionato <> idNessunElementoSelezionato Then
        Select Case tipoStatoProdotto
            Case TSP_IN_CONFIGURAZIONE
'                cmdConvalidaConfigura.Visible = True
                cmdConvalidaConfigura.Caption = "Convalida"
                cmdConvalidaConfigura.Enabled = (statoModificabile = VB_VERO)
            Case TSP_ATTIVABILE
'                cmdConvalidaConfigura.Visible = True
                cmdConvalidaConfigura.Caption = "Configura"
                cmdConvalidaConfigura.Enabled = (statoModificabile = VB_VERO)
            Case TSP_ATTIVO
'                cmdConvalidaConfigura.Visible = False
                cmdConvalidaConfigura.Enabled = False
            Case TSP_ARCHIVIATO
'                cmdConvalidaConfigura.Visible = False
                cmdConvalidaConfigura.Enabled = False
            Case Else
        End Select
    Else
        cmdConvalidaConfigura.Enabled = False
    End If
End Sub

Private Sub AggiornaDataGrid()
    Call CaricaValoriGriglia
End Sub

Private Sub dgrProdottiConfigurati_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call GetStatoProdottoRecordSelezionato
        Call GetStatoModificabileProdottoRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrProdottiConfigurati, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub GetStatoProdottoRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        tipoStatoProdotto = rec("TIPOSTATOENUM")
    Else
        tipoStatoProdotto = TSP_NON_SPECIFICATO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub GetStatoModificabileProdottoRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        statoModificabile = IIf(rec("Stato modificabile") = "SI", VB_VERO, VB_FALSO)
    Else
        statoModificabile = VB_FALSO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init()
    Dim sql As String
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE ORDER BY NOME"
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    Call AggiornaAbilitazioneControlli
    idRecordSelezionato = idNessunElementoSelezionato
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcProdotto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrProdottiConfigurati_Init
End Sub

Private Sub adcProdotto_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Set d = adcProdotto
    
    Select Case idOrganizzazioneSelezionata
        Case idTuttiGliElementiSelezionati
'            sql = "SELECT P.IDPRODOTTO AS ""ID"", P.NOME AS ""Nome""," & _
'                " P.DESCRIZIONE AS ""Descrizione""," & _
'                " O.NOME AS ""Organizzazione""," & _
'                " O.IDORGANIZZAZIONE," & _
'                " TSP.NOME AS ""Stato""," & _
'                " P.IDTIPOSTATOPRODOTTO AS ""TIPOSTATOENUM""" & _
'                " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP, ORGANIZZAZIONE O WHERE" & _
'                " (P.IDTIPOSTATOPRODOTTO = TSP.IDTIPOSTATOPRODOTTO) AND" & _
'                " (P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE)" & _
'                " ORDER BY ""TIPOSTATOENUM"", ""Nome"""
            sql = "SELECT P.IDPRODOTTO ID, P.NOME ""Nome""," & _
                " P.DESCRIZIONE ""Descrizione""," & _
                " O.NOME ""Organizzazione""," & _
                " O.IDORGANIZZAZIONE," & _
                " TSP.NOME ""Stato""," & _
                " P.IDTIPOSTATOPRODOTTO TIPOSTATOENUM," & _
                " DECODE (P.IDTIPOSTATOPRODOTTO, 1, DECODE (P.IDSESSIONECONFIGURAZIONE, -1, 'SI', NULL, 'SI', 'NO')," & _
                " 2, DECODE (P.IDSESSIONECONFIGURAZIONE, " & idSessioneConfigurazioneCorrente & _
                " , 'SI', NULL, 'SI', 'NO'), 'NO'" & _
                " ) ""Stato modificabile""" & _
                " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP, ORGANIZZAZIONE O WHERE" & _
                " (P.IDTIPOSTATOPRODOTTO = TSP.IDTIPOSTATOPRODOTTO) AND" & _
                " (P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE) AND" & _
                " (P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")" & _
                " ORDER BY TIPOSTATOENUM, ""Nome"""
        Case Else
'            sql = "SELECT P.IDPRODOTTO AS ""ID"", P.NOME AS ""Nome""," & _
'                " P.DESCRIZIONE AS ""Descrizione""," & _
'                " O.NOME AS ""Organizzazione""," & _
'                " O.IDORGANIZZAZIONE," & _
'                " TSP.NOME AS ""Stato""," & _
'                " P.IDTIPOSTATOPRODOTTO AS ""TIPOSTATOENUM""" & _
'                " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP, ORGANIZZAZIONE O WHERE" & _
'                " (P.IDTIPOSTATOPRODOTTO = TSP.IDTIPOSTATOPRODOTTO) AND" & _
'                " (P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE) AND" & _
'                " (P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")" & _
'                " ORDER BY ""TIPOSTATOENUM"", ""Nome"""
            sql = "SELECT P.IDPRODOTTO ID, P.NOME ""Nome""," & _
                " P.DESCRIZIONE ""Descrizione"", O.NOME ""Organizzazione""," & _
                " O.IDORGANIZZAZIONE, TSP.NOME ""Stato"", P.IDTIPOSTATOPRODOTTO TIPOSTATOENUM," & _
                " DECODE (P.IDTIPOSTATOPRODOTTO, 1, DECODE (P.IDSESSIONECONFIGURAZIONE, -1, 'SI', NULL, 'SI', 'NO')," & _
                " 2, DECODE (P.IDSESSIONECONFIGURAZIONE, " & idSessioneConfigurazioneCorrente & _
                " , 'SI', NULL, 'SI', 'NO'), 'NO'" & _
                " ) ""Stato modificabile""" & _
                " FROM PRODOTTO P, TIPOSTATOPRODOTTO TSP, ORGANIZZAZIONE O" & _
                " WHERE P.IDTIPOSTATOPRODOTTO = TSP.IDTIPOSTATOPRODOTTO" & _
                " AND P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
                " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " ORDER BY TIPOSTATOENUM, ""Nome"""
    End Select
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrProdottiConfigurati.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrProdottiConfigurati_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrProdottiConfigurati
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Visible = False
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcProdotto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub cmdConvalidaConfigura_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConvalidaConfigura
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConvalidaConfigura()
    Dim ccAttivit� As CCTipoAttivit�Enum
    
'    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
'    If Not isProdottoBloccatoDaUtente Then
'        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
'    Else
        Call AggiornaStatoProdottoInBaseDati(ccAttivit�)
        Call ScriviLog(ccAttivit�, CCDA_PRODOTTO, , "IDPRODOTTO = " & idRecordSelezionato)
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
'        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
        Call AggiornaAbilitazioneControlli
'    End If
End Sub

Private Sub AggiornaStatoProdottoInBaseDati(ccAttivit� As CCTipoAttivit�Enum)
    Dim sql As String
    Dim n As Integer
    Dim m As Integer
    Dim internalEventOld As Boolean
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    If statoModificabile = VB_VERO Then
        Select Case tipoStatoProdotto
            Case TSP_IN_CONFIGURAZIONE
                If IsProdottoAttivabile(idRecordSelezionato, True) Then
                    n = 0
                    m = 0
                    sql = "UPDATE PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE & _
                        " WHERE IDPRODOTTO = " & idRecordSelezionato & _
                        " AND IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE
                    SETAConnection.Execute sql, n, adCmdText
                    sql = "UPDATE " & nomeSchema & "PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE & _
                        " WHERE IDPRODOTTO = " & idRecordSelezionato & _
                        " AND IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE
                    SETAConnection.Execute sql, m, adCmdText
                    If (n = 1) And (m = 1) Then
                        tipoStatoProdotto = TSP_ATTIVABILE
                        ccAttivit� = CCTA_CONVALIDA
                    End If
                End If
            Case TSP_ATTIVABILE
                    n = 0
                    m = 0
                sql = "UPDATE PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE & _
                    " WHERE IDPRODOTTO = " & idRecordSelezionato & _
                    " AND IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE
                SETAConnection.Execute sql, n, adCmdText
                sql = "UPDATE " & nomeSchema & "PRODOTTO SET IDTIPOSTATOPRODOTTO = " & TSP_IN_CONFIGURAZIONE & _
                    " WHERE IDPRODOTTO = " & idRecordSelezionato & _
                    " AND IDTIPOSTATOPRODOTTO = " & TSP_ATTIVABILE
                SETAConnection.Execute sql, m, adCmdText
                    If (n = 1) And (m = 1) Then
                    tipoStatoProdotto = TSP_IN_CONFIGURAZIONE
                    ccAttivit� = CCTA_CONFIGURAZIONE
                Else
                    tipoStatoProdotto = TSP_ATTIVO
                End If
            Case Else
                'Do Nothing
        End Select
    End If
    SETAConnection.CommitTrans
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call adcProdotto_Init
    Call dgrProdottiConfigurati_Init
    
    internalEvent = internalEventOld
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub



