VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmReportRimborsi 
   Caption         =   "Report per rimborsi"
   ClientHeight    =   8280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9300
   LinkTopic       =   "Form1"
   ScaleHeight     =   8280
   ScaleWidth      =   9300
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEsportaDettaglioOrdine 
      Caption         =   "Esporta dettaglio ordine"
      Height          =   375
      Left            =   5880
      TabIndex        =   16
      Top             =   4440
      Width           =   3015
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   465
      Left            =   8040
      TabIndex        =   12
      Top             =   7560
      Width           =   1140
   End
   Begin VB.Frame Frame2 
      Caption         =   "Report per data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   120
      TabIndex        =   7
      Top             =   3120
      Width           =   9015
      Begin VB.TextBox txtIdOrdine 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         MaxLength       =   30
         TabIndex        =   14
         Top             =   1440
         Width           =   2175
      End
      Begin VB.CommandButton cmdElencoOrdiniPerData 
         Caption         =   "Esporta report ordini"
         Height          =   375
         Left            =   5760
         TabIndex        =   13
         Top             =   720
         Width           =   3015
      End
      Begin MSComCtl2.DTPicker dtpDataFine 
         Height          =   315
         Left            =   960
         TabIndex        =   8
         Top             =   720
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   556
         _Version        =   393216
         Format          =   67108865
         CurrentDate     =   36526
      End
      Begin MSComCtl2.DTPicker dtpDataInizio 
         Height          =   315
         Left            =   960
         TabIndex        =   9
         Top             =   360
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   556
         _Version        =   393216
         Format          =   67108865
         CurrentDate     =   36526
      End
      Begin VB.Label lblDataOraTermineAnnullo 
         Caption         =   "idOrdine"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label lblInizio 
         Caption         =   "Inizio:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   420
         Width           =   495
      End
      Begin VB.Label lblTermine 
         Caption         =   "Fine:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   780
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Report per prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   9015
      Begin VB.ComboBox cmbProdotto 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1335
         Width           =   5580
      End
      Begin VB.CommandButton cmdEsporta 
         Caption         =   "Esporta report aggregato per ordine"
         Height          =   375
         Left            =   5760
         TabIndex        =   3
         Top             =   1350
         Width           =   3015
      End
      Begin VB.ComboBox cmbOrganizzazione 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   5580
      End
      Begin VB.CommandButton cmdEsportaDettaglio 
         Caption         =   "Esporta report dettaglio titoli"
         Height          =   375
         Left            =   5760
         TabIndex        =   1
         Top             =   1950
         Width           =   3015
      End
      Begin VB.Label lblOrganizzazione 
         Caption         =   "Organizzazioni"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1470
      End
      Begin VB.Label lblProdotto 
         Caption         =   "Prodotti"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   1110
         Width           =   2025
      End
   End
End
Attribute VB_Name = "frmReportRimborsi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long

Private Sub cmbOrganizzazione_Click()
    Call cmbProdotto_Update
End Sub

Private Sub cmbProdotto_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
'    sql = "SELECT P.idprodotto ID, P.idprodotto || ' - ' || P.NOME as NOME"
'    sql = sql & " FROM prodotto P"
'    sql = sql & " WHERE P.idtipostatoprodotto = 3"
'    sql = sql & " AND P.idorganizzazione = " & idOrganizzazioneSelezionata
'    sql = sql & " ORDER BY P.NOME, P.IDPRODOTTO ASC"
    sql = "SELECT P.idprodotto ID, P.idprodotto || ' - ' || P.NOME as NOME"
    sql = sql & " FROM prodotto P"
    sql = sql & " WHERE P.idorganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY P.IDPRODOTTO DESC"
    
    Call CaricaValoriCombo(cmbProdotto, sql, "NOME")
End Sub

Private Sub cmbProdotto_Click()
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
End Sub

Private Sub cmdElencoOrdiniPerData_Click()
    Call EsportaElencoOrdiniPerData
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Public Sub Init()
    Dim sql As String

    dtpDataInizio.Value = Now
    dtpDataFine.Value = Now
    
    sql = "SELECT O.idorganizzazione ID, O.nome NOME"
    sql = sql & " From organizzazione O, tipoStatoOrganizzazione TSO"
    sql = sql & " WHERE ((TSO.idtipostatoorganizzazione ="
    sql = sql & " O.idTipoStatoOrganizzazione"
    sql = sql & " )"
    sql = sql & " AND (TSO.idtipostatoorganizzazione = " & TSO_ATTIVA & ")"
    sql = sql & " )"
    sql = sql & " ORDER BY O.nome ASC"

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idProdottoSelezionato = idNessunElementoSelezionato
    
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    
    Call Me.Show(vbModal)
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
'    cmb.AddItem "<tutti>"
'    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub cmdEsporta_Click()
    Call EsportaDatiSuExcel
End Sub

Private Sub EsportaDatiSuExcel()
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim mousePointerOld As Integer
    Dim s As String
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    If idOrganizzazioneSelezionata = idNessunElementoSelezionato Then
        MsgBox "Nessuna organizzazione selezionata!"
    Else
    
        nomeFile = App.Path & "\" & idOrganizzazioneSelezionata & "-" & idProdottoSelezionato & "-ElencoPerRimborsi-"

        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        If idProdottoSelezionato = idNessunElementoSelezionato Then
            MsgBox "Nessun prodotto selezionato!"
        Else
            sql = "select pr.org, pr.nome prod, to_char(pr.dataorainizio, 'DD-MM-YYYY HH24:MI:SS') dataorainizio, pr.descrizionestampavenue luogo, o.idordine," & _
                " to_char(opz.dataora, 'DD-MM-YYYY HH24:MI:SS') dataora_operazione, opt.username operatore, pers.cognome cogn_acq, pers.nome nome_acq, pag.email, sped.indirizzo, sped.cap, c.nome comune, count(eo.idtitolo) qta_totale_titoli, tst.nome stato, count(st.idstatotitolo) qta_titoli_per_stato," & _
                " sum(ptp.importobase)/100 importo_base_titoli, sum(ptp.importoservizioprevendita)/100 importo_prev_titoli," & _
                " decode(sped.idspedizione, null, 0, 12.00) IMPORTO_SPED," & _
                " pag.importo/100 - sum(ptp.importobase+ptp.importoservizioprevendita)/100 - decode(sped.idspedizione, null, 0, 12.00) importo_commissioni, pag.importo/100 importo_pagato," & _
                " tsp.nome stato_pag" & _
                " from ordine o, operazione opz, elementoordine eo, titolo t, statotitolo st, tipostatotitolo tst, pagamento pag, seta_integra.tipostatopagamento tsp, prezzotitoloprodotto ptp," & _
                " operatore opt, spedizione sped, comune c, acquirente a, persona pers," & _
                " (" & _
                " select org.nome org, p.idprodotto, p.nome, p.descrizionestampavenue, min(dataorainizio) dataorainizio" & _
                " from organizzazione org, prodotto p, prodotto_rappresentazione pr, rappresentazione r" & _
                " Where org.idOrganizzazione = p.idOrganizzazione" & _
                " and p.idprodotto = pr.idprodotto" & _
                " and pr.idrappresentazione = r.idrappresentazione" & _
                " group by org.nome, p.idprodotto, p.nome, p.descrizionestampavenue" & _
                " ) pr"
            sql = sql & " Where o.idoperazionecreazione = opz.idoperazione" & _
                " and o.idordine = eo.idordine" & _
                " and eo.idtitolo = t.idtitolo(+)" & _
                " and t.idstatotitolocorrente = st.idstatotitolo(+)" & _
                " and st.idtipostatotitolo = tst.idtipostatotitolo(+)" & _
                " and st.idprezzotitoloprodotto = ptp.idprezzotitoloprodotto(+)" & _
                " and o.idordine = pag.idordine" & _
                " and pag.errore is null" & _
                " and pag.idtipostatopagamento = tsp.idtipostatopagamento" & _
                " and opz.idoperatore = opt.idoperatore" & _
                " and o.idacquirente = a.idacquirente(+)" & _
                " and a.idpersona = pers.idpersona(+)" & _
                " and o.idordine = sped.idordine(+)" & _
                " and sped.idcomune = c.idcomune(+)" & _
                " and t.idprodotto = pr.idprodotto" & _
                " and pr.idprodotto = " & idProdottoSelezionato & _
                " group by pr.org, pr.nome, pr.dataorainizio, pr.descrizionestampavenue, o.idordine, opz.dataora, opt.username, pers.cognome, pers.nome, pag.email, sped.indirizzo, sped.cap, c.nome, tst.nome, decode(sped.idspedizione, null, 0, 12.00), pag.importo, tsp.nome" & _
                " Having count(eo.idtitolo) > 0" & _
                " order by o.idordine"

            'Apro file xls
            Set xlsDoc = xlsApp.Workbooks.Add
            Set xlsSheet = xlsDoc.ActiveSheet
    
            xlsSheet.Rows.Cells(1, 1) = "ORG"
            xlsSheet.Rows.Cells(1, 2) = "PROD"
            xlsSheet.Rows.Cells(1, 3) = "DATAORAINIZIO"
            xlsSheet.Rows.Cells(1, 4) = "LUOGO"
            xlsSheet.Rows.Cells(1, 5) = "IDORDINE"
            xlsSheet.Rows.Cells(1, 6) = "DATAORA_OPERAZIONE"
            xlsSheet.Rows.Cells(1, 7) = "OPERATORE"
            xlsSheet.Rows.Cells(1, 8) = "COGN_ACQ"
            xlsSheet.Rows.Cells(1, 9) = "NOME_ACQ"
            xlsSheet.Rows.Cells(1, 10) = "EMAIL"
            xlsSheet.Rows.Cells(1, 11) = "INDIRIZZO"
            xlsSheet.Rows.Cells(1, 12) = "CAP"
            xlsSheet.Rows.Cells(1, 13) = "COMUNE"
            xlsSheet.Rows.Cells(1, 14) = "QTA_TOTALE_TITOLI"
            xlsSheet.Rows.Cells(1, 15) = "STATO"
            xlsSheet.Rows.Cells(1, 16) = "QTA_TITOLI_PER_STATO"
            xlsSheet.Rows.Cells(1, 17) = "IMPORTO_BASE_TITOLI"
            xlsSheet.Rows.Cells(1, 18) = "IMPORTO_PREV_TITOLI"
            xlsSheet.Rows.Cells(1, 19) = "IMPORTO_SPED"
            xlsSheet.Rows.Cells(1, 20) = "IMPORTO_COMMISSIONI"
            xlsSheet.Rows.Cells(1, 21) = "IMPORTO_PAGATO"
            xlsSheet.Rows.Cells(1, 22) = "STATO_PAG"
    
            Call ApriConnessioneBD
            rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                i = 2
                While Not rec.EOF
                    xlsSheet.Rows.Cells(i, 1) = rec("ORG")
                    xlsSheet.Rows.Cells(i, 2) = rec("PROD")
                    xlsSheet.Rows.Cells(i, 3) = rec("DATAORAINIZIO")
                    xlsSheet.Rows.Cells(i, 4) = rec("LUOGO")
                    xlsSheet.Rows.Cells(i, 5) = rec("IDORDINE")
                    xlsSheet.Rows.Cells(i, 6) = rec("DATAORA_OPERAZIONE")
                    xlsSheet.Rows.Cells(i, 7) = rec("OPERATORE")
                    xlsSheet.Rows.Cells(i, 8) = rec("COGN_ACQ")
                    xlsSheet.Rows.Cells(i, 9) = rec("NOME_ACQ")
                    xlsSheet.Rows.Cells(i, 10) = rec("EMAIL")
                    xlsSheet.Rows.Cells(i, 11) = rec("INDIRIZZO")
                    xlsSheet.Rows.Cells(i, 12) = rec("CAP")
                    xlsSheet.Rows.Cells(i, 13) = rec("COMUNE")
                    xlsSheet.Rows.Cells(i, 14) = rec("QTA_TOTALE_TITOLI")
                    xlsSheet.Rows.Cells(i, 15) = rec("STATO")
                    xlsSheet.Rows.Cells(i, 16) = rec("QTA_TITOLI_PER_STATO")
                    xlsSheet.Rows.Cells(i, 17) = rec("IMPORTO_BASE_TITOLI")
                    xlsSheet.Rows.Cells(i, 18) = rec("IMPORTO_PREV_TITOLI")
                    xlsSheet.Rows.Cells(i, 19) = rec("IMPORTO_SPED")
                    xlsSheet.Rows.Cells(i, 20) = rec("IMPORTO_COMMISSIONI")
                    xlsSheet.Rows.Cells(i, 21) = rec("IMPORTO_PAGATO")
                    xlsSheet.Rows.Cells(i, 22) = rec("STATO_PAG")
                    
                    i = i + 1
                    rec.MoveNext
                Wend
            End If
            rec.Close
            Call ChiudiConnessioneBD
    
            Call xlsDoc.SaveAs(nomeFile)
            Call xlsDoc.Close
            Call xlsApp.Quit
            Set xlsApp = Nothing
    
            MsgBox "Report salvato sul file: " & nomeFile
    
            MousePointer = mousePointerOld

        End If
    End If
    MousePointer = mousePointerOld

End Sub

Private Sub cmdEsportaDettaglio_Click()
    Call EsportaDatiDettaglioSuExcel
End Sub

Private Sub EsportaDatiDettaglioSuExcel()
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim mousePointerOld As Integer
    Dim s As String
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    If idOrganizzazioneSelezionata = idNessunElementoSelezionato Then
        MsgBox "Nessuna organizzazione selezionata!"
    Else
    
        nomeFile = App.Path & "\" & idOrganizzazioneSelezionata & "-" & idProdottoSelezionato & "-ElencoDettagliatoPerRimborsi-"

        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        If idProdottoSelezionato = idNessunElementoSelezionato Then
            MsgBox "Nessun prodotto selezionato!"
        Else
            sql = "select pr.org, pr.nome prod, to_char(pr.dataorainizio, 'DD-MM-YYYY HH24:MI:SS') dataorainizio, pr.descrizionestampavenue luogo, o.idordine," & _
                " to_char(opz.dataora, 'DD-MM-YYYY HH24:MI:SS') dataora_operazione, opt.username operatore, pers.cognome cogn_acq, pers.nome nome_acq, pag.email, sped.indirizzo, sped.cap, c.nome comune, tst.nome stato," & _
                " numerotipografico, identificativosupportodigitale, ptp.importobase/100 importo_base_titolo, ptp.importoservizioprevendita/100 importo_prev_titolo," & _
                " pag.importo/100 importo_pagato," & _
                " tsp.nome stato_pag" & _
                " from ordine o, operazione opz, elementoordine eo, titolo t, statotitolo st, supporto s, tipostatotitolo tst, pagamento pag, seta_integra.tipostatopagamento tsp, prezzotitoloprodotto ptp," & _
                " operatore opt, spedizione sped, comune c, acquirente a, persona pers," & _
                " (" & _
                " select org.nome org, p.idprodotto, p.nome, p.descrizionestampavenue, min(dataorainizio) dataorainizio" & _
                " from organizzazione org, prodotto p, prodotto_rappresentazione pr, rappresentazione r" & _
                " Where org.idOrganizzazione = p.idOrganizzazione" & _
                " and p.idprodotto = pr.idprodotto" & _
                " and pr.idrappresentazione = r.idrappresentazione" & _
                " group by org.nome, p.idprodotto, p.nome, p.descrizionestampavenue" & _
                " ) pr"
            sql = sql & " Where o.idoperazionecreazione = opz.idoperazione" & _
                " and o.idordine = eo.idordine" & _
                " and eo.idtitolo = t.idtitolo(+)" & _
                " and t.idstatotitolocorrente = st.idstatotitolo(+)" & _
                " and st.idtipostatotitolo = tst.idtipostatotitolo(+)" & _
                " and st.idprezzotitoloprodotto = ptp.idprezzotitoloprodotto(+)" & _
                " and st.idsupporto = s.idsupporto(+)" & _
                " and o.idordine = pag.idordine" & _
                " and pag.errore is null" & _
                " and pag.idtipostatopagamento = tsp.idtipostatopagamento" & _
                " and opz.idoperatore = opt.idoperatore" & _
                " and o.idacquirente = a.idacquirente(+)" & _
                " and a.idpersona = pers.idpersona(+)" & _
                " and o.idordine = sped.idordine(+)" & _
                " and sped.idcomune = c.idcomune(+)" & _
                " and t.idprodotto = pr.idprodotto" & _
                " and pr.idprodotto = " & idProdottoSelezionato & _
                " order by o.idordine, t.idtitolo"

            'Apro file xls
            Set xlsDoc = xlsApp.Workbooks.Add
            Set xlsSheet = xlsDoc.ActiveSheet
    
            xlsSheet.Rows.Cells(1, 1) = "ORG"
            xlsSheet.Rows.Cells(1, 2) = "PROD"
            xlsSheet.Rows.Cells(1, 3) = "DATAORAINIZIO"
            xlsSheet.Rows.Cells(1, 4) = "LUOGO"
            xlsSheet.Rows.Cells(1, 5) = "IDORDINE"
            xlsSheet.Rows.Cells(1, 6) = "DATAORA_OPERAZIONE"
            xlsSheet.Rows.Cells(1, 7) = "OPERATORE"
            xlsSheet.Rows.Cells(1, 8) = "COGN_ACQ"
            xlsSheet.Rows.Cells(1, 9) = "NOME_ACQ"
            xlsSheet.Rows.Cells(1, 10) = "EMAIL"
            xlsSheet.Rows.Cells(1, 11) = "INDIRIZZO"
            xlsSheet.Rows.Cells(1, 12) = "CAP"
            xlsSheet.Rows.Cells(1, 13) = "COMUNE"
            xlsSheet.Rows.Cells(1, 14) = "STATO"
            xlsSheet.Rows.Cells(1, 15) = "NUMEROTIPOGRAFICO"
            xlsSheet.Rows.Cells(1, 16) = "IDENTIFICATIVOSUPPORTODIGITALE"
            xlsSheet.Rows.Cells(1, 17) = "IMPORTO_BASE_TITOLO"
            xlsSheet.Rows.Cells(1, 18) = "IMPORTO_PREV_TITOLO"
            xlsSheet.Rows.Cells(1, 19) = "IMPORTO_PAGATO"
            xlsSheet.Rows.Cells(1, 20) = "STATO_PAG"
    
            Call ApriConnessioneBD
            rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                i = 2
                While Not rec.EOF
                    xlsSheet.Rows.Cells(i, 1) = rec("ORG")
                    xlsSheet.Rows.Cells(i, 2) = rec("PROD")
                    xlsSheet.Rows.Cells(i, 3) = rec("DATAORAINIZIO")
                    xlsSheet.Rows.Cells(i, 4) = rec("LUOGO")
                    xlsSheet.Rows.Cells(i, 5) = rec("IDORDINE")
                    xlsSheet.Rows.Cells(i, 6) = rec("DATAORA_OPERAZIONE")
                    xlsSheet.Rows.Cells(i, 7) = rec("OPERATORE")
                    xlsSheet.Rows.Cells(i, 8) = rec("COGN_ACQ")
                    xlsSheet.Rows.Cells(i, 9) = rec("NOME_ACQ")
                    xlsSheet.Rows.Cells(i, 10) = rec("EMAIL")
                    xlsSheet.Rows.Cells(i, 11) = rec("INDIRIZZO")
                    xlsSheet.Rows.Cells(i, 12) = rec("CAP")
                    xlsSheet.Rows.Cells(i, 13) = rec("COMUNE")
                    xlsSheet.Rows.Cells(i, 14) = rec("STATO")
                    xlsSheet.Rows.Cells(i, 15) = rec("NUMEROTIPOGRAFICO")
                    xlsSheet.Rows.Cells(i, 16) = rec("IDENTIFICATIVOSUPPORTODIGITALE")
                    xlsSheet.Rows.Cells(i, 17) = rec("IMPORTO_BASE_TITOLO")
                    xlsSheet.Rows.Cells(i, 18) = rec("IMPORTO_PREV_TITOLO")
                    xlsSheet.Rows.Cells(i, 19) = rec("IMPORTO_PAGATO")
                    xlsSheet.Rows.Cells(i, 20) = rec("STATO_PAG")
                    
                    i = i + 1
                    rec.MoveNext
                Wend
            End If
            rec.Close
            Call ChiudiConnessioneBD
    
            Call xlsDoc.SaveAs(nomeFile)
            Call xlsDoc.Close
            Call xlsApp.Quit
            Set xlsApp = Nothing
    
            MsgBox "Report salvato sul file: " & nomeFile
    
            MousePointer = mousePointerOld

        End If
    End If
    MousePointer = mousePointerOld

End Sub

Private Sub EsportaElencoOrdiniPerData()
    Dim dataInizio As Date
    Dim dataFine As Date
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim mousePointerOld As Integer
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    dataInizio = FormatDateTime(dtpDataInizio.Value, vbShortDate)
    dataFine = FormatDateTime(dtpDataFine.Value, vbShortDate)
    
    nomeFile = App.Path & "\" & "OrdiniPerRimborsi-" & Year(Now) & Month(Now) & Day(Now) & Hour(Now) & Minute(Now)

    nomeFile = nomeFile & _
        CompletaStringaConZeri(CStr(Year(Now)), 2) & _
        CompletaStringaConZeri(CStr(Month(Now)), 2) & _
        CompletaStringaConZeri(CStr(Day(Now)), 2) & _
        CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
        CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
        CompletaStringaConZeri(CStr(Second(Now)), 2)
    
    sql = "SELECT O.IDORDINE, TO_CHAR(OPZ.DATAORA, 'DD-MM-YYYY HH24:MI:SS') DATAORA, OPT.USERNAME, COUNT(EO.IDELEMENTOORDINE) QTAELEM, COUNT(EO.IDTITOLO) QTATITOLI, TST.NOME STATO, COUNT(ST.IDSTATOTITOLO) QTATITOLIINSTATO," & _
        " SUM(PTP.IMPORTOBASE+PTP.IMPORTOSERVIZIOPREVENDITA) IMPORTOTITOLI, PG.IMPORTO IMPORTOPAGATO, TSP.NOME STATOPAG" & _
        " FROM ORDINE O, OPERAZIONE OPZ, OPERATORE OPT, PAGAMENTO PG, TIPOSTATOPAGAMENTO TSP, ELEMENTOORDINE EO, TITOLO T, STATOTITOLO ST, TIPOSTATOTITOLO TST, PREZZOTITOLOPRODOTTO PTP" & _
        " WHERE O.IDOPERAZIONECREAZIONE = OPZ.IDOPERAZIONE" & _
        " AND DATAORA BETWEEN TO_DATE('" & Day(dataInizio) & "-" & Month(dataInizio) & "-" & Year(dataInizio) & " 00:00:00', 'DD-MM-YYYY HH24:MI:SS') AND TO_DATE('" & Day(dataInizio) & "-" & Month(dataInizio) & "-" & Year(dataInizio) & " 23:59:59', 'DD-MM-YYYY HH24:MI:SS')" & _
        " AND OPZ.IDOPERATORE = OPT.IDOPERATORE" & _
        " AND OPT.IDOPERATORE IN (" & _
        " SELECT DISTINCT IDOPERATORE" & _
        " FROM OPERATORE OPT, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP" & _
        " WHERE OPT.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA" & _
        " AND OCP.IDCLASSEPUNTOVENDITA IN (6,7,8)" & _
        " )" & _
        " AND O.IDORDINE = PG.IDORDINE(+)" & _
        " AND PG.IDTIPOSTATOPAGAMENTO = TSP.IDTIPOSTATOPAGAMENTO(+)" & _
        " AND PG.ERRORE(+) IS NULL" & _
        " AND O.IDORDINE = EO.IDORDINE(+)" & _
        " AND EO.IDTITOLO = T.IDTITOLO(+)" & _
        " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO(+)" & _
        " AND ST.IDTIPOSTATOTITOLO = TST.IDTIPOSTATOTITOLO(+)" & _
        " AND ST.IDPREZZOTITOLOPRODOTTO = PTP.IDPREZZOTITOLOPRODOTTO(+)" & _
        " GROUP BY O.IDORDINE, OPZ.DATAORA, OPT.USERNAME, TST.NOME, PG.IMPORTO, TSP.NOME" & _
        " ORDER BY O.IDORDINE"
    
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet

        xlsSheet.Rows.Cells(1, 1) = "IDORDINE"
        xlsSheet.Rows.Cells(1, 2) = "DATAORA"
        xlsSheet.Rows.Cells(1, 3) = "USERNAME"
        xlsSheet.Rows.Cells(1, 4) = "QTAELEM"
        xlsSheet.Rows.Cells(1, 5) = "QTATITOLI"
        xlsSheet.Rows.Cells(1, 6) = "STATO"
        xlsSheet.Rows.Cells(1, 7) = "QTATITOLIINSTATO"
        xlsSheet.Rows.Cells(1, 8) = "IMPORTOTITOLI"
        xlsSheet.Rows.Cells(1, 9) = "IMPORTOPAGATO"
        xlsSheet.Rows.Cells(1, 10) = "STATOPAG"

        Call ApriConnessioneBD
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 2
            While Not rec.EOF
                xlsSheet.Rows.Cells(i, 1) = rec("IDORDINE")
                xlsSheet.Rows.Cells(i, 2) = rec("DATAORA")
                xlsSheet.Rows.Cells(i, 3) = rec("USERNAME")
                xlsSheet.Rows.Cells(i, 4) = rec("QTAELEM")
                xlsSheet.Rows.Cells(i, 5) = rec("QTATITOLI")
                xlsSheet.Rows.Cells(i, 6) = rec("STATO")
                xlsSheet.Rows.Cells(i, 7) = rec("QTATITOLIINSTATO")
                xlsSheet.Rows.Cells(i, 8) = rec("IMPORTOTITOLI")
                xlsSheet.Rows.Cells(i, 9) = rec("IMPORTOPAGATO")
                xlsSheet.Rows.Cells(i, 10) = rec("STATOPAG")
                
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing

        MsgBox "Report salvato sul file: " & nomeFile

        MousePointer = mousePointerOld
            
End Sub

Private Sub cmdEsportaDettaglioOrdine_Click()
    Call EsportaDettaglioOrdine
End Sub

Private Sub EsportaDettaglioOrdine()
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim mousePointerOld As Integer
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    nomeFile = App.Path & "\" & "DettaglioOrdine-" & txtIdOrdine.Text

        sql = "SELECT O.IDORDINE, PG.IMPORTO, T.IDTITOLO, PR.NOME PROD, PR.IDORGANIZZAZIONE, TO_CHAR(DATAORA, 'DD-MM-YYYY HH24:MI:SS') DATAORA, TST.NOME STATO, PTP.IMPORTOBASE, PTP.IMPORTOSERVIZIOPREVENDITA" & _
        " FROM ORDINE O, OPERAZIONE OPZ, PAGAMENTO PG, TIPOSTATOPAGAMENTO TSP, ELEMENTOORDINE EO, TITOLO T, STATOTITOLO ST, TIPOSTATOTITOLO TST, PREZZOTITOLOPRODOTTO PTP, PRODOTTO PR" & _
        " WHERE O.IDORDINE = " & txtIdOrdine.Text & _
        " AND O.IDORDINE = PG.IDORDINE(+)" & _
        " AND PG.IDTIPOSTATOPAGAMENTO = TSP.IDTIPOSTATOPAGAMENTO(+)" & _
        " AND PG.ERRORE(+) IS NULL" & _
        " AND O.IDORDINE = EO.IDORDINE(+)" & _
        " AND EO.IDTITOLO = T.IDTITOLO(+)" & _
        " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO(+)" & _
        " AND ST.IDTIPOSTATOTITOLO = TST.IDTIPOSTATOTITOLO(+)" & _
        " AND ST.IDPREZZOTITOLOPRODOTTO = PTP.IDPREZZOTITOLOPRODOTTO(+)" & _
        " AND ST.IDOPERAZIONE = OPZ.IDOPERAZIONE(+)" & _
        " AND T.IDPRODOTTO = PR.IDPRODOTTO(+)" & _
        " ORDER BY O.IDORDINE, T.IDTITOLO"
    
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet

        xlsSheet.Rows.Cells(1, 1) = "IDORDINE"
        xlsSheet.Rows.Cells(1, 2) = "IMPORTO_PAG"
        xlsSheet.Rows.Cells(1, 3) = "IDTITOLO"
        xlsSheet.Rows.Cells(1, 4) = "PROD"
        xlsSheet.Rows.Cells(1, 5) = "IDORGANIZZAZIONE"
        xlsSheet.Rows.Cells(1, 6) = "DATAORA"
        xlsSheet.Rows.Cells(1, 7) = "STATO"
        xlsSheet.Rows.Cells(1, 8) = "IMPORTOBASE"
        xlsSheet.Rows.Cells(1, 9) = "IMPORTOSERVIZIOPREVENDITA"

        Call ApriConnessioneBD
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 2
            While Not rec.EOF
                xlsSheet.Rows.Cells(i, 1) = rec("IDORDINE")
                xlsSheet.Rows.Cells(i, 2) = rec("IMPORTO")
                xlsSheet.Rows.Cells(i, 3) = rec("IDTITOLO")
                xlsSheet.Rows.Cells(i, 4) = rec("PROD")
                xlsSheet.Rows.Cells(i, 5) = rec("IDORGANIZZAZIONE")
                xlsSheet.Rows.Cells(i, 6) = rec("DATAORA")
                xlsSheet.Rows.Cells(i, 7) = rec("STATO")
                xlsSheet.Rows.Cells(i, 8) = rec("IMPORTOBASE")
                xlsSheet.Rows.Cells(i, 9) = rec("IMPORTOSERVIZIOPREVENDITA")
                
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing

        MsgBox "Report salvato sul file: " & nomeFile

        MousePointer = mousePointerOld
            
End Sub


