VERSION 5.00
Begin VB.Form frmPresaInCaricoProdottiTDL 
   Caption         =   "Presa in carico prodotti TDL"
   ClientHeight    =   10200
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15360
   LinkTopic       =   "Form1"
   ScaleHeight     =   10200
   ScaleWidth      =   15360
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstProdottiTDLDaAttivare 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   8
      Top             =   5280
      Width           =   7335
   End
   Begin VB.CommandButton cmdAttivaProdottiTDL 
      Caption         =   "Attiva"
      Height          =   435
      Left            =   5640
      TabIndex        =   7
      Top             =   8880
      Width           =   1755
   End
   Begin VB.ListBox lstProdottiTDLPresiInCarico 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8250
      Left            =   7560
      TabIndex        =   6
      Top             =   1080
      Width           =   7695
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   7080
      TabIndex        =   4
      Top             =   9600
      Width           =   1155
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Prendi in carico"
      Height          =   435
      Left            =   5640
      TabIndex        =   3
      Top             =   4440
      Width           =   1755
   End
   Begin VB.ListBox lstProdottiTDL 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3180
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   1080
      Width           =   7335
   End
   Begin VB.Label Label2 
      Caption         =   "Prodotti TDL presi in carico da attivare"
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   4920
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "Prodotti TDL presi in carico negli ultimi due giorni"
      Height          =   195
      Left            =   7560
      TabIndex        =   5
      Top             =   780
      Width           =   4215
   End
   Begin VB.Label lblProdottiNonScaduti 
      Caption         =   "Prodotti TDL da prendere in carico"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   780
      Width           =   3255
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Presa in carico prodotti TDL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   7275
   End
End
Attribute VB_Name = "frmPresaInCaricoProdottiTDL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean
Private gestioneExitCode As ExitCodeEnum
Private listaProdottiTDL As Collection
Private listaProdottiTDLDaAttivare As Collection
Private listaProdottiTDLPresiInCarico As Collection
Private listaIdProdottiTDLSelezionati As Collection
Private listaIdProdottiTDLDaAttivareSelezionati As Collection

Public Sub Init()
    ResettaControlli
    CaricaValoriListaProdottiTDL
    CaricaValoriListaProdottiTDLDaAttivare
    CaricaValoriListaProdottiTDLPresiInCarico
    Call Me.Show(vbModal)
End Sub

Private Sub ResettaControlli()
    ListaProdottiTDL_Reset
    ListaProdottiTDLDaAttivare_Reset
    ListaProdottiTDLPresiInCarico_Reset
End Sub

Private Sub ListaProdottiTDL_Reset()
    Set listaIdProdottiTDLSelezionati = Nothing
    Set listaIdProdottiTDLSelezionati = New Collection
    lstProdottiTDL.Clear
End Sub

Private Sub ListaProdottiTDLDaAttivare_Reset()
    Set listaIdProdottiTDLDaAttivareSelezionati = Nothing
    Set listaIdProdottiTDLDaAttivareSelezionati = New Collection
    lstProdottiTDLDaAttivare.Clear
End Sub

Private Sub ListaProdottiTDLPresiInCarico_Reset()
    lstProdottiTDLPresiInCarico.Clear
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub lstProdottiTDL_ItemCheck(Item As Integer)
    Dim internalEventOld As Boolean
    
    If Not internalEvent Then
        internalEventOld = internalEvent
        internalEvent = True
        
        If lstProdottiTDL.Selected(Item) Then
            Call listaIdProdottiTDLSelezionati.Add(lstProdottiTDL.ItemData(Item), ChiaveId(CLng(Item)))
        Else
            Call listaIdProdottiTDLSelezionati.Remove(ChiaveId(CLng(Item)))
        End If
        
        internalEvent = internalEventOld
    End If
End Sub

Private Sub lstProdottiTDLDaAttivare_ItemCheck(Item As Integer)
    Dim internalEventOld As Boolean
    
    If Not internalEvent Then
        internalEventOld = internalEvent
        internalEvent = True
        
        If lstProdottiTDLDaAttivare.Selected(Item) Then
            Call listaIdProdottiTDLDaAttivareSelezionati.Add(lstProdottiTDLDaAttivare.ItemData(Item), ChiaveId(CLng(Item)))
        Else
            Call listaIdProdottiTDLDaAttivareSelezionati.Remove(ChiaveId(CLng(Item)))
        End If
        
        internalEvent = internalEventOld
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    ResettaControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim sql As String
    Dim stringaIdProdottiTDLSelezionati As String
    Dim n As Integer

On Error GoTo gestioneErrori

    stringaIdProdottiTDLSelezionati = CollezioneToStringa(listaIdProdottiTDLSelezionati, ",")

    If stringaIdProdottiTDLSelezionati <> "" Then
        sql = "UPDATE TDLPRODOTTO SET DATAORAPRESAINCARICO = SYSDATE, TERMINALEPRESAINCARICO = '" & UCase(getNomeMacchina()) & "' WHERE IDPRODOTTO IN (" & stringaIdProdottiTDLSelezionati & ")"
    
        Call ApriConnessioneBD
        SETAConnection.Execute sql, n, adCmdText
        Call ChiudiConnessioneBD
        
        MsgBox n & " prodotti TDL sono stati presi in carico"
        
        ResettaControlli
        CaricaValoriListaProdottiTDL
        CaricaValoriListaProdottiTDLDaAttivare
        CaricaValoriListaProdottiTDLPresiInCarico
    Else
        MsgBox "Nessun prodotto selezionato!"
    End If
    Exit Sub
    
gestioneErrori:
    MsgBox "Errore: " & Err.Description
    
End Sub

Private Sub CaricaValoriListaProdottiTDL()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim prodottoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaProdottiTDL = New Collection
    
    sql = "SELECT DISTINCT P.IDPRODOTTO, O.IDORGANIZZAZIONE || '-'  || O.NOME || ' - ' || P.IDPRODOTTO || '-' || P.NOME || ' - ' || DATAORAINIZIO DESCR" & _
            " FROM TDLPRODOTTO T, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, ORGANIZZAZIONE O, TARIFFA TF, PREZZOTITOLOPRODOTTO PTP" & _
            " WHERE T.IDPRODOTTO = P.IDPRODOTTO" & _
            " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
            " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
            " AND T.DATAORAPRESAINCARICO IS NULL" & _
            " AND P.IDTIPOSTATOPRODOTTO IN (1, 2)" & _
            " AND P.IDPRODOTTO = TF.IDPRODOTTO" & _
            " AND TF.IDTARIFFA = PTP.IDTARIFFA" & _
            " ORDER BY DESCR"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodottoCorrente = New clsElementoLista
            prodottoCorrente.nomeElementoLista = rec("DESCR")
            prodottoCorrente.idElementoLista = rec("IDPRODOTTO").Value
            prodottoCorrente.descrizioneElementoLista = rec("DESCR")
            chiaveTipoSupporto = ChiaveId(prodottoCorrente.idElementoLista)
            Call listaProdottiTDL.Add(prodottoCorrente, chiaveTipoSupporto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Call LstProdottiTDL_Init
    
End Sub

Private Sub CaricaValoriListaProdottiTDLDaAttivare()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim prodottoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaProdottiTDLDaAttivare = New Collection
    
    sql = "SELECT P.IDPRODOTTO, O.IDORGANIZZAZIONE || '-'  || O.NOME || ' - ' || P.IDPRODOTTO || '-' || P.NOME || ' - ' || DATAORAINIZIO DESCR" & _
            " FROM TDLPRODOTTO T, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, ORGANIZZAZIONE O" & _
            " WHERE T.IDPRODOTTO = P.IDPRODOTTO" & _
            " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
            " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
            " AND T.DATAORAPRESAINCARICO IS NOT NULL" & _
            " AND T.TERMINALEPRESAINCARICO = '" & UCase(getNomeMacchina()) & "'" & _
            " AND P.IDTIPOSTATOPRODOTTO IN (1, 2)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodottoCorrente = New clsElementoLista
            prodottoCorrente.nomeElementoLista = rec("DESCR")
            prodottoCorrente.idElementoLista = rec("IDPRODOTTO").Value
            prodottoCorrente.descrizioneElementoLista = rec("DESCR")
            chiaveTipoSupporto = ChiaveId(prodottoCorrente.idElementoLista)
            Call listaProdottiTDLDaAttivare.Add(prodottoCorrente, chiaveTipoSupporto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Call LstProdottiTDLDaAttivare_Init
    
End Sub

Private Sub CaricaValoriListaProdottiTDLPresiInCarico()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim prodottoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    sql = "SELECT P.IDPRODOTTO, O.NOME || ' - ' || P.NOME || ' - ' || DATAORAINIZIO || ' - ' || TERMINALEPRESAINCARICO || ' - ' || DATAORAPRESAINCARICO DESCR" & _
            " FROM TDLPRODOTTO T, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, ORGANIZZAZIONE O" & _
            " WHERE T.IDPRODOTTO = P.IDPRODOTTO" & _
            " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
            " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
            " AND T.DATAORAPRESAINCARICO > SYSDATE - 2"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            lstProdottiTDLPresiInCarico.AddItem rec("DESCR")
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub LstProdottiTDL_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstProdottiTDL.Clear

    If Not (listaProdottiTDL Is Nothing) Then
        i = 1
        For Each tip In listaProdottiTDL
            lstProdottiTDL.AddItem tip.descrizioneElementoLista
            lstProdottiTDL.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub LstProdottiTDLDaAttivare_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstProdottiTDLDaAttivare.Clear

    If Not (listaProdottiTDLDaAttivare Is Nothing) Then
        i = 1
        For Each tip In listaProdottiTDLDaAttivare
            lstProdottiTDLDaAttivare.AddItem tip.descrizioneElementoLista
            lstProdottiTDLDaAttivare.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdAttivaProdottiTDL_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call AttivaProdottiTDL
    
    MousePointer = mousePointerOld
End Sub

Private Sub AttivaProdottiTDL()
    Dim sql As String
    Dim stringaIdProdottiTDLDaAttivareSelezionati As String
    Dim n As Integer

On Error GoTo gestioneErrori

    stringaIdProdottiTDLDaAttivareSelezionati = CollezioneToStringa(listaIdProdottiTDLDaAttivareSelezionati, ",")

    If stringaIdProdottiTDLDaAttivareSelezionati <> "" Then
        sql = "UPDATE PRODOTTO SET IDTIPOSTATOPRODOTTO = 3 WHERE IDPRODOTTO IN (" & stringaIdProdottiTDLDaAttivareSelezionati & ")"
    
        Call ApriConnessioneBD
        SETAConnection.Execute sql, n, adCmdText
        Call ChiudiConnessioneBD
        
        MsgBox n & " prodotti TDL selezionati sono stati attivati"
        
        ResettaControlli
        CaricaValoriListaProdottiTDL
        CaricaValoriListaProdottiTDLDaAttivare
        CaricaValoriListaProdottiTDLPresiInCarico
    Else
        MsgBox "Nessun prodotto selezionato!"
    End If
    Exit Sub
    
gestioneErrori:
    MsgBox "Errore: " & Err.Description
    
End Sub

