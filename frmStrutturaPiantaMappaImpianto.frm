VERSION 5.00
Begin VB.Form frmStrutturaPiantaMappaImpianto 
   Caption         =   "Collegamento a mappa impianto"
   ClientHeight    =   4545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   ScaleHeight     =   4545
   ScaleWidth      =   8895
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdElimina 
      Caption         =   "Elimina mappa configurata"
      Height          =   495
      Left            =   5280
      TabIndex        =   7
      Top             =   3000
      Width           =   1575
   End
   Begin VB.ComboBox cmbImpianto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1140
      Width           =   4995
   End
   Begin VB.ComboBox cmbMappa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1815
      Width           =   4980
   End
   Begin VB.CommandButton cmdConfermaSuperarea 
      Caption         =   "Conferma"
      Height          =   495
      Left            =   3480
      TabIndex        =   1
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Esci"
      Height          =   495
      Left            =   4320
      TabIndex        =   0
      Top             =   3720
      Width           =   1575
   End
   Begin VB.Label lblAvanzamento 
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   2400
      Width           =   4935
   End
   Begin VB.Label lblImpianto 
      Caption         =   "Impianto"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   840
      Width           =   1470
   End
   Begin VB.Label lblMappa 
      Caption         =   "Mappa"
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   1590
      Width           =   2025
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione della mappa impianto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   4995
   End
End
Attribute VB_Name = "frmStrutturaPiantaMappaImpianto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idPiantaSelezionata As Long
Private nomePiantaSelezionata As String

Private idImpiantoSelezionato As Long
Private idMappaSelezionata As Long

Private gestioneExitCode As ExitCodeEnum
Private gestioneSuperarea As AzioneSuGrigliaEnum
Private gestioneArea As AzioneSuGrigliaEnum
Private tipoArea As TipoAreaEnum

Private Sub AggiornaAbilitazioneControlli()
    
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetNomePiantaSelezionata(nome As String)
    nomePiantaSelezionata = nome
End Sub

Public Sub Init()
    Call initComboImpianti
    Call Me.Show(vbModal)
End Sub

Private Sub cmdAnnulla_Click()
    Unload Me
End Sub

Private Sub conferma()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    Dim nSuperaree As Long
    Dim nAree As Long
    Dim nPosti As Long
    Dim idNuovaSuperarea As Long
    Dim rec As OraDynaset
    Dim sqlUpdate As String

On Error GoTo ConfermaStrutturaPiantaMappaImpianto_errore
    idNuovaSuperarea = OttieniIdentificatoreDaSequenza("SQ_AREA")

    Call ApriConnessioneBD_ORA

    ORADB.BeginTrans

' questa � la query giusta: per il momento devo aggiornare anche l'impianto per non modificare il war
' a regime il cmapo pianta.idimpianto va cancellato
    sql = "UPDATE PIANTA SET IDMAPPAIMPIANTO = " & idMappaSelezionata & " WHERE IDPIANTA = " & idPiantaSelezionata
    sql = "UPDATE PIANTA SET IDIMPIANTO = " & idImpiantoSelezionato & ", IDMAPPAIMPIANTO = " & idMappaSelezionata & " WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)

    sql = "UPDATE AREA SET IDSUPERAREAIMPIANTO = NULL, IDSETTOREIMPIANTO = NULL" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)
    
    sql = "UPDATE AREA A SET IDSUPERAREAIMPIANTO = (" & _
            " SELECT IDSUPERAREAIMPIANTO FROM SUPERAREAIMPIANTO SI WHERE A.NOME = SI.NOME AND IDMAPPAIMPIANTO = " & idMappaSelezionata & _
            " )" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA = 5"
    nSuperaree = ORADB.ExecuteSQL(sql)
    
    sql = "UPDATE AREA A SET IDSETTOREIMPIANTO = (" & _
            " SELECT IDSETTOREIMPIANTO FROM SETTOREIMPIANTO SI WHERE A.NOME = SI.NOME AND IDIMPIANTO = " & idImpiantoSelezionato & _
            " )" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA = 2"
    nAree = ORADB.ExecuteSQL(sql)
    
    ' questa query e' troppo pesante
'    sql = "UPDATE POSTO POS SET IDPOSTOIMPIANTO = (" & _
'            " SELECT PI.IDPOSTOIMPIANTO" & _
'            " FROM SETTOREIMPIANTO SI, AREA A, POSTOIMPIANTO PI, POSTO P" & _
'            " WHERE SI.IDIMPIANTO = " & idImpiantoSelezionato & _
'            " AND A.IDPIANTA = " & idPiantaSelezionata & _
'            " AND SI.IDSETTOREIMPIANTO = A.IDSETTOREIMPIANTO" & _
'            " AND A.IDAREA = P.IDAREA" & _
'            " AND PI.IDSETTOREIMPIANTO = SI.IDSETTOREIMPIANTO" & _
'            " AND P.NOMEFILA = PI.NOMEFILA" & _
'            " AND P.NOMEPOSTO = PI.NOMEPOSTO" & _
'            " AND POS.IDPOSTO = P.IDPOSTO" & _
'            " )" & _
'            " WHERE POS.IDPOSTO IN" & _
'            " (SELECT IDPOSTO FROM POSTO P, AREA A WHERE A.IDPIANTA = " & idPiantaSelezionata & " AND A.IDAREA = P.IDAREA)"
'    nPosti = ORADB.ExecuteSQL(sql)

    sql = "UPDATE POSTO SET IDPOSTOIMPIANTO = NULL" & _
        " WHERE IDPOSTO IN (" & _
        " SELECT IDPOSTO" & _
        " FROM POSTO P, AREA A" & _
        " WHERE A.IDPIANTA = " & idPiantaSelezionata & _
        " AND A.IDAREA = P.IDAREA" & _
        ")"
    nPosti = ORADB.ExecuteSQL(sql)
    
    sql = "SELECT IDPOSTO, NOMEFILA, NOMEPOSTO, IDSETTOREIMPIANTO" & _
            " FROM POSTO P, AREA A" & _
            " WHERE A.IDPIANTA = " & idPiantaSelezionata & _
            " AND A.IDAREA = P.IDAREA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lblAvanzamento.Caption = "Aggiornamento posto " & i & " di " & nPosti & "..."
            DoEvents
            If Not IsNull(rec("IDSETTOREIMPIANTO")) Then
                sqlUpdate = "UPDATE POSTO POS SET IDPOSTOIMPIANTO = (" & _
                            " SELECT PI.IDPOSTOIMPIANTO" & _
                            " FROM SETTOREIMPIANTO SI, POSTOIMPIANTO PI" & _
                            " WHERE SI.IDIMPIANTO = " & idImpiantoSelezionato & _
                            " AND SI.IDSETTOREIMPIANTO = " & rec("IDSETTOREIMPIANTO") & _
                            " AND PI.IDSETTOREIMPIANTO = SI.IDSETTOREIMPIANTO" & _
                            " AND PI.NOMEFILA = '" & rec("NOMEFILA") & "'" & _
                            " AND PI.NOMEPOSTO = '" & rec("NOMEPOSTO") & "'" & _
                            ")" & _
                            " WHERE IDPOSTO = " & rec("IDPOSTO")
                n = ORADB.ExecuteSQL(sqlUpdate)
            End If

            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close

    MsgBox "Pianta aggiornata: " & nSuperaree & " superaree, " & nAree & " aree, " & nPosti & " posti aggiornati."
    ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA
    Exit Sub

ConfermaStrutturaPiantaMappaImpianto_errore:
    ORADB.Rollback
    MsgBox "Si � verificato un errore: operazione non eseguita."
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub Elimina()
    Dim sql As String
    Dim n As Long
    Dim nSuperaree As Long
    Dim nAree As Long
    Dim nPosti As Long
    Dim idNuovaSuperarea As Long

On Error GoTo ConfermaStrutturaPiantaMappaImpianto_errore
    idNuovaSuperarea = OttieniIdentificatoreDaSequenza("SQ_AREA")

    Call ApriConnessioneBD_ORA

    ORADB.BeginTrans

    sql = "UPDATE PIANTA SET IDMAPPAIMPIANTO = NULL WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)

    sql = "UPDATE AREA SET IDSUPERAREAIMPIANTO = NULL WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA = 5"
    nSuperaree = ORADB.ExecuteSQL(sql)
    
    sql = "UPDATE AREA SET IDSETTOREIMPIANTO = NULL WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA = 2"
    nAree = ORADB.ExecuteSQL(sql)
    
    sql = "UPDATE POSTO POS SET IDPOSTOIMPIANTO = NULL" & _
            " WHERE POS.IDPOSTO IN" & _
            " (SELECT IDPOSTO FROM POSTO P, AREA A WHERE A.IDPIANTA = " & idPiantaSelezionata & " AND A.IDAREA = P.IDAREA)"
    nPosti = ORADB.ExecuteSQL(sql)

    MsgBox "Pianta aggiornata: " & nSuperaree & " superaree, " & nAree & " aree, " & nPosti & " posti aggiornati."
    ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA
    Exit Sub

ConfermaStrutturaPiantaMappaImpianto_errore:
    MsgBox "Si � verificato un errore: operazione non eseguita."
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub initComboImpianti()
    Dim sql As String

    idImpiantoSelezionato = idNessunElementoSelezionato
    
    sql = "SELECT IDIMPIANTO ID, NOME" & _
            " FROM IMPIANTO" & _
            " ORDER BY NOME"
    
    Call CaricaValoriCombo2(cmbImpianto, sql, "NOME", False)
End Sub

Private Sub cmbImpianto_Click()
    Call impianto_Update
End Sub

Private Sub impianto_Update()
    Dim sql As String

    idImpiantoSelezionato = cmbImpianto.ItemData(cmbImpianto.ListIndex)
    
    sql = "SELECT MI.IDMAPPAIMPIANTO ID, MI.NOME" & _
            " FROM MAPPAIMPIANTO MI, IMPIANTO I" & _
            " WHERE I.IDIMPIANTO = " & idImpiantoSelezionato & _
            " AND I.IDIMPIANTO = MI.IDIMPIANTO" & _
            " ORDER BY MI.NOME"
    
    Call CaricaValoriCombo2(cmbMappa, sql, "NOME", False)
End Sub

Private Sub cmbMappa_Click()
    Call mappa_Update
End Sub

Private Sub mappa_Update()
    idMappaSelezionata = cmbMappa.ItemData(cmbMappa.ListIndex)
End Sub

Private Sub cmdConfermaSuperarea_Click()
    Call conferma
End Sub

Private Sub cmdElimina_Click()
    Call Elimina
End Sub

