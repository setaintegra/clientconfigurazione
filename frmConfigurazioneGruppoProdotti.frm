VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazioneGruppoProdotti 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gruppo prodotti"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12360
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   12360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCaricaProdottiSlave 
      Caption         =   "carica prodotti slave"
      Height          =   315
      Left            =   120
      TabIndex        =   13
      Top             =   7200
      Width           =   5775
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   10
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   11040
      TabIndex        =   9
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraSlave 
      Caption         =   "Prodotti slave (nome - data ora prima rappresentazione - stato)"
      Height          =   6975
      Left            =   6000
      TabIndex        =   7
      Top             =   780
      Width           =   6195
      Begin VB.CommandButton cmdDeselezionaTutti 
         Caption         =   "deseleziona tutti"
         Height          =   315
         Left            =   3120
         TabIndex        =   15
         Top             =   6240
         Width           =   2895
      End
      Begin VB.CommandButton cmdSelezionaTutti 
         Caption         =   "seleziona tutti"
         Height          =   315
         Left            =   240
         TabIndex        =   14
         Top             =   6240
         Width           =   2895
      End
      Begin VB.ListBox lstProdottiSlave 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5820
         Left            =   240
         Style           =   1  'Checkbox
         TabIndex        =   8
         Top             =   420
         Width           =   5775
      End
      Begin VB.Label lblNota 
         Caption         =   "Nota: il simbolo (Š) indica che il prodotto č bloccato da un altro utente"
         Height          =   195
         Left            =   240
         TabIndex        =   16
         Top             =   6660
         Width           =   5775
      End
   End
   Begin VB.Frame fraMaster 
      Caption         =   "Prodotto master"
      Height          =   1395
      Left            =   120
      TabIndex        =   2
      Top             =   780
      Width           =   5775
      Begin VB.ComboBox cmbProdottoMaster 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   840
         Width           =   4305
      End
      Begin VB.ComboBox cmbOrganizzazioneMaster 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmConfigurazioneGruppoProdotti.frx":0000
         Left            =   1320
         List            =   "frmConfigurazioneGruppoProdotti.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   360
         Width           =   4305
      End
      Begin VB.Label lblProdottoMaster 
         Alignment       =   1  'Right Justify
         Caption         =   "Prodotto"
         Height          =   195
         Left            =   540
         TabIndex        =   6
         Top             =   900
         Width           =   675
      End
      Begin VB.Label lblOrganizzazioneMaster 
         Alignment       =   1  'Right Justify
         Caption         =   "Organizzazione"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   420
         Width           =   1095
      End
   End
   Begin MSComctlLib.TreeView tvwCaratteristiche 
      Height          =   4875
      Left            =   120
      TabIndex        =   0
      Top             =   2340
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   8599
      _Version        =   393217
      Indentation     =   706
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione del gruppo di prodotti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmConfigurazioneGruppoProdotti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrganizzazioneMaster As Long
Private idProdottoMaster As Long
Private nomeProdottoMaster As String
Private listaProdottiSlave As Collection
Private listaProdottiSlaveSelezionati As Collection
Private listaCaratteristicheSelezionate As Collection
Private numeroCaratteristicheSelezionate As Integer
Private prodottiDefinitiSuStessaPianta As Boolean
Private listaIdTariffeCorrispondentiATariffaMaster As Collection
Private listaCaratteristicheNonAggiornate As Collection
Private listaProdottiBloccatiDaAltroUtente As Collection
Private listaProdottiAttivi As Collection
Private listaProdottiAttivabili As Collection
Private listaProdottiInAltriStati As Collection

Private Const codiceRadice As String = "R0"
Private Const codiceAttributiStandard As String = "C1"
Private Const codiceContratto As String = "C1-1"
Private Const codiceAliquotaIVA As String = "C1-2"
Private Const codiceRateo As String = "C1-3"
Private Const codicePropagazProtezAiProdCorrelati As String = "C1-4"
Private Const codiceClasseProdotto As String = "C1-5"
Private Const codiceManifestazione As String = "C1-6"
Private Const codiceDurateRiservazione As String = "C2"
Private Const codiceRiservazioneWeb As String = "C2-1"
Private Const codiceRiservazioneWebProrogata As String = "C2-2"
Private Const codiceTariffe As String = "C3"
Private Const codicePrezzi As String = "C4"
Private Const codiceAssociazioneTipiLayoutSupporti As String = "C5"
Private Const codiceClassiDiUtenza As String = "C6"
Private Const codiceDirittiOperatoreSuTipiOperazione As String = "C7"
'Private Const codiceDirittiOperatoreSenzaEliminazioneSuSlave As String = "C6-1"
'Private Const codiceDirittiOperatoreConEliminazioneSuSlave As String = "C6-2"

Private internalEvent As Boolean
Private isRecordEditabile As Boolean
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private exitCode As ExitCodeEnum

Public Sub Init()
    Dim sql As String
    
    Call Variabili_Init
    Call tvwCaratteristiche_Init
    sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " WHERE IDTIPOSTATOORGANIZZAZIONE = 3" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbOrganizzazioneMaster, sql, "NOME", False)
    Call AggiornaAbilitazioneControlli
    Me.Show (vbModal)
End Sub

Private Sub Variabili_Init()
    idOrganizzazioneMaster = idNessunElementoSelezionato
    idProdottoMaster = idNessunElementoSelezionato
    nomeProdottoMaster = ""
    numeroCaratteristicheSelezionate = 0
    prodottiDefinitiSuStessaPianta = False
End Sub

Private Sub cmbOrganizzazioneMaster_Click()
    If Not internalEvent Then
        Call cmbOrganizzazioneMaster_Update
    End If
End Sub

Private Sub cmbOrganizzazioneMaster_Update()
    Dim sql As String
    
    idOrganizzazioneMaster = cmbOrganizzazioneMaster.ItemData(cmbOrganizzazioneMaster.ListIndex)
    Call cmbProdottoMaster.Clear
    sql = "SELECT IDPRODOTTO ID, NOME"
    sql = sql & " FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneMaster
    sql = sql & " ORDER BY NOME"
    Call CaricaValoriCombo(cmbProdottoMaster, sql, "NOME", True)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmbProdottoMaster.Enabled = (idOrganizzazioneMaster <> idNessunElementoSelezionato)
    lblProdottoMaster.Enabled = (idOrganizzazioneMaster <> idNessunElementoSelezionato)
    If Not (listaProdottiSlaveSelezionati Is Nothing) Then
        cmdConferma.Enabled = (numeroCaratteristicheSelezionate > 0)
    Else
        cmdConferma.Enabled = False
    End If
    tvwCaratteristiche.Enabled = (idProdottoMaster <> idNessunElementoSelezionato)
    cmdCaricaProdottiSlave.Enabled = (numeroCaratteristicheSelezionate > 0)
    lstProdottiSlave.Enabled = (numeroCaratteristicheSelezionate > 0)
    fraSlave.Enabled = (numeroCaratteristicheSelezionate > 0)
    lblNota.Visible = (numeroCaratteristicheSelezionate > 0)
    cmdSelezionaTutti.Enabled = (numeroCaratteristicheSelezionate > 0)
    cmdDeselezionaTutti.Enabled = (numeroCaratteristicheSelezionate > 0)
End Sub

Private Sub cmbProdottoMaster_Click()
    If Not internalEvent Then
        Call cmbProdottoMaster_Update
    End If
End Sub

Private Sub cmbProdottoMaster_Update()
    idProdottoMaster = cmbProdottoMaster.ItemData(cmbProdottoMaster.ListIndex)
    nomeProdottoMaster = cmbProdottoMaster.Text
    Call tvwCaratteristiche_Update
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub tvwCaratteristiche_Update()
    tvwCaratteristiche.Nodes(codiceRadice).Text = nomeProdottoMaster
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Call PulisciValoriCampi
End Sub

Private Sub PulisciValoriCampi()
    Dim internalEventOld As Boolean
    Dim nodoStart As Node
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call Variabili_Init
    Set listaProdottiSlave = Nothing
    Set listaProdottiSlaveSelezionati = Nothing
    Set listaCaratteristicheSelezionate = Nothing
    Set listaIdTariffeCorrispondentiATariffaMaster = Nothing
    
    Set nodoStart = tvwCaratteristiche.Nodes(codiceRadice)
    Set listaCaratteristicheSelezionate = New Collection
    Call DeselezionaTutteCaratteristiche(nodoStart)
    
    Call SelezionaElementoSuCombo(cmbOrganizzazioneMaster, idNessunElementoSelezionato)
    Call cmbProdottoMaster.Clear
    Call SelezionaElementoSuCombo(cmbProdottoMaster, idNessunElementoSelezionato)
    Call lstProdottiSlave.Clear
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub DeselezionaTutteCaratteristiche(nodoStart As Node)
    Dim nodoCorrente As Node
    Dim nodoSuccessivo As Node
    Dim caratteristicaAggiunta As clsElementoLista
    
    If Not nodoStart Is Nothing Then
        Set nodoCorrente = nodoStart
        If nodoCorrente.Children = 0 Then 'se nodoCorrente non ha figli
            If nodoCorrente.Next Is Nothing Then 'se nodoCorrente non ha fratelli
                Set nodoSuccessivo = nodoCorrente.Parent.Next
            Else
                Set nodoSuccessivo = nodoCorrente.Next
            End If
            nodoCorrente.Checked = False
            Call DeselezionaTutteCaratteristiche(nodoSuccessivo)
        Else
            nodoCorrente.Checked = False
            Set nodoSuccessivo = nodoCorrente.Child
            Call DeselezionaTutteCaratteristiche(nodoSuccessivo)
        End If
    End If
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub Controlli_Init()

End Sub

Private Sub cmdCaricaProdottiSlave_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CaricaProdottiSlave
    
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaProdottiSlave()
    Dim nodoStart As Node
    
    Set nodoStart = tvwCaratteristiche.Nodes(codiceRadice)
    Set listaCaratteristicheSelezionate = New Collection
    Call CreaListaCaratteristicheSelezionate(nodoStart, listaCaratteristicheSelezionate, False)
    Call CaricaValoriLstProdottiSlave
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub RilevaStatoProdottiSlaveSelezionati()
    Dim p As clsElementoLista
    Dim lista As Collection
    
    Set lista = New Collection
    Set listaProdottiAttivabili = New Collection
    Set listaProdottiAttivi = New Collection
    Set listaProdottiInAltriStati = New Collection
    For Each p In listaProdottiSlaveSelezionati
        Select Case p.idAttributoElementoLista
            Case TSP_IN_CONFIGURAZIONE
                Call lista.Add(p, ChiaveId(p.idElementoLista))
            Case TSP_ATTIVABILE
                Call listaProdottiAttivabili.Add(p, ChiaveId(p.idElementoLista))
            Case TSP_ATTIVO
                Call listaProdottiAttivi.Add(p, ChiaveId(p.idElementoLista))
                Call lista.Add(p, ChiaveId(p.idElementoLista))
            Case Else
                Call listaProdottiInAltriStati.Add(p, ChiaveId(p.idElementoLista))
        End Select
    Next p
    Set listaProdottiSlaveSelezionati = Nothing
    Set listaProdottiSlaveSelezionati = lista
End Sub

Private Sub RimuoviProdottiAttiviDaListaProdottiSlaveSelezionati()
    Dim p As clsElementoLista
    
    For Each p In listaProdottiAttivi
        Call listaProdottiSlaveSelezionati.Remove(ChiaveId(p.idElementoLista))
    Next p
End Sub

Private Sub Conferma()
    Dim c As clsElementoLista
    Dim codice As String
    Dim listaCampiAttributiStandard As Collection
    Dim elencoProdottiBloccatiDaAltroUtente As String
    Dim elencoProdottiAttivi As String
    Dim elencoProdottiAttivabili As String
    Dim elencoProdottiInAltriStati As String
    
    exitCode = EC_CONFERMA
    
    Call BloccaProdottiSlave
    
    Call RilevaStatoProdottiSlaveSelezionati
    If listaProdottiBloccatiDaAltroUtente.count > 0 Then
        elencoProdottiBloccatiDaAltroUtente = ElencoDaListaElementi(listaProdottiBloccatiDaAltroUtente, CEL_NOME_ELEMENTO_LISTA)
    Else
        elencoProdottiBloccatiDaAltroUtente = "<nessuno>"
    End If
    If listaProdottiAttivi.count > 0 Then
        elencoProdottiAttivi = ElencoDaListaElementi(listaProdottiAttivi, CEL_NOME_ELEMENTO_LISTA)
    Else
        elencoProdottiAttivi = "<nessuno>"
    End If
    If listaProdottiAttivabili.count > 0 Then
        elencoProdottiAttivabili = ElencoDaListaElementi(listaProdottiAttivabili, CEL_NOME_ELEMENTO_LISTA)
    Else
        elencoProdottiAttivabili = "<nessuno>"
    End If
    If listaProdottiInAltriStati.count > 0 Then
        elencoProdottiInAltriStati = ElencoDaListaElementi(listaProdottiInAltriStati, CEL_NOME_ELEMENTO_LISTA)
    Else
        elencoProdottiInAltriStati = "<nessuno>"
    End If
    Call frmMessaggio.Visualizza("ConfermaAggiornamentoListaProdotti", _
        elencoProdottiAttivi, elencoProdottiAttivabili, elencoProdottiBloccatiDaAltroUtente, elencoProdottiInAltriStati)
    If frmMessaggio.exitCode = EC_CONFERMA Then
        'Do Nothing
    Else
        Call RimuoviProdottiAttiviDaListaProdottiSlaveSelezionati
    End If
    Set listaCaratteristicheNonAggiornate = New Collection
    Set listaCampiAttributiStandard = New Collection
    For Each c In listaCaratteristicheSelezionate
        codice = Left$(c.codiceElementoLista, 2)
        Select Case codice
            Case codiceRadice
                'To Do
            Case codiceAttributiStandard
                Select Case c.codiceElementoLista
                    Case codiceContratto
                        Call listaCampiAttributiStandard.Add("IDCONTRATTO")
                    Case codiceAliquotaIVA
                        Call listaCampiAttributiStandard.Add("ALIQUOTAIVA")
                    Case codiceRateo
                        Call listaCampiAttributiStandard.Add("RATEO")
                    Case codicePropagazProtezAiProdCorrelati
                        Call listaCampiAttributiStandard.Add("PROPAGAZIOPROTEZIONICONSENTITA")
                    Case codiceClasseProdotto
                        Call listaCampiAttributiStandard.Add("IDCLASSEPRODOTTO")
                    Case codiceManifestazione
                        Call listaCampiAttributiStandard.Add("IDMANIFESTAZIONE")
                End Select
            Case codiceDurateRiservazione
                Select Case c.codiceElementoLista
                    Case codiceRiservazioneWeb
                        Call AggiornaDurateRiservazione("RISERVAZIONE_WEB")
                    Case codiceRiservazioneWebProrogata
                        Call AggiornaDurateRiservazione("RISERVAZIONE_WEB_PROROGATA")
                End Select
            Case codiceTariffe
                'To Do
            Case codicePrezzi
                'To Do
            Case codiceAssociazioneTipiLayoutSupporti
                If frmDettagliAssociazioneTipiLayoutSupporto.GetExitCode = EC_CONFERMA Then
                    Call AggiornaAssociazioneTipiLayoutSupporti
                End If
            Case codiceClassiDiUtenza
                Call AggiornaClassiDiUtenza
            Case codiceDirittiOperatoreSuTipiOperazione
                Call AggiornaDirittiOperatoreSuTipiOperazione
        End Select
    Next c
    If listaCampiAttributiStandard.count > 0 Then
        Call AggiornaAttributiStandard(listaCampiAttributiStandard)
    End If
    If listaCaratteristicheNonAggiornate.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreAggiornamentoGruppoProdotti", ArgomentoMessaggio(listaCaratteristicheNonAggiornate))
    Else
        Call frmMessaggio.Visualizza("NotificaAggiornamentoGruppoProdotti")
    End If
    
    Call SbloccaProdottiSlave
    
    Call PulisciValoriCampi
End Sub

Private Sub SbloccaProdottiSlave()
    Dim p As clsElementoLista
    
    For Each p In listaProdottiSlaveSelezionati
        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, p.idElementoLista, isProdottoBloccatoDaUtente)
    Next p
End Sub

Private Sub BloccaProdottiSlave()
    Dim p As clsElementoLista
    Dim lista As Collection
    
    Set lista = New Collection
    Set listaProdottiBloccatiDaAltroUtente = New Collection
    For Each p In listaProdottiSlaveSelezionati
        Call BloccaDominioPerUtente(CCDA_PRODOTTO, p.idElementoLista, isProdottoBloccatoDaUtente)
        If isProdottoBloccatoDaUtente Then
            Call lista.Add(p, ChiaveId(p.idElementoLista))
        Else
            Call listaProdottiBloccatiDaAltroUtente.Add(p, ChiaveId(p.idElementoLista))
        End If
    Next p
    Set listaProdottiSlaveSelezionati = Nothing
    Set listaProdottiSlaveSelezionati = lista
End Sub

Private Function IsCausaleRiservazioneDefinitaPerProdotto(idProdotto As Long, nomeCausale As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Integer
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(PC.IDCAUSALERISERVAZIONE) CONT"
    sql = sql & " FROM PRODOTTO_CAUSALERISERVAZIONE PC, CAUSALERISERVAZIONE C"
    sql = sql & " WHERE PC.IDCAUSALERISERVAZIONE = C.IDCAUSALERISERVAZIONE"
    sql = sql & " AND PC.IDPRODOTTO = " & idProdotto
    sql = sql & " AND C.NOME = '" & nomeCausale & "'"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IsCausaleRiservazioneDefinitaPerProdotto = (cont = 1)
End Function

Private Sub AggiornaAttributiStandard(lista As Collection)
    Dim sql As String
    Dim n As Long
    Dim p As clsElementoLista
    Dim tipoStatoRecordSelezionato As TipoStatoRecordEnum
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    
    For Each p In listaProdottiSlaveSelezionati
        Call CaricaParametriSessioneDallaBaseDati("PRODOTTO", "IDPRODOTTO", p.idElementoLista, p.idElementoLista)
        If isRecordEditabile Then
            sql = "UPDATE PRODOTTO"
            sql = sql & " SET (" & ElencoDaLista(lista) & ") ="
            sql = sql & " ("
            sql = sql & " SELECT " & ElencoDaLista(lista)
            sql = sql & " FROM PRODOTTO"
            sql = sql & " WHERE IDPRODOTTO = " & idProdottoMaster
            sql = sql & " )"
            sql = sql & " WHERE IDPRODOTTO = " & p.idElementoLista
            SETAConnection.Execute sql, n, adCmdText
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'                    Call AggiornaParametriSessioneSuRecord("PRODOTTO", "IDPRODOTTO", p.idElementoLista, TSR_MODIFICATO)
'                End If
'            End If
            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_PRODOTTO, "IDPRODOTTO = " & p.idElementoLista, p.idElementoLista)
        End If
    Next p
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call listaCaratteristicheNonAggiornate.Add("- attributi standard;")
End Sub

Private Sub AggiornaDurateRiservazione(nomeCausale As String)
    Dim sql As String
    Dim n As Long
    Dim isCausaleDefinitaPerMaster As Boolean
    Dim isCausaleDefinitaPerSlave As Boolean
    Dim i As Integer
    Dim p As clsElementoLista
    Dim idCausaleRiservazione As Long
    Dim condizioniSQL As String
    Dim stringaNota As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    
    isCausaleDefinitaPerMaster = IsCausaleRiservazioneDefinitaPerProdotto(idProdottoMaster, nomeCausale)
    isCausaleDefinitaPerSlave = False
    For Each p In listaProdottiSlaveSelezionati
        isCausaleDefinitaPerSlave = IsCausaleRiservazioneDefinitaPerProdotto(p.idElementoLista, nomeCausale)
        idCausaleRiservazione = IdCausaleRiservazioneDaNome(nomeCausale)
        stringaNota = "IDPRODOTTO = " & p.idElementoLista & _
            "; IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
        If isCausaleDefinitaPerMaster And isCausaleDefinitaPerSlave Then
            Call CaricaParametriSessioneDallaBaseDati("PRODOTTO_CAUSALERISERVAZIONE", "IDCAUSALERISERVAZIONE", p.idElementoLista, idCausaleRiservazione)
            If isRecordEditabile Then
                sql = "UPDATE PRODOTTO_CAUSALERISERVAZIONE" & _
                    " SET (DURATARISERVAZIONI, DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA) = " & _
                    " (" & _
                    " SELECT DURATARISERVAZIONI, DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA" & _
                    " FROM PRODOTTO_CAUSALERISERVAZIONE" & _
                    " WHERE IDPRODOTTO = " & idProdottoMaster & _
                    " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione & _
                    ")" & _
                    " WHERE IDPRODOTTO = " & p.idElementoLista & _
                    " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
                SETAConnection.Execute sql, n, adCmdText
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'                        condizioniSQL = " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
'                        Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", p.idElementoLista, TSR_NUOVO, condizioniSQL)
'                    End If
'                End If
                Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_CAUSALE_RISERVAZIONE, stringaNota, p.idElementoLista)
            End If
        ElseIf isCausaleDefinitaPerMaster And Not isCausaleDefinitaPerSlave Then
            sql = "INSERT INTO PRODOTTO_CAUSALERISERVAZIONE (" & _
                " IDPRODOTTO, IDCAUSALERISERVAZIONE, DURATARISERVAZIONI," & _
                " DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA)" & _
                " SELECT " & p.idElementoLista & ", " & _
                " IDCAUSALERISERVAZIONE, DURATARISERVAZIONI," & _
                " DATAORASCADENZARISERVAZIONI, DURATAPERIODOSCADENZAFORZATA" & _
                " FROM PRODOTTO_CAUSALERISERVAZIONE" & _
                " WHERE IDPRODOTTO = " & idProdottoMaster & _
                " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
            SETAConnection.Execute sql, n, adCmdText
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                condizioniSQL = " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
'                Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", p.idElementoLista, TSR_NUOVO, condizioniSQL)
'            End If
            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_CAUSALE_RISERVAZIONE, stringaNota, p.idElementoLista)
        ElseIf Not isCausaleDefinitaPerMaster And isCausaleDefinitaPerSlave Then
            Call CaricaParametriSessioneDallaBaseDati("PRODOTTO_CAUSALERISERVAZIONE", "IDCAUSALERISERVAZIONE", p.idElementoLista, idCausaleRiservazione)
            If isRecordEditabile Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                        sql = "DELETE FROM PRODOTTO_CAUSALERISERVAZIONE" & _
'                            " WHERE IDPRODOTTO = " & p.idElementoLista & _
'                            " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
'                        SETAConnection.Execute sql, n, adCmdText
'                    Else
'                        condizioniSQL = " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
'                        Call AggiornaParametriSessioneSuRecord("PRODOTTO_CAUSALERISERVAZIONE", "IDPRODOTTO", p.idElementoLista, TSR_ELIMINATO, condizioniSQL)
'                    End If
'                Else
                    sql = "DELETE FROM PRODOTTO_CAUSALERISERVAZIONE" & _
                        " WHERE IDPRODOTTO = " & p.idElementoLista & _
                        " AND IDCAUSALERISERVAZIONE = " & idCausaleRiservazione
                    SETAConnection.Execute sql, n, adCmdText
'                End If
            End If
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_CAUSALE_RISERVAZIONE, stringaNota, p.idElementoLista)
        End If
    Next p
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call listaCaratteristicheNonAggiornate.Add("- durate riserazione: " & nomeCausale & ";")
End Sub

Private Sub CaricaParametriSessioneDallaBaseDati(nomeTabella As String, NomeCampo As String, idProdotto As Long, idRecord As Long)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
'            " FROM " & nomeTabella & _
'            " WHERE " & NomeCampo & " = " & idRecord & _
'            " AND IDPRODOTTO = " & idProdotto
'    Else
        sql = "SELECT " & TSR_NON_SPECIFICATO & " IDTIPOSTATORECORD," & _
            idSessioneConfigurazioneCorrente & " IDSESSIONECONFIGURAZIONE" & _
            " FROM DUAL"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
        idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
        isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
            idSessione = idSessioneConfigurazioneCorrente)
    Else
        tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO
        isRecordEditabile = True
    End If
    rec.Close
End Sub

Private Function IdCausaleRiservazioneDaNome(nome As String) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT IDCAUSALERISERVAZIONE FROM CAUSALERISERVAZIONE WHERE NOME = '" & nome & "'"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        id = rec("IDCAUSALERISERVAZIONE")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    IdCausaleRiservazioneDaNome = id
End Function

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, inserisciID As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            If (inserisciID) Then
                cmb.AddItem (rec("ID").Value & " - " & rec(NomeCampo))
            Else
                cmb.AddItem (rec(NomeCampo))
            End If
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub


Private Sub tvwCaratteristiche_Init()
   Dim nodo As Node
   
   tvwCaratteristiche.CheckBoxes = True
   Set nodo = tvwCaratteristiche.Nodes.Add(, , codiceRadice, "Prodotto Master")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceAttributiStandard, "attributi standard")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceContratto, "contratto")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceAliquotaIVA, "aliquota IVA")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceRateo, "rateo")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codicePropagazProtezAiProdCorrelati, "propapagaz. protez. ai prodotti correl.")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceClasseProdotto, "classe prodotto")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceManifestazione, "manifestazione")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceDurateRiservazione, "durate riservazioni")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceDurateRiservazione, tvwChild, codiceRiservazioneWeb, "RISERVAZIONE_WEB")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceDurateRiservazione, tvwChild, codiceRiservazioneWebProrogata, "RISERVAZIONE_WEB_PROROGATA")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceAssociazioneTipiLayoutSupporti, "associazione tipi/layout supporto")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceClassiDiUtenza, "classi di utenza")
   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceDirittiOperatoreSuTipiOperazione, "diritti operatore su tipi operazione")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceDirittiOperatoreSuProdotto, tvwChild, codiceDirittiOperatoreSenzaEliminazioneSuSlave, "senza eliminazione su slave")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceDirittiOperatoreSuProdotto, tvwChild, codiceDirittiOperatoreConEliminazioneSuSlave, "con eliminazione su slave")
   nodo.EnsureVisible
   tvwCaratteristiche.BorderStyle = vbFixedSingle
End Sub

Private Sub CaricaValoriLstProdottiSlave()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiave As String
    Dim prodottoCorrente As clsElementoLista
    Dim idSessioneRecord As Long

    Call ApriConnessioneBD

    Set listaProdottiSlave = New Collection
    Set listaProdottiSlaveSelezionati = New Collection
    
    If prodottiDefinitiSuStessaPianta Then
        sql = "SELECT P1.IDPRODOTTO ID, P1.NOME, R.DATAORAINIZIO, P1.IDTIPOSTATOPRODOTTO"
        sql = sql & " FROM PRODOTTO P1, PRODOTTO P2,"
        sql = sql & " ("
        sql = sql & " SELECT IDPRODOTTO, MIN(R.DATAORAINIZIO) DATAORAINIZIO"
        sql = sql & " FROM RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE PR"
        sql = sql & " WHERE R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
        sql = sql & " GROUP BY IDPRODOTTO"
        sql = sql & " ) R"
        sql = sql & " WHERE P1.IDORGANIZZAZIONE = " & idOrganizzazioneMaster
        sql = sql & " AND P1.IDPRODOTTO = R.IDPRODOTTO(+)"
        sql = sql & " AND P1.IDPRODOTTO <> " & idProdottoMaster
        sql = sql & " AND P2.IDPRODOTTO = " & idProdottoMaster
        sql = sql & " AND P1.IDPIANTA = P2.IDPIANTA"
        sql = sql & " ORDER BY NOME"
    Else
        sql = "SELECT P.IDPRODOTTO ID, P.NOME, R.DATAORAINIZIO, P.IDTIPOSTATOPRODOTTO"
        sql = sql & " FROM PRODOTTO P,"
        sql = sql & " ("
        sql = sql & " SELECT IDPRODOTTO, MIN(R.DATAORAINIZIO) DATAORAINIZIO"
        sql = sql & " FROM RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE PR"
        sql = sql & " WHERE R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
        sql = sql & " GROUP BY IDPRODOTTO"
        sql = sql & " ) R"
        sql = sql & " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneMaster
        sql = sql & " AND P.IDPRODOTTO = R.IDPRODOTTO(+)"
        sql = sql & " AND P.IDPRODOTTO <> " & idProdottoMaster
        sql = sql & " ORDER BY NOME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodottoCorrente = New clsElementoLista
            prodottoCorrente.nomeElementoLista = rec("NOME")
            prodottoCorrente.idElementoLista = rec("ID").Value
            prodottoCorrente.idAttributoElementoLista = rec("IDTIPOSTATOPRODOTTO").Value
            idSessioneRecord = idNessunElementoSelezionato
            prodottoCorrente.descrizioneElementoLista = rec("NOME") & " - " & rec("DATAORAINIZIO")
            chiave = ChiaveId(prodottoCorrente.idElementoLista)
            Call listaProdottiSlave.Add(prodottoCorrente, chiave)
            rec.MoveNext
        Wend
    End If
    rec.Close

    Call ChiudiConnessioneBD

    Call lstProdottiSlave_Init

End Sub

Private Sub lstProdottiSlave_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstProdottiSlave.Clear

    If Not (listaProdottiSlave Is Nothing) Then
        i = 1
        For Each tip In listaProdottiSlave
            lstProdottiSlave.AddItem tip.descrizioneElementoLista
            lstProdottiSlave.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CreaListaCaratteristicheSelezionate(nodoStart As Node, lista As Collection, aggiungiCaratteristica As Boolean)
    Dim nodoCorrente As Node
    Dim nodoSuccessivo As Node
    Dim caratteristicaAggiunta As clsElementoLista
    
    If Not nodoStart Is Nothing Then
        Set nodoCorrente = nodoStart
        aggiungiCaratteristica = aggiungiCaratteristica Or _
            nodoCorrente.Checked
        If nodoCorrente.Children = 0 Then 'se nodoCorrente non ha figli
            If aggiungiCaratteristica Then
                Set caratteristicaAggiunta = New clsElementoLista
                caratteristicaAggiunta.codiceElementoLista = nodoCorrente.Key
                caratteristicaAggiunta.descrizioneElementoLista = nodoCorrente.Text
                Call lista.Add(caratteristicaAggiunta)
            End If
            If nodoCorrente.Next Is Nothing Then 'se nodoCorrente non ha fratelli
                Set nodoSuccessivo = nodoCorrente.Parent.Next
                aggiungiCaratteristica = False
            Else
                Set nodoSuccessivo = nodoCorrente.Next
                aggiungiCaratteristica = nodoSuccessivo.Checked Or nodoCorrente.Parent.Checked
            End If
            Call CreaListaCaratteristicheSelezionate(nodoSuccessivo, lista, aggiungiCaratteristica)
        Else
            Set nodoSuccessivo = nodoCorrente.Child
            Call CreaListaCaratteristicheSelezionate(nodoSuccessivo, lista, aggiungiCaratteristica)
        End If
    End If
End Sub

Private Sub SelezionaDeselezionaElementiTreeView_old(nodoCorrente As MSComctlLib.Node)
    Dim i As Integer
    Dim nodoFiglio As Node
    
    prodottiDefinitiSuStessaPianta = False
    If nodoCorrente.Checked Then
        numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate + 1
        If nodoCorrente.Key = codiceAssociazioneTipiLayoutSupporti Then
            prodottiDefinitiSuStessaPianta = True
            Call CaricaFormDettagliAssociazioneTipiLayoutSupporti
        End If
        If nodoCorrente.Children > 0 Then
            Set nodoFiglio = nodoCorrente.Child
            For i = 1 To nodoCorrente.Children
                If nodoFiglio.Checked = False Then
                    numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate + 1
                End If
                nodoFiglio.Checked = True
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    ElseIf Not nodoCorrente.Checked Then
        numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate - 1
        If nodoCorrente.Parent.Checked = True Then
            numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate - 1
        End If
        nodoCorrente.Parent.Checked = False
        If nodoCorrente.Children > 0 Then
            Set nodoFiglio = nodoCorrente.Child
            For i = 1 To nodoCorrente.Children
                If nodoFiglio.Checked = True Then
                    numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate - 1
                End If
                nodoFiglio.Checked = False
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormDettagliAssociazioneTipiLayoutSupporti()
    Call frmDettagliAssociazioneTipiLayoutSupporto.SetIdProdottoMaster(idProdottoMaster)
    Call frmDettagliAssociazioneTipiLayoutSupporto.Init
End Sub

Private Sub CaricaFormDettagliModalitāImportazioneDirittiOperatore()
    Call frmDettagliModalitāInserimentoDirittiOperatore.Init
End Sub

Private Sub tvwCaratteristiche_NodeCheck(ByVal nodoCorrente As MSComctlLib.Node)
    If Not internalEvent Then
        Call SelezionaDeselezionaElementiTreeView(nodoCorrente)
    End If
End Sub

Private Sub SelezionaDeselezionaProdottoSlave()
    Dim idProdottoSlaveSelezionato As Long
    Dim prodottoSlaveSelezionato As clsElementoLista
    
    idProdottoSlaveSelezionato = lstProdottiSlave.ItemData(lstProdottiSlave.ListIndex)
    Set prodottoSlaveSelezionato = listaProdottiSlave.Item(ChiaveId(idProdottoSlaveSelezionato))
    If lstProdottiSlave.Selected(lstProdottiSlave.ListIndex) Then
        Call AggiungiProdottoALista(prodottoSlaveSelezionato)
    Else
        Call RimuoviProdottoDaLista(prodottoSlaveSelezionato)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiungiProdottoALista(prodotto As clsElementoLista)
    Call listaProdottiSlaveSelezionati.Add(prodotto, ChiaveId(prodotto.idElementoLista))
End Sub

Private Sub RimuoviProdottoDaLista(prodotto As clsElementoLista)

On Error Resume Next

    Call listaProdottiSlaveSelezionati.Remove(ChiaveId(prodotto.idElementoLista))
    
End Sub

Private Sub lstProdottiSlave_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaProdottoSlave
    End If
End Sub

Private Sub cmdSelezionaTutti_Click()
    Call SelezionaTuttiProdottiSlave
End Sub

Private Sub cmdDeselezionaTutti_Click()
    Call DeselezionaTuttiProdottiSlave
End Sub

Private Sub SelezionaTuttiProdottiSlave()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idProdotto As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstProdottiSlave.ListCount
        lstProdottiSlave.Selected(i - 1) = True
    Next i
    Set listaProdottiSlaveSelezionati = Nothing
    Set listaProdottiSlaveSelezionati = listaProdottiSlave
    
    internalEvent = internalEventOld
End Sub

Private Sub DeselezionaTuttiProdottiSlave()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idArea As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstProdottiSlave.ListCount
        lstProdottiSlave.Selected(i - 1) = False
    Next i
    Set listaProdottiSlaveSelezionati = Nothing
    Set listaProdottiSlaveSelezionati = New Collection
    
    internalEvent = internalEventOld
End Sub

Private Sub AggiornaAssociazioneTipiLayoutSupporti()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    Dim i As Integer
    Dim listaIdAreeSuperaree As Collection
    Dim listaIdTariffe As Collection
    Dim listaIdTipiSupporto As Collection
    Dim listaIdLayoutSupporto As Collection
    Dim idTariffaMaster As Long
    Dim idTariffaSlave As Long
    Dim p As clsElementoLista
    Dim condizioniSQL As String
    Dim selezionaInteraPianta As Boolean
    Dim idPiantaMaster As Long
    
    Set listaIdAreeSuperaree = frmDettagliAssociazioneTipiLayoutSupporto.GetListaIdAreeSuperareeSelezionate
    Set listaIdTariffe = frmDettagliAssociazioneTipiLayoutSupporto.GetListaIdTariffeSelezionate
    Set listaIdTipiSupporto = frmDettagliAssociazioneTipiLayoutSupporto.GetListaIdTipiSupportoSelezionati
    Set listaIdLayoutSupporto = frmDettagliAssociazioneTipiLayoutSupporto.GetListaIdLayoutSupportoSelezionati
    selezionaInteraPianta = frmDettagliAssociazioneTipiLayoutSupporto.GetSelezionaInteraPianta
    
On Error GoTo gestioneErrori
    
    idPiantaMaster = IdPiantaAssociataAProdotto(idProdottoMaster)
    
    SETAConnection.BeginTrans
    
    If listaIdAreeSuperaree.count > 0 Or selezionaInteraPianta Then
        If listaIdTariffe.count = 0 Then
            Set listaIdTariffe = Nothing
            Set listaIdTariffe = New Collection
            sql = "SELECT DISTINCT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoMaster
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.EOF And rec.BOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idTariffaMaster = rec("IDTARIFFA").Value
                    Call listaIdTariffe.Add(idTariffaMaster, ChiaveId(idTariffaMaster))
                    rec.MoveNext
                Wend
            End If
            rec.Close
        End If
        For Each p In listaProdottiSlaveSelezionati
            For i = 1 To listaIdTariffe.count
                idTariffaMaster = listaIdTariffe.Item(i)
                idTariffaSlave = IdTariffaCorrispondente(idTariffaMaster, p.idElementoLista)
                sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDTARIFFA = " & idTariffaSlave
                If selezionaInteraPianta Then
                    sql = sql & " AND IDAREA IN ("
                    sql = sql & " SELECT IDAREA FROM AREA"
                    sql = sql & " WHERE IDPIANTA = " & idPiantaMaster
                    sql = sql & ")"
                Else
                    sql = sql & IIf(listaIdAreeSuperaree.count = 0, "", " AND IDAREA IN (" & ElencoDaLista(listaIdAreeSuperaree) & ")")
                End If
                sql = sql & IIf(listaIdTipiSupporto.count = 0, "", " AND IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupporto) & ")")
                sql = sql & IIf(listaIdLayoutSupporto.count = 0, "", " AND IDLAYOUTSUPPORTO IN (" & ElencoDaLista(listaIdLayoutSupporto) & ")")
                SETAConnection.Execute sql, n, adCmdText
                
                sql = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)"
                sql = sql & " SELECT DISTINCT IDAREA, " & idTariffaSlave & ","
                sql = sql & " IDTIPOSUPPORTO, IDLAYOUTSUPPORTO"
                sql = sql & " FROM UTILIZZOLAYOUTSUPPORTOCPV"
                sql = sql & " WHERE IDTARIFFA = " & idTariffaMaster
                If selezionaInteraPianta Then
                    sql = sql & " AND IDAREA IN ("
                    sql = sql & " SELECT IDAREA FROM AREA"
                    sql = sql & " WHERE IDPIANTA = " & idPiantaMaster
                    sql = sql & ")"
                Else
                    sql = sql & IIf(listaIdAreeSuperaree.count = 0, "", " AND IDAREA IN (" & ElencoDaLista(listaIdAreeSuperaree) & ")")
                End If
                sql = sql & IIf(listaIdTipiSupporto.count = 0, "", " AND IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupporto) & ")")
                sql = sql & IIf(listaIdLayoutSupporto.count = 0, "", " AND IDLAYOUTSUPPORTO IN (" & ElencoDaLista(listaIdLayoutSupporto) & ")")
                SETAConnection.Execute sql, n, adCmdText
            Next i
            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI, , p.idElementoLista)
        Next p
    End If
    
    SETAConnection.CommitTrans
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call listaCaratteristicheNonAggiornate.Add("- associazione tipi/layout supporti;")
End Sub

''''Private Sub AggiornaDirittiOperatoreSuProdotto_old()
''''    Dim sql As String
''''    Dim sql1 As String
''''    Dim n As Long
''''    Dim rec As New ADODB.Recordset
''''    Dim idTariffaMaster As Long
''''    Dim idTariffaSlave As Long
''''    Dim p As clsElementoLista
''''    Dim condizioniSQL As String
''''
''''On Error GoTo gestioneErrori
''''
''''    Call ApriConnessioneBD
''''
''''    SETAConnection.BeginTrans
''''
''''    For Each p In listaProdottiSlaveSelezionati
'''''        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'''''            condizioniSQL = " AND IDOPERATORE IN"
'''''            condizioniSQL = condizioniSQL & " (SELECT IDOPERATORE FROM OPERATORE_PRODOTTO"
'''''            condizioniSQL = condizioniSQL & " WHERE IDPRODOTTO = " & idProdottoMaster & ")"
'''''            Call AggiornaParametriSessioneSuRecord("OPERATORE_CAUSALEPROTEZIONE", "IDPRODOTTO", p.idElementoLista, TSR_ELIMINATO, condizioniSQL)
'''''        Else
''''            sql = "DELETE FROM OPERATORE_PRODOTTO"
''''            sql = sql & " WHERE IDPRODOTTO = " & p.idElementoLista
''''            sql = sql & " AND IDOPERATORE IN"
''''            sql = sql & " (SELECT IDOPERATORE FROM OPERATORE_PRODOTTO"
''''            sql = sql & " WHERE IDPRODOTTO = " & idProdottoMaster & ")"
''''            SETAConnection.Execute sql, n, adCmdText
'''''        End If
'''''        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'''''            sql = "INSERT INTO OPERATORE_PRODOTTO"
'''''            sql = sql & " (IDOPERATORE, IDPRODOTTO)"
'''''            sql = sql & " SELECT"
'''''            sql = sql & " IDOPERATORE, " & p.idElementoLista
'''''            sql = sql & " FROM OPERATORE_PRODOTTO"
'''''            sql = sql & " WHERE IDPRODOTTO = " & idProdottoMaster
'''''            sql = sql & " MINUS"
'''''            sql = sql & " SELECT IDOPERATORE, IDPRODOTTO"
'''''            sql = sql & " FROM OPERATORE_PRODOTTO"
'''''            sql = sql & " WHERE IDPRODOTTO = " & p.idElementoLista
'''''            SETAConnection.Execute sql, n, adCmdText
''''''                    condizioneSql = " AND IDOPERATORE = " & operatore.idElementoLista
''''''                   la condizione sql buona dovrebbe essere comunque quella del delete
'''''            Call AggiornaParametriSessioneSuRecord("OPERATORE_PRODOTTO", "IDPRODOTTO", p.idElementoLista, TSR_NUOVO, condizioniSQL)
'''''        Else
''''            sql = "INSERT INTO OPERATORE_PRODOTTO"
''''            sql = sql & " (IDOPERATORE, IDPRODOTTO)"
''''            sql = sql & " SELECT"
''''            sql = sql & " IDOPERATORE, " & p.idElementoLista
''''            sql = sql & " FROM OPERATORE_PRODOTTO"
''''            sql = sql & " WHERE IDPRODOTTO = " & idProdottoMaster
''''            SETAConnection.Execute sql, n, adCmdText
'''''        End If
''''        Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, , p.idElementoLista)
''''    Next p
''''
''''    SETAConnection.CommitTrans
''''
''''    Call ChiudiConnessioneBD
''''
''''    Exit Sub
''''
''''gestioneErrori:
''''    SETAConnection.RollbackTrans
''''    Call listaCaratteristicheNonAggiornate.Add("- diritti operatore su prodotto;")
''''End Sub

Private Sub lstProdottiSlave_Click()
    Call VisualizzaListBoxToolTip(lstProdottiSlave, lstProdottiSlave.Text)
End Sub

Private Sub SelezionaDeselezionaElementiTreeView(nodoCorrente As MSComctlLib.Node)
    Dim i As Integer
    Dim nodoFiglio As Node
    
    prodottiDefinitiSuStessaPianta = False
    If nodoCorrente.Checked Then
        numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate + 1
        If nodoCorrente.Key = codiceAssociazioneTipiLayoutSupporti Then
            prodottiDefinitiSuStessaPianta = True
            Call CaricaFormDettagliAssociazioneTipiLayoutSupporti
'        ElseIf nodoCorrente.Key = codiceDirittiOperatoreSuProdotto Then
'            Call CaricaFormDettagliModalitāImportazioneDirittiOperatore
        End If
        If nodoCorrente.Children > 0 Then
            Set nodoFiglio = nodoCorrente.Child
            For i = 1 To nodoCorrente.Children
                If nodoFiglio.Checked = False Then
                    numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate + 1
                End If
                nodoFiglio.Checked = True
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    ElseIf Not nodoCorrente.Checked Then
        numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate - 1
        If nodoCorrente.Parent.Checked = True Then
            numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate - 1
        End If
        nodoCorrente.Parent.Checked = False
        If nodoCorrente.Children > 0 Then
            Set nodoFiglio = nodoCorrente.Child
            For i = 1 To nodoCorrente.Children
                If nodoFiglio.Checked = True Then
                    numeroCaratteristicheSelezionate = numeroCaratteristicheSelezionate - 1
                End If
                nodoFiglio.Checked = False
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaClassiDiUtenza()
    Dim sql As String
    Dim sql1 As String
    Dim n As Long
    Dim rec As New ADODB.Recordset
    Dim idTariffaMaster As Long
    Dim idTariffaSlave As Long
    Dim p As clsElementoLista
    Dim condizioniSQL As String
    Dim importaEliminandoDiritti As Boolean
    Dim importaConservandoDiritti As Boolean
    
On Error GoTo gestioneErrori

'    importaConservandoDiritti = frmDettagliModalitāInserimentoDirittiOperatore.GetImportaConservandoPreesistenti
'    importaEliminandoDiritti = frmDettagliModalitāInserimentoDirittiOperatore.GetImportaEliminandoPreesistenti
    
    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    
    For Each p In listaProdottiSlaveSelezionati
'        If importaConservandoDiritti Then
'            Call EliminaDirittiOperatoreCorrispondentiSuProdotto(p.idElementoLista)
'        ElseIf importaEliminandoDiritti Then
'            Call EliminaTuttiDirittiOperatoreSuProdotto(p.idElementoLista)
'        End If
        Call EliminaClassiPuntoVenditaSuProdotto(p.idElementoLista)
'        Call InserisciDirittiOperatoreSuProdotto(p.idElementoLista)
        Call InserisciClassiPuntoVenditaSuProdotto(p.idElementoLista)
        Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, , p.idElementoLista)
    Next p
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call listaCaratteristicheNonAggiornate.Add("- diritti operatore su prodotto;")
End Sub

Private Sub AggiornaDirittiOperatoreSuTipiOperazione()
    Dim sql As String
    Dim sql1 As String
    Dim n As Long
    Dim rec As New ADODB.Recordset
    Dim idTariffaMaster As Long
    Dim idTariffaSlave As Long
    Dim p As clsElementoLista
    Dim condizioniSQL As String
    Dim importaEliminandoDiritti As Boolean
    Dim importaConservandoDiritti As Boolean
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    
    For Each p In listaProdottiSlaveSelezionati
        Call EliminaDirittiOperatoreSuProdotto(p.idElementoLista)
        Call InserisciDirittiOperatoreSuProdotto(p.idElementoLista)
        Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, , p.idElementoLista)
    Next p
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call listaCaratteristicheNonAggiornate.Add("- diritti operatore su prodotto;")
End Sub
'
'Private Sub EliminaTuttiDirittiOperatoreSuProdotto(idProdotto As Long)
'    Dim sql As String
'    Dim n As Long
'
'    sql = "DELETE FROM OPERATORE_PRODOTTO"
'    sql = sql & " WHERE IDPRODOTTO = " & idProdotto
'    SETAConnection.Execute sql, n, adCmdText
'
'End Sub
'
'Private Sub EliminaDirittiOperatoreCorrispondentiSuProdotto(idProdotto As Long)
'    Dim sql As String
'    Dim n As Long
'    Dim condizioniSQL As String
'
'    sql = "DELETE FROM OPERATORE_PRODOTTO" & _
'        " WHERE IDPRODOTTO = " & idProdotto & _
'        " AND IDOPERATORE IN" & _
'        " (SELECT IDOPERATORE" & _
'        " FROM OPERATORE_PRODOTTO" & _
'        " WHERE IDPRODOTTO = " & idProdottoMaster & ")"
'    SETAConnection.Execute sql, n, adCmdText
'
'End Sub

Private Sub EliminaClassiPuntoVenditaSuProdotto(idProdotto As Long)
    Dim sql As String
    Dim n As Long
    Dim condizioniSQL As String
    
    sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
        " WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO" & _
        " FROM CLASSESUPERAREAPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM CLASSESUPERAREAPROD_SUPERAREA" & _
        " WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO" & _
        " FROM CLASSESUPERAREAPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
        " WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO" & _
        " FROM CLASSESUPERAREAPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM CLASSESUPERAREAPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdotto
    SETAConnection.Execute sql, n, adCmdText
    
End Sub
'
'Private Sub InserisciDirittiOperatoreSuProdotto(idProdotto As Long)
'    Dim sql As String
'    Dim n As Long
'
'    sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)"
'    sql = sql & " SELECT IDOPERATORE, " & idProdotto
'    sql = sql & " FROM OPERATORE_PRODOTTO"
'    sql = sql & " WHERE IDPRODOTTO = " & idProdottoMaster
'    SETAConnection.Execute sql, n, adCmdText
'
'End Sub

Private Sub EliminaDirittiOperatoreSuProdotto(idProdotto As Long)
    Dim sql As String
    Dim n As Long
    
    sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE WHERE IDPRODOTTO = " & idProdotto
    SETAConnection.Execute sql, n, adCmdText

End Sub

Private Sub InserisciDirittiOperatoreSuProdotto(idProdotto As Long)
    Dim sql As String
    Dim n As Long
    
    sql = "INSERT INTO OPERATORE_TIPOOPERAZIONE (IDOPERATORE, IDPRODOTTO, IDTIPOOPERAZIONE)"
    sql = sql & " SELECT IDOPERATORE, " & idProdotto & ", IDTIPOOPERAZIONE"
    sql = sql & " FROM OPERATORE_TIPOOPERAZIONE"
    sql = sql & " WHERE IDPRODOTTO = " & idProdottoMaster
    SETAConnection.Execute sql, n, adCmdText

End Sub

Private Sub InserisciClassiPuntoVenditaSuProdotto(idProdotto As Long)
    Dim sql As String
    Dim n As Long
    
    sql = "INSERT INTO CLASSESUPERAREAPRODOTTO (IDCLASSESUPERAREAPRODOTTO, IDCLASSESUPERAREA, IDPRODOTTO)" & _
        " SELECT SQ_CLASSESUPERAREAPRODOTTO.NEXTVAL, IDCLASSESUPERAREA, " & idProdotto & _
        " FROM CLASSESUPERAREAPRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdottoMaster
    SETAConnection.Execute sql, n, adCmdText

    sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
        " SELECT DISTINCT CSEC.IDCLASSESUPERAREAPRODOTTO, CPV.IDPUNTOVENDITA" & _
        " FROM CLASSESAPROD_PUNTOVENDITA CPV, CLASSESUPERAREAPRODOTTO CPR, CLASSESUPERAREAPRODOTTO CSEC" & _
        " WHERE CPR.IDPRODOTTO = " & idProdottoMaster & _
        " AND CSEC.IDPRODOTTO = " & idProdotto & _
        " AND CPR.IDCLASSESUPERAREA = CSEC.IDCLASSESUPERAREA" & _
        " AND CPR.IDCLASSESUPERAREAPRODOTTO = CPV.IDCLASSESUPERAREAPRODOTTO"
    SETAConnection.Execute sql, n, adCmdText

    sql = "INSERT INTO CLASSESUPERAREAPROD_SUPERAREA (IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA)" & _
        " SELECT CSEC.IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA" & _
        " FROM CLASSESUPERAREAPROD_SUPERAREA CS, CLASSESUPERAREAPRODOTTO CPR, CLASSESUPERAREAPRODOTTO CSEC" & _
        " WHERE CPR.IDPRODOTTO = " & idProdottoMaster & _
        " AND CSEC.IDPRODOTTO = " & idProdotto & _
        " AND CPR.IDCLASSESUPERAREA = CSEC.IDCLASSESUPERAREA" & _
        " AND CPR.IDCLASSESUPERAREAPRODOTTO = CS.IDCLASSESUPERAREAPRODOTTO"
    SETAConnection.Execute sql, n, adCmdText

    sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
        " SELECT CSEC.IDCLASSESUPERAREAPRODOTTO, PCT.IDCLASSEPUNTOVENDITA, PCT.IDTIPOOPERAZIONE, PCT.DATAORAINIZIOVALIDITA, PCT.DATAORAFINEVALIDITA" & _
        " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCT, CLASSESUPERAREAPRODOTTO CPR, CLASSESUPERAREAPRODOTTO CSEC" & _
        " WHERE CPR.IDPRODOTTO = " & idProdottoMaster & _
        " AND CSEC.IDPRODOTTO = " & idProdotto & _
        " AND CPR.IDCLASSESUPERAREA = CSEC.IDCLASSESUPERAREA" & _
        " AND CPR.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO"
    SETAConnection.Execute sql, n, adCmdText

End Sub

