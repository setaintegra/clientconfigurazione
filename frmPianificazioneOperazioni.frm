VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmPianificazioneOperazioni 
   Caption         =   "Pianificazione operazioni"
   ClientHeight    =   11460
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   ScaleHeight     =   11460
   ScaleWidth      =   15240
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalvaSuFileExcel 
      Caption         =   "Salva su file"
      Height          =   495
      Left            =   13860
      TabIndex        =   22
      Top             =   6180
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Attivitą ancora da eseguire"
      Height          =   5235
      Left            =   180
      TabIndex        =   20
      Top             =   6060
      Width           =   13515
      Begin VB.ListBox lstAttivita 
         Height          =   4740
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   13275
      End
   End
   Begin VB.CheckBox chkTuttoIlProdotto 
      Caption         =   "Tutto il prodotto"
      Height          =   315
      Left            =   4560
      TabIndex        =   19
      Top             =   1980
      Width           =   4095
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   495
      Left            =   3720
      TabIndex        =   9
      Top             =   5220
      Width           =   1335
   End
   Begin VB.TextBox txtOra 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5820
      MaxLength       =   2
      TabIndex        =   7
      Top             =   4740
      Width           =   435
   End
   Begin VB.TextBox txtMinuti 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6420
      MaxLength       =   2
      TabIndex        =   8
      Top             =   4740
      Width           =   435
   End
   Begin VB.ListBox lstSuperaree 
      Height          =   1860
      Left            =   4560
      Style           =   1  'Checkbox
      TabIndex        =   5
      Top             =   2820
      Width           =   4095
   End
   Begin VB.ListBox lstTipiTerminale 
      Height          =   2310
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   4
      Top             =   2340
      Width           =   4095
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   495
      Left            =   13800
      TabIndex        =   12
      Top             =   10800
      Width           =   1335
   End
   Begin VB.ComboBox cmbProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1560
      Width           =   4095
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   840
      Width           =   4095
   End
   Begin VB.CheckBox chkOrganizzazioniAttive 
      Caption         =   "solo organizzazioni attive"
      Height          =   195
      Left            =   1920
      TabIndex        =   1
      Top             =   600
      Width           =   2295
   End
   Begin MSComCtl2.DTPicker dtpData 
      Height          =   315
      Left            =   2400
      TabIndex        =   6
      Top             =   4740
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   16580609
      CurrentDate     =   37684.7534259259
   End
   Begin VB.Label lblDataOra 
      Caption         =   "Data/ora esecuzione:"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   4800
      Width           =   1635
   End
   Begin VB.Label lblData 
      Alignment       =   1  'Right Justify
      Caption         =   "data"
      Height          =   255
      Left            =   1860
      TabIndex        =   17
      Top             =   4800
      Width           =   435
   End
   Begin VB.Label lblSeparatoreOreMinuti 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6300
      TabIndex        =   16
      Top             =   4740
      Width           =   75
   End
   Begin VB.Label lblOra 
      Alignment       =   1  'Right Justify
      Caption         =   "ora (HH:MM)"
      Height          =   255
      Left            =   4800
      TabIndex        =   15
      Top             =   4800
      Width           =   915
   End
   Begin VB.Label Label4 
      Caption         =   "Superaree"
      Height          =   255
      Left            =   4560
      TabIndex        =   14
      Top             =   2520
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Tipi terminale"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   2040
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Prodotto"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1320
      Width           =   1695
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   600
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Definizione sold out"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3375
   End
End
Attribute VB_Name = "frmPianificazioneOperazioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private mostraSoloOrganizzazioniAttive As ValoreBooleanoEnum
Private dataOraPianificata As Date

Private excReport As New Excel.Application

Public Sub Init()
    
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    
    'default stato organizzazione
    Me.chkOrganizzazioniAttive.Value = 1 '+
    mostraSoloOrganizzazioniAttive = VB_VERO '+
    
    Call CaricaComboOrganizzazioni
    Call CaricaListaOperazioni
    
    Call Me.Show(vbModal)
    
End Sub

Private Sub chkOrganizzazioniAttive_Click()
    If Not internalEvent Then
        mostraSoloOrganizzazioniAttive = chkOrganizzazioniAttive.Value
        Call CaricaComboOrganizzazioni
    End If
End Sub

Private Sub CaricaComboOrganizzazioni()
    Dim sql As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    cmbOrganizzazione.Clear
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE"
    If mostraSoloOrganizzazioniAttive = VB_VERO Then
        sql = sql & " WHERE IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVA
    End If
    sql = sql & " ORDER BY NOME"
    Call CaricaValoriCombo3(cmbOrganizzazione, sql, "NOME", numeroMassimoElementiInCombo)
        
    MousePointer = mousePointerOld
End Sub

Private Sub chkTuttoIlProdotto_Click()
    If chkTuttoIlProdotto.Value = vbChecked Then
        lstSuperaree.Enabled = False
    Else
        lstSuperaree.Enabled = True
    End If
End Sub

Private Sub cmbOrganizzazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = Me.MousePointer
    Me.MousePointer = vbHourglass
    Call cmbOrganizzazione_Update
    
    Me.MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Update()
    
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    Call CaricaComboProdotto
    
End Sub

Private Sub CaricaComboProdotto()
    Dim sql As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    cmbProdotto.Clear
    
    sql = "SELECT DISTINCT P.IDPRODOTTO ID, P.IDPRODOTTO || ' - ' || NOME AS NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
        " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " AND R.DATAORAINIZIO > SYSDATE" & _
        " ORDER BY P.IDPRODOTTO DESC"
    Call CaricaValoriCombo3(cmbProdotto, sql, "NOME", numeroMassimoElementiInCombo)

    MousePointer = mousePointerOld
End Sub

Private Sub cmbProdotto_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = Me.MousePointer
    Me.MousePointer = vbHourglass
    Call cmbProdotto_Update
    
    Me.MousePointer = mousePointerOld
End Sub

Private Sub cmbProdotto_Update()
    
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
    Call CaricaListaTipiTerminale
    Call CaricaListaSuperaree
    
End Sub

Private Sub CaricaListaTipiTerminale()
    Dim sql As String
    Dim rec As OraDynaset
    Dim mousePointerOld As Integer
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ApriConnessioneBD_ORA
    sql = "SELECT IDTIPOTERMINALE, NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0 AND IDTIPOTERMINALE <> " & TT_TERMINALE_CONTROLLO_ACCESSI & _
        " ORDER BY NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstTipiTerminale.AddItem(rec("NOME"))
            lstTipiTerminale.ItemData(i) = rec("IDTIPOTERMINALE")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD_ORA

    MousePointer = mousePointerOld
End Sub

Private Sub CaricaListaSuperaree()
    Dim sql As String
    Dim rec As OraDynaset
    Dim mousePointerOld As Integer
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ApriConnessioneBD_ORA
    sql = "SELECT IDAREA, A.CODICE || ' - ' || A.NOME AS DESCR" & _
        " FROM AREA A, PRODOTTO P" & _
        " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND P.IDPIANTA = A.IDPIANTA" & _
        " AND A.IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
        " ORDER BY DESCR"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstSuperaree.AddItem (rec("DESCR"))
            lstSuperaree.ItemData(i) = rec("IDAREA")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD_ORA

    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub cmdEsci_Click()
    Call Esci
End Sub

Private Sub Conferma()
    Dim data As Date
    Dim ora As Integer
    Dim minuti As Integer
    Dim oraDurata As Integer
    Dim minutiDurata As Integer
    Dim oraDurataScadenza As Integer
    Dim minutiDurataScadenza As Integer
    Dim listaNonConformita As New Collection
    Dim ValoriCampiOK As Boolean
    Dim i As Long
    Dim j As Long
    Dim n As Long
    Dim numeroAttivitaEsistenti As Long
    Dim numeroAttivitaInserite As Long
    Dim idNuovaAttivita As Long
    Dim idTipoTerminale As Long
    Dim idSuperarea As Long
    Dim sql As String
    Dim rec As OraDynaset
    
    ValoriCampiOK = True

    If IsNull(dtpData.Value) Then
        dataOraPianificata = dataNulla
        Call listaNonConformita.Add("- la data deve essere specificata;")
    Else
        data = FormatDateTime(dtpData.Value, vbShortDate)
        If txtOra.Text <> "" Then
            If IsCampoOraCorretto(txtOra) Then
                ora = CInt(Trim(txtOra.Text))
            Else
                ValoriCampiOK = False
                Call listaNonConformita.Add("- il valore immesso sul campo Ora deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        Else
            ValoriCampiOK = False
            Call listaNonConformita.Add("- l'ora deve essere specificata;")
        End If
        If txtMinuti.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinuti) Then
                minuti = CInt(Trim(txtMinuti.Text))
            Else
                ValoriCampiOK = False
                Call listaNonConformita.Add("- il valore immesso sul campo Minuti scadenza deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        Else
            ValoriCampiOK = False
            Call listaNonConformita.Add("- i minuti devono essere specificati;")
        End If
        dataOraPianificata = FormatDateTime(data & " " & ora & ":" & minuti, vbGeneralDate)
    End If
        
    If listaNonConformita.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformita))
    Else
    
        Call ApriConnessioneBD_ORA
        Call ORADB.BeginTrans
        numeroAttivitaInserite = 0
        numeroAttivitaEsistenti = 0
        
        For i = 0 To lstTipiTerminale.ListCount - 1
            If lstTipiTerminale.Selected(i) Then
                idTipoTerminale = lstTipiTerminale.ItemData(i)
                
                If chkTuttoIlProdotto.Value = vbChecked Then
                    If (Not attivitaEsiste(idProdottoSelezionato, idNessunElementoSelezionato, idTipoTerminale, dataOraPianificata)) Then
                        idNuovaAttivita = OttieniIdentificatoreDaSequenza("SQ_ATTIVITAPIANIFICATA")
                        sql = "INSERT INTO ATTIVITAPIANIFICATA (IDATTIVITAPIANIFICATA, IDTIPOATTIVITAPIANIFICATA, IDORGANIZZAZIONE, IDPRODOTTO," & _
                            " IDSUPERAREA, IDTIPOTERMINALE, DATAORAPIANIFICATA) VALUES" & _
                            " (" & idNuovaAttivita & ", 1" & _
                            ", " & idOrganizzazioneSelezionata & _
                            ", " & idProdottoSelezionato & _
                            ", NULL" & _
                            ", " & idTipoTerminale & _
                            ", " & SqlDateTimeValue(dataOraPianificata) & ")"
                        n = ORADB.ExecuteSQL(sql)
                        numeroAttivitaInserite = numeroAttivitaInserite + n
                    Else
                        numeroAttivitaEsistenti = numeroAttivitaEsistenti + 1
                    End If
                Else
                    For j = 0 To lstSuperaree.ListCount - 1
                        If lstSuperaree.Selected(j) Then
                            idSuperarea = lstSuperaree.ItemData(j)
                    
                            If (Not attivitaEsiste(idProdottoSelezionato, idSuperarea, idTipoTerminale, dataOraPianificata)) Then
                                idNuovaAttivita = OttieniIdentificatoreDaSequenza("SQ_ATTIVITAPIANIFICATA")
                                sql = "INSERT INTO ATTIVITAPIANIFICATA (IDATTIVITAPIANIFICATA, IDTIPOATTIVITAPIANIFICATA, IDORGANIZZAZIONE, IDPRODOTTO," & _
                                    " IDSUPERAREA, IDTIPOTERMINALE, DATAORAPIANIFICATA) VALUES" & _
                                    " (" & idNuovaAttivita & ", 1" & _
                                    ", " & idOrganizzazioneSelezionata & _
                                    ", " & idProdottoSelezionato & _
                                    ", " & idSuperarea & _
                                    ", " & idTipoTerminale & _
                                    ", " & SqlDateTimeValue(dataOraPianificata) & ")"
                                n = ORADB.ExecuteSQL(sql)
                                numeroAttivitaInserite = numeroAttivitaInserite + n
                            Else
                                numeroAttivitaEsistenti = numeroAttivitaEsistenti + 1
                            End If
                        End If
                    Next j
                End If
            End If
        Next i
        
        Call ORADB.CommitTrans
        Call ChiudiConnessioneBD_ORA
    
        Call CaricaListaOperazioni
    End If
    
    MsgBox "Inserite " & numeroAttivitaInserite & " attivitą; " & numeroAttivitaEsistenti & " gią inserite."
    Call EliminaSelezioni
    Set listaNonConformita = Nothing

End Sub

Private Sub EliminaSelezioni()
    Dim i As Long
    
    For i = 0 To lstTipiTerminale.ListCount - 1
        lstTipiTerminale.Selected(i) = False
    Next i
    
    For i = 0 To lstSuperaree.ListCount - 1
        lstSuperaree.Selected(i) = False
    Next i
    
    chkTuttoIlProdotto.Value = vbUnchecked
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaListaOperazioni()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim s As String
    
    lstAttivita.Clear
    
    Call ApriConnessioneBD_ORA
    sql = "SELECT P.IDPRODOTTO, O.NOME AS ORGANIZZAZIONE, P.DESCRIZIONE AS PRODOTTO, R.DATAORAINIZIO, SUP.DESCRIZIONE AS SUPERAREA, TT.NOME AS TIPOTERMINALE, A.DATAORAPIANIFICATA" & _
        " FROM ORGANIZZAZIONE O, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, ATTIVITAPIANIFICATA A, AREA SUP, TIPOTERMINALE TT" & _
        " WHERE A.DATAORAPIANIFICATA > SYSDATE - 7" & _
        " AND A.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " AND SUP.IDAREA(+) = A.IDSUPERAREA" & _
        " AND TT.IDTIPOTERMINALE = A.IDTIPOTERMINALE" & _
        " AND A.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
        " ORDER BY A.DATAORAPIANIFICATA, P.IDPRODOTTO, SUP.DESCRIZIONE ASC"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            s = rec("IDPRODOTTO") & " - " & rec("ORGANIZZAZIONE") & " - " & rec("PRODOTTO") & " - " & rec("DATAORAINIZIO") & " - "
            If IsNull(rec("SUPERAREA")) Then
                s = s & " ---INTERO PRODOTTO ---"
            Else
                s = s & rec("SUPERAREA")
            End If
            s = s & " - " & rec("TIPOTERMINALE") & " - " & rec("DATAORAPIANIFICATA")
            lstAttivita.AddItem (s)
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD_ORA

End Sub

Private Function attivitaEsiste(idProdotto As Long, idSuperarea As Long, idTipoTerminale As Long, dataOraPianificata As Date)
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim s As String
    
    Call ApriConnessioneBD_ORA
    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM ATTIVITAPIANIFICATA" & _
        " WHERE IDPRODOTTO = " & idProdotto & _
        " AND IDSUPERAREA = " & IIf(idSuperarea = idNessunElementoSelezionato, "NULL", idSuperarea) & _
        " AND IDTIPOTERMINALE = " & idTipoTerminale & _
        " AND DATAORAPIANIFICATA = " & SqlDateTimeValue(dataOraPianificata)
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = 0 Then
            attivitaEsiste = False
        Else
            attivitaEsiste = True
        End If
    End If
    rec.Close
    Call ChiudiConnessioneBD_ORA

End Function

Public Sub SalvaListaOperazioniSuFileExcel()
    Dim workbook As workbook
    Dim workSheet As workSheet
    Dim p As clsPosto
    Dim i As Long
    Dim numeroProtezioni As Long
    Dim sql As String
    Dim rec As OraDynaset
    Dim s As String
    Dim nomeFileExcel As String
    
    nomeFileExcel = pathApplicazione & "reportOperazioni" & "_________"
        
    Set excReport = New Excel.Application
    excReport.Visible = True
    Set workbook = excReport.Workbooks.Add
    Set workSheet = workbook.ActiveSheet

    workSheet.Rows.Cells.NumberFormat = "@"

    workSheet.Rows.Cells(1, 1) = "IDPRODOTTO"
    workSheet.Rows.Cells(1, 2) = "ORGANIZZAZIONE"
    workSheet.Rows.Cells(1, 3) = "PRODOTTO"
    workSheet.Rows.Cells(1, 4) = "DATAORAINIZIORAPPRESENTAZIONE"
    workSheet.Rows.Cells(1, 5) = "SUPERAREA"
    workSheet.Rows.Cells(1, 6) = "TIPOTERMINALE"
    workSheet.Rows.Cells(1, 7) = "DATAORAPIANIFICATA"
    workSheet.Rows.Cells(1, 8) = "DATAORAINIZIOATTIVITA"
    workSheet.Rows.Cells(1, 9) = "DATAORAFINEATTIVITA"
    workSheet.Rows.Cells(1, 10) = "ESITO"
    
    Call ApriConnessioneBD_ORA
    sql = "SELECT P.IDPRODOTTO, O.NOME AS ORGANIZZAZIONE, P.DESCRIZIONE AS PRODOTTO, TO_CHAR(R.DATAORAINIZIO, 'DD/MM/YYYY HH24:MI:SS') AS DATAORAINIZIORAPPRESENTAZIONE, SUP.DESCRIZIONE AS SUPERAREA, TT.NOME AS TIPOTERMINALE, " & _
        " TO_CHAR(A.DATAORAPIANIFICATA, 'DD/MM/YYYY HH24:MI:SS') AS DATAORAPIANIFICATA, TO_CHAR(A.DATAORAINIZIO, 'DD/MM/YYYY HH24:MI:SS') AS DATAORAINIZIOATTIVITA, TO_CHAR(A.DATAORAFINE, 'DD/MM/YYYY HH24:MI:SS') AS DATAORAFINEATTIVITA, A.ESITO, TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS') AS ADESSO" & _
        " FROM ORGANIZZAZIONE O, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, ATTIVITAPIANIFICATA A, AREA SUP, TIPOTERMINALE TT" & _
        " WHERE A.DATAORAPIANIFICATA > SYSDATE - 7" & _
        " AND A.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " AND SUP.IDAREA(+) = A.IDSUPERAREA" & _
        " AND TT.IDTIPOTERMINALE = A.IDTIPOTERMINALE" & _
        " AND A.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
        " ORDER BY A.DATAORAPIANIFICATA, P.IDPRODOTTO, SUP.DESCRIZIONE ASC"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        nomeFileExcel = pathApplicazione & "reportOperazioni" & rec("ADESSO")
        i = 2

        While Not rec.EOF
            workSheet.Rows.Cells(i, 1) = rec("IDPRODOTTO")
            workSheet.Rows.Cells(i, 2) = rec("ORGANIZZAZIONE")
            workSheet.Rows.Cells(i, 3) = rec("PRODOTTO")
            workSheet.Rows.Cells(i, 4) = rec("DATAORAINIZIORAPPRESENTAZIONE")
            workSheet.Rows.Cells(i, 5) = rec("SUPERAREA")
            workSheet.Rows.Cells(i, 6) = rec("TIPOTERMINALE")
            workSheet.Rows.Cells(i, 7) = rec("DATAORAPIANIFICATA")
            If Not (IsNull(rec("DATAORAINIZIOATTIVITA"))) Then
                workSheet.Rows.Cells(i, 8) = rec("DATAORAINIZIOATTIVITA")
            End If
            If Not (IsNull(rec("DATAORAFINEATTIVITA"))) Then
                workSheet.Rows.Cells(i, 9) = rec("DATAORAFINEATTIVITA")
            End If
            workSheet.Rows.Cells(i, 10) = rec("ESITO")
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD_ORA
    workSheet.Rows("1:1").Font.Bold = True

    Call workSheet.Columns("A:L").Select
    Call workSheet.Columns("A:L").EntireColumn.AutoFit
    Call workbook.SaveAs(nomeFileExcel)
    Call workbook.Close
    Call excReport.Quit
    Set workbook = Nothing
    Set excReport = Nothing
End Sub

Private Sub cmdSalvaSuFileExcel_Click()
    Call SalvaListaOperazioniSuFileExcel
End Sub

