VERSION 5.00
Begin VB.Form frmClonazioneProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraDatiNuovoProdotto 
      Caption         =   "Dati nuovo prodotto "
      Height          =   6975
      Left            =   120
      TabIndex        =   24
      Top             =   600
      Width           =   9855
      Begin VB.TextBox txtDescrizionePOS 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         MaxLength       =   21
         TabIndex        =   34
         Top             =   1680
         Width           =   2055
      End
      Begin VB.ComboBox cmbImportazioneCriteriMigliorPosto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   5040
         Width           =   4700
      End
      Begin VB.CheckBox chkImportazioneCriteriMigliorPosto 
         Caption         =   "Importa criteri miglior posto"
         Height          =   195
         Left            =   180
         TabIndex        =   11
         Top             =   4800
         Width           =   2475
      End
      Begin VB.CheckBox chkImportazioneOperativita 
         Caption         =   "Importa operativita / catalogo WEB"
         Height          =   195
         Left            =   180
         TabIndex        =   9
         Top             =   4080
         Width           =   4515
      End
      Begin VB.ComboBox cmbImportazioneOperativita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   4320
         Width           =   4700
      End
      Begin VB.CheckBox chkImportazioneProtezioniSuPosti 
         Caption         =   "Importa protezioni su posti"
         Height          =   195
         Left            =   180
         TabIndex        =   15
         Top             =   6240
         Width           =   2355
      End
      Begin VB.ComboBox cmbImportazioneProtezioniSuPosti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   6480
         Width           =   4700
      End
      Begin VB.ComboBox cmbImportazioneCausaliProtezione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   5760
         Width           =   4700
      End
      Begin VB.ComboBox cmbImportazioneClassiPuntiVendita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   3600
         Width           =   4700
      End
      Begin VB.ComboBox cmbImportazionePrezzi 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   2880
         Width           =   4700
      End
      Begin VB.CheckBox chkImportazioneCausaliProtezione 
         Caption         =   "Importa causali di protezione"
         Height          =   195
         Left            =   180
         TabIndex        =   13
         Top             =   5520
         Width           =   2355
      End
      Begin VB.CheckBox chkImportazioneClassiPuntiVendita 
         Caption         =   "Importa composizione classi punti vendita"
         Height          =   195
         Left            =   180
         TabIndex        =   7
         Top             =   3360
         Width           =   4575
      End
      Begin VB.CheckBox chkImportazionePrezzi 
         Caption         =   "Importa Tariffe/Periodi comm.li/Prezzi"
         Height          =   195
         Left            =   180
         TabIndex        =   5
         Top             =   2640
         Width           =   2955
      End
      Begin VB.ListBox lstCodiciPiantaOrganizzazione 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   5100
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   2880
         Width           =   4515
      End
      Begin VB.TextBox txtCodiceTL 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2340
         MaxLength       =   6
         TabIndex        =   3
         Top             =   2100
         Width           =   795
      End
      Begin VB.TextBox txtCodiceTLTipoProdotto 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   2
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   2100
         Width           =   315
      End
      Begin VB.TextBox txtCodiceTLOrganizzazione 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   2100
         Width           =   555
      End
      Begin VB.CheckBox chkImportaRappresentazioni 
         Caption         =   "Importa rappresentazioni"
         Height          =   195
         Left            =   3840
         TabIndex        =   4
         Top             =   2160
         Width           =   2355
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   0
         Top             =   300
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1500
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   720
         Width           =   7635
      End
      Begin VB.TextBox txtDescrizioneAlternativa 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   2
         Top             =   1680
         Width           =   3255
      End
      Begin VB.Label lblDescrizionePOS 
         Alignment       =   1  'Right Justify
         Caption         =   "Descr. POS"
         Height          =   255
         Left            =   5520
         TabIndex        =   33
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label lblCodiciOrganizzazione 
         Caption         =   "Codici giā presenti per l'Organizzazione"
         Height          =   195
         Left            =   5100
         TabIndex        =   32
         Top             =   2640
         Width           =   3015
      End
      Begin VB.Label lblCodiceTerminaleLotto 
         Alignment       =   1  'Right Justify
         Caption         =   "Codice TL"
         Height          =   255
         Left            =   480
         TabIndex        =   30
         Top             =   2160
         Width           =   855
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   300
         TabIndex        =   27
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblDescrizioneAlternativa 
         Alignment       =   1  'Right Justify
         Caption         =   "Descr. alternativa"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1800
         Width           =   1335
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8580
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   240
      Width           =   3255
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   19
      Top             =   8040
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   23
      Top             =   0
      Width           =   2595
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Clonazione del Prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   6975
   End
End
Attribute VB_Name = "frmClonazioneProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private newNome As String
Private newDescrizione As String
Private newDescrizioneAlternativa As String
Private newDescrizionePOS As String
Private newCodiceTL As String
Private codiceTLOrganizzazione As String
Private codiceTLTipoProdotto As String
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private idProdottoPrezzi As Long
Private idProdottoClassiPuntiVendita As Long
Private idProdottoOperativita As Long
Private idProdottoCausaliProtezione As Long
Private idProdottoProtezioniSuPosto As Long
Private idProdottoMigliorPosto As Long
Private internalEvent As Boolean
Private idPiantaSelezionata As Long
Private isProdottoDefinitoSuMedesimaPianta As Boolean
Private idClasseProdotto As Long

Private importaRappresentazioni As ValoreBooleanoEnum
Private importaPrezzi As ValoreBooleanoEnum
Private importaCausaliProtezione As ValoreBooleanoEnum
Private importaProtezioniSuPosti As ValoreBooleanoEnum
Private importaClassiPuntiVendita As ValoreBooleanoEnum
Private importaOperativita As ValoreBooleanoEnum
Private importaCriteriMigliorPosto As ValoreBooleanoEnum
Private exitCode As ExitCodeEnum

Private Sub AggiornaAbilitazioneControlli()
    txtCodiceTLTipoProdotto.Enabled = False
    txtCodiceTLOrganizzazione.Enabled = False
    txtInfo1.Enabled = False
    lblInfo1.Caption = "Prodotto"
    cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
        Trim(txtDescrizioneAlternativa.Text) <> "" And _
        Len(Trim(txtCodiceTL)) = 6)
    chkImportaRappresentazioni.Enabled = (idClasseProdotto <> CPR_BIGLIETTERIA_ORDINARIA)
    cmbImportazionePrezzi.Enabled = (importaPrezzi = VB_VERO)
    cmbImportazioneCausaliProtezione.Enabled = (importaCausaliProtezione = VB_VERO)
    cmbImportazioneCriteriMigliorPosto.Enabled = (importaCriteriMigliorPosto = VB_VERO)
    cmbImportazioneClassiPuntiVendita.Enabled = (importaClassiPuntiVendita = VB_VERO)
    cmbImportazioneOperativita.Enabled = (importaOperativita = VB_VERO)
    chkImportazioneProtezioniSuPosti.Enabled = (importaCausaliProtezione = VB_VERO And _
        isProdottoDefinitoSuMedesimaPianta)
    cmbImportazioneProtezioniSuPosti.Enabled = False
End Sub

Public Sub Init()
    Dim sql As String
    
    Call Variabili_Init
    sql = "SELECT IDPRODOTTO ID, CODICETERMINALELOTTO COD, NOME"
    sql = sql & " FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY COD"
    Call CaricaDallaBaseDati
    Call CaricaValoriLista(lstCodiciPiantaOrganizzazione, sql, "COD", "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub Variabili_Init()
    idProdottoClassiPuntiVendita = idProdottoSelezionato
    idProdottoOperativita = idProdottoSelezionato
    idProdottoPrezzi = idProdottoSelezionato
    idProdottoCausaliProtezione = idProdottoSelezionato
    idProdottoProtezioniSuPosto = idProdottoSelezionato
    idProdottoMigliorPosto = idProdottoSelezionato
    importaCausaliProtezione = VB_FALSO
    importaClassiPuntiVendita = VB_FALSO
    importaOperativita = VB_FALSO
    importaPrezzi = VB_FALSO
    importaProtezioniSuPosti = VB_FALSO
    importaRappresentazioni = VB_FALSO
    importaCriteriMigliorPosto = VB_FALSO
    newNome = ""
    newCodiceTL = ""
    newDescrizione = ""
    newDescrizioneAlternativa = ""
    newDescrizionePOS = ""
    isProdottoDefinitoSuMedesimaPianta = False
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtCodiceTLOrganizzazione.Text = codiceTLOrganizzazione
    txtCodiceTLTipoProdotto.Text = codiceTLTipoProdotto
    txtInfo1.Text = nomeProdottoSelezionato
    txtNome.Text = newNome
    txtDescrizione.Text = newDescrizione
    txtDescrizioneAlternativa.Text = newDescrizioneAlternativa
    txtDescrizionePOS.Text = newDescrizionePOS
    txtCodiceTL.Text = newCodiceTL
    chkImportaRappresentazioni.Value = importaRappresentazioni
    chkImportazioneCausaliProtezione.Value = importaCausaliProtezione
    chkImportazioneClassiPuntiVendita.Value = importaClassiPuntiVendita
    chkImportazioneOperativita.Value = importaOperativita
    chkImportazionePrezzi.Value = importaPrezzi
    chkImportazioneProtezioniSuPosti.Value = importaProtezioniSuPosti
    chkImportazioneCriteriMigliorPosto.Value = importaCriteriMigliorPosto
    
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdProdottoSelezionato(idP As Long)
    idProdottoSelezionato = idP
End Sub

Public Sub SetIdClasseProdottoSelezionato(idCP As Long)
    idClasseProdotto = idCP
End Sub

Public Sub SetIdOrganizzazioneSelezionata(idO As Long)
    idOrganizzazioneSelezionata = idO
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    
    valoriCampiOK = True
    Set listaNonConformitā = New Collection
    newNome = Trim(txtNome.Text)
'    If ViolataUnicitā("PRODOTTO", "NOME = " & SqlStringValue(newNome), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
'        "", "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(newNome) & _
'            " č giā presente in DB per la stessa Stagione e la stessa Organizzazione;")
'    End If
    newDescrizione = Trim(txtDescrizione.Text)
    newDescrizioneAlternativa = Trim(txtDescrizioneAlternativa.Text)
    newDescrizionePOS = Trim(txtDescrizionePOS.Text)
    newCodiceTL = UCase(Trim(txtCodiceTL.Text))
    If ViolataUnicitā("PRODOTTO", "CODICETERMINALELOTTO = " & SqlStringValue(newCodiceTL), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
        "", "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice TL = " & SqlStringValue(newCodiceTL) & _
            " č giā presente in DB per la stessa Organizzazione;")
    End If
    importaRappresentazioni = IIf(chkImportaRappresentazioni.Value = vbChecked, VB_VERO, VB_FALSO)
    importaPrezzi = IIf(chkImportazionePrezzi.Value = vbChecked, VB_VERO, VB_FALSO)
    importaCausaliProtezione = IIf(chkImportazioneCausaliProtezione.Value = vbChecked, VB_VERO, VB_FALSO)
    importaProtezioniSuPosti = IIf(chkImportazioneProtezioniSuPosti.Value = vbChecked, VB_VERO, VB_FALSO)
    importaClassiPuntiVendita = IIf(chkImportazioneClassiPuntiVendita.Value = vbChecked, VB_VERO, VB_FALSO)
    importaOperativita = IIf(chkImportazioneOperativita.Value = vbChecked, VB_VERO, VB_FALSO)
    importaCriteriMigliorPosto = IIf(chkImportazioneCriteriMigliorPosto.Value = vbChecked, VB_VERO, VB_FALSO)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If
End Function

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT PRODOTTO.NOME AS NOME, PRODOTTO.IDPIANTA IDP,"
    sql = sql & " ORGANIZZAZIONE.CODICETERMINALELOTTO AS CODTLORG,"
    sql = sql & " TIPOPRODOTTO.CODICETERMINALELOTTO AS CODTLTIPO"
    sql = sql & " FROM PRODOTTO, ORGANIZZAZIONE, TIPOPRODOTTO WHERE"
    sql = sql & " (PRODOTTO.IDORGANIZZAZIONE = ORGANIZZAZIONE.IDORGANIZZAZIONE) AND"
    sql = sql & " (PRODOTTO.IDTIPOPRODOTTO = TIPOPRODOTTO.IDTIPOPRODOTTO) AND"
    sql = sql & " (PRODOTTO.IDPRODOTTO = " & idProdottoSelezionato & ")"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeProdottoSelezionato = rec("NOME")
        codiceTLOrganizzazione = rec("CODTLORG")
        codiceTLTipoProdotto = rec("CODTLTIPO")
        idPiantaSelezionata = rec("IDP")
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub cmbImportazioneOperativita_Click()
    If Not internalEvent Then
        idProdottoOperativita = cmbImportazioneOperativita.ItemData(cmbImportazioneOperativita.ListIndex)
    End If
End Sub

Private Sub cmbImportazioneCriteriMigliorPosto_Click()
    If Not internalEvent Then
        idProdottoMigliorPosto = cmbImportazioneCriteriMigliorPosto.ItemData(cmbImportazioneCriteriMigliorPosto.ListIndex)
    End If
End Sub

Private Sub cmbImportazioneProtezioniSuPosti_Click()
    If Not internalEvent Then
        idProdottoProtezioniSuPosto = cmbImportazioneProtezioniSuPosti.ItemData(cmbImportazioneProtezioniSuPosti.ListIndex)
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
'    Call PulisciValoriCampi
    Call Variabili_Init
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    
    If valoriCampiOK = True Then
        dataOraInizio = FormatDateTime(Now, vbGeneralDate)
        If IsClonazioneProdottoOK(idProdottoSelezionato, newNome, newDescrizione, _
                newDescrizioneAlternativa, newDescrizionePOS, newCodiceTL, importaRappresentazioni, _
                idProdottoPrezzi, idProdottoClassiPuntiVendita, idProdottoCausaliProtezione, _
                idProdottoProtezioniSuPosto, idProdottoMigliorPosto, idProdottoOperativita) Then
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("NotificaClonazioneProdotto", newNome, nomeProdottoSelezionato, dataOraInizio, dataOraFine)
        Else
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("ErroreClonazione", dataOraInizio, dataOraFine)
        End If
        Call Esci
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub PulisciValoriCampi()
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDescrizioneAlternativa.Text = ""
    txtCodiceTL.Text = ""
    chkImportaRappresentazioni.Value = vbUnchecked
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & "'" & newNome & "'")
'    Call listaCampiValoriUnici.Add("CODICETERMINALELOTTO = " & "'" & newCodiceTL & "'")
'End Sub

Private Sub lstCodiciPiantaOrganizzazione_Click()
    Call VisualizzaListBoxToolTip(lstCodiciPiantaOrganizzazione, lstCodiciPiantaOrganizzazione.Text)
End Sub

Private Sub txtCodiceTL_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizioneAlternativa_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNome_Change()
'    If Not internalEvent Then
        Call ValorizzaAutomaticamenteCampoCorrelato
        Call AggiornaAbilitazioneControlli
'    End If
End Sub

Private Sub ValorizzaAutomaticamenteCampoCorrelato()
    txtDescrizioneAlternativa.Text = Left$(txtNome.Text, 30)
End Sub

Private Sub CaricaValoriLista(lst As ListBox, strSQL As String, NomeCampo1 As String, Optional nomeCampo2 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaVisualizzata As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            stringaVisualizzata = rec(NomeCampo1) & " - " & rec(nomeCampo2)
            lst.AddItem stringaVisualizzata
            lst.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Private Sub chkImportazionePrezzi_Click()
    If Not internalEvent Then
        Call chkImportazionePrezzi_Update
    End If
End Sub

Private Sub chkImportazionePrezzi_Update()
    If chkImportazionePrezzi.Value = vbChecked Then
        importaPrezzi = VB_VERO
        Call cmbImportazionePrezzi.Clear
        Call CaricaListaProdottiPrezzi
        Call SelezionaElementoSuCombo(cmbImportazionePrezzi, idProdottoSelezionato)
    ElseIf chkImportazionePrezzi.Value = vbUnchecked Then
        importaPrezzi = VB_FALSO
        idProdottoPrezzi = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaProdottiPrezzi()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME ||' - '|| IDPRODOTTO LABEL FROM PRODOTTO"
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
    sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY NOME, -IDPRODOTTO"
    Call cmbImportazionePrezzi.Clear
    Call CaricaValoriCombo(cmbImportazionePrezzi, sql, "LABEL")
End Sub

Private Sub cmbImportazionePrezzi_Click()
    If Not internalEvent Then
        idProdottoPrezzi = cmbImportazionePrezzi.ItemData(cmbImportazionePrezzi.ListIndex)
    End If
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub chkImportazioneClassiPuntiVendita_Click()
    If Not internalEvent Then
        Call chkImportazioneClassiPuntiVendita_Update
    End If
End Sub

Private Sub chkImportazioneClassiPuntiVendita_Update()
    If chkImportazioneClassiPuntiVendita.Value = vbChecked Then
        importaClassiPuntiVendita = VB_VERO
        Call cmbImportazioneClassiPuntiVendita.Clear
        Call CaricaListaProdottiDirittiOperatori
        Call SelezionaElementoSuCombo(cmbImportazioneClassiPuntiVendita, idProdottoSelezionato)
    ElseIf chkImportazioneClassiPuntiVendita.Value = vbUnchecked Then
        importaClassiPuntiVendita = VB_FALSO
        idProdottoClassiPuntiVendita = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaProdottiDirittiOperatori()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME ||' - '|| IDPRODOTTO LABEL FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY NOME, -IDPRODOTTO"
    Call cmbImportazioneClassiPuntiVendita.Clear
    Call CaricaValoriCombo(cmbImportazioneClassiPuntiVendita, sql, "LABEL")
End Sub

Private Sub chkImportazioneCriteriMigliorPosto_Click()
    If Not internalEvent Then
        Call chkImportazioneCriteriMigliorPosto_Update
    End If
End Sub

Private Sub chkImportazioneCriteriMigliorPosto_Update()
    If chkImportazioneCriteriMigliorPosto.Value = vbChecked Then
        importaCriteriMigliorPosto = VB_VERO
        Call cmbImportazioneCriteriMigliorPosto.Clear
        Call CaricaListaProdottiMigliorPosto
        Call SelezionaElementoSuCombo(cmbImportazioneCriteriMigliorPosto, idProdottoSelezionato)
    ElseIf chkImportazioneCriteriMigliorPosto.Value = vbUnchecked Then
        importaCriteriMigliorPosto = VB_FALSO
        idProdottoMigliorPosto = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaProdottiMigliorPosto()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME ||' - '|| IDPRODOTTO LABEL FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND IDPIANTA = " & idPiantaSelezionata
    sql = sql & " ORDER BY NOME, -IDPRODOTTO"
    Call cmbImportazioneCriteriMigliorPosto.Clear
    Call CaricaValoriCombo(cmbImportazioneCriteriMigliorPosto, sql, "LABEL")
End Sub

Private Sub cmbImportazioneClassiPuntiVendita_Click()
    If Not internalEvent Then
        idProdottoClassiPuntiVendita = cmbImportazioneClassiPuntiVendita.ItemData(cmbImportazioneClassiPuntiVendita.ListIndex)
    End If
End Sub

Private Sub chkImportazioneCausaliProtezione_Click()
    If Not internalEvent Then
        Call chkImportazioneCausaliProtezione_Update
    End If
End Sub

Private Sub chkImportazioneCausaliProtezione_Update()
    If chkImportazioneCausaliProtezione.Value = vbChecked Then
        importaCausaliProtezione = VB_VERO
        Call cmbImportazioneCausaliProtezione.Clear
        Call CaricaListaProdottiCausaliProtezione
        Call SelezionaElementoSuCombo(cmbImportazioneCausaliProtezione, idProdottoSelezionato)
    ElseIf chkImportazioneCausaliProtezione.Value = vbUnchecked Then
        importaCausaliProtezione = VB_FALSO
        idProdottoCausaliProtezione = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaProdottiCausaliProtezione()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME ||' - '|| IDPRODOTTO LABEL FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY NOME, -IDPRODOTTO"
    Call cmbImportazioneCausaliProtezione.Clear
    Call CaricaValoriCombo(cmbImportazioneCausaliProtezione, sql, "LABEL")
End Sub

Private Sub cmbImportazioneCausaliProtezione_Click()
    If Not internalEvent Then
        idProdottoCausaliProtezione = cmbImportazioneCausaliProtezione.ItemData(cmbImportazioneCausaliProtezione.ListIndex)
        Call SelezionaProdottoPerProtezioniSuPosti
    End If
End Sub

Private Sub SelezionaProdottoPerProtezioniSuPosti()
    If IdPiantaCausaliProtezione(idProdottoCausaliProtezione) = idPiantaSelezionata Then
        isProdottoDefinitoSuMedesimaPianta = True
        idProdottoProtezioniSuPosto = idProdottoCausaliProtezione
    Else
        isProdottoDefinitoSuMedesimaPianta = False
        idProdottoProtezioniSuPosto = idNessunElementoSelezionato
        importaProtezioniSuPosti = VB_FALSO
    End If
    Call CaricaListaProdottiProtezioniSuPosti
    Call SelezionaElementoSuCombo(cmbImportazioneProtezioniSuPosti, idProdottoProtezioniSuPosto)
    idProdottoProtezioniSuPosto = idNessunElementoSelezionato
    importaProtezioniSuPosti = VB_FALSO
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkImportazioneOperativita_Click()
    If Not internalEvent Then
        Call chkImportazioneOperativita_Update
    End If
End Sub

Private Sub chkImportazioneOperativita_Update()
    If chkImportazioneOperativita.Value = vbChecked Then
        importaOperativita = VB_VERO
        Call cmbImportazioneOperativita.Clear
        Call CaricaListaProdottiOperativita
        Call SelezionaElementoSuCombo(cmbImportazioneOperativita, idProdottoSelezionato)
    ElseIf chkImportazioneOperativita.Value = vbUnchecked Then
        importaOperativita = VB_FALSO
        idProdottoOperativita = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaProdottiOperativita()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME ||' - '|| IDPRODOTTO LABEL FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY NOME, -IDPRODOTTO"
    Call cmbImportazioneOperativita.Clear
    Call CaricaValoriCombo(cmbImportazioneOperativita, sql, "LABEL")
End Sub

Private Sub chkImportazioneProtezioniSuPosti_Click()
    If Not internalEvent Then
        Call chkImportazioneProtezioniSuPosti_Update
    End If
End Sub

Private Sub chkImportazioneProtezioniSuPosti_Update()
    If chkImportazioneProtezioniSuPosti.Value = vbChecked Then
        importaProtezioniSuPosti = VB_VERO
        idProdottoProtezioniSuPosto = cmbImportazioneProtezioniSuPosti.ItemData(cmbImportazioneProtezioniSuPosti.ListIndex)
        Call SelezionaElementoSuCombo(cmbImportazioneProtezioniSuPosti, idProdottoSelezionato)
    ElseIf chkImportazioneProtezioniSuPosti.Value = vbUnchecked Then
        importaProtezioniSuPosti = VB_FALSO
        idProdottoProtezioniSuPosto = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaProdottiProtezioniSuPosti()
    Dim sql As String
    
    sql = "SELECT IDPRODOTTO ID, NOME ||' - '|| IDPRODOTTO LABEL FROM PRODOTTO"
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
    sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY NOME, -IDPRODOTTO"
    Call cmbImportazioneProtezioniSuPosti.Clear
    Call CaricaValoriCombo(cmbImportazioneProtezioniSuPosti, sql, "LABEL")
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Function IdPiantaCausaliProtezione(idProdotto) As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idProdotto
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        rec.MoveFirst
        id = rec("IDPIANTA").Value
    rec.Close
    IdPiantaCausaliProtezione = id
    
    Call ChiudiConnessioneBD
    
End Function


