VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAssocTariffa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idTariffa As Long
Public utilizzabileDaTL As Boolean
Public labelTariffa As String
'Public listaTerminaliAttivi As New Collection

Public Function Clona() As clsAssocTariffa
    Dim b As clsAssocTariffa

    Set b = New clsAssocTariffa

    b.idTariffa = idTariffa
    b.labelTariffa = labelTariffa

    Set Clona = b
End Function

