VERSION 5.00
Begin VB.Form frmReportStatoPosti 
   Caption         =   "Report stato posti"
   ClientHeight    =   7380
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8250
   LinkTopic       =   "Form1"
   ScaleHeight     =   7380
   ScaleWidth      =   8250
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCreaFileExcel 
      Caption         =   "Crea file Excel"
      Height          =   495
      Left            =   3120
      TabIndex        =   17
      Top             =   6120
      Width           =   2055
   End
   Begin VB.Frame frmTipoStatoPosti 
      Caption         =   "Tipo stato posti"
      Height          =   4335
      Left            =   6000
      TabIndex        =   12
      Top             =   1560
      Width           =   2115
      Begin VB.OptionButton optSoloPostiProtetti 
         Caption         =   "solo posti protetti"
         Height          =   195
         Left            =   240
         TabIndex        =   16
         Top             =   1440
         Width           =   1755
      End
      Begin VB.OptionButton optSoloPostiVenduti 
         Caption         =   "solo posti venduti"
         Height          =   195
         Left            =   240
         TabIndex        =   15
         Top             =   1080
         Width           =   1755
      End
      Begin VB.OptionButton optSoloPostiLiberi 
         Caption         =   "solo posti liberi"
         Height          =   195
         Left            =   240
         TabIndex        =   14
         Top             =   720
         Width           =   1395
      End
      Begin VB.OptionButton optTuttiIPosti 
         Caption         =   "tutti i posti"
         Height          =   195
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Value           =   -1  'True
         Width           =   1395
      End
   End
   Begin VB.CommandButton cmdChiudi 
      Caption         =   "Chiudi"
      Height          =   495
      Left            =   3120
      TabIndex        =   11
      Top             =   6720
      Width           =   2055
   End
   Begin VB.Frame fraSelezioneSuPianta 
      Caption         =   "Selezione su pianta "
      Height          =   4335
      Left            =   120
      TabIndex        =   4
      Top             =   1560
      Width           =   5655
      Begin VB.OptionButton optPianta 
         Caption         =   "tutta la pianta"
         Height          =   195
         Left            =   180
         TabIndex        =   10
         Top             =   300
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton optSuperaree 
         Caption         =   "superaree"
         Height          =   195
         Left            =   1800
         TabIndex        =   9
         Top             =   300
         Width           =   1395
      End
      Begin VB.OptionButton optAree 
         Caption         =   "aree"
         Height          =   195
         Left            =   3240
         TabIndex        =   8
         Top             =   300
         Width           =   675
      End
      Begin VB.ListBox lstAreeSuperaree 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3180
         Left            =   180
         Style           =   1  'Checkbox
         TabIndex        =   7
         Top             =   600
         Width           =   5295
      End
      Begin VB.CommandButton cmdSelezionaTutti 
         Caption         =   "seleziona tutti"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   3780
         Width           =   2595
      End
      Begin VB.CommandButton cmdDeselezionaTutti 
         Caption         =   "deseleziona tutti"
         Height          =   435
         Left            =   2880
         TabIndex        =   5
         Top             =   3780
         Width           =   2595
      End
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmReportStatoPosti.frx":0000
      Left            =   120
      List            =   "frmReportStatoPosti.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   8025
   End
   Begin VB.ComboBox cmbProdotto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmReportStatoPosti.frx":0004
      Left            =   120
      List            =   "frmReportStatoPosti.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1080
      Width           =   8025
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblProdotto 
      Caption         =   "Prodotto"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   1695
   End
End
Attribute VB_Name = "frmReportStatoPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idPiantaSelezionata As Long
Private tipoSelezione As TipoSelezioneSuPiantaEnum
Private listaIdAreeSuperareeSelezionate As Collection
Private tuttaLaPianta As Boolean
Private insiemeSuperaree As Boolean
Private insiemeAree As Boolean
Private tuttiIPosti As Boolean
Private soloPostiLiberi As Boolean
Private soloPostiVenduti As Boolean
Private soloPostiProtetti As Boolean

Private internalEvent As Boolean

Public Sub Init()
    Call CaricaComboOrganizzazione
    tuttaLaPianta = True
    insiemeSuperaree = False
    insiemeAree = False
    tuttiIPosti = True
    soloPostiLiberi = False
    soloPostiVenduti = False
    soloPostiProtetti = False
    idProdottoSelezionato = idNessunElementoSelezionato
    Call Me.Show
End Sub

Private Sub CaricaComboOrganizzazione()
    Dim sqlOrg As String
    
    Call cmbOrganizzazione.Clear
    sqlOrg = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE ORDER BY NOME"
    Call CaricaValoriCombo3(cmbOrganizzazione, sqlOrg, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbOrganizzazione_Click()
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    lstAreeSuperaree.Clear
    Call CaricaComboProdotto
End Sub

Private Sub CaricaComboProdotto()
    Dim sqlProd As String
    Dim idDaRilevare As Variant
    
    Call cmbProdotto.Clear
    sqlProd = "SELECT P.IDPRODOTTO ID, NOME || ' - ' || TO_CHAR(MIN(DATAORAINIZIO), 'DD-MM-YYYY HH24:MI:SS') AS NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND IDTIPOSTATOPRODOTTO IN (" & TSP_ATTIVO & ", " & TSP_ATTIVABILE & ")" & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " GROUP BY P.IDPRODOTTO, NOME, IDPIANTA" & _
        " ORDER BY MIN(DATAORAINIZIO) DESC"
        
        Call CaricaValoriCombo3(cmbProdotto, sqlProd, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbProdotto_Click()
    Call SelezionaDatiProdotto
End Sub

Private Sub SelezionaDatiProdotto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
    idPiantaSelezionata = idNessunElementoSelezionato
    
    Call ApriConnessioneBD
    sql = "SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        idPiantaSelezionata = rec("IDPIANTA")
    End If
    rec.Close
    Call ChiudiConnessioneBD
End Sub

Private Sub optPianta_Click()
    If Not internalEvent Then
        tipoSelezione = TSSP_TUTTA_LA_PIANTA
    End If
End Sub

Private Sub optSuperaree_Click()
    If Not internalEvent Then
        tipoSelezione = TSSP_SUPERAREE
        Call CaricaValoriLstAreeSuperaree(tipoSelezione)
    End If
End Sub

Private Sub optAree_Click()
    If Not internalEvent Then
        tipoSelezione = TSSP_AREE
        Call CaricaValoriLstAreeSuperaree(tipoSelezione)
    End If
End Sub

Private Sub CaricaValoriLstAreeSuperaree(tipo As TipoSelezioneSuPiantaEnum)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idArea As Long
    Dim descr As String
    Dim i As Integer
    
    i = 1
    Call ApriConnessioneBD
    Call lstAreeSuperaree.Clear
    Set listaIdAreeSuperareeSelezionate = New Collection
    Select Case tipo
        Case TSSP_AREE
            sql = "SELECT IDAREA, CODICE, NOME" & _
                " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & _
                " AND IDTIPOAREA IN (2, 3)" & _
                " ORDER BY CODICE, NOME"
        Case TSSP_SUPERAREE
            sql = "SELECT IDAREA, CODICE, NOME" & _
                " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & _
                " AND IDTIPOAREA IN (5, 6)" & _
                " ORDER BY CODICE, NOME"
    End Select
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idArea = rec("IDAREA")
            descr = rec("CODICE") & " / " & rec("NOME")
            Call lstAreeSuperaree.AddItem(descr)
            lstAreeSuperaree.ItemData(i - 1) = idArea
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD
End Sub

Private Sub lstAreeSuperaree_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaAreaSuperarea
    End If
End Sub

Private Sub SelezionaDeselezionaAreaSuperarea()
    Dim idArea As Long
    Dim descrizioneArea As String
    
    idArea = lstAreeSuperaree.ItemData(lstAreeSuperaree.ListIndex)
    descrizioneArea = lstAreeSuperaree.Text
    If lstAreeSuperaree.Selected(lstAreeSuperaree.ListIndex) Then
        Call listaIdAreeSuperareeSelezionate.Add(idArea, ChiaveId(idArea))
    Else
        Call listaIdAreeSuperareeSelezionate.Remove(ChiaveId(idArea))
    End If
End Sub

Private Sub cmdSelezionaTutti_Click()
    Call SelezionaTutteLeAreeSuperaree
End Sub

Private Sub SelezionaTutteLeAreeSuperaree()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idArea As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set listaIdAreeSuperareeSelezionate = Nothing
    Set listaIdAreeSuperareeSelezionate = New Collection
    For i = 1 To lstAreeSuperaree.ListCount
        lstAreeSuperaree.Selected(i - 1) = True
        idArea = lstAreeSuperaree.ItemData(i - 1)
        Call listaIdAreeSuperareeSelezionate.Add(idArea, ChiaveId(idArea))
    Next i
    
    internalEvent = internalEventOld
End Sub

Private Sub cmdDeselezionaTutti_Click()
    Call DeselezionaTutteLeAreeSuperaree
End Sub

Private Sub DeselezionaTutteLeAreeSuperaree()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idArea As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstAreeSuperaree.ListCount
        lstAreeSuperaree.Selected(i - 1) = False
    Next i
    Set listaIdAreeSuperareeSelezionate = Nothing
    Set listaIdAreeSuperareeSelezionate = New Collection
    
    internalEvent = internalEventOld
End Sub

Private Sub cmdChiudi_Click()
    Unload Me
End Sub

Private Sub cmdCreaFileExcel_Click()
    Call CreaFileExcel
End Sub

Private Sub optTuttiIPosti_Click()
    tuttiIPosti = True
    soloPostiLiberi = False
    soloPostiVenduti = False
    soloPostiProtetti = False
End Sub

Private Sub optSoloPostiLiberi_Click()
    tuttiIPosti = False
    soloPostiLiberi = True
    soloPostiVenduti = False
    soloPostiProtetti = False
End Sub

Private Sub optSoloPostiVenduti_Click()
    tuttiIPosti = False
    soloPostiLiberi = False
    soloPostiVenduti = True
    soloPostiProtetti = False
End Sub

Private Sub optSoloPostiProtetti_Click()
    tuttiIPosti = False
    soloPostiLiberi = False
    soloPostiVenduti = False
    soloPostiProtetti = True
End Sub

Private Sub CreaFileExcel()
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim mousePointerOld As Integer
    Dim s As String

    If idProdottoSelezionato = idNessunElementoSelezionato Then
        MsgBox "Nessun prodotto selezionato!"
    Else
    
        mousePointerOld = MousePointer
        MousePointer = vbHourglass
        
        If tuttiIPosti Then
            sql = "SELECT PR.DESCRIZIONE, SA.NOME SUPERAREA, A.CODICE CODICE, A.NOME SETTORE, NOMEFILA, NOMEPOSTO," & _
                " CASE IDTIPOSTATOTITOLO" & _
                " WHEN 1 THEN 'RISERVATO'" & _
                " WHEN 2 THEN 'VENDUTO_NON_STAMPATO'" & _
                " WHEN 3 THEN 'VENDUTO_STAMPATO_PREVENTIVAMENTE'" & _
                " WHEN 4 THEN 'VENDUTO_STAMPATO_CONTESTUALMENTE'" & _
                " WHEN 5 THEN 'VENDUTO_STAMPATO_SUCCESSIVAMENTE'" & _
                " ELSE 'LIBERO'" & _
                " END STATOPOSTO," & _
                " SIMBOLOSUPIANTA" & _
                " FROM PRODOTTO PR, AREA A, AREA SA, POSTO P," & _
                "(" & _
                " SELECT T.IDPOSTO, IDTIPOSTATOTITOLO" & _
                " FROM TITOLO T, STATOTITOLO ST" & _
                " WHERE T.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO & _
                ") T," & _
                "(" & _
                "SELECT PP.IDPOSTO, SIMBOLOSUPIANTA" & _
                " FROM PROTEZIONEPOSTO PP, CAUSALEPROTEZIONE CP" & _
                " WHERE PP.idProdotto = " & idProdottoSelezionato & _
                " AND PP.IDCAUSALEPROTEZIONE = CP.IDCAUSALEPROTEZIONE" & _
                ") PP"
            sql = sql & " WHERE PR.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND PR.IDPIANTA = SA.IDPIANTA" & _
                " AND SA.IDAREA = A.IDAREA_PADRE" & _
                " AND A.IDAREA = P.IDAREA" & _
                " AND P.IDPOSTO = T.IDPOSTO(+)" & _
                " AND P.IDPOSTO = PP.IDPOSTO(+)"
        ElseIf soloPostiLiberi Then
            sql = "SELECT PR.DESCRIZIONE, SA.NOME SUPERAREA, A.CODICE CODICE, A.NOME SETTORE, NOMEFILA, NOMEPOSTO," & _
                " CASE IDTIPOSTATOTITOLO" & _
                " WHEN 1 THEN 'RISERVATO'" & _
                " WHEN 2 THEN 'VENDUTO_NON_STAMPATO'" & _
                " WHEN 3 THEN 'VENDUTO_STAMPATO_PREVENTIVAMENTE'" & _
                " WHEN 4 THEN 'VENDUTO_STAMPATO_CONTESTUALMENTE'" & _
                " WHEN 5 THEN 'VENDUTO_STAMPATO_SUCCESSIVAMENTE'" & _
                " ELSE 'LIBERO'" & _
                " END STATOPOSTO," & _
                " SIMBOLOSUPIANTA" & _
                " FROM PRODOTTO PR, AREA A, AREA SA, POSTO P," & _
                "(" & _
                " SELECT T.IDPOSTO, IDTIPOSTATOTITOLO" & _
                " FROM TITOLO T, STATOTITOLO ST" & _
                " WHERE T.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO & _
                ") T," & _
                "(" & _
                "SELECT PP.IDPOSTO, SIMBOLOSUPIANTA" & _
                " FROM PROTEZIONEPOSTO PP, CAUSALEPROTEZIONE CP" & _
                " WHERE PP.idProdotto = " & idProdottoSelezionato & _
                " AND PP.IDCAUSALEPROTEZIONE = CP.IDCAUSALEPROTEZIONE" & _
                ") PP"
            sql = sql & " WHERE PR.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND PR.IDPIANTA = SA.IDPIANTA" & _
                " AND SA.IDAREA = A.IDAREA_PADRE" & _
                " AND A.IDAREA = P.IDAREA" & _
                " AND P.IDPOSTO = T.IDPOSTO(+)" & _
                " AND T.IDPOSTO IS NULL" & _
                " AND P.IDPOSTO = PP.IDPOSTO(+)" & _
                " AND PP.IDPOSTO IS NULL"
        ElseIf soloPostiVenduti Then
            sql = "SELECT PR.DESCRIZIONE, SA.NOME SUPERAREA, A.CODICE CODICE, A.NOME SETTORE, NOMEFILA, NOMEPOSTO," & _
                " CASE IDTIPOSTATOTITOLO" & _
                " WHEN 1 THEN 'RISERVATO'" & _
                " WHEN 2 THEN 'VENDUTO_NON_STAMPATO'" & _
                " WHEN 3 THEN 'VENDUTO_STAMPATO_PREVENTIVAMENTE'" & _
                " WHEN 4 THEN 'VENDUTO_STAMPATO_CONTESTUALMENTE'" & _
                " WHEN 5 THEN 'VENDUTO_STAMPATO_SUCCESSIVAMENTE'" & _
                " ELSE 'LIBERO'" & _
                " END STATOPOSTO," & _
                " SIMBOLOSUPIANTA" & _
                " FROM PRODOTTO PR, AREA A, AREA SA, POSTO P," & _
                "(" & _
                " SELECT T.IDPOSTO, IDTIPOSTATOTITOLO" & _
                " FROM TITOLO T, STATOTITOLO ST" & _
                " WHERE T.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO & _
                ") T," & _
                "(" & _
                "SELECT PP.IDPOSTO, SIMBOLOSUPIANTA" & _
                " FROM PROTEZIONEPOSTO PP, CAUSALEPROTEZIONE CP" & _
                " WHERE PP.idProdotto = " & idProdottoSelezionato & _
                " AND PP.IDCAUSALEPROTEZIONE = CP.IDCAUSALEPROTEZIONE" & _
                ") PP"
            sql = sql & " WHERE PR.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND PR.IDPIANTA = SA.IDPIANTA" & _
                " AND SA.IDAREA = A.IDAREA_PADRE" & _
                " AND A.IDAREA = P.IDAREA" & _
                " AND P.IDPOSTO = T.IDPOSTO" & _
                " AND P.IDPOSTO = PP.IDPOSTO(+)"
        ElseIf soloPostiProtetti Then
            sql = "SELECT PR.DESCRIZIONE, SA.NOME SUPERAREA, A.CODICE CODICE, A.NOME SETTORE, NOMEFILA, NOMEPOSTO," & _
                " CASE IDTIPOSTATOTITOLO" & _
                " WHEN 1 THEN 'RISERVATO'" & _
                " WHEN 2 THEN 'VENDUTO_NON_STAMPATO'" & _
                " WHEN 3 THEN 'VENDUTO_STAMPATO_PREVENTIVAMENTE'" & _
                " WHEN 4 THEN 'VENDUTO_STAMPATO_CONTESTUALMENTE'" & _
                " WHEN 5 THEN 'VENDUTO_STAMPATO_SUCCESSIVAMENTE'" & _
                " ELSE 'LIBERO'" & _
                " END STATOPOSTO," & _
                " SIMBOLOSUPIANTA" & _
                " FROM PRODOTTO PR, AREA A, AREA SA, POSTO P," & _
                "(" & _
                " SELECT T.IDPOSTO, IDTIPOSTATOTITOLO" & _
                " FROM TITOLO T, STATOTITOLO ST" & _
                " WHERE T.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO & _
                " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO & _
                ") T," & _
                "(" & _
                "SELECT PP.IDPOSTO, SIMBOLOSUPIANTA" & _
                " FROM PROTEZIONEPOSTO PP, CAUSALEPROTEZIONE CP" & _
                " WHERE PP.idProdotto = " & idProdottoSelezionato & _
                " AND PP.IDCAUSALEPROTEZIONE = CP.IDCAUSALEPROTEZIONE" & _
                ") PP"
            sql = sql & " WHERE PR.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND PR.IDPIANTA = SA.IDPIANTA" & _
                " AND SA.IDAREA = A.IDAREA_PADRE" & _
                " AND A.IDAREA = P.IDAREA" & _
                " AND P.IDPOSTO = T.IDPOSTO(+)" & _
                " AND T.IDPOSTO IS NULL" & _
                " AND P.IDPOSTO = PP.IDPOSTO"
        End If
        
        If tipoSelezione = TSSP_SUPERAREE Then
            sql = sql & " AND SA.IDAREA IN (0"
            For i = 1 To lstAreeSuperaree.ListCount
                If lstAreeSuperaree.Selected(i - 1) Then
                    sql = sql & "," & lstAreeSuperaree.ItemData(i - 1)
                End If
            Next i
            sql = sql & ")"
        End If
        If tipoSelezione = TSSP_AREE Then
            sql = sql & " AND A.IDAREA IN (0"
            For i = 1 To lstAreeSuperaree.ListCount
                If lstAreeSuperaree.Selected(i - 1) Then
                    sql = sql & "," & lstAreeSuperaree.ItemData(i - 1)
                End If
            Next i
            sql = sql & ")"
        End If
        
        sql = sql & " ORDER BY SA.NOME, A.CODICE, LPAD(NOMEFILA, 2, ' '), LPAD(NOMEPOSTO, 3, ' ')"
        
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet
        
        xlsSheet.Rows.Cells(1, 1) = "PRODOTTO"
        xlsSheet.Rows.Cells(1, 2) = "SUPERAREA"
        xlsSheet.Rows.Cells(1, 3) = "SETTORE"
        xlsSheet.Rows.Cells(1, 4) = "NOMEFILA"
        xlsSheet.Rows.Cells(1, 5) = "NOMEPOSTO"
        xlsSheet.Rows.Cells(1, 6) = "STATOPOSTO"
        xlsSheet.Rows.Cells(1, 7) = "SIMBOLOSUPIANTA"
        
        Call ApriConnessioneBD
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 2
            While Not rec.EOF
                xlsSheet.Rows.Cells(i, 1) = rec("DESCRIZIONE")
                xlsSheet.Rows.Cells(i, 2) = rec("SUPERAREA")
                xlsSheet.Rows.Cells(i, 3) = rec("SETTORE")
                xlsSheet.Rows.Cells(i, 4) = rec("NOMEFILA")
                xlsSheet.Rows.Cells(i, 5) = rec("NOMEPOSTO")
                xlsSheet.Rows.Cells(i, 6) = rec("STATOPOSTO")
                xlsSheet.Rows.Cells(i, 7) = rec("SIMBOLOSUPIANTA")
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD
        
        nomeFile = App.Path & "\" & idProdottoSelezionato & " - "
        If tipoSelezione = TSSP_TUTTA_LA_PIANTA Then
            nomeFile = nomeFile & "PIANTA"
        ElseIf tipoSelezione = TSSP_SUPERAREE Then
            nomeFile = nomeFile & "SUPERAREE"
        ElseIf tipoSelezione = TSSP_AREE Then
            nomeFile = nomeFile & "AREE"
        End If
        nomeFile = nomeFile & " - "
        If tuttiIPosti Then
            nomeFile = nomeFile & "TUTTI"
        ElseIf soloPostiLiberi Then
            nomeFile = nomeFile & "LIBERI"
        ElseIf soloPostiVenduti Then
            nomeFile = nomeFile & "VENDUTI"
        ElseIf soloPostiProtetti Then
            nomeFile = nomeFile & "PROTETTI"
        End If
        nomeFile = nomeFile & " - "
        
        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing
    
        MsgBox "Report salvato sul file: " & nomeFile
        
        MousePointer = mousePointerOld
        
    End If

End Sub

