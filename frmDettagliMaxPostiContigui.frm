VERSION 5.00
Begin VB.Form frmDettagliMaxPostiContigui 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Numero massimo posti contigui"
   ClientHeight    =   1335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtNumeromaxPostiContigui 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   240
      MaxLength       =   10
      TabIndex        =   0
      Top             =   300
      Width           =   4155
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   900
      TabIndex        =   2
      Top             =   780
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2580
      TabIndex        =   1
      Top             =   780
      Width           =   1035
   End
End
Attribute VB_Name = "frmDettagliMaxPostiContigui"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private numeroMaxPostiContigui As Long

Private exitCode As ExitCodeEnum

Public Sub Init()

    numeroMaxPostiContigui = 0
    Call Controlli_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub Controlli_Init()
    txtNumeromaxPostiContigui.Text = ""
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = txtNumeromaxPostiContigui.Text <> ""
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    If ValoriCampiOK Then
        exitCode = EC_CONFERMA
        Unload Me
    End If
End Sub

Public Function GetNumeroMaxPostiContigui() As Long
    GetNumeroMaxPostiContigui = numeroMaxPostiContigui
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Private Sub txtNumeromaxPostiContigui_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitą As Collection
    
On Error Resume Next
    
    ValoriCampiOK = True
    Set listaNonConformitą = New Collection
    
    If IsCampoInteroCorretto(txtNumeromaxPostiContigui) Then
        numeroMaxPostiContigui = CLng(txtNumeromaxPostiContigui.Text)
    Else
        ValoriCampiOK = False
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Numero massimo posti contigui")
        Call listaNonConformitą.Add("- il valore immesso sul campo Numero massimo posti contigui deve essere numerico di tipo intero;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If
    
End Function
