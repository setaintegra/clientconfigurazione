VERSION 5.00
Begin VB.Form frmDettagliGriglieClonabili 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Griglie posti clonabili"
   ClientHeight    =   2385
   ClientLeft      =   10545
   ClientTop       =   5385
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2385
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkClonaFasceSequenze 
      Caption         =   "clona Fasce e Sequenze"
      Height          =   255
      Left            =   180
      TabIndex        =   2
      Top             =   1560
      Width           =   3735
   End
   Begin VB.ComboBox cmbPianta 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   420
      Width           =   4275
   End
   Begin VB.ComboBox cmbAree 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1080
      Width           =   4275
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2580
      TabIndex        =   4
      Top             =   1920
      Width           =   1035
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   900
      TabIndex        =   3
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblAree 
      Caption         =   "Area"
      Height          =   195
      Left            =   180
      TabIndex        =   6
      Top             =   840
      Width           =   1515
   End
   Begin VB.Label lblPianta 
      Caption         =   "Pianta"
      Height          =   195
      Left            =   180
      TabIndex        =   5
      Top             =   180
      Width           =   1515
   End
End
Attribute VB_Name = "frmDettagliGriglieClonabili"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idAreaSelezionata As Long
Private clonaFasceSequenze As Boolean

Private exitCode As ExitCodeEnum
Private internalEvent As Boolean

Public Sub Init(idPianta As Long)
    Dim listaId As Collection
    Dim listaLabel As Collection
    Dim sql As String
    Dim internalEventOld As Boolean

    internalEventOld = internalEvent
    internalEvent = True
    
    clonaFasceSequenze = False
    sql = "SELECT P.IDPIANTA ID, P.NOME" & _
        " FROM PIANTA P, GRIGLIAPOSTI G" & _
        " WHERE G.IDPIANTA(+) = P.IDPIANTA" & _
        " AND G.IDPIANTA IS NULL" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbPianta, sql, "NOME")
    idPiantaSelezionata = idPianta
    Call SelezionaElementoSuCombo(cmbPianta, idPiantaSelezionata)
    Call CaricaAreePerPianta
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmbAree.Enabled = (cmbPianta.ListIndex <> idNessunElementoSelezionato)
    lblAree.Enabled = (cmbPianta.ListIndex <> idNessunElementoSelezionato)
    cmdConferma.Enabled = (cmbAree.ListIndex <> idNessunElementoSelezionato)
End Sub

Private Sub chkClonaFasceSequenze_Click()
    Call chkClonaFasceSequenze_Update
End Sub

Private Sub chkClonaFasceSequenze_Update()
    If chkClonaFasceSequenze.Value = vbChecked Then
        clonaFasceSequenze = True
    Else
        clonaFasceSequenze = False
    End If
End Sub

Private Sub cmbAree_Click()
    Call cmbAree_Update
End Sub

Private Sub cmbAree_Update()
    idAreaSelezionata = cmbAree.ItemData(cmbAree.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbPianta_Click()
    idPiantaSelezionata = cmbPianta.ItemData(cmbPianta.ListIndex)
    If Not internalEvent Then
        Call CaricaAreePerPianta
    End If
End Sub

Private Sub CaricaAreePerPianta()
    Dim sql As String
    
    Call cmbAree.Clear
    sql = "SELECT A.IDAREA ID, A.CODICE || ' - ' || A.NOME AREALABEL" & _
        " FROM GRIGLIAPOSTI G, AREA A" & _
        " WHERE G.IDAREA = A.IDAREA" & _
        " AND A.IDPIANTA = " & idPiantaSelezionata & _
        " ORDER BY AREALABEL"
    Call CaricaValoriCombo(cmbAree, sql, "AREALABEL")
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Function GetIdAreaSelezionata() As Long
    GetIdAreaSelezionata = idAreaSelezionata
End Function

Public Function GetClonaFasceSequenze() As Boolean
    GetClonaFasceSequenze = clonaFasceSequenze
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub






