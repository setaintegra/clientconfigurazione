VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneTemplate 
   Caption         =   "Template"
   ClientHeight    =   9030
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   ScaleHeight     =   9030
   ScaleWidth      =   11955
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstOrganizzazioniDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   6120
      MultiSelect     =   2  'Extended
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   5880
      Width           =   2355
   End
   Begin VB.CommandButton cmdSpostaTuttiASinistra 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   7320
      Width           =   435
   End
   Begin VB.CommandButton cmdSpostaSelezioneASinistra 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   6840
      Width           =   435
   End
   Begin VB.CommandButton cmdSpostaSelezioneADestra 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   6360
      Width           =   435
   End
   Begin VB.ListBox lstOrganizzazioniSelezionate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   9000
      MultiSelect     =   2  'Extended
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   5880
      Width           =   2415
   End
   Begin VB.CommandButton cmdSpostaTuttiADestra 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   5880
      Width           =   435
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   20
      TabIndex        =   10
      Top             =   5940
      Width           =   3315
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   9
      Top             =   8460
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   7980
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   1
      Top             =   4620
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   6600
      Width           =   4635
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneTemplate 
      Height          =   330
      Left            =   5880
      Top             =   4500
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneTemplate 
      Height          =   3675
      Left            =   120
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6482
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblOrganizzazioniSelezionate 
      Alignment       =   2  'Center
      Caption         =   "Organizzazioni selezionate"
      Height          =   195
      Left            =   9000
      TabIndex        =   24
      Top             =   5640
      Width           =   2355
   End
   Begin VB.Label lblOrganizzazioniDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Organizzazioni disponibili"
      Height          =   195
      Left            =   6120
      TabIndex        =   23
      Top             =   5640
      Width           =   2355
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   5700
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Template"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   5775
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   14
      Top             =   4380
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   4380
      Width           =   1815
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   6360
      Width           =   1575
   End
End
Attribute VB_Name = "frmConfigurazioneTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private nomeRecordSelezionato As String
Private descrizioneRecordSelezionato As String

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private listaDisponibili As Collection
Private listaSelezionati As Collection

Private Sub AggiornaAbilitazioneControlli()
    
    dgrConfigurazioneTemplate.Caption = "TEMPLATE CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTemplate.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblOrganizzazioniDisponibili.Enabled = False
        lblOrganizzazioniSelezionate.Enabled = False
        lstOrganizzazioniDisponibili.Enabled = False
        lstOrganizzazioniSelezionate.Enabled = False
        Call lstOrganizzazioniDisponibili.Clear
        Call lstOrganizzazioniSelezionate.Clear
        cmdSpostaTuttiADestra.Enabled = False
        cmdSpostaSelezioneADestra.Enabled = False
        cmdSpostaTuttiASinistra.Enabled = False
        cmdSpostaSelezioneASinistra.Enabled = False
        
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTemplate.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOrganizzazioniDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOrganizzazioniSelezionate.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstOrganizzazioniDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstOrganizzazioniSelezionate.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSpostaTuttiADestra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSpostaSelezioneADestra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSpostaTuttiASinistra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSpostaSelezioneASinistra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTemplate.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblOrganizzazioniDisponibili.Enabled = False
        lblOrganizzazioniSelezionate.Enabled = False
        lstOrganizzazioniDisponibili.Enabled = False
        lstOrganizzazioniSelezionate.Enabled = False
        cmdSpostaTuttiADestra.Enabled = False
        cmdSpostaSelezioneADestra.Enabled = False
        cmdSpostaTuttiASinistra.Enabled = False
        cmdSpostaSelezioneASinistra.Enabled = False
        Call lstOrganizzazioniDisponibili.Clear
        Call lstOrganizzazioniSelezionate.Clear
        
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    End If
    
End Sub

Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
    gestioneFormCorrente = gs
End Sub

Public Sub SetNomeTemplateSelezionato(nome As String)
    nomeRecordSelezionato = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CaricaValoriLstDisponibili
    Set listaSelezionati = New Collection
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
        
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDTEMPLATE = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    
    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_TEMPLATE, CCDA_TEMPLATE, stringaNota)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneTemplate_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneTemplate_Init
    Else
        If ValoriCampiOK Then
            If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_TEMPLATE, CCDA_TEMPLATE, stringaNota)
            ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_TEMPLATE, CCDA_TEMPLATE, stringaNota)
            End If
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazioneTemplate_Init
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
            Call dgrConfigurazioneTemplate_Init
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AssegnaValoriCampi
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AssegnaValoriCampi
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDZONA ID, NOME || ' - ' || DESCRIZIONE LABEL" & _
        " FROM ZONA ORDER BY LABEL"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AssegnaValoriCampi
End Sub

Private Sub dgrConfigurazioneTemplate_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneTemplate_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneTemplate_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneTemplate.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec(0).Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneTemplate_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazioneTemplate
    
    sql = "SELECT IDTEMPLATE AS ""ID"", NOME AS ""nome"", DESCRIZIONE AS ""Descrizione""" & _
        " FROM TEMPLATE" & _
        " ORDER BY NOME"

    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneTemplate.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoTemplate As Long
    Dim organizzazione As clsElementoLista
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovoTemplate = OttieniIdentificatoreDaSequenza("SQ_TEMPLATE")
    sql = "INSERT INTO TEMPLATE (IDTEMPLATE, NOME, DESCRIZIONE)" & _
        " VALUES (" & _
        idNuovoTemplate & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ")"
    SETAConnection.Execute sql, n, adCmdText

'   INSERIMENTO IN TABELLA ORGANIZZAZIONE_TEMPLATE
    sql = "DELETE FROM ORGANIZZAZIONE_TEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    If Not (listaSelezionati Is Nothing) Then
        For Each organizzazione In listaSelezionati
            sql = "INSERT INTO ORGANIZZAZIONE_TEMPLATE (IDORGANIZZAZIONE, IDTEMPLATE)"
            sql = sql & " VALUES (" & organizzazione.idElementoLista & ", "
            sql = sql & idNuovoTemplate & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next organizzazione
    End If
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoTemplate)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT NOME, DESCRIZIONE FROM TEMPLATE" & _
        " WHERE IDTEMPLATE = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtNome.Text = nomeRecordSelezionato
    
    txtDescrizione.Text = ""
    txtDescrizione.Text = descrizioneRecordSelezionato
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    Dim numeroElementi As Long
    Dim organizzazione As clsElementoLista

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    sql = "UPDATE TEMPLATE SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & ", " & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & _
        " WHERE IDTEMPLATE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
'   AGGIORNAMENTO IN TABELLA ORGANIZZAZIONE_TEMPLATE
    sql = "DELETE FROM ORGANIZZAZIONE_TEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    If Not (listaSelezionati Is Nothing) Then
        For Each organizzazione In listaSelezionati
            sql = "INSERT INTO ORGANIZZAZIONE_TEMPLATE (IDORGANIZZAZIONE, IDTEMPLATE)"
            sql = sql & " VALUES (" & organizzazione.idElementoLista & ", "
            sql = sql & idRecordSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next organizzazione
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    
    sql = "DELETE FROM UTILIZZOTEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM ORGANIZZAZIONE_TEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM TEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazioneTemplate_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneTemplate
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 2
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneTemplate.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTEMPLATE <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("TEMPLATE", "NOME = " & SqlStringValue(nomeRecordSelezionato), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub



Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    sql = "SELECT O.IDORGANIZZAZIONE," & _
        " O.NOME AS ""Nome""," & _
        " O.DESCRIZIONE AS ""Descrizione""" & _
        " FROM ORGANIZZAZIONE O" & _
        " WHERE IDORGANIZZAZIONE NOT IN" & _
        " (SELECT IDORGANIZZAZIONE FROM ORGANIZZAZIONE_TEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato & ")" & _
        " ORDER BY ""Nome"""
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            organizzazioneCorrente.nomeElementoLista = rec("NOME")
            organizzazioneCorrente.idElementoLista = rec("IDORGANIZZAZIONE").Value
            organizzazioneCorrente.descrizioneElementoLista = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            Call listaDisponibili.Add(organizzazioneCorrente, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOrganizzazioniDisponibili_Init

End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSelezionati = New Collection
    
    sql = "SELECT O.IDORGANIZZAZIONE," & _
        " O.NOME AS ""Nome""," & _
        " O.DESCRIZIONE AS ""Descrizione""" & _
        " FROM ORGANIZZAZIONE O" & _
        " WHERE IDORGANIZZAZIONE IN" & _
        " (SELECT IDORGANIZZAZIONE FROM ORGANIZZAZIONE_TEMPLATE WHERE IDTEMPLATE = " & idRecordSelezionato & ")" & _
        " ORDER BY ""Nome"""
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            organizzazioneCorrente.nomeElementoLista = rec("NOME")
            organizzazioneCorrente.idElementoLista = rec("IDORGANIZZAZIONE").Value
            organizzazioneCorrente.descrizioneElementoLista = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            Call listaSelezionati.Add(organizzazioneCorrente, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOrganizzazioniSelezionate_Init

End Sub

Private Sub lstOrganizzazioniDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOrganizzazioniDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each organizzazione In listaDisponibili
            lstOrganizzazioniDisponibili.AddItem organizzazione.nomeElementoLista
            lstOrganizzazioniDisponibili.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOrganizzazioniSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOrganizzazioniSelezionate.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each organizzazione In listaSelezionati
            lstOrganizzazioniSelezionate.AddItem organizzazione.nomeElementoLista
            lstOrganizzazioniSelezionate.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSpostaTuttiADestra_Click()
    Call SpostaTuttiADestra
End Sub

Private Sub SpostaTuttiADestra()
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaDisponibili
        chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
        Call listaSelezionati.Add(organizzazione, chiaveOrganizzazione)
    Next organizzazione
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub cmdSpostaSelezioneADestra_Click()
    Call SpostaSelezioneADestra
End Sub

Private Sub SpostaSelezioneADestra()
    Dim i As Integer
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For i = 1 To lstOrganizzazioniDisponibili.ListCount
        If lstOrganizzazioniDisponibili.Selected(i - 1) Then
            idOrganizzazione = lstOrganizzazioniDisponibili.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaDisponibili.Item(chiaveOrganizzazione)
            Call listaSelezionati.Add(organizzazione, chiaveOrganizzazione)
            Call listaDisponibili.Remove(chiaveOrganizzazione)
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init

End Sub

Private Sub cmdSpostaSelezioneASinistra_Click()
    Call SpostaSelezioneASinistra
End Sub

Private Sub SpostaSelezioneASinistra()
    Dim i As Integer
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For i = 1 To lstOrganizzazioniSelezionate.ListCount
        If lstOrganizzazioniSelezionate.Selected(i - 1) Then
            idOrganizzazione = lstOrganizzazioniSelezionate.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaSelezionati.Item(chiaveOrganizzazione)
            Call listaDisponibili.Add(organizzazione, chiaveOrganizzazione)
            Call listaSelezionati.Remove(chiaveOrganizzazione)
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init

End Sub

Private Sub cmdSpostaTuttiASinistra_Click()
    Call SpostaTuttiASinistra
End Sub

Private Sub SpostaTuttiASinistra()
    Dim template As clsElementoLista
    Dim chiaveTemplate As String
    
    For Each template In listaSelezionati
        chiaveTemplate = ChiaveId(template.idElementoLista)
        Call listaDisponibili.Add(template, chiaveTemplate)
    Next template
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

