VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmConfigurazionePacchettoScelteProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtNumeroProdottiSelezionabili 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7560
      MaxLength       =   3
      TabIndex        =   13
      Top             =   5580
      Width           =   555
   End
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca"
      Height          =   435
      Left            =   7560
      TabIndex        =   12
      Top             =   4800
      Width           =   795
   End
   Begin VB.CommandButton cmdDeselezionaTutteStagioni 
      Caption         =   "Deseleziona tutti"
      Height          =   255
      Left            =   1920
      TabIndex        =   7
      Top             =   5640
      Width           =   1755
   End
   Begin VB.CommandButton cmdSelezionaTutteStagioni 
      Caption         =   "Seleziona tutti"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   5640
      Width           =   1755
   End
   Begin VB.ListBox lstStagione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   5
      Top             =   4380
      Width           =   3555
   End
   Begin VB.CommandButton cmdDeselezionaTuttiOrganizzazioniSpettacoli 
      Caption         =   "Deseleziona tutti"
      Height          =   255
      Left            =   5640
      TabIndex        =   10
      Top             =   5640
      Width           =   1755
   End
   Begin VB.CommandButton cmdSelezionaTuttiOrganizzazioniSpettacoli 
      Caption         =   "Seleziona tutti"
      Height          =   255
      Left            =   3840
      TabIndex        =   9
      Top             =   5640
      Width           =   1755
   End
   Begin VB.ListBox lstOrganizzazioneSpettacolo 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   3840
      Style           =   1  'Checkbox
      TabIndex        =   8
      Top             =   4380
      Width           =   3555
   End
   Begin VB.Frame fraModalitąSelezioneProdotti 
      Caption         =   "Seleziona prodotti per:"
      Height          =   915
      Left            =   4440
      TabIndex        =   37
      Top             =   3120
      Width           =   3915
      Begin VB.OptionButton optOrganizzazioni 
         Caption         =   "organizzazioni"
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   600
         Width           =   1575
      End
      Begin VB.OptionButton optSpettacoli 
         Caption         =   "spettacoli (solo prodotti monorappresentazione)"
         Height          =   195
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   3615
      End
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   6240
      Width           =   5595
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   7200
      Width           =   435
   End
   Begin VB.CommandButton cmdDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   6900
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   6600
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   6240
      MultiSelect     =   2  'Extended
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   6240
      Width           =   5595
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   28
      Top             =   3120
      Width           =   4035
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   27
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   24
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   22
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   23
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   26
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   25
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   20
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   21
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   6300
      Width           =   435
   End
   Begin VB.ComboBox cmbTurnoRappresentazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7560
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   4380
      Width           =   3555
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePacchettoScelteProdotto 
      Height          =   330
      Left            =   6360
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePacchettoScelteProdotto 
      Height          =   2175
      Left            =   120
      TabIndex        =   29
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   3836
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblNumeroProdottiSelezionabili 
      Caption         =   "Numero prodotti selezionabili"
      Height          =   255
      Left            =   7560
      TabIndex        =   40
      Top             =   5340
      Width           =   2775
   End
   Begin VB.Label lblStagione 
      Caption         =   "Stagione"
      Height          =   195
      Left            =   120
      TabIndex        =   39
      Top             =   4140
      Width           =   3555
   End
   Begin VB.Label lblOrganizzazioneSpettacolo 
      Caption         =   "Organizzazione / Spettacolo"
      Height          =   195
      Left            =   3840
      TabIndex        =   38
      Top             =   4140
      Width           =   3555
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Prodotti selezionati - numero rappresentazioni"
      Height          =   195
      Left            =   6240
      TabIndex        =   36
      Top             =   6000
      Width           =   5595
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Prodotti disponibili - numero rappresentazioni"
      Height          =   195
      Left            =   120
      TabIndex        =   35
      Top             =   6000
      Width           =   5595
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   34
      Top             =   2880
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   33
      Top             =   2880
      Width           =   2775
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   32
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Scelte Prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   31
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblTurnoRappresentazione 
      Caption         =   "Turno rappresentazione"
      Height          =   255
      Left            =   7560
      TabIndex        =   30
      Top             =   4140
      Width           =   2055
   End
End
Attribute VB_Name = "frmConfigurazionePacchettoScelteProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOffertaPacchettoSelezionata As Long
Private idOrganizzazioneSelezionata As Long
Private isRecordEditabile As Boolean
Private nomeOffertaPacchettoSelezionata As String
Private idSpettacoloSelezionato As Long
Private idTurnoRappresentazioneSelezionato As Long
Private listaProdottiDisponibili As Collection
Private listaProdottiSelezionati As Collection
Private listaIdOrganizzazioniSpettacoliSelezionati As Collection
Private listaIdStagioniSelezionate As Collection
Private nomeTabellaTemporanea As String
Private listaAppoggioProdotto As Collection
Private Const ID_NESSUN_TURNO As Long = -3
Private numeroProdottiSelezionabili As Integer
'Private numeroMaxProdottiSelezionabili As Integer

Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum
Private modalitąSelezioneProdottiPerSceltaProdotti As ModalitąSelezioneProdottiPerSceltaProdottiEnum
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitąFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    Dim abilitaTastoCerca As Boolean
    
    lblInfo1.Caption = "Pacchetto"
    txtInfo1.Text = nomeOffertaPacchettoSelezionata
    txtInfo1.Enabled = False
    
    If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
        lblOrganizzazioneSpettacolo.Caption = "Spettacolo"
    ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
        lblOrganizzazioneSpettacolo.Caption = "Organizzazione"
    Else
        lblOrganizzazioneSpettacolo.Caption = "(...)"
    End If
    
    dgrConfigurazionePacchettoScelteProdotto.Caption = "SCELTE PRODOTTO CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoScelteProdotto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbTurnoRappresentazione.Clear
        txtNumeroProdottiSelezionabili.Text = ""
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        cmbTurnoRappresentazione.Enabled = False
        lblTurnoRappresentazione.Enabled = False
        lstStagione.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        lblStagione.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdSelezionaTutteStagioni.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdDeselezionaTutteStagioni.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        lstOrganizzazioneSpettacolo.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        lblOrganizzazioneSpettacolo.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdSelezionaTuttiOrganizzazioniSpettacoli.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdDeselezionaTuttiOrganizzazioniSpettacoli.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        txtNumeroProdottiSelezionabili.Enabled = False
        lblNumeroProdottiSelezionabili.Enabled = False
'        updNumeroProdottiSelezionabili.Enabled = False
        optOrganizzazioni.Enabled = True
        optSpettacoli.Enabled = True
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDisponibile.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdCerca.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoScelteProdotto.Enabled = False
        cmbTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO)
        lblTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO)
        txtNumeroProdottiSelezionabili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmbTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstStagione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblStagione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionaTutteStagioni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDeselezionaTutteStagioni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstOrganizzazioneSpettacolo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOrganizzazioneSpettacolo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionaTuttiOrganizzazioniSpettacoli.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDeselezionaTuttiOrganizzazioniSpettacoli.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNumeroProdottiSelezionabili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        updNumeroProdottiSelezionabili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        optOrganizzazioni.Enabled = False
        optSpettacoli.Enabled = False
'        lblTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDisponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        If (listaIdStagioniSelezionate Is Nothing) Or _
            (listaIdOrganizzazioniSpettacoliSelezionati Is Nothing) Then
            abilitaTastoCerca = False
        Else
            abilitaTastoCerca = (listaIdStagioniSelezionate.count > 0 And _
                listaIdOrganizzazioniSpettacoliSelezionati.count > 0)
        End If
        If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
            abilitaTastoCerca = abilitaTastoCerca And _
                idTurnoRappresentazioneSelezionato <> idNessunElementoSelezionato
        End If
        cmdCerca.Enabled = abilitaTastoCerca
        cmdConferma.Enabled = lstSelezionati.ListCount > 0 And _
            Trim(txtNumeroProdottiSelezionabili.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoScelteProdotto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNumeroProdottiSelezionabili.Text = ""
        Call cmbTurnoRappresentazione.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        Call lstOrganizzazioneSpettacolo.Clear
        Call lstStagione.Clear
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        cmbTurnoRappresentazione.Enabled = False
        lblTurnoRappresentazione.Enabled = False
        lstStagione.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        lblStagione.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdSelezionaTutteStagioni.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdDeselezionaTutteStagioni.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        lstOrganizzazioneSpettacolo.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        lblOrganizzazioneSpettacolo.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdSelezionaTuttiOrganizzazioniSpettacoli.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdDeselezionaTuttiOrganizzazioniSpettacoli.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        txtNumeroProdottiSelezionabili.Enabled = False
        lblNumeroProdottiSelezionabili.Enabled = False
'        updNumeroProdottiSelezionabili.Enabled = False
        optOrganizzazioni.Enabled = True
        optSpettacoli.Enabled = True
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDisponibile.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = (modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            modalitąSelezioneProdottiPerSceltaProdotti <> MSPPSP_NON_SPECIFICATA)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdCerca.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    End If
    
    Select Case modalitąFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioProdotti
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchettoSelezionata)
        Call EliminaTabellaAppoggioProdotti
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Public Sub SetIdOffertaPacchettoSelezionata(id As Long)
    idOffertaPacchettoSelezionata = id
End Sub

Public Sub SetNomeOffertaPacchettoSelezionata(nome As String)
    nomeOffertaPacchettoSelezionata = nome
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazionePacchettoOrdineScelteProdotto
    Call EliminaTabellaAppoggioProdotti
End Sub

Private Sub CaricaFormConfigurazionePacchettoOrdineScelteProdotto()
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetModalitąForm(modalitąFormCorrente)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.Init
End Sub


Private Sub cmbTurnoRappresentazione_Click()
    If Not internalEvent Then
        idTurnoRappresentazioneSelezionato = cmbTurnoRappresentazione.ItemData(cmbTurnoRappresentazione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCerca_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If Not internalEvent Then
        Call Cerca
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub Cerca()
    Call CaricaProdottiDisponibili
    Call CaricaProdottiSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    If IsProdottoEditabile(idOffertaPacchettoSelezionata, causaNonEditabilita) Then
    If isOffertaPacchettoBloccataDaUtente Then
'        isConfigurabile = True
'        If tipoStatoProdotto = TSP_ATTIVO Then
'            Call frmMessaggio.Visualizza("ConfermaEditabilitąProdottoAttivo")
'            If frmMessaggio.exitCode <> EC_CONFERMA Then
'                isConfigurabile = False
'            End If
'        End If
'        If isConfigurabile Then
    
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If ValoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idOffertaPacchettoSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazionePacchettoScelteProdotto_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazionePacchettoScelteProdotto_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idOffertaPacchettoSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazionePacchettoScelteProdotto_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazionePacchettoScelteProdotto_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idOffertaPacchettoSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazionePacchettoScelteProdotto_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazionePacchettoScelteProdotto_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idOffertaPacchettoSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazionePacchettoScelteProdotto_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazionePacchettoScelteProdotto_Init
                    End Select
                End If
                Call AggiornaAbilitazioneControlli
            End If
'        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitąCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDTURNORAPPRESENTAZIONE ID, NOME"
    sql = sql & " FROM TURNORAPPRESENTAZIONE"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTurnoRappresentazione, sql, "NOME", True)
    Call AssegnaValoriCampi
'    Call CaricaListaStagioni
'    If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
'        Call CaricaListaOrganizzazioniSpettacoli
'    ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
'        Call CaricaListaOrganizzazioniSpettacoli
'    End If
'    Call CaricaProdottiDisponibili
    Call CaricaProdottiSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitąFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioProdotti
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    isRecordEditabile = True
    sql = "SELECT IDTURNORAPPRESENTAZIONE ID, NOME"
    sql = sql & " FROM TURNORAPPRESENTAZIONE"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbTurnoRappresentazione, sql, "NOME", True)
    Call CaricaListaStagioni
    If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
        Call CaricaListaOrganizzazioniSpettacoli
    ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
        Call CaricaListaOrganizzazioniSpettacoli
    End If
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePacchettoScelteProdotto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDTURNORAPPRESENTAZIONE ID, NOME"
    sql = sql & " FROM TURNORAPPRESENTAZIONE"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTurnoRappresentazione, sql, "NOME", True)
    Call AssegnaValoriCampi
    Call CaricaListaStagioni
    If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
        Call CaricaListaOrganizzazioniSpettacoli
    ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
        Call CaricaListaOrganizzazioniSpettacoli
    End If
'    Call CaricaProdottiDisponibili
'    Call CaricaProdottiSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazionePacchettoScelteProdotto_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Dim internalEventOld As Boolean
    
'    idSpettacoloSelezionato = idNessunElementoSelezionato
'    idTurnoRappresentazioneSelezionato = idNessunElementoSelezionato
'    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call Variabili_Init
    Call Controlli_Init
    Call CreaTabellaAppoggioProdotti
    Call adcConfigurazionePacchettoScelteProdotto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePacchettoScelteProdotto_Init
    Call AggiornaAbilitazioneControlli
    
'    internalEventOld = internalEvent
'    internalEvent = True
    
    Call Me.Show(vbModal)
    
'    internalEvent = internalEventOld

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePacchettoScelteProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePacchettoScelteProdotto_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Call PopolaTabellaAppoggioProdotti

    Set d = adcConfigurazionePacchettoScelteProdotto
        
    sql = "SELECT S.IDSCELTAPRODOTTO ID,"
    sql = sql & " S.ORDINE ""Ordine"", TMP.NOME ""Prodotti"","
    sql = sql & " S.NUMEROPRODOTTISELEZIONABILI ""Selezionabili"""
    sql = sql & " FROM SCELTAPRODOTTO S, " & nomeTabellaTemporanea & " TMP"
    sql = sql & " WHERE S.IDSCELTAPRODOTTO = TMP.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " ORDER BY ""Ordine"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazionePacchettoScelteProdotto.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT S.IDSESSIONECONFIGURAZIONE, S.IDTIPOSTATORECORD"
'        sql = sql & " SUM(S.NUMEROPRODOTTISELEZIONABILI) SOMMA"
'        sql = sql & " FROM SCELTAPRODOTTO S"
'        sql = sql & " WHERE S.IDSCELTAPRODOTTO = " & idRecordSelezionato
'        sql = sql & " GROUP BY S.IDSESSIONECONFIGURAZIONE, S.IDTIPOSTATORECORD"
'    Else
        sql = " SELECT SUM(S.NUMEROPRODOTTISELEZIONABILI) SOMMA"
        sql = sql & " FROM SCELTAPRODOTTO S"
        sql = sql & " WHERE S.IDSCELTAPRODOTTO = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        numeroProdottiSelezionabili = IIf(IsNull(rec("SOMMA")), 0, rec("SOMMA").Value)
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close
'
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT SP.IDSESSIONECONFIGURAZIONE, SP.IDTIPOSTATORECORD"
'        sql = sql & " COUNT(SP.IDPRODOTTO) MAX"
'        sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP"
'        sql = sql & " WHERE SP.IDSCELTAPRODOTTO = " & idRecordSelezionato
'        sql = sql & " GROUP BY S.IDSESSIONECONFIGURAZIONE, S.IDTIPOSTATORECORD"
'    Else
'        sql = " SELECT COUNT(SP.IDPRODOTTO) MAX"
'        sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP"
'        sql = sql & " WHERE SP.IDSCELTAPRODOTTO = " & idRecordSelezionato
'    End If
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        numeroMaxProdottiSelezionabili = IIf(IsNull(rec("MAX")), 0, rec("MAX").Value)
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
'    End If
'    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

'    txtNome.Text = ""
'    txtDescrizione.Text = ""
'    txtNome.Text = nomeRecordSelezionato
'    txtDescrizione.Text = descrizioneRecordSelezionato
'    Call SelezionaElementoSuCombo(cmbSpettacolo, idSpettacoloSelezionato)
    txtNumeroProdottiSelezionabili.Text = CStr(numeroProdottiSelezionabili)
    Call SelezionaElementoSuCombo(cmbTurnoRappresentazione, idTurnoRappresentazioneSelezionato)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim prodotto As clsElementoLista
    Dim condizioniSQL As String
    Dim listaRapprNonAssociabili As Collection
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    Set listaRapprNonAssociabili = New Collection
    SETAConnection.BeginTrans
'   AGGIORNAMENTO TABELLA SCELTAPRODOTTO
    sql = "UPDATE SCELTAPRODOTTO SET"
    sql = sql & " NUMEROPRODOTTISELEZIONABILI = " & numeroProdottiSelezionabili
    sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO", "IDSCELTAPRODOTTO", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    
'   AGGIORNAMENTO TABELLA SCELTAPRODOTTO_PRODOTTO
    If Not (listaProdottiSelezionati Is Nothing) Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO_PRODOTTO", "IDSCELTAPRODOTTO", idRecordSelezionato, TSR_ELIMINATO)
'        Else
            sql = "DELETE FROM SCELTAPRODOTTO_PRODOTTO"
            sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
        For Each prodotto In listaProdottiSelezionati
'            If ProdottiCorrelandiBloccati(rappresentazione.idElementoLista, idOffertaPacchettoSelezionata) Then
'                If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO SCELTAPRODOTTO_PRODOTTO"
'                    sql = sql & " (IDSCELTAPRODOTTO, IDPRODOTTO)"
'                    sql = sql & " (SELECT "
'                    sql = sql & idRecordSelezionato & " IDSCELTAPRODOTTO, "
'                    sql = sql & prodotto.idElementoLista & " IDPRODOTTO"
'                    sql = sql & " FROM DUAL"
'                    sql = sql & " MINUS"
'                    sql = sql & " SELECT IDSCELTAPRODOTTO, IDPRODOTTO"
'                    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO"
'                    sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
'                    sql = sql & " AND IDPRODOTTO = " & prodotto.idElementoLista & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                    condizioniSQL = " AND IDPRODOTTO = " & prodotto.idElementoLista
'                    Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO_PRODOTTO", "IDSCELTAPRODOTTO", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'                Else
                    sql = "INSERT INTO SCELTAPRODOTTO_PRODOTTO"
                    sql = sql & " (IDSCELTAPRODOTTO, IDPRODOTTO)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & idRecordSelezionato & ", "
                    sql = sql & prodotto.idElementoLista & ")"
                    SETAConnection.Execute sql, n, adCmdText
'                End If
'            Else
'                Call listaRapprNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
'                Call SETAConnection.RollbackTrans
'            End If
'            Call SbloccaProdottiCorrelandi(getNomeMacchina, idOffertaPacchettoSelezionata)
        Next prodotto
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazionePacchettoScelteProdotto_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePacchettoScelteProdotto
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitą As Collection
'    Dim condizioneSql As String

    ValoriCampiOK = True
'    condizioneSql = ""
    Set listaNonConformitą = New Collection
'    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
'        condizioneSql = "IDSCELTAPRODOTTO <> " & idRecordSelezionato
'    End If
    
'    nomeRecordSelezionato = Trim(txtNome.Text)
'    If ViolataUnicitą("SCELTARAPPRESENTAZIONE", "NOME = " & SqlStringValue(nomeRecordSelezionato), " IDPRODOTTO = " & idOffertaPacchettoSelezionata, _
        condizioneSql, "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformitą.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
'            " č gią presente in DB per lo stesso Prodotto;")
'    End If
'    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)

    If IsCampoInteroCorretto(txtNumeroProdottiSelezionabili) Then
        numeroProdottiSelezionabili = CLng(Trim(txtNumeroProdottiSelezionabili.Text))
        If numeroProdottiSelezionabili > 0 Then
            If numeroProdottiSelezionabili > listaProdottiSelezionati.count Then
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Numero prodotti selezionabili deve essere minore o uguale al numero di prodotti selezionati (" & listaProdottiSelezionati.count & ");")
            End If
        Else
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore immesso sul campo Numero prodotti selezionabili deve essere numerico di tipo intero e maggiore di zero;")
        End If
    Else
        Call listaNonConformitą.Add("- il valore immesso sul campo Numero prodotti selezionabili deve essere numerico di tipo intero;")
        ValoriCampiOK = False
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Public Sub SetModalitąForm(mf As AzioneEnum)
    modalitąFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmInizialePacchetto.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmInizialePacchetto.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, caricaValoriEstesi As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    
    If caricaValoriEstesi Then
        cmb.AddItem "<tutti>"
        cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
        cmb.AddItem "<nessuno>"
'        cmb.ItemData(i) = idNessunElementoSelezionato
        cmb.ItemData(i) = ID_NESSUN_TURNO
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM SCELTAPRODOTTO_PRODOTTO"
'            sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM SCELTAPRODOTTO"
'            sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO_PRODOTTO", "IDSCELTAPRODOTTO", idRecordSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO", "IDSCELTAPRODOTTO", idRecordSelezionato, TSR_ELIMINATO)
'        End If
'    Else
        sql = "DELETE FROM SCELTAPRODOTTO_PRODOTTO"
        sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM SCELTAPRODOTTO"
        sql = sql & " WHERE IDSCELTAPRODOTTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdDisponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstSelezionati
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idProdotto As Long
    Dim prodotto As clsElementoLista
    Dim chiaveProdotto As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idProdotto = lstSelezionati.ItemData(i - 1)
            chiaveProdotto = ChiaveId(idProdotto)
            Set prodotto = listaProdottiSelezionati.Item(chiaveProdotto)
            Call listaProdottiDisponibili.Add(prodotto, chiaveProdotto)
            Call listaProdottiSelezionati.Remove(chiaveProdotto)
        End If
    Next i
'    Call AggiornaNumeroMaxProdottiSelezionabili
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim prodotto As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaProdottiDisponibili Is Nothing) Then
        i = 1
        For Each prodotto In listaProdottiDisponibili
            lstDisponibili.AddItem prodotto.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = prodotto.idElementoLista
            i = i + 1
        Next prodotto
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim prodotto As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaProdottiSelezionati Is Nothing) Then
        i = 1
        For Each prodotto In listaProdottiSelezionati
            lstSelezionati.AddItem prodotto.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = prodotto.idElementoLista
            i = i + 1
        Next prodotto
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaProdottiSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeTipoTerminale As String
    Dim codiceTastoTipoTerminale As String
    Dim idTipoTerminale As Long
    Dim idTasto As Long
    Dim chiaveProdotto As String
    Dim prodotto As clsElementoLista
    
    Call lstSelezionati.Clear
    
    Call ApriConnessioneBD

    Set listaProdottiSelezionati = New Collection
    
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
''        sql = "SELECT DISTINCT R.IDRAPPRESENTAZIONE ID, R.DATAORAINIZIO, V.NOME NOMEVENUE"
''        sql = sql & " FROM RAPPRESENTAZIONE R, VENUE V,"
''        sql = sql & " SCELTARAPPRESENTAZIONE_RAPPR SR"
''        sql = sql & " WHERE SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
''        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
''        sql = sql & " AND R.IDVENUE = V.IDVENUE"
''        sql = sql & " AND (SR.IDTIPOSTATORECORD IS NULL OR SR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
''        sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
'        sql = " SELECT P.IDPRODOTTO, P.NOME, COUNT(PR.IDRAPPRESENTAZIONE) CONT"
'        sql = sql & " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR,"
'        sql = sql & " SCELTAPRODOTTO_PRODOTTO SP"
'        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
'        sql = sql & " AND SP.IDSCELTAPRODOTTO = " & idRecordSelezionato
'        sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
'        sql = sql & " AND (SP.IDTIPOSTATORECORD IS NULL OR SP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " GROUP BY P.IDPRODOTTO, P.NOME"
'        sql = sql & " ORDER BY NOME"
'    Else
'        sql = "SELECT DISTINCT R.IDRAPPRESENTAZIONE ID, R.DATAORAINIZIO, V.NOME NOMEVENUE"
'        sql = sql & " FROM RAPPRESENTAZIONE R, VENUE V,"
'        sql = sql & " SCELTARAPPRESENTAZIONE_RAPPR SR"
'        sql = sql & " WHERE SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
'        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
'        sql = sql & " AND R.IDVENUE = V.IDVENUE"
'        sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
        sql = " SELECT P.IDPRODOTTO ID, P.NOME, COUNT(PR.IDRAPPRESENTAZIONE) CONT"
        sql = sql & " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR,"
        sql = sql & " SCELTAPRODOTTO_PRODOTTO SP"
        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
        sql = sql & " AND SP.IDSCELTAPRODOTTO = " & idRecordSelezionato
        sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
        sql = sql & " GROUP BY P.IDPRODOTTO, P.NOME"
        sql = sql & " ORDER BY NOME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodotto = New clsElementoLista
            prodotto.idElementoLista = rec("ID").Value
            prodotto.descrizioneElementoLista = rec("NOME") & " - " & rec("CONT")
            chiaveProdotto = ChiaveId(prodotto.idElementoLista)
            Call listaProdottiSelezionati.Add(prodotto, chiaveProdotto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
'    Call AggiornaNumeroMaxProdottiSelezionabili
    Call lstSelezionati_Init
    
End Sub

Private Sub CaricaProdottiDisponibili_old()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeRappresentazione As String
    Dim idRappresentazione As Long
    Dim chiaveRappresentazione As String
    Dim rappresentazioneCorrente As clsElementoLista
    Dim condizioniSQL As String
    
    Call lstDisponibili.Clear
    
    Call ApriConnessioneBD
    
    Set listaProdottiDisponibili = New Collection
    
    condizioniSQL = ""
    Select Case idTurnoRappresentazioneSelezionato
    Case idTuttiGliElementiSelezionati
        condizioniSQL = " AND R1.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE"
    Case ID_NESSUN_TURNO
        condizioniSQL = " AND R1.IDRAPPRESENTAZIONE NOT IN" & _
            " (SELECT IDRAPPRESENTAZIONE FROM RAPPRESENTAZIONE_TURNORAPPR)"
    Case Else
        condizioniSQL = " AND R1.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE" & _
            " AND RT.IDTURNORAPPRESENTAZIONE = " & idTurnoRappresentazioneSelezionato
    End Select
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT DISTINCT R1.IDRAPPRESENTAZIONE ID, R1.DATAORAINIZIO, V.NOME NOMEVENUE"
'        sql = sql & " FROM RAPPRESENTAZIONE R1, RAPPRESENTAZIONE_TURNORAPPR RT, VENUE_PIANTA VP, VENUE V,"
'        sql = sql & " ("
'        sql = sql & " SELECT DISTINCT R.IDRAPPRESENTAZIONE "
'        sql = sql & " FROM RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR"
'        sql = sql & " WHERE S.IDSCELTARAPPRESENTAZIONE = SR.IDSCELTARAPPRESENTAZIONE"
'        sql = sql & " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
'        sql = sql & " AND S.IDPRODOTTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (S.IDTIPOSTATORECORD IS NULL OR S.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " AND (SR.IDTIPOSTATORECORD IS NULL OR SR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " ) R2"
'        sql = sql & " WHERE R1.IDVENUE = VP.IDVENUE"
'        sql = sql & " AND VP.IDVENUE = V.IDVENUE"
''        sql = sql & " AND VP.IDPIANTA = " & idPiantaSelezionata
'        sql = sql & condizioniSQL
'        sql = sql & " AND R1.IDSPETTACOLO = " & idSpettacoloSelezionato
'        sql = sql & " AND R1.IDRAPPRESENTAZIONE = R2.IDRAPPRESENTAZIONE(+)"
'        sql = sql & " AND R2.IDRAPPRESENTAZIONE IS NULL"
'        sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
'    Else
        sql = "SELECT DISTINCT R1.IDRAPPRESENTAZIONE ID, R1.DATAORAINIZIO, V.NOME NOMEVENUE"
        sql = sql & " FROM RAPPRESENTAZIONE R1, RAPPRESENTAZIONE_TURNORAPPR RT, VENUE_PIANTA VP, VENUE V,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT R.IDRAPPRESENTAZIONE "
        sql = sql & " FROM RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR"
        sql = sql & " WHERE S.IDSCELTARAPPRESENTAZIONE = SR.IDSCELTARAPPRESENTAZIONE"
        sql = sql & " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
        sql = sql & " AND S.IDPRODOTTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ) R2"
        sql = sql & " WHERE R1.IDVENUE = VP.IDVENUE"
        sql = sql & " AND VP.IDVENUE = V.IDVENUE"
'        sql = sql & " AND VP.IDPIANTA = " & idPiantaSelezionata
        sql = sql & condizioniSQL
        sql = sql & " AND R1.IDSPETTACOLO = " & idSpettacoloSelezionato
        sql = sql & " AND R1.IDRAPPRESENTAZIONE = R2.IDRAPPRESENTAZIONE(+)"
        sql = sql & " AND R2.IDRAPPRESENTAZIONE IS NULL"
        sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set rappresentazioneCorrente = New clsElementoLista
            rappresentazioneCorrente.idElementoLista = rec("ID").Value
            rappresentazioneCorrente.descrizioneElementoLista = rec("DATAORAINIZIO") & " - " & rec("NOMEVENUE")
            chiaveRappresentazione = ChiaveId(rappresentazioneCorrente.idElementoLista)
            Call listaProdottiDisponibili.Add(rappresentazioneCorrente, chiaveRappresentazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaSelezionati()
    Dim prodotto As clsElementoLista
    Dim chiaveProdotto As String
    
    For Each prodotto In listaProdottiSelezionati
        chiaveProdotto = ChiaveId(prodotto.idElementoLista)
        Call listaProdottiDisponibili.Add(prodotto, chiaveProdotto)
    Next prodotto
    Set listaProdottiSelezionati = Nothing
    Set listaProdottiSelezionati = New Collection
    
'    Call AggiornaNumeroMaxProdottiSelezionabili
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaDisponibili
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaDisponibili()
    Dim prodotto As clsElementoLista
    Dim chiaveProdotto As String
    Dim listaProdottiNonAssociabili As Collection
    
    Set listaProdottiNonAssociabili = New Collection
    For Each prodotto In listaProdottiDisponibili
        chiaveProdotto = ChiaveId(prodotto.idElementoLista)
'        If RappresentazioneAssociabileAProdotto(prodotto.idElementoLista) Then
            Call listaProdottiSelezionati.Add(prodotto, chiaveProdotto)
            Call listaProdottiDisponibili.Remove(chiaveProdotto)
'        Else
'            Call listaProdottiNonAssociabili.Add(prodotto.descrizioneElementoLista)
'        End If
    Next prodotto
    Set listaProdottiDisponibili = Nothing
    Set listaProdottiDisponibili = New Collection
    If listaProdottiNonAssociabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguita", ArgomentoMessaggio(listaProdottiNonAssociabili))
    End If
'    Call AggiornaNumeroMaxProdottiSelezionabili
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idProdotto As Long
    Dim prodotto As clsElementoLista
    Dim chiaveProdotto As String
    Dim listaProdottiNonAssociabili As Collection
    
    Set listaProdottiNonAssociabili = New Collection
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idProdotto = lstDisponibili.ItemData(i - 1)
            chiaveProdotto = ChiaveId(idProdotto)
            Set prodotto = listaProdottiDisponibili.Item(chiaveProdotto)
'            If RappresentazioneAssociabileAProdotto(idProdotto) Then
                Call listaProdottiSelezionati.Add(prodotto, chiaveProdotto)
                Call listaProdottiDisponibili.Remove(chiaveProdotto)
'            Else
'                Call listaProdottiNonAssociabili.Add(prodotto.descrizioneElementoLista)
'            End If
        End If
    Next i
    If listaProdottiNonAssociabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguita", ArgomentoMessaggio(listaProdottiNonAssociabili))
    End If
'    Call AggiornaNumeroMaxProdottiSelezionabili
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub CreaTabellaAppoggioProdotti()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_PACCH_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDSCELTAPRODOTTO NUMBER(10), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggioProdotti
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub PopolaTabellaAppoggioProdotti()
    Dim sql As String
    Dim rec As New ADODB.Recordset
'    Dim id As Integer
    Dim idSceltaProdotto As Long
'    Dim idProdotto As Long
    Dim elencoNomi As String
'    Dim nomeSpettacolo As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioProdotto = New Collection
    
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT IDSCELTAPRODOTTO"
'        sql = sql & " FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (IDTIPOSTATORECORD IS NULL"
'        sql = sql & " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = " SELECT IDSCELTAPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDSCELTAPRODOTTO").Value
            Call listaAppoggioProdotto.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioProdotto
        campoNome = ""
'        sql = "SELECT DISTINCT R.IDSPETTACOLO IDSPETTACOLO, S.NOME NOMESPETTACOLO, DATAORAINIZIO "
'        sql = sql & " FROM SCELTARAPPRESENTAZIONE_RAPPR SR, RAPPRESENTAZIONE R, SPETTACOLO S "
'        sql = sql & " WHERE SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE "
'        sql = sql & " AND R.IDSPETTACOLO = S.IDSPETTACOLO"
'        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = " & recordTemporaneo.idElementoLista
'        sql = sql & " ORDER BY IDSPETTACOLO, DATAORAINIZIO"
        sql = " SELECT DISTINCT P.IDPRODOTTO IDPRODOTTO, P.NOME NOMEPRODOTTO"
        sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, PRODOTTO P"
        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO "
        sql = sql & " AND SP.IDSCELTAPRODOTTO = " & recordTemporaneo.idElementoLista
        sql = sql & " ORDER BY IDPRODOTTO, NOMEPRODOTTO"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
'            idProdotto = rec("IDPRODOTTO").Value
'            nomeSpettacolo = rec("NOMEPRODOTTO")
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("NOMEPRODOTTO"), campoNome & "; " & rec("NOMEPRODOTTO"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
'        recordTemporaneo.idAttributoElementoLista = idSpettacolo
'        recordTemporaneo.nomeAttributoElementoLista = nomeSpettacolo
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sł
    For Each recordTemporaneo In listaAppoggioProdotto
        idSceltaProdotto = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = " INSERT INTO " & nomeTabellaTemporanea & " (IDSCELTAPRODOTTO, NOME)"
        sql = sql & " VALUES (" & idSceltaProdotto & ", "
        sql = sql & SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub EliminaTabellaAppoggioProdotti()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub CaricaListaStagioni()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer

    i = 1
    Call lstStagione.Clear
    Set listaIdStagioniSelezionate = New Collection
    sql = " SELECT DISTINCT S.IDSTAGIONE ID, S.CODICE, S.NOME"
    sql = sql & " FROM STAGIONE S, SPETTACOLO P, "
    sql = sql & " ("
    sql = sql & " SELECT IDRAPPRESENTAZIONE, IDSPETTACOLO"
    sql = sql & " FROM RAPPRESENTAZIONE"
    sql = sql & " WHERE (DATAORAINIZIO + (DURATAINMINUTI/1440)) > SYSDATE "
    sql = sql & " ) R"
    sql = sql & " WHERE S.IDSTAGIONE = P.IDSTAGIONE"
    sql = sql & " AND P.IDSPETTACOLO = R.IDSPETTACOLO"
    sql = sql & " ORDER BY S.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Call lstStagione.AddItem(rec("NOME").Value)
            lstStagione.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub CaricaListaOrganizzazioniSpettacoli()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer

    i = 1
    Call lstOrganizzazioneSpettacolo.Clear
    Set listaIdOrganizzazioniSpettacoliSelezionati = New Collection
    If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
        sql = " SELECT DISTINCT S.IDSPETTACOLO ID, S.NOME"
        sql = sql & " FROM SPETTACOLO S, RAPPRESENTAZIONE R"
        sql = sql & " WHERE S.IDSPETTACOLO = R.IDSPETTACOLO"
        sql = sql & " AND (DATAORAINIZIO + (DURATAINMINUTI/1440)) > SYSDATE "
        sql = sql & " ORDER BY S.NOME"
    ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
        sql = " SELECT IDORGANIZZAZIONE ID, NOME"
        sql = sql & " FROM ORGANIZZAZIONE"
        sql = sql & " ORDER BY NOME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Call lstOrganizzazioneSpettacolo.AddItem(rec("NOME").Value)
            lstOrganizzazioneSpettacolo.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub Variabili_Init()
    idTurnoRappresentazioneSelezionato = idNessunElementoSelezionato
    numeroProdottiSelezionabili = 0
    modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_NON_SPECIFICATA
    Set listaIdOrganizzazioniSpettacoliSelezionati = Nothing
    Set listaProdottiDisponibili = Nothing
    Set listaProdottiSelezionati = Nothing
'    Set listaSpettacoliSelezionati = Nothing
    Set listaIdStagioniSelezionate = Nothing
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
End Sub

Private Sub optSpettacoli_Click()
    If Not internalEvent Then
        Call ModalitąSelezioneProdottiPerSceltaProdotti_Update
    End If
End Sub

Private Sub optOrganizzazioni_Click()
    If Not internalEvent Then
        Call ModalitąSelezioneProdottiPerSceltaProdotti_Update
    End If
End Sub

Private Sub ModalitąSelezioneProdottiPerSceltaProdotti_Update()
    If optSpettacoli.Value = True Then
        modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO
    ElseIf optOrganizzazioni.Value = True Then
        modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Controlli_Init()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    optOrganizzazioni.Value = False
    optSpettacoli.Value = False
    
    internalEvent = internalEventOld
End Sub

Private Sub cmdSelezionaTutteStagioni_Click()
    Call SelezionaTutteStagioni
End Sub

Private Sub cmdDeselezionaTutteStagioni_Click()
    Call DeselezionaTutteStagioni
End Sub

Private Sub SelezionaTutteStagioni()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idStagione As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set listaIdStagioniSelezionate = Nothing
    Set listaIdStagioniSelezionate = New Collection
    idStagione = idNessunElementoSelezionato
    For i = 1 To lstStagione.ListCount
        lstStagione.Selected(i - 1) = True
        idStagione = lstStagione.ItemData(i - 1)
        Call listaIdStagioniSelezionate.Add(idStagione, ChiaveId(idStagione))
    Next i
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub DeselezionaTutteStagioni()
    Dim i As Integer
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstStagione.ListCount
        lstStagione.Selected(i - 1) = False
    Next i
    Set listaIdStagioniSelezionate = Nothing
    Set listaIdStagioniSelezionate = New Collection
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstStagione_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaStagione
    End If
End Sub

Private Sub SelezionaDeselezionaStagione()
    Dim idStagione As Long
    
    idStagione = lstStagione.ItemData(lstStagione.ListIndex)
    If lstStagione.Selected(lstStagione.ListIndex) Then
        Call listaIdStagioniSelezionate.Add(idStagione, ChiaveId(idStagione))
    Else
        Call listaIdStagioniSelezionate.Remove(ChiaveId(idStagione))
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSelezionaTuttiOrganizzazioniSpettacoli_Click()
    Call SelezionaTuttiOrganizzazioniSpettacoli
End Sub

Private Sub cmdDeselezionaTuttiOrganizzazioniSpettacoli_Click()
    Call DeselezionaTuttiOrganizzazioniSpettacoli
End Sub

Private Sub SelezionaTuttiOrganizzazioniSpettacoli()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idOrganizzazioneSpettacolo As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set listaIdOrganizzazioniSpettacoliSelezionati = Nothing
    Set listaIdOrganizzazioniSpettacoliSelezionati = New Collection
    idOrganizzazioneSpettacolo = idNessunElementoSelezionato
    For i = 1 To lstOrganizzazioneSpettacolo.ListCount
        lstOrganizzazioneSpettacolo.Selected(i - 1) = True
        idOrganizzazioneSpettacolo = lstOrganizzazioneSpettacolo.ItemData(i - 1)
        Call listaIdOrganizzazioniSpettacoliSelezionati.Add(idOrganizzazioneSpettacolo, ChiaveId(idOrganizzazioneSpettacolo))
    Next i
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub DeselezionaTuttiOrganizzazioniSpettacoli()
    Dim i As Integer
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstOrganizzazioneSpettacolo.ListCount
        lstOrganizzazioneSpettacolo.Selected(i - 1) = False
    Next i
    Set listaIdOrganizzazioniSpettacoliSelezionati = Nothing
    Set listaIdOrganizzazioniSpettacoliSelezionati = New Collection
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstOrganizzazioneSpettacolo_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaOrganizzazioneSpettacolo
    End If
End Sub

Private Sub SelezionaDeselezionaOrganizzazioneSpettacolo()
    Dim idOrganizzazioneSpettacolo As Long
    
    idOrganizzazioneSpettacolo = lstOrganizzazioneSpettacolo.ItemData(lstOrganizzazioneSpettacolo.ListIndex)
    If lstOrganizzazioneSpettacolo.Selected(lstOrganizzazioneSpettacolo.ListIndex) Then
        Call listaIdOrganizzazioniSpettacoliSelezionati.Add(idOrganizzazioneSpettacolo, ChiaveId(idOrganizzazioneSpettacolo))
    Else
        Call listaIdOrganizzazioniSpettacoliSelezionati.Remove(ChiaveId(idOrganizzazioneSpettacolo))
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNumeroProdottiSelezionabili_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub CaricaProdottiDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
'    Dim nomeRappresentazione As String
'    Dim idRappresentazione As Long
    Dim chiaveProdotto As String
    Dim prodotto As clsElementoLista
    Dim condSQL As String
    
    Call lstDisponibili.Clear
    
    Call ApriConnessioneBD
    
    Set listaProdottiDisponibili = New Collection
    
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
'        ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
'        End If
'    Else
        If modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_SPETTACOLO Then
            condSQL = ""
            Select Case idTurnoRappresentazioneSelezionato
            Case idTuttiGliElementiSelezionati
                condSQL = " AND R.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE"
            Case ID_NESSUN_TURNO
                condSQL = " AND R.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE(+)"
                condSQL = condSQL & " AND RT.IDRAPPRESENTAZIONE IS NULL"
            Case Else
                condSQL = " AND R.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE"
                condSQL = condSQL & " AND RT.IDTURNORAPPRESENTAZIONE = " & idTurnoRappresentazioneSelezionato
            End Select
            sql = " SELECT DISTINCT P.IDPRODOTTO ID, P.NOME, COUNT(R.IDRAPPRESENTAZIONE) CONT"
            sql = sql & " FROM SPETTACOLO S, RAPPRESENTAZIONE R, RAPPRESENTAZIONE_TURNORAPPR RT,"
            sql = sql & " PRODOTTO_RAPPRESENTAZIONE PR, PRODOTTO P,"
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT IDPRODOTTO "
            sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP, SCELTAPRODOTTO S "
            sql = sql & " WHERE SPP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO "
            sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
            sql = sql & " ) SP"
            sql = sql & " WHERE S.IDSPETTACOLO = R.IDSPETTACOLO"
            sql = sql & " AND R.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE"
            sql = sql & condSQL
            sql = sql & " AND R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
            sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
            sql = sql & " AND P.IDCLASSEPRODOTTO = " & CPR_BIGLIETTERIA_ORDINARIA
            sql = sql & " AND P.IDPRODOTTO = SP.IDPRODOTTO(+)"
            sql = sql & " AND SP.IDPRODOTTO IS NULL"
            sql = sql & " AND S.IDSPETTACOLO IN (" & ElencoDaLista(listaIdOrganizzazioniSpettacoliSelezionati) & ")"
            sql = sql & " AND S.IDSTAGIONE IN (" & ElencoDaLista(listaIdStagioniSelezionate) & ")"
            sql = sql & " GROUP BY P.IDPRODOTTO, P.NOME"
        ElseIf modalitąSelezioneProdottiPerSceltaProdotti = MSPPSP_PER_ORGANIZZAZIONE_PRODOTTO Then
            sql = " SELECT P.IDPRODOTTO ID, P.NOME, COUNT(PR.IDRAPPRESENTAZIONE) CONT"
            sql = sql & " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR,"
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT IDPRODOTTO "
            sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP, SCELTAPRODOTTO S "
            sql = sql & " WHERE SPP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO "
            sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
            sql = sql & " ) SP"
            sql = sql & " WHERE P.IDPRODOTTO = PR.IDPRODOTTO "
            sql = sql & " AND P.IDPRODOTTO = SP.IDPRODOTTO(+)"
            sql = sql & " AND SP.IDPRODOTTO IS NULL"
            sql = sql & " AND IDSTAGIONE IN (" & ElencoDaLista(listaIdStagioniSelezionate) & ")"
            sql = sql & " AND IDORGANIZZAZIONE IN (" & ElencoDaLista(listaIdOrganizzazioniSpettacoliSelezionati) & ")"
            sql = sql & " GROUP BY P.IDPRODOTTO, P.NOME"
            sql = sql & " ORDER BY P.NOME"
        End If
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodotto = New clsElementoLista
            prodotto.idElementoLista = rec("ID").Value
            prodotto.idAttributoElementoLista = rec("CONT").Value
            prodotto.descrizioneElementoLista = rec("NOME") & " - " & rec("CONT")
            chiaveProdotto = ChiaveId(prodotto.idElementoLista)
            Call listaProdottiDisponibili.Add(prodotto, chiaveProdotto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim max As Long
    Dim i As Integer
    Dim idNuovaSceltaProdotto As Long
    Dim n As Long
    Dim prodotto As clsElementoLista
    Dim condizioniSQL As String
    Dim listaRapprNonAssociabili As Collection
    Dim maxOrdine As Integer
    Dim ordineSuccessivo As Integer
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Set listaRapprNonAssociabili = New Collection
    SETAConnection.BeginTrans
    
    sql = "SELECT MAX(ORDINE) MAXORD FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        maxOrdine = IIf(IsNull(rec("MAXORD")), 0, rec("MAXORD").Value)
    End If
    rec.Close
'   INSERIMENTO IN TABELLA SCELTAPRODOTTO
    idNuovaSceltaProdotto = OttieniIdentificatoreDaSequenza("SQ_SCELTAPRODOTTO")
    ordineSuccessivo = maxOrdine + 1
    sql = " INSERT INTO SCELTAPRODOTTO ("
    sql = sql & " IDSCELTAPRODOTTO, IDOFFERTAPACCHETTO, NUMEROPRODOTTISELEZIONABILI, "
    sql = sql & " ORDINE) "
    sql = sql & " VALUES ("
    sql = sql & idNuovaSceltaProdotto & ", "
    sql = sql & idOffertaPacchettoSelezionata & ", "
    sql = sql & numeroProdottiSelezionabili & ", "
    sql = sql & ordineSuccessivo & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO", "IDSCELTAPRODOTTO", idNuovaSceltaProdotto, TSR_NUOVO)
'    End If

'   INSERIMENTO IN TABELLA SCELTAPRODOTTO_PRODOTTO
    If Not (listaProdottiSelezionati Is Nothing) Then
        For Each prodotto In listaProdottiSelezionati
'            If ProdottiCorrelandiBloccati(prodotto.idElementoLista, idOffertaPacchettoSelezionata) Then
                sql = "INSERT INTO SCELTAPRODOTTO_PRODOTTO"
                sql = sql & " (IDSCELTAPRODOTTO, IDPRODOTTO)"
                sql = sql & " VALUES "
                sql = sql & "(" & idNuovaSceltaProdotto & ", "
                sql = sql & prodotto.idElementoLista & ")"
                SETAConnection.Execute sql, n, adCmdText
'                If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'                    condizioniSQL = " AND IDPRODOTTO = " & prodotto.idElementoLista
'                    tipoStatoRecordSelezionato = TSR_NUOVO
'                    Call AggiornaParametriSessioneSuRecord("SCELTAPRODOTTO_PRODOTTO", "IDSCELTAPRODOTTO", idNuovaSceltaProdotto, TSR_NUOVO, condizioniSQL)
'                End If
'            Else
'                Call listaRapprNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
'                Call SETAConnection.RollbackTrans
'            End If
'            Call SbloccaProdottiCorrelandi(getNomeMacchina, idOffertaPacchettoSelezionata)
        Next prodotto
    End If
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idNuovaSeltaRappresentazione, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaSceltaProdotto)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

'Private Sub AggiornaNumeroMaxProdottiSelezionabili()
'    numeroMaxProdottiSelezionabili = listaProdottiSelezionati.count
'End Sub

Public Sub SetTipoOffertaPacchettoSelezionata(tipo As TipoOffertaPacchettoEnum)
    tipoOffertaPacchettoSelezionata = tipo
End Sub

