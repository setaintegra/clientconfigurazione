VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazionePacchettoAree 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   4080
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   4740
      Width           =   3915
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   14
      Top             =   2820
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   13
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   12
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePacchettoAree 
      Height          =   330
      Left            =   6360
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePacchettoAree 
      Height          =   1875
      Left            =   120
      TabIndex        =   15
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   3307
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwPianteProdottiAree 
      Height          =   3495
      Left            =   4200
      TabIndex        =   22
      Top             =   4080
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   6165
      _Version        =   393217
      Indentation     =   706
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin VB.Label lblPianteProdottiAree 
      Caption         =   "Associazione aree"
      Height          =   255
      Left            =   4200
      TabIndex        =   23
      Top             =   3840
      Width           =   4635
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   3840
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   4500
      Width           =   1575
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   2580
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   18
      Top             =   2580
      Width           =   2775
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   17
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Aree pacchetto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazionePacchettoAree"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOffertaPacchettoSelezionata As Long
Private isRecordEditabile As Boolean
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeOffertaPacchettoSelezionata As String
'Private listaProdottiAree As Collection
Private nomeTabellaTemporanea As String
Private numeroProdottiAssociati As Long
Private listaProdottiAreeSelezionate As Collection
Private listaProdottiAreeInDB As Collection

Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pacchetto"
    txtInfo1.Text = nomeOffertaPacchettoSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazionePacchettoAree.Caption = "AREE PACCHETTO CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoAree.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        tvwPianteProdottiAree.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblPianteProdottiAree.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoAree.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        tvwPianteProdottiAree.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblPianteProdottiAree.Enabled = False
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePacchettoAree.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
'        Call tvwPianteProdottiAree.Clear
        tvwPianteProdottiAree.Enabled = True
        lblPianteProdottiAree.Enabled = True
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato And _
            tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioPacchettoArea
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchettoSelezionata)
        Call EliminaTabellaAppoggioPacchettoArea
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub CaricaFormConfigurazioneTariffePacchetto()
    Call frmConfigurazionePacchettoTariffe.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoTariffe.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoTariffe.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoTariffe.SetNumeroProdottiAssociati(numeroProdottiAssociati)
    Call frmConfigurazionePacchettoTariffe.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoTariffe.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazionePacchettoTariffe.Init
End Sub

Public Sub SetIdOffertaPacchettoSelezionata(id As Long)
    idOffertaPacchettoSelezionata = id
End Sub

Public Sub SetNumeroProdottiAssociati(numero As Long)
    numeroProdottiAssociati = numero
End Sub

Public Sub SetNomeOffertaPacchettoSelezionata(nome As String)
    nomeOffertaPacchettoSelezionata = nome
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneTariffePacchetto
    Call EliminaTabellaAppoggioPacchettoArea
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim causaNonEditabilita As String

    causaNonEditabilita = ""
    stringaNota = "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    
    If isOffertaPacchettoBloccataDaUtente Then
    
        Call SetGestioneExitCode(EC_CONFERMA)
        Call AggiornaAbilitazioneControlli
        
        If valoriCampiOK Then
    '        If isRecordEditabile Then
                Select Case gestioneRecordGriglia
                    Case ASG_INSERISCI_NUOVO
                        Call InserisciNellaBaseDati
                        Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_AREA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoAree_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazionePacchettoAree_Init
                    Case ASG_INSERISCI_DA_SELEZIONE
                        Call InserisciNellaBaseDati
                        Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_AREA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoAree_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazionePacchettoAree_Init
                    Case ASG_MODIFICA
                        Call AggiornaNellaBaseDati
                        Call ScriviLog(CCTA_MODIFICA, CCDA_OFFERTA_PACCHETTO, CCDA_AREA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoAree_Init
                        Call SelezionaElementoSuGriglia(idRecordSelezionato)
                        Call dgrConfigurazionePacchettoAree_Init
                    Case ASG_ELIMINA
                        Call EliminaDallaBaseDati
                        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OFFERTA_PACCHETTO, CCDA_AREA_PACCHETTO, stringaNota, idOffertaPacchettoSelezionata)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazionePacchettoAree_Init
                        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                        Call dgrConfigurazionePacchettoAree_Init
                End Select
    '        End If
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
'    Call CaricaValoriLstProdottiAree
    Call tvwPianteProdottiAree_Init
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        Call CaricaGerarchiaPianteAree
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        Call CaricaGerarchiaProdottiPianteAree
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioPacchettoArea
            Unload Me
    End Select
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
'    Call CaricaValoriLstProdottiAree
    Call tvwPianteProdottiAree_Init
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        Call CaricaGerarchiaPianteAree
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        Call CaricaGerarchiaProdottiPianteAree
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    isRecordEditabile = True
    Set listaProdottiAreeSelezionate = New Collection
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call tvwPianteProdottiAree_Init
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        Call CaricaGerarchiaPianteAree
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        Call CaricaGerarchiaProdottiPianteAree
    End If
'    Call CaricaValoriLstProdottiAree
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePacchettoAree.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
'    Call CaricaValoriLstProdottiAree
    Call tvwPianteProdottiAree_Init
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        Call CaricaGerarchiaPianteAree
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        Call CaricaGerarchiaProdottiPianteAree
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazionePacchettoAree_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioPacchettoArea
    Call adcConfigurazionePacchettoAree_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePacchettoAree_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePacchettoAree.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePacchettoAree_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioPacchettoArea

    Set d = adcConfigurazionePacchettoAree
        
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT DISTINCT A.IDAREAPACCHETTO ID, A.NOME ""Nome"", "
'        sql = sql & " A.DESCRIZIONE ""Descrizione"", TMP.NOME ""Aree"","
'        sql = sql & " DECODE(NUMEROASSOCIAZIONI, " & numeroProdottiAssociati & ", 'SI', 'NO') ""Associazione completa"""
'        sql = sql & " FROM AREAPACCHETTO A, " & nomeTabellaTemporanea & " TMP"
'        sql = sql & " WHERE A.IDAREAPACCHETTO = TMP.IDAREAPACCHETTO(+)"
'        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (A.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'        sql = sql & " OR A.IDTIPOSTATORECORD IS NULL)"
'        sql = sql & " ORDER BY ""Nome"""
'    Else
        sql = " SELECT DISTINCT A.IDAREAPACCHETTO ID, A.NOME ""Nome"", "
        sql = sql & " A.DESCRIZIONE ""Descrizione"", TMP.NOME ""Aree"","
        sql = sql & " DECODE(NUMEROASSOCIAZIONI, " & numeroProdottiAssociati & ", 'SI', 'NO') ""Associazione completa"""
        sql = sql & " FROM AREAPACCHETTO A, " & nomeTabellaTemporanea & " TMP"
        sql = sql & " WHERE A.IDAREAPACCHETTO = TMP.IDAREAPACCHETTO(+)"
        sql = sql & " AND A.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ORDER BY ""Nome"""
'    End If
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati_old()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idNuovaAreaPacchetto As Long
    Dim n As Long
    Dim pt As clsElementoLista

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA AREAPACCHETTO
    idNuovaAreaPacchetto = OttieniIdentificatoreDaSequenza("SQ_AREAPACCHETTO")
    sql = " INSERT INTO AREAPACCHETTO ("
    sql = sql & " IDAREAPACCHETTO, NOME, DESCRIZIONE,"
    sql = sql & " IDOFFERTAPACCHETTO)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaAreaPacchetto & " , "
    sql = sql & SqlStringValue(nomeRecordSelezionato) & " , "
    sql = sql & SqlStringValue(descrizioneRecordSelezionato) & " , "
    sql = sql & idOffertaPacchettoSelezionata & " )"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO", "IDAREAPACCHETTO", idNuovaAreaPacchetto, TSR_NUOVO)
'    End If

'    INSERIMENTO IN TABELLA AREAPACCHETTO_AREA
    If Not (listaProdottiAreeSelezionate Is Nothing) Then
        For Each pt In listaProdottiAreeSelezionate
            If pt.idAttributoElementoLista <> idNessunElementoSelezionato Then
                sql = " INSERT INTO AREAPACCHETTO_AREA ("
                sql = sql & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
                sql = sql & " VALUES ( "
                sql = sql & idNuovaAreaPacchetto & " , "
                sql = sql & pt.idAttributoElementoLista & " , "
                sql = sql & pt.idElementoLista & " )"
                SETAConnection.Execute sql, n, adCmdText
            End If
        Next pt
    End If
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idNuovaAreaPacchetto, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD

    Call SetIdRecordSelezionato(idNuovaAreaPacchetto)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long

    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT IDAREAPACCHETTO, NOME, DESCRIZIONE,"
'        sql = sql & " IDOFFERTAPACCHETTO, IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE"
'        sql = sql & " FROM AREAPACCHETTO"
'        sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
'    Else
        sql = " SELECT IDAREAPACCHETTO, NOME, DESCRIZIONE,"
        sql = sql & " IDOFFERTAPACCHETTO"
        sql = sql & " FROM AREAPACCHETTO"
        sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close

    Call ChiudiConnessioneBD

    Call CaricaListaProdottiAree
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    
    Call SelezionaNodi
    
    internalEvent = internalEventOld

End Sub

Private Sub SelezionaNodi()
    Dim i As Integer
    Dim idArea As Long
    Dim idProdotto As Long
    Dim nodoCorrente As Node
    Dim t As TreeView
    Dim chiave As String
    
    Set listaProdottiAreeSelezionate = New Collection
    Set t = tvwPianteProdottiAree
    For i = 1 To listaProdottiAreeInDB.count
'        idArea = listaProdottiAreeInDB.Item(i)
        If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
            tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
            idArea = IdChiave(listaProdottiAreeInDB.Item(i))
            chiave = ChiaveId(idArea)
        ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
            idProdotto = IdChiaveMultipla(listaProdottiAreeInDB.Item(i), 1)
            idArea = IdChiaveMultipla(listaProdottiAreeInDB.Item(i), 2)
            chiave = ChiaveId(idProdotto) & ChiaveId(idArea)
        End If
        Set nodoCorrente = t.Nodes(chiave)
        Call SelezionaDeselezionaTuttiNodiFigli(nodoCorrente, True)
        Call SelezionaTuttiNodiPadri(nodoCorrente)
    Next i
    Set listaProdottiAreeInDB = Nothing
End Sub

Private Sub AggiornaNellaBaseDati_OLD()
    Dim sql As String
    Dim n As Long
    Dim pa As clsElementoLista
    Dim condizioniSQL As String

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   AGGIORNAMENTO TABELLA TARIFFAPACCHETTO
    sql = " UPDATE AREAPACCHETTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ", "
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato)
    sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO", "IDAREAPACCHETTO", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If

'   AGGIORNAMENTO TABELLA PACCHETTOTARIFFA_TARIFFA
    If Not (listaProdottiAreeSelezionate Is Nothing) Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'        Else
            sql = "DELETE FROM AREAPACCHETTO_AREA WHERE IDAREAPACCHETTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
        For Each pa In listaProdottiAreeSelezionate
            If pa.idAttributoElementoLista <> idNessunElementoSelezionato Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO AREAPACCHETTO_AREA"
'                    sql = sql & " (IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
'                    sql = sql & " (SELECT "
'                    sql = sql & idRecordSelezionato & " IDAREAPACCHETTO, "
'                    sql = sql & pa.idAttributoElementoLista & " IDAREA, "
'                    sql = sql & pa.idElementoLista & " IDPRODOTTO"
'                    sql = sql & " FROM DUAL"
'                    sql = sql & " MINUS"
'                    sql = sql & " SELECT IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
'                    sql = sql & " FROM AREAPACCHETTO_AREA"
'                    sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
'                    sql = sql & " AND IDPRODOTTO = " & pa.idElementoLista
'                    sql = sql & " AND IDAREA = " & pa.idAttributoElementoLista & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                    condizioniSQL = " AND IDAREAPACCHETTO = " & pa.idAttributoElementoLista & _
'                        " AND IDPRODOTTO " & pa.idElementoLista
'                    Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'                Else
                    sql = "INSERT INTO AREAPACCHETTO_AREA"
                    sql = sql & " (IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & idRecordSelezionato & ", "
                    sql = sql & pa.idAttributoElementoLista & ", "
                    sql = sql & pa.idElementoLista & ")"
                    SETAConnection.Execute sql, n, adCmdText
'                End If
            End If
        Next pa
    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazionePacchettoAree_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePacchettoAree
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDAREAPACCHETTO <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("AREAPACCHETTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata, _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB per lo stesso Pacchetto;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If
    
End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetTipoOffertaPacchettoSelezionata(tipo As TipoOffertaPacchettoEnum)
    tipoOffertaPacchettoSelezionata = tipo
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazionePacchettoOrdiniScelteProdotto.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazionePacchettoOrdiniScelteProdotto.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim listaTabelleCorrelate As Collection
    Dim areaPacchettoEliminabile As Boolean
    Dim tabelleCorrelate As String
    Dim n As Long

    Call ApriConnessioneBD

    Set listaTabelleCorrelate = New Collection
    areaPacchettoEliminabile = True

'    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTO", CStr(idTar)) Then
'        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
'        tariffaEliminabile = False
'    End If

    If areaPacchettoEliminabile Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                sql = "DELETE FROM AREAPACCHETTO_AREA WHERE IDAREAPACCHETTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'                sql = "DELETE FROM AREAPACCHETTO WHERE IDAREAPACCHETTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'            Else
'                Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'                Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO", "IDAREAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'            End If
'        Else
            sql = "DELETE FROM AREAPACCHETTO_AREA WHERE IDAREAPACCHETTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
            sql = "DELETE FROM AREAPACCHETTO WHERE IDAREAPACCHETTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "L'Area pacchetto selezionata", tabelleCorrelate)
    End If

    Call ChiudiConnessioneBD

End Sub

Private Sub CreaTabellaAppoggioPacchettoArea()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_PACCH_AREA_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDAREAPACCHETTO NUMBER(10), NOME VARCHAR2(2000), NUMEROASSOCIAZIONI NUMBER(10))"
    
    Call EliminaTabellaAppoggioPacchettoArea
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioPacchettoArea()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioPacchettoArea_old()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idTariffaArea As Long
    Dim elencoNomi As String
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    Dim listaAppoggioPacchettoArea As Collection
    Dim numeroPiantaAssociate As Integer
    Dim idPiantaCorrente As Long
    Dim idPiantaPrecedente As Long
    
    Call ApriConnessioneBD
    
    Set listaAppoggioPacchettoArea = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT IDAREAPACCHETTO"
'        sql = sql & " FROM AREAPACCHETTO"
'        sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (IDTIPOSTATORECORD IS NULL OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = " SELECT IDAREAPACCHETTO"
        sql = sql & " FROM AREAPACCHETTO"
        sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDAREAPACCHETTO").Value
            Call listaAppoggioPacchettoArea.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioPacchettoArea
        campoNome = ""
        numeroPiantaAssociate = 0
        idPiantaPrecedente = idNessunElementoSelezionato
        If tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
            sql = " SELECT A.IDAREA ID, A.IDPIANTA, A.CODICE || ' - ' || A.NOME LABEL"
        Else
            sql = " SELECT DISTINCT A.IDAREA ID, A.IDPIANTA, A.CODICE || ' - ' || A.NOME LABEL"
        End If
        sql = sql & " FROM AREA A, AREAPACCHETTO_AREA AP"
        sql = sql & " WHERE AP.IDAREA(+) = A.IDAREA"
        sql = sql & " AND AP.IDAREAPACCHETTO = " & recordTemporaneo.idElementoLista
        sql = sql & " ORDER BY A.IDPIANTA"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("LABEL"), campoNome & "; " & rec("LABEL"))
                idPiantaCorrente = rec("IDPIANTA").Value
                If idPiantaCorrente <> idPiantaPrecedente Then
                    numeroPiantaAssociate = numeroPiantaAssociate + 1
                End If
                idPiantaPrecedente = idPiantaCorrente
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
        recordTemporaneo.idAttributoElementoLista = numeroPiantaAssociate 'nota: qui dentro metto il numero di AREE (quindi non č un id)
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioPacchettoArea
        idTariffaArea = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea
        sql = sql & " VALUES (" & idTariffaArea & ", "
        sql = sql & SqlStringValue(elencoNomi) & ", "
        sql = sql & recordTemporaneo.idAttributoElementoLista & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub


Private Function RilevataAreeAssociataAProdotto(idProdotto As Long) As Boolean
    Call frmDettagliAreaPacchetto.SetIdProdottoSelezionato(idProdotto)
    Call frmDettagliAreaPacchetto.Init
    If frmDettagliAreaPacchetto.GetExitCode = EC_CONFERMA Then
        RilevataAreeAssociataAProdotto = True
    Else
        RilevataAreeAssociataAProdotto = False
    End If
End Function

Private Sub tvwPianteProdottiAree_Init()
'   Dim nodo As Node
   
   tvwPianteProdottiAree.CheckBoxes = True
'   Set nodo = tvwCaratteristiche.Nodes.Add(, , codiceRadice, "Prodotto Master")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceAttributiStandard, "attributi standard")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceContratto, "contratto")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceAliquotaIVA, "aliquota IVA")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceRateo, "rateo")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codicePropagazProtezAiProdCorrelati, "propapagaz. protez. ai prodotti correl.")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceAttributiStandard, tvwChild, codiceClasseProdotto, "classe prodotto")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceDurateRiservazione, "durate riservazioni")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceDurateRiservazione, tvwChild, codiceRiservazioneWeb, "RISERVAZIONE_WEB")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceDurateRiservazione, tvwChild, codiceRiservazioneWebProrogata, "RISERVAZIONE_WEB_PROROGATA")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceAssociazioneTipiLayoutSupporti, "associazione tipi/layout supporto")
'   Set nodo = tvwCaratteristiche.Nodes.Add(codiceRadice, tvwChild, codiceDirittiOperatoreSuProdotto, "diritti operatore su prodotto")
'   nodo.EnsureVisible
   tvwPianteProdottiAree.BorderStyle = vbFixedSingle
End Sub

Private Sub CaricaGerarchiaPianteAree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim level As Integer
    Dim idArea As Long
    Dim idPianta As Long
    Dim idPadre As Long
    Dim chiaveArea As String
    Dim chiavePadre As String
    Dim chiavePianta As String
    Dim nomeArea As String
    Dim nomePianta As String
    Dim t As TreeView
'    Dim l As ImageList
    Dim nodo As Node
'    Dim tipoSA As TipoAreaEnum
    
    Set t = Nothing
    Set t = tvwPianteProdottiAree
    Call t.Nodes.Clear
'    Set l = imlCompletezza
'    chiavePianta = ChiaveId(idTuttiGliElementiSelezionati)
'    Set nodo = t.Nodes.Add(, , chiavePianta, "Pianta")
'    t.Nodes(chiavePianta).Image = IAT_PIANTA
    Call ApriConnessioneBD
    
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
'        sql = " SELECT LEVEL, A.IDPIANTA, PP.NOME PIANTA, A.IDAREA,"
'        sql = sql & " A.NOME AREA, A.CODICE, A.IDAREA_PADRE"
'        sql = sql & " FROM AREA A,"
'        sql = sql & " ("
'        sql = sql & " SELECT DISTINCT PI.IDPIANTA, PI.NOME"
'        sql = sql & " FROM PIANTA PI, PRODOTTO P, OFFERTAPACCHETTO O,"
'        sql = sql & " SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
'        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
'        sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
'        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND PI.IDPIANTA = P.IDPIANTA"
'        sql = sql & " ) PP"
'        sql = sql & " WHERE A.IDPIANTA = PP.IDPIANTA "
'        sql = sql & " AND A.IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
'        sql = sql & " START WITH A.IDAREA_PADRE IS NULL"
'        sql = sql & " CONNECT BY PRIOR A.IDAREA = A.IDAREA_PADRE"
'    '    sql = sql & " ORDER BY LEVEL, PIANTA, AREA"
'        sql = sql & " ORDER BY PIANTA, LEVEL, CODICE, AREA"
        sql = " SELECT LEVEL, A.IDPIANTA, PP.NOME PIANTA, A.IDAREA,"
        sql = sql & " A.NOME AREA, A.CODICE, A.IDAREA_PADRE"
        sql = sql & " FROM AREA A,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT PI.IDPIANTA, PI.NOME"
        sql = sql & " FROM PIANTA PI, PRODOTTO P, OFFERTAPACCHETTO O,"
        sql = sql & " SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
        sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " AND PI.IDPIANTA = P.IDPIANTA "
        sql = sql & " ) PP,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT IDAREA"
        sql = sql & " FROM AREAPACCHETTO_AREA AA1, AREAPACCHETTO AP"
        sql = sql & " WHERE AA1.IDAREAPACCHETTO = AP.IDAREAPACCHETTO "
        sql = sql & " AND AP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " AND AP.IDAREAPACCHETTO <> " & idRecordSelezionato
        sql = sql & " ) A1"
        sql = sql & " WHERE A.IDPIANTA = PP.IDPIANTA"
        sql = sql & " AND A.IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
        sql = sql & " AND A.IDAREA = A1.IDAREA(+)"
        sql = sql & " AND A1.IDAREA IS NULL"
        sql = sql & " START WITH A.IDAREA_PADRE IS NULL "
        sql = sql & " CONNECT BY PRIOR A.IDAREA = A.IDAREA_PADRE "
        sql = sql & " ORDER BY PIANTA, LEVEL, CODICE, AREA"
    ElseIf gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
        sql = " SELECT LEVEL, A.IDPIANTA, PP.NOME PIANTA, A.IDAREA,"
        sql = sql & " A.NOME AREA, A.CODICE, A.IDAREA_PADRE"
        sql = sql & " FROM AREA A,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT PI.IDPIANTA, PI.NOME"
        sql = sql & " FROM PIANTA PI, PRODOTTO P, OFFERTAPACCHETTO O,"
        sql = sql & " SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
        sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
        sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
        sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " AND PI.IDPIANTA = P.IDPIANTA "
        sql = sql & " ) PP,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT IDAREA"
        sql = sql & " FROM AREAPACCHETTO_AREA AA1, AREAPACCHETTO AP"
        sql = sql & " WHERE AA1.IDAREAPACCHETTO = AP.IDAREAPACCHETTO "
        sql = sql & " AND AP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
        sql = sql & " ) A1"
        sql = sql & " WHERE A.IDPIANTA = PP.IDPIANTA"
        sql = sql & " AND A.IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
        sql = sql & " AND A.IDAREA = A1.IDAREA(+)"
        sql = sql & " AND A1.IDAREA IS NULL"
        sql = sql & " START WITH A.IDAREA_PADRE IS NULL "
        sql = sql & " CONNECT BY PRIOR A.IDAREA = A.IDAREA_PADRE "
        sql = sql & " ORDER BY PIANTA, LEVEL, CODICE, AREA"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    DoEvents
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not (rec.EOF)
            level = rec("LEVEL").Value
            idPianta = rec("IDPIANTA").Value
            chiavePianta = ChiaveId(idPianta)
            idArea = rec("IDAREA")
            nomeArea = rec("CODICE") & " - " & rec("AREA")
            nomePianta = rec("PIANTA")
            idPadre = IIf(IsNull(rec("IDAREA_PADRE")), idPianta, rec("IDAREA_PADRE"))
            chiaveArea = ChiaveId(idArea)
            chiavePadre = ChiaveId(idPadre)
            If idPadre = idPianta Then
                Call AggiungiPiantaATreeView(chiavePianta, nomePianta)
                Set nodo = t.Nodes.Add(chiavePadre, tvwChild, chiaveArea, nomeArea)
            Else
                Set nodo = t.Nodes.Add(chiavePadre, tvwChild, chiaveArea, nomeArea)
            End If
'            t.Nodes(chiaveArea).Image = IAT_MANCANTE
            Call nodo.EnsureVisible
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AggiungiPiantaATreeView(chiave As String, nome As String)
    Dim t As TreeView
    Dim nodo As Node
    
    Set t = tvwPianteProdottiAree
    
On Error Resume Next
    Set nodo = t.Nodes.Add(, , chiave, nome)
End Sub

Private Sub CaricaGerarchiaProdottiPianteAree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim level As Integer
    Dim idArea As Long
    Dim idPianta As Long
    Dim idProdotto As Long
    Dim idPadre As Long
    Dim chiaveProdottoArea As String
    Dim chiaveProdottoPadre As String
    Dim chiaveProdottoPianta As String
    Dim nomeArea As String
    Dim nomeProdottoPianta As String
    Dim t As TreeView
'    Dim l As ImageList
    Dim nodo As Node
'    Dim tipoSA As TipoAreaEnum
    
    Set t = Nothing
    Set t = tvwPianteProdottiAree
    Call t.Nodes.Clear
'    Set l = imlCompletezza
    
'    chiavePianta = ChiaveId(idTuttiGliElementiSelezionati)
'    Set nodo = t.Nodes.Add(, , chiavePianta, "Pianta")
'    t.Nodes(chiavePianta).Image = IAT_PIANTA
    Call ApriConnessioneBD
    
    sql = " SELECT DISTINCT LEVEL, A.IDPIANTA, PP.IDPRODOTTO, PP.PROPIA PIANTA, A.IDAREA, "
    sql = sql & " A.NOME AREA, A.CODICE, A.IDAREA_PADRE"
    sql = sql & " FROM AREA A,"
    sql = sql & " ("
    sql = sql & " SELECT PI.IDPIANTA, P.NOME ||' ('|| PI.NOME || ')' PROPIA, P.IDPRODOTTO"
    sql = sql & " FROM PIANTA PI, PRODOTTO P, OFFERTAPACCHETTO O, "
    sql = sql & " SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S"
    sql = sql & " WHERE SP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " AND PI.IDPIANTA = P.IDPIANTA"
    sql = sql & " ) PP"
    sql = sql & " WHERE A.IDPIANTA = PP.IDPIANTA"
    sql = sql & " AND A.IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
    sql = sql & " START WITH A.IDAREA_PADRE IS NULL"
    sql = sql & " CONNECT BY PRIOR A.IDAREA = A.IDAREA_PADRE"
'    sql = sql & " ORDER BY PP.IDPRODOTTO, PIANTA, LEVEL, CODICE, AREA"
    sql = sql & " ORDER BY PIANTA, LEVEL, CODICE, AREA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    DoEvents
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not (rec.EOF)
            level = rec("LEVEL").Value
            idPianta = rec("IDPIANTA").Value
            idProdotto = rec("IDPRODOTTO").Value
            chiaveProdottoPianta = ChiaveId(idProdotto) & ChiaveId(idPianta)
            idArea = rec("IDAREA")
            nomeArea = rec("CODICE") & " - " & rec("AREA")
            nomeProdottoPianta = rec("PIANTA")
            idPadre = IIf(IsNull(rec("IDAREA_PADRE")), idPianta, rec("IDAREA_PADRE"))
            chiaveProdottoArea = ChiaveId(idProdotto) & ChiaveId(idArea)
            chiaveProdottoPadre = ChiaveId(idProdotto) & ChiaveId(idPadre)
            If idPadre = idPianta Then
                Call AggiungiPiantaATreeView(chiaveProdottoPianta, nomeProdottoPianta)
                Set nodo = t.Nodes.Add(chiaveProdottoPadre, tvwChild, chiaveProdottoArea, nomeArea)
            Else
                Set nodo = t.Nodes.Add(chiaveProdottoPadre, tvwChild, chiaveProdottoArea, nomeArea)
            End If
'            t.Nodes(chiaveArea).Image = IAT_MANCANTE
            Call nodo.EnsureVisible
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub tvwPianteProdottiAree_NodeCheck(ByVal nodoCorrente As MSComctlLib.Node)
    If Not internalEvent Then
        Call SelezionaDeselezionaElementiTreeView(nodoCorrente)
    End If
End Sub

Private Sub SelezionaDeselezionaElementiTreeView(nodoCorrente As MSComctlLib.Node)
    If nodoCorrente.Checked Then
        Call SelezionaDeselezionaTuttiNodiFigli(nodoCorrente, True)
        Call SelezionaTuttiNodiPadri(nodoCorrente)
    ElseIf Not nodoCorrente.Checked Then
        Call SelezionaDeselezionaTuttiNodiFigli(nodoCorrente, False)
        Call DeselezionaTuttiNodiPadri(nodoCorrente)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaDeselezionaTuttiNodiFigli_old(nodoCorrente As Node, statoFinale As Boolean)
    Dim nodoFiglio As Node
    Dim i As Integer
    
    If nodoCorrente.Children > 0 Then
        Set nodoFiglio = nodoCorrente.Child
        nodoFiglio.Checked = statoFinale
'        Call SelezionaDeselezionaTuttiNodiFigli(nodoFiglio, statoFinale)
        If statoFinale = True Then
            Call listaProdottiAreeSelezionate.Add(IdChiave(nodoFiglio.Key), nodoFiglio.Key)
        Else
            Call listaProdottiAreeSelezionate.Remove(nodoFiglio.Key)
        End If
        For i = 1 To nodoCorrente.Children - 1
            Set nodoFiglio = nodoFiglio.Next
            Call SelezionaDeselezionaTuttiNodiFigli(nodoFiglio, statoFinale)
        Next i
    Else
        nodoCorrente.Checked = statoFinale
        If statoFinale = True Then
            Call listaProdottiAreeSelezionate.Add(IdChiave(nodoCorrente.Key), nodoCorrente.Key)
        Else
            Call listaProdottiAreeSelezionate.Remove(nodoCorrente.Key)
        End If
    End If
End Sub

Private Function TuttiNodiFratelliDeselezionati(nodoPadre As Node) As Boolean
    Dim deselezionato As Boolean
    Dim nodoCorrente As Node
    Dim i As Integer
    
    deselezionato = True
    If nodoPadre.Children > 0 Then
        Set nodoCorrente = nodoPadre.Child
        For i = 1 To nodoPadre.Children
            deselezionato = deselezionato And Not nodoCorrente.Checked
            Set nodoCorrente = nodoCorrente.Next
        Next i
    End If
    
    TuttiNodiFratelliDeselezionati = deselezionato
End Function

Private Sub SelezionaTuttiNodiPadri(nodoCorrente As Node)
    Dim nodoPadre As Node
    
    Set nodoPadre = nodoCorrente.Parent
    If Not (nodoPadre Is Nothing) Then
        nodoPadre.Checked = True
        Call SelezionaTuttiNodiPadri(nodoPadre)
    End If
End Sub

Private Sub DeselezionaTuttiNodiPadri(nodoCorrente As Node)
    Dim nodoPadre As Node
    
    Set nodoPadre = nodoCorrente.Parent
    If Not (nodoPadre Is Nothing) Then
        If TuttiNodiFratelliDeselezionati(nodoPadre) Then
            nodoPadre.Checked = False
            Call DeselezionaTuttiNodiPadri(nodoPadre)
        End If
    End If
End Sub

Private Sub SelezionaDeselezionaTuttiNodiFigli(nodoCorrente As Node, statoFinale As Boolean)
    Dim nodoFiglio As Node
    Dim i As Integer
    
    If nodoCorrente.Children > 0 Then
        Set nodoFiglio = nodoCorrente.Child
        nodoFiglio.Checked = statoFinale
        For i = 1 To nodoCorrente.Children
            Call SelezionaDeselezionaTuttiNodiFigli(nodoFiglio, statoFinale)
            Set nodoFiglio = nodoFiglio.Next
        Next i
    Else
        nodoCorrente.Checked = statoFinale
        If statoFinale = True Then
'            Call listaProdottiAreeSelezionate.Add(IdChiave(nodoCorrente.Key), nodoCorrente.Key)
            Call listaProdottiAreeSelezionate.Add(nodoCorrente.Key, nodoCorrente.Key)
        Else
'            Call listaProdottiAreeSelezionate.Remove(nodoCorrente.Key)
            Call EliminaElementoDaLista(listaProdottiAreeSelezionate, nodoCorrente.Key)
        End If
    End If
End Sub

Private Sub EliminaElementoDaLista(lista As Collection, chiaveElemento As String)
    On Error Resume Next
    Call lista.Remove(chiaveElemento)
End Sub

Private Sub CaricaListaProdottiAree_old()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idArea As Long
    Dim idProdotto As Long
    Dim prodottoArea As clsElementoLista
    Dim chiaveArea As String
    Dim chiaveProdotto As String
    
    Call ApriConnessioneBD
    
    Set listaProdottiAreeSelezionate = New Collection
    sql = " SELECT IDAREA, IDPRODOTTO"
    sql = sql & " FROM AREAPACCHETTO_AREA AA"
    sql = sql & " WHERE AA.IDAREAPACCHETTO = " & idRecordSelezionato
'    sql = " SELECT DISTINCT AA.IDAREA, A.IDPIANTA"
'    sql = sql & " FROM AREAPACCHETTO_AREA AA, AREA A"
'    sql = sql & " WHERE AA.IDAREA = A.IDAREA"
'    sql = sql & " AND AA.IDAREAPACCHETTO = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodottoArea = New clsElementoLista
            idArea = rec("IDAREA").Value
            idProdotto = rec("IDPRODOTTO").Value
            prodottoArea.idElementoLista = idProdotto
            prodottoArea.idAttributoElementoLista = idArea
            chiaveArea = ChiaveId(idArea)
            chiaveProdotto = ChiaveId(idProdotto)
'            Call listaProdottiAreeSelezionate.Add(id, ChiaveId(id))
            If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
                tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
                Call listaProdottiAreeSelezionate.Add(prodottoArea, chiaveArea)
            ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
                Call listaProdottiAreeSelezionate.Add(prodottoArea, chiaveProdotto & chiaveArea)
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CaricaListaProdottiAree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idArea As Long
    Dim idProdotto As Long
'    Dim prodottoArea As clsElementoLista
'    Dim chiaveArea As String
'    Dim chiaveProdotto As String
    
    Call ApriConnessioneBD
    
    Set listaProdottiAreeInDB = New Collection
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        sql = " SELECT DISTINCT IDAREA"
        sql = sql & " FROM AREAPACCHETTO_AREA AA"
        sql = sql & " WHERE AA.IDAREAPACCHETTO = " & idRecordSelezionato
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        sql = " SELECT DISTINCT IDAREA, IDPRODOTTO"
        sql = sql & " FROM AREAPACCHETTO_AREA AA"
        sql = sql & " WHERE AA.IDAREAPACCHETTO = " & idRecordSelezionato
    End If
'    sql = " SELECT DISTINCT AA.IDAREA, A.IDPIANTA"
'    sql = sql & " FROM AREAPACCHETTO_AREA AA, AREA A"
'    sql = sql & " WHERE AA.IDAREA = A.IDAREA"
'    sql = sql & " AND AA.IDAREAPACCHETTO = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idArea = rec("IDAREA").Value
            If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
                tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
'                Call listaProdottiAreeInDB.Add(idArea, ChiaveId(idArea))
                Call listaProdottiAreeInDB.Add(ChiaveId(idArea), ChiaveId(idArea))
            ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
                idProdotto = rec("IDPRODOTTO").Value
                Call listaProdottiAreeInDB.Add(ChiaveId(idProdotto) & ChiaveId(idArea), ChiaveId(idProdotto) & ChiaveId(idArea))
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idNuovaAreaPacchetto As Long
    Dim n As Long
'    Dim pt As clsElementoLista
    Dim i As Integer

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA AREAPACCHETTO
    idNuovaAreaPacchetto = OttieniIdentificatoreDaSequenza("SQ_AREAPACCHETTO")
    sql = " INSERT INTO AREAPACCHETTO ("
    sql = sql & " IDAREAPACCHETTO, NOME, DESCRIZIONE,"
    sql = sql & " IDOFFERTAPACCHETTO)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaAreaPacchetto & " , "
    sql = sql & SqlStringValue(nomeRecordSelezionato) & " , "
    sql = sql & SqlStringValue(descrizioneRecordSelezionato) & " , "
    sql = sql & idOffertaPacchettoSelezionata & " )"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO", "IDAREAPACCHETTO", idNuovaAreaPacchetto, TSR_NUOVO)
'    End If

'    INSERIMENTO IN TABELLA AREAPACCHETTO_AREA
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        If Not (listaProdottiAreeSelezionate Is Nothing) Then
            For i = 1 To listaProdottiAreeSelezionate.count
                sql = " INSERT INTO AREAPACCHETTO_AREA ("
                sql = sql & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
                sql = sql & " SELECT " & idNuovaAreaPacchetto & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
                sql = sql & " FROM AREA A,"
                sql = sql & " ("
                sql = sql & " SELECT DISTINCT SP.IDPRODOTTO, P.IDPIANTA"
                sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S, PRODOTTO P"
                sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
                sql = sql & " AND SP.IDPRODOTTO = P.IDPRODOTTO"
                sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
                sql = sql & " ) PP"
                sql = sql & " WHERE PP.IDPIANTA = A.IDPIANTA"
                sql = sql & " AND A.IDAREA =  " & IdChiave(listaProdottiAreeSelezionate.Item(i))
                SETAConnection.Execute sql, n, adCmdText
            Next i
        End If
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        If Not (listaProdottiAreeSelezionate Is Nothing) Then
            For i = 1 To listaProdottiAreeSelezionate.count
                sql = " INSERT INTO AREAPACCHETTO_AREA ("
                sql = sql & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO) "
                sql = sql & " VALUES ("
                sql = sql & idNuovaAreaPacchetto & ", "
                sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & ", "
                sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1) & ")"
                SETAConnection.Execute sql, n, adCmdText
            Next i
        End If
    End If
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idNuovaAreaPacchetto, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD

    Call SetIdRecordSelezionato(idNuovaAreaPacchetto)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
'    Dim pa As clsElementoLista
    Dim condizioniSQL As String
    Dim i As Integer

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'   AGGIORNAMENTO TABELLA TARIFFAPACCHETTO
    sql = " UPDATE AREAPACCHETTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ", "
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato)
    sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO", "IDAREAPACCHETTO", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If

'   AGGIORNAMENTO TABELLA PACCHETTOTARIFFA_TARIFFA
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idRecordSelezionato, TSR_ELIMINATO)
'    Else
        sql = "DELETE FROM AREAPACCHETTO_AREA WHERE IDAREAPACCHETTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Or _
        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
        If Not (listaProdottiAreeSelezionate Is Nothing) Then
            For i = 1 To listaProdottiAreeSelezionate.count
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''                    sql = "INSERT INTO AREAPACCHETTO_AREA"
''                    sql = sql & " (IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
''                    sql = sql & " (SELECT "
''                    sql = sql & idRecordSelezionato & " IDAREAPACCHETTO, "
''                    sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & " IDAREA, "
''                    sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1) & " IDPRODOTTO"
''                    sql = sql & " FROM DUAL"
''                    sql = sql & " MINUS"
''                    sql = sql & " SELECT IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
''                    sql = sql & " FROM AREAPACCHETTO_AREA"
''                    sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
''                    sql = sql & " AND IDPRODOTTO = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1)
''                    sql = sql & " AND IDAREA = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & ")"
'                    sql = " INSERT INTO AREAPACCHETTO_AREA ("
'                    sql = sql & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
'                    sql = sql & " (SELECT " & idRecordSelezionato & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
'                    sql = sql & " FROM AREA A,"
'                    sql = sql & " ("
'                    sql = sql & " SELECT DISTINCT SP.IDPRODOTTO, P.IDPIANTA"
'                    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S, PRODOTTO P"
'                    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
'                    sql = sql & " AND SP.IDPRODOTTO = P.IDPRODOTTO"
'                    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'                    sql = sql & " ) PP"
'                    sql = sql & " WHERE PP.IDPIANTA = A.IDPIANTA"
'                    sql = sql & " AND A.IDAREA =  " & IdChiave(listaProdottiAreeSelezionate.Item(i))
'                    sql = sql & " MINUS"
'                    sql = sql & " SELECT IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
'                    sql = sql & " FROM AREAPACCHETTO_AREA"
'                    sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
''                    sql = sql & " AND IDPRODOTTO = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1)
'                    sql = sql & " AND IDAREA = " & IdChiave(listaProdottiAreeSelezionate.Item(i)) & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                    condizioniSQL = " AND IDAREAPACCHETTO = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & _
'                        " AND IDPRODOTTO " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1)
'                    Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'                Else
                    sql = " INSERT INTO AREAPACCHETTO_AREA ("
                    sql = sql & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
                    sql = sql & " SELECT " & idRecordSelezionato & " IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
                    sql = sql & " FROM AREA A,"
                    sql = sql & " ("
                    sql = sql & " SELECT DISTINCT SP.IDPRODOTTO, P.IDPIANTA"
                    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SP, SCELTAPRODOTTO S, PRODOTTO P"
                    sql = sql & " WHERE SP.IDSCELTAPRODOTTO = S.IDSCELTAPRODOTTO"
                    sql = sql & " AND SP.IDPRODOTTO = P.IDPRODOTTO"
                    sql = sql & " AND S.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
                    sql = sql & " ) PP"
                    sql = sql & " WHERE PP.IDPIANTA = A.IDPIANTA"
                    sql = sql & " AND A.IDAREA =  " & IdChiave(listaProdottiAreeSelezionate.Item(i))
                    SETAConnection.Execute sql, n, adCmdText
'                End If
            Next i
        End If
    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
        If Not (listaProdottiAreeSelezionate Is Nothing) Then
            For i = 1 To listaProdottiAreeSelezionate.count
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO AREAPACCHETTO_AREA"
'                    sql = sql & " (IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
'                    sql = sql & " (SELECT "
'                    sql = sql & idRecordSelezionato & " IDAREAPACCHETTO, "
'                    sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & " IDAREA, "
'                    sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1) & " IDPRODOTTO"
'                    sql = sql & " FROM DUAL"
'                    sql = sql & " MINUS"
'                    sql = sql & " SELECT IDAREAPACCHETTO, IDAREA, IDPRODOTTO"
'                    sql = sql & " FROM AREAPACCHETTO_AREA"
'                    sql = sql & " WHERE IDAREAPACCHETTO = " & idRecordSelezionato
'                    sql = sql & " AND IDPRODOTTO = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1)
'                    sql = sql & " AND IDAREA = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                    condizioniSQL = " AND IDAREAPACCHETTO = " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & _
'                        " AND IDPRODOTTO " & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1)
'                    Call AggiornaParametriSessioneSuRecord("AREAPACCHETTO_AREA", "IDAREAPACCHETTO", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'                Else
                    sql = "INSERT INTO AREAPACCHETTO_AREA"
                    sql = sql & " (IDAREAPACCHETTO, IDAREA, IDPRODOTTO)"
                    sql = sql & " VALUES "
                    sql = sql & "(" & idRecordSelezionato & ", "
                    sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 2) & ", "
                    sql = sql & IdChiaveMultipla(listaProdottiAreeSelezionate.Item(i), 1) & ")"
                    SETAConnection.Execute sql, n, adCmdText
'                End If
            Next i
        End If
    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub PopolaTabellaAppoggioPacchettoArea()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idTariffaArea As Long
    Dim elencoNomi As String
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    Dim listaAppoggioPacchettoArea As Collection
    Dim numeroPiantaAssociate As Integer
    Dim idPiantaCorrente As Long
    Dim idPiantaPrecedente As Long
    Dim idProdottoCorrente As Long
    Dim idProdottoPrecedente As Long
    Dim descrizione As String
    
    Call ApriConnessioneBD
    
    Set listaAppoggioPacchettoArea = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT IDAREAPACCHETTO"
'        sql = sql & " FROM AREAPACCHETTO"
'        sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'        sql = sql & " AND (IDTIPOSTATORECORD IS NULL OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = " SELECT IDAREAPACCHETTO"
        sql = sql & " FROM AREAPACCHETTO"
        sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDAREAPACCHETTO").Value
            Call listaAppoggioPacchettoArea.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioPacchettoArea
        campoNome = ""
        descrizione = ""
        numeroPiantaAssociate = 0
        idPiantaPrecedente = idNessunElementoSelezionato
        idProdottoPrecedente = idNessunElementoSelezionato
        If tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
            sql = " SELECT A.IDAREA ID, A.IDPIANTA, AP.IDPRODOTTO, "
            sql = sql & " P.NOME PIA, PR.NOME PROD, A.CODICE || ' - ' || A.NOME LABEL"
            sql = sql & " FROM AREA A, AREAPACCHETTO_AREA AP, PIANTA P, PRODOTTO PR"
            sql = sql & " WHERE AP.IDAREA(+) = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = P.IDPIANTA"
            sql = sql & " AND AP.IDPRODOTTO = PR.IDPRODOTTO"
            sql = sql & " AND AP.IDAREAPACCHETTO = " & recordTemporaneo.idElementoLista
            sql = sql & " ORDER BY PIA, PROD, LABEL"
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idPiantaCorrente = rec("IDPIANTA").Value
                    idProdottoCorrente = rec("IDPRODOTTO").Value
                    If idPiantaCorrente <> idPiantaPrecedente Then
                        descrizione = rec("PIA") & " (" & rec("PROD") & "): " & rec("LABEL")
                        campoNome = IIf(campoNome = "", descrizione, campoNome & "; " & descrizione)
                        numeroPiantaAssociate = numeroPiantaAssociate + 1
                    Else
                        If idProdottoCorrente <> idProdottoPrecedente Then
                            descrizione = rec("PIA") & " (" & rec("PROD") & "): " & rec("LABEL")
                            campoNome = IIf(campoNome = "", descrizione, campoNome & "; " & descrizione)
                        Else
                            campoNome = IIf(campoNome = "", rec("LABEL"), campoNome & ", " & rec("LABEL"))
                        End If
                    End If
                    idPiantaPrecedente = idPiantaCorrente
                    idProdottoPrecedente = idProdottoCorrente
                    rec.MoveNext
                Wend
            End If
            rec.Close
        Else
            sql = " SELECT DISTINCT A.IDAREA ID, A.IDPIANTA,"
            sql = sql & " P.NOME PIA, A.CODICE || ' - ' || A.NOME LABEL"
            sql = sql & " FROM AREA A, AREAPACCHETTO_AREA AP, PIANTA P"
            sql = sql & " WHERE AP.IDAREA(+) = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = P.IDPIANTA"
            sql = sql & " AND AP.IDAREAPACCHETTO = " & recordTemporaneo.idElementoLista
            sql = sql & " ORDER BY PIA, LABEL"
            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idPiantaCorrente = rec("IDPIANTA").Value
                    If idPiantaCorrente <> idPiantaPrecedente Then
                        descrizione = rec("PIA") & ": " & rec("LABEL")
                        campoNome = IIf(campoNome = "", descrizione, campoNome & "; " & descrizione)
                        numeroPiantaAssociate = numeroPiantaAssociate + 1
                    Else
                        campoNome = IIf(campoNome = "", rec("LABEL"), campoNome & ", " & rec("LABEL"))
                    End If
                    idPiantaPrecedente = idPiantaCorrente
                    rec.MoveNext
                Wend
            End If
            rec.Close
        End If
        recordTemporaneo.nomeElementoLista = campoNome
        recordTemporaneo.idAttributoElementoLista = numeroPiantaAssociate 'nota: qui dentro metto il numero di AREE (quindi non č un id)
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioPacchettoArea
        idTariffaArea = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea
        sql = sql & " VALUES (" & idTariffaArea & ", "
        sql = sql & SqlStringValue(elencoNomi) & ", "
        sql = sql & recordTemporaneo.idAttributoElementoLista & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub



