VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmConfigurazioneComunicazione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Comunicazione"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   31
      Top             =   4440
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CheckBox chkVisualizzaSoloSelezionati 
      Caption         =   "visualizza solo i selezionati"
      Height          =   255
      Left            =   4200
      TabIndex        =   13
      Top             =   7200
      Width           =   2415
   End
   Begin VB.CommandButton cmdDeselezionaTutti 
      Caption         =   "Deseleziona tutti"
      Height          =   315
      Left            =   9420
      TabIndex        =   17
      Top             =   7560
      Width           =   2415
   End
   Begin VB.CommandButton cmdSelezionaTutti 
      Caption         =   "Seleziona tutti"
      Height          =   315
      Left            =   7020
      TabIndex        =   16
      Top             =   7560
      Width           =   2415
   End
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca"
      Height          =   315
      Left            =   4200
      TabIndex        =   14
      Top             =   7560
      Width           =   1155
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4200
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   6780
      Width           =   2655
   End
   Begin VB.ListBox lstDestinatari 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2220
      Left            =   7020
      Style           =   1  'Checkbox
      TabIndex        =   15
      Top             =   5340
      Width           =   4815
   End
   Begin VB.OptionButton optOperatori 
      Caption         =   "operatori"
      Height          =   195
      Left            =   4440
      TabIndex        =   11
      Top             =   6240
      Width           =   1455
   End
   Begin VB.OptionButton optProdotti 
      Caption         =   "prodotti"
      Height          =   195
      Left            =   4440
      TabIndex        =   10
      Top             =   6000
      Width           =   1455
   End
   Begin VB.OptionButton optOrganizzazioni 
      Caption         =   "organizzazioni"
      Height          =   195
      Left            =   4440
      TabIndex        =   9
      Top             =   5760
      Width           =   1455
   End
   Begin VB.TextBox txtMinutiScadenza 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2700
      MaxLength       =   2
      TabIndex        =   8
      Top             =   7260
      Width           =   435
   End
   Begin VB.TextBox txtOraScadenza 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      MaxLength       =   2
      TabIndex        =   7
      Top             =   7260
      Width           =   435
   End
   Begin VB.TextBox txtIntestazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   5700
      Width           =   3315
   End
   Begin VB.TextBox txtTesto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   6360
      Width           =   3615
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   21
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   19
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   20
      Top             =   8100
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneComunicazione 
      Height          =   330
      Left            =   240
      Top             =   3660
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneComunicazione 
      Bindings        =   "frmConfigurazioneComunicazione.frx":0000
      Height          =   3495
      Left            =   120
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6165
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDataScadenza 
      Height          =   315
      Left            =   120
      TabIndex        =   6
      Top             =   7260
      Width           =   1875
      _ExtentX        =   3307
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   63307777
      CurrentDate     =   37606
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   33
      Top             =   4200
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   32
      Top             =   4200
      Width           =   1815
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   4200
      TabIndex        =   30
      Top             =   6540
      Width           =   1755
   End
   Begin VB.Line Line4 
      X1              =   4260
      X2              =   4380
      Y1              =   5880
      Y2              =   5880
   End
   Begin VB.Line Line3 
      X1              =   4260
      X2              =   4380
      Y1              =   6120
      Y2              =   6120
   End
   Begin VB.Line Line2 
      X1              =   4260
      X2              =   4380
      Y1              =   6360
      Y2              =   6360
   End
   Begin VB.Line Line1 
      X1              =   4260
      X2              =   4260
      Y1              =   5640
      Y2              =   6360
   End
   Begin VB.Label lblInviaA 
      Caption         =   "invia a:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4140
      TabIndex        =   29
      Top             =   5460
      Width           =   1035
   End
   Begin VB.Label lblData 
      Caption         =   "Data scadenza"
      Height          =   195
      Left            =   120
      TabIndex        =   28
      Top             =   7020
      Width           =   1395
   End
   Begin VB.Label lblOraScadenza 
      Caption         =   "Ora (HH:MM)"
      Height          =   195
      Left            =   2100
      TabIndex        =   27
      Top             =   7020
      Width           =   1035
   End
   Begin VB.Label lblSeparatoreOreMinutiScadenza 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2580
      TabIndex        =   26
      Top             =   7260
      Width           =   75
   End
   Begin VB.Label lblIntestazione 
      Caption         =   "Intestazione"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   5460
      Width           =   1695
   End
   Begin VB.Label lblTesto 
      Caption         =   "Testo"
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   6120
      Width           =   1575
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Comunicazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   23
      Top             =   120
      Width           =   8595
   End
End
Attribute VB_Name = "frmConfigurazioneComunicazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private isRecordEditabile As Boolean
Private intestazioneRecordSelezionato As String
Private codiceRecordSelezionato As String
Private testoRecordSelezionato As String
Private dataOraCreazione As Date
Private dataOraScadenza As Date
Private listaProdottiSelezionati As Collection
Private listaOperatoriSelezionati As Collection
Private listaOrganizzazioniSelezionate As Collection
Private inviaAdOrganizzazioni As Boolean
Private inviaAProdotti As Boolean
Private inviaAdOperatori As Boolean
Private idOrganizzazioneSelezionata As Long
Private visualizzaSoloSelezionati As ValoreBooleanoEnum

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
        
    dgrConfigurazioneComunicazione.Caption = "COMUNICAZIONI CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneComunicazione.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtIntestazione.Text = ""
        txtTesto.Text = ""
        txtIntestazione.Enabled = False
        txtTesto.Enabled = False
        lblIntestazione.Enabled = False
        lblTesto.Enabled = False
        lblInviaA.Enabled = False
        optOrganizzazioni.Enabled = False
        optOperatori.Enabled = False
        optProdotti.Enabled = False
        lblOrganizzazione.Enabled = False
        cmbOrganizzazione.Enabled = False
        Call cmbOrganizzazione.Clear
        chkVisualizzaSoloSelezionati.Value = VB_FALSO
        chkVisualizzaSoloSelezionati.Enabled = False
        lstDestinatari.Enabled = False
        Call lstDestinatari.Clear
        cmdSelezionaTutti.Enabled = False
        cmdDeselezionaTutti.Enabled = False
        cmdCerca.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneComunicazione.Enabled = False
        txtIntestazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtTesto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblIntestazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTesto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblInviaA.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        optOrganizzazioni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        optOperatori.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        optProdotti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblOrganizzazione.Enabled = gestioneRecordGriglia <> ASG_ELIMINA And _
            (inviaAdOperatori Or inviaAProdotti)
        cmbOrganizzazione.Enabled = gestioneRecordGriglia <> ASG_ELIMINA And _
            (inviaAdOperatori Or inviaAProdotti)
        chkVisualizzaSoloSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstDestinatari.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionaTutti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDeselezionaTutti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdCerca.Enabled = gestioneRecordGriglia <> ASG_ELIMINA And _
            (inviaAdOperatori Or inviaAdOrganizzazioni Or inviaAProdotti)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtIntestazione.Text) <> "" And _
                               Trim(txtTesto.Text) <> "")
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneComunicazione.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtIntestazione.Text = ""
        txtTesto.Text = ""
        txtIntestazione.Enabled = False
        txtTesto.Enabled = False
        lblIntestazione.Enabled = False
        lblTesto.Enabled = False
        lblInviaA.Enabled = False
        optOrganizzazioni.Enabled = False
        optOperatori.Enabled = False
        optProdotti.Enabled = False
        lblOrganizzazione.Enabled = False
        cmbOrganizzazione.Enabled = False
        Call cmbOrganizzazione.Clear
        chkVisualizzaSoloSelezionati.Value = VB_FALSO
        chkVisualizzaSoloSelezionati.Enabled = False
        lstDestinatari.Enabled = False
        Call lstDestinatari.Clear
        cmdSelezionaTutti.Enabled = False
        cmdDeselezionaTutti.Enabled = False
        cmdCerca.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Private Sub chkVisualizzaSoloSelezionati_Click()
    If Not internalEvent Then
        Call chkVisualizzaSoloSelezionati_Update
    End If
End Sub

Private Sub chkVisualizzaSoloSelezionati_Update()
    visualizzaSoloSelezionati = (chkVisualizzaSoloSelezionati.Value)
End Sub

Private Sub cmbOrganizzazione_Click()
    If Not internalEvent Then
        idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCerca_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Cerca
    
    MousePointer = mousePointerOld
End Sub

Private Sub Cerca()
    Call CaricaValoriListaDestinatari
End Sub

Private Sub CaricaValoriListaDestinatari()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim destinatario As clsElementoLista
    Dim chiave As String
    Dim i As Integer
    Dim stringaJoinEsterno As String
    
    Call lstDestinatari.Clear
    
    i = 1
    If visualizzaSoloSelezionati = VB_VERO Then
        stringaJoinEsterno = ""
    Else
        stringaJoinEsterno = "(+)"
    End If
    If inviaAdOrganizzazioni Then
        sql = " SELECT O.IDORGANIZZAZIONE ID, O.NOME, CO.IDCOMUNICAZIONE"
        sql = sql & " FROM ORGANIZZAZIONE O, COMUNICAZIONE_ORGANIZZAZIONE CO"
'        sql = sql & " WHERE O.IDORGANIZZAZIONE = CO.IDORGANIZZAZIONE(+)"
'        sql = sql & " AND CO.IDCOMUNICAZIONE(+) = " & idRecordSelezionato
        sql = sql & " WHERE O.IDORGANIZZAZIONE = CO.IDORGANIZZAZIONE" & stringaJoinEsterno
        sql = sql & " AND CO.IDCOMUNICAZIONE" & stringaJoinEsterno & " = " & idRecordSelezionato
        sql = sql & " ORDER BY O.NOME"
    ElseIf inviaAProdotti Then
        sql = " SELECT P.IDPRODOTTO ID, P.NOME, CP.IDCOMUNICAZIONE"
        sql = sql & " FROM PRODOTTO P, COMUNICAZIONE_PRODOTTO CP"
'        sql = sql & " WHERE P.IDPRODOTTO = CP.IDPRODOTTO(+)"
'        sql = sql & " AND CP.IDCOMUNICAZIONE(+) = " & idRecordSelezionato
        sql = sql & " WHERE P.IDPRODOTTO = CP.IDPRODOTTO" & stringaJoinEsterno
        sql = sql & " AND CP.IDCOMUNICAZIONE" & stringaJoinEsterno & " = " & idRecordSelezionato
        If idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
            sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
        End If
        sql = sql & " ORDER BY P.NOME"
    ElseIf inviaAdOperatori Then
        If idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
            sql = " SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME NOME, CO.IDCOMUNICAZIONE"
            sql = sql & " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PRODOTTO P, COMUNICAZIONE_OPERATORE CO"
            sql = sql & " WHERE O.IDOPERATORE = OP.IDOPERATORE"
            sql = sql & " AND OP.IDPRODOTTO = P.IDPRODOTTO"
            sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'            sql = sql & " AND O.IDOPERATORE = CO.IDOPERATORE(+)"
'            sql = sql & " AND CO.IDCOMUNICAZIONE(+) = " & idRecordSelezionato
            sql = sql & " AND O.IDOPERATORE = CO.IDOPERATORE" & stringaJoinEsterno
            sql = sql & " AND CO.IDCOMUNICAZIONE" & stringaJoinEsterno & " = " & idRecordSelezionato
            sql = sql & " ORDER BY O.USERNAME"
        Else
            sql = " SELECT O.IDOPERATORE ID, O.USERNAME NOME, CO.IDCOMUNICAZIONE"
            sql = sql & " FROM OPERATORE O, COMUNICAZIONE_OPERATORE CO"
'            sql = sql & " WHERE O.IDOPERATORE = CO.IDOPERATORE(+)"
'            sql = sql & " AND CO.IDCOMUNICAZIONE(+) = " & idRecordSelezionato
            sql = sql & " WHERE O.IDOPERATORE = CO.IDOPERATORE" & stringaJoinEsterno
            sql = sql & " AND CO.IDCOMUNICAZIONE" & stringaJoinEsterno & " = " & idRecordSelezionato
            sql = sql & " ORDER BY O.USERNAME"
        End If
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set destinatario = New clsElementoLista
            destinatario.nomeElementoLista = rec("NOME")
            destinatario.idElementoLista = rec("ID").Value
            chiave = ChiaveId(destinatario.idElementoLista)
            Call lstDestinatari.AddItem(destinatario.nomeElementoLista)
            lstDestinatari.ItemData(i - 1) = destinatario.idElementoLista
            If inviaAdOperatori Then
                If Not IsNull(rec("IDCOMUNICAZIONE")) Then
'                    Call listaOperatoriSelezionati.Add(destinatario, chiave)
                    Call AggiungiElementoALista(destinatario, listaOperatoriSelezionati)
                    lstDestinatari.Selected(i - 1) = True
                Else
                    Call EliminaElementoDaLista(destinatario, listaOperatoriSelezionati)
                End If
            ElseIf inviaAdOrganizzazioni Then
                If Not IsNull(rec("IDCOMUNICAZIONE")) Then
'                    Call listaOrganizzazioniSelezionate.Add(destinatario, chiave)
                    Call AggiungiElementoALista(destinatario, listaOrganizzazioniSelezionate)
                    lstDestinatari.Selected(i - 1) = True
                Else
                    Call EliminaElementoDaLista(destinatario, listaOrganizzazioniSelezionate)
                End If
            ElseIf inviaAProdotti Then
                If Not IsNull(rec("IDCOMUNICAZIONE")) Then
'                    Call listaProdottiSelezionati.Add(destinatario, chiave)
                    Call AggiungiElementoALista(destinatario, listaProdottiSelezionati)
                    lstDestinatari.Selected(i - 1) = True
                Else
                    Call EliminaElementoDaLista(destinatario, listaProdottiSelezionati)
                End If
            End If
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String

    Set listaOperatoriSelezionati = New Collection
    Set listaOrganizzazioniSelezionate = New Collection
    Set listaProdottiSelezionati = New Collection
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String

    Set listaOperatoriSelezionati = New Collection
    Set listaOrganizzazioniSelezionate = New Collection
    Set listaProdottiSelezionati = New Collection
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Set listaOperatoriSelezionati = New Collection
    Set listaOrganizzazioniSelezionate = New Collection
    Set listaProdottiSelezionati = New Collection
    isRecordEditabile = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    intestazioneRecordSelezionato = ""
    testoRecordSelezionato = ""
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String

    Set listaOperatoriSelezionati = New Collection
    Set listaOrganizzazioniSelezionate = New Collection
    Set listaProdottiSelezionati = New Collection
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneComunicazione_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneComunicazione_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneComunicazione_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneComunicazione.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub adcConfigurazioneComunicazione_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneComunicazione
    
    sql = " SELECT C.IDCOMUNICAZIONE ID, C.DATAORACREAZIONE ""Creata il"","
    sql = sql & "C.INTESTAZIONE ""Intestazione"", C.TESTO ""Testo"", "
    sql = sql & " C.DATAORASCADENZA ""Scade il"""
    sql = sql & " FROM COMUNICAZIONE C"
    sql = sql & " ORDER BY DATAORACREAZIONE DESC"
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovaComunicazione As Long
    Dim n As Long
    Dim destinatario As clsElementoLista
    
    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovaComunicazione = OttieniIdentificatoreDaSequenza("SQ_COMUNICAZIONE")
    sql = " INSERT INTO COMUNICAZIONE (IDCOMUNICAZIONE, "
    sql = sql & " INTESTAZIONE, TESTO, DATAORACREAZIONE, DATAORASCADENZA) "
    sql = sql & " VALUES (" & idNuovaComunicazione & ", "
    sql = sql & SqlStringValue(intestazioneRecordSelezionato) & ", "
    sql = sql & SqlStringValue(testoRecordSelezionato) & ", "
    sql = sql & "SYSDATE, "
    sql = sql & IIf(dataOraScadenza = dataNulla, "NULL", SqlDateTimeValue(dataOraScadenza))
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText

'   INSERIMENTO IN TABELLA COMUNICAZIONE_ORGANIZZAZIONE
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        For Each destinatario In listaOrganizzazioniSelezionate
            sql = "INSERT INTO COMUNICAZIONE_ORGANIZZAZIONE (IDCOMUNICAZIONE, IDORGANIZZAZIONE)"
            sql = sql & " VALUES (" & idNuovaComunicazione & ", "
            sql = sql & destinatario.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
            sql = " UPDATE OPERATORE"
            sql = sql & " SET COMUNICAZIONIPRESENTI = " & VB_VERO
            sql = sql & " WHERE IDOPERATORE IN"
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT O.IDOPERATORE"
            sql = sql & " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PRODOTTO P"
            sql = sql & " WHERE O.IDOPERATORE = OP.IDOPERATORE"
            sql = sql & " AND OP.IDPRODOTTO = P.IDPRODOTTO"
            sql = sql & " AND P.IDORGANIZZAZIONE = " & destinatario.idElementoLista
            sql = sql & " )"
            SETAConnection.Execute sql, n, adCmdText
        Next destinatario
    End If

'   INSERIMENTO IN TABELLA COMUNICAZIONE_PRODOTTO
    If Not (listaProdottiSelezionati Is Nothing) Then
        For Each destinatario In listaProdottiSelezionati
            sql = "INSERT INTO COMUNICAZIONE_PRODOTTO (IDCOMUNICAZIONE, IDPRODOTTO)"
            sql = sql & " VALUES (" & idNuovaComunicazione & ", "
            sql = sql & destinatario.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
            sql = " UPDATE OPERATORE"
            sql = sql & " SET COMUNICAZIONIPRESENTI = " & VB_VERO
            sql = sql & " WHERE IDOPERATORE IN"
            sql = sql & " ("
            sql = sql & " SELECT DISTINCT O.IDOPERATORE"
            sql = sql & " FROM OPERATORE O, OPERATORE_PRODOTTO OP"
            sql = sql & " WHERE O.IDOPERATORE = OP.IDOPERATORE"
            sql = sql & " AND OP.IDPRODOTTO = " & destinatario.idElementoLista
            sql = sql & " )"
            SETAConnection.Execute sql, n, adCmdText
        Next destinatario
    End If

'   INSERIMENTO IN TABELLA COMUNICAZIONE_OPERATORE
    If Not (listaOperatoriSelezionati Is Nothing) Then
        For Each destinatario In listaOperatoriSelezionati
            sql = "INSERT INTO COMUNICAZIONE_OPERATORE (IDCOMUNICAZIONE, IDOPERATORE)"
            sql = sql & " VALUES (" & idNuovaComunicazione & ", "
            sql = sql & destinatario.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
            sql = " UPDATE OPERATORE "
            sql = sql & " SET COMUNICAZIONIPRESENTI = " & VB_VERO
            sql = sql & " WHERE IDOPERATORE = " & destinatario.idElementoLista
            SETAConnection.Execute sql, n, adCmdText
        Next destinatario
    End If
    
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD

    Call SetIdRecordSelezionato(idNuovaComunicazione)
    Call AggiornaAbilitazioneControlli
    
Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    isRecordEditabile = True
    sql = "SELECT INTESTAZIONE, TESTO, "
    sql = sql & " DATAORACREAZIONE, DATAORASCADENZA"
    sql = sql & " FROM COMUNICAZIONE"
    sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        intestazioneRecordSelezionato = rec("INTESTAZIONE")
        testoRecordSelezionato = rec("TESTO")
        dataOraCreazione = rec("DATAORACREAZIONE")
        dataOraScadenza = IIf(IsNull(rec("DATAORASCADENZA")), dataNulla, rec("DATAORASCADENZA"))
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtTesto.Text = testoRecordSelezionato
    txtIntestazione.Text = intestazioneRecordSelezionato
'    dtpDataScadenza.Value = dataOraScadenza
    dtpDataScadenza.Value = IIf(dataOraScadenza = dataNulla, "", dataOraScadenza)
    txtOraScadenza.Text = IIf(dataOraScadenza = dataNulla, "", StringaOraMinuti(CStr(Hour(dataOraScadenza))))
    txtMinutiScadenza.Text = IIf(dataOraScadenza = dataNulla, "", StringaOraMinuti(CStr(Minute(dataOraScadenza))))

    internalEvent = internalEventOld

End Sub

Private Sub dgrConfigurazioneComunicazione_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneComunicazione
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneComunicazione.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String
    Dim ora As String
    Dim minuti As String
    Dim oreDurata As String
    Dim minutiDurata As String
    Dim secondi As String
    Dim dataScadenza As Date

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
'    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
'        condizioneSql = "IDLAYOUTSUPPORTO <> " & idRecordSelezionato
'    End If

    testoRecordSelezionato = Trim(txtTesto.Text)
'    If ViolataUnicitā("LAYOUTSUPPORTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "", _
'        condizioneSql, "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
'            " č giā presente in DB;")
'    End If
    intestazioneRecordSelezionato = Trim(txtIntestazione.Text)
'    If ViolataUnicitā("LAYOUTSUPPORTO", "CODICE = " & SqlStringValue(codiceRecordSelezionato), "", _
'        condizioneSql, "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore codice = " & SqlStringValue(codiceRecordSelezionato) & _
'            " č giā presente in DB;")
'    End If
'    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)

    secondi = "00"
    dataScadenza = FormatDateTime(dtpDataScadenza.Value, vbShortDate)
    Set listaNonConformitā = New Collection
    
    If IsCampoOraCorretto(txtOraScadenza) Then
        ora = CStr(Trim(txtOraScadenza.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Scadenza ore deve essere numerico di tipo intero e compreso tra 0 e 23;")
    End If
    If IsCampoMinutiCorretto(txtMinutiScadenza) Then
        minuti = CStr(Trim(txtMinutiScadenza.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Scadenza minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    
    dataOraScadenza = FormatDateTime(dataScadenza & " " & ora & ":" & minuti & ":" & secondi, vbGeneralDate)

    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub txtTesto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaDallaBaseDati()
    Dim destinatarioEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim sql As String
    Dim n As Long
    
    Set listaTabelleCorrelate = New Collection

    destinatarioEliminabile = True
'    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("ORGANIZ_TIPOSUP_LAYOUTSUP")
'        layoutEliminabile = False
'    End If
'    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "UTILIZZOLAYOUTSUPPORTO", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
'        layoutEliminabile = False
'    End If
'    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "STATOTITOLO", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("STATOTITOLO")
'        layoutEliminabile = False
'    End If

On Error GoTo gestioneErrori

    Call ApriConnessioneBD

    If destinatarioEliminabile Then
        sql = "DELETE FROM COMUNICAZIONE_ORGANIZZAZIONE"
        sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM COMUNICAZIONE_PRODOTTO"
        sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM COMUNICAZIONE_OPERATORE"
        sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM COMUNICAZIONE WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Comunicazione selezionata", _
                                        tabelleCorrelate)
    End If

    Call ChiudiConnessioneBD

    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim destinatario As clsElementoLista
    Dim condizioneSql As String

    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE COMUNICAZIONE SET"
    sql = sql & " INTESTAZIONE = " & SqlStringValue(intestazioneRecordSelezionato) & ","
    sql = sql & " TESTO = " & SqlStringValue(testoRecordSelezionato) & ","
    sql = sql & " DATAORASCADENZA = " & IIf(dataOraScadenza = dataNulla, "NULL", SqlDateTimeValue(dataOraScadenza))
    sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText

'   BISOGNEREBBE ANCHE TOGLIERE IL FLAG PER QUEGLI OPERATORI NON PIU' REFERENZIATI

'   AGGIORNAMENTO TABELLA COMUNICAZIONE_ORGANIZZAZIONE
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        sql = "DELETE FROM COMUNICAZIONE_ORGANIZZAZIONE"
        sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each destinatario In listaOrganizzazioniSelezionate
            sql = "INSERT INTO COMUNICAZIONE_ORGANIZZAZIONE (IDCOMUNICAZIONE, IDORGANIZZAZIONE)"
            sql = sql & " VALUES (" & idRecordSelezionato & ", "
            sql = sql & destinatario.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next destinatario
    End If

'   AGGIORNAMENTO TABELLA COMUNICAZIONE_PRODOTTO
    If Not (listaProdottiSelezionati Is Nothing) Then
        sql = "DELETE FROM COMUNICAZIONE_PRODOTTO"
        sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each destinatario In listaProdottiSelezionati
            sql = "INSERT INTO COMUNICAZIONE_PRODOTTO (IDCOMUNICAZIONE, IDPRODOTTO)"
            sql = sql & " VALUES (" & idRecordSelezionato & ", "
            sql = sql & destinatario.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next destinatario
    End If

'   AGGIORNAMENTO TABELLA COMUNICAZIONE_OPERATORE
    If Not (listaOperatoriSelezionati Is Nothing) Then
        sql = "DELETE FROM COMUNICAZIONE_OPERATORE"
        sql = sql & " WHERE IDCOMUNICAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each destinatario In listaOperatoriSelezionati
            sql = "INSERT INTO COMUNICAZIONE_OPERATORE (IDCOMUNICAZIONE, IDOPERATORE)"
            sql = sql & " VALUES (" & idRecordSelezionato & ", "
            sql = sql & destinatario.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next destinatario
    End If
    
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD
    
Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    stringaNota = "IDCOMUNICAZIONE = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli

    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_COMUNICAZIONE, CCDA_COMUNICAZIONE, stringaNota, idRecordSelezionato)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneComunicazione_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneComunicazione_Init
    Else
        If ValoriCampiOK Then
            If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_COMUNICAZIONE, CCDA_COMUNICAZIONE, stringaNota, idRecordSelezionato)
            ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_COMUNICAZIONE, CCDA_COMUNICAZIONE, stringaNota, idRecordSelezionato)
            End If
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazioneComunicazione_Init
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
            Call dgrConfigurazioneComunicazione_Init
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optOrganizzazioni_Click()
    If Not internalEvent Then
        Call optionButtons_Update
    End If
End Sub

Private Sub optOperatori_Click()
    If Not internalEvent Then
        Call optionButtons_Update
    End If
End Sub

Private Sub optProdotti_Click()
    If Not internalEvent Then
        Call optionButtons_Update
    End If
End Sub

Private Sub optionButtons_Update()
    Dim sql As String
    
    inviaAdOperatori = (optOperatori.Value)
    inviaAdOrganizzazioni = (optOrganizzazioni.Value)
    inviaAProdotti = (optProdotti.Value)
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    If inviaAdOperatori Or inviaAProdotti Then
        sql = " SELECT O.IDORGANIZZAZIONE ID, O.NOME"
        sql = sql & " FROM ORGANIZZAZIONE O"
        sql = sql & " ORDER BY O.NOME"
        Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
        Call SelezionaElementoSuCombo(cmbOrganizzazione, idOrganizzazioneSelezionata)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaDeselezionaDestinatario()
    Dim idDestinatario As Long
    Dim destinatario As clsElementoLista
    
On Error Resume Next

    idDestinatario = lstDestinatari.ItemData(lstDestinatari.ListIndex)
    If lstDestinatari.Selected(lstDestinatari.ListIndex) Then
        Set destinatario = New clsElementoLista
        destinatario.idElementoLista = idDestinatario
        destinatario.nomeElementoLista = lstDestinatari.Text
        If inviaAdOrganizzazioni Then
'            Call listaOrganizzazioniSelezionate.Add(destinatario, ChiaveId(destinatario.idElementoLista))
            Call AggiungiElementoALista(destinatario, listaOrganizzazioniSelezionate)
        ElseIf inviaAProdotti Then
'            Call listaProdottiSelezionati.Add(destinatario, ChiaveId(destinatario.idElementoLista))
            Call AggiungiElementoALista(destinatario, listaProdottiSelezionati)
        ElseIf inviaAdOperatori Then
'            Call listaOperatoriSelezionati.Add(destinatario, ChiaveId(destinatario.idElementoLista))
            Call AggiungiElementoALista(destinatario, listaOperatoriSelezionati)
        End If
    Else
        If inviaAdOrganizzazioni Then
'            Call listaOrganizzazioniSelezionate.Remove(ChiaveId(destinatario.idElementoLista))
            Call EliminaElementoDaLista(destinatario, listaOrganizzazioniSelezionate)
        ElseIf inviaAProdotti Then
'            Call listaProdottiSelezionati.Remove(ChiaveId(destinatario.idElementoLista))
            Call EliminaElementoDaLista(destinatario, listaProdottiSelezionati)
        ElseIf inviaAdOperatori Then
'            Call listaOperatoriSelezionati.Remove(ChiaveId(destinatario.idElementoLista))
            Call EliminaElementoDaLista(destinatario, listaOperatoriSelezionati)
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Variabili_Init()

End Sub

Private Sub lstDestinatari_Click()
    Call VisualizzaListBoxToolTip(lstDestinatari, "ID = " & lstDestinatari.ItemData(lstDestinatari.ListIndex) & "; " & lstDestinatari.Text)
End Sub

Private Sub lstDestinatari_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaDestinatario
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaTuttiDestinatari()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idDestinatario As Long
    Dim destinatario As clsElementoLista
'    Dim listaDestinatariSelezionati As Collection
    
    internalEventOld = internalEvent
    internalEvent = True
    
'    Set dirittiOperatore.listaProdottiSelezionati = Nothing
'    Set dirittiOperatore.listaProdottiSelezionati = New Collection
    If inviaAdOrganizzazioni Then
        Set listaOrganizzazioniSelezionate = Nothing
        Set listaOrganizzazioniSelezionate = New Collection
    ElseIf inviaAProdotti Then
        Set listaProdottiSelezionati = Nothing
        Set listaProdottiSelezionati = New Collection
    ElseIf inviaAdOperatori Then
        Set listaOperatoriSelezionati = Nothing
        Set listaOperatoriSelezionati = New Collection
    End If
    For i = 1 To lstDestinatari.ListCount
        Set destinatario = New clsElementoLista
        idDestinatario = lstDestinatari.ItemData(i - 1)
        destinatario.idElementoLista = idDestinatario
        destinatario.nomeElementoLista = lstDestinatari.Text
        If inviaAdOrganizzazioni Then
            Call listaOrganizzazioniSelezionate.Add(destinatario, ChiaveId(idDestinatario))
        ElseIf inviaAProdotti Then
            Call listaProdottiSelezionati.Add(destinatario, ChiaveId(idDestinatario))
        ElseIf inviaAdOperatori Then
            Call listaOperatoriSelezionati.Add(destinatario, ChiaveId(idDestinatario))
        End If
'        Call dirittiOperatore.listaProdottiSelezionati.Add(prodotto, ChiaveId(idProdotto))
        lstDestinatari.Selected(i - 1) = True
    Next i
    
    internalEvent = internalEventOld
End Sub

Private Sub DeselezionaTuttiDestinatari()
    Dim i As Integer
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstDestinatari.ListCount
        lstDestinatari.Selected(i - 1) = False
    Next i
    If inviaAdOrganizzazioni Then
        Set listaOrganizzazioniSelezionate = Nothing
        Set listaOrganizzazioniSelezionate = New Collection
    ElseIf inviaAProdotti Then
        Set listaProdottiSelezionati = Nothing
        Set listaProdottiSelezionati = New Collection
    ElseIf inviaAdOperatori Then
        Set listaOperatoriSelezionati = Nothing
        Set listaOperatoriSelezionati = New Collection
    End If
    
    internalEvent = internalEventOld
End Sub

Private Sub cmdSelezionaTutti_Click()
    Call SelezionaTuttiDestinatari
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdDeselezionaTutti_Click()
    Call DeselezionaTuttiDestinatari
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraScadenza_Change()
    If Not internalEvent Then
        If Len(txtOraScadenza.Text) = 2 Then
            txtMinutiScadenza.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiScadenza_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub



