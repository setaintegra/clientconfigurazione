VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneProdottoScelteRappresentazione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca"
      Height          =   315
      Left            =   10980
      TabIndex        =   7
      Top             =   5580
      Width           =   795
   End
   Begin VB.ComboBox cmbTurnoRappresentazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7200
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   5580
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6300
      Width           =   435
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   23
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   22
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   21
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   20
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   16
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   19
      Top             =   3660
      Width           =   4035
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3540
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   4920
      Width           =   3555
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   3
      Top             =   4920
      Width           =   3315
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   6240
      MultiSelect     =   2  'Extended
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   6240
      Width           =   5595
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   6600
      Width           =   435
   End
   Begin VB.CommandButton cmdDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   6900
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   7200
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   6240
      Width           =   5595
   End
   Begin VB.ComboBox cmbSpettacolo 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7200
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   4920
      Width           =   3555
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoScelteRappresentazione 
      Height          =   330
      Left            =   6360
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoScelteRappresentazione 
      Height          =   2715
      Left            =   120
      TabIndex        =   24
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   4789
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblAttenzione 
      Caption         =   "Attenzione! Tutti i controlli sulla associabilitā sono disabilitati"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   495
      Left            =   120
      TabIndex        =   36
      Top             =   5400
      Width           =   2775
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000005&
      X1              =   10860
      X2              =   10860
      Y1              =   5040
      Y2              =   5700
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000005&
      X1              =   10740
      X2              =   10980
      Y1              =   5700
      Y2              =   5700
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000005&
      X1              =   10740
      X2              =   10860
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Label lblTurnoRappresentazione 
      Caption         =   "Turno rappresentazione"
      Height          =   255
      Left            =   7200
      TabIndex        =   35
      Top             =   5340
      Width           =   2055
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Scelte Rappresentazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   34
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   33
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   32
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   31
      Top             =   3420
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   30
      Top             =   3420
      Width           =   1815
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   3540
      TabIndex        =   29
      Top             =   4680
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   4680
      Width           =   1695
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Rappresentazioni disponibili"
      Height          =   195
      Left            =   120
      TabIndex        =   27
      Top             =   6000
      Width           =   5595
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Rappresentazioni selezionate"
      Height          =   195
      Left            =   6240
      TabIndex        =   26
      Top             =   6000
      Width           =   5595
   End
   Begin VB.Label lblSpettacolo 
      Caption         =   "Spettacolo"
      Height          =   255
      Left            =   7200
      TabIndex        =   25
      Top             =   4680
      Width           =   1515
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoScelteRappresentazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private isRecordEditabile As Boolean
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private rateo As Long
Private idSpettacoloSelezionato As Long
Private idTurnoRappresentazioneSelezionato As Long
Private listaRappresentazioniDisponibili As Collection
Private listaRappresentazioniSelezionate As Collection
Private isProdottoAttivoSuTL As Boolean
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private nomeTabellaTemporanea As String
Private listaAppoggioRappresentazione As Collection
Private Const ID_NESSUN_TURNO As Long = -3

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    dgrConfigurazioneProdottoScelteRappresentazione.Caption = "SCELTE RAPPRESENTAZIONE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoScelteRappresentazione.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbSpettacolo.Clear
        Call cmbTurnoRappresentazione.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        cmbSpettacolo.Enabled = False
        cmbTurnoRappresentazione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblSpettacolo.Enabled = False
        lblTurnoRappresentazione.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDisponibile.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdCerca.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        lblAttenzione.Visible = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoScelteRappresentazione.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbSpettacolo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia <> ASG_MODIFICA)
        cmbTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSpettacolo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia <> ASG_MODIFICA)
        lblTurnoRappresentazione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDisponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdCerca.Enabled = (idSpettacoloSelezionato <> idNessunElementoSelezionato And _
            idTurnoRappresentazioneSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = Trim(txtNome.Text) <> "" And _
            lstSelezionati.ListCount > 0
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        lblAttenzione.Visible = False
        lblAttenzione.Visible = (gestioneRecordGriglia <> ASG_ELIMINA)
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoScelteRappresentazione.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbSpettacolo.Clear
        Call cmbTurnoRappresentazione.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        cmbSpettacolo.Enabled = False
        cmbTurnoRappresentazione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblSpettacolo.Enabled = False
        lblTurnoRappresentazione.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDisponibile.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdCerca.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        lblAttenzione.Visible = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Call EliminaTabellaAppoggioRappresentazioni
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call EliminaTabellaAppoggioRappresentazioni
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub
'
'Private Sub CaricaFormConfigurazioneTitoliDiServizio()
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetIdProdottoSelezionato(idProdottoSelezionato)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetIdPiantaSelezionata(idPiantaSelezionata)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazioneProdottoTitoliDiServizio.SetModalitāForm(A_NUOVO)
'    Call frmConfigurazioneProdottoTitoliDiServizio.Init
'End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
'    Dim numero As Long
    
    Call CaricaFormConfigurazioneCapienzeTurnoLibero
    Call EliminaTabellaAppoggioRappresentazioni
End Sub

Private Sub CaricaFormConfigurazioneCapienzeTurnoLibero()
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetRateo(rateo)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.Init
End Sub

Private Sub cmbSpettacolo_Click()
    If Not internalEvent Then
        idSpettacoloSelezionato = cmbSpettacolo.ItemData(cmbSpettacolo.ListIndex)
        txtNome = cmbSpettacolo.Text
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbTurnoRappresentazione_Click()
    If Not internalEvent Then
        idTurnoRappresentazioneSelezionato = cmbTurnoRappresentazione.ItemData(cmbTurnoRappresentazione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCerca_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If Not internalEvent Then
        Call cerca
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub cerca()
    Call CaricaValoriDisponibili
    Call CaricaValoriSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
    
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If valoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoScelteRappresentazione_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoScelteRappresentazione_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoScelteRappresentazione_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoScelteRappresentazione_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoScelteRappresentazione_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoScelteRappresentazione_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati(idRecordSelezionato)
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_SCELTA_RAPPRESENTAZIONE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoScelteRappresentazione_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneProdottoScelteRappresentazione_Init
                    End Select
                End If
               
                Call AggiornaAbilitazioneControlli
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sqlSpettacolo As String
    Dim sqlTurnoRappresentazione As String
    
    sqlTurnoRappresentazione = "SELECT TURNORAPPRESENTAZIONE.IDTURNORAPPRESENTAZIONE ID," & _
        " TURNORAPPRESENTAZIONE.NOME FROM TURNORAPPRESENTAZIONE" & _
        " ORDER BY NOME"
'    sqlSpettacolo = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO ID, SPETTACOLO.NOME FROM" & _
'        " SPETTACOLO, RAPPRESENTAZIONE, VENUE_PIANTA, PIANTA WHERE" & _
'        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
'        " (VENUE_PIANTA.IDVENUE = RAPPRESENTAZIONE.IDVENUE) AND" & _
'        " (RAPPRESENTAZIONE.DATAORAINIZIO > SYSDATE) AND" & _
'        " (VENUE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
'        " (PIANTA.IDPIANTA = " & idPiantaSelezionata & ") AND" & _
'        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
'        " ORDER BY NOME"
    sqlSpettacolo = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO ID, SPETTACOLO.NOME FROM" & _
        " SPETTACOLO, RAPPRESENTAZIONE WHERE" & _
        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
        " (RAPPRESENTAZIONE.DATAORAINIZIO > SYSDATE) AND" & _
        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbSpettacolo, sqlSpettacolo, "NOME", False)
    Call CaricaValoriCombo(cmbTurnoRappresentazione, sqlTurnoRappresentazione, "NOME", True)
    Call AssegnaValoriCampi
    Call CaricaValoriDisponibili
    Call CaricaValoriSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Call EliminaTabellaAppoggioRappresentazioni
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim SQLSpettacoli As String
    Dim SQLTurni As String
    
    isRecordEditabile = True
    SQLTurni = "SELECT TURNORAPPRESENTAZIONE.IDTURNORAPPRESENTAZIONE AS ""ID""," & _
        " TURNORAPPRESENTAZIONE.NOME FROM TURNORAPPRESENTAZIONE" & _
        " ORDER BY NOME"
'    SQLSpettacoli = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO AS ""ID"", SPETTACOLO.NOME FROM" & _
'        " SPETTACOLO, RAPPRESENTAZIONE, VENUE_PIANTA, PIANTA WHERE" & _
'        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
'        " (RAPPRESENTAZIONE.DATAORAINIZIO > SYSDATE) AND" & _
'        " (VENUE_PIANTA.IDVENUE = RAPPRESENTAZIONE.IDVENUE) AND" & _
'        " (VENUE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
'        " (PIANTA.IDPIANTA = " & idPiantaSelezionata & ") AND" & _
'        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
'        " ORDER BY NOME"
    SQLSpettacoli = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO ID, SPETTACOLO.NOME FROM" & _
        " SPETTACOLO, RAPPRESENTAZIONE WHERE" & _
        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
        " (RAPPRESENTAZIONE.DATAORAINIZIO > SYSDATE) AND" & _
        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbSpettacolo, SQLSpettacoli, "NOME", False)
    Call CaricaValoriCombo(cmbTurnoRappresentazione, SQLTurni, "NOME", True)
'    Call CaricaValoriDisponibili
'    Call CaricaValoriSelezionati
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoScelteRappresentazione.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim SQLSpettacoli As String
    Dim SQLTurni As String
    
    SQLTurni = "SELECT TURNORAPPRESENTAZIONE.IDTURNORAPPRESENTAZIONE AS ""ID""," & _
        " TURNORAPPRESENTAZIONE.NOME FROM TURNORAPPRESENTAZIONE" & _
        " ORDER BY NOME"
'    SQLSpettacoli = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO AS ""ID"", SPETTACOLO.NOME FROM" & _
'        " SPETTACOLO, RAPPRESENTAZIONE, VENUE_PIANTA, PIANTA WHERE" & _
'        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
'        " (RAPPRESENTAZIONE.DATAORAINIZIO > SYSDATE) AND" & _
'        " (VENUE_PIANTA.IDVENUE = RAPPRESENTAZIONE.IDVENUE) AND" & _
'        " (VENUE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
'        " (PIANTA.IDPIANTA = " & idPiantaSelezionata & ") AND" & _
'        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
'        " ORDER BY NOME"
    SQLSpettacoli = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO ID, SPETTACOLO.NOME FROM" & _
        " SPETTACOLO, RAPPRESENTAZIONE WHERE" & _
        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
        " (RAPPRESENTAZIONE.DATAORAINIZIO > SYSDATE) AND" & _
        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbSpettacolo, SQLSpettacoli, "NOME", False)
    Call CaricaValoriCombo(cmbTurnoRappresentazione, SQLTurni, "NOME", True)
    Call AssegnaValoriCampi
    Call CaricaValoriDisponibili
    Call CaricaValoriSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneProdottoScelteRappresentazione_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    idSpettacoloSelezionato = idNessunElementoSelezionato
    idTurnoRappresentazioneSelezionato = idNessunElementoSelezionato
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioRappresentazioni
    Call adcConfigurazioneProdottoScelteRappresentazione_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoScelteRappresentazione_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoScelteRappresentazione.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoScelteRappresentazione_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Call PopolaTabellaAppoggioRappresentazioni

    Set d = adcConfigurazioneProdottoScelteRappresentazione
        
    sql = "SELECT S.IDSCELTARAPPRESENTAZIONE ID,"
    sql = sql & " S.NOME ""Nome"", S.DESCRIZIONE ""Descrizione"","
    sql = sql & " TMP.NOMESPETTACOLO ""Spettacolo"", TMP.NOME ""Rappresentazioni"""
    sql = sql & " FROM SCELTARAPPRESENTAZIONE S, " & nomeTabellaTemporanea & " TMP"
    sql = sql & " WHERE S.IDSCELTARAPPRESENTAZIONE = TMP.IDSCELTARAPPRESENTAZIONE"
    sql = sql & " AND S.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql & " ORDER BY ""Nome"", ""Spettacolo"", ""Rappresentazioni"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneProdottoScelteRappresentazione.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim max As Long
    Dim i As Integer
    Dim idNuovaSeltaRappresentazione As Long
    Dim n As Long
    Dim rappresentazione As clsElementoLista
    Dim condizioniSQL As String
    Dim listaRapprNonAssociabili As Collection
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Set listaRapprNonAssociabili = New Collection
    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA SCELTARAPPRESENTAZIONE
    idNuovaSeltaRappresentazione = OttieniIdentificatoreDaSequenza("SQ_SCELTARAPPRESENTAZIONE")
    sql = "INSERT INTO SCELTARAPPRESENTAZIONE (IDSCELTARAPPRESENTAZIONE, NOME, DESCRIZIONE," & _
        " IDPRODOTTO)" & _
        " VALUES (" & _
        idNuovaSeltaRappresentazione & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        idProdottoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE", "IDSCELTARAPPRESENTAZIONE", idNuovaSeltaRappresentazione, TSR_NUOVO)
'    End If

'   INSERIMENTO IN TABELLA SCELTARAPPRESENTAZIONE_RAPPR
    If Not (listaRappresentazioniSelezionate Is Nothing) Then
        For Each rappresentazione In listaRappresentazioniSelezionate
            If ProdottiCorrelandiBloccati(rappresentazione.idElementoLista, idProdottoSelezionato) Then
                sql = "INSERT INTO SCELTARAPPRESENTAZIONE_RAPPR" & _
                    " (IDSCELTARAPPRESENTAZIONE, IDRAPPRESENTAZIONE)" & _
                    " VALUES " & _
                    "(" & idNuovaSeltaRappresentazione & ", " & _
                    rappresentazione.idElementoLista & ")"
                SETAConnection.Execute sql, n, adCmdText
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    condizioniSQL = " AND IDRAPPRESENTAZIONE = " & rappresentazione.idElementoLista
'                    tipoStatoRecordSelezionato = TSR_NUOVO
'                    Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idNuovaSeltaRappresentazione, TSR_NUOVO, condizioniSQL)
'                End If
            Else
                Call listaRapprNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
                Call SETAConnection.RollbackTrans
            End If
            Call SbloccaProdottiCorrelandi(getNomeMacchina, idProdottoSelezionato)
        Next rappresentazione
    End If
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idNuovaSeltaRappresentazione, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaSeltaRappresentazione)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT S.NOME NOME, S.DESCRIZIONE DESCR, R.IDSPETTACOLO IDS," & _
'            " S.IDSESSIONECONFIGURAZIONE, S.IDTIPOSTATORECORD" & _
'            " FROM SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR, RAPPRESENTAZIONE R" & _
'            " WHERE S.IDSCELTARAPPRESENTAZIONE = SR.IDSCELTARAPPRESENTAZIONE" & _
'            " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
'            " AND S.IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
'    Else
        sql = "SELECT S.NOME NOME, S.DESCRIZIONE DESCR, R.IDSPETTACOLO IDS" & _
            " FROM SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR, RAPPRESENTAZIONE R" & _
            " WHERE S.IDSCELTARAPPRESENTAZIONE = SR.IDSCELTARAPPRESENTAZIONE" & _
            " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND S.IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCR")), "", rec("DESCR"))
        idSpettacoloSelezionato = rec("IDS")
        idTurnoRappresentazioneSelezionato = idNessunElementoSelezionato
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    Call SelezionaElementoSuCombo(cmbSpettacolo, idSpettacoloSelezionato)
    Call SelezionaElementoSuCombo(cmbTurnoRappresentazione, idTurnoRappresentazioneSelezionato)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim rappresentazione As clsElementoLista
    Dim condizioniSQL As String
    Dim listaRapprNonAssociabili As Collection
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    Set listaRapprNonAssociabili = New Collection
    SETAConnection.BeginTrans
'   AGGIORNAMENTO TABELLA SCELTARAPPRESENTAZIONE
    sql = "UPDATE SCELTARAPPRESENTAZIONE SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " IDPRODOTTO = " & idProdottoSelezionato & _
        " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE", "IDSCELTARAPPRESENTAZIONE", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    
'   AGGIORNAMENTO TABELLA SCELTARAPPRESENTAZIONE_RAPPR
    If Not (listaRappresentazioniSelezionate Is Nothing) Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idRecordSelezionato, TSR_ELIMINATO)
'        Else
            sql = "DELETE FROM SCELTARAPPRESENTAZIONE_RAPPR" & _
                " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
        For Each rappresentazione In listaRappresentazioniSelezionate
            If ProdottiCorrelandiBloccati(rappresentazione.idElementoLista, idProdottoSelezionato) Then
'                If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                    sql = "INSERT INTO SCELTARAPPRESENTAZIONE_RAPPR" & _
'                        " (IDSCELTARAPPRESENTAZIONE, IDRAPPRESENTAZIONE)" & _
'                        " (SELECT " & _
'                        idRecordSelezionato & " IDSCELTARAPPRESENTAZIONE, " & _
'                        rappresentazione.idElementoLista & " IDRAPPRESENTAZIONE" & _
'                        " FROM DUAL" & _
'                        " MINUS" & _
'                        " SELECT IDSCELTARAPPRESENTAZIONE, IDRAPPRESENTAZIONE" & _
'                        " FROM SCELTARAPPRESENTAZIONE_RAPPR" & _
'                        " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato & _
'                        " AND IDRAPPRESENTAZIONE = " & rappresentazione.idElementoLista & ")"
'                    SETAConnection.Execute sql, n, adCmdText
'                    condizioniSQL = " AND IDRAPPRESENTAZIONE = " & rappresentazione.idElementoLista
'                    Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'                Else
                    sql = "INSERT INTO SCELTARAPPRESENTAZIONE_RAPPR" & _
                        " (IDSCELTARAPPRESENTAZIONE, IDRAPPRESENTAZIONE)" & _
                        " VALUES " & _
                        "(" & idRecordSelezionato & ", " & _
                        rappresentazione.idElementoLista & ")"
                    SETAConnection.Execute sql, n, adCmdText
'                End If
            Else
                Call listaRapprNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
                Call SETAConnection.RollbackTrans
            End If
            Call SbloccaProdottiCorrelandi(getNomeMacchina, idProdottoSelezionato)
        Next rappresentazione
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazioneProdottoScelteRappresentazione_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoScelteRappresentazione
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDSCELTARAPPRESENTAZIONE <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("SCELTARAPPRESENTAZIONE", "NOME = " & SqlStringValue(nomeRecordSelezionato), " IDPRODOTTO = " & idProdottoSelezionato, _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB per lo stesso Prodotto;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoSistemiDiEmissione.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoSistemiDiEmissione.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, caricaValoriEstesi As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    
    If caricaValoriEstesi Then
        cmb.AddItem "<tutti>"
        cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
        cmb.AddItem "<nessuno>"
'        cmb.ItemData(i) = idNessunElementoSelezionato
        cmb.ItemData(i) = ID_NESSUN_TURNO
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati(idTar As Long)
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM SCELTARAPPRESENTAZIONE_RAPPR" & _
'                " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM SCELTARAPPRESENTAZIONE" & _
'                " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE_RAPPR", "IDSCELTARAPPRESENTAZIONE", idRecordSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("SCELTARAPPRESENTAZIONE", "IDSCELTARAPPRESENTAZIONE", idRecordSelezionato, TSR_ELIMINATO)
'        End If
'    Else
        sql = "DELETE FROM SCELTARAPPRESENTAZIONE_RAPPR" & _
            " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM SCELTARAPPRESENTAZIONE" & _
            " WHERE IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdDisponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstSelezionati
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idRappresentazione As Long
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idRappresentazione = lstSelezionati.ItemData(i - 1)
            chiaveRappresentazione = ChiaveId(idRappresentazione)
            Set rappresentazione = listaRappresentazioniSelezionate.Item(chiaveRappresentazione)
            Call listaRappresentazioniDisponibili.Add(rappresentazione, chiaveRappresentazione)
            Call listaRappresentazioniSelezionate.Remove(chiaveRappresentazione)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim rappresentazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaRappresentazioniDisponibili Is Nothing) Then
        i = 1
        For Each rappresentazione In listaRappresentazioniDisponibili
            lstDisponibili.AddItem rappresentazione.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = rappresentazione.idElementoLista
            i = i + 1
        Next rappresentazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim rappresentazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaRappresentazioniSelezionate Is Nothing) Then
        i = 1
        For Each rappresentazione In listaRappresentazioniSelezionate
            lstSelezionati.AddItem rappresentazione.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = rappresentazione.idElementoLista
            i = i + 1
        Next rappresentazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeTipoTerminale As String
    Dim codiceTastoTipoTerminale As String
    Dim idTipoTerminale As Long
    Dim idTasto As Long
    Dim chiaveRappresentazione As String
    Dim rappresentazione As clsElementoLista
    
    Call lstSelezionati.Clear
    
    Call ApriConnessioneBD

    Set listaRappresentazioniSelezionate = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT DISTINCT R.IDRAPPRESENTAZIONE ID, R.DATAORAINIZIO, V.NOME NOMEVENUE"
'        sql = sql & " FROM RAPPRESENTAZIONE R, VENUE V,"
'        sql = sql & " SCELTARAPPRESENTAZIONE_RAPPR SR"
'        sql = sql & " WHERE SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
'        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
'        sql = sql & " AND R.IDVENUE = V.IDVENUE"
'        sql = sql & " AND (SR.IDTIPOSTATORECORD IS NULL OR SR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
'    Else
        sql = "SELECT DISTINCT R.IDRAPPRESENTAZIONE ID, R.DATAORAINIZIO, V.NOME NOMEVENUE"
        sql = sql & " FROM RAPPRESENTAZIONE R, VENUE V,"
        sql = sql & " SCELTARAPPRESENTAZIONE_RAPPR SR"
        sql = sql & " WHERE SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = " & idRecordSelezionato
        sql = sql & " AND R.IDVENUE = V.IDVENUE"
        sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set rappresentazione = New clsElementoLista
            rappresentazione.idElementoLista = rec("ID").Value
            rappresentazione.descrizioneElementoLista = rec("DATAORAINIZIO") & " - " & rec("NOMEVENUE")
            chiaveRappresentazione = ChiaveId(rappresentazione.idElementoLista)
            Call listaRappresentazioniSelezionate.Add(rappresentazione, chiaveRappresentazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstSelezionati_Init
    
End Sub

Private Sub CaricaValoriDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeRappresentazione As String
    Dim idRappresentazione As Long
    Dim chiaveRappresentazione As String
    Dim rappresentazioneCorrente As clsElementoLista
    Dim condizioniSQL As String
    
    Call lstDisponibili.Clear
    
    Call ApriConnessioneBD
    
    Set listaRappresentazioniDisponibili = New Collection
    
    condizioniSQL = ""
    Select Case idTurnoRappresentazioneSelezionato
    Case idTuttiGliElementiSelezionati
        condizioniSQL = " AND R1.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE"
    Case ID_NESSUN_TURNO
        condizioniSQL = " AND R1.IDRAPPRESENTAZIONE NOT IN" & _
            " (SELECT IDRAPPRESENTAZIONE FROM RAPPRESENTAZIONE_TURNORAPPR)"
    Case Else
        condizioniSQL = " AND R1.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE" & _
            " AND RT.IDTURNORAPPRESENTAZIONE = " & idTurnoRappresentazioneSelezionato
    End Select
        
''    sql = "SELECT DISTINCT R1.IDRAPPRESENTAZIONE ID, R1.DATAORAINIZIO, V.NOME NOMEVENUE"
''    sql = sql & " FROM RAPPRESENTAZIONE R1, RAPPRESENTAZIONE_TURNORAPPR RT, VENUE_PIANTA VP, VENUE V,"
''    sql = sql & " ("
''    sql = sql & " SELECT DISTINCT R.IDRAPPRESENTAZIONE "
''    sql = sql & " FROM RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR"
''    sql = sql & " WHERE S.IDSCELTARAPPRESENTAZIONE = SR.IDSCELTARAPPRESENTAZIONE"
''    sql = sql & " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
''    sql = sql & " AND S.IDPRODOTTO = " & idProdottoSelezionato
''    sql = sql & " ) R2"
''    sql = sql & " WHERE R1.IDVENUE = VP.IDVENUE"
''    sql = sql & " AND VP.IDVENUE = V.IDVENUE"
''    sql = sql & " AND VP.IDPIANTA = " & idPiantaSelezionata
''    sql = sql & condizioniSQL
''    sql = sql & " AND R1.IDSPETTACOLO = " & idSpettacoloSelezionato
''    sql = sql & " AND R1.IDRAPPRESENTAZIONE = R2.IDRAPPRESENTAZIONE(+)"
''    sql = sql & " AND R2.IDRAPPRESENTAZIONE IS NULL"
''    sql = sql & " ORDER BY DATAORAINIZIO, NOMEVENUE"
'
'    sql = "SELECT DISTINCT R1.IDRAPPRESENTAZIONE ID, R1.DATAORAINIZIO" & _
'    " FROM RAPPRESENTAZIONE R1, PRODOTTO_RAPPRESENTAZIONE PR, PRODOTTO P, RAPPRESENTAZIONE_TURNORAPPR RT," & _
'    "(" & _
'    " SELECT DISTINCT R.IDRAPPRESENTAZIONE" & _
'    " FROM RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR" & _
'    " WHERE s.idSceltaRappresentazione = SR.idSceltaRappresentazione" & _
'    " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
'    " AND S.IDPRODOTTO = " & idProdottoSelezionato & _
'    " ) R2" & _
'    " WHERE R1.idRappresentazione = PR.idRappresentazione" & _
'    " AND PR.IDPRODOTTO = P.IDPRODOTTO" & _
'    " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'    " AND R1.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE" & _
'    " AND R1.IDSPETTACOLO = " & idSpettacoloSelezionato & _
'    " AND R1.IDRAPPRESENTAZIONE = R2.IDRAPPRESENTAZIONE(+)" & _
'    " AND R2.IDRAPPRESENTAZIONE IS NULL" & condizioniSQL & _
'    " ORDER BY DATAORAINIZIO"
    
    ' Considero anche le rappresentazioni ancora non associate a nessun prodotto
    sql = "SELECT DISTINCT R1.IDRAPPRESENTAZIONE ID, R1.DATAORAINIZIO" & _
    " FROM RAPPRESENTAZIONE R1, RAPPRESENTAZIONE_TURNORAPPR RT," & _
    "(" & _
    " SELECT DISTINCT R.IDRAPPRESENTAZIONE" & _
    " FROM RAPPRESENTAZIONE R, SCELTARAPPRESENTAZIONE S, SCELTARAPPRESENTAZIONE_RAPPR SR" & _
    " WHERE s.idSceltaRappresentazione = SR.idSceltaRappresentazione" & _
    " AND SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
    " AND S.IDPRODOTTO = " & idProdottoSelezionato & _
    " ) R2" & _
    " WHERE R1.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE" & _
    " AND R1.IDSPETTACOLO = " & idSpettacoloSelezionato & _
    " AND R1.IDRAPPRESENTAZIONE = R2.IDRAPPRESENTAZIONE(+)" & _
    " AND R2.IDRAPPRESENTAZIONE IS NULL" & condizioniSQL & _
    " ORDER BY DATAORAINIZIO"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set rappresentazioneCorrente = New clsElementoLista
            rappresentazioneCorrente.idElementoLista = rec("ID").Value
            rappresentazioneCorrente.descrizioneElementoLista = rec("DATAORAINIZIO") '& " - " & rec("NOMEVENUE")
            chiaveRappresentazione = ChiaveId(rappresentazioneCorrente.idElementoLista)
            Call listaRappresentazioniDisponibili.Add(rappresentazioneCorrente, chiaveRappresentazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaSelezionati()
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    
    For Each rappresentazione In listaRappresentazioniSelezionate
        chiaveRappresentazione = ChiaveId(rappresentazione.idElementoLista)
        Call listaRappresentazioniDisponibili.Add(rappresentazione, chiaveRappresentazione)
    Next rappresentazione
    Set listaRappresentazioniSelezionate = Nothing
    Set listaRappresentazioniSelezionate = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaDisponibili
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaDisponibili()
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    Dim listaRappresentazioniNonAssociabili As Collection
    
    Set listaRappresentazioniNonAssociabili = New Collection
    For Each rappresentazione In listaRappresentazioniDisponibili
        chiaveRappresentazione = ChiaveId(rappresentazione.idElementoLista)
        If RappresentazioneAssociabileAProdotto(rappresentazione.idElementoLista) Then
            Call listaRappresentazioniSelezionate.Add(rappresentazione, chiaveRappresentazione)
            Call listaRappresentazioniDisponibili.Remove(chiaveRappresentazione)
        Else
            Call listaRappresentazioniNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
        End If
    Next rappresentazione
    Set listaRappresentazioniDisponibili = Nothing
    Set listaRappresentazioniDisponibili = New Collection
    If listaRappresentazioniNonAssociabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguita", ArgomentoMessaggio(listaRappresentazioniNonAssociabili))
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idRappresentazione As Long
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    Dim listaRappresentazioniNonAssociabili As Collection
    
    Set listaRappresentazioniNonAssociabili = New Collection
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idRappresentazione = lstDisponibili.ItemData(i - 1)
            chiaveRappresentazione = ChiaveId(idRappresentazione)
            Set rappresentazione = listaRappresentazioniDisponibili.Item(chiaveRappresentazione)
            If RappresentazioneAssociabileAProdotto(idRappresentazione) Then
                Call listaRappresentazioniSelezionate.Add(rappresentazione, chiaveRappresentazione)
                Call listaRappresentazioniDisponibili.Remove(chiaveRappresentazione)
            Else
                Call listaRappresentazioniNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
            End If
        End If
    Next i
    If listaRappresentazioniNonAssociabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguita", ArgomentoMessaggio(listaRappresentazioniNonAssociabili))
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Function RappresentazioneAssociabileAProdotto_old(idRappresentazione As Long) As Boolean
    Dim elencoCorrelandi As String
    Dim correlandiHannoTitoliEmessi As Boolean
    
'    elencoCorrelandi = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
'    correlandiHannoTitoliEmessi = (NumeroTotaleTitoli(elencoCorrelandi) > 0)
'    If (Not ProdottoSelezionatoHaTitoli Or Not correlandiHannoTitoliEmessi Or _
'            (correlandiHannoTitoliEmessi And Not StessiPostiVendutiSuProdottoCorrenteECorrelandi(elencoCorrelandi))) And _
'            NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione) And _
'            TuttiProdottiCorrelandiInConfigurazione(idRappresentazione) Then
    If NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione) And _
            TuttiProdottiCorrelandiInConfigurazione(idRappresentazione) Then
        RappresentazioneAssociabileAProdotto_old = True
    Else
        RappresentazioneAssociabileAProdotto_old = False
    End If
End Function

Private Function RappresentazioneAssociabileAProdotto(idRappresentazione As Long) As Boolean
    Dim elencoCorrelandi As String
    Dim correlandiHannoTitoliEmessi As Boolean
    
'    elencoCorrelandi = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
'    correlandiHannoTitoliEmessi = (NumeroTotaleTitoli(elencoCorrelandi) > 0)
'    If (Not ProdottoSelezionatoHaTitoli Or Not correlandiHannoTitoliEmessi Or _
'            (correlandiHannoTitoliEmessi And Not StessiPostiVendutiSuProdottoCorrenteECorrelandi(elencoCorrelandi))) And _
'            NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione) And _
'            TuttiProdottiCorrelandiInConfigurazione(idRappresentazione) Then
'        RappresentazioneAssociabileAProdotto = True
'    Else
'        RappresentazioneAssociabileAProdotto = False
'    End If

' TUTTO PERMESSO!!!!!!!!!!!!
    RappresentazioneAssociabileAProdotto = True
End Function

Private Function NumeroTotaleTitoli(listaIdProdotto As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numeroTit As Long
    
    Call ApriConnessioneBD
    
'NOTA: NON C'E' BISOGNO DI RENDERE TRANSAZIONALE QUESTA QUERY, PERCHE' I PRODOTTI
'CON TITOLI EMESSI NON SONO ELIMINABILI
    If listaIdProdotto <> "" Then
        sql = "SELECT COUNT(T.IDTITOLO) NUMTIT"
        sql = sql & " FROM TITOLO T, STATOTITOLO ST"
        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO
        sql = sql & " AND IDPRODOTTO IN " & listaIdProdotto
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        numeroTit = rec("NUMTIT")
        rec.Close
    Else
        numeroTit = 0
    End If
    
    Call ChiudiConnessioneBD
    
    NumeroTotaleTitoli = numeroTit
End Function

Private Sub CreaTabellaAppoggioRappresentazioni()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_RAPPR_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDSCELTARAPPRESENTAZIONE NUMBER(10), IDSPETTACOLO NUMBER(10)," & _
        " NOMESPETTACOLO VARCHAR2(30), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggioRappresentazioni
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Sub PopolaTabellaAppoggioRappresentazioni()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idSceltaRappresentazione As Long
    Dim idSpettacolo As Long
    Dim elencoNomi As String
    Dim nomeSpettacolo As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioRappresentazione = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDSCELTARAPPRESENTAZIONE" & _
'            " FROM SCELTARAPPRESENTAZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL" & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT IDSCELTARAPPRESENTAZIONE" & _
            " FROM SCELTARAPPRESENTAZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDSCELTARAPPRESENTAZIONE").Value
            Call listaAppoggioRappresentazione.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioRappresentazione
        campoNome = ""
        sql = "SELECT DISTINCT R.IDSPETTACOLO IDSPETTACOLO, S.NOME NOMESPETTACOLO, DATAORAINIZIO "
        sql = sql & " FROM SCELTARAPPRESENTAZIONE_RAPPR SR, RAPPRESENTAZIONE R, SPETTACOLO S "
        sql = sql & " WHERE SR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE "
        sql = sql & " AND R.IDSPETTACOLO = S.IDSPETTACOLO"
        sql = sql & " AND SR.IDSCELTARAPPRESENTAZIONE = " & recordTemporaneo.idElementoLista
        sql = sql & " ORDER BY IDSPETTACOLO, DATAORAINIZIO"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            idSpettacolo = rec("IDSPETTACOLO").Value
            nomeSpettacolo = rec("NOMESPETTACOLO")
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("DATAORAINIZIO"), campoNome & "; " & rec("DATAORAINIZIO"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
        recordTemporaneo.idAttributoElementoLista = idSpettacolo
        recordTemporaneo.nomeAttributoElementoLista = nomeSpettacolo
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioRappresentazione
        idSceltaRappresentazione = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDSCELTARAPPRESENTAZIONE," & _
            " IDSPETTACOLO, NOMESPETTACOLO, NOME)" & _
            " VALUES (" & idSceltaRappresentazione & ", " & _
            recordTemporaneo.idAttributoElementoLista & ", " & _
            SqlStringValue(CStr(recordTemporaneo.nomeAttributoElementoLista)) & ", " & _
            SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Public Sub SetIdStagioneSelezionata(ids As Long)
    idStagioneSelezionata = ids
End Sub

Private Sub EliminaTabellaAppoggioRappresentazioni()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Function ProdottoSelezionatoHaTitoli() As Boolean
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim statement As Boolean
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(T.IDTITOLO) NUMTIT FROM TITOLO T, STATOTITOLO ST" & _
        " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
        " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO & _
        " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO & _
        " AND T.IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    statement = (rec("NUMTIT") > 0)
    rec.Close
    
    Call ChiudiConnessioneBD
    
    ProdottoSelezionatoHaTitoli = statement
End Function

Private Function NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elenco As String
    Dim esisteBlocco As Boolean
    
    Call ApriConnessioneBD
    
'MI PARE CHE QUESTA QUERY DOVREBBE RIMANERE COSI' ANCHE IN REGIME TRANSAZIONALE; VERIFICARE
    elenco = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
    If elenco <> "" Then
        sql = "SELECT COUNT(IDPRODOTTO) NUMBLOC FROM PRODOTTO" & _
            " WHERE IDPRODOTTO IN" & _
            " (SELECT IDPRODOTTO FROM CC_PRODOTTOUTILIZZATO" & _
            " WHERE UTENTE <> '" & getNomeMacchina & "')" & _
            " AND IDPRODOTTO IN " & elenco
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        esisteBlocco = (rec("NUMBLOC") > 0)
        rec.Close
    Else
        esisteBlocco = False
    End If
    
    Call ChiudiConnessioneBD
    
    NessunProdottoCorrelandoBloccatoDaAltroUtente = Not esisteBlocco
End Function

Private Function TuttiProdottiCorrelandiInConfigurazione(idRappresentazione As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elenco As String
    Dim esisteProdottoAttivo As Boolean
    
    Call ApriConnessioneBD
    
'MI PARE CHE QUESTA QUERY DOVREBBE RIMANERE COSI' ANCHE IN REGIME TRANSAZIONALE; VERIFICARE
    elenco = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
    If elenco <> "" Then
        sql = "SELECT COUNT(IDPRODOTTO) NUMPROD FROM PRODOTTO " & _
            " WHERE IDTIPOSTATOPRODOTTO <> " & TSP_IN_CONFIGURAZIONE & _
            " AND IDPRODOTTO IN " & elenco
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        esisteProdottoAttivo = (rec("NUMPROD") > 0)
        rec.Close
    Else
        esisteProdottoAttivo = False
    End If
    
    Call ChiudiConnessioneBD
    
    TuttiProdottiCorrelandiInConfigurazione = Not esisteProdottoAttivo
End Function

Private Function StessiPostiVendutiSuProdottoCorrenteECorrelandi(elencoCorrelandi As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim statement As Boolean
    
    Call ApriConnessioneBD
    
    statement = False
    If elencoCorrelandi <> "" Then
        sql = "SELECT P1.IDPOSTO"
        sql = sql & " FROM"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT IDPOSTO "
        sql = sql & " FROM TITOLO T, STATOTITOLO ST "
        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO
        sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
        sql = sql & " ) P1,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT IDPOSTO "
        sql = sql & " FROM TITOLO T, STATOTITOLO ST "
        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO
        sql = sql & " AND T.IDPRODOTTO IN " & elencoCorrelandi
        sql = sql & " ) P2"
        sql = sql & " WHERE P1.IDPOSTO = P2.IDPOSTO"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            statement = True
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD
    
    StessiPostiVendutiSuProdottoCorrenteECorrelandi = statement
End Function

