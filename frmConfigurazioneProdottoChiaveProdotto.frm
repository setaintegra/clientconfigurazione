VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneProdottoChiaveProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbNomeChiaveProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   6420
      Width           =   3615
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   15
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   14
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   13
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   12
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   4440
      Width           =   2775
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   7380
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   6420
      Width           =   4395
   End
   Begin VB.TextBox txtNome 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3900
      MaxLength       =   30
      TabIndex        =   4
      Top             =   6420
      Width           =   3315
   End
   Begin VB.ComboBox cmbTipoChiaveProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   5760
      Width           =   3615
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoChiaveProdotto 
      Height          =   330
      Left            =   6360
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoChiaveProdotto 
      Height          =   3495
      Left            =   120
      TabIndex        =   16
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6165
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblNomeChiaveProdotto 
      Caption         =   "Nome chiave prodotto"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   6180
      Width           =   2295
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle chiavi del prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   24
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   23
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   22
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   21
      Top             =   4200
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   4200
      Width           =   1815
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   7380
      TabIndex        =   19
      Top             =   6180
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   3900
      TabIndex        =   18
      Top             =   6180
      Width           =   1695
   End
   Begin VB.Label lblTipoChiaveProdotto 
      Caption         =   "Tipo chiave prodotto"
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   5520
      Width           =   1515
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoChiaveProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idTipoChiaveProdottoSelezionata As Long
Private isRecordEditabile As Boolean
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private rateo As Long
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private isProdottoAttivoSuTL As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    dgrConfigurazioneProdottoChiaveProdotto.Caption = "CHIAVI PRODOTTO CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoChiaveProdotto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbTipoChiaveProdotto.Clear
        Call cmbNomeChiaveProdotto.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        cmbTipoChiaveProdotto.Enabled = False
        cmbNomeChiaveProdotto.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblTipoChiaveProdotto.Enabled = False
        lblNomeChiaveProdotto.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoChiaveProdotto.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            idRecordSelezionato = idAggiungiNuovo)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            idRecordSelezionato = idAggiungiNuovo)
        cmbTipoChiaveProdotto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbNomeChiaveProdotto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            idTipoChiaveProdottoSelezionata <> idNessunElementoSelezionato)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            idRecordSelezionato = idAggiungiNuovo)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            idRecordSelezionato = idAggiungiNuovo)
        lblTipoChiaveProdotto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNomeChiaveProdotto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            idTipoChiaveProdottoSelezionata <> idNessunElementoSelezionato)
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> "" And _
            cmbTipoChiaveProdotto.ListIndex <> idNessunElementoSelezionato
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoChiaveProdotto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbTipoChiaveProdotto.Clear
        Call cmbNomeChiaveProdotto.Clear
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        cmbTipoChiaveProdotto.Enabled = False
        cmbNomeChiaveProdotto.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblTipoChiaveProdotto.Enabled = False
        lblNomeChiaveProdotto.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
    End Select
    
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub CaricaFormImpostazioneCambioUtilizzatore()
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetRateo(rateo)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.Init
End Sub

Private Sub CaricaFormImpostazioneAbilitazionePuntiVendita()
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetRateo(rateo)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.Init
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
    If isProdottoCalendario(idProdottoSelezionato) Then
        Call CaricaFormImpostazioneAbilitazionePuntiVendita
    Else
        Call CaricaFormImpostazioneCambioUtilizzatore
    End If
End Sub

Private Sub cmbNomeChiaveProdotto_Click()
    idRecordSelezionato = cmbNomeChiaveProdotto.ItemData(cmbNomeChiaveProdotto.ListIndex)
    If Not internalEvent Then
        Call SelezionaAggiungiNuovaChiave
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmbTipoChiaveProdotto_Click()
    idTipoChiaveProdottoSelezionata = cmbTipoChiaveProdotto.ItemData(cmbTipoChiaveProdotto.ListIndex)
    If Not internalEvent Then
        Call CaricaValoriCmbNomeChiaveProdotto
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
    
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If isRecordEditabile Then
                Select Case gestioneRecordGriglia
                    Case ASG_INSERISCI_NUOVO
                        If valoriCampiOK Then
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_CHIAVE_PRODOTTO, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoChiaveProdotto_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoChiaveProdotto_Init
                        End If
                    Case ASG_ELIMINA
                        Call EliminaDallaBaseDati
                        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_CHIAVE_PRODOTTO, stringaNota, idProdottoSelezionato)
                        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                        Call adcConfigurazioneProdottoChiaveProdotto_Init
                        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                        Call dgrConfigurazioneProdottoChiaveProdotto_Init
                End Select
            End If
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sqlTipoChiave As String
    Dim sqlNomeChiave As String
    
    sqlTipoChiave = "SELECT IDTIPOCHIAVEPRODOTTO ID, NOME" & _
        " FROM TIPOCHIAVEPRODOTTO WHERE IDTIPOCHIAVEPRODOTTO > 0" & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    sqlNomeChiave = "SELECT C.IDCHIAVEPRODOTTO ID, C.NOME NOME" & _
        " FROM CHIAVEPRODOTTO C, PRODOTTO_CHIAVEPRODOTTO P" & _
        " WHERE P.IDCHIAVEPRODOTTO(+) = C.IDCHIAVEPRODOTTO" & _
        " AND P.IDCHIAVEPRODOTTO IS NULL " & _
        " AND C.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND C.IDTIPOCHIAVEPRODOTTO = " & idTipoChiaveProdottoSelezionata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbTipoChiaveProdotto, sqlTipoChiave, "NOME", False)
    Call CaricaValoriCombo(cmbNomeChiaveProdotto, sqlNomeChiave, "NOME", False)
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriCmbNomeChiaveProdotto()
    Dim sql As String
    
    Call cmbNomeChiaveProdotto.Clear
'    sql = "SELECT C.IDCHIAVEPRODOTTO ID, C.NOME NOME" & _
'        " FROM CHIAVEPRODOTTO C, PRODOTTO_CHIAVEPRODOTTO P" & _
'        " WHERE P.IDCHIAVEPRODOTTO(+) = C.IDCHIAVEPRODOTTO" & _
'        " AND P.IDCHIAVEPRODOTTO(+) IS NULL " & _
'        " AND C.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'        " AND C.IDTIPOCHIAVEPRODOTTO <> " & idTipoChiaveProdottoSelezionata & _
'        " ORDER BY NOME"
    sql = " SELECT C.IDCHIAVEPRODOTTO ID, C.NOME NOME" & _
        " FROM CHIAVEPRODOTTO C, PRODOTTO_CHIAVEPRODOTTO P" & _
        " WHERE P.IDCHIAVEPRODOTTO(+) = C.IDCHIAVEPRODOTTO" & _
        " AND P.IDCHIAVEPRODOTTO IS NULL" & _
        " AND P.IDPRODOTTO(+) = " & idProdottoSelezionato & _
        " AND C.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND C.IDTIPOCHIAVEPRODOTTO = " & idTipoChiaveProdottoSelezionata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbNomeChiaveProdotto, sql, "NOME", True)
End Sub

Private Sub SelezionaAggiungiNuovaChiave()
    If idRecordSelezionato = idAggiungiNuovo Then
        'Do Nothing
    Else
        Call CaricaDallaBaseDati
        Call AssegnaValoriCampi
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sqlTipoChiave As String
    
    isRecordEditabile = True
    sqlTipoChiave = "SELECT IDTIPOCHIAVEPRODOTTO ID, NOME" & _
        " FROM TIPOCHIAVEPRODOTTO WHERE IDTIPOCHIAVEPRODOTTO > 0" & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbTipoChiaveProdotto, sqlTipoChiave, "NOME", False)
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub


Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoChiaveProdotto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneProdottoChiaveProdotto_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    idTipoChiaveProdottoSelezionata = idNessunElementoSelezionato
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoChiaveProdotto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoChiaveProdotto_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoChiaveProdotto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoChiaveProdotto_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazioneProdottoChiaveProdotto
        
    sql = "SELECT C.IDCHIAVEPRODOTTO ID, T.NOME ""Tipo chiave""," & _
        " C.NOME ""Nome chiave"", C.DESCRIZIONE AS ""Descrizione chiave""" & _
        " FROM TIPOCHIAVEPRODOTTO T, CHIAVEPRODOTTO C, PRODOTTO_CHIAVEPRODOTTO P" & _
        " WHERE T.IDTIPOCHIAVEPRODOTTO = C.IDTIPOCHIAVEPRODOTTO" & _
        " AND P.IDCHIAVEPRODOTTO = C.IDCHIAVEPRODOTTO" & _
        " AND P.IDPRODOTTO = " & idProdottoSelezionato & _
        " ORDER BY ""Tipo chiave"", ""Nome chiave"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneProdottoChiaveProdotto.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovaChiave As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA TARIFFA
    If idRecordSelezionato = idAggiungiNuovo Then
        idNuovaChiave = OttieniIdentificatoreDaSequenza("SQ_CHIAVEPRODOTTO")
        idRecordSelezionato = idNuovaChiave
        sql = "INSERT INTO CHIAVEPRODOTTO (IDCHIAVEPRODOTTO, NOME, DESCRIZIONE," & _
            " IDTIPOCHIAVEPRODOTTO, IDORGANIZZAZIONE)" & _
            " VALUES (" & _
            idNuovaChiave & ", " & _
            SqlStringValue(nomeRecordSelezionato) & ", " & _
            SqlStringValue(descrizioneRecordSelezionato) & ", " & _
            idTipoChiaveProdottoSelezionata & ", " & _
            idOrganizzazioneSelezionata & ")"
        SETAConnection.Execute sql, n, adCmdText
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = TSR_NUOVO
'            Call AggiornaParametriSessioneSuRecord("CHIAVEPRODOTTO", "IDCHIAVEPRODOTTO", idNuovaChiave, TSR_NUOVO)
'        End If
    End If

'   INSERIMENTO IN TABELLA PRODOTTO_CHIAVEPRODOTTO
    sql = "INSERT INTO PRODOTTO_CHIAVEPRODOTTO (IDCHIAVEPRODOTTO, IDPRODOTTO)" & _
        " VALUES " & _
        "(" & idRecordSelezionato & ", " & _
        idProdottoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("PRODOTTO_CHIAVEPRODOTTO", "IDCHIAVEPRODOTTO", idNuovaChiave, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaChiave)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDTIPOCHIAVEPRODOTTO, NOME, IDTIPOCHIAVEPRODOTTO, DESCRIZIONE," & _
'            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
'            " FROM CHIAVEPRODOTTO" & _
'            " WHERE IDCHIAVEPRODOTTO = " & idRecordSelezionato
'    Else
        sql = "SELECT IDTIPOCHIAVEPRODOTTO, NOME, IDTIPOCHIAVEPRODOTTO, DESCRIZIONE" & _
            " FROM CHIAVEPRODOTTO" & _
            " WHERE IDCHIAVEPRODOTTO = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        idTipoChiaveProdottoSelezionata = rec("IDTIPOCHIAVEPRODOTTO")
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    Call SelezionaElementoSuCombo(cmbTipoChiaveProdotto, idTipoChiaveProdottoSelezionata)
    Call SelezionaElementoSuCombo(cmbNomeChiaveProdotto, idRecordSelezionato)
    
    internalEvent = internalEventOld

End Sub

Private Sub dgrConfigurazioneProdottoChiaveProdotto_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoChiaveProdotto
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    
    If idRecordSelezionato = idAggiungiNuovo Then
        condizioneSql = "IDTIPOCHIAVEPRODOTTO = " & idTipoChiaveProdottoSelezionata & _
            " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    Else
        condizioneSql = "IDTIPOCHIAVEPRODOTTO = " & idNessunElementoSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("CHIAVEPRODOTTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "", _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB per la stessa Organizzazione;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.AzionePercorsoGuidato(TNCP_FINE)
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, elencaValoriEstesi As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    If elencaValoriEstesi Then
        If i <= 0 Then
            i = 1
        End If
        cmb.AddItem "<nuovo...>"
        cmb.ItemData(i - 1) = idAggiungiNuovo
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Dim i As Integer
'
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim listaTabelleCorrelate As Collection
    Dim chiaveEliminabile As Boolean
    Dim tabelleCorrelate As String
    Dim n As Long
    Dim condizioneSql As String

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    chiaveEliminabile = True
    
'    If Not IsRecordEliminabile("IDTARIFFA", "UTILIZZOLAYOUTSUPPORTO", CStr(idTar)) Then
'        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
'        tariffaEliminabile = False
'    End If
    
    If chiaveEliminabile Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                sql = "DELETE FROM PRODOTTO_CHIAVEPRODOTTO"
'                sql = sql & " WHERE IDCHIAVEPRODOTTO = " & idRecordSelezionato
'                sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'            Else
'                condizioneSql = " AND IDPRODOTTO = " & idProdottoSelezionato
'                Call AggiornaParametriSessioneSuRecord("PRODOTTO_CHIAVEPRODOTTO", "IDCHIAVEPRODOTTO", idRecordSelezionato, TSR_ELIMINATO, condizioneSql)
'            End If
'        Else
            sql = "DELETE FROM PRODOTTO_CHIAVEPRODOTTO"
            sql = sql & " WHERE IDCHIAVEPRODOTTO = " & idRecordSelezionato
            sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Chiave prodotto selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD

End Sub


Public Sub SetRateo(r As Long)
    rateo = r
End Sub

