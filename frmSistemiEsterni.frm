VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSistemiEsterni 
   Caption         =   "Sistemi esterni"
   ClientHeight    =   9015
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10575
   LinkTopic       =   "Form1"
   ScaleHeight     =   9015
   ScaleWidth      =   10575
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmProdottiEsterni 
      Caption         =   "Prodotti esterni"
      Height          =   4935
      Left            =   120
      TabIndex        =   18
      Top             =   3480
      Width           =   10335
      Begin VB.ComboBox cmbProdottiEsterni 
         Height          =   315
         Left            =   240
         TabIndex        =   35
         Top             =   360
         Width           =   4095
      End
      Begin VB.Frame frmInternoProdottiEsterni 
         Height          =   4095
         Left            =   240
         TabIndex        =   22
         Top             =   720
         Width           =   8775
         Begin VB.CommandButton cmdAssociaArea 
            Caption         =   "Associa"
            Height          =   255
            Left            =   6120
            TabIndex        =   41
            Top             =   2280
            Width           =   1455
         End
         Begin VB.TextBox txtIdAreaEsterna 
            Height          =   285
            Left            =   5280
            TabIndex        =   32
            Top             =   2280
            Width           =   735
         End
         Begin VB.ListBox lstAree 
            Height          =   2010
            Left            =   1200
            TabIndex        =   31
            Top             =   1440
            Width           =   2655
         End
         Begin VB.TextBox txtOraAperturaCA 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5040
            MaxLength       =   2
            TabIndex        =   29
            Top             =   960
            Width           =   435
         End
         Begin VB.TextBox txtMinutiAperturaCA 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5640
            MaxLength       =   2
            TabIndex        =   30
            Top             =   960
            Width           =   435
         End
         Begin VB.ComboBox cmbProdotti 
            Height          =   315
            Left            =   6240
            TabIndex        =   27
            Top             =   600
            Width           =   2415
         End
         Begin VB.ComboBox cmbOrganizzazioni 
            Height          =   315
            Left            =   3840
            TabIndex        =   26
            Top             =   600
            Width           =   2415
         End
         Begin VB.TextBox txtProdottoSETA 
            Height          =   285
            Left            =   1200
            TabIndex        =   25
            Top             =   600
            Width           =   2415
         End
         Begin VB.CommandButton cmdConfermaProdottiEsterni 
            Caption         =   "Conferma"
            Height          =   375
            Left            =   2880
            TabIndex        =   24
            Top             =   3600
            Width           =   1455
         End
         Begin VB.CommandButton cmdAnnullaProdottiEsterni 
            Caption         =   "Annulla"
            Height          =   375
            Left            =   4440
            TabIndex        =   23
            Top             =   3600
            Width           =   1455
         End
         Begin MSComCtl2.DTPicker dtpDataAperturaCA 
            Height          =   315
            Left            =   1620
            TabIndex        =   28
            Top             =   960
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   66912257
            CurrentDate     =   37684.7534259259
         End
         Begin VB.Label lblIdAreaEsterna 
            Caption         =   "id area esterna:"
            Height          =   255
            Left            =   4080
            TabIndex        =   40
            Top             =   2280
            Width           =   1095
         End
         Begin VB.Label lblListaAree 
            Caption         =   "lista aree:"
            Height          =   255
            Left            =   120
            TabIndex        =   39
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label lblDataAperturaCA 
            Alignment       =   1  'Right Justify
            Caption         =   "data"
            Height          =   255
            Left            =   1200
            TabIndex        =   38
            Top             =   960
            Width           =   315
         End
         Begin VB.Label lblSeparatoreOreMinuti 
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5520
            TabIndex        =   37
            Top             =   960
            Width           =   75
         End
         Begin VB.Label lblOraAperturaCA 
            Alignment       =   1  'Right Justify
            Caption         =   "ora (HH:MM)"
            Height          =   255
            Left            =   4020
            TabIndex        =   36
            Top             =   960
            Width           =   915
         End
         Begin VB.Label lblProdottoSETA 
            Caption         =   "prodotto SETA"
            Height          =   255
            Left            =   120
            TabIndex        =   34
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label lblAperturaCA 
            Caption         =   "apertura CA:"
            Height          =   255
            Left            =   120
            TabIndex        =   33
            Top             =   960
            Width           =   975
         End
      End
      Begin VB.CommandButton cmdNuovoProdottoEsterno 
         Caption         =   "Nuovo"
         Height          =   375
         Left            =   9120
         TabIndex        =   21
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton cmdModificaProdottoEsterno 
         Caption         =   "Modifica"
         Height          =   375
         Left            =   9120
         TabIndex        =   20
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancellaProdottoEsterno 
         Caption         =   "Cancella"
         Height          =   375
         Left            =   9120
         TabIndex        =   19
         Top             =   1800
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   375
      Left            =   9360
      TabIndex        =   15
      Top             =   8520
      Width           =   1095
   End
   Begin VB.Frame frmSistemiEsterni 
      Caption         =   "Sistemi esterni"
      Height          =   3135
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10335
      Begin VB.CommandButton cmdCancellaSistemaEsterno 
         Caption         =   "Cancella"
         Height          =   375
         Left            =   9120
         TabIndex        =   13
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CommandButton cmdModificaSistemaEsterno 
         Caption         =   "Modifica"
         Height          =   375
         Left            =   9120
         TabIndex        =   12
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton cmdNuovoSistemaEsterno 
         Caption         =   "Nuovo"
         Height          =   375
         Left            =   9120
         TabIndex        =   11
         Top             =   840
         Width           =   1095
      End
      Begin VB.Frame frmInternoSistemiEsterni 
         Height          =   2295
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   8775
         Begin VB.ComboBox cmbInterfacciaSistemaEsterno 
            Height          =   315
            Left            =   3960
            TabIndex        =   17
            Top             =   1320
            Width           =   3255
         End
         Begin VB.TextBox txtInterfacciaSistemaEsterno 
            Height          =   285
            Left            =   1080
            TabIndex        =   16
            Top             =   1320
            Width           =   2775
         End
         Begin VB.CommandButton cmdAnnullaSistemiEsterni 
            Caption         =   "Annulla"
            Height          =   375
            Left            =   4440
            TabIndex        =   10
            Top             =   1800
            Width           =   1455
         End
         Begin VB.CommandButton cmdConfermaSistemiEsterni 
            Caption         =   "Conferma"
            Height          =   375
            Left            =   2880
            TabIndex        =   9
            Top             =   1800
            Width           =   1455
         End
         Begin VB.TextBox txtDescrizioneSistemaEsterno 
            Height          =   285
            Left            =   1080
            TabIndex        =   8
            Top             =   960
            Width           =   2775
         End
         Begin VB.TextBox txtNomeSistemaEsterno 
            Height          =   285
            Left            =   1080
            TabIndex        =   7
            Top             =   600
            Width           =   2775
         End
         Begin VB.TextBox txtIdSistemaEsterno 
            Height          =   285
            Left            =   1080
            TabIndex        =   6
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label lblInterfacciaSistemaEsterno 
            Caption         =   "interfaccia:"
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblDescrizioneSistemaEsterno 
            Caption         =   "descrizione:"
            Height          =   255
            Left            =   120
            TabIndex        =   5
            Top             =   960
            Width           =   975
         End
         Begin VB.Label lblNomeSistemaEsterno 
            Caption         =   "nome:"
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   600
            Width           =   975
         End
         Begin VB.Label lblIdSistemaEsterno 
            Caption         =   "id:"
            Height          =   255
            Left            =   120
            TabIndex        =   3
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.ComboBox cmbSistemiEsterni 
         Height          =   315
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   3975
      End
   End
End
Attribute VB_Name = "frmSistemiEsterni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum StatoEnum
    STFSE_NON_SPECIFICATO = 0
    STFSE_NESSUN_SISTEMAESTERNO = 1
    STFSE_SISTEMAESTERNO_SELEZIONATO = 2
    STFSE_NUOVO_SISTEMAESTERNO = 3
    STFSE_MODIFICA_SISTEMAESTERNO = 4
    STFSE_CANCELLA_SISTEMAESTERNO = 5
    STFSE_NESSUN_PRODOTTOESTERNO = 6
    STFSE_PRODOTTOESTERNO_SELEZIONATO = 7
    STFSE_NUOVO_PRODOTTOESTERNO = 8
    STFSE_MODIFICA_PRODOTTOESTERNO = 9
    STFSE_CANCELLA_PRODOTTOESTERNO = 10
End Enum

Private idSistemaEsternoSelezionato As Long
Private idInterfacciaSistemaEsternoSelezionata As Long
Private idProdottoEsternoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idProdottoSETASelezionato As Long
Private dataOraAperturaCA As Date

Private internalEvent As Boolean
Private stato As StatoEnum

Private Sub CaricaComboSistemiEsterni()
    Dim sql As String

    cmbSistemiEsterni.Clear
    sql = "SELECT IDSISTEMAESTERNO ID, NOME, DESCRIZIONE" & _
        " FROM SISTEMAESTERNO" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo2(cmbSistemiEsterni, sql, "NOME", False)

End Sub

Private Sub CaricaComboProdottiEsterni()
    Dim sql As String

    cmbProdottiEsterni.Clear
    sql = "SELECT PE.IDPRODOTTOESTERNO ID, PE.IDPRODOTTO || ' - ' || P.NOME NOME, DATAORAAPERTURACONTROLLOACC" & _
        " FROM PRODOTTOESTERNO PE, PRODOTTO P" & _
        " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato & _
        " AND PE.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDTIPOSTATOPRODOTTO IN (1,2,3)" & _
        " ORDER BY DATAORAAPERTURACONTROLLOACC DESC"
    Call CaricaValoriCombo2(cmbProdottiEsterni, sql, "NOME", False)

End Sub

Public Sub Init()
    stato = STFSE_NON_SPECIFICATO
    idSistemaEsternoSelezionato = idNessunElementoSelezionato
    Call CaricaComboSistemiEsterni
    stato = STFSE_NESSUN_SISTEMAESTERNO
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub AnnullaValoriSistemiEsterni()
    txtIdSistemaEsterno.Text = ""
    txtNomeSistemaEsterno.Text = ""
    txtDescrizioneSistemaEsterno.Text = ""
    txtInterfacciaSistemaEsterno.Text = ""
End Sub

Private Sub AnnullaValoriProdottiEsterni()
'    txtIdProdottoEsterno.Text = ""
    txtProdottoSETA.Text = ""
    dtpDataAperturaCA.Value = Null
    txtOraAperturaCA.Text = ""
    txtMinutiAperturaCA.Text = ""
End Sub

Private Sub DisabilitaFrameProdottiEsterni()
    Call AnnullaValoriProdottiEsterni
'    lblIdProdottoEsterno.Enabled = False
'    txtIdProdottoEsterno.Enabled = False
    lblProdottoSETA.Enabled = False
    txtProdottoSETA.Enabled = False
    cmbOrganizzazioni.Enabled = False
    cmbProdotti.Enabled = False
    lblAperturaCA.Enabled = False
    lblDataAperturaCA.Enabled = False
    dtpDataAperturaCA.Enabled = False
    lblOraAperturaCA.Enabled = False
    txtOraAperturaCA.Enabled = False
    lblSeparatoreOreMinuti.Enabled = False
    txtMinutiAperturaCA.Enabled = False
    cmdConfermaProdottiEsterni.Enabled = False
    cmdAnnullaProdottiEsterni.Enabled = False
    Call DisabilitaComandiProdottiEsterni
End Sub

Private Sub DisabilitaComandiProdottiEsterni()
    cmdNuovoProdottoEsterno.Enabled = False
    cmdModificaProdottoEsterno.Enabled = False
    cmdCancellaProdottoEsterno.Enabled = False
End Sub

Private Sub AggiornaAbilitazioneControlli()
    Select Case stato
        Case STFSE_NESSUN_SISTEMAESTERNO
            frmInternoSistemiEsterni.Enabled = False
            Call AnnullaValoriSistemiEsterni
            Call AnnullaValoriProdottiEsterni
            lblIdSistemaEsterno.Enabled = False
            txtIdSistemaEsterno.Enabled = False
            lblNomeSistemaEsterno.Enabled = False
            txtNomeSistemaEsterno.Enabled = False
            lblDescrizioneSistemaEsterno.Enabled = False
            txtDescrizioneSistemaEsterno.Enabled = False
            lblInterfacciaSistemaEsterno.Enabled = False
            txtInterfacciaSistemaEsterno.Visible = True
            txtInterfacciaSistemaEsterno.Enabled = False
            cmbInterfacciaSistemaEsterno.Visible = False
            cmdConfermaSistemiEsterni.Enabled = False
            cmdAnnullaSistemiEsterni.Enabled = False
            cmdNuovoSistemaEsterno.Enabled = True
            cmdModificaSistemaEsterno.Enabled = False
            cmdCancellaSistemaEsterno.Enabled = False
            Call DisabilitaFrameProdottiEsterni
        Case STFSE_SISTEMAESTERNO_SELEZIONATO
            frmInternoSistemiEsterni.Enabled = False
            lblIdSistemaEsterno.Enabled = False
            txtIdSistemaEsterno.Enabled = False
            lblNomeSistemaEsterno.Enabled = False
            txtNomeSistemaEsterno.Enabled = False
            lblDescrizioneSistemaEsterno.Enabled = False
            txtDescrizioneSistemaEsterno.Enabled = False
            lblInterfacciaSistemaEsterno.Enabled = False
            txtInterfacciaSistemaEsterno.Visible = True
            txtInterfacciaSistemaEsterno.Enabled = False
            cmbInterfacciaSistemaEsterno.Visible = False
            cmdConfermaSistemiEsterni.Enabled = False
            cmdAnnullaSistemiEsterni.Enabled = False
            cmdNuovoSistemaEsterno.Enabled = True
            cmdModificaSistemaEsterno.Enabled = True
            cmdCancellaSistemaEsterno.Enabled = True
            Call DisabilitaFrameProdottiEsterni
            cmdNuovoProdottoEsterno.Enabled = True
        Case STFSE_NUOVO_SISTEMAESTERNO
            frmInternoSistemiEsterni.Enabled = True
            Call AnnullaValoriSistemiEsterni
            Call AnnullaValoriProdottiEsterni
            lblIdSistemaEsterno.Enabled = False
            txtIdSistemaEsterno.Enabled = False
            lblNomeSistemaEsterno.Enabled = True
            txtNomeSistemaEsterno.Enabled = True
            lblDescrizioneSistemaEsterno.Enabled = True
            txtDescrizioneSistemaEsterno.Enabled = True
            lblInterfacciaSistemaEsterno.Enabled = True
            txtInterfacciaSistemaEsterno.Visible = False
            cmbInterfacciaSistemaEsterno.Visible = True
            cmbInterfacciaSistemaEsterno.Enabled = True
            cmdConfermaSistemiEsterni.Enabled = True
            cmdAnnullaSistemiEsterni.Enabled = True
            cmdNuovoSistemaEsterno.Enabled = False
            cmdModificaSistemaEsterno.Enabled = False
            cmdCancellaSistemaEsterno.Enabled = False
        Case STFSE_MODIFICA_SISTEMAESTERNO
            frmInternoSistemiEsterni.Enabled = True
            lblIdSistemaEsterno.Enabled = False
            txtIdSistemaEsterno.Enabled = False
            lblNomeSistemaEsterno.Enabled = True
            txtNomeSistemaEsterno.Enabled = True
            lblDescrizioneSistemaEsterno.Enabled = True
            txtDescrizioneSistemaEsterno.Enabled = True
            lblInterfacciaSistemaEsterno.Enabled = True
            txtInterfacciaSistemaEsterno.Visible = False
            cmbInterfacciaSistemaEsterno.Visible = True
            cmbInterfacciaSistemaEsterno.Enabled = True
            cmdConfermaSistemiEsterni.Enabled = True
            cmdAnnullaSistemiEsterni.Enabled = True
            idInterfacciaSistemaEsternoSelezionata = idNessunElementoSelezionato
            cmdNuovoSistemaEsterno.Enabled = False
            cmdModificaSistemaEsterno.Enabled = False
            cmdCancellaSistemaEsterno.Enabled = False
        Case STFSE_CANCELLA_SISTEMAESTERNO
            frmInternoSistemiEsterni.Enabled = True
            lblIdSistemaEsterno.Enabled = False
            txtIdSistemaEsterno.Enabled = False
            lblNomeSistemaEsterno.Enabled = False
            txtNomeSistemaEsterno.Enabled = False
            lblDescrizioneSistemaEsterno.Enabled = False
            txtDescrizioneSistemaEsterno.Enabled = False
            lblInterfacciaSistemaEsterno.Enabled = False
            txtInterfacciaSistemaEsterno.Visible = True
            cmbInterfacciaSistemaEsterno.Visible = False
            cmbInterfacciaSistemaEsterno.Enabled = False
            cmdConfermaSistemiEsterni.Enabled = True
            cmdAnnullaSistemiEsterni.Enabled = True
            cmdNuovoSistemaEsterno.Enabled = False
            cmdModificaSistemaEsterno.Enabled = False
            cmdCancellaSistemaEsterno.Enabled = False
        Case STFSE_NESSUN_PRODOTTOESTERNO
            frmInternoProdottiEsterni.Enabled = False
'            lblIdProdottoEsterno.Enabled = False
'            txtIdProdottoEsterno.Enabled = False
            cmdConfermaProdottiEsterni.Enabled = False
            cmdAnnullaProdottiEsterni.Enabled = False
            cmdNuovoProdottoEsterno.Enabled = True
            lstAree.Enabled = False
            lblIdAreaEsterna.Enabled = False
            txtIdAreaEsterna.Enabled = False
            cmdModificaProdottoEsterno.Enabled = True
            cmdCancellaProdottoEsterno.Enabled = True
            cmdNuovoProdottoEsterno.Enabled = True
            cmdModificaProdottoEsterno.Enabled = False
            cmdCancellaProdottoEsterno.Enabled = False
        Case STFSE_PRODOTTOESTERNO_SELEZIONATO
            frmInternoProdottiEsterni.Enabled = True
'            lblIdProdottoEsterno.Enabled = False
'            txtIdProdottoEsterno.Enabled = False
            cmdConfermaProdottiEsterni.Enabled = False
            cmdAnnullaProdottiEsterni.Enabled = False
            cmdNuovoProdottoEsterno.Enabled = True
            lstAree.Enabled = True
            lblIdAreaEsterna.Enabled = False
            txtIdAreaEsterna.Enabled = False
            cmdModificaProdottoEsterno.Enabled = True
            cmdCancellaProdottoEsterno.Enabled = True
        Case STFSE_NUOVO_PRODOTTOESTERNO
            Call AnnullaValoriProdottiEsterni
'            lblIdProdottoEsterno.Enabled = False
'            txtIdProdottoEsterno.Enabled = False
            lblProdottoSETA.Enabled = False
            txtProdottoSETA.Enabled = False
            cmbOrganizzazioni.Enabled = True
            cmbProdotti.Enabled = True
            lblAperturaCA.Enabled = True
            lblDataAperturaCA.Enabled = True
            dtpDataAperturaCA.Enabled = True
            lblOraAperturaCA.Enabled = True
            txtOraAperturaCA.Enabled = True
            lblSeparatoreOreMinuti.Enabled = True
            txtMinutiAperturaCA.Enabled = True
            cmdConfermaProdottiEsterni.Enabled = True
            cmdAnnullaProdottiEsterni.Enabled = True
        Case STFSE_MODIFICA_PRODOTTOESTERNO
            frmInternoProdottiEsterni.Enabled = True
'            lblIdProdottoEsterno.Enabled = False
'            txtIdProdottoEsterno.Enabled = False
            lblProdottoSETA.Enabled = True
            txtProdottoSETA.Visible = True
            txtProdottoSETA.Enabled = False
            cmbOrganizzazioni.Enabled = True
            cmbProdotti.Enabled = True
            cmdConfermaProdottiEsterni.Enabled = True
            cmdAnnullaProdottiEsterni.Enabled = True
            idOrganizzazioneSelezionata = idNessunElementoSelezionato
            lstAree.Enabled = True
            lblIdAreaEsterna.Enabled = True
            txtIdAreaEsterna.Enabled = True
            cmdNuovoProdottoEsterno.Enabled = False
            cmdModificaProdottoEsterno.Enabled = False
            cmdCancellaProdottoEsterno.Enabled = False
        Case STFSE_CANCELLA_PRODOTTOESTERNO
            frmInternoProdottiEsterni.Enabled = True
'            lblIdProdottoEsterno.Enabled = False
'            txtIdProdottoEsterno.Enabled = False
            lblProdottoSETA.Enabled = False
            txtProdottoSETA.Enabled = False
            lblDataAperturaCA.Enabled = False
            dtpDataAperturaCA.Enabled = False
            lblOraAperturaCA.Enabled = False
            txtOraAperturaCA.Enabled = False
            lblSeparatoreOreMinuti.Enabled = False
            txtMinutiAperturaCA.Enabled = False
            cmdConfermaProdottiEsterni.Enabled = True
            cmdAnnullaProdottiEsterni.Enabled = True
            cmdNuovoProdottoEsterno.Enabled = False
            cmdModificaProdottoEsterno.Enabled = False
            cmdCancellaProdottoEsterno.Enabled = False
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmbOrganizzazioni_Click()
    Dim sql As String
    idOrganizzazioneSelezionata = cmbOrganizzazioni.ItemData(cmbOrganizzazioni.ListIndex)
    
    sql = "SELECT IDPRODOTTO ID, IDPRODOTTO || ' - ' || NOME NOME" & _
        " FROM PRODOTTO" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY IDPRODOTTO DESC"
    Call CaricaValoriCombo2(cmbProdotti, sql, "NOME", False)
End Sub

Private Sub cmbProdotti_Click()
    idProdottoSETASelezionato = cmbProdotti.ItemData(cmbProdotti.ListIndex)
    Call CaricaListaAree
End Sub

Private Sub cmbSistemiEsterni_Click()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    idSistemaEsternoSelezionato = cmbSistemiEsterni.ItemData(cmbSistemiEsterni.ListIndex)
    
    sql = "SELECT SE.NOME, SE.DESCRIZIONE, I.NOME INTERFACCIA" & _
        " FROM SISTEMAESTERNO SE, INTERFACCIASISTEMAESTERNO I" & _
        " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato & _
        " AND SE.IDINTERFACCIASISTEMAESTERNO = I.IDINTERFACCIASISTEMAESTERNO"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Call InizializzaFrameSistemaEsterno(idSistemaEsternoSelezionato, rec("NOME"), rec("DESCRIZIONE"), rec("INTERFACCIA"))
            Call CaricaComboProdottiEsterni
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    stato = STFSE_SISTEMAESTERNO_SELEZIONATO
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbInterfacciaSistemaEsterno_Click()
    idInterfacciaSistemaEsternoSelezionata = cmbInterfacciaSistemaEsterno.ItemData(cmbInterfacciaSistemaEsterno.ListIndex)
End Sub

Private Sub cmdNuovoProdottoEsterno_Click()
    Call NuovoProdottoEsterno
End Sub

Private Sub cmdNuovoSistemaEsterno_Click()
    Call NuovoSistemaEsterno
End Sub

Private Sub cmdModificaSistemaEsterno_Click()
    Call ModificaSistemaEsterno
End Sub

Private Sub cmdModificaProdottoEsterno_Click()
    Call ModificaProdottoEsterno
End Sub

Private Sub cmdCancellaSistemaEsterno_Click()
    Call CancellaSistemaEsterno
End Sub

Private Sub cmdCancellaProdottoEsterno_Click()
    Call CancellaProdottoEsterno
End Sub

Private Sub cmdAnnullaSistemiEsterni_Click()
    stato = STFSE_NESSUN_SISTEMAESTERNO
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAssociaArea_Click()
    Call AssociaArea
End Sub

Private Sub cmdConfermaSistemiEsterni_Click()
    Call ConfermaSistemiEsterni
End Sub

Private Sub ConfermaSistemiEsterni()
    Dim sql As String
    Dim n As Long
    
    Select Case stato
        Case STFSE_NUOVO_SISTEMAESTERNO
            sql = "INSERT INTO SISTEMAESTERNO (IDSISTEMAESTERNO, NOME, DESCRIZIONE, IDINTERFACCIASISTEMAESTERNO)" & _
                " VALUES (SQ_SISTEMAESTERNO.NEXTVAL, '" & txtNomeSistemaEsterno & "', '" & txtDescrizioneSistemaEsterno & "', " & idInterfacciaSistemaEsternoSelezionata & ")"
            SETAConnection.Execute sql, n, adCmdText
            stato = STFSE_NESSUN_SISTEMAESTERNO
        Case STFSE_MODIFICA_SISTEMAESTERNO
            sql = "UPDATE SISTEMAESTERNO SET NOME = '" & txtNomeSistemaEsterno.Text & "'"
            sql = sql & ", DESCRIZIONE = '" & txtDescrizioneSistemaEsterno.Text & "'"
            If idInterfacciaSistemaEsternoSelezionata <> idNessunElementoSelezionato Then
                sql = sql & ", IDINTERFACCIASISTEMAESTERNO = " & idInterfacciaSistemaEsternoSelezionata
            End If
            sql = sql & " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato
            SETAConnection.Execute sql, n, adCmdText
            stato = STFSE_NESSUN_SISTEMAESTERNO
        Case STFSE_CANCELLA_SISTEMAESTERNO
            If IsSistemaEsternoCancellabile Then
                sql = "DELETE FROM SISTEMAESTERNO"
                sql = sql & " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato
                SETAConnection.Execute sql, n, adCmdText
                stato = STFSE_NESSUN_SISTEMAESTERNO
            Else
                MsgBox "Il sistema esterno non � cancellabile: � necessario prima cancellare tutti i prodotti esterni che vi fanno riferimento"
            End If
        Case Else
            'Do Nothing
    End Select
    Call CaricaComboSistemiEsterni
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConfermaProdottiEsterni_Click()
    Call ConfermaProdottiEsterni
End Sub

Private Sub ConfermaProdottiEsterni()
    Dim sql As String
    Dim n As Long
    
    Select Case stato
        Case STFSE_NUOVO_PRODOTTOESTERNO
            idProdottoEsternoSelezionato = idProdottoSETASelezionato
            If LeggiDataOraAperturaCA = True Then
                If IsIdProdottoEsternoValido Then
                    sql = "INSERT INTO PRODOTTOESTERNO (IDPRODOTTOESTERNO, IDSISTEMAESTERNO, IDPRODOTTO, DATAORAAPERTURACONTROLLOACC, PRIMOINVIOEFFETTUATO)"
                    sql = sql & " VALUES (" & idProdottoEsternoSelezionato & ", " & idSistemaEsternoSelezionato & ", " & idProdottoSETASelezionato & ", "
                    sql = sql & IIf(dataOraAperturaCA = dataNulla, "NULL", SqlDateTimeValue(dataOraAperturaCA))
                    sql = sql & ", 0)"
                    SETAConnection.Execute sql, n, adCmdText
                    stato = STFSE_NESSUN_PRODOTTOESTERNO
                Else
                    MsgBox "Id prodotto esterno non valido"
                End If
            Else
                MsgBox "La data di apertura controllo accessi non � valida"
            End If
            stato = STFSE_NESSUN_PRODOTTOESTERNO
        Case STFSE_MODIFICA_PRODOTTOESTERNO
            If LeggiDataOraAperturaCA = True Then
                If idProdottoEsternoSelezionato <> idNessunElementoSelezionato Then
                    If idProdottoSETASelezionato <> idNessunElementoSelezionato Then
                        sql = "UPDATE PRODOTTOESTERNO SET IDPRODOTTO = " & idProdottoSETASelezionato
                        sql = sql & ", DATAORAAPERTURACONTROLLOACC = " & IIf(dataOraAperturaCA = dataNulla, "NULL", SqlDateTimeValue(dataOraAperturaCA))
                        sql = sql & " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato
                        sql = sql & " AND IDPRODOTTOESTERNO = " & idProdottoEsternoSelezionato
                        SETAConnection.Execute sql, n, adCmdText
                        stato = STFSE_NESSUN_PRODOTTOESTERNO
                    Else
                        MsgBox "Selezionare un prodotto SETA non valido"
                    End If
                Else
                    MsgBox "Id prodotto esterno non valido"
                End If
            Else
                MsgBox "La data di apertura controllo accessi non � valida"
            End If
            stato = STFSE_NESSUN_PRODOTTOESTERNO
        Case STFSE_CANCELLA_PRODOTTOESTERNO
            sql = "DELETE FROM PRODOTTOESTERNO"
            sql = sql & " WHERE IDPRODOTTOESTERNO = " & idProdottoEsternoSelezionato
            SETAConnection.Execute sql, n, adCmdText
            stato = STFSE_NESSUN_PRODOTTOESTERNO
        Case Else
            'Do Nothing
    End Select
    Call CaricaComboProdottiEsterni
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub NuovoSistemaEsterno()
    Dim sql As String
    
    stato = STFSE_NUOVO_SISTEMAESTERNO
    sql = "SELECT IDINTERFACCIASISTEMAESTERNO ID, NOME" & _
        " FROM INTERFACCIASISTEMAESTERNO" & _
        " ORDER BY NOME"
    idSistemaEsternoSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo2(cmbInterfacciaSistemaEsterno, sql, "NOME", False)
    AggiornaAbilitazioneControlli
End Sub

Private Sub NuovoProdottoEsterno()
    Dim sql As String
    
    stato = STFSE_NUOVO_PRODOTTOESTERNO
    sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " ORDER BY NOME"
    idProdottoEsternoSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo2(cmbOrganizzazioni, sql, "NOME", False)
    lstAree.Clear
    AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaSistemaEsterno()
    Dim sql As String
    
    stato = STFSE_MODIFICA_SISTEMAESTERNO
    sql = "SELECT IDINTERFACCIASISTEMAESTERNO ID, NOME" & _
        " FROM INTERFACCIASISTEMAESTERNO" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo2(cmbInterfacciaSistemaEsterno, sql, "NOME", False)
    AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaProdottoEsterno()
    Dim sql As String
    
    stato = STFSE_MODIFICA_PRODOTTOESTERNO
    sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo2(cmbOrganizzazioni, sql, "NOME", False)
    AggiornaAbilitazioneControlli
End Sub

Private Sub CancellaSistemaEsterno()
    Dim sql As String
    
    stato = STFSE_CANCELLA_SISTEMAESTERNO
    AggiornaAbilitazioneControlli
End Sub

Private Sub CancellaProdottoEsterno()
    Dim sql As String
    
    stato = STFSE_CANCELLA_PRODOTTOESTERNO
    AggiornaAbilitazioneControlli
End Sub

Private Sub InizializzaFrameSistemaEsterno(idSistemaEsternoSelezionato As Long, nome As String, descrizione As String, interfaccia As String)

    txtIdSistemaEsterno = idSistemaEsternoSelezionato
    txtNomeSistemaEsterno = nome
    txtDescrizioneSistemaEsterno = descrizione
    txtInterfacciaSistemaEsterno = interfaccia

End Sub

Private Sub cmbProdottiEsterni_Click()
    Call InizializzaProdottoEsterno
End Sub

Private Sub InizializzaProdottoEsterno()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    idProdottoEsternoSelezionato = cmbProdottiEsterni.ItemData(cmbProdottiEsterni.ListIndex)
    sql = "SELECT PE.IDPRODOTTOESTERNO, P.IDPRODOTTO, P.NOME, PE.DATAORAAPERTURACONTROLLOACC, PRIMOINVIOEFFETTUATO" & _
        " FROM PRODOTTOESTERNO PE, PRODOTTO P" & _
        " WHERE IDPRODOTTOESTERNO = " & idProdottoEsternoSelezionato & _
        " AND PE.IDPRODOTTO = P.IDPRODOTTO" & _
        " ORDER BY PE.DATAORAAPERTURACONTROLLOACC DESC"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        
        txtProdottoSETA = rec("IDPRODOTTO") & " - " & rec("NOME")
        idProdottoSETASelezionato = rec("IDPRODOTTO")
        dataOraAperturaCA = IIf(IsNull(rec("DATAORAAPERTURACONTROLLOACC")), dataNulla, rec("DATAORAAPERTURACONTROLLOACC"))
        Call InizializzaDataOraAperturaCA
    End If
    rec.Close
    
    Call CaricaListaAree
    
    stato = STFSE_PRODOTTOESTERNO_SELEZIONATO
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InizializzaDataOraAperturaCA()
    If dataOraAperturaCA = dataNulla Then
        'Do Nothing
    Else
        dtpDataAperturaCA.Value = dataOraAperturaCA
        txtOraAperturaCA.Text = StringaOraMinuti(CStr(Hour(dataOraAperturaCA)))
        txtMinutiAperturaCA.Text = StringaOraMinuti(CStr(Minute(dataOraAperturaCA)))
    End If
End Sub

Private Sub CaricaListaAree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    Dim s As String

    lstAree.Clear
    ' aree numerate e aree non numerate
    sql = "SELECT A.IDAREA, CODICE, IDAREAESTERNA" & _
        " FROM AREA A, PRODOTTO PR," & _
        " (" & _
        " SELECT IDAREA, IDAREAESTERNA FROM AREAESTERNA WHERE IDPRODOTTO = " & idProdottoSETASelezionato & _
        " ) AE" & _
        " WHERE PR.IDPRODOTTO = " & idProdottoSETASelezionato & _
        " AND PR.IDPIANTA = A.IDPIANTA" & _
        " AND A.IDTIPOAREA IN (2, 3)" & _
        " AND A.IDAREA = AE.IDAREA(+)" & _
        " ORDER BY CODICE"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            s = rec("IDAREA") & " - " & rec("CODICE")
            If Not IsNull(rec("IDAREAESTERNA")) Then
                s = s & ": " & rec("IDAREAESTERNA")
            End If
            lstAree.AddItem (s)
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
End Sub
    
Private Sub AssociaArea()
    Dim i As Long
    Dim s As String
    Dim sql As String
    Dim idAreaSETA As Long
    Dim idAreaEsterna As Long
    Dim n As Long
    
    For i = 1 To lstAree.ListCount
        If lstAree.Selected(i - 1) Then
            s = lstAree.Text
        End If
    Next i
    idAreaSETA = CLng(Left(s, InStr(s, " ") - 1))
    
    If IsNumeric(txtIdAreaEsterna) Then
        idAreaEsterna = CLng(txtIdAreaEsterna)
        
        sql = "DELETE FROM AREAESTERNA" & _
            " WHERE IDPRODOTTO = " & idProdottoSETASelezionato & _
            " AND IDAREA = " & idAreaSETA
        SETAConnection.Execute sql, n, adCmdText
    
        sql = "INSERT INTO AREAESTERNA (IDPRODOTTO, IDAREA, IDAREAESTERNA)" & _
            " VALUES (" & idProdottoSETASelezionato & ", " & idAreaSETA & ", " & idAreaEsterna & ")"
        SETAConnection.Execute sql, n, adCmdText
        
        Call CaricaListaAree
    Else
        MsgBox "Valore numerico non valido"
    End If
End Sub

Private Function LeggiDataOraAperturaCA() As Boolean
    Dim data As Date
    Dim ora As Integer
    Dim minuti As Integer

    LeggiDataOraAperturaCA = True
    
    If IsNull(dtpDataAperturaCA.Value) Then
        dataOraAperturaCA = dataNulla
    Else
        data = FormatDateTime(dtpDataAperturaCA.Value, vbShortDate)
        If txtOraAperturaCA.Text <> "" Then
            If IsCampoOraCorretto(txtOraAperturaCA) Then
                ora = CInt(Trim(txtOraAperturaCA.Text))
            Else
                LeggiDataOraAperturaCA = False
                MsgBox ("Il valore immesso sul campo Ora deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiAperturaCA.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiAperturaCA) Then
                minuti = CInt(Trim(txtMinutiAperturaCA.Text))
            Else
                LeggiDataOraAperturaCA = False
                MsgBox ("Il valore immesso sul campo Minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraAperturaCA = FormatDateTime(data & " " & ora & ":" & minuti, vbGeneralDate)
    End If
    
    If dataOraAperturaCA = dataNulla Then
        LeggiDataOraAperturaCA = False
    End If

End Function

Private Function IsSistemaEsternoCancellabile() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    sql = "SELECT COUNT(*) CONT" & _
        " FROM PRODOTTOESTERNO" & _
        " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = 0 Then
            IsSistemaEsternoCancellabile = True
        Else
            IsSistemaEsternoCancellabile = False
        End If
    End If
    rec.Close
End Function

Private Function IsIdProdottoEsternoValido() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    If idProdottoEsternoSelezionato = -1 Then
        IsIdProdottoEsternoValido = False
    Else
        sql = "SELECT COUNT(*) CONT" & _
            " FROM PRODOTTOESTERNO" & _
            " WHERE IDSISTEMAESTERNO = " & idSistemaEsternoSelezionato & _
            " AND IDPRODOTTOESTERNO = " & idProdottoEsternoSelezionato
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            If rec("CONT") = 0 Then
                IsIdProdottoEsternoValido = True
            Else
                IsIdProdottoEsternoValido = False
            End If
        End If
        rec.Close
    End If
End Function

