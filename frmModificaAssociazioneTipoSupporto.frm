VERSION 5.00
Begin VB.Form frmModificaAssociazioneTipoSupporto 
   Caption         =   "Prodotto"
   ClientHeight    =   6255
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11205
   LinkTopic       =   "Form1"
   ScaleHeight     =   6255
   ScaleWidth      =   11205
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbTipoSupportoDisponibili 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6435
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   4470
      Width           =   4575
   End
   Begin VB.ListBox lstTipoSupportoPerProdotto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1980
      Left            =   6435
      Style           =   1  'Checkbox
      TabIndex        =   6
      Top             =   1890
      Width           =   4515
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   6435
      TabIndex        =   2
      Top             =   5130
      Width           =   4575
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Esci"
         Height          =   435
         Left            =   3240
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ComboBox cmbOrganizzazioni 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1110
      Width           =   6165
   End
   Begin VB.ListBox lstProdotti 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4140
      Left            =   75
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   1890
      Width           =   6135
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Modifica associazione Tipo supporto per Prodotti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   75
      TabIndex        =   12
      Top             =   150
      Width           =   7275
   End
   Begin VB.Label lblLayoutDisponibili 
      Caption         =   "Tipo supporto disponibili"
      Height          =   195
      Left            =   6435
      TabIndex        =   11
      Top             =   4230
      Width           =   2295
   End
   Begin VB.Label lblLayoutPerProdotto 
      Caption         =   "Tipo supporto utilizzati nel Prodotto"
      Height          =   195
      Left            =   6435
      TabIndex        =   10
      Top             =   1650
      Width           =   3255
   End
   Begin VB.Label lblOrganizzazioni 
      Caption         =   "Organizzazioni attive"
      Height          =   195
      Left            =   75
      TabIndex        =   9
      Top             =   870
      Width           =   2295
   End
   Begin VB.Label lblProdottiNonScaduti 
      Caption         =   "Prodotti non scaduti"
      Height          =   195
      Left            =   75
      TabIndex        =   8
      Top             =   1650
      Width           =   3255
   End
End
Attribute VB_Name = "frmModificaAssociazioneTipoSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean
Private gestioneExitCode As ExitCodeEnum
Private idOrganizzazioneSelezionata As Long
Private listaProdotti As Collection
Private listaIdProdottiSelezionati As Collection
'Private listaLayoutPerProdotto As Collection
'Private idLayoutUtilizzatoSelezionato As Long
'Private idNuovoLayoutSelezionato As Long
Private listaTipoSupportoPerProdotto As Collection
Private idTipoSupportoUtilizzatoSelezionato As Long
Private idNuovoTipoSupportoSelezionato As Long


Public Sub Init()
    ResettaControlli
    CaricaValoriCmbOrganizzazioni
    AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = (idNuovoTipoSupportoSelezionato <> valoreLongNullo)
End Sub

Private Sub ResettaControlli()
    OrganizzazioneSelezionata_Reset
    ListaProdotti_Reset
    listaTipoSupportoPerProdotto_Reset
    NuovoTipoSupportoSelezionato_Reset
End Sub

Private Sub OrganizzazioneSelezionata_Reset()
    idOrganizzazioneSelezionata = valoreLongNullo
    cmbOrganizzazioni.Clear
End Sub

Private Sub ListaProdotti_Reset()
    Set listaIdProdottiSelezionati = Nothing
    Set listaIdProdottiSelezionati = New Collection
    idTipoSupportoUtilizzatoSelezionato = valoreLongNullo
    lstProdotti.Clear
End Sub

Private Sub listaTipoSupportoPerProdotto_Reset()
    Set listaTipoSupportoPerProdotto = Nothing
    Set listaIdProdottiSelezionati = New Collection
    idTipoSupportoUtilizzatoSelezionato = valoreLongNullo
    lstTipoSupportoPerProdotto.Clear
End Sub

Private Sub NuovoTipoSupportoSelezionato_Reset()
    idNuovoTipoSupportoSelezionato = valoreLongNullo
    cmbTipoSupportoDisponibili.Clear
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmbOrganizzazioni_Click()
    idOrganizzazioneSelezionata = cmbOrganizzazioni.ItemData(cmbOrganizzazioni.ListIndex)
    
    ListaProdotti_Reset
    listaTipoSupportoPerProdotto_Reset
    NuovoTipoSupportoSelezionato_Reset

    Call CaricaValoriLstProdotti
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub lstProdotti_ItemCheck(Item As Integer)
    Dim internalEventOld As Boolean
    
    If Not internalEvent Then
        internalEventOld = internalEvent
        internalEvent = True
        
        If lstProdotti.Selected(Item) Then
            Call listaIdProdottiSelezionati.Add(lstProdotti.ItemData(Item), ChiaveId(CLng(Item)))
        Else
            Call listaIdProdottiSelezionati.Remove(ChiaveId(CLng(Item)))
        End If
        
        If listaIdProdottiSelezionati.count > 0 Then
            CaricaValoriLstTipoSupportoPerProdotto
        Else
            listaTipoSupportoPerProdotto_Reset
        End If
        NuovoTipoSupportoSelezionato_Reset
        AggiornaAbilitazioneControlli
    
        internalEvent = internalEventOld
    End If
End Sub
   
Private Sub lstTipoSupportoPerProdotto_ItemCheck(Item As Integer)
    Dim internalEventOld As Boolean
    
    If Not internalEvent Then
        internalEventOld = internalEvent
        internalEvent = True
        
        Call ListBox_SelezioneSingola(lstTipoSupportoPerProdotto, Item)
        idTipoSupportoUtilizzatoSelezionato = lstTipoSupportoPerProdotto.ItemData(Item)
    
        NuovoTipoSupportoSelezionato_Reset
        CaricaValoriCmbTipoSupportoDisponibili
        AggiornaAbilitazioneControlli
        
        internalEvent = internalEventOld
    End If
End Sub

Private Sub cmbTipoSupportoDisponibili_Click()
    idNuovoTipoSupportoSelezionato = cmbTipoSupportoDisponibili.ItemData(cmbTipoSupportoDisponibili.ListIndex)
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    ResettaControlli
    CaricaValoriCmbOrganizzazioni
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim esito As String
    
    Call frmMessaggio.Visualizza("ConfermaModificaTipoSupportoProdotti")
    
    If (frmMessaggio.exitCode = EC_CONFERMA) Then
        esito = ModificaAssociazione
        
        If esito = "" Then
            'OK
            Call frmMessaggio.Visualizza("NotificaModificaDati")
            Call ScriviLog(CCTA_MODIFICA, CCDA_TIPO_SUPPORTO, CCDA_PRODOTTO, "IDNUOVOTIPOSUPPORTO = " & idNuovoTipoSupportoSelezionato, idNuovoTipoSupportoSelezionato)
            ResettaControlli
            CaricaValoriCmbOrganizzazioni
            AggiornaAbilitazioneControlli
        Else
            'Errore
            Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        End If
    End If
End Sub

Private Function ModificaAssociazione() As String
On Error GoTo gestioneErrori

    Dim sql As String
    Dim stringaIdProdottiSelezionati As String
    
    stringaIdProdottiSelezionati = CollezioneToStringa(listaIdProdottiSelezionati, ",")
    
    '*** Query da testare ***
    sql = "UPDATE UTILIZZOLAYOUTSUPPORTOCPV SET IDTIPOSUPPORTO = " & idNuovoTipoSupportoSelezionato & _
        " WHERE IDTIPOSUPPORTO = " & idTipoSupportoUtilizzatoSelezionato & _
        " AND IDTARIFFA IN" & _
        " (SELECT IDTARIFFA" & _
        " FROM TARIFFA" & _
        " WHERE IDPRODOTTO IN (" & stringaIdProdottiSelezionati & "))"
    
    Call ApriConnessioneBD
    SETAConnection.Execute sql, , adCmdText
    Call ChiudiConnessioneBD

    ModificaAssociazione = ""
gestioneErrori:
    ModificaAssociazione = Err.Description
End Function

Private Sub CaricaValoriCmbOrganizzazioni()
    Dim sql As String
    
    sql = "SELECT IDORGANIZZAZIONE AS ""ID"", NOME, CODICETERMINALELOTTO" & _
        " FROM ORGANIZZAZIONE" & _
        " WHERE IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVA & _
        " ORDER BY NOME"

    Call CaricaValoriCombo2(cmbOrganizzazioni, sql, "NOME", False)
End Sub

Private Sub CaricaValoriLstProdotti()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim prodottoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaProdotti = New Collection
    
    sql = "SELECT P.IDPRODOTTO AS ""ID"", NOME, MIN(R.DATAORAINIZIO) AS DATAORAPRIMARAPPRESENTAZIONE" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
        " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " AND R.DATAORAINIZIO > SYSDATE" & _
        " GROUP BY P.IDPRODOTTO, NOME" & _
        " ORDER BY NOME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set prodottoCorrente = New clsElementoLista
            prodottoCorrente.nomeElementoLista = rec("NOME")
            prodottoCorrente.idElementoLista = rec("ID").Value
            prodottoCorrente.descrizioneElementoLista = rec("NOME") & " - " & rec("DATAORAPRIMARAPPRESENTAZIONE")
            chiaveTipoSupporto = ChiaveId(prodottoCorrente.idElementoLista)
            Call listaProdotti.Add(prodottoCorrente, chiaveTipoSupporto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Call LstProdotti_Init
    
End Sub

Private Sub CaricaValoriLstTipoSupportoPerProdotto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim tipoSupportoCorrente As clsElementoLista
    Dim stringaIdProdottiSelezionati As String
    
    stringaIdProdottiSelezionati = CollezioneToStringa(listaIdProdottiSelezionati, ",")
        
        If stringaIdProdottiSelezionati <> "" Then
        Call ApriConnessioneBD
        
        Set listaTipoSupportoPerProdotto = New Collection
        
        sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO AS ""ID"", TS.CODICE || ' ' || TS.NOME AS CODICE" & _
            " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, TIPOSUPPORTO TS" & _
            " WHERE T.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
            " AND T.IDTARIFFA = ULS.IDTARIFFA" & _
            " AND ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO"
    
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set tipoSupportoCorrente = New clsElementoLista
                tipoSupportoCorrente.nomeElementoLista = rec("CODICE")
                tipoSupportoCorrente.codiceElementoLista = rec("CODICE")
                tipoSupportoCorrente.idElementoLista = rec("ID").Value
                tipoSupportoCorrente.descrizioneElementoLista = rec("CODICE")
                chiaveTipoSupporto = ChiaveId(tipoSupportoCorrente.idElementoLista)
                Call listaTipoSupportoPerProdotto.Add(tipoSupportoCorrente, chiaveTipoSupporto)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ChiudiConnessioneBD
        Call LstTipoSupportoPerProdotto_Init
    End If
End Sub

Private Sub CaricaValoriCmbTipoSupportoDisponibili()
    Dim sql As String
    
    '--- SQL ---
    'select idlayoutsupport, codice, nome
    'from layoutsupporto ls, organiz_tiposup_layoutsup otl
    'Where ls.idLayoutSupporto = otl.idLayoutSupporto
    'and utl.idorganizzazione = IDORG
    'and utl.idtiposupporto in
    '(
    'select idtiposupporto
    'From
    '(
    '-- tipo supporto utilizzati nei prodotti
    'select idtiposupporto
    'from utilizzolayoutsupporto uls, tariffa t
    'Where uls.idLayoutSupporto = IDLAY_VECCHIO
    'and uls.idtariffa = t.idtariffa
    'and t.idprodotto in (LISTA_IDPRODOTTO)
    ') ts_utilizzati
    'Where Exists
    '(
    'select idtiposupporto
    'From organiz_tiposup_layoutsup
    'Where idOrganizzazione = idOrg
    'and idlayoutsupporto = IDLAY_NUOVO
    'and idtiposupporto = ts_utilizzati.idtiposupporto
    ')
    ')
    
'    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO AS ID, TS.CODICE, TS.NOME" & _
'        " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
'        " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
'        " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'        " AND OTL.IDTIPOSUPPORTO IN" & _
'        " (" & _
'        " SELECT DISTINCT IDTIPOSUPPORTO" & _
'        " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T" & _
'        " WHERE ULS.IDTARIFFA = T.IDTARIFFA" & _
'        " AND ULS.IDTIPOSUPPORTO = " & idTipoSupportoUtilizzatoSelezionato & _
'        " AND T.IDPRODOTTO IN (" & CollezioneToStringa(listaIdProdottiSelezionati, ",") & "))" & _
'        " ORDER BY TS.NOME"
    
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO AS ID, TS.CODICE, TS.NOME" & _
        " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
        " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
        " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY TS.CODICE"
        
    Call CaricaValoriCombo2(cmbTipoSupportoDisponibili, sql, "NOME", False)
End Sub

Private Sub LstProdotti_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstProdotti.Clear

    If Not (listaProdotti Is Nothing) Then
        i = 1
        For Each tip In listaProdotti
            lstProdotti.AddItem tip.descrizioneElementoLista
            lstProdotti.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub LstTipoSupportoPerProdotto_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTipoSupportoPerProdotto.Clear

    If Not (listaTipoSupportoPerProdotto Is Nothing) Then
        i = 1
        For Each tip In listaTipoSupportoPerProdotto
            lstTipoSupportoPerProdotto.AddItem tip.descrizioneElementoLista
            lstTipoSupportoPerProdotto.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub





