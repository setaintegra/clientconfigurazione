VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmConfigurazioneOrganizzazionePerfezionatori 
   Caption         =   "Organizzazione"
   ClientHeight    =   8790
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   ScaleHeight     =   8790
   ScaleWidth      =   11910
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   11
      Top             =   8100
      Width           =   1155
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   10
      Top             =   240
      Width           =   3255
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   7
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   2
      Top             =   4200
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   1
      Top             =   5460
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   6120
      Width           =   3795
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneOrganizzazionePerfezionatori 
      Height          =   330
      Left            =   240
      Top             =   3420
      Visible         =   0   'False
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adcConfigurazioneOrganizzazionePerfezionatori"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneOrganizzazionePerfezionatori 
      Height          =   3255
      Left            =   120
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   5741
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   18
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Perfezionatori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   5775
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   16
      Top             =   3960
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   3960
      Width           =   1815
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   5220
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   5880
      Width           =   1575
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazionePerfezionatori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idOperatoreSelezionato As Long
Private isRecordEditabile As Boolean
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idTipoProtCausaleSelezionata As Long
Private simboloSuPiantaRecordSelezionato As String

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazioneOrganizzazionePerfezionatori.Caption = "PERFEZIONATORI CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazionePerfezionatori.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazionePerfezionatori.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazionePerfezionatori.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim causaNonEditabilita As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        "; IDCAUSALEPROTEZIONE = " & idRecordSelezionato
        
    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoOrganizzazione = TSO_ATTIVA Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If ValoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_CAUSALE_DI_PROTEZIONE, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazionePerfezionatori_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazionePerfezionatori_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_CAUSALE_DI_PROTEZIONE, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazionePerfezionatori_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazionePerfezionatori_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_ORGANIZZAZIONE, CCDA_CAUSALE_DI_PROTEZIONE, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazionePerfezionatori_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazionePerfezionatori_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_CAUSALE_DI_PROTEZIONE, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazionePerfezionatori_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneOrganizzazionePerfezionatori_Init
                    End Select
                Else
                    Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", "la Causale di protezione selezionata č in lavorazione da parte di un altro utente")
                End If
                Call AggiornaAbilitazioneControlli
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT OP.IDOPERATORE ID, USERNAME" & _
        " FROM OPERATORE OP, OPERATORE_ORGANIZZAZIONE ORG" & _
        " WHERE OP.IDOPERATORE = ORG.IDOPERATORE" & _
        " AND ORG.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call RilevaTipoProtezioneGiāAssociata
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    isRecordEditabile = True
    idOperatoreSelezionato = idNessunElementoSelezionato
'    sql = "SELECT OP.IDOPERATORE ID, USERNAME" & _
'        " FROM OPERATORE OP, OPERATORE_ORGANIZZAZIONE ORG" & _
'        " WHERE OP.IDOPERATORE = ORG.IDOPERATORE" & _
'        " AND ORG.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = "SELECT OP.IDOPERATORE ID, USERNAME" & _
        " FROM OPERATORE OP, OPERATORE_ORGANIZZAZIONE ORG" & _
        " WHERE OP.IDOPERATORE = ORG.IDOPERATORE" & _
        " AND OP.IDPUNTOVENDITA IS NULL" & _
        " AND ORG.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY USERNAME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneOrganizzazionePerfezionatori.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    'isRecordEditabile = True
    'idOperatoreSelezionato = idNessunElementoSelezionato

    'sql = "SELECT OP.IDOPERATORE ID, USERNAME" & _
    '    " FROM OPERATORE OP, OPERATORE_ORGANIZZAZIONE ORG" & _
    '    " WHERE OP.IDOPERATORE = ORG.IDOPERATORE" & _
    '    " AND OP.IDPUNTOVENDITA IS NULL" & _
    '    " AND ORG.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
    '    " ORDER BY USERNAME"
    'Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    'Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    'Call AggiornaAbilitazioneControlli
    'Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    
    'Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneOrganizzazionePerfezionatori_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneOrganizzazionePerfezionatori_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneOrganizzazionePerfezionatori_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneOrganizzazionePerfezionatori.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneOrganizzazionePerfezionatori_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneOrganizzazionePerfezionatori
    
    sql = "SELECT IDPERFEZIONATORE AS ""ID"", NOME AS ""Nome""," & _
        " DESCRIZIONE AS ""Descrizione""" & _
        " FROM PERFEZIONATORE P" & _
        " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY NOME"
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneOrganizzazionePerfezionatori.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim max As Long
    Dim i As Integer
    Dim idNuovoPerfezionatore As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    idNuovoPerfezionatore = OttieniIdentificatoreDaSequenza("SQ_PERFEZIONATORE")
    sql = "INSERT INTO PERFEZIONATORE (IDPERFEZIONATORE, NOME," & _
        " DESCRIZIONE, IDORGANIZZAZIONE)" & _
        " VALUES (" & _
        idNuovoPerfezionatore & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        idOrganizzazioneSelezionata & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoPerfezionatore)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

On Error GoTo gestioneErrori
    
    isRecordEditabile = True
    sql = "SELECT NOME, DESCRIZIONE" & _
        " FROM PERFEZIONATORE" & _
        " WHERE IDPERFEZIONATORE = " & idRecordSelezionato
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    
    internalEvent = internalEventOld

End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
 
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
'    sql = "UPDATE CAUSALEPROTEZIONE SET" & _
'        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
'        " SIMBOLOSUPIANTA = " & SqlStringValue(simboloSuPiantaRecordSelezionato) & "," & _
'        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
'        " IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & "," & _
'        " PROPAGABILEAPRODOTTICORRELATI = " & VB_VERO & "," & _
'        " IDOPERATORE = " & IIf(idOperatoreSelezionato = idNessunElementoSelezionato, "NULL", idOperatoreSelezionato) & _
'        " WHERE IDCAUSALEPROTEZIONE = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("CAUSALEPROTEZIONE", "IDCAUSALEPROTEZIONE", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

    Call ApriConnessioneBD


On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    sql = "UPDATE PERFEZIONATORE " & _
        " SET NOME = " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        " DESCRIZIONE = " & _
        SqlStringValue(descrizioneRecordSelezionato) & _
        " WHERE IDPERFEZIONATORE = " & idRecordSelezionato
        
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("CAUSALEPROTEZIONE", "IDCAUSALEPROTEZIONE", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    SETAConnection.CommitTrans

    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazioneOrganizzazionePerfezionatori_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneOrganizzazionePerfezionatori
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
'    numeroCampi = 5
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtSimboloSuPianta_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDPERFEZIONATORE <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If ViolataUnicitā("PERFEZIONATORE", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
            " č giā presente in DB per la stessa organizzazione;")
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

'Private Sub CreaListaCampiValoriUnici()
'
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'    Call listaCampiValoriUnici.Add("SIMBOLOSUPIANTA = " & SqlStringValue(simboloSuPiantaRecordSelezionato))
'End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
    sql = "DELETE FROM PERFEZIONATORE" & _
        " WHERE IDPERFEZIONATORE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD

End Sub

Private Sub optEsclusiva_Click()
End Sub

Private Sub optCondivisa_Click()
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Private Sub RilevaTipoProtezioneGiāAssociata()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numRec As Long
    
End Sub



