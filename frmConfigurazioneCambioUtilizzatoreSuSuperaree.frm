VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneProdottoStampeAggiuntive 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   31
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   30
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraProgressivo 
      Caption         =   "Progressivo"
      Height          =   1275
      Left            =   4080
      TabIndex        =   8
      Top             =   5040
      Width           =   1095
      Begin VB.OptionButton opt3 
         Caption         =   "3"
         Height          =   255
         Left            =   180
         TabIndex        =   29
         Top             =   900
         Width           =   795
      End
      Begin VB.OptionButton opt2 
         Caption         =   "2"
         Height          =   255
         Left            =   180
         TabIndex        =   28
         Top             =   600
         Width           =   795
      End
      Begin VB.OptionButton opt1 
         Caption         =   "1"
         Height          =   255
         Left            =   180
         TabIndex        =   27
         Top             =   300
         Width           =   795
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoStampeAgiuntive 
      Height          =   330
      Left            =   6480
      Top             =   180
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   23
      Top             =   3780
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtCoordinataY 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1260
      MaxLength       =   3
      TabIndex        =   7
      Top             =   6540
      Width           =   555
   End
   Begin VB.TextBox txtCoordinataX 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   3
      TabIndex        =   6
      Top             =   6540
      Width           =   555
   End
   Begin VB.ComboBox cmbFormatoStampaAggiuntiva 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   5820
      Width           =   3615
   End
   Begin VB.TextBox txtValore 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   5100
      Width           =   3315
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   15
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   14
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSComCtl2.UpDown updCoordinataX 
      Height          =   315
      Left            =   660
      TabIndex        =   21
      Top             =   6540
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   556
      _Version        =   393216
      BuddyControl    =   "txtCoordinataX"
      BuddyDispid     =   196622
      OrigLeft        =   6060
      OrigTop         =   6120
      OrigRight       =   6300
      OrigBottom      =   6495
      Max             =   255
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin MSComCtl2.UpDown updCoordinataY 
      Height          =   315
      Left            =   1800
      TabIndex        =   22
      Top             =   6540
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   556
      _Version        =   393216
      BuddyControl    =   "txtCoordinataY"
      BuddyDispid     =   196621
      OrigLeft        =   6060
      OrigTop         =   6120
      OrigRight       =   6300
      OrigBottom      =   6495
      Max             =   255
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoStampeAgiuntive 
      Bindings        =   "frmConfigurazioneProdottoStampeAggiuntive.frx":0000
      Height          =   2835
      Left            =   120
      TabIndex        =   24
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   5001
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   33
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   32
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   3540
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   25
      Top             =   3540
      Width           =   2775
   End
   Begin VB.Label lblCoordinataY 
      Caption         =   "coordinata Y"
      Height          =   255
      Left            =   1260
      TabIndex        =   20
      Top             =   6300
      Width           =   1275
   End
   Begin VB.Label lblCoordinataX 
      Caption         =   "coordinata X"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   6300
      Width           =   1515
   End
   Begin VB.Label lblFormatoStampaAggiuntiva 
      Caption         =   "Formato"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   5580
      Width           =   1695
   End
   Begin VB.Label lblValore 
      Caption         =   "Valore"
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   4860
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Stampe Aggiuntive"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoStampeAggiuntive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private isRecordEditabile As Boolean
Private valoreRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private coordinataXRecordSelezionato As Integer
Private coordinataYRecordSelezionato As Integer
Private progressivoRecordSelezionato As Integer
Private nomeOrganizzazioneSelezionata As String
Private rateo As Long
Private isProdottoAttivoSuTL As Boolean
Private idFormatoStampaAggiuntiva As Long
Private stampeAggiuntiveInseribili As Boolean
Private idClasseProdottoSelezionata As ClasseProdottoEnum

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    dgrConfigurazioneProdottoStampeAgiuntive.Caption = "STAMPE AGGIUNTIVE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoStampeAgiuntive.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtValore.Text = ""
        txtCoordinataX.Text = ""
        txtCoordinataY.Text = ""
        opt1.Value = False
        opt2.Value = False
        opt3.Value = False
        Call cmbFormatoStampaAggiuntiva.Clear
        txtValore.Enabled = False
        txtCoordinataX.Enabled = False
        txtCoordinataY.Enabled = False
        opt1.Enabled = False
        opt2.Enabled = False
        opt3.Enabled = False
        fraProgressivo.Enabled = False
        cmbFormatoStampaAggiuntiva.Enabled = False
        updCoordinataX.Enabled = False
        updCoordinataY.Enabled = False
        lblValore.Enabled = False
        lblCoordinataX.Enabled = False
        lblCoordinataY.Enabled = False
        lblFormatoStampaAggiuntiva.Enabled = False
        cmdInserisciNuovo.Enabled = stampeAggiuntiveInseribili
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato) And _
            stampeAggiuntiveInseribili
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoStampeAgiuntive.Enabled = False
        txtValore.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCoordinataX.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCoordinataY.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        opt1.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or _
            gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE)
        opt2.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or _
            gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE)
        opt3.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or _
            gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE)
        fraProgressivo.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or _
            gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE)
        cmbFormatoStampaAggiuntiva.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        updCoordinataX.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        updCoordinataY.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblValore.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCoordinataX.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCoordinataY.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblFormatoStampaAggiuntiva.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtValore.Text) <> "" And _
            Trim(txtCoordinataX.Text) <> "" And _
            Trim(txtCoordinataY.Text) <> "" And _
            cmbFormatoStampaAggiuntiva.ListIndex <> idNessunElementoSelezionato And _
            (opt1.Value = True Or opt2.Value = True Or opt3.Value = True)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
                
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoStampeAgiuntive.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtValore.Text = ""
        txtCoordinataX.Text = ""
        txtCoordinataY.Text = ""
        opt1.Value = False
        opt2.Value = False
        opt3.Value = False
        Call cmbFormatoStampaAggiuntiva.Clear
        txtValore.Enabled = False
        txtCoordinataX.Enabled = False
        txtCoordinataY.Enabled = False
        opt1.Enabled = False
        opt2.Enabled = False
        opt3.Enabled = False
        fraProgressivo.Enabled = False
        cmbFormatoStampaAggiuntiva.Enabled = False
        updCoordinataX.Enabled = False
        updCoordinataY.Enabled = False
        lblValore.Enabled = False
        lblCoordinataX.Enabled = False
        lblCoordinataY.Enabled = False
        lblFormatoStampaAggiuntiva.Enabled = False
        cmdInserisciNuovo.Enabled = stampeAggiuntiveInseribili
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato) And _
            stampeAggiuntiveInseribili
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneDurateRiservazioni
End Sub

Private Sub cmbFormatoStampaAggiuntiva_Click()
    If Not internalEvent Then
        idFormatoStampaAggiuntiva = cmbFormatoStampaAggiuntiva.ItemData(cmbFormatoStampaAggiuntiva.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
    
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If ValoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_STAMPA_AGGIUNTIVA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoStampeAgiuntive_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoStampeAgiuntive_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_STAMPA_AGGIUNTIVA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoStampeAgiuntive_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoStampeAgiuntive_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_STAMPA_AGGIUNTIVA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoStampeAgiuntive_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoStampeAgiuntive_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_STAMPA_AGGIUNTIVA, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoStampeAgiuntive_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneProdottoStampeAgiuntive_Init
                    End Select
                End If
                stampeAggiuntiveInseribili = (NumeroStampeAggiuntivePerProdotto < 3)
                Call AggiornaAbilitazioneControlli
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDFORMATOSTAMPAAGGIUNTIVA ID, NOME" & _
        " FROM FORMATOSTAMPAAGGIUNTIVA WHERE IDFORMATOSTAMPAAGGIUNTIVA > 0" & _
        " ORDER BY ID"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbFormatoStampaAggiuntiva, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    sql = "SELECT IDFORMATOSTAMPAAGGIUNTIVA ID, NOME" & _
        " FROM FORMATOSTAMPAAGGIUNTIVA WHERE IDFORMATOSTAMPAAGGIUNTIVA > 0" & _
        " ORDER BY ID"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbFormatoStampaAggiuntiva, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    sql = "SELECT IDFORMATOSTAMPAAGGIUNTIVA ID, NOME" & _
        " FROM FORMATOSTAMPAAGGIUNTIVA WHERE IDFORMATOSTAMPAAGGIUNTIVA > 0" & _
        " ORDER BY ID"
    isRecordEditabile = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbFormatoStampaAggiuntiva, sql, "NOME")
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoStampeAgiuntive.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDFORMATOSTAMPAAGGIUNTIVA ID, NOME" & _
        " FROM FORMATOSTAMPAAGGIUNTIVA WHERE IDFORMATOSTAMPAAGGIUNTIVA > 0" & _
        " ORDER BY ID"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbFormatoStampaAggiuntiva, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneProdottoStampeAgiuntive_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    stampeAggiuntiveInseribili = (NumeroStampeAggiuntivePerProdotto < 3)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoStampeAgiuntive_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoStampeAgiuntive_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoStampeAgiuntive.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        If Not IsNull(rec("ID")) Then
            idRecordSelezionato = rec("ID").Value
        Else
            idRecordSelezionato = idNessunElementoSelezionato
        End If
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoStampeAgiuntive_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazioneProdottoStampeAgiuntive
        
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT IDSTAMPAAGGIUNTIVA ID, P.PROGRESSIVO ""Progressivo"","
'        sql = sql & " VALORE ""Valore"", COORDINATAX ""Coord. X"", COORDINATAY ""Coord. Y"", F.NOME ""Formato"""
'        sql = sql & " FROM STAMPAAGGIUNTIVA S,"
'        sql = sql & " ("
'        sql = sql & " SELECT 1 PROGRESSIVO FROM DUAL"
'        sql = sql & " UNION"
'        sql = sql & " SELECT 2 PROGRESSIVO FROM DUAL"
'        sql = sql & " UNION"
'        sql = sql & " SELECT 3 PROGRESSIVO FROM DUAL"
'        sql = sql & " ) P, FORMATOSTAMPAAGGIUNTIVA F"
'        sql = sql & " WHERE S.PROGRESSIVO(+) = P.PROGRESSIVO"
'        sql = sql & " AND S.IDPRODOTTO(+) = " & idProdottoSelezionato
'        sql = sql & " AND S.IDFORMATOSTAMPAAGGIUNTIVA = F.IDFORMATOSTAMPAAGGIUNTIVA(+)"
'        sql = sql & " AND (S.IDTIPOSTATORECORD IS NULL"
'        sql = sql & " OR S.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'        sql = sql & " ORDER BY ""Progressivo"""
'    Else
        sql = "SELECT IDSTAMPAAGGIUNTIVA ID, P.PROGRESSIVO ""Progressivo"","
        sql = sql & " VALORE ""Valore"", COORDINATAX ""Coord. X"", COORDINATAY ""Coord. Y"", F.NOME ""Formato"""
        sql = sql & " FROM STAMPAAGGIUNTIVA S,"
        sql = sql & " ("
        sql = sql & " SELECT 1 PROGRESSIVO FROM DUAL"
        sql = sql & " UNION"
        sql = sql & " SELECT 2 PROGRESSIVO FROM DUAL"
        sql = sql & " UNION"
        sql = sql & " SELECT 3 PROGRESSIVO FROM DUAL"
        sql = sql & " ) P, FORMATOSTAMPAAGGIUNTIVA F"
        sql = sql & " WHERE S.PROGRESSIVO(+) = P.PROGRESSIVO"
        sql = sql & " AND S.IDPRODOTTO(+) = " & idProdottoSelezionato
        sql = sql & " AND S.IDFORMATOSTAMPAAGGIUNTIVA = F.IDFORMATOSTAMPAAGGIUNTIVA(+)"
        sql = sql & " ORDER BY ""Progressivo"""
'    End If
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idNuovaStampaAggiuntiva As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA TARIFFA
    idNuovaStampaAggiuntiva = OttieniIdentificatoreDaSequenza("SQ_STAMPAAGGIUNTIVA")
    sql = "INSERT INTO STAMPAAGGIUNTIVA (IDSTAMPAAGGIUNTIVA, IDPRODOTTO, PROGRESSIVO," & _
        " IDFORMATOSTAMPAAGGIUNTIVA, COORDINATAX, COORDINATAY, VALORE)" & _
        " VALUES (" & _
        idNuovaStampaAggiuntiva & ", " & _
        idProdottoSelezionato & ", " & _
        progressivoRecordSelezionato & ", " & _
        idFormatoStampaAggiuntiva & ", " & _
        coordinataXRecordSelezionato & ", " & _
        coordinataYRecordSelezionato & ", " & _
        SqlStringValue(valoreRecordSelezionato) & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("STAMPAAGGIUNTIVA", "IDSTAMPAAGGIUNTIVA", idNuovaStampaAggiuntiva, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaStampaAggiuntiva)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT PROGRESSIVO, IDFORMATOSTAMPAAGGIUNTIVA," & _
'            " COORDINATAX, COORDINATAY, VALORE," & _
'            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
'            " FROM STAMPAAGGIUNTIVA" & _
'            " WHERE IDSTAMPAAGGIUNTIVA = " & idRecordSelezionato
'    Else
        sql = "SELECT PROGRESSIVO, IDFORMATOSTAMPAAGGIUNTIVA," & _
            " COORDINATAX, COORDINATAY, VALORE" & _
            " FROM STAMPAAGGIUNTIVA" & _
            " WHERE IDSTAMPAAGGIUNTIVA = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        valoreRecordSelezionato = IIf(IsNull(rec("VALORE")), "", rec("VALORE"))
        progressivoRecordSelezionato = rec("PROGRESSIVO").Value
        idFormatoStampaAggiuntiva = rec("IDFORMATOSTAMPAAGGIUNTIVA").Value
        coordinataXRecordSelezionato = rec("COORDINATAX").Value
        coordinataYRecordSelezionato = rec("COORDINATAY").Value
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtValore.Text = ""
    txtCoordinataX.Text = ""
    txtCoordinataY.Text = ""
    txtValore.Text = valoreRecordSelezionato
    txtCoordinataX.Text = coordinataXRecordSelezionato
    txtCoordinataY.Text = coordinataYRecordSelezionato
    Select Case progressivoRecordSelezionato
        Case Is = 1
            opt1.Value = True
            opt2.Value = False
            opt3.Value = False
        Case Is = 2
            opt1.Value = False
            opt2.Value = True
            opt3.Value = False
        Case Is = 3
            opt1.Value = False
            opt2.Value = False
            opt3.Value = True
    End Select
    Call SelezionaElementoSuCombo(cmbFormatoStampaAggiuntiva, idFormatoStampaAggiuntiva)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    sql = "UPDATE STAMPAAGGIUNTIVA SET" & _
        " VALORE = " & SqlStringValue(valoreRecordSelezionato) & "," & _
        " COORDINATAX = " & coordinataXRecordSelezionato & "," & _
        " COORDINATAY = " & coordinataYRecordSelezionato & "," & _
        " IDFORMATOSTAMPAAGGIUNTIVA = " & idFormatoStampaAggiuntiva & "," & _
        " PROGRESSIVO = " & progressivoRecordSelezionato & _
        " WHERE IDSTAMPAAGGIUNTIVA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("STAMPAAGGIUNTIVA", "IDSTAMPAAGGIUNTIVA", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazioneProdottoStampeAgiuntive_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoStampeAgiuntive
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub opt1_Click()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub opt2_Click()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub


Private Sub opt3_Click()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub


Private Sub txtCoordinataX_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtCoordinataY_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub


Private Sub txtValore_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    
    Set listaNonConformitā = New Collection
    condizioneSql = ""
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDSTAMPAAGGIUNTIVA <> " & idRecordSelezionato
    End If
    
    valoreRecordSelezionato = Trim(txtValore.Text)
    If IsCampoInteroCorretto(txtCoordinataX) Then
        coordinataXRecordSelezionato = CInt(Trim(txtCoordinataX.Text))
        If coordinataXRecordSelezionato <= 0 Or coordinataXRecordSelezionato > 255 Then
'            Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata X", "con valori compresi tra 1 e 255")
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata X deve essere numerico di tipo intero e compreso tra 1 e 255;")
        End If
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata X", "con valori compresi tra 1 e 255")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata X deve essere numerico di tipo intero e compreso tra 1 e 255;")
    End If
    If IsCampoInteroCorretto(txtCoordinataY) Then
        coordinataYRecordSelezionato = CInt(Trim(txtCoordinataY.Text))
        If coordinataYRecordSelezionato <= 0 Or coordinataYRecordSelezionato > 255 Then
'            Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata Y", "con valori compresi tra 1 e 255")
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata Y deve essere numerico di tipo intero e compreso tra 1 e 255;")
        End If
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata Y", "con valori compresi tra 1 e 255")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata Y deve essere numerico di tipo intero e compreso tra 1 e 255;")
    End If
    If opt1.Value = True Then
        progressivoRecordSelezionato = 1
    ElseIf opt2.Value = True Then
        progressivoRecordSelezionato = 2
    ElseIf opt3.Value = True Then
        progressivoRecordSelezionato = 3
    End If
    If ViolataUnicitā("STAMPAAGGIUNTIVA", "PROGRESSIVO = " & progressivoRecordSelezionato, "IDPRODOTTO = " & idProdottoSelezionato, _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore progressivo = " & progressivoRecordSelezionato & _
            " č giā presente in DB per lo stesso Prodotto;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoDateOperazioni.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoDateOperazioni.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM STAMPAAGGIUNTIVA WHERE IDSTAMPAAGGIUNTIVA = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            Call AggiornaParametriSessioneSuRecord("STAMPAAGGIUNTIVA", "IDSTAMPAAGGIUNTIVA", idRecordSelezionato, TSR_ELIMINATO)
'        End If
'    Else
        sql = "DELETE FROM STAMPAAGGIUNTIVA WHERE IDSTAMPAAGGIUNTIVA = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdStagioneSelezionata(idS As Long)
    idStagioneSelezionata = idS
End Sub

Private Function NumeroStampeAggiuntivePerProdotto() As Integer
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Integer
    
    Call ApriConnessioneBD
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT COUNT(IDSTAMPAAGGIUNTIVA) CONT" & _
'            " FROM STAMPAAGGIUNTIVA WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL" & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT COUNT(IDSTAMPAAGGIUNTIVA) CONT" & _
            " FROM STAMPAAGGIUNTIVA WHERE IDPRODOTTO = " & idProdottoSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    NumeroStampeAggiuntivePerProdotto = cont
End Function

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Private Sub CaricaFormConfigurazioneDurateRiservazioni()
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoDurateRiservazioni.Init
End Sub

