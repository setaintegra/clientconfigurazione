VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazionePiantaSuperarea 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   10410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10410
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Gestione disponibilitā"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   41
      Top             =   8400
      Width           =   11415
      Begin VB.TextBox txtSoglia2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10560
         MaxLength       =   21
         TabIndex        =   44
         Top             =   280
         Width           =   795
      End
      Begin VB.TextBox txtSoglia1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4920
         MaxLength       =   21
         TabIndex        =   42
         Top             =   280
         Width           =   795
      End
      Begin VB.Label lblSoglia2 
         Caption         =   "Numero di posti al sopra del quale c'č ampia disponibilitā (verde)"
         Height          =   255
         Left            =   6000
         TabIndex        =   45
         Top             =   360
         Width           =   4575
      End
      Begin VB.Label lblSoglia1 
         Caption         =   "Numero di posti al di sotto del quale c'č scarsa disponibilitā (rosso)"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   360
         Width           =   4695
      End
   End
   Begin VB.CheckBox chkPostoUnico 
      Alignment       =   1  'Right Justify
      Caption         =   "Posto unico"
      Height          =   255
      Left            =   4200
      TabIndex        =   40
      Top             =   7560
      Width           =   1815
   End
   Begin VB.TextBox txtDescrizionePOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   21
      TabIndex        =   38
      Top             =   7500
      Width           =   2355
   End
   Begin VB.TextBox txtCapienza 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7620
      MaxLength       =   6
      TabIndex        =   6
      Top             =   5520
      Width           =   855
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9060
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   6480
      Width           =   435
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   35
      Top             =   240
      Width           =   3255
   End
   Begin VB.TextBox txtDescrizioneAlternativa 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4200
      MaxLength       =   30
      TabIndex        =   8
      Top             =   5520
      Width           =   3315
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3540
      MaxLength       =   3
      TabIndex        =   5
      Top             =   5520
      Width           =   555
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   9540
      MultiSelect     =   2  'Extended
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   6420
      Width           =   1995
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9060
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   6900
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9060
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   7320
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9060
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   7740
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   7020
      MultiSelect     =   2  'Extended
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   6420
      Width           =   1995
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   5520
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   6420
      Width           =   3975
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   25
      Top             =   4260
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   22
      Top             =   9240
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   19
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   20
      Top             =   9720
      Width           =   1155
   End
   Begin VB.Frame fraSuperArea 
      Caption         =   "Tipo "
      Height          =   1035
      Left            =   4200
      TabIndex        =   21
      Top             =   6360
      Width           =   2595
      Begin VB.OptionButton optSuperarea 
         Caption         =   "Superarea"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   1755
      End
      Begin VB.OptionButton optSuperareaNumerata 
         Caption         =   "Superarea numerata"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   1755
      End
      Begin VB.OptionButton optSuperareaNonNumerata 
         Caption         =   "Superarearea non numerata"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   2355
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePiantaSuperArea 
      Height          =   330
      Left            =   300
      Top             =   3420
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePiantaSuperArea 
      Height          =   3315
      Left            =   120
      TabIndex        =   23
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   5847
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblDescrizionePOS 
      Caption         =   "Descrizione POS"
      Height          =   255
      Left            =   120
      TabIndex        =   39
      Top             =   7260
      Width           =   1935
   End
   Begin VB.Label lblCapienza 
      Caption         =   "Capienza"
      Height          =   255
      Left            =   7620
      TabIndex        =   37
      Top             =   5280
      Width           =   795
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   36
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblDescrizioneAlternativa 
      Caption         =   "Descrizione alternativa"
      Height          =   255
      Left            =   4200
      TabIndex        =   34
      Top             =   5280
      Width           =   1935
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   3540
      TabIndex        =   33
      Top             =   5280
      Width           =   555
   End
   Begin VB.Label lblAree 
      Alignment       =   2  'Center
      Caption         =   "lblAree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   7020
      TabIndex        =   32
      Top             =   5940
      Width           =   4515
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   7020
      TabIndex        =   31
      Top             =   6180
      Width           =   1995
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionate"
      Height          =   195
      Left            =   9540
      TabIndex        =   30
      Top             =   6180
      Width           =   1995
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   29
      Top             =   5280
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   6180
      Width           =   1575
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   4020
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   26
      Top             =   4020
      Width           =   2775
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle SuperAree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   24
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazionePiantaSuperarea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idPiantaSelezionata As Long
Private descrizioneRecordSelezionato As String
Private descrizioneAlternativaRecordSelezionato As String
Private descrizionePOSRecordSelezionato As String
Private nomeRecordSelezionato As String
Private codiceRecordSelezionato As String
Private nomePiantaSelezionata As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private listaAppoggioPiantaSuperArea As Collection
'Private listaCampiValoriUnici As Collection
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String
Private numeroVersioneArea As Long
Private capienza As Long
Private soglia1 As Long
Private soglia2 As Long

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private tipoSuperarea As TipoAreaEnum
Private postoUnico As Boolean
Private continuitaPostiGarantitaRecordSelezionato As ContinuitaPostiGarantitaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pianta"
    txtInfo1.Text = nomePiantaSelezionata
    txtInfo1.Enabled = False

    Select Case tipoSuperarea
    Case TA_SUPERAREA_NUMERATA
        lblAree.Caption = "AREE NUMERATE"
    Case TA_SUPERAREA_NON_NUMERATA
        lblAree.Caption = "AREE NON NUMERATE"
    Case TA_SUPERAREA
        lblAree.Caption = "SUPERAREE"
    Case Else
        lblAree.Caption = ""
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
    End Select
    
    dgrConfigurazionePiantaSuperArea.Caption = "SUPERAREE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaSuperArea.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        txtCapienza.Text = ""
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtSoglia1.Text = ""
        txtSoglia2.Text = ""
        
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        txtCapienza.Enabled = False
        optSuperareaNumerata.Value = False
        optSuperareaNonNumerata.Value = False
        optSuperarea.Value = False
        txtSoglia1.Enabled = False
        txtSoglia2.Enabled = False
        
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblCapienza.Enabled = False
        lblAree.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        optSuperareaNumerata.Enabled = False
        optSuperareaNonNumerata.Enabled = False
        optSuperarea.Enabled = False
        
        chkPostoUnico.Enabled = False
        
        fraSuperArea.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        
        lblSoglia1.Enabled = False
        lblSoglia2.Enabled = False
        
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaSuperArea.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCapienza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            (tipoSuperarea = TA_SUPERAREA_NUMERATA Or tipoSuperarea = TA_SUPERAREA_NON_NUMERATA))
        optSuperareaNumerata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia <> ASG_MODIFICA)
        optSuperareaNonNumerata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia <> ASG_MODIFICA)
        optSuperarea.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia <> ASG_MODIFICA)
        txtSoglia1.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtSoglia2.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
        chkPostoUnico.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia = ASG_MODIFICA And tipoSuperarea = TA_SUPERAREA_NUMERATA)
        
        fraSuperArea.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            gestioneRecordGriglia <> ASG_MODIFICA)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizioneAlternativa.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCapienza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
            (tipoSuperarea = TA_SUPERAREA_NUMERATA Or tipoSuperarea = TA_SUPERAREA_NON_NUMERATA))
        lblAree.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSoglia1.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSoglia2.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        Select Case gestioneRecordGriglia
            Case ASG_ELIMINA
                cmdConferma.Enabled = True
            Case ASG_NON_SPECIFICATO
                'Do Nothing
            Case Else
                If listaSelezionati Is Nothing Then
                    cmdConferma.Enabled = False
                Else
'                    cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
'                        Trim(txtCodice.Text) <> "" And _
'                        Trim(txtCapienza.Text) <> "" And _
'                        Trim(txtDescrizioneAlternativa.Text) <> "" And _
'                        listaSelezionati.count > 0 And _
'                        (tipoSuperarea = TA_SUPERAREA_NUMERATA Or tipoSuperarea = TA_SUPERAREA_NON_NUMERATA)) Or _
'                        (Trim(txtNome.Text) <> "" And _
'                        Trim(txtCodice.Text) <> "" And _
'                        Trim(txtDescrizioneAlternativa.Text) <> "" And _
'                        listaSelezionati.count > 0)
                    cmdConferma.Enabled = (tipoSuperarea = TA_SUPERAREA And _
                        Trim(txtNome.Text) <> "" And _
                        Trim(txtCodice.Text) <> "" And _
                        Trim(txtDescrizioneAlternativa.Text) <> "" And _
                        listaSelezionati.count > 0 And _
                        tipoSuperarea = TA_SUPERAREA) Or _
                        ((tipoSuperarea = TA_SUPERAREA_NUMERATA Or tipoSuperarea = TA_SUPERAREA_NON_NUMERATA) And _
                        Trim(txtNome.Text) <> "" And _
                        Trim(txtCapienza.Text) <> "" And _
                        Trim(txtCodice.Text) <> "" And _
                        Trim(txtDescrizioneAlternativa.Text) <> "" And _
                        listaSelezionati.count > 0)
                End If
        End Select
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaSuperArea.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtDescrizioneAlternativa.Text = ""
        txtDescrizionePOS.Text = ""
        txtCodice.Text = ""
        txtCapienza.Text = ""
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtSoglia1.Text = ""
        txtSoglia2.Text = ""
        
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtCodice.Enabled = False
        txtCapienza.Enabled = False
        optSuperareaNumerata.Value = False
        optSuperareaNonNumerata.Value = False
        optSuperarea.Value = False
        txtSoglia1.Enabled = False
        txtSoglia2.Enabled = False
        
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizioneAlternativa.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblCapienza.Enabled = False
        lblAree.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        optSuperareaNumerata.Enabled = False
        optSuperareaNonNumerata.Enabled = False
        optSuperarea.Enabled = False
        
        chkPostoUnico.Enabled = False
        
        fraSuperArea.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetNomePiantaSelezionata(nome As String)
    nomePiantaSelezionata = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    tipoSuperarea = TA_NON_SPECIFICATO
    Call AggiornaAbilitazioneControlli
    Call SbloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()

    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    numeroVersioneArea = numeroVersioneArea + 1
    If ValoriCampiOK Then
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA, CCDA_SUPERAREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazionePiantaSuperArea_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazionePiantaSuperArea_Init
            Case ASG_INSERISCI_DA_SELEZIONE
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA, CCDA_SUPERAREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazionePiantaSuperArea_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazionePiantaSuperArea_Init
            Case ASG_MODIFICA
                Call AggiornaNellaBaseDati
                Call SbloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
                Call ScriviLog(CCTA_MODIFICA, CCDA_PIANTA, CCDA_SUPERAREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazionePiantaSuperArea_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazionePiantaSuperArea_Init
            Case ASG_ELIMINA
                Call EliminaDallaBaseDati
                Call SbloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PIANTA, CCDA_SUPERAREA, "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazionePiantaSuperArea_Init
                Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                Call dgrConfigurazionePiantaSuperArea_Init
        End Select
        tipoSuperarea = TA_NON_SPECIFICATO
        Call frmInizialePianta.SetIsAttributiPiantaModificati(True)
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idArea = lstSelezionati.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaSelezionati.Item(chiaveArea)
            Call listaDisponibili.Add(area, chiaveArea)
            Call listaSelezionati.Remove(chiaveArea)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String

    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call BloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggioPiantaSuperArea
    Call SbloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String

    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    numeroVersioneArea = 0
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Set listaSelezionati = New Collection
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String

    numeroVersioneArea = 0
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    Dim tipiArea As String
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    Set listaDisponibili = New Collection
    
    Select Case tipoSuperarea
        Case TA_SUPERAREA
            tipiArea = TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA
        Case TA_SUPERAREA_NUMERATA
            tipiArea = TA_AREA_NUMERATA
        Case TA_SUPERAREA_NON_NUMERATA
            tipiArea = TA_AREA_NON_NUMERATA
        Case Else
            tipiArea = idNessunElementoSelezionato
    End Select
    sql = "SELECT IDAREA, NOME FROM AREA"
    sql = sql & " WHERE AREA.IDTIPOAREA IN (" & tipiArea & ")"
    sql = sql & " AND AREA.IDPIANTA = " & idPiantaSelezionata
    sql = sql & " AND AREA.IDAREA <> " & idRecordSelezionato
    sql = sql & " AND AREA.IDAREA_PADRE IS NULL"
    sql = sql & " ORDER BY NOME"
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            areaCorrente.idElementoLista = rec("IDAREA").Value
            areaCorrente.nomeElementoLista = rec("NOME")
            areaCorrente.descrizioneElementoLista = rec("NOME")
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaDisponibili.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    Set listaSelezionati = New Collection
        
    If gestioneRecordGriglia <> ASG_INSERISCI_NUOVO Then
        sql = "SELECT IDAREA, NOME, DESCRIZIONE FROM AREA"
        sql = sql & " WHERE AREA.IDAREA_PADRE = " & idRecordSelezionato
        sql = sql & " ORDER BY NOME"
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set areaCorrente = New clsElementoLista
                areaCorrente.idElementoLista = rec("IDAREA").Value
                areaCorrente.nomeElementoLista = rec("NOME")
                areaCorrente.descrizioneElementoLista = rec("NOME")
                chiaveArea = ChiaveId(areaCorrente.idElementoLista)
                Call listaSelezionati.Add(areaCorrente, chiaveArea)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ORADB.CommitTrans
        
        Call ChiudiConnessioneBD_ORA
        
        Call lstSelezionati_Init
    Else
        Call lstSelezionati_Init
    End If
    
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each area In listaDisponibili
            lstDisponibili.AddItem area.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each area In listaSelezionati
            lstSelezionati.AddItem area.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePiantaSuperArea.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String

    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call BloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
    If Not isAreaBloccataDaUtente Then
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idArea = lstDisponibili.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaDisponibili.Item(chiaveArea)
            Call listaSelezionati.Add(area, chiaveArea)
            Call listaDisponibili.Remove(chiaveArea)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaSelezionati()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaSelezionati
        chiaveArea = ChiaveId(area.idElementoLista)
        Call listaDisponibili.Add(area, chiaveArea)
    Next area
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub dgrConfigurazionePiantaSuperArea_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioPiantaSuperArea
    Call adcConfigurazionePiantaSuperArea_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePiantaSuperArea_Init
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePiantaSuperArea.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePiantaSuperArea_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Call PopolaTabellaAppoggioPiantaSuperArea
        
    Set d = adcConfigurazionePiantaSuperArea
        
    sql = "SELECT A.IDAREA ID,"
    sql = sql & " A.NOME ""Nome"","
    sql = sql & " A.CODICE ""Codice"","
    sql = sql & " A.DESCRIZIONE ""Descrizione"","
    sql = sql & " A.CAPIENZA ""Capienza"","
    sql = sql & " A.IDTIPOAREA,"
    sql = sql & " TA.NOME ""Tipo"","
    sql = sql & " TMP.NOME ""Aree"""
    sql = sql & " FROM AREA A, " & nomeTabellaTemporanea & " TMP, TIPOAREA TA"
    sql = sql & " WHERE A.IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
    sql = sql & " AND TMP.IDSUPERAREA(+) = A.IDAREA"
    sql = sql & " AND A.IDTIPOAREA = TA.IDTIPOAREA"
    sql = sql & " AND A.IDPIANTA = " & idPiantaSelezionata
    sql = sql & " ORDER BY A.NOME"
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazionePiantaSuperArea.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub PopolaTabellaAppoggioPiantaSuperArea()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim id As Integer
    Dim i As Integer
    Dim campoNome As String
    Dim idSA As Long
    Dim elencoNomi As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    Set listaAppoggioPiantaSuperArea = New Collection
    
    sql = "SELECT DISTINCT A.IDAREA IDAPPO"
    sql = sql & " FROM PIANTA P, AREA A"
    sql = sql & " WHERE P.IDPIANTA = A.IDPIANTA"
    sql = sql & " AND A.IDTIPOAREA IN (" & TA_SUPERAREA & ", " & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
    sql = sql & " AND P.IDPIANTA = " & idPiantaSelezionata
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDAPPO").Value
            Call listaAppoggioPiantaSuperArea.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioPiantaSuperArea
        campoNome = ""
        sql = "SELECT IDAREA, IDPIANTA, NOME"
        sql = sql & " FROM AREA"
        sql = sql & " WHERE IDAREA_PADRE = " & recordTemporaneo.idElementoLista
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("NOME"), campoNome & "; " & rec("NOME"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioPiantaSuperArea
        idSA = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea
        sql = sql & " VALUES (" & idSA & ", "
        sql = sql & SqlStringValue(elencoNomi) & ")"
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
    Next recordTemporaneo
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub CreaTabellaAppoggioPiantaSuperArea()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_SUPERAREE_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea
    sql = sql & " (IDSUPERAREA NUMBER(10), NOME VARCHAR2(4000))"
    
    Call EliminaTabellaAppoggioPiantaSuperArea
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    SETAConnection.Execute (sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub EliminaTabellaAppoggioPiantaSuperArea()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    SETAConnection.Execute (sql)
    
    Call ORADB.CommitTrans
    
gestioneErrori:
     
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovaSuperArea As Long
    Dim i As Integer
    Dim n As Long
    Dim area As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
        
'   INSERIMENTO IN TABELLA AREA DELLA SUPERAREA PADRE
    idNuovaSuperArea = OttieniIdentificatoreDaSequenza("SQ_AREA")
    sql = "INSERT INTO AREA (IDAREA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
    sql = sql & " CODICE, INDICEDIPREFERIBILITA, CAPIENZA,"
    sql = sql & " IDPIANTA, IDTIPOAREA, IDAREA_PADRE, NUMEROVERSIONE, NUMERATAAPOSTOUNICO, SOGLIA1, SOGLIA2)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaSuperArea & ", "
    sql = sql & SqlStringValue(nomeRecordSelezionato) & ", "
    sql = sql & SqlStringValue(descrizioneRecordSelezionato) & ", "
    sql = sql & SqlStringValue(descrizioneAlternativaRecordSelezionato) & ", "
    sql = sql & SqlStringValue(descrizionePOSRecordSelezionato) & ", "
    sql = sql & SqlStringValue(codiceRecordSelezionato) & ", "
    sql = sql & "NULL, "
    sql = sql & capienza & ", "
    sql = sql & idPiantaSelezionata & ", "
    sql = sql & tipoSuperarea & ", "
    sql = sql & "NULL, "
    sql = sql & numeroVersioneArea & ", "
    sql = sql & IIf(postoUnico, 1, 0) & ", "
    sql = sql & soglia1 & ", "
    sql = sql & soglia2 & ")"
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    Call SetIdRecordSelezionato(idNuovaSuperArea)
    
'   AGGIORNAMENTO IN TABELLA AREA DELLE AREE FIGLIE
    For Each area In listaSelezionati
        sql = "UPDATE AREA SET IDAREA_PADRE = " & idRecordSelezionato
        sql = sql & ", NUMERATAAPOSTOUNICO = " & IIf(postoUnico, 1, 0)
        sql = sql & " WHERE IDAREA = " & area.idElementoLista
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
    Next area
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "SELECT NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS, CODICE,"
    sql = sql & " CAPIENZA, NUMEROVERSIONE, IDTIPOAREA, NUMERATAAPOSTOUNICO, SOGLIA1, SOGLIA2"
    sql = sql & " FROM AREA WHERE IDAREA = " & idRecordSelezionato
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        descrizioneAlternativaRecordSelezionato = rec("DESCRIZIONEALTERNATIVA")
        descrizionePOSRecordSelezionato = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        codiceRecordSelezionato = rec("CODICE")
        capienza = IIf(IsNull(rec("CAPIENZA")), 0, rec("CAPIENZA"))
        numeroVersioneArea = rec("NUMEROVERSIONE")
        tipoSuperarea = rec("IDTIPOAREA")
        postoUnico = IIf(rec("NUMERATAAPOSTOUNICO") = 1, True, False)
        soglia1 = IIf(IsNull(rec("SOGLIA1")), 0, rec("SOGLIA1"))
        soglia2 = IIf(IsNull(rec("SOGLIA2")), 0, rec("SOGLIA2"))
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDescrizionePOS.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizioneAlternativa.Text = descrizioneAlternativaRecordSelezionato
    txtDescrizionePOS.Text = descrizionePOSRecordSelezionato
    txtCodice.Text = codiceRecordSelezionato
    txtCapienza.Text = capienza
    txtSoglia1.Text = soglia1
    txtSoglia2.Text = soglia2

    Select Case tipoSuperarea
    Case TA_SUPERAREA_NUMERATA
        optSuperareaNumerata.Value = True
        chkPostoUnico.Value = IIf(postoUnico, 1, 0)
    Case TA_SUPERAREA_NON_NUMERATA
        optSuperareaNonNumerata.Value = True
    Case TA_SUPERAREA
        optSuperarea.Value = True
    Case Else
        'Do Nothing
    End Select
        
    internalEvent = internalEventOld

End Sub

Private Sub dgrConfigurazionePiantaSuperArea_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePiantaSuperArea
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 6
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Visible = False
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub optSuperareaNumerata_Click()
    If Not internalEvent Then
        tipoSuperarea = TA_SUPERAREA_NUMERATA
        Call AggiornaAbilitazioneControlli
        Call CaricaValoriLstDisponibili
        Call CaricaValoriLstSelezionati
    End If
End Sub

Private Sub optSuperareaNonNumerata_Click()
    If Not internalEvent Then
        tipoSuperarea = TA_SUPERAREA_NON_NUMERATA
        Call AggiornaAbilitazioneControlli
        Call CaricaValoriLstDisponibili
        Call CaricaValoriLstSelezionati
    End If
End Sub

Private Sub optSuperarea_Click()
    If Not internalEvent Then
        tipoSuperarea = TA_SUPERAREA
        Call AggiornaAbilitazioneControlli
        Call CaricaValoriLstDisponibili
        Call CaricaValoriLstSelezionati
    End If
End Sub

Private Sub txtCodice_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizioneAlternativa_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizionePOS_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call ValorizzaCodiceAutomaticamente
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub ValorizzaCodiceAutomaticamente()
    txtCodice.Text = Left$(txtNome.Text, 3)
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim numero As Long
    Dim listaNonConformitā As Collection
    Dim listaValoriDaConfermare As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    Set listaValoriDaConfermare = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDAREA <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrizioneAlternativaRecordSelezionato = Trim(txtDescrizioneAlternativa.Text)
    descrizionePOSRecordSelezionato = Trim(txtDescrizionePOS.Text)
    codiceRecordSelezionato = Trim(txtCodice.Text)
    If ViolataUnicitā("AREA", "CODICE = " & SqlStringValue(codiceRecordSelezionato), "IDPIANTA = " & idPiantaSelezionata, _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice = " & SqlStringValue(codiceRecordSelezionato) & _
            " č giā presente in DB per la stessa pianta;")
    End If
    If tipoSuperarea = TA_SUPERAREA_NUMERATA Or tipoSuperarea = TA_SUPERAREA_NON_NUMERATA Then
        If IsCampoInteroCorretto(txtCapienza) Then
            capienza = CLng(Trim(txtCapienza.Text))
            
            If capienza > 0 Then
                If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_INSERISCI_NUOVO Then
                    If txtSoglia1.Text = "" Then
                        txtSoglia1.Text = "0"
                    End If
                    If txtSoglia2.Text = "" Then
                        txtSoglia2.Text = calcolaSoglia2(capienza)
                    End If
                End If
            End If
            
            If capienza > 0 Then
                If gestioneRecordGriglia = ASG_MODIFICA Then
                    If txtSoglia1.Text = "" Then
                        txtSoglia1.Text = "0"
                    End If
                    If txtSoglia2.Text = "" Then
                        txtSoglia2.Text = calcolaSoglia2(capienza)
                    End If
                    numero = NumeroPostiInSuperarea
                    If capienza <> numero Then
                        Call listaValoriDaConfermare.Add("- il valore Capienza non č uguale alla somma dei posti delle aree associate associate (" & numero & ");")
'                        Call frmMessaggio.Visualizza("ConfermaCapienzaNonCorretta", numero)
'                        If frmMessaggio.exitCode = EC_ANNULLA Then
'                            ValoriCampiOK = False
    '                        Exit Function
'                        End If
                    End If
                End If
            Else
                ValoriCampiOK = False
'                Call frmMessaggio.Visualizza("ErroreFormatoDatiCapienza")
                Call listaNonConformitā.Add("- il valore immesso sul campo capienza deve essere numerico di tipo intero e maggiore di zero;")
            End If
        Else
'            Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Capienza")
            Call listaNonConformitā.Add("- il valore immesso sul campo capienza deve essere numerico di tipo intero;")
            ValoriCampiOK = False
        End If
    End If
    
    If optSuperareaNumerata.Value = True Then
        tipoSuperarea = TA_SUPERAREA_NUMERATA
    ElseIf optSuperareaNonNumerata.Value = True Then
        tipoSuperarea = TA_SUPERAREA_NON_NUMERATA
    ElseIf optSuperarea.Value = True Then
        tipoSuperarea = TA_SUPERAREA
    End If
    
    If chkPostoUnico.Value = 1 Then
        postoUnico = True
    Else
        postoUnico = False
    End If
    
    If tipoSuperarea = TA_SUPERAREA_NUMERATA Then
        If IsCampoInteroCorretto(txtSoglia1) Then
            soglia1 = txtSoglia1.Text
        Else
            Call listaNonConformitā.Add("- il valore immesso per la prima soglia deve essere numerico di tipo intero;")
            ValoriCampiOK = False
        End If
        If IsCampoInteroCorretto(txtSoglia2) Then
            soglia2 = txtSoglia2.Text
        Else
            Call listaNonConformitā.Add("- il valore immesso per la seconda soglia deve essere numerico di tipo intero;")
            ValoriCampiOK = False
        End If
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    Else
        If listaValoriDaConfermare.count > 0 Then
            Call frmMessaggio.Visualizza("ConfermaValori", ArgomentoMessaggio(listaValoriDaConfermare))
            If frmMessaggio.exitCode = EC_ANNULLA Then
                ValoriCampiOK = False
            End If
        End If
    End If
    
End Function
'
'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("CODICE = " & SqlStringValue(codiceRecordSelezionato))
'End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim superareaEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
'    Dim mousePointerOld As Integer
    Dim n As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
    
    Call ApriConnessioneBD_ORA
    
    On Error GoTo gestioneErrori
    
    Set listaTabelleCorrelate = New Collection
    
    Call ORADB.BeginTrans
        
    superareaEliminabile = True
    If Not IsRecordEliminabile("IDAREA", "PREZZOTITOLOPRODOTTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PREZZOTITOLOPRODOTTO")
        superareaEliminabile = False
    End If
    If Not IsRecordEliminabile("IDAREA", "UTILIZZOLAYOUTSUPPORTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
        superareaEliminabile = False
    End If
    If Not IsRecordEliminabile("IDAREA", "PIANTASIAE_AREA_ORDINEDIPOSTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PIANTASIAE_AREA_ORDINEDIPOSTO")
        superareaEliminabile = False
    End If
    
    If superareaEliminabile Then
    '   PRIMA FASE: VANNO CANCELLATI I RIFERIMENTI SU AREA CHE PUNTANO ALLA SUPERAREA
        sql = "UPDATE AREA SET IDAREA_PADRE = NULL WHERE IDAREA_PADRE = " & idRecordSelezionato
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
    '   SECONDA FASE: IL RECORD VIENE ELIMINATO
        Call SbloccaDominioPerUtente(CCDA_SUPERAREA, idRecordSelezionato, isAreaBloccataDaUtente)
        sql = "DELETE FROM AREA WHERE IDAREA = " & idRecordSelezionato
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
        
        Call ORADB.CommitTrans
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La superarea selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD_ORA
'    MousePointer = mousePointerOld
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
'    MousePointer = mousePointerOld
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim i As Integer
    
'    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = strSQL
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
'    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub AggiornaVersioneInBaseDati()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql = "UPDATE AREA SET NUMEROVERSIONE = " & numeroVersioneArea
    sql = sql & " WHERE IDAREA = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaDisponibili()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaDisponibili
        chiaveArea = ChiaveId(area.idElementoLista)
        Call listaSelezionati.Add(area, chiaveArea)
    Next area
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Function NumeroPostiInSuperarea() As Long
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim sql As String
    Dim cont As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    cont = 0
    If tipoSuperarea <> TA_SUPERAREA Then
        Select Case tipoSuperarea
            Case TA_SUPERAREA_NUMERATA
                sql = "SELECT COUNT(IDPOSTO) NUMERO FROM POSTO P, AREA A"
                sql = sql & " WHERE P.IDAREA = A.IDAREA"
                sql = sql & " AND A.IDAREA_PADRE = " & idRecordSelezionato
            Case TA_SUPERAREA_NON_NUMERATA
                sql = "SELECT SUM(CAPIENZA) NUMERO FROM AREA"
                sql = sql & " WHERE IDAREA_PADRE = " & idRecordSelezionato
        End Select
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        Set rec = ORADB.CreateDynaset(sql, 0&)
        cont = rec("NUMERO")
        rec.Close
    End If
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    NumeroPostiInSuperarea = cont
End Function

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim area As clsElementoLista

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "UPDATE AREA SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & ","
    sql = sql & " DESCRIZIONEALTERNATIVA = " & SqlStringValue(descrizioneAlternativaRecordSelezionato) & ","
    sql = sql & " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOSRecordSelezionato) & ","
    sql = sql & " CODICE = " & SqlStringValue(codiceRecordSelezionato) & ","
    sql = sql & " CAPIENZA = " & capienza & ","
    sql = sql & " IDPIANTA = " & idPiantaSelezionata & ","
    sql = sql & " IDTIPOAREA = " & tipoSuperarea & ","
    sql = sql & " NUMEROVERSIONE = " & numeroVersioneArea & ","
    sql = sql & " NUMERATAAPOSTOUNICO = " & IIf(postoUnico, 1, 0) & ","
    sql = sql & " SOGLIA1 = " & soglia1 & ","
    sql = sql & " SOGLIA2 = " & soglia2
    sql = sql & " WHERE IDAREA = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
'   PRIMA FASE: VANNO CANCELLATI I VALORI DEL CAMPO IDAREA_PADRE DEI RECORD
'   ASSOCIATI ALLA SUPERAREA SELEZIONATA
    sql = "UPDATE AREA SET IDAREA_PADRE = NULL, NUMERATAAPOSTOUNICO=0 WHERE IDAREA_PADRE = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
        
'   SECONDA FASE: IL CAMPO IDAREA_PADRE DEI RECORD SELEZIONATI IN LISTA
'   VA VALORIZZATO CON L'IDAREA DELLA SUPERAREA SELEZIONATA
    For Each area In listaSelezionati
        sql = "UPDATE AREA SET IDAREA_PADRE = " & idRecordSelezionato
        sql = sql & ", NUMERATAAPOSTOUNICO = " & IIf(postoUnico, 1, 0)
        sql = sql & " WHERE IDAREA = " & area.idElementoLista
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
    Next area
    
    Call ORADB.CommitTrans
        
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

'>= 1000: 10%
'500 - 1000: 15%
'<500: 25%

Private Function calcolaSoglia2(capienza As Long) As Long

    calcolaSoglia2 = 0
    If capienza > 1000 Then
        calcolaSoglia2 = capienza * 10 / 100
    ElseIf capienza > 500 Then
        calcolaSoglia2 = capienza * 15 / 100
    Else
        calcolaSoglia2 = capienza * 25 / 100
    End If
End Function

