VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDiritOperProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public listaProdottiSelezionati As Collection
Public selezionaDirittiDaOperatoreMaster As Boolean
Public selezionaDirittiSuCaratteristicheProdotto As Boolean
Public importaDirittiSuTutteLeTariffe As ValoreBooleanoEnum
Public importaDirittiSuTutteLeProtezioni As ValoreBooleanoEnum
Public idOperatoreSelezionato As Long
'Public aggiorna As Boolean

Public Sub Init()
    idOperatoreSelezionato = idNessunElementoSelezionato
    Set listaProdottiSelezionati = New Collection
    importaDirittiSuTutteLeProtezioni = VB_FALSO
    importaDirittiSuTutteLeTariffe = VB_FALSO
    selezionaDirittiDaOperatoreMaster = False
    selezionaDirittiSuCaratteristicheProdotto = False
'    aggiorna = False
End Sub

Public Sub Update()
    If selezionaDirittiDaOperatoreMaster Then
        importaDirittiSuTutteLeProtezioni = VB_FALSO
        importaDirittiSuTutteLeTariffe = VB_FALSO
    ElseIf selezionaDirittiSuCaratteristicheProdotto Then
        idOperatoreSelezionato = idNessunElementoSelezionato
    End If
'    aggiorna = True
End Sub


