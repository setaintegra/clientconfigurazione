VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneTipoSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tipo Supporto"
   ClientHeight    =   11355
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11355
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkAbilitatoPerTitoloDigitale 
      Alignment       =   1  'Right Justify
      Caption         =   "Abilitato per titolo digitale"
      Height          =   255
      Left            =   9480
      TabIndex        =   42
      Top             =   9720
      Width           =   2175
   End
   Begin VB.CheckBox chkSostitutivo 
      Alignment       =   1  'Right Justify
      Caption         =   "Sostitutivo"
      Height          =   255
      Left            =   7800
      TabIndex        =   41
      Top             =   9720
      Width           =   1215
   End
   Begin VB.CheckBox chkCorporate 
      Alignment       =   1  'Right Justify
      Caption         =   "Corporate"
      Height          =   255
      Left            =   6120
      TabIndex        =   40
      Top             =   9720
      Width           =   1215
   End
   Begin VB.TextBox txtCodiceSerieGESDI 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4560
      MaxLength       =   20
      TabIndex        =   38
      Top             =   9720
      Width           =   1035
   End
   Begin VB.TextBox txtEmittenteGESDI 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      MaxLength       =   20
      TabIndex        =   36
      Top             =   9720
      Width           =   915
   End
   Begin VB.CheckBox chkAttivo 
      Alignment       =   1  'Right Justify
      Caption         =   "Attivo"
      Height          =   255
      Left            =   2520
      TabIndex        =   35
      Top             =   7200
      Width           =   975
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   33
      Top             =   960
      Width           =   4095
   End
   Begin VB.CheckBox chkTipiSupportiAttivi 
      Caption         =   "solo tipi supporti attivi"
      Height          =   195
      Left            =   4440
      TabIndex        =   32
      Top             =   960
      Value           =   1  'Checked
      Width           =   2655
   End
   Begin VB.ComboBox cmbTipoSupportoSIAE 
      Height          =   315
      Left            =   120
      TabIndex        =   30
      Top             =   9240
      Width           =   3615
   End
   Begin VB.TextBox txtDescrizionePOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   20
      TabIndex        =   6
      Top             =   7920
      Width           =   2235
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   8220
      MultiSelect     =   2  'Extended
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   6720
      Width           =   3555
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   7200
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   7620
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   8040
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   4140
      MultiSelect     =   2  'Extended
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   6720
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6780
      Width           =   435
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   6540
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   8520
      Width           =   7935
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   5280
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   3
      TabIndex        =   5
      Top             =   7200
      Width           =   495
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   17
      Top             =   10320
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   16
      Top             =   10680
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneTipoSupporto 
      Height          =   330
      Left            =   240
      Top             =   4500
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneTipoSupporto 
      Height          =   3495
      Left            =   120
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1440
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6165
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCodiceSerieGESDI 
      Caption         =   "codice serie GESDI"
      Height          =   255
      Left            =   2880
      TabIndex        =   39
      Top             =   9720
      Width           =   1575
   End
   Begin VB.Label lblEmittenteGESDI 
      Caption         =   "emittente GESDI"
      Height          =   255
      Left            =   120
      TabIndex        =   37
      Top             =   9720
      Width           =   1455
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   34
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label lbltipoSupportoSIAE 
      Caption         =   "Tipo supporto SIAE"
      Height          =   255
      Left            =   120
      TabIndex        =   31
      Top             =   9000
      Width           =   1575
   End
   Begin VB.Label lblDescrizionePOS 
      Caption         =   "Descrizione POS"
      Height          =   255
      Left            =   120
      TabIndex        =   29
      Top             =   7680
      Width           =   1695
   End
   Begin VB.Label lblLayoutSupporto 
      Alignment       =   2  'Center
      Caption         =   "LAYOUT SUPPORTO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4140
      TabIndex        =   28
      Top             =   6240
      Width           =   7635
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   4140
      TabIndex        =   27
      Top             =   6480
      Width           =   3555
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati"
      Height          =   195
      Left            =   8220
      TabIndex        =   26
      Top             =   6480
      Width           =   3555
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   6300
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   8280
      Width           =   1575
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   22
      Top             =   5040
      Width           =   2775
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   6960
      Width           =   675
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Tipi di Supporto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   8595
   End
End
Attribute VB_Name = "frmConfigurazioneTipoSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeRecordSelezionato As String
Private codiceRecordSelezionato As String
Private attivoRecordSelezionato As ValoreBooleanoEnum
Private descrizioneRecordSelezionato As String
Private descrizionePOSRecordSelezionato As String
Private idTipoSupportoSIAESelezionato As Long
Private emittenteGESDI As String
Private codiceSerieGESDI As String
Private corporate As Long
Private sostitutivo As Long
Private abilitatoPerTitoloDigitale As Long
Private nomeTabellaTemporanea As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private listaAppoggioTipiSupportoLayout As Collection

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
        
    dgrConfigurazioneTipoSupporto.Caption = "TIPI DI SUPPORTO CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTipoSupporto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtCodice.Text = ""
        txtDescrizione.Text = ""
        txtDescrizionePOS.Text = ""
        lblEmittenteGESDI.Enabled = False
        txtEmittenteGESDI.Enabled = False
        txtEmittenteGESDI.Text = ""
        lblCodiceSerieGESDI.Enabled = False
        txtCodiceSerieGESDI.Enabled = False
        txtCodiceSerieGESDI.Text = ""
        chkCorporate.Enabled = False
        chkSostitutivo.Enabled = False
        chkAbilitatoPerTitoloDigitale.Enabled = False
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtNome.Enabled = False
        txtCodice.Enabled = False
        chkAttivo.Enabled = False
        txtDescrizione.Enabled = False
        cmbTipoSupportoSIAE.Enabled = False
        txtDescrizionePOS.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblLayoutSupporto.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        lbltipoSupportoSIAE.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmbTipoSupportoSIAE.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTipoSupporto.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkAttivo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizionePOS.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbTipoSupportoSIAE.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblLayoutSupporto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblEmittenteGESDI.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtEmittenteGESDI.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodiceSerieGESDI.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodiceSerieGESDI.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        If idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Or idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
            chkCorporate.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
            chkSostitutivo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
            chkAbilitatoPerTitoloDigitale.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        Else
            chkCorporate.Enabled = False
            chkSostitutivo.Enabled = False
            chkAbilitatoPerTitoloDigitale.Enabled = False
        End If
        
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                               Len(Trim(txtCodice)) = 3)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneTipoSupporto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtCodice.Text = ""
        txtDescrizione.Text = ""
        txtDescrizionePOS.Text = ""
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtNome.Enabled = False
        txtCodice.Enabled = False
        chkAttivo.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizionePOS.Enabled = False
        cmbTipoSupportoSIAE.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblDescrizionePOS.Enabled = False
        lblCodice.Enabled = False
        lblLayoutSupporto.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        lbltipoSupportoSIAE.Enabled = False
        lblEmittenteGESDI.Enabled = False
        txtEmittenteGESDI.Enabled = False
        txtEmittenteGESDI.Text = ""
        lblCodiceSerieGESDI.Enabled = False
        txtCodiceSerieGESDI.Enabled = False
        txtCodiceSerieGESDI.Text = ""
        chkCorporate.Enabled = False
        chkSostitutivo.Enabled = False
        chkAbilitatoPerTitoloDigitale.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmbTipoSupportoSIAE.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    End If
    
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    stringaNota = "IDTIPOSUPPORTO = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_TIPO_SUPPORTO, , stringaNota)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneTipoSupporto_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneTipoSupporto_Init
    Else
        If ValoriCampiOK Then
            If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_TIPO_SUPPORTO, , stringaNota)
            ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_TIPO_SUPPORTO, , stringaNota)
            End If
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazioneTipoSupporto_Init
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
            Call dgrConfigurazioneTipoSupporto_Init
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String

    sql = "SELECT IDTIPOTERMINALE AS ID, NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggioTipiSupportoLayout
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String

    sql = "SELECT IDTIPOTERMINALE AS ID, NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String

    sql = "SELECT IDTIPOTERMINALE AS ID, NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    nomeRecordSelezionato = ""
    descrizioneRecordSelezionato = ""
    attivoRecordSelezionato = VB_VERO
    chkAttivo.Value = attivoRecordSelezionato
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
    
    sql = "SELECT IDTIPOSUPPORTOSIAE ID, NOME FROM TIPOSUPPORTOSIAE ORDER BY IDTIPOSUPPORTOSIAE"
    Call CaricaValoriCombo(cmbTipoSupportoSIAE, sql, "NOME", False)
    
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String

    sql = "SELECT IDTIPOTERMINALE AS ID, NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub

Private Sub dgrConfigurazioneTipoSupporto_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioTipiSupportoLayout
        
    Call CaricaOrganizzazioni
    Call AggiornaAbilitazioneControlli

'    Call adcConfigurazioneTipoSupporto_Init
'    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
'    Call dgrConfigurazioneTipoSupporto_Init
'
'    sqlTipiSupportoSIAE = "SELECT IDTIPOSUPPORTOSIAE ID, NOME FROM TIPOSUPPORTOSIAE ORDER BY IDTIPOSUPPORTOSIAE"
'    Call CaricaValoriCombo(cmbTipoSupportoSIAE, sqlTipiSupportoSIAE, "NOME", False)
    
    idTipoSupportoSIAESelezionato = idNessunElementoSelezionato
    
    Call Me.Show(vbModal)
End Sub

Private Sub CaricaOrganizzazioni()
    Dim sql As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE"
    sql = sql & " ORDER BY NOME"
    Call CaricaValoriComboConOpzioneTutti(cmbOrganizzazione, sql, "NOME")

    MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = Me.MousePointer
    Me.MousePointer = vbHourglass
    Call cmbOrganizzazione_Update
    
    Me.MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Update()
    Dim sqlTipiSupportoSIAE As String
    
    idOrganizzazioneSelezionata = Me.cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    Call adcConfigurazioneTipoSupporto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneTipoSupporto_Init
    
    sqlTipiSupportoSIAE = "SELECT IDTIPOSUPPORTOSIAE ID, NOME FROM TIPOSUPPORTOSIAE ORDER BY IDTIPOSUPPORTOSIAE"

    Call CaricaValoriCombo(cmbTipoSupportoSIAE, sqlTipiSupportoSIAE, "NOME", False)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneTipoSupporto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub adcConfigurazioneTipoSupporto_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioTipiSupportoLayout
    
    Set d = adcConfigurazioneTipoSupporto
    
'    sql = "SELECT TIPOSUPPORTO.IDTIPOSUPPORTO ID, TIPOSUPPORTO.CODICE AS ""Codice""," & _
'        " TIPOSUPPORTO.NOME AS ""Nome"", TIPOSUPPORTO.DESCRIZIONE AS ""Descrizione""," & _
'        " TIPOSUPPORTO.DESCRIZIONEPOS AS ""Descrizione POS""," & _
'        " TSS.NOME AS ""Tipo supporto SIAE""," & _
'        " TMP.NOME AS ""Layout associati""" & _
'        " FROM TIPOSUPPORTO, " & nomeTabellaTemporanea & " TMP, TIPOSUPPORTOSIAE TSS WHERE" & _
'        " (TMP.IDTIPO(+) = TIPOSUPPORTO.IDTIPOSUPPORTO)" & _
'        " AND TIPOSUPPORTO.IDTIPOSUPPORTOSIAE = TSS.IDTIPOSUPPORTOSIAE" & _
'        " ORDER BY ""Codice"""
    
    sql = "SELECT TIPOSUPPORTO.IDTIPOSUPPORTO ID, TIPOSUPPORTO.CODICE AS ""Codice""," & _
        " TIPOSUPPORTO.NOME AS ""Nome"", TIPOSUPPORTO.DESCRIZIONE AS ""Descrizione""," & _
        " TIPOSUPPORTO.DESCRIZIONEPOS AS ""Descrizione POS""," & _
        " TSS.NOME AS ""Tipo supporto SIAE""," & _
        " TMP.NOME AS ""Layout associati""," & _
        " DECODE(ATTIVO, 0, 'NO', 'SI') ""Attivo""" & _
        " FROM TIPOSUPPORTO, " & nomeTabellaTemporanea & " TMP, TIPOSUPPORTOSIAE TSS" & _
        " WHERE TMP.IDTIPO(+) = TIPOSUPPORTO.IDTIPOSUPPORTO" & _
        " AND TIPOSUPPORTO.IDTIPOSUPPORTOSIAE = TSS.IDTIPOSUPPORTOSIAE"
        
    If chkTipiSupportiAttivi.Value = vbChecked Then
        sql = sql & " AND ATTIVO = 1"
    End If
    
    sql = sql & " ORDER BY ""Codice"""
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneTipoSupporto.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim max As Long
    Dim idNuovoTipoSupporto As Long
    Dim i As Integer
    Dim layout As clsElementoLista
    Dim n As Long
    
    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori

    idNuovoTipoSupporto = OttieniIdentificatoreDaSequenza("SQ_TIPOSUPPORTO")
    
    If idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Or idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
        sql = "INSERT INTO TIPOSUPPORTO (IDTIPOSUPPORTO, NOME, DESCRIZIONE, DESCRIZIONEPOS, CODICE, IDTIPOSUPPORTOSIAE, ATTIVO, EMITTENTEGESDI, CODICESERIEGESDI, CORPORATE, SOSTITUTIVO, ABILITATOPERTITOLODIGITALE)" & _
            " VALUES (" & _
            idNuovoTipoSupporto & ", " & _
            SqlStringValue(nomeRecordSelezionato) & ", " & _
            SqlStringValue(descrizioneRecordSelezionato) & ", " & _
            SqlStringValue(descrizionePOSRecordSelezionato) & ", " & _
            SqlStringValue(codiceRecordSelezionato) & ", " & _
            idTipoSupportoSIAESelezionato & ", " & _
            IIf(attivoRecordSelezionato = VB_VERO, "1", "0") & ", " & _
            IIf(txtEmittenteGESDI.Text = "", "NULL", "'" & txtEmittenteGESDI.Text & "'") & ", " & _
            IIf(txtCodiceSerieGESDI.Text = "", "NULL", "'" & txtCodiceSerieGESDI.Text & "'") & ", " & _
            IIf(chkCorporate.Value = vbChecked, "1", "0") & ", " & _
            IIf(chkSostitutivo.Value = vbChecked, "1", "0") & ", " & _
            IIf(chkAbilitatoPerTitoloDigitale.Value = vbChecked, "1", "0") & ")"
    Else
        sql = "INSERT INTO TIPOSUPPORTO (IDTIPOSUPPORTO, NOME, DESCRIZIONE, DESCRIZIONEPOS, CODICE, IDTIPOSUPPORTOSIAE, ATTIVO, EMITTENTEGESDI, CODICESERIEGESDI, CORPORATE, SOSTITUTIVO, ABILITATOPERTITOLODIGITALE)" & _
            " VALUES (" & _
            idNuovoTipoSupporto & ", " & _
            SqlStringValue(nomeRecordSelezionato) & ", " & _
            SqlStringValue(descrizioneRecordSelezionato) & ", " & _
            SqlStringValue(descrizionePOSRecordSelezionato) & ", " & _
            SqlStringValue(codiceRecordSelezionato) & ", " & _
            idTipoSupportoSIAESelezionato & ", " & _
            IIf(attivoRecordSelezionato = VB_VERO, "1", "0") & ", NULL, NULL, NULL, NULL, NULL)"
    End If
    SETAConnection.Execute sql, n, adCmdText
    
    
'   INSERIMENTO IN TABELLA LAYOUTSUPPORTO_TIPOSUPPORTO
    If idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_BIGLIETTO_TRADIZIONALE Then
        If Not (listaSelezionati Is Nothing) Then
            For Each layout In listaSelezionati
                sql = "INSERT INTO LAYOUTSUPPORTO_TIPOSUPPORTO (IDLAYOUTSUPPORTO, IDTIPOSUPPORTO)" & _
                    " VALUES (" & layout.idElementoLista & ", " & _
                    idNuovoTipoSupporto & ")"
                SETAConnection.Execute sql, n, adCmdText
            Next layout
        End If
    End If
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoTipoSupporto)
    Call AggiornaAbilitazioneControlli
    
Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT NOME, CODICE, DESCRIZIONE, DESCRIZIONEPOS, IDTIPOSUPPORTOSIAE, ATTIVO, EMITTENTEGESDI, CODICESERIEGESDI, CORPORATE, SOSTITUTIVO, ABILITATOPERTITOLODIGITALE FROM TIPOSUPPORTO" & _
        " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        codiceRecordSelezionato = rec("CODICE")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        descrizionePOSRecordSelezionato = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        idTipoSupportoSIAESelezionato = rec("IDTIPOSUPPORTOSIAE")
        attivoRecordSelezionato = IIf(rec("ATTIVO") = 1, VB_VERO, VB_FALSO)
        emittenteGESDI = IIf(IsNull(rec("EMITTENTEGESDI")), "", rec("EMITTENTEGESDI"))
        codiceSerieGESDI = IIf(IsNull(rec("CODICESERIEGESDI")), "", rec("CODICESERIEGESDI"))
        corporate = IIf(rec("CORPORATE") = 1, VB_VERO, VB_FALSO)
        sostitutivo = IIf(rec("SOSTITUTIVO") = 1, VB_VERO, VB_FALSO)
        abilitatoPerTitoloDigitale = IIf(rec("ABILITATOPERTITOLODIGITALE") = 1, VB_VERO, VB_FALSO)
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = nomeRecordSelezionato
    txtCodice.Text = codiceRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtDescrizionePOS.Text = descrizionePOSRecordSelezionato
    Call SelezionaElementoSuCombo(cmbTipoSupportoSIAE, idTipoSupportoSIAESelezionato)
    chkAttivo.Value = attivoRecordSelezionato
    txtEmittenteGESDI.Text = emittenteGESDI
    txtCodiceSerieGESDI.Text = codiceSerieGESDI
    chkCorporate.Value = corporate
    chkSostitutivo.Value = sostitutivo
    chkAbilitatoPerTitoloDigitale.Value = abilitatoPerTitoloDigitale

    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Integer
    Dim layout As clsElementoLista

    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori
    
    If idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE Or idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO Then
        sql = "UPDATE TIPOSUPPORTO SET" & _
            " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
            " CODICE = " & SqlStringValue(codiceRecordSelezionato) & ", " & _
            " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & ", " & _
            " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOSRecordSelezionato) & ", " & _
            " IDTIPOSUPPORTOSIAE = " & idTipoSupportoSIAESelezionato & ", " & _
            " ATTIVO = " & IIf(attivoRecordSelezionato = VB_VERO, 1, 0) & ", " & _
            " EMITTENTEGESDI = " & SqlStringValue(emittenteGESDI) & ", " & _
            " CODICESERIEGESDI = " & SqlStringValue(codiceSerieGESDI) & ", " & _
            " CORPORATE = " & IIf(corporate = VB_VERO, 1, 0) & ", " & _
            " SOSTITUTIVO = " & IIf(sostitutivo = VB_VERO, 1, 0) & ", " & _
            " ABILITATOPERTITOLODIGITALE = " & IIf(abilitatoPerTitoloDigitale = VB_VERO, 1, 0) & _
            " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
    Else
        sql = "UPDATE TIPOSUPPORTO SET" & _
            " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
            " CODICE = " & SqlStringValue(codiceRecordSelezionato) & ", " & _
            " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & ", " & _
            " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOSRecordSelezionato) & ", " & _
            " IDTIPOSUPPORTOSIAE = " & idTipoSupportoSIAESelezionato & ", " & _
            " ATTIVO = " & IIf(attivoRecordSelezionato = VB_VERO, 1, 0) & _
            " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
    End If
    SETAConnection.Execute sql, n, adCmdText
    
'   AGGIORNAMENTO TABELLA LAYOUTSUPPORTO_TIPOSUPPORTO

' prima della introduzione della classe medium questa sezione prendeva in considerazione solo i tipi tradizionali
'    If idTipoSupportoSIAESelezionato = IDTIPOSUPPORTOSIAE_BIGLIETTO_TRADIZIONALE Then
'    ora e' eseguita sempre
        If Not (listaSelezionati Is Nothing) Then
            'prima fase: eliminazione dei record correlati giā presenti,
            'il reinserimento dei quali causerebbe violazione della chiave nella coppia idarea, idvarco
            'NOTA: va fatta solo se effettivamente c'č giā qualche record correlato.
            sql = "DELETE FROM LAYOUTSUPPORTO_TIPOSUPPORTO" & _
                " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
            
            'seconda fase: inserimento delle nuove selezioni e reinserimento delle vecchie,
            'eliminate alla fase 1
            For Each layout In listaSelezionati
                sql = "INSERT INTO LAYOUTSUPPORTO_TIPOSUPPORTO (IDLAYOUTSUPPORTO, IDTIPOSUPPORTO)" & _
                    " VALUES (" & layout.idElementoLista & ", " & _
                    idRecordSelezionato & ")"
                SETAConnection.Execute sql, n, adCmdText
            Next layout
        End If
'    End If
    
    Call ChiudiConnessioneBD
    
Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub dgrConfigurazioneTipoSupporto_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneTipoSupporto
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 7
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneTipoSupporto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

On Error Resume Next

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTIPOSUPPORTO <> " & idRecordSelezionato
    End If
    
    attivoRecordSelezionato = IIf(chkAttivo.Value = vbChecked, VB_VERO, VB_FALSO)
    corporate = IIf(chkCorporate.Value = vbChecked, VB_VERO, VB_FALSO)
    sostitutivo = IIf(chkSostitutivo.Value = vbChecked, VB_VERO, VB_FALSO)
    abilitatoPerTitoloDigitale = IIf(chkAbilitatoPerTitoloDigitale.Value = vbChecked, VB_VERO, VB_FALSO)
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If attivoRecordSelezionato = VB_VERO Then
        If ViolataUnicitā("TIPOSUPPORTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "", _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
                " č giā presente in DB;")
        End If
    End If
    
    codiceRecordSelezionato = Trim(txtCodice.Text)
    If attivoRecordSelezionato = VB_VERO Then
        If ViolataUnicitā("TIPOSUPPORTO", "CODICE = " & SqlStringValue(codiceRecordSelezionato), "", _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore codice = " & SqlStringValue(codiceRecordSelezionato) & _
                " č giā presente in DB;")
        End If
    End If
    
    If idTipoSupportoSIAESelezionato = idNessunElementoSelezionato Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- non č stato selezionato il codice SIAE;")
    End If
    
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    descrizionePOSRecordSelezionato = Trim(txtDescrizionePOS.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCodice_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdDidsponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstDisponibili
    
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveLayout As String
    Dim layoutCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Then
        sql = "SELECT NOME, CODICE, IDLAYOUTSUPPORTO" & _
            " FROM LAYOUTSUPPORTO ORDER BY CODICE"
    Else
        sql = "SELECT DISTINCT LS.NOME, LS.CODICE, LS.IDLAYOUTSUPPORTO" & _
            " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO(+)" & _
            " AND LSTS.IDLAYOUTSUPPORTO IS NULL" & _
            " AND LSTS.IDTIPOSUPPORTO(+) = " & idRecordSelezionato & _
            " ORDER BY CODICE"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set layoutCorrente = New clsElementoLista
            layoutCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
            layoutCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO").Value
            chiaveLayout = ChiaveId(layoutCorrente.idElementoLista)
            Call listaDisponibili.Add(layoutCorrente, chiaveLayout)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveLayout As String
    Dim layoutCorrente As clsElementoLista
    
    Call ApriConnessioneBD

    Set listaSelezionati = New Collection
    If gestioneRecordGriglia <> ASG_INSERISCI_NUOVO Then
        sql = "SELECT LSTS.IDTIPOSUPPORTO," & _
            " LS.IDLAYOUTSUPPORTO AS ""IDLAYOUTSUPPORTO"", LS.NOME, LS.CODICE" & _
            " FROM LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
            " AND LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
            " AND LSTS.IDTIPOSUPPORTO = " & idRecordSelezionato & _
            " ORDER BY CODICE"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set layoutCorrente = New clsElementoLista
                layoutCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
                layoutCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO").Value
                chiaveLayout = ChiaveId(layoutCorrente.idElementoLista)
                Call listaSelezionati.Add(layoutCorrente, chiaveLayout)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ChiudiConnessioneBD
        
        Call lstSelezionati_Init
    End If
    
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim layout As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each layout In listaDisponibili
            lstDisponibili.AddItem layout.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = layout.idElementoLista
            i = i + 1
        Next layout
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim layout As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each layout In listaSelezionati
            lstSelezionati.AddItem layout.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = layout.idElementoLista
            i = i + 1
        Next layout
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idLayout As Long
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idLayout = lstDisponibili.ItemData(i - 1)
            chiaveLayout = ChiaveId(idLayout)
            Set layout = listaDisponibili.Item(chiaveLayout)
            Call listaSelezionati.Add(layout, chiaveLayout)
            Call listaDisponibili.Remove(chiaveLayout)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaSelezionati
    
    MousePointer = mousePointerOld
End Sub


Private Sub CreaTabellaAppoggioTipiSupportoLayout()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_TIPILAYOUT_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDTIPO NUMBER(10), NOME VARCHAR2(4000))"
    
    Call EliminaTabellaAppoggioTipiSupportoLayout
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioTipiSupportoLayout()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioTipiSupportoLayout()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idTipoSupp As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Integer
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioTipiSupportoLayout = New Collection
    
'    sql = "SELECT DISTINCT LSTS.IDTIPOSUPPORTO ID" & _
'        " FROM TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
'        " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO"
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set recordTemporaneo = New clsElementoLista
'            recordTemporaneo.idElementoLista = rec("ID").Value
'            Call listaAppoggioTipiSupportoLayout.Add(recordTemporaneo)
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close

    If idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT LSTS.IDTIPOSUPPORTO ID" & _
            " FROM TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
            " AND TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
            " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    Else
        sql = "SELECT DISTINCT LSTS.IDTIPOSUPPORTO ID" & _
            " FROM TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("ID").Value
            Call listaAppoggioTipiSupportoLayout.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    
    For Each recordTemporaneo In listaAppoggioTipiSupportoLayout
        campoNome = ""
        ' aggiunta una condizione rownum per non inserire nulla
        sql = "SELECT TS.IDTIPOSUPPORTO, LS.IDLAYOUTSUPPORTO, LS.CODICE CODICE" & _
            " FROM LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
            " AND LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
            " AND TS.IDTIPOSUPPORTO= " & recordTemporaneo.idElementoLista & _
            " AND ROWNUM < 0" & _
            " ORDER BY LS.NOME"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("CODICE"), campoNome & ";" & rec("CODICE"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qui sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioTipiSupportoLayout
        idTipoSupp = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDTIPO, NOME)" & _
            " VALUES (" & idTipoSupp & "," & _
            SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
'terza fase: ci aggiungo i tipi supporto digitali
    sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDTIPO, NOME)" & _
        " SELECT TS.IDTIPOSUPPORTO, TS.NOME" & _
        " FROM TSDIGCONSENTITOPERORG TSCPO, TIPOSUPPORTO TS" & _
        " WHERE TSCPO.idOrganizzazione = " & idOrganizzazioneSelezionata & _
        " AND TSCPO.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim tipoEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim n As Long

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    
On Error GoTo gestioneErrori

    tipoEliminabile = True
    If Not IsRecordEliminabile("IDTIPOSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("ORGANIZ_TIPOSUP_LAYOUTSUP")
        tipoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDTIPOSUPPORTO", "SUPPORTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("SUPPORTO")
        tipoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDTIPOSUPPORTO", "UTILIZZOLAYOUTSUPPORTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
        tipoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDTIPOSUPPORTO", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTOCPV")
        tipoEliminabile = False
    End If
    If Not IsRecordEliminabile("IDTIPOSUPPORTO", "LOTTOSUPPORTI", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("LOTTOSUPPORTI")
        tipoEliminabile = False
    End If
    
    If tipoEliminabile Then
        sql = "DELETE FROM LAYOUTSUPPORTO_TIPOSUPPORTO" & _
            " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        sql = "DELETE FROM TIPOSUPPORTO WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", _
                                "Il Tipo Supporto selezionato", tabelleCorrelate)
    End If
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Sub SvuotaDisponibili()
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    
    For Each layout In listaDisponibili
        chiaveLayout = ChiaveId(layout.idElementoLista)
        Call listaSelezionati.Add(layout, chiaveLayout)
    Next layout
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SvuotaSelezionati()
    Dim idLayout As Long
    Dim listaLayoutNonEliminabili As Collection
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    Dim condizioneSql As String
    
    Set listaLayoutNonEliminabili = New Collection
    For Each layout In listaSelezionati
        idLayout = layout.idElementoLista
        condizioneSql = " AND IDTIPOSUPPORTO = " & idRecordSelezionato
'        If IsRecordEliminabile("IDLAYOUTSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idLayout)) Then
        If IsRecordEliminabile("IDLAYOUTSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idLayout), condizioneSql) Then
            chiaveLayout = ChiaveId(idLayout)
            Call listaDisponibili.Add(layout, chiaveLayout)
            Call listaSelezionati.Remove(chiaveLayout)
        Else
            Call listaLayoutNonEliminabili.Add(layout.descrizioneElementoLista)
        End If
    Next layout
    If listaLayoutNonEliminabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", ArgomentoMessaggio(listaLayoutNonEliminabili), "ORGANIZ_TIPOSUP_LAYOUTSUP")
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim listaLayoutNonEliminabili As Collection
    Dim idLayout As Long
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    Dim condizioneSql As String
    
    Set listaLayoutNonEliminabili = New Collection
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idLayout = lstSelezionati.ItemData(i - 1)
            chiaveLayout = ChiaveId(idLayout)
            Set layout = listaSelezionati.Item(chiaveLayout)
'            If IsRecordEliminabile("IDLAYOUTSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idLayout)) Then
            condizioneSql = " AND IDTIPOSUPPORTO = " & idRecordSelezionato
            If IsRecordEliminabile("IDLAYOUTSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idLayout), condizioneSql) Then
                Call listaDisponibili.Add(layout, chiaveLayout)
                Call listaSelezionati.Remove(chiaveLayout)
            Else
                Call listaLayoutNonEliminabili.Add(layout.descrizioneElementoLista)
            End If
        End If
    Next i
    If listaLayoutNonEliminabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", ArgomentoMessaggio(listaLayoutNonEliminabili), "ORGANIZ_TIPOSUP_LAYOUTSUP")
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmbTipoSupportoSIAE_Click()
    If Not internalEvent Then
        idTipoSupportoSIAESelezionato = cmbTipoSupportoSIAE.ItemData(cmbTipoSupportoSIAE.ListIndex)
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, includiNessuno As Boolean)
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Integer

    cmb.Clear

    Call ApriConnessioneBD_ORA

    sql = strSQL

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

    If includiNessuno Then
        If i <= 0 Then
            i = 1
        Else
            'Do Nothing
        End If
        cmb.AddItem "<nessuno>"
        cmb.ItemData(i - 1) = idNessunElementoSelezionato
    End If

    Call ChiudiConnessioneBD_ORA

End Sub
