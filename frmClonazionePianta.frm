VERSION 5.00
Begin VB.Form frmClonazionePianta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   15
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   10
      Top             =   8040
      Width           =   1155
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9540
      TabIndex        =   14
      Top             =   240
      Width           =   2295
   End
   Begin VB.Frame fraDatiNuovaPianta 
      Caption         =   "Dati nuova pianta"
      Height          =   5895
      Left            =   120
      TabIndex        =   11
      Top             =   660
      Width           =   9855
      Begin VB.ListBox lstOrgDisponibili 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   1500
         TabIndex        =   2
         Top             =   2700
         Width           =   3555
      End
      Begin VB.CommandButton cmdOrgSvuotaSelezionati 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   5100
         TabIndex        =   5
         Top             =   4320
         Width           =   435
      End
      Begin VB.CommandButton cmdOrgDisponibile 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   5100
         TabIndex        =   4
         Top             =   3540
         Width           =   435
      End
      Begin VB.CommandButton cmdOrgSelezionata 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   5100
         TabIndex        =   3
         Top             =   2760
         Width           =   435
      End
      Begin VB.ListBox lstOrgSelezionate 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   5580
         MultiSelect     =   2  'Extended
         TabIndex        =   6
         Top             =   2700
         Width           =   3555
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1500
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   1020
         Width           =   7635
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   0
         Top             =   600
         Width           =   3255
      End
      Begin VB.CheckBox chkImportaFasceSequenze 
         Caption         =   "Importa fasce/sequenze"
         Height          =   195
         Left            =   1500
         TabIndex        =   7
         Top             =   5400
         Width           =   3255
      End
      Begin VB.Label lblOrgSelezionate 
         Alignment       =   2  'Center
         Caption         =   "Selezionate"
         Height          =   195
         Left            =   5580
         TabIndex        =   20
         Top             =   2460
         Width           =   3555
      End
      Begin VB.Label lblOrgDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Height          =   195
         Left            =   1500
         TabIndex        =   19
         Top             =   2460
         Width           =   3555
      End
      Begin VB.Label lblOrganizzazioni 
         Alignment       =   2  'Center
         Caption         =   "ORGANIZZAZIONI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1500
         TabIndex        =   18
         Top             =   2220
         Width           =   7635
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   300
         TabIndex        =   12
         Top             =   660
         Width           =   1035
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Clonazione della Pianta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   6975
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9540
      TabIndex        =   16
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmClonazionePianta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private newNome As String
Private newDescrizione As String
Private idPiantaSelezionata As Long
Private nomePiantaSelezionata As String
Private strCodicePiantaOrganizzazioneSelezionata As String
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
'Private listaIdOrgDisponibili As Collection
'Private listaNomeOrgSelezionate As Collection
'Private listaNomeOrgDisponibili As Collection
'Private listaCodiceOrgSelezionate As Collection
'Private listaIdOrgSelezionate As Collection
Private listaDisponibili As Collection
Private listaSelezionati As Collection
'Private listaCampiValoriUnici As Collection

Private internalEvent As Boolean
Private importaFasceSequenze As ValoreBooleanoEnum
Private exitCode As ExitCodeEnum
Private exitCodeFormDettagli As ExitCodeEnum

Private Sub AggiornaAbilitazioneControlli()
    txtInfo1.Enabled = False
    lblInfo1.Caption = "Pianta"
    cmdConferma.Enabled = (Trim(txtNome.Text) <> "")
End Sub

Public Sub Init()
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstDisponibili
'    Set listaIdOrgSelezionate = New Collection
'    Set listaNomeOrgSelezionate = New Collection
'    Set listaCodiceOrgSelezionate = New Collection
    Set listaSelezionati = New Collection
    Call Me.Show(vbModal)
End Sub

Private Sub AssegnaValoriCampi()
    txtInfo1.Text = nomePiantaSelezionata
End Sub

Public Sub SetIdPiantaSelezionata(idP As Long)
    idPiantaSelezionata = idP
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    
    ValoriCampiOK = True
    Set listaNonConformitā = New Collection
    newNome = Trim(txtNome.Text)
    If ViolataUnicitā("PIANTA", "NOME = " & SqlStringValue(newNome), "", _
        "", "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(newNome) & _
            " č giā presente in DB;")
    End If
    newDescrizione = Trim(txtDescrizione.Text)
    importaFasceSequenze = IIf(chkImportaFasceSequenze.Value = vbChecked, VB_VERO, VB_FALSO)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If
    
End Function

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT NOME FROM PIANTA WHERE IDPIANTA =" & idPiantaSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomePiantaSelezionata = rec("NOME")
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call PulisciValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    
    If ValoriCampiOK Then
        dataOraInizio = FormatDateTime(Now, vbGeneralDate)
        If IsClonazionePiantaOK(idPiantaSelezionata, newNome, newDescrizione, _
                                listaSelezionati, importaFasceSequenze) Then
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("NotificaClonazionePianta", newNome, nomePiantaSelezionata, dataOraInizio, dataOraFine)
        Else
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("ErroreClonazione", dataOraInizio, dataOraFine)
        End If
    End If
    Call Esci
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub PulisciValoriCampi()
    txtNome.Text = ""
    txtDescrizione.Text = ""
    chkImportaFasceSequenze.Value = vbUnchecked
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & "'" & newNome & "'")
'End Sub

Private Sub lstOrgDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstOrgDisponibili, lstOrgDisponibili.Text)
End Sub

Private Sub lstOrgSelezionate_Click()
    Call VisualizzaListBoxToolTip(lstOrgSelezionate, lstOrgSelezionate.Text)
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    If idPiantaSelezionata <> idNessunElementoSelezionato Then
        sql = "SELECT ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE ID, NOME, CODICEPIANTA" & _
            " FROM ORGANIZZAZIONE, ORGANIZZAZIONE_PIANTA" & _
            " WHERE ORGANIZZAZIONE_PIANTA.IDPIANTA = " & idPiantaSelezionata & _
            " AND ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = ORGANIZZAZIONE.IDORGANIZZAZIONE" & _
            " ORDER BY NOME"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set organizzazioneCorrente = New clsElementoLista
                organizzazioneCorrente.nomeElementoLista = rec("NOME")
                organizzazioneCorrente.descrizioneElementoLista = rec("NOME")
                organizzazioneCorrente.idElementoLista = rec("ID").Value
                chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
                Call listaDisponibili.Add(organizzazioneCorrente, chiaveOrganizzazione)
                rec.MoveNext
            Wend
        End If
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeOrganizzazione As String
    Dim codicePiantaOrganizzazione As Integer
    Dim idOrganizzazione As Long
    Dim label As String
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD

'    Set listaNomeOrgSelezionate = New Collection
'    Set listaCodiceOrgSelezionate = New Collection
'    Set listaIdOrgSelezionate = New Collection
    Set listaSelezionati = New Collection
        
    If idPiantaSelezionata <> idNessunElementoSelezionato Then
        sql = "SELECT ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE ID, NOME, CODICEPIANTA" & _
            " FROM ORGANIZZAZIONE, ORGANIZZAZIONE_PIANTA" & _
            " WHERE ORGANIZZAZIONE_PIANTA.IDPIANTA = " & idPiantaSelezionata & _
            " AND ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = ORGANIZZAZIONE.IDORGANIZZAZIONE" & _
            " ORDER BY NOME"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set organizzazioneCorrente = New clsElementoLista
                organizzazioneCorrente.codiceElementoLista = rec("CODICEPIANTA").Value
                organizzazioneCorrente.nomeElementoLista = rec("NOME")
'                organizzazioneCorrente.descrizioneElementoLista = rec("NOME")
                label = organizzazioneCorrente.codiceElementoLista & _
                    " - " & organizzazioneCorrente.nomeElementoLista
                organizzazioneCorrente.descrizioneElementoLista = label
                organizzazioneCorrente.idElementoLista = rec("ID").Value
                chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
                Call listaSelezionati.Add(organizzazioneCorrente, chiaveOrganizzazione)
                rec.MoveNext
            Wend
        End If
        
        rec.Close
        Call ChiudiConnessioneBD
        
        Call lstSelezionate_Init
    Else
        'do Nothing
    End If
    
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOrgDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each organizzazione In listaDisponibili
            lstOrgDisponibili.AddItem organizzazione.descrizioneElementoLista
            lstOrgDisponibili.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim label As String
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOrgSelezionate.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each organizzazione In listaSelezionati
            label = organizzazione.codiceElementoLista & " - " & organizzazione.nomeElementoLista
            organizzazione.descrizioneElementoLista = label
            lstOrgSelezionate.AddItem label
            lstOrgSelezionate.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For i = 1 To lstOrgSelezionate.ListCount
        If lstOrgSelezionate.Selected(i - 1) Then
            idOrganizzazione = lstOrgSelezionate.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaSelezionati.Item(chiaveOrganizzazione)
            organizzazione.descrizioneElementoLista = organizzazione.nomeElementoLista
            Call listaDisponibili.Add(organizzazione, chiaveOrganizzazione)
            Call listaSelezionati.Remove(chiaveOrganizzazione)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionate_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For i = 1 To lstOrgDisponibili.ListCount
        If lstOrgDisponibili.Selected(i - 1) Then
            idOrganizzazione = lstOrgDisponibili.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaDisponibili.Item(chiaveOrganizzazione)
            organizzazione.codiceElementoLista = strCodicePiantaOrganizzazioneSelezionata
            Call listaSelezionati.Add(organizzazione, chiaveOrganizzazione)
            Call listaDisponibili.Remove(chiaveOrganizzazione)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionate_Init
End Sub

Private Sub SvuotaSelezionati()
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaSelezionati
        organizzazione.descrizioneElementoLista = organizzazione.nomeElementoLista
        chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
        Call listaDisponibili.Add(organizzazione, chiaveOrganizzazione)
    Next organizzazione
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionate_Init
End Sub

Private Sub cmdOrgDisponibile_Click()
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdOrgSelezionata_Click()
    If lstOrgDisponibili.SelCount = 1 Then
        idOrganizzazioneSelezionata = lstOrgDisponibili.ItemData(lstOrgDisponibili.ListIndex)
        nomeOrganizzazioneSelezionata = lstOrgDisponibili.Text
        Call CaricaFormDettagliOrganizzazionePianta
        If exitCodeFormDettagli = EC_CONFERMA Then
'            If IsCodiceUnivoco Then
                Call SpostaInLstSelezionati
'            Else
'                Call frmMessaggio.Visualizza("ErroreDuplicazioneCodiceConstraint", codicePiantaOrganizzazioneSelezionata)
'            End If
        Else
            'Do Nothing
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormDettagliOrganizzazionePianta()
    Dim nomeP As String
    
    nomeP = Trim(txtNome.Text)
    Call frmDettagliOrganizzazionePianta.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmDettagliOrganizzazionePianta.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmDettagliOrganizzazionePianta.SetNomePianta(nomeP)
    Call frmDettagliOrganizzazionePianta.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmDettagliOrganizzazionePianta.SetOperazioneSuPianta(A_CLONA)
    Call frmDettagliOrganizzazionePianta.Init
End Sub

Private Sub cmdOrgSvuotaSelezionati_Click()
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetCodicePiantaOrganizzazioneSelezionata(cod As String)
    strCodicePiantaOrganizzazioneSelezionata = cod
End Sub

Public Sub SetExitCodeFormDettagliPiantaOrganizzazione(exCode As ExitCodeEnum)
    exitCodeFormDettagli = exCode
End Sub

