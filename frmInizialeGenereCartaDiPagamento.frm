VERSION 5.00
Begin VB.Form frmInizialeGenereCartaDiPagamento 
   Caption         =   "Genere Carta di Pagamento"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11970
   LinkTopic       =   "Form1"
   ScaleHeight     =   8730
   ScaleWidth      =   11970
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   16
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   915
      Left            =   10140
      TabIndex        =   14
      Top             =   600
      Width           =   1695
      Begin VB.CommandButton cmdCategorieCartaDiPagamento 
         Caption         =   "Categorie Carta di Pagamento"
         Height          =   435
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   6075
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   9855
      Begin VB.ListBox lstOrganizzazioniDisponibili 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   360
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   1980
         Width           =   4335
      End
      Begin VB.CommandButton cmdSvuotaOrgSelezionati 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   4740
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   4860
         Width           =   435
      End
      Begin VB.CommandButton cmdOrgDisponibile 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   4740
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   3900
         Width           =   435
      End
      Begin VB.CommandButton cmdOrgSelezionato 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   4740
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   2940
         Width           =   435
      End
      Begin VB.ListBox lstOrganizzazioniSelezionate 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   5220
         MultiSelect     =   2  'Extended
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   1980
         Width           =   4335
      End
      Begin VB.CommandButton cmdSvuotaOrgDisponibili 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   4740
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   1980
         Width           =   435
      End
      Begin VB.TextBox txtDescrizione 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   2
         Top             =   960
         Width           =   7875
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   1
         Top             =   480
         Width           =   3315
      End
      Begin VB.Label lblSelezionati 
         Alignment       =   2  'Center
         Caption         =   "Selezionate"
         Height          =   195
         Left            =   5220
         TabIndex        =   13
         Top             =   1740
         Width           =   4335
      End
      Begin VB.Label lblDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Height          =   195
         Left            =   360
         TabIndex        =   12
         Top             =   1740
         Width           =   4335
      End
      Begin VB.Label lblOrganizzazioni 
         Alignment       =   2  'Center
         Caption         =   "Organizzazioni"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   360
         TabIndex        =   11
         Top             =   1500
         Width           =   9255
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   255
         Left            =   60
         TabIndex        =   10
         Top             =   1020
         Width           =   1455
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   600
         TabIndex        =   9
         Top             =   540
         Width           =   915
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeGenereCartaDiPagamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idGenereCartaDiPagamentoSelezionato As Long
Private idCausaleProtezionePersonaleSelezionata As Long
Private nome As String
Private descrizione As String
Private listaOrganizzazioniDisponibili As Collection
Private listaOrganizzazioniSelezionate As Collection
Private isRecordEditabile As Boolean

Private internalEvent As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalit�FormCorrente As AzioneEnum

Public Sub Init()
    
    Call Variabili_Init
    Call lstOrganizzazioniDisponibili.Clear
    Call lstOrganizzazioniSelezionate.Clear
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            idGenereCartaDiPagamentoSelezionato = idNessunElementoSelezionato
            isRecordEditabile = True
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
    End Select
    
    Call CaricaValoriLstOrganizzazioniDisponibili
    Call CaricaValoriLstOrganizzazioniSelezionati
    
    Call AggiornaAbilitazioneControlli
    
    Call frmInizialeGenereCartaDiPagamento.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    nome = ""
    descrizione = ""
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdGenereCartaDiPagamentoSelezionato(id As Long)
    idGenereCartaDiPagamentoSelezionato = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    cmdSvuotaOrgDisponibili.Enabled = False

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Genere Carta di Pagamento"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Genere Carta di Pagamento configurato"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Genere Carta di Pagamento configurato"
    End Select
    
    cmdConferma.Enabled = (Trim(txtNome.Text) <> "")
    fraDati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    
    fraAzioni.Visible = (modalit�FormCorrente = A_MODIFICA)
                                
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
        
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaGenereCartaDiPagamento.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
    sql = "SELECT NOME, DESCRIZIONE" & _
        " FROM GENERECARTADIPAGAMENTO WHERE IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        nome = rec("NOME")
        descrizione = rec("DESCRIZIONE")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = nome
    txtDescrizione.Text = descrizione
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdCategorieCartaLIS_Click()

End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
    
    If isRecordEditabile Then
        Select Case modalit�FormCorrente
            Case A_NUOVO
                If ValoriCampiOK = True Then
                    Call InserisciNellaBaseDati
                    stringaNota = "IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_GENERECARTADIPAGAMENTO, CCDA_GENERECARTADIPAGAMENTO, stringaNota, idGenereCartaDiPagamentoSelezionato)
                    Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                    Call SetModalit�Form(A_MODIFICA)
                    Call AggiornaAbilitazioneControlli
                End If
            Case A_MODIFICA
                If ValoriCampiOK = True Then
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_GENERECARTADIPAGAMENTO, CCDA_GENERECARTADIPAGAMENTO, stringaNota, idGenereCartaDiPagamentoSelezionato)
                    Call frmMessaggio.Visualizza("NotificaModificaDati")
                    Call Esci
                End If
            Case A_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_GENERECARTADIPAGAMENTO, CCDA_GENERECARTADIPAGAMENTO, stringaNota, idGenereCartaDiPagamentoSelezionato)
                Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
                Call Esci
        End Select
    End If
    Call frmSceltaGenereCartaDiPagamento.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub cmdOrgDisponibile_Click()
    Call SpostaInLstOrganizzazioniDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdOrgSelezionato_Click()
    Call SpostaInLstOrganizzazioniSelezionate
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaOrgSelezionati_Click()
    Call SvuotaOrganizzazioniSelezionate
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstOrganizzazioniDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstOrganizzazioniDisponibili, lstOrganizzazioniDisponibili.Text)
End Sub

Private Sub lstOrganizzazioniSelezionate_Click()
    Call VisualizzaListBoxToolTip(lstOrganizzazioniSelezionate, lstOrganizzazioniSelezionate.Text)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim condizioniSQL As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM ORG_GENERECARTADIPAGAMENTO WHERE IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM GENERECARTADIPAGAMENTO WHERE IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String
    
On Error Resume Next

    ValoriCampiOK = True
    Set listaNonConformit� = New Collection
    condizioneSql = ""
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_ELIMINA Then
        condizioneSql = "IDGENERECARTADIPAGAMENTO <> " & idGenereCartaDiPagamentoSelezionato
    End If
    
    nome = Trim(txtNome.Text)
    If ViolataUnicit�("GENERECARTADIPAGAMENTO", "NOME = " & SqlStringValue(nome), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nome) & _
            " � gi� presente in DB;")
    End If
    descrizione = txtDescrizione.Text
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function

Private Sub cmdCategorieCartaDiPagamento_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraCategorieCartaDiPagamento
    
    MousePointer = mousePointerOld
End Sub
Private Sub ConfiguraCategorieCartaDiPagamento()
    Call CaricaFormConfigurazioneCategoriaCartaDiPagamento
End Sub
Private Sub CaricaFormConfigurazioneCategoriaCartaDiPagamento()
    Call frmConfigurazioneCategoriaCartaDiPagamento.SetIdGenereCartaDiPagamentoSelezionato(idGenereCartaDiPagamentoSelezionato)
    Call frmConfigurazioneCategoriaCartaDiPagamento.SetNomeGenereCartaDiPagamentoSelezionato(nome)
    Call frmConfigurazioneCategoriaCartaDiPagamento.Init
End Sub

Private Sub CaricaValoriLstOrganizzazioniDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    Dim idTipoStatoOrganizzazione As TipoStatoOrganizzazioneEnum
        
    Set listaOrganizzazioniDisponibili = New Collection
    
    Call ApriConnessioneBD
    
    sql = "SELECT O.IDORGANIZZAZIONE ID," & _
       " O.NOME || ' - ' || TSO.NOME ORG, O.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO" & _
       " FROM ORGANIZZAZIONE O, TIPOSTATOORGANIZZAZIONE TSO, ORG_GENERECARTADIPAGAMENTO O_G" & _
       " WHERE O.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
       " AND O.IDORGANIZZAZIONE = O_G.IDORGANIZZAZIONE(+)" & _
       " AND O_G.IDORGANIZZAZIONE IS NULL" & _
       " AND O_G.IDGENERECARTADIPAGAMENTO(+) = " & idGenereCartaDiPagamentoSelezionato & _
       " ORDER BY ORG"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            organizzazioneCorrente.nomeElementoLista = rec("ORG")
            organizzazioneCorrente.descrizioneElementoLista = rec("ORG")
            organizzazioneCorrente.idElementoLista = rec("ID").Value
            organizzazioneCorrente.idAttributoElementoLista = rec("TIPOSTATO").Value
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            Call listaOrganizzazioniDisponibili.Add(organizzazioneCorrente, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOrganizzazioniDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstOrganizzazioniSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    Dim idTipoStatoOrganizzazione As TipoStatoOrganizzazioneEnum
    Dim dirittiOperatore As clsDiritOperOrg
        
    Call ApriConnessioneBD

    Set listaOrganizzazioniSelezionate = New Collection
    
    sql = "SELECT ORG.IDORGANIZZAZIONE ID," & _
        " ORG.NOME || ' - ' || TSO.NOME ORG, ORG.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO" & _
        " FROM ORGANIZZAZIONE ORG, ORG_GENERECARTADIPAGAMENTO OG, TIPOSTATOORGANIZZAZIONE TSO" & _
        " WHERE OG.IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato & _
        " AND ORG.IDORGANIZZAZIONE = OG.IDORGANIZZAZIONE" & _
        " AND ORG.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
        " ORDER BY ORG"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            Set dirittiOperatore = New clsDiritOperOrg
            Call dirittiOperatore.Init
            organizzazioneCorrente.nomeElementoLista = rec("ORG")
            organizzazioneCorrente.descrizioneElementoLista = rec("ORG")
            organizzazioneCorrente.idElementoLista = rec("ID").Value
            organizzazioneCorrente.idAttributoElementoLista = rec("TIPOSTATO").Value
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            dirittiOperatore.idOrganizzazione = organizzazioneCorrente.idElementoLista
            Call listaOrganizzazioniSelezionate.Add(organizzazioneCorrente, chiaveOrganizzazione)
'            Call listaDirittiOperatoreSuOrganizzazione.Add(dirittiOperatore, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOrganizzazioniSelezionate_Init
    
End Sub
''''''''''''''''''''''''''''''''
Private Sub lstOrganizzazioniDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    Call lstOrganizzazioniDisponibili.Clear

    If Not (listaOrganizzazioniDisponibili Is Nothing) Then
        i = 1
        For Each organizzazione In listaOrganizzazioniDisponibili
            lstOrganizzazioniDisponibili.AddItem organizzazione.descrizioneElementoLista
            lstOrganizzazioniDisponibili.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOrganizzazioniSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    Call lstOrganizzazioniSelezionate.Clear

    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        i = 1
        For Each organizzazione In listaOrganizzazioniSelezionate
            lstOrganizzazioniSelezionate.AddItem organizzazione.descrizioneElementoLista
            lstOrganizzazioniSelezionate.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSvuotaOrgDisponibili_Click()
    Call SvuotaOrganizzazioniDisponibili
'    faseConfigurazioneOperatoriControllati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaOrganizzazioniDisponibili()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaOrganizzazioniDisponibili
        isConfigurabile = True
        Select Case organizzazione.idAttributoElementoLista
            Case TSO_IN_CONFIGURAZIONE
                isConfigurabile = True
            Case TSO_ATTIVA
                Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            Case TSO_ATTIVABILE
                isConfigurabile = False
        End Select
        If isConfigurabile Then
            chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
            Call listaOrganizzazioniSelezionate.Add(organizzazione, chiaveOrganizzazione)
            Call listaOrganizzazioniDisponibili.Remove(chiaveOrganizzazione)
        End If
    Next organizzazione
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub SpostaInLstOrganizzazioniDisponibili()
    Dim i As Integer
    Dim j As Integer
    Dim isConfigurabile As Boolean
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    Dim dirittiOperatore As clsDiritOperOrg
    
    For i = 1 To lstOrganizzazioniSelezionate.ListCount
        If lstOrganizzazioniSelezionate.Selected(i - 1) Then
            isConfigurabile = True
            idOrganizzazione = lstOrganizzazioniSelezionate.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaOrganizzazioniSelezionate.Item(chiaveOrganizzazione)
            
            Select Case organizzazione.idAttributoElementoLista
                Case TSO_IN_CONFIGURAZIONE
                    isConfigurabile = True
                Case TSO_ATTIVA
                    Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                Case TSO_ATTIVABILE
                    isConfigurabile = False
            End Select
            If isConfigurabile Then
                Call listaOrganizzazioniDisponibili.Add(organizzazione, chiaveOrganizzazione)
                Call listaOrganizzazioniSelezionate.Remove(chiaveOrganizzazione)
            End If
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub SvuotaOrganizzazioniSelezionate()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaOrganizzazioniSelezionate
        isConfigurabile = True
        Select Case organizzazione.idAttributoElementoLista
            Case TSO_IN_CONFIGURAZIONE
                isConfigurabile = True
            Case TSO_ATTIVA
                Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            Case TSO_ATTIVABILE
                isConfigurabile = False
        End Select
        If isConfigurabile Then
            chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
            Call listaOrganizzazioniDisponibili.Add(organizzazione, chiaveOrganizzazione)
            Call listaOrganizzazioniSelezionate.Remove(chiaveOrganizzazione)
        End If
    Next organizzazione
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim numeroVociLista As Long
    Dim numeroRecordInseritiSimultaneamente As Integer
    Dim posizione As Long
    Dim operatore As clsElementoLista
    Dim organizzazione As clsElementoLista
    Dim condizioneSql As String
    Dim sqlProd As String
    Dim sqlTar As String
    Dim sqlTipoOp As String
    Dim sql0 As String
    Dim sql1 As String
    Dim dirittiOperatore As clsDiritOperOrg

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    Call SETAConnection.BeginTrans

'   AGGIORNAMENTO TABELLA GENERECARTADIPAGAMENTO
    sql = "UPDATE GENERECARTADIPAGAMENTO SET"
    sql = sql & " NOME = " & SqlStringValue(nome) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizione)
    sql = sql & " WHERE IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
    SETAConnection.Execute sql, n, adCmdText

'   AGGIORNAMENTO TABELLA OPERATORE_ORGANIZZAZIONE
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        sql = "DELETE FROM ORG_GENERECARTADIPAGAMENTO"
        sql = sql & " WHERE IDGENERECARTADIPAGAMENTO = " & idGenereCartaDiPagamentoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        If listaOrganizzazioniSelezionate.count > 0 Then
            sql = "INSERT INTO ORG_GENERECARTADIPAGAMENTO (IDGENERECARTADIPAGAMENTO, IDORGANIZZAZIONE)" & _
                " SELECT " & idGenereCartaDiPagamentoSelezionato & "," & _
                " IDORGANIZZAZIONE FROM ORGANIZZAZIONE WHERE IDORGANIZZAZIONE IN ("
            For i = 1 To listaOrganizzazioniSelezionate.count - 1
                Set organizzazione = listaOrganizzazioniSelezionate.Item(i)
                sql = sql & organizzazione.idElementoLista & ","
            Next i
            Set organizzazione = listaOrganizzazioniSelezionate.Item(i)
            sql = sql & organizzazione.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
    End If

    Call SETAConnection.CommitTrans
    Call ChiudiConnessioneBD

    Exit Sub

gestioneErrori:
    Call SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub SpostaInLstOrganizzazioniSelezionate()
    Dim i As Integer
    Dim isConfigurabile As Boolean
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    Dim dirittiOperatore As clsDiritOperOrg
    
    For i = 1 To lstOrganizzazioniDisponibili.ListCount
        If lstOrganizzazioniDisponibili.Selected(i - 1) Then
            isConfigurabile = True
            idOrganizzazione = lstOrganizzazioniDisponibili.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaOrganizzazioniDisponibili.Item(chiaveOrganizzazione)
            Select Case organizzazione.idAttributoElementoLista
                Case TSO_IN_CONFIGURAZIONE
                    isConfigurabile = True
                Case TSO_ATTIVA
                    Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                Case TSO_ATTIVABILE
                    isConfigurabile = False
            End Select
            If isConfigurabile Then
                Call listaOrganizzazioniSelezionate.Add(organizzazione, chiaveOrganizzazione)
                Call listaOrganizzazioniDisponibili.Remove(chiaveOrganizzazione)
            End If
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim idNuovoGenereCartaDiPagamento As Long
    Dim n As Long
    Dim organizzazione As clsElementoLista
    Dim dirittiOperatore As clsDiritOperOrg
    Const STATO_CASSA_CHIUSA As Integer = 2
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovoGenereCartaDiPagamento = OttieniIdentificatoreDaSequenza("SQ_GENERECARTADIPAGAMENTO")
    sql = "INSERT INTO GENERECARTADIPAGAMENTO (IDGENERECARTADIPAGAMENTO, NOME, DESCRIZIONE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovoGenereCartaDiPagamento & ", "
    sql = sql & SqlStringValue(nome) & ", "
    sql = sql & SqlStringValue(descrizione) & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        For Each organizzazione In listaOrganizzazioniSelezionate
            sql = "INSERT INTO ORG_GENERECARTADIPAGAMENTO (IDGENERECARTADIPAGAMENTO, IDORGANIZZAZIONE)" & _
                " VALUES (" & _
                idNuovoGenereCartaDiPagamento & ", " & _
                organizzazione.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next organizzazione
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call frmSceltaGenereCartaDiPagamento.SetIdRecordSelezionato(idNuovoGenereCartaDiPagamento)
    Call SetIdGenereCartaDiPagamentoSelezionato(idNuovoGenereCartaDiPagamento)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

