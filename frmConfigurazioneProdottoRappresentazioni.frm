VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneProdottoRappresentazioni 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "3"
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   6
      Top             =   5760
      Width           =   435
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   30
      Top             =   3420
      Width           =   2775
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   29
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraQuery 
      Caption         =   "Selezione"
      Height          =   735
      Left            =   120
      TabIndex        =   26
      Top             =   4440
      Width           =   11715
      Begin VB.CommandButton cmdCerca 
         Caption         =   "Cerca"
         Height          =   315
         Left            =   10680
         TabIndex        =   4
         Top             =   240
         Width           =   795
      End
      Begin VB.ComboBox cmbSpettacoli 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6240
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Width           =   4035
      End
      Begin VB.ComboBox cmbTurni 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   4035
      End
      Begin VB.Label lblSpettacoli 
         Alignment       =   1  'Right Justify
         Caption         =   "Spettacoli"
         Height          =   255
         Left            =   5400
         TabIndex        =   28
         Top             =   300
         Width           =   735
      End
      Begin VB.Label lblTurni 
         Alignment       =   1  'Right Justify
         Caption         =   "Turni rappr."
         Height          =   255
         Left            =   60
         TabIndex        =   27
         Top             =   300
         Width           =   915
      End
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   5
      Top             =   5700
      Width           =   5580
   End
   Begin VB.ListBox lstSelezionati 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   6240
      MultiSelect     =   2  'Extended
      TabIndex        =   10
      Top             =   5700
      Width           =   5580
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   7
      Top             =   6180
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   8
      Top             =   6600
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   9
      Top             =   7020
      Width           =   435
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   18
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   17
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   16
      Top             =   240
      Width           =   1635
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoRappresentazioni 
      Height          =   330
      Left            =   240
      Top             =   2640
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoRappresentazioni 
      Height          =   2475
      Left            =   120
      TabIndex        =   19
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   4366
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   32
      Top             =   3180
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   31
      Top             =   3180
      Width           =   2775
   End
   Begin VB.Label lblListe 
      Alignment       =   2  'Center
      Caption         =   "lblListe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   25
      Top             =   5280
      Width           =   11655
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "lblDisponibili"
      Height          =   195
      Left            =   120
      TabIndex        =   24
      Top             =   5520
      Width           =   5535
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "lblSelezionati"
      Height          =   195
      Left            =   6240
      TabIndex        =   23
      Top             =   5520
      Width           =   5535
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Rappresentazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   21
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   20
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoRappresentazioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private isProdottoTMaster As Long
Private IsProdottoTDL As Long
Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idTurnoRappresentazioneSelezionato As Long
Private idStagioneSelezionata As Long
Private idStagioneCorrente As Long
Private idSpettacoloSelezionato As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private nomeTurnoRappresentazioneSelezionato As String
Private nomeStagioneSelezionata As String
Private nomeSpettacoloSelezionato As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private condizioniSQL As String
Private queryEseguita As Boolean
Private isProdottoAttivoSuTL As Boolean
Private isProdottoMonorappresentazione As Boolean
Private rateo As Long
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private rappresentazioniInseribili As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private tipoArea As TipoAreaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    lblListe.Caption = "SPETTACOLI - RAPPRESENTAZIONI"
    lblDisponibili.Caption = "Disponibili"
    lblSelezionati.Caption = "Selezionati"
    
    
    dgrConfigurazioneProdottoRappresentazioni.Caption = "RAPPRESENTAZIONI ASSOCIATE AL PRODOTTO"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoRappresentazioni.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbTurni.Clear
        Call cmbSpettacoli.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        cmbTurni.Enabled = False
        cmbSpettacoli.Enabled = False
        lblTurni.Enabled = False
        lblSpettacoli.Enabled = False
        lblListe.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdCerca.Enabled = False
        fraQuery.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = rappresentazioniInseribili
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoRappresentazioni.Enabled = False
        cmbTurni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
                                    Not queryEseguita)
        cmbSpettacoli.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
                                    Not queryEseguita)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTurni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSpettacoli.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblListe.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdCerca.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA And _
                                    Not queryEseguita)
        fraQuery.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And _
            (lstDisponibili.SelCount <= 1)
        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA) And _
            idClasseProdottoSelezionata <> CPR_BIGLIETTERIA_ORDINARIA
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (lstSelezionati.ListCount > 0 Or _
                               gestioneRecordGriglia = ASG_ELIMINA)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoRappresentazioni.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbTurni.Clear
        Call cmbSpettacoli.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        cmbTurni.Enabled = False
        cmbSpettacoli.Enabled = False
        lblTurni.Enabled = False
        lblSpettacoli.Enabled = False
        lblListe.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdCerca.Enabled = False
        fraQuery.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = rappresentazioniInseribili
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)) And _
                                    (IsConfigurataAlmenoUnaRappresentazione)
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
    If isProdottoTMaster Then
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
    If IsProdottoTDL Then
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = False
    End If
    
End Sub

Public Sub SetIsProdottoTMaster(b As ValoreBooleanoEnum)
    isProdottoTMaster = b
End Sub

Public Sub SetIsProdottoTDL(b As ValoreBooleanoEnum)
    IsProdottoTDL = b
End Sub

Private Function RappresentazioneAssociabileAProdotto_old(idRappresentazione As Long) As Boolean
    Dim elencoCorrelandi As String
    
    elencoCorrelandi = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
    If (Not ProdottoSelezionatoHaTitoli Or NumeroTotaleTitoli(elencoCorrelandi) = 0) And _
            NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione) And _
            TuttiProdottiCorrelandiInConfigurazione(idRappresentazione) Then
        RappresentazioneAssociabileAProdotto_old = True
    Else
        RappresentazioneAssociabileAProdotto_old = False
    End If
End Function

Private Function RappresentazioneAssociabileAProdotto(idRappresentazione As Long) As Boolean
    Dim elencoCorrelandi As String
    Dim correlandiHannoTitoliEmessi As Boolean
    
    elencoCorrelandi = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
'    correlandiHannoTitoliEmessi = (NumeroTotaleTitoli(elencoCorrelandi) > 0)
    correlandiHannoTitoliEmessi = EsistonoTitoli(elencoCorrelandi)
    If (Not ProdottoSelezionatoHaTitoli Or Not correlandiHannoTitoliEmessi Or _
            (correlandiHannoTitoliEmessi And Not StessiPostiVendutiSuProdottoCorrenteECorrelandi(elencoCorrelandi))) And _
            NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione) And _
            TuttiProdottiCorrelandiInConfigurazione(idRappresentazione) Then
        RappresentazioneAssociabileAProdotto = True
    Else
        RappresentazioneAssociabileAProdotto = False
    End If
End Function

Private Function NumeroTotaleTitoli(listaIdProdotto As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numeroTit As Long
    
    Call ApriConnessioneBD
    
'NOTA: NON C'E' BISOGNO DI RENDERE RANSAZIONALE QUESTA QUERY, PERCHé I PRODOTTI
'CON TITOLI EMESSI NON SONO ELIMINABILI
    If listaIdProdotto <> "" Then
'        sql = "SELECT COUNT(*) NUMTIT FROM TITOLO WHERE IDPRODOTTO IN " & listaIdProdotto
        sql = "SELECT COUNT(T.IDTITOLO) NUMTIT"
        sql = sql & " FROM TITOLO T, STATOTITOLO ST"
        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO
        sql = sql & " AND IDPRODOTTO IN " & listaIdProdotto
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        numeroTit = rec("NUMTIT")
        rec.Close
    Else
        numeroTit = 0
    End If
    
    Call ChiudiConnessioneBD
    
    NumeroTotaleTitoli = numeroTit
End Function

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
    Dim numero As Long
    
    numero = adcConfigurazioneProdottoRappresentazioni.Recordset.RecordCount
    If numero <> rateo Then
        Call frmMessaggio.Visualizza("AvvertimentoRateoNonCorretto")
    End If
    Call CaricaFormConfigurazioneCausaliProtezione
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    queryEseguita = False
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdCerca_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EseguiRicercaRappresentazioni
    
    MousePointer = mousePointerOld
End Sub

Private Sub EseguiRicercaRappresentazioni()
    queryEseguita = True
    Call AggiornaAbilitazioneControlli
    Call RilevaValoriCampi
    Call ComponiQueryRicercaRappresentazioni
    Call CaricaValoriLstDisponibili
'    Set listaNomeSelezionati = New Collection
'    Set listaIdSelezionati = New Collection
    Set listaSelezionati = New Collection
End Sub

Private Sub ComponiQueryRicercaRappresentazioni()
    
    condizioniSQL = ""
    
    Select Case idTurnoRappresentazioneSelezionato
    Case idTuttiGliElementiSelezionati
        condizioniSQL = condizioniSQL & _
                " (RAPPRESENTAZIONE.IDRAPPRESENTAZIONE IN (SELECT IDRAPPRESENTAZIONE FROM RAPPRESENTAZIONE_TURNORAPPR)) "
    Case idNessunElementoSelezionato
        condizioniSQL = condizioniSQL & _
                " (RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN (SELECT IDRAPPRESENTAZIONE FROM RAPPRESENTAZIONE_TURNORAPPR)) "
    Case Else ' in questo caso va fatto il join per IDTURNORAPPRESENTAZIONE
        condizioniSQL = condizioniSQL & _
                " (RAPPRESENTAZIONE_TURNORAPPR.IDTURNORAPPRESENTAZIONE = " & idTurnoRappresentazioneSelezionato & ")  AND" & _
                " (RAPPRESENTAZIONE_TURNORAPPR.IDRAPPRESENTAZIONE = RAPPRESENTAZIONE.IDRAPPRESENTAZIONE)"
    End Select
    
    ' join con spettacolo e stagione
    condizioniSQL = condizioniSQL & " AND RAPPRESENTAZIONE.IDSPETTACOLO = SPETTACOLO.IDSPETTACOLO"
    condizioniSQL = condizioniSQL & " AND SPETTACOLO.IDSTAGIONE = STAGIONE.IDSTAGIONE"
    
    Select Case idSpettacoloSelezionato
    Case idTuttiGliElementiSelezionati
    Case Else
        condizioniSQL = condizioniSQL & " AND (SPETTACOLO.IDSPETTACOLO = " & idSpettacoloSelezionato & ")"
    End Select
    
    Select Case idStagioneSelezionata
    Case idTuttiGliElementiSelezionati
        condizioniSQL = condizioniSQL & " AND (SPETTACOLO.IDSTAGIONE IS NOT NULL) "
    Case idNessunElementoSelezionato
        condizioniSQL = condizioniSQL & " AND (SPETTACOLO.IDSTAGIONE IS NULL) "
    Case Else
        condizioniSQL = condizioniSQL & " AND (SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & ") "
    End Select
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            queryEseguita = False
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            Select Case gestioneRecordGriglia
                Case ASG_INSERISCI_NUOVO
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_RAPPRESENTAZIONE, stringaNota, idProdottoSelezionato)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazioneProdottoRappresentazioni_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazioneProdottoRappresentazioni_Init
                Case ASG_ELIMINA
                    Call EliminaDallaBaseDati
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_RAPPRESENTAZIONE, stringaNota, idProdottoSelezionato)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazioneProdottoRappresentazioni_Init
                    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                    Call dgrConfigurazioneProdottoRappresentazioni_Init
                Case Else
                    'Do Nothing
            End Select
            If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
                rappresentazioniInseribili = (NumeroRappresentazioniAssociateAProdotto < 1)
            End If
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idRappresentazione As Long
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idRappresentazione = lstSelezionati.ItemData(i - 1)
            chiaveRappresentazione = ChiaveId(idRappresentazione)
            Set rappresentazione = listaSelezionati.Item(chiaveRappresentazione)
            Call listaDisponibili.Add(rappresentazione, chiaveRappresentazione)
            Call listaSelezionati.Remove(chiaveRappresentazione)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Call Esci
End Sub

Private Sub Precedente()
    Dim numero As Long
    
    numero = adcConfigurazioneProdottoRappresentazioni.Recordset.RecordCount
    If numero <> rateo Then
        Call frmMessaggio.Visualizza("AvvertimentoRateoNonCorretto")
    End If
    
    Unload Me
End Sub

Private Sub Esci()
    Dim numero As Long
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            numero = adcConfigurazioneProdottoRappresentazioni.Recordset.RecordCount
            If numero <> rateo Then
                Call frmMessaggio.Visualizza("AvvertimentoRateoNonCorretto")
            End If
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub
'
'Private Sub CaricaFormConfigurazioneTariffe()
'    Call frmConfigurazioneProdottoTariffe.SetIdProdottoSelezionato(idProdottoSelezionato)
'    Call frmConfigurazioneProdottoTariffe.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
'    Call frmConfigurazioneProdottoTariffe.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoTariffe.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoTariffe.SetIdPiantaSelezionata(idPiantaSelezionata)
'    Call frmConfigurazioneProdottoTariffe.SetIdStagioneSelezionata(idStagioneSelezionata)
'    Call frmConfigurazioneProdottoTariffe.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
'    Call frmConfigurazioneProdottoTariffe.SetRateo(rateo)
'    Call frmConfigurazioneProdottoTariffe.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
'    Call frmConfigurazioneProdottoTariffe.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
'    Call frmConfigurazioneProdottoTariffe.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
'
'    Call frmConfigurazioneProdottoTariffe.SetModalitāForm(A_NUOVO)
'    Call frmConfigurazioneProdottoTariffe.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazioneProdottoTariffe.Init
'End Sub

Private Sub CaricaFormConfigurazioneCausaliProtezione()
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoCausaliProtezione.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetRateo(rateo)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoCausaliProtezione.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoCausaliProtezione.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoCausaliProtezione.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoCausaliProtezione.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoCausaliProtezione.Init
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim SQLTurni As String
    Dim SQLSpettacoli As String

'    isRecordEditabile = True
    SQLTurni = "SELECT TURNORAPPRESENTAZIONE.IDTURNORAPPRESENTAZIONE AS ""ID""," & _
        " TURNORAPPRESENTAZIONE.NOME FROM TURNORAPPRESENTAZIONE" & _
        " ORDER BY NOME"
    SQLSpettacoli = "SELECT DISTINCT SPETTACOLO.IDSPETTACOLO AS ""ID"", SPETTACOLO.NOME FROM" & _
        " SPETTACOLO, RAPPRESENTAZIONE, VENUE_PIANTA, PIANTA WHERE" & _
        " (SPETTACOLO.IDSPETTACOLO = RAPPRESENTAZIONE.IDSPETTACOLO) AND" & _
        " (VENUE_PIANTA.IDVENUE = RAPPRESENTAZIONE.IDVENUE) AND" & _
        " (VENUE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
        " (PIANTA.IDPIANTA = " & idPiantaSelezionata & ") AND" & _
        " SPETTACOLO.IDSTAGIONE = " & idStagioneSelezionata & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbTurni, SQLTurni, "NOME")
    Call SelezionaElementoSuCombo(cmbTurni, idTuttiGliElementiSelezionati)
    Call CaricaValoriCombo(cmbSpettacoli, SQLSpettacoli, "NOME")
    Call SelezionaElementoSuCombo(cmbSpettacoli, idTuttiGliElementiSelezionati)
    
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeRappresentazione As String
    Dim idRappresentazione As Long
    Dim chiaveRappresentazione As String
    Dim rappresentazioneCorrente As clsElementoLista
    Dim condizioniSqlMonorappresentazione As String
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
'    sql = "SELECT DISTINCT RAPPRESENTAZIONE.IDRAPPRESENTAZIONE," & _
'        " SPETTACOLO.NOME AS ""Nome""," & _
'        " RAPPRESENTAZIONE.DATAORAINIZIO AS ""Inizio""" & _
'        " FROM RAPPRESENTAZIONE, SPETTACOLO, STAGIONE, TURNORAPPRESENTAZIONE, RAPPRESENTAZIONE_TURNORAPPR, VENUE_PIANTA" & _
'        " WHERE" & condizioniSql & _
'        " AND RAPPRESENTAZIONE.IDVENUE = VENUE_PIANTA.IDVENUE" & _
'        " AND VENUE_PIANTA.IDPIANTA = " & idPiantaSelezionata & _
'        " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN " & _
'        " (SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE" & _
'        " WHERE IDPRODOTTO = " & idProdottoSelezionato & ") AND" & _
'        " RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN " & _
'        " (SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE PR, PRODOTTO P" & _
'        " WHERE PR.IDPRODOTTO = P.IDPRODOTTO AND" & _
'        " P.IDORGANIZZAZIONE <> " & idOrganizzazioneSelezionata & ") " & _
'        " ORDER BY ""Nome"""
    condizioniSqlMonorappresentazione = ""
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
'            condizioniSqlMonorappresentazione = " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN" & _
'                " (SELECT R.IDRAPPRESENTAZIONE" & _
'                " FROM PRODOTTO_RAPPRESENTAZIONE R, PRODOTTO P" & _
'                " WHERE R.IDPRODOTTO = P.IDPRODOTTO" & _
'                " AND P.IDCLASSEPRODOTTO = " & CPR_BIGLIETTERIA_ORDINARIA & _
'                " AND P.IDPRODOTTO <> " & idProdottoSelezionato & _
'                " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND (R.IDTIPOSTATORECORD IS NULL" & _
'                " OR R.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))"
'        End If
'        sql = "SELECT DISTINCT RAPPRESENTAZIONE.IDRAPPRESENTAZIONE," & _
'            " SPETTACOLO.NOME AS ""Nome""," & _
'            " RAPPRESENTAZIONE.DATAORAINIZIO AS ""Inizio""" & _
'            " FROM RAPPRESENTAZIONE, SPETTACOLO, STAGIONE, TURNORAPPRESENTAZIONE, RAPPRESENTAZIONE_TURNORAPPR, VENUE_PIANTA" & _
'            " WHERE" & condizioniSQL & _
'            " AND RAPPRESENTAZIONE.IDVENUE = VENUE_PIANTA.IDVENUE" & _
'            " AND VENUE_PIANTA.IDPIANTA = " & idPiantaSelezionata & _
'            " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN " & _
'            " (SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN " & _
'            " (SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE PR, PRODOTTO P" & _
'            " WHERE PR.IDPRODOTTO = P.IDPRODOTTO" & _
'            " AND P.IDORGANIZZAZIONE <> " & idOrganizzazioneSelezionata & _
'            " AND (PR.IDTIPOSTATORECORD IS NULL" & _
'            " OR PR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND (P.IDTIPOSTATORECORD IS NULL" & _
'            " OR P.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & "))" & _
'            condizioniSqlMonorappresentazione & _
'            " ORDER BY ""Nome"""
'    Else
        If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
            condizioniSqlMonorappresentazione = " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN" & _
                " (SELECT R.IDRAPPRESENTAZIONE" & _
                " FROM PRODOTTO_RAPPRESENTAZIONE R, PRODOTTO P" & _
                " WHERE R.IDPRODOTTO = P.IDPRODOTTO" & _
                " AND P.IDCLASSEPRODOTTO = " & CPR_BIGLIETTERIA_ORDINARIA & _
                " AND P.IDPRODOTTO <> " & idProdottoSelezionato & _
                " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")"
        End If
        sql = "SELECT DISTINCT RAPPRESENTAZIONE.IDRAPPRESENTAZIONE," & _
            " SPETTACOLO.NOME AS ""Nome""," & _
            " RAPPRESENTAZIONE.DATAORAINIZIO AS ""Inizio""" & _
            " FROM RAPPRESENTAZIONE, SPETTACOLO, STAGIONE, TURNORAPPRESENTAZIONE, RAPPRESENTAZIONE_TURNORAPPR, VENUE_PIANTA" & _
            " WHERE" & condizioniSQL & _
            " AND RAPPRESENTAZIONE.IDVENUE = VENUE_PIANTA.IDVENUE" & _
            " AND VENUE_PIANTA.IDPIANTA = " & idPiantaSelezionata & _
            " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN " & _
            " (SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & ")" & _
            " AND RAPPRESENTAZIONE.IDRAPPRESENTAZIONE NOT IN" & _
            " (SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE PR, PRODOTTO P" & _
            " WHERE PR.IDPRODOTTO = P.IDPRODOTTO" & _
            " AND P.IDORGANIZZAZIONE <> " & idOrganizzazioneSelezionata & ")" & _
            condizioniSqlMonorappresentazione & _
            " ORDER BY ""Nome"", ""Inizio"""
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set rappresentazioneCorrente = New clsElementoLista
            rappresentazioneCorrente.idElementoLista = rec("IDRAPPRESENTAZIONE").Value
            rappresentazioneCorrente.nomeElementoLista = rec("NOME")
            rappresentazioneCorrente.descrizioneElementoLista = rec("Inizio") & " - " & rec("NOME")
            chiaveRappresentazione = ChiaveId(rappresentazioneCorrente.idElementoLista)
            Call listaDisponibili.Add(rappresentazioneCorrente, chiaveRappresentazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim rappresentazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each rappresentazione In listaDisponibili
            lstDisponibili.AddItem rappresentazione.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = rappresentazione.idElementoLista
            i = i + 1
        Next rappresentazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim rappresentazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each rappresentazione In listaSelezionati
            lstSelezionati.AddItem rappresentazione.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = rappresentazione.idElementoLista
            i = i + 1
        Next rappresentazione
    End If
           
    internalEvent = internalEventOld

End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoRappresentazioni.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstSelezionati
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idRappresentazione As Long
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    Dim listaRappresentazioniNonAssociabili As Collection
    
    Set listaRappresentazioniNonAssociabili = New Collection
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idRappresentazione = lstDisponibili.ItemData(i - 1)
            chiaveRappresentazione = ChiaveId(idRappresentazione)
            Set rappresentazione = listaDisponibili.Item(chiaveRappresentazione)
            If RappresentazioneAssociabileAProdotto(idRappresentazione) Then
                Call listaSelezionati.Add(rappresentazione, chiaveRappresentazione)
                Call listaDisponibili.Remove(chiaveRappresentazione)
            Else
                Call listaRappresentazioniNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
            End If
        End If
    Next i
    If listaRappresentazioniNonAssociabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguita", ArgomentoMessaggio(listaRappresentazioniNonAssociabili))
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaSelezionati()
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    
    For Each rappresentazione In listaSelezionati
        chiaveRappresentazione = ChiaveId(rappresentazione.idElementoLista)
        Call listaDisponibili.Add(rappresentazione, chiaveRappresentazione)
    Next rappresentazione
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Public Sub Init()

    If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
        rappresentazioniInseribili = (NumeroRappresentazioniAssociateAProdotto < 1)
    Else
        rappresentazioniInseribili = True
    End If
    queryEseguita = False
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoRappresentazioni_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoRappresentazioni_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoRappresentazioni.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoRappresentazioni_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
        
    Set d = adcConfigurazioneProdottoRappresentazioni
    
    sql = "SELECT R.IDRAPPRESENTAZIONE ID," & _
        " R.DATAORAINIZIO ""Inizio""," & _
        " S.NOME ""Spettacolo""," & _
        " S.IDSTAGIONE IDSTAG," & _
        " T.NOME AS ""Stagione""" & _
        " FROM RAPPRESENTAZIONE R, PRODOTTO_RAPPRESENTAZIONE P, SPETTACOLO S, STAGIONE T" & _
        " WHERE R.IDRAPPRESENTAZIONE = P.IDRAPPRESENTAZIONE" & _
        " AND R.IDSPETTACOLO = S.IDSPETTACOLO" & _
        " AND S.IDSTAGIONE = T.IDSTAGIONE" & _
        " AND P.IDPRODOTTO = " & idProdottoSelezionato & _
        " ORDER BY ""Inizio"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    If d.Recordset.RecordCount > 0 Then
        idStagioneCorrente = d.Recordset("IDSTAG")
    Else
        idStagioneCorrente = idNessunElementoSelezionato
    End If
    
    Set dgrConfigurazioneProdottoRappresentazioni.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub RilevaValoriCampi()
    nomeTurnoRappresentazioneSelezionato = Trim(cmbTurni.Text)
    nomeSpettacoloSelezionato = cmbSpettacoli.Text
    idTurnoRappresentazioneSelezionato = cmbTurni.ItemData(cmbTurni.ListIndex)
    idSpettacoloSelezionato = cmbSpettacoli.ItemData(cmbSpettacoli.ListIndex)
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim SqlConditions As String

    Call ApriConnessioneBD

'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM PRODOTTO_RAPPRESENTAZIONE WHERE" & _
'                " (IDPRODOTTO = " & idProdottoSelezionato & ") AND" & _
'                " (IDRAPPRESENTAZIONE = " & idRecordSelezionato & ")"
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            SqlConditions = " AND IDRAPPRESENTAZIONE = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("PRODOTTO_RAPPRESENTAZIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO, SqlConditions)
'        End If
'    Else
        sql = "DELETE FROM PRODOTTO_RAPPRESENTAZIONE WHERE" & _
            " (IDPRODOTTO = " & idProdottoSelezionato & ") AND" & _
            " (IDRAPPRESENTAZIONE = " & idRecordSelezionato & ")"
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub dgrConfigurazioneProdottoRappresentazioni_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoRappresentazioni
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Visible = False
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    cmb.AddItem "<nessuno>"
    cmb.ItemData(i) = idNessunElementoSelezionato
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    For i = 1 To cmb.ListCount
        If id = cmb.ItemData(i - 1) Then
            cmb.ListIndex = i - 1
        End If
    Next i
    
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Private Sub dgrConfigurazioneProdottoRappresentazioni_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoSistemiDiEmissione.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoSistemiDiEmissione.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Function IsConfigurataAlmenoUnaRappresentazione() As Boolean
    Dim n As Integer
    
    n = adcConfigurazioneProdottoRappresentazioni.Recordset.RecordCount
    If n = 0 Then
        IsConfigurataAlmenoUnaRappresentazione = False
    Else
        IsConfigurataAlmenoUnaRappresentazione = True
    End If
End Function

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Public Sub SetIdStagioneSelezionata(ids As Long)
    idStagioneSelezionata = ids
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaDisponibili
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaDisponibili()
    Dim rappresentazione As clsElementoLista
    Dim chiaveRappresentazione As String
    Dim listaRappresentazioniNonAssociabili As Collection
    
    Set listaRappresentazioniNonAssociabili = New Collection
    For Each rappresentazione In listaDisponibili
        chiaveRappresentazione = ChiaveId(rappresentazione.idElementoLista)
        If RappresentazioneAssociabileAProdotto(rappresentazione.idElementoLista) Then
            Call listaSelezionati.Add(rappresentazione, chiaveRappresentazione)
            Call listaDisponibili.Remove(chiaveRappresentazione)
        Else
            Call listaRappresentazioniNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
        End If
    Next rappresentazione
    If listaRappresentazioniNonAssociabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguita", ArgomentoMessaggio(listaRappresentazioniNonAssociabili))
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Function TuttiProdottiCorrelandiInConfigurazione(idRappresentazione As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elenco As String
    Dim esisteProdottoAttivo As Boolean
    
    Call ApriConnessioneBD
    
'MI PARE CHE QUESTA QUERY DOVREBBE RIMANERE COSI' ANCHE IN REGIME TRANSAZIONALE; VERIFICARE
    elenco = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
    If elenco <> "" Then
        sql = "SELECT COUNT(IDPRODOTTO) NUMPROD FROM PRODOTTO " & _
            " WHERE IDTIPOSTATOPRODOTTO <> " & TSP_IN_CONFIGURAZIONE & _
            " AND IDPRODOTTO IN " & elenco
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        esisteProdottoAttivo = (rec("NUMPROD") > 0)
        rec.Close
    Else
        esisteProdottoAttivo = False
    End If
    
    Call ChiudiConnessioneBD
    
    TuttiProdottiCorrelandiInConfigurazione = Not esisteProdottoAttivo
End Function

Private Function NessunProdottoCorrelandoBloccatoDaAltroUtente(idRappresentazione As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elenco As String
    Dim esisteBlocco As Boolean
    
    Call ApriConnessioneBD
    
'MI PARE CHE QUESTA QUERY DOVREBBE RIMANERE COSI' ANCHE IN REGIME TRANSAZIONALE; VERIFICARE
    elenco = ElencoProdottiCorrelandi(idProdottoSelezionato, idRappresentazione)
    If elenco <> "" Then
        sql = "SELECT COUNT(IDPRODOTTO) NUMBLOC FROM PRODOTTO" & _
            " WHERE IDPRODOTTO IN" & _
            " (SELECT IDPRODOTTO FROM CC_PRODOTTOUTILIZZATO" & _
            " WHERE UTENTE <> '" & getNomeMacchina & "')" & _
            " AND IDPRODOTTO IN " & elenco
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        esisteBlocco = (rec("NUMBLOC") > 0)
        rec.Close
    Else
        esisteBlocco = False
    End If
    
    Call ChiudiConnessioneBD
    
    NessunProdottoCorrelandoBloccatoDaAltroUtente = Not esisteBlocco
End Function

Private Function ProdottoSelezionatoHaTitoli() As Boolean
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim statement As Boolean
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(T.IDTITOLO) NUMTIT FROM TITOLO T, STATOTITOLO ST" & _
        " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
        " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO & _
        " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO & _
        " AND T.IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    statement = (rec("NUMTIT") > 0)
    rec.Close
    
    Call ChiudiConnessioneBD
    
    ProdottoSelezionatoHaTitoli = statement
End Function

Private Function ElencoProdottiAssociatiARapprNonCorrelatiAProd(idRappr As Long) As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elencoParziale As String
    Dim idProdCorr As Long
    
    Call ApriConnessioneBD
    
    elencoParziale = ""
'DALL'INSIEME DEI PRODOTTI CHE INCLUDONO idRappr DEVO TOGLIERE L'INSIEME DI QUELLI GIA' CORRELATI A idProdottoSelezionato
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT DISTINCT PR.IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
'            " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
'            " AND R.DATAORAINIZIO + (R.DURATAINMINUTI/1440) < SYSDATE" & _
'            " AND PR.IDRAPPRESENTAZIONE = " & idRappr & _
'            " AND (PR.IDTIPOSTATORECORD IS NULL" & _
'            " OR PR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND PR.IDPRODOTTO NOT IN" & _
'            " (SELECT DISTINCT PR.IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
'            " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
'            " AND (PR.IDTIPOSTATOREORD IS NULL" & _
'            " OR PR.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND R.DATAORAINIZIO + (R.DURATAINMINUTI/1440) < SYSDATE" & _
'            " AND PR.IDPRODOTTO = " & idProdottoSelezionato & ")"
'    Else
        sql = "SELECT DISTINCT PR.IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
            " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND R.DATAORAINIZIO + (R.DURATAINMINUTI/1440) < SYSDATE" & _
            " AND PR.IDRAPPRESENTAZIONE = " & idRappr & _
            " AND PR.IDPRODOTTO NOT IN" & _
            " (SELECT DISTINCT PR.IDPRODOTTO FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
            " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND R.DATAORAINIZIO + (R.DURATAINMINUTI/1440) < SYSDATE" & _
            " AND PR.IDPRODOTTO = " & idProdottoSelezionato & ")"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idProdCorr = rec("IDPRODOTTO")
            elencoParziale = IIf(elencoParziale = "", "(" & idProdCorr, elencoParziale & ", " & idProdCorr)
            rec.MoveNext
        Wend
    End If
    elencoParziale = IIf(elencoParziale = "", "", elencoParziale & ")")
    rec.Close
    
    Call ChiudiConnessioneBD
    
    ElencoProdottiAssociatiARapprNonCorrelatiAProd = elencoParziale
End Function

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim listaRapprNonAssociabili As Collection
    Dim elenco As String
    Dim rappresentazione As clsElementoLista
    Dim SqlConditions As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Set listaRapprNonAssociabili = New Collection
    If Not (listaSelezionati Is Nothing) Then
        For Each rappresentazione In listaSelezionati
            Call SETAConnection.BeginTrans
            If ProdottiCorrelandiBloccati(rappresentazione.idElementoLista, idProdottoSelezionato) Then
                sql = "INSERT INTO PRODOTTO_RAPPRESENTAZIONE (IDRAPPRESENTAZIONE," & _
                    " IDPRODOTTO)" & _
                    " VALUES (" & rappresentazione.idElementoLista & ", " & _
                    idProdottoSelezionato & ")"
                SETAConnection.Execute sql, n, adCmdText
                
                
                Call SETAConnection.CommitTrans
            Else
                Call listaRapprNonAssociabili.Add(rappresentazione.descrizioneElementoLista)
                Call SETAConnection.RollbackTrans
            End If
            Call SbloccaProdottiCorrelandi(getNomeMacchina, idProdottoSelezionato)
        Next rappresentazione
    End If
    Call ChiudiConnessioneBD
    
    If listaRapprNonAssociabili.count > 0 Then
        elenco = ArgomentoMessaggio(listaRapprNonAssociabili)
        Call frmMessaggio.Visualizza("NotificaAssociazioneRappresentazioniNonEseguitaPerProdottiBloccati", elenco)
    End If
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Private Function NumeroRappresentazioniAssociateAProdotto() As Integer
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Integer
    
    cont = 0
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT COUNT(IDRAPPRESENTAZIONE) CONT" & _
'            " FROM PRODOTTO_RAPPRESENTAZIONE" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL" & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT COUNT(IDRAPPRESENTAZIONE) CONT" & _
            " FROM PRODOTTO_RAPPRESENTAZIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    NumeroRappresentazioniAssociateAProdotto = cont
End Function

Private Function EsisteProdottoMonorappresentazioneAssociatoARappresentazione() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT COUNT(R.IDPRODOTTO) CONT" & _
'            " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE R" & _
'            " WHERE P.IDPRODOTTO = R.IDPRODOTTO(+)" & _
'            " AND R.IDRAPPRESENTAZIONE = " & idRecordSelezionato & _
'            " AND P.IDCLASSEPRODOTTO = " & CPR_BIGLIETTERIA_ORDINARIA & _
'            " AND (P.IDTIPOSTATORECORD IS NULL" & _
'            " OR P.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND (R.IDTIPOSTATORECORD IS NULL" & _
'            " OR R.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " AND P.IDPRODOTTO <> " & idProdottoSelezionato
'    Else
        sql = "SELECT COUNT(R.IDPRODOTTO) CONT" & _
            " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE R" & _
            " WHERE P.IDPRODOTTO = R.IDPRODOTTO(+)" & _
            " AND R.IDRAPPRESENTAZIONE = " & idRecordSelezionato & _
            " AND P.IDCLASSEPRODOTTO = " & CPR_BIGLIETTERIA_ORDINARIA & _
            " AND P.IDPRODOTTO <> " & idProdottoSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    EsisteProdottoMonorappresentazioneAssociatoARappresentazione = (cont > 0)
End Function

Private Function StessiPostiVendutiSuProdottoCorrenteECorrelandi(elencoCorrelandi As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim statement As Boolean
    
    Call ApriConnessioneBD
    
    statement = False
    If elencoCorrelandi <> "" Then
' QUERY ORIGINALE
'        sql = "SELECT P1.IDPOSTO"
'        sql = sql & " FROM"
'        sql = sql & " ("
'        sql = sql & " SELECT DISTINCT IDPOSTO "
'        sql = sql & " FROM TITOLO T, STATOTITOLO ST "
'        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
'        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
'        sql = sql & " AND T.IDPRODOTTO = " & idProdottoSelezionato
'        sql = sql & " ) P1,"
'        sql = sql & " ("
'        sql = sql & " SELECT DISTINCT IDPOSTO "
'        sql = sql & " FROM TITOLO T, STATOTITOLO ST "
'        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
'        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
'        sql = sql & " AND T.IDPRODOTTO IN " & elencoCorrelandi
'        sql = sql & " ) P2"
'        sql = sql & " WHERE P1.IDPOSTO = P2.IDPOSTO"
        
        sql = "SELECT P1.IDPOSTO"
        sql = sql & " FROM"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT IDPOSTO "
        sql = sql & " FROM TITOLO T, STATOTITOLO ST,PRODOTTO P "
        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO
        sql = sql & " AND T.IDPRODOTTO=P.IDPRODOTTO"
        sql = sql & " AND P.IDPRODOTTO = " & idProdottoSelezionato
        sql = sql & " ) P1,"
        sql = sql & " ("
        sql = sql & " SELECT DISTINCT IDPOSTO "
        sql = sql & " FROM TITOLO T, STATOTITOLO ST,PRODOTTO P "
        sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO"
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_VENDUTO_ANNULLATO
        sql = sql & " AND ST.IDTIPOSTATOTITOLO <> " & TST_RISERVATO_ANNULLATO
        sql = sql & " AND T.IDPRODOTTO=P.IDPRODOTTO"
        sql = sql & " AND P.IDPRODOTTO IN " & elencoCorrelandi
        sql = sql & " ) P2"
        sql = sql & " WHERE P1.IDPOSTO = P2.IDPOSTO"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            statement = True
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD
    
    StessiPostiVendutiSuProdottoCorrenteECorrelandi = statement
End Function

Private Function EsistonoTitoli(listaIdProdotto As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numeroTit As Long
    
    EsistonoTitoli = False
    Call ApriConnessioneBD
    
'NOTA: NON C'E' BISOGNO DI RENDERE TRANSAZIONALE QUESTA QUERY, PERCHé I PRODOTTI
'CON TITOLI EMESSI NON SONO ELIMINABILI
    If listaIdProdotto <> "" Then
'        sql = "SELECT COUNT(*) NUMTIT FROM TITOLO WHERE IDPRODOTTO IN " & listaIdProdotto
    sql = " SELECT T.IDTITOLO FROM TITOLO T, STATOTITOLO ST, PRODOTTO P"
    sql = sql & " WHERE T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO "
    sql = sql & " AND ST.IDTIPOSTATOTITOLO NOT IN (" & TST_VENDUTO_ANNULLATO & ", " & TST_RISERVATO_ANNULLATO & ")"
    sql = sql & " AND T.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDPRODOTTO IN " & listaIdProdotto
    sql = sql & " AND ROWNUM = 1"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.EOF And rec.BOF) Then
            EsistonoTitoli = True
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD
End Function



