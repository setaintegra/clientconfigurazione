VERSION 5.00
Begin VB.Form frmConfigurazioneOperatoreOperatori 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Operatore"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbOrganizzazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   780
      Width           =   4095
   End
   Begin VB.Frame fraOperatoriControllori 
      Caption         =   "Operatori controllori"
      Height          =   3195
      Left            =   120
      TabIndex        =   23
      Top             =   4440
      Width           =   8295
      Begin VB.CommandButton cmdSvuotaOperatoriControlloriDisponibili 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   8
         Top             =   600
         Width           =   435
      End
      Begin VB.ListBox lstOperatoriControlloriDisponibili 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   300
         MultiSelect     =   2  'Extended
         TabIndex        =   7
         Top             =   600
         Width           =   3555
      End
      Begin VB.CommandButton cmdSvuotaOperatoriControlloriSelezionati 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   11
         Top             =   2400
         Width           =   435
      End
      Begin VB.CommandButton cmdOperatoreControlloreDisponibile 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   10
         Top             =   1800
         Width           =   435
      End
      Begin VB.CommandButton cmdOperatoreControlloreSelezionato 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   9
         Top             =   1200
         Width           =   435
      End
      Begin VB.ListBox lstOperatoriControlloriSelezionati 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   4380
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         Top             =   600
         Width           =   3555
      End
      Begin VB.Label lblOperatoriControlloriSelezionati 
         Alignment       =   2  'Center
         Caption         =   "Controllori"
         Height          =   195
         Left            =   4380
         TabIndex        =   25
         Top             =   360
         Width           =   3555
      End
      Begin VB.Label lblOperatoriControlloriDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Height          =   195
         Left            =   300
         TabIndex        =   24
         Top             =   360
         Width           =   3555
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   15
      Top             =   8160
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   22
      Top             =   7680
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   19
      Top             =   240
      Width           =   3255
   End
   Begin VB.Frame fraOperatoriControllati 
      Caption         =   "Operatori controllati"
      Height          =   3195
      Left            =   120
      TabIndex        =   16
      Top             =   1200
      Width           =   8295
      Begin VB.ListBox lstOperatoriControllatiSelezionati 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   4380
         MultiSelect     =   2  'Extended
         TabIndex        =   6
         Top             =   600
         Width           =   3555
      End
      Begin VB.CommandButton cmdOperatoreControllatoSelezionato 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   3
         Top             =   1200
         Width           =   435
      End
      Begin VB.CommandButton cmdOperatoreControllatoDisponibile 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   4
         Top             =   1800
         Width           =   435
      End
      Begin VB.CommandButton cmdSvuotaOperatoriControllatiSelezionati 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   5
         Top             =   2400
         Width           =   435
      End
      Begin VB.ListBox lstOperatoriControllatiDisponibili 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   300
         MultiSelect     =   2  'Extended
         TabIndex        =   1
         Top             =   600
         Width           =   3555
      End
      Begin VB.CommandButton cmdSvuotaOperatoriControllatiDisponibili 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3900
         TabIndex        =   2
         Top             =   600
         Width           =   435
      End
      Begin VB.Label lblOperatoriControllatiDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Height          =   195
         Left            =   300
         TabIndex        =   18
         Top             =   360
         Width           =   3555
      End
      Begin VB.Label lblOperatoriControllatiSelezionati 
         Alignment       =   2  'Center
         Caption         =   "Controllati"
         Height          =   195
         Left            =   4380
         TabIndex        =   17
         Top             =   360
         Width           =   3555
      End
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   540
      Width           =   1695
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   21
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Relazioni con altri operatori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneOperatoreOperatori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOperatoreSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private userNameOperatoreSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private internalEvent As Boolean
Private listaOperatoriControllatiDisponibili As Collection
Private listaOperatoriControllatiSelezionati As Collection
Private listaOperatoriControlloriDisponibili As Collection
Private listaOperatoriControlloriSelezionati As Collection
Private isRecordEditabile As Boolean

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum

Public Sub SetIsOperatoreEditabile(isEditabile As Boolean)
    isRecordEditabile = isEditabile
End Sub

Public Sub SetIdOperatoreSelezionato(id As Long)
    idOperatoreSelezionato = id
End Sub

Public Sub SetUserNameOperatoreSelezionato(nome As String)
    userNameOperatoreSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Operatore"
    txtInfo1.Text = userNameOperatoreSelezionato
    txtInfo1.Enabled = False
    
    fraOperatoriControllati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lstOperatoriControllatiDisponibili.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lstOperatoriControllatiSelezionati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lblOperatoriControllatiDisponibili.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lblOperatoriControllatiSelezionati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdOperatoreControllatoDisponibile.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdOperatoreControllatoSelezionato.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdSvuotaOperatoriControllatiDisponibili.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdSvuotaOperatoriControllatiSelezionati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    fraOperatoriControllori.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lstOperatoriControlloriDisponibili.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lstOperatoriControlloriSelezionati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lblOperatoriControlloriDisponibili.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    lblOperatoriControlloriSelezionati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdOperatoreControlloreDisponibile.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdOperatoreControlloreSelezionato.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdSvuotaOperatoriControlloriDisponibili.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    cmdSvuotaOperatoriControlloriSelezionati.Enabled = (idOrganizzazioneSelezionata <> idNessunElementoSelezionato)
    
End Sub

Public Sub Init()
    Dim sql As String
    
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    sql = "SELECT ORG.IDORGANIZZAZIONE ID, ORG.NOME" & _
        " FROM ORGANIZZAZIONE ORG, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, OPERATORE O" & _
        " WHERE ORG.IDORGANIZZAZIONE = OCP.IDORGANIZZAZIONE" & _
        " AND OCP.IDPUNTOVENDITA = O.IDPUNTOVENDITAELETTIVO" & _
        " AND O.IDOPERATORE = " & idOperatoreSelezionato & _
        " ORDER BY NOME"
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmbOrganizzazione_Click()
    If Not internalEvent Then
        Call cmbOrganizzazione_Update
    End If
End Sub

Private Sub cmbOrganizzazione_Update()
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    Call CaricaValoriLstOperatoriControllatiDisponibili
    Call CaricaValoriLstOperatoriControllatiSelezionati
    Call CaricaValoriLstOperatoriControlloriDisponibili
    Call CaricaValoriLstOperatoriControlloriSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
'    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDOPERATORE = " & idOperatoreSelezionato
    If isRecordEditabile Then
        Call SetGestioneExitCode(EC_CONFERMA)
        Call AggiornaAbilitazioneControlli
        Call InserisciNellaBaseDati
        Call ScriviLog(CCTA_INSERIMENTO, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
        Call SetGestioneExitCode(EC_NON_SPECIFICATO)
        Call AggiornaAbilitazioneControlli
    End If
    
End Sub

Private Sub CaricaValoriLstOperatoriControllatiDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
            
    Call ApriConnessioneBD
    
    Set listaOperatoriControllatiDisponibili = New Collection
    
    If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM (" & _
            " SELECT DISTINCT OP.IDORGANIZZAZIONE" & _
            " FROM OPERATORE O, ORGANIZ_CLASSEPV_PUNTOVENDITA OP" & _
            " WHERE O.IDOPERATORE = " & idOperatoreSelezionato & _
            " AND O.IDPUNTOVENDITAELETTIVO = OP.IDPUNTOVENDITA" & _
            ") ORG, OPERATORE O, OPERATORE_OPERATORE OO, ORGANIZ_CLASSEPV_PUNTOVENDITA OP" & _
            " WHERE O.IDPUNTOVENDITAELETTIVO = OP.IDPUNTOVENDITA" & _
            " AND OP.IDORGANIZZAZIONE = ORG.IDORGANIZZAZIONE" & _
            " AND O.IDOPERATORE = OO.IDOPERATORECONTROLLATO(+)" & _
            " AND OO.IDOPERATORECONTROLLATO IS NULL" & _
            " AND OO.IDOPERATORECONTROLLORE(+) = " & idOperatoreSelezionato & _
            " AND O.IDOPERATORE <> " & idOperatoreSelezionato & _
            " ORDER BY USERNAME"
    Else
        sql = "SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM OPERATORE O, OPERATORE_OPERATORE OO, ORGANIZ_CLASSEPV_PUNTOVENDITA OP" & _
        " WHERE O.IDPUNTOVENDITAELETTIVO = OP.IDPUNTOVENDITA" & _
        " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND O.IDOPERATORE = OO.IDOPERATORECONTROLLATO(+)" & _
        " AND OO.IDOPERATORECONTROLLATO IS NULL" & _
        " AND OO.IDOPERATORECONTROLLORE(+) = " & idOperatoreSelezionato & _
        " AND O.IDOPERATORE <> " & idOperatoreSelezionato & _
        " ORDER BY USERNAME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.idElementoLista = rec("ID").Value
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriControllatiDisponibili.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOperatoriControllatiDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstOperatoriControllatiSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista

    Call ApriConnessioneBD

    Set listaOperatoriControllatiSelezionati = New Collection
    
    If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM OPERATORE O, OPERATORE_OPERATORE OO" & _
            " WHERE O.IDOPERATORE = OO.IDOPERATORECONTROLLATO" & _
            " AND OO.IDOPERATORECONTROLLORE = " & idOperatoreSelezionato & _
            " ORDER BY USERNAME"
    Else
        sql = " SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM OPERATORE O, OPERATORE_OPERATORE OO, ORGANIZ_CLASSEPV_PUNTOVENDITA OP" & _
            " WHERE O.IDOPERATORE = OO.IDOPERATORECONTROLLATO" & _
            " AND O.IDPUNTOVENDITAELETTIVO = OP.IDPUNTOVENDITA" & _
            " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OO.IDOPERATORECONTROLLORE = " & idOperatoreSelezionato & _
            " ORDER BY USERNAME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.idElementoLista = rec("ID").Value
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriControllatiSelezionati.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOperatoriControllatiSelezionati_Init
    
End Sub

Private Sub lstOperatoriControllatiDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriControllatiDisponibili.Clear

    If Not (listaOperatoriControllatiDisponibili Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriControllatiDisponibili
            lstOperatoriControllatiDisponibili.AddItem operatore.descrizioneElementoLista
            lstOperatoriControllatiDisponibili.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOperatoriControllatiSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriControllatiSelezionati.Clear

    If Not (listaOperatoriControllatiSelezionati Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriControllatiSelezionati
            lstOperatoriControllatiSelezionati.AddItem operatore.descrizioneElementoLista
            lstOperatoriControllatiSelezionati.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaInLstOperatoriControllatiDisponibili()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriControllatiSelezionati.ListCount
        If lstOperatoriControllatiSelezionati.Selected(i - 1) Then
            idOperatore = lstOperatoriControllatiSelezionati.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriControllatiSelezionati.Item(chiaveOperatore)
            Call listaOperatoriControllatiDisponibili.Add(operatore, chiaveOperatore)
            Call listaOperatoriControllatiSelezionati.Remove(chiaveOperatore)
        End If
    Next i
    Call lstOperatoriControllatiDisponibili_Init
    Call lstOperatoriControllatiSelezionati_Init
End Sub

Private Sub SpostaInLstOperatoriControllatiSelezionati()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriControllatiDisponibili.ListCount
        If lstOperatoriControllatiDisponibili.Selected(i - 1) Then
            idOperatore = lstOperatoriControllatiDisponibili.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriControllatiDisponibili.Item(chiaveOperatore)
            Call listaOperatoriControllatiSelezionati.Add(operatore, chiaveOperatore)
            Call listaOperatoriControllatiDisponibili.Remove(chiaveOperatore)
        End If
    Next i
    Call lstOperatoriControllatiDisponibili_Init
    Call lstOperatoriControllatiSelezionati_Init
End Sub

Private Sub SvuotaOperatoriControllatiSelezionati()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriControllatiSelezionati
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriControllatiDisponibili.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriControllatiSelezionati = Nothing
    Set listaOperatoriControllatiSelezionati = New Collection
    
    Call lstOperatoriControllatiDisponibili_Init
    Call lstOperatoriControllatiSelezionati_Init
End Sub

Private Sub SvuotaOperatoriControllatiDisponibili()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriControllatiDisponibili
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriControllatiSelezionati.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriControllatiDisponibili = Nothing
    Set listaOperatoriControllatiDisponibili = New Collection
    
    Call lstOperatoriControllatiDisponibili_Init
    Call lstOperatoriControllatiSelezionati_Init
End Sub

Private Sub cmdOperatoreControllatoDisponibile_Click()
    Call SpostaInLstOperatoriControllatiDisponibili
End Sub

Private Sub cmdOperatoreControllatoSelezionato_Click()
    Call SpostaInLstOperatoriControllatiSelezionati
End Sub

Private Sub cmdSvuotaOperatoriControllatiDisponibili_Click()
    Call SvuotaOperatoriControllatiDisponibili
End Sub

Private Sub cmdSvuotaOperatoriControllatiSelezionati_Click()
    Call SvuotaOperatoriControllatiSelezionati
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub CaricaValoriLstOperatoriControlloriDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
            
    Call ApriConnessioneBD
    
    Set listaOperatoriControlloriDisponibili = New Collection
    
    If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
        sql = " SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM (" & _
            " SELECT DISTINCT OCP.IDORGANIZZAZIONE" & _
            " FROM OPERATORE O, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP" & _
            " WHERE O.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA" & _
            " AND O.IDOPERATORE = " & idOperatoreSelezionato & _
            ") ORG, OPERATORE O, OPERATORE_OPERATORE OO, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP" & _
            " WHERE O.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA" & _
            " AND OCP.IDORGANIZZAZIONE = ORG.IDORGANIZZAZIONE" & _
            " AND O.IDOPERATORE = OO.IDOPERATORECONTROLLORE(+)" & _
            " AND OO.IDOPERATORECONTROLLORE IS NULL" & _
            " AND OO.IDOPERATORECONTROLLATO(+) = " & idOperatoreSelezionato & _
            " AND O.IDOPERATORE <> " & idOperatoreSelezionato & _
            " ORDER BY USERNAME"
    Else
        sql = "SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM OPERATORE O, OPERATORE_OPERATORE OO, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP" & _
            " WHERE O.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA" & _
            " AND OCP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND O.IDOPERATORE = OO.IDOPERATORECONTROLLORE(+)" & _
            " AND OO.IDOPERATORECONTROLLORE IS NULL" & _
            " AND OO.IDOPERATORECONTROLLATO(+) = " & idOperatoreSelezionato & _
            " AND O.IDOPERATORE <> " & idOperatoreSelezionato & _
            " ORDER BY USERNAME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.idElementoLista = rec("ID").Value
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriControlloriDisponibili.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOperatoriControlloriDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstOperatoriControlloriSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista

    Call ApriConnessioneBD

    Set listaOperatoriControlloriSelezionati = New Collection
    
    If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM OPERATORE O, OPERATORE_OPERATORE OO" & _
            " WHERE O.IDOPERATORE = OO.IDOPERATORECONTROLLORE" & _
            " AND OO.IDOPERATORECONTROLLATO = " & idOperatoreSelezionato & _
            " ORDER BY USERNAME"
    Else
        sql = "SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME" & _
            " FROM OPERATORE O, OPERATORE_OPERATORE OO, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP" & _
            " WHERE O.IDOPERATORE = OO.IDOPERATORECONTROLLORE" & _
            " AND O.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA" & _
            " AND OCP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OO.IDOPERATORECONTROLLATO = " & idOperatoreSelezionato & _
            " ORDER BY USERNAME "
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.idElementoLista = rec("ID").Value
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriControlloriSelezionati.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOperatoriControlloriSelezionati_Init
    
End Sub

Private Sub lstOperatoriControlloriDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriControlloriDisponibili.Clear

    If Not (listaOperatoriControlloriDisponibili Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriControlloriDisponibili
            lstOperatoriControlloriDisponibili.AddItem operatore.descrizioneElementoLista
            lstOperatoriControlloriDisponibili.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOperatoriControlloriSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriControlloriSelezionati.Clear

    If Not (listaOperatoriControlloriSelezionati Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriControlloriSelezionati
            lstOperatoriControlloriSelezionati.AddItem operatore.descrizioneElementoLista
            lstOperatoriControlloriSelezionati.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaInLstOperatoriControlloriDisponibili()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriControlloriSelezionati.ListCount
        If lstOperatoriControlloriSelezionati.Selected(i - 1) Then
            idOperatore = lstOperatoriControlloriSelezionati.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriControlloriSelezionati.Item(chiaveOperatore)
            Call listaOperatoriControlloriDisponibili.Add(operatore, chiaveOperatore)
            Call listaOperatoriControlloriSelezionati.Remove(chiaveOperatore)
        End If
    Next i
    Call lstOperatoriControlloriDisponibili_Init
    Call lstOperatoriControlloriSelezionati_Init
End Sub

Private Sub SpostaInLstOperatoriControlloriSelezionati()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriControlloriDisponibili.ListCount
        If lstOperatoriControlloriDisponibili.Selected(i - 1) Then
            idOperatore = lstOperatoriControlloriDisponibili.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriControlloriDisponibili.Item(chiaveOperatore)
            Call listaOperatoriControlloriSelezionati.Add(operatore, chiaveOperatore)
            Call listaOperatoriControlloriDisponibili.Remove(chiaveOperatore)
        End If
    Next i
    Call lstOperatoriControlloriDisponibili_Init
    Call lstOperatoriControlloriSelezionati_Init
End Sub

Private Sub SvuotaOperatoriControlloriSelezionati()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriControlloriSelezionati
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriControlloriDisponibili.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriControlloriSelezionati = Nothing
    Set listaOperatoriControlloriSelezionati = New Collection
    
    Call lstOperatoriControlloriDisponibili_Init
    Call lstOperatoriControlloriSelezionati_Init
End Sub

Private Sub SvuotaOperatoriControlloriDisponibili()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriControlloriDisponibili
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriControlloriSelezionati.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriControlloriDisponibili = Nothing
    Set listaOperatoriControlloriDisponibili = New Collection
    
    Call lstOperatoriControlloriDisponibili_Init
    Call lstOperatoriControlloriSelezionati_Init
End Sub

Private Sub cmdOperatoreControlloreDisponibile_Click()
    Call SpostaInLstOperatoriControlloriDisponibili
End Sub

Private Sub cmdOperatoreControlloreSelezionato_Click()
    Call SpostaInLstOperatoriControlloriSelezionati
End Sub

Private Sub cmdSvuotaOperatoriControlloriDisponibili_Click()
    Call SvuotaOperatoriControlloriDisponibili
End Sub

Private Sub cmdSvuotaOperatoriControlloriSelezionati_Click()
    Call SvuotaOperatoriControlloriSelezionati
End Sub

Private Sub InserisciNellaBaseDati()
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    Call InserisciNellaBaseDatiOperatoriControllati
    Call InserisciNellaBaseDatiOperatoriControllori
    SETAConnection.CommitTrans
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub InserisciNellaBaseDatiOperatoriControllati()
    Dim sql As String
    Dim sql0 As String
    Dim sql1 As String
    Dim numeroRecordInseritiSimultaneamente As Integer
    Dim numeroVociLista As Integer
    Dim posizione As Integer
    Dim operatore As clsElementoLista
    Dim n As Long
    Dim condizioneSql As String

    sql = "DELETE FROM OPERATORE_OPERATORE"
    sql = sql & " WHERE IDOPERATORECONTROLLORE = " & idOperatoreSelezionato
    If idOrganizzazioneSelezionata <> idNessunElementoSelezionato And _
        idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND IDOPERATORECONTROLLATO IN ("
        sql = sql & " SELECT IDOPERATORE"
        sql = sql & " FROM OPERATORE O, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP"
        sql = sql & " WHERE O.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA"
        sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")"
    End If
    SETAConnection.Execute sql, n, adCmdText
    If listaOperatoriControllatiSelezionati.count > 0 Then
        numeroVociLista = listaOperatoriControllatiSelezionati.count
        numeroRecordInseritiSimultaneamente = 100
        sql = "INSERT INTO OPERATORE_OPERATORE (IDOPERATORECONTROLLORE, IDOPERATORECONTROLLATO)"
        sql = sql & " SELECT " & idOperatoreSelezionato & ","
        sql = sql & " IDOPERATORE FROM OPERATORE WHERE IDOPERATORE IN ("
        For posizione = 1 To numeroVociLista
            Set operatore = listaOperatoriControllatiSelezionati.Item(posizione)
            sql = sql & operatore.idElementoLista
            If posizione = numeroVociLista Or posizione Mod numeroRecordInseritiSimultaneamente = 0 Then
                sql = sql & ")"
                SETAConnection.Execute sql, n, adCmdText
                sql = "INSERT INTO OPERATORE_OPERATORE (IDOPERATORECONTROLLORE, IDOPERATORECONTROLLATO)"
                sql = sql & " SELECT " & idOperatoreSelezionato & ","
                sql = sql & " IDOPERATORE FROM OPERATORE WHERE IDOPERATORE IN ("
            Else
                sql = sql & ","
            End If
        Next posizione
    End If
End Sub

Private Sub InserisciNellaBaseDatiOperatoriControllori()
    Dim sql As String
    Dim sql0 As String
    Dim sql1 As String
    Dim numeroRecordInseritiSimultaneamente As Integer
    Dim numeroVociLista As Integer
    Dim posizione As Integer
    Dim operatore As clsElementoLista
    Dim n As Long
    Dim condizioneSql As String

    sql = "DELETE FROM OPERATORE_OPERATORE"
    sql = sql & " WHERE IDOPERATORECONTROLLATO = " & idOperatoreSelezionato
    If idOrganizzazioneSelezionata <> idNessunElementoSelezionato And _
        idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND IDOPERATORECONTROLLORE IN ("
        sql = sql & " SELECT IDOPERATORE"
        sql = sql & " FROM OPERATORE O, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP"
        sql = sql & " WHERE O.IDPUNTOVENDITAELETTIVO = OCP.IDPUNTOVENDITA"
        sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")"
    End If
    SETAConnection.Execute sql, n, adCmdText
    If listaOperatoriControlloriSelezionati.count > 0 Then
        numeroVociLista = listaOperatoriControlloriSelezionati.count
        numeroRecordInseritiSimultaneamente = 100
        sql = "INSERT INTO OPERATORE_OPERATORE (IDOPERATORECONTROLLORE, IDOPERATORECONTROLLATO)"
        sql = sql & " SELECT IDOPERATORE,"
        sql = sql & idOperatoreSelezionato & " FROM OPERATORE WHERE IDOPERATORE IN ("
        For posizione = 1 To numeroVociLista
            Set operatore = listaOperatoriControlloriSelezionati.Item(posizione)
            sql = sql & operatore.idElementoLista
            If posizione = numeroVociLista Or posizione Mod numeroRecordInseritiSimultaneamente = 0 Then
                sql = sql & ")"
                SETAConnection.Execute sql, n, adCmdText
                sql = "INSERT INTO OPERATORE_OPERATORE (IDOPERATORECONTROLLORE, IDOPERATORECONTROLLATO)"
                sql = sql & " SELECT IDOPERATORE,"
                sql = sql & idOperatoreSelezionato & " FROM OPERATORE WHERE IDOPERATORE IN ("
            Else
                sql = sql & ","
            End If
        Next posizione
    End If
End Sub

