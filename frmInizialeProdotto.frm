VERSION 5.00
Begin VB.Form frmInizialeProdotto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   11805
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13050
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11805
   ScaleWidth      =   13050
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "1"
   Begin VB.Frame frmFiscalita 
      Caption         =   "Fiscalit�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   95
      Top             =   9120
      Width           =   9855
      Begin VB.TextBox txtMinutiAnnullamentoDaFineEvento 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8520
         MaxLength       =   10
         TabIndex        =   99
         Top             =   720
         Width           =   1230
      End
      Begin VB.TextBox txtMinutiAnnullamentoTitTradiz 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3720
         MaxLength       =   10
         TabIndex        =   97
         Top             =   720
         Width           =   1215
      End
      Begin VB.CheckBox chkFiscale 
         Alignment       =   1  'Right Justify
         Caption         =   "Prodotto fiscale"
         Height          =   255
         Left            =   120
         TabIndex        =   96
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblMinutiAnnullamentoDaFineEvento2 
         Alignment       =   1  'Right Justify
         Caption         =   "Impostare stringa vuota (null) per nessun limite"
         Height          =   255
         Left            =   4920
         TabIndex        =   103
         Top             =   960
         Width           =   3495
      End
      Begin VB.Label lblMinutiAnnullamentoTitTradiz2 
         Alignment       =   1  'Right Justify
         Caption         =   "Impostare stringa vuota (null) per nessun limite"
         Height          =   255
         Left            =   120
         TabIndex        =   102
         Top             =   960
         Width           =   3495
      End
      Begin VB.Label lblMinutiAnnullamentoDaFineEvento 
         Alignment       =   1  'Right Justify
         Caption         =   "Max minuti per annullamento da fine evento"
         Height          =   255
         Left            =   5160
         TabIndex        =   100
         Top             =   720
         Width           =   3255
      End
      Begin VB.Label lblMinutiAnnullamentoTitTradiz 
         Alignment       =   1  'Right Justify
         Caption         =   "Max minuti per annullamento titolo tradizionale"
         Height          =   255
         Left            =   240
         TabIndex        =   98
         Top             =   720
         Width           =   3375
      End
   End
   Begin VB.Frame frmReteVenditaProdottoTDL 
      Caption         =   "Rete di vendita per prodotti TDL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3000
      TabIndex        =   89
      Top             =   10920
      Visible         =   0   'False
      Width           =   4455
      Begin VB.CheckBox chkTDLReteALTURA 
         Alignment       =   1  'Right Justify
         Caption         =   "Prodotto da abilitare su rete ALTURA"
         Height          =   255
         Left            =   6120
         TabIndex        =   91
         Top             =   360
         Width           =   3375
      End
      Begin VB.CheckBox chkTDLReteLOTTO 
         Alignment       =   1  'Right Justify
         Caption         =   "Prodotto da abilitare su rete LOTTO"
         Height          =   255
         Left            =   120
         TabIndex        =   90
         Top             =   360
         Width           =   3255
      End
   End
   Begin VB.CheckBox chkProdottoTDL 
      Alignment       =   1  'Right Justify
      Caption         =   "Prodotto TDL"
      Enabled         =   0   'False
      Height          =   255
      Left            =   8160
      TabIndex        =   88
      Top             =   3480
      Width           =   1455
   End
   Begin VB.Frame frmRuoloIlTuoAbbonamento 
      Caption         =   "Ruolo nel progetto ""Il Tuo Abbonamento"""
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   83
      Top             =   7800
      Width           =   9855
      Begin VB.CheckBox chkSpesaCreditoObbligatoria 
         Alignment       =   1  'Right Justify
         Caption         =   "Spesa credito obbligatoria"
         Height          =   255
         Left            =   7560
         TabIndex        =   92
         Top             =   720
         Width           =   2175
      End
      Begin VB.ComboBox cmbCausaleUtilizzoCreditiITA 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   86
         Top             =   720
         Width           =   5595
      End
      Begin VB.ComboBox cmbRuoloIlTuoAbbonamento 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   84
         Top             =   360
         Width           =   5595
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Causale utilizzo crediti"
         Height          =   255
         Left            =   120
         TabIndex        =   87
         Top             =   780
         Width           =   1575
      End
      Begin VB.Label lblRuoloIlTuoAbbonamento 
         Alignment       =   1  'Right Justify
         Caption         =   "Ruolo"
         Height          =   255
         Left            =   120
         TabIndex        =   85
         Top             =   420
         Width           =   1575
      End
   End
   Begin VB.Frame frmGestioneVenditeSuLisClick 
      Caption         =   "Gestione vendite su Listicket.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   79
      Top             =   6840
      Width           =   9855
      Begin VB.ComboBox cmbTipoVenditaSuLisClick 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5520
         Style           =   2  'Dropdown List
         TabIndex        =   81
         Top             =   300
         Width           =   4275
      End
      Begin VB.CheckBox chkSospesoSuCPVInternet 
         Alignment       =   1  'Right Justify
         Caption         =   "Prodotto sospeso su CPV Internet"
         Height          =   255
         Left            =   120
         TabIndex        =   80
         Top             =   360
         Width           =   2775
      End
      Begin VB.Label lblTipoVenditaLisClick 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo vendita su Listicket.com"
         Height          =   255
         Left            =   3240
         TabIndex        =   82
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.CheckBox chkGestioneTariffeOmaggio 
      Alignment       =   1  'Right Justify
      Caption         =   "Gest tariffe omaggio"
      Height          =   255
      Left            =   7920
      TabIndex        =   65
      Top             =   4320
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   48
      Top             =   10800
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   22
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   23
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7740
      TabIndex        =   47
      Top             =   10800
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   26
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   24
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   25
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6255
      Left            =   120
      TabIndex        =   42
      Top             =   480
      Width           =   9855
      Begin VB.TextBox txtNumeroMassimoCessioniRateo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6240
         MaxLength       =   2
         TabIndex        =   104
         Top             =   5640
         Width           =   495
      End
      Begin VB.ComboBox cmbLivelloGestioneAnagrafiche 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   71
         Top             =   5160
         Width           =   6135
      End
      Begin VB.ComboBox cmbSito 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   67
         Top             =   3840
         Width           =   5715
      End
      Begin VB.ComboBox cmbFormatoSupporto 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6000
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   2580
         Width           =   3555
      End
      Begin VB.TextBox txtNumeroMaxTitoliPerAcquirente 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9120
         MaxLength       =   2
         TabIndex        =   5
         Top             =   5160
         Width           =   390
      End
      Begin VB.CheckBox chkRientraInDecretoSicurezza 
         Alignment       =   1  'Right Justify
         Caption         =   "Decreto sicurezza"
         Enabled         =   0   'False
         Height          =   255
         Left            =   7920
         TabIndex        =   4
         Top             =   5640
         Width           =   1635
      End
      Begin VB.TextBox txtDescrizionePOS 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         MaxLength       =   21
         TabIndex        =   3
         Top             =   1320
         Width           =   2475
      End
      Begin VB.TextBox txtQuantit�AccessiConsentiti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8700
         MaxLength       =   6
         TabIndex        =   10
         Top             =   2160
         Width           =   855
      End
      Begin VB.CheckBox chkProdottoOpen 
         Alignment       =   1  'Right Justify
         Caption         =   "Prod. open"
         Height          =   255
         Left            =   6120
         TabIndex        =   9
         Top             =   2220
         Width           =   1095
      End
      Begin VB.ComboBox cmbClasseProdotto 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   2160
         Width           =   4095
      End
      Begin VB.ComboBox cmbManifestazione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6000
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   1320
         Width           =   3555
      End
      Begin VB.ComboBox cmbContratto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6000
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   4260
         Width           =   3555
      End
      Begin VB.TextBox txtAliquotaIVA 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4440
         MaxLength       =   5
         TabIndex        =   18
         Top             =   4680
         Width           =   675
      End
      Begin VB.ComboBox cmbPianteSIAE 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   4260
         Width           =   3555
      End
      Begin VB.TextBox txtRateo 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6000
         MaxLength       =   8
         TabIndex        =   19
         Top             =   4680
         Width           =   975
      End
      Begin VB.TextBox txtDescrizioneStampaVenue 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6300
         MaxLength       =   30
         TabIndex        =   1
         Top             =   300
         Width           =   3255
      End
      Begin VB.ComboBox cmbStagione 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6000
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1740
         Width           =   3555
      End
      Begin VB.TextBox txtCodiceTLOrganizzazione 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1860
         MaxLength       =   4
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   4680
         Width           =   555
      End
      Begin VB.ComboBox cmbTipoProdotto 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   2580
         Width           =   2535
      End
      Begin VB.ComboBox cmbPiante 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   3420
         Width           =   7995
      End
      Begin VB.ComboBox cmbOrganizzazioni 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   3000
         Width           =   4095
      End
      Begin VB.TextBox txtDescrizioneAlternativa 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   6
         Top             =   1740
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1560
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   720
         Width           =   7995
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   0
         Top             =   300
         Width           =   3255
      End
      Begin VB.TextBox txtCodiceTLTipoProdotto 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         MaxLength       =   2
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   4680
         Width           =   315
      End
      Begin VB.CheckBox chkPropagazioneProtezioni 
         Alignment       =   1  'Right Justify
         Caption         =   "Prop. protez. ai pr. correlati"
         Height          =   255
         Left            =   7260
         TabIndex        =   20
         Top             =   4740
         Width           =   2295
      End
      Begin VB.TextBox txtCodiceTL 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         MaxLength       =   6
         TabIndex        =   17
         Top             =   4680
         Width           =   795
      End
      Begin VB.Label lblNumeroMassimoCessioniRateo 
         Alignment       =   1  'Right Justify
         Caption         =   "Numero massimo di cessioni reato permesse"
         Height          =   255
         Left            =   2760
         TabIndex        =   106
         Top             =   5640
         Width           =   3375
      End
      Begin VB.Label lblNumeroMassimoCessioniRateo2 
         Alignment       =   1  'Right Justify
         Caption         =   "Impostare stringa vuota (null) per nessun limite"
         Height          =   255
         Left            =   2640
         TabIndex        =   105
         Top             =   5880
         Width           =   3495
      End
      Begin VB.Label lblLivelloGestioneAnagrafiche 
         Alignment       =   1  'Right Justify
         Caption         =   "Livello gestione anagrafiche"
         Height          =   495
         Left            =   120
         TabIndex        =   72
         Top             =   5115
         Width           =   1335
      End
      Begin VB.Label lblSito 
         Alignment       =   1  'Right Justify
         Caption         =   "Sito"
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   3900
         Width           =   1275
      End
      Begin VB.Label lblFormatoSupporto 
         Caption         =   "Formato supporto POS"
         Height          =   315
         Left            =   4320
         TabIndex        =   64
         Top             =   2640
         Width           =   1635
      End
      Begin VB.Label lblNumeroMaxTitoliPerAcquirente 
         Alignment       =   1  'Right Justify
         Caption         =   "Num. max titoli per acquirente"
         Height          =   375
         Left            =   7920
         TabIndex        =   63
         Top             =   5115
         Width           =   1095
      End
      Begin VB.Label lblDescrizionePOS 
         Alignment       =   1  'Right Justify
         Caption         =   " Descr. POS"
         Height          =   255
         Left            =   300
         TabIndex        =   62
         Top             =   1380
         Width           =   1095
      End
      Begin VB.Label lblQuantit�AccessiConsentiti 
         Alignment       =   1  'Right Justify
         Caption         =   "Q.t� accessi"
         Height          =   255
         Left            =   7560
         TabIndex        =   61
         Top             =   2220
         Width           =   1035
      End
      Begin VB.Label lblClasseProdotto 
         Alignment       =   1  'Right Justify
         Caption         =   "Classe prodotto"
         Height          =   255
         Left            =   180
         TabIndex        =   60
         Top             =   2220
         Width           =   1215
      End
      Begin VB.Label lblManifestazione 
         Alignment       =   1  'Right Justify
         Caption         =   "Manifestazione"
         Height          =   255
         Left            =   4740
         TabIndex        =   59
         Top             =   1380
         Width           =   1095
      End
      Begin VB.Label lblContratto 
         Alignment       =   1  'Right Justify
         Caption         =   "Contratto"
         Height          =   255
         Left            =   5160
         TabIndex        =   58
         Top             =   4320
         Width           =   675
      End
      Begin VB.Label lblAliquotaIVA 
         Alignment       =   1  'Right Justify
         Caption         =   "Aliquota IVA"
         Height          =   255
         Left            =   3300
         TabIndex        =   57
         Top             =   4740
         Width           =   975
      End
      Begin VB.Label lblPianteSIAE 
         Alignment       =   1  'Right Justify
         Caption         =   "Pianta SIAE"
         Height          =   255
         Left            =   180
         TabIndex        =   56
         Top             =   4320
         Width           =   1215
      End
      Begin VB.Label lblRateo 
         Alignment       =   1  'Right Justify
         Caption         =   "Rateo"
         Height          =   255
         Left            =   5220
         TabIndex        =   55
         Top             =   4740
         Width           =   615
      End
      Begin VB.Label lblDescrizioneStampaVenue 
         Alignment       =   1  'Right Justify
         Caption         =   " Descr. stampa venue"
         Height          =   375
         Left            =   5040
         TabIndex        =   54
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblStagione 
         Alignment       =   1  'Right Justify
         Caption         =   "Stagione"
         Height          =   255
         Left            =   5160
         TabIndex        =   53
         Top             =   1800
         Width           =   675
      End
      Begin VB.Label lblTipoProdotto 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo prodotto"
         Height          =   255
         Left            =   300
         TabIndex        =   52
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label lblDescrizioneAlternativa 
         Alignment       =   1  'Right Justify
         Caption         =   "Descr. alternativa"
         Height          =   255
         Left            =   120
         TabIndex        =   51
         Top             =   1800
         Width           =   1275
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   255
         Left            =   300
         TabIndex        =   50
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   360
         TabIndex        =   49
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblPianta 
         Alignment       =   1  'Right Justify
         Caption         =   "Venue - pianta"
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   3480
         Width           =   1275
      End
      Begin VB.Label lblOrganizzazione 
         Alignment       =   1  'Right Justify
         Caption         =   "Organizzazione"
         Height          =   255
         Left            =   300
         TabIndex        =   45
         Top             =   3060
         Width           =   1095
      End
      Begin VB.Label lblCodiceTerminaleLotto 
         Alignment       =   1  'Right Justify
         Caption         =   "Codice TL"
         Height          =   255
         Left            =   540
         TabIndex        =   44
         Top             =   4740
         Width           =   855
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   9855
      Left            =   10140
      TabIndex        =   41
      Top             =   480
      Width           =   2715
      Begin VB.CommandButton cmdSuperareeTipiSupporto 
         Caption         =   "Superaree - tipi supporto digitali"
         Height          =   380
         Left            =   120
         TabIndex        =   101
         Top             =   9350
         Width           =   2475
      End
      Begin VB.CommandButton cmdGestioneAnagrafiche 
         Caption         =   "Gestione anagrafiche"
         Height          =   380
         Left            =   120
         TabIndex        =   94
         Top             =   8950
         Width           =   2475
      End
      Begin VB.CommandButton cmdTipoStampante 
         Caption         =   "Tipo stampante"
         Height          =   380
         Left            =   120
         TabIndex        =   93
         Top             =   8550
         Width           =   2475
      End
      Begin VB.CommandButton cmdLayoutEMailAcquistiInternet 
         Caption         =   "Layout eMail acquisti internet"
         Height          =   380
         Left            =   120
         TabIndex        =   78
         Top             =   8150
         Width           =   2475
      End
      Begin VB.CommandButton cmdAssociazioneLayoutRicevute 
         Caption         =   "Associazione layout ricevute"
         Height          =   380
         Left            =   120
         TabIndex        =   77
         Top             =   5350
         Width           =   2475
      End
      Begin VB.CommandButton cmdCausaliProtezione 
         Caption         =   "Causali protezione"
         Height          =   380
         Left            =   120
         TabIndex        =   76
         Top             =   3350
         Width           =   2475
      End
      Begin VB.CommandButton cmdClassiSuperAree 
         Caption         =   "Classi superaree"
         Height          =   380
         Left            =   120
         TabIndex        =   75
         Top             =   150
         Width           =   2475
      End
      Begin VB.CommandButton cmdAbilitazionePuntiVendita 
         Caption         =   "Abilitazione punti vendita"
         Height          =   380
         Left            =   120
         TabIndex        =   74
         Top             =   550
         Width           =   2475
      End
      Begin VB.CommandButton cmdAbilitazioniSupportiDigitali 
         Caption         =   "Abilitazioni supporti digitali"
         Height          =   380
         Left            =   120
         TabIndex        =   73
         Top             =   7350
         Width           =   2475
      End
      Begin VB.CommandButton cmdModalitaFornitura 
         Caption         =   "Modalita fornitura"
         Height          =   380
         Left            =   120
         TabIndex        =   70
         Top             =   7750
         Width           =   2475
      End
      Begin VB.CommandButton cmdCambioUtilizzatoreSuSuperaree 
         Caption         =   "Cambio utilizzatore"
         Height          =   380
         Left            =   120
         TabIndex        =   69
         Top             =   6150
         Width           =   2475
      End
      Begin VB.CommandButton cmdDateOperazioni 
         Caption         =   "Date operazioni e catalogo WEB"
         Height          =   380
         Left            =   120
         TabIndex        =   68
         Top             =   950
         Width           =   2475
      End
      Begin VB.CommandButton cmdCapienze 
         Caption         =   "Capienze superaree"
         Height          =   380
         Left            =   120
         TabIndex        =   31
         Top             =   2950
         Width           =   2475
      End
      Begin VB.CommandButton cmdStampeAggiuntive 
         Caption         =   "Stampe aggiuntive"
         Height          =   380
         Left            =   120
         TabIndex        =   27
         Top             =   1350
         Width           =   2475
      End
      Begin VB.CommandButton cmdSistemaDiEmissione 
         Caption         =   "Modalit� di emissione"
         Height          =   380
         Left            =   120
         TabIndex        =   29
         Top             =   2150
         Width           =   2475
      End
      Begin VB.CommandButton cmdChiaviProdotto 
         Caption         =   "Chiavi prodotto"
         Height          =   380
         Left            =   120
         TabIndex        =   36
         Top             =   5750
         Width           =   2475
      End
      Begin VB.CommandButton cmdMigliorPosto 
         Caption         =   "Miglior posto"
         Height          =   380
         Left            =   120
         TabIndex        =   38
         Top             =   6950
         Width           =   2475
      End
      Begin VB.CommandButton cmdImpostazioneProtezioni 
         Caption         =   "Impostazione protezioni"
         Height          =   380
         Left            =   120
         TabIndex        =   37
         Top             =   6550
         Width           =   2475
      End
      Begin VB.CommandButton cmdPeriodiCommerciali 
         Caption         =   "Periodi commerciali"
         Height          =   380
         Left            =   120
         TabIndex        =   33
         Top             =   4150
         Width           =   2475
      End
      Begin VB.CommandButton cmdPrezzi 
         Caption         =   "Prezzi"
         Height          =   380
         Left            =   120
         TabIndex        =   34
         Top             =   4550
         Width           =   2475
      End
      Begin VB.CommandButton cmdAssociazioneSupporti 
         Caption         =   "Associazione tipi/layout supp."
         Height          =   380
         Left            =   120
         TabIndex        =   35
         Top             =   4950
         Width           =   2475
      End
      Begin VB.CommandButton cmdDurateRiservazioni 
         Caption         =   "Durate riservazioni"
         Height          =   380
         Left            =   120
         TabIndex        =   28
         Top             =   1750
         Width           =   2475
      End
      Begin VB.CommandButton cmdRappresentazioni 
         Caption         =   "Rappresentazioni"
         Height          =   380
         Left            =   120
         TabIndex        =   30
         Top             =   2550
         Width           =   2475
      End
      Begin VB.CommandButton cmdTariffe 
         Caption         =   "Tariffe"
         Height          =   380
         Left            =   120
         TabIndex        =   32
         Top             =   3750
         Width           =   2475
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   43
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idProdottoSelezionato As Long
Private isRecordEditabile As Boolean
Private nomeProdotto As String
Private descrizioneProdotto As String
Private descrizioneAlternativaProdotto As String
Private descrizioneStampaVenueProdotto As String
Private descrizionePOSProdotto As String
Private codiceTL As String
Private dataInizioConsegna As Date
Private dataFineConsegna As Date
Private nomeOrganizzazioneSelezionata As String
Private codiceTLOrganizzazioneSelezionata As String
Private codiceTLTipoProdottoSelezionato As String
Private nomePiantaSelezionata As String
Private stampaPossibileDaTL As Integer
Private sigilloFiscale As Integer
Private progressivoVendite As Long
Private progressivoTipografico As Long
Private propagazioneProtezioni As Integer
Private rateo As Long
Private aliquotaIVA As Long
Private idTipoProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idSitoSelezionato As Long
Private idStagioneSelezionata As Long
Private idContrattoSelezionato As Long
Private idManifestazioneSelezionata As Long
Private prodottoSalvatoInBD As Boolean
Private isProdottoAttivoSuTL As Boolean 'indica attivit� TL momentanea
Private quantit�AccessiConsentiti As Long
Private isProdottoVendibile As Boolean
Private idTipoTerminaleSelezionato As Long
Private listaTipiTerminale As Collection
Private idPiantaSIAESelezionata As Long
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private idLivelloGestioneAnagrafiche As LivelloGestioneAnagraficheEnum
'Private idLivelloGestioneContatti As LivelloGestioneContattiEnum
Private numeroMaxTitoliPerAcquirente As Long
Private isProdottoBOOpen As ValoreBooleanoEnum
Private isSospesoSuCPVInternet As ValoreBooleanoEnum
Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private idFormatoSupportoSelezionato As Long
Private numGiorniNecessariPerSpediz As Long
Private gestEccedenzeOmaggioEvolutaOrganizzazione As ValoreBooleanoEnum
Private gestEccedenzeOmaggioEvolutaProdotto As ValoreBooleanoEnum
Private aggiungiOperazioniDefaultANuovoProdotto As ValoreBooleanoEnum
Private isProdottoTMaster As ValoreBooleanoEnum
Private idTipoVenditaSuLisClickSelezionata As Long
Private idRuoloIlTuoAbbonamento As Long
Private idCausaleUtilizzoCreditiITA As Long
Private isSpesaCreditoITAObbligatoria As ValoreBooleanoEnum
Private IsProdottoTDL As ValoreBooleanoEnum
Private isTDLInVenditaSuALTURA As ValoreBooleanoEnum
Private isTDLInVenditaSuLOTTO As ValoreBooleanoEnum
Private isProdottoFiscale As ValoreBooleanoEnum
Private minutiAnnullamentoTitTradiz As Long
Private minutiAnnullamentoFineEvento As Long
Private numeroMassimoCessioniRateo As Long

Private internalEvent As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private TipoProdotto As TipoProdottoEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitaFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Const quantitaNulla As Long = -1

Public Sub Init()
    Dim sqlOrganizzazione As String
    Dim sqlPianta As String
    Dim sqlTipoProdotto As String
    Dim sqlTipoTerminale As String
    Dim sqlStagione As String
    Dim sqlPianteSIAE As String
    Dim sqlContratto As String
    Dim sqlSito As String
    Dim sqlManifestazione As String
    Dim sqlClasseProdotto As String
    Dim sqlFormatoSupporto As String
    Dim sqlLivelloGestioneAnagrafiche As String
'    Dim sqlLivelloGestioneContatti As String
    Dim sqlClassiPuntoVendita As String
    
    Call Variabili_Init
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    sqlOrganizzazione = "SELECT IDORGANIZZAZIONE AS ""ID""," & _
        " NOME, CODICETERMINALELOTTO FROM ORGANIZZAZIONE ORDER BY NOME"
    sqlPianta = "SELECT PIANTA.IDPIANTA AS ""ID"", VENUE.NOME AS ""VENUE"", PIANTA.NOME AS ""NOME""" & _
        " FROM PIANTA, VENUE_PIANTA, VENUE WHERE" & _
        " PIANTA.IDPIANTA = VENUE_PIANTA.IDPIANTA AND" & _
        " VENUE.IDVENUE = VENUE_PIANTA.IDVENUE" & _
        " ORDER BY ""VENUE"", ""NOME"""
    sqlTipoProdotto = "SELECT IDTIPOPRODOTTO ID, NOME || ' - ' || DESCRIZIONE LABEL" & _
        " FROM TIPOPRODOTTO WHERE IDTIPOPRODOTTO > 0" & _
        " ORDER BY NOME"
    sqlTipoTerminale = "SELECT IDTIPOTERMINALE AS ""ID"", NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0 AND IDTIPOTERMINALE <> " & TT_TERMINALE_CONTROLLO_ACCESSI & _
        " ORDER BY NOME"
    sqlStagione = "SELECT IDSTAGIONE AS ""ID""," & _
        " NOME FROM STAGIONE ORDER BY NOME"
    sqlManifestazione = "SELECT MIN(IDMANIFESTAZIONE) ID, NOME FROM MANIFESTAZIONE GROUP BY NOME" & _
        " ORDER BY NOME"
    sqlClasseProdotto = "SELECT IDCLASSEPRODOTTO ID, NOME FROM CLASSEPRODOTTO" & _
        " WHERE IDCLASSEPRODOTTO > 0" & _
        " ORDER BY NOME"
    sqlFormatoSupporto = "SELECT IDFORMATOSUPPORTO ID, NOME FROM FORMATOSUPPORTO" & _
        " ORDER BY NOME"
    sqlLivelloGestioneAnagrafiche = "SELECT IDLIVELLOGESTIONEANAGRAFICHE ID, NOME || ' - ' || DESCRIZIONE AS NOME FROM LIVELLOGESTIONEANAGRAFICHE" & _
        " ORDER BY NOME"
'    sqlLivelloGestioneContatti = "SELECT IDLIVELLOGESTIONECONTATTI ID, NOME || ' - ' || DESCRIZIONE AS NOME FROM LIVELLOGESTIONECONTATTI" & _
'        " ORDER BY NOME"
    sqlClassiPuntoVendita = "SELECT IDCLASSEPUNTOVENDITA ID, DESCRIZIONE AS NOME FROM CLASSEPUNTOVENDITA" & _
        " ORDER BY IDCLASSEPUNTOVENDITA"
        
    isProdottoAttivoSuTL = True
    Select Case modalitaFormCorrente
        Case A_NUOVO
            isRecordEditabile = True
            tipoStatoProdotto = TSP_IN_CONFIGURAZIONE
            prodottoSalvatoInBD = False
            Call SetIdProdottoSelezionato(idNessunElementoSelezionato)
            Call AssegnaValoriCampi
            Call CaricaValoriCombo(cmbOrganizzazioni, sqlOrganizzazione, "NOME", False)
            Call CaricaValoriCombo(cmbTipoProdotto, sqlTipoProdotto, "LABEL", False)
            Call CaricaValoriCombo(cmbStagione, sqlStagione, "NOME", False)
            Call CaricaValoriCombo(cmbManifestazione, sqlManifestazione, "NOME", True)
            Call CaricaValoriCombo(cmbClasseProdotto, sqlClasseProdotto, "NOME", False)
            Call CaricaValoriCombo(cmbFormatoSupporto, sqlFormatoSupporto, "NOME", False)
            Call CaricaValoriCombo(cmbLivelloGestioneAnagrafiche, sqlLivelloGestioneAnagrafiche, "NOME", False)
            'Call CaricaValoriCombo(cmbLivelloGestioneContatti, sqlLivelloGestioneContatti, "NOME", False)
'            Call CaricaValoriLista(lstClassiPuntoVendita, sqlClassiPuntoVendita)
            Call CaricaComboTipoVenditaSuLisClick
            Call CaricaComboRuoloIlTuoAbbonamento
            Call CaricaComboCausaleUtilizzoCreditiITA
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            
            Call AggiornaRegistroUltimiProdotti

            sqlPianteSIAE = "SELECT IDPIANTASIAE ID, NOME FROM PIANTASIAE" & _
                " WHERE IDPIANTA = " & idPiantaSelezionata & _
                " ORDER BY NOME"
            sqlContratto = "SELECT IDCONTRATTO ID, NOME FROM SETA_REP.CONTRATTO" & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND VISIBILE = 1" & _
                " ORDER BY NOME"
            sqlSito = "SELECT IDSITO ID, NOME FROM SITO" & _
                " ORDER BY NOME"
            Call CaricaValoriCombo(cmbOrganizzazioni, sqlOrganizzazione, "NOME", False)
            Call CaricaListaPianteComplete(cmbPiante, sqlPianta)
            Call CaricaValoriCombo(cmbContratto, sqlContratto, "NOME", False)
            Call CaricaComboSiti
            Call CaricaComboTipoVenditaSuLisClick
            Call CaricaComboRuoloIlTuoAbbonamento
            Call CaricaComboCausaleUtilizzoCreditiITA
            Call CaricaValoriCombo(cmbTipoProdotto, sqlTipoProdotto, "LABEL", False)
            Call CaricaValoriCombo(cmbStagione, sqlStagione, "NOME", False)
            Call CaricaValoriCombo(cmbPianteSIAE, sqlPianteSIAE, "NOME", False)
            Call CaricaValoriCombo(cmbManifestazione, sqlManifestazione, "NOME", True)
            Call CaricaValoriCombo(cmbClasseProdotto, sqlClasseProdotto, "NOME", False)
            Call CaricaValoriCombo(cmbFormatoSupporto, sqlFormatoSupporto, "NOME", False)
            Call CaricaValoriCombo(cmbLivelloGestioneAnagrafiche, sqlLivelloGestioneAnagrafiche, "NOME", False)
            'Call CaricaValoriCombo(cmbLivelloGestioneContatti, sqlLivelloGestioneContatti, "NOME", False)
'            Call CaricaValoriLista(lstClassiPuntoVendita, sqlClassiPuntoVendita)
            Call AssegnaValoriCampi
            If IsProdottoTDL = VB_FALSO Then
                Call IsPiantaCompleta(idPiantaSelezionata, False, True)
            End If
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            sqlContratto = "SELECT IDCONTRATTO ID, NOME FROM SETA_REP.CONTRATTO" & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " ORDER BY NOME"
            sqlSito = "SELECT IDCONTRATTO ID, NOME FROM SETA_REP.CONTRATTO" & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " ORDER BY NOME"
            Call CaricaValoriCombo(cmbOrganizzazioni, sqlOrganizzazione, "NOME", False)
            Call CaricaListaPianteComplete(cmbPiante, sqlPianta)
            Call CaricaValoriCombo(cmbContratto, sqlContratto, "NOME", False)
            Call CaricaComboSiti
            Call CaricaValoriCombo(cmbTipoProdotto, sqlTipoProdotto, "LABEL", False)
            Call CaricaValoriCombo(cmbStagione, sqlStagione, "NOME", False)
            Call CaricaValoriCombo(cmbManifestazione, sqlManifestazione, "NOME", True)
            Call CaricaValoriCombo(cmbClasseProdotto, sqlClasseProdotto, "NOME", False)
            Call CaricaValoriCombo(cmbFormatoSupporto, sqlFormatoSupporto, "NOME", False)
            Call CaricaValoriCombo(cmbLivelloGestioneAnagrafiche, sqlLivelloGestioneAnagrafiche, "NOME", False)
'            Call CaricaValoriCombo(cmbLivelloGestioneContatti, sqlLivelloGestioneContatti, "NOME", False)
            Call CaricaComboTipoVenditaSuLisClick
            Call CaricaComboRuoloIlTuoAbbonamento
            Call CaricaComboCausaleUtilizzoCreditiITA
'            Call CaricaValoriLista(lstClassiPuntoVendita, sqlClassiPuntoVendita)
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call AggiornaAbilitazioneControlli
    Call frmInizialeProdotto.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    If modalitaFormCorrente = A_NUOVO Then
        idProdottoSelezionato = idNessunElementoSelezionato
    End If
    nomeProdotto = ""
    descrizioneProdotto = ""
    descrizioneAlternativaProdotto = ""
    descrizioneStampaVenueProdotto = ""
    descrizionePOSProdotto = ""
    codiceTL = ""
    nomeOrganizzazioneSelezionata = ""
    codiceTLOrganizzazioneSelezionata = ""
    nomePiantaSelezionata = ""
    rateo = 0
    aliquotaIVA = idNessunElementoSelezionato
    stampaPossibileDaTL = VB_FALSO
    sigilloFiscale = VB_FALSO
    progressivoVendite = VB_FALSO
    progressivoTipografico = VB_FALSO
    propagazioneProtezioni = VB_FALSO
    isProdottoBOOpen = VB_FALSO
    isSospesoSuCPVInternet = VB_FALSO
    gestEccedenzeOmaggioEvolutaProdotto = VB_FALSO
    quantit�AccessiConsentiti = 0
    dataInizioConsegna = dataNulla
    dataFineConsegna = dataNulla
    idTipoProdottoSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idPiantaSelezionata = idNessunElementoSelezionato
    idSitoSelezionato = idNessunElementoSelezionato
    idStagioneSelezionata = idNessunElementoSelezionato
    idPiantaSIAESelezionata = idNessunElementoSelezionato
    idContrattoSelezionato = idNessunElementoSelezionato
    idManifestazioneSelezionata = idNessunElementoSelezionato
    idClasseProdottoSelezionata = idNessunElementoSelezionato
    idLivelloGestioneAnagrafiche = idNessunElementoSelezionato
'    idLivelloGestioneContatti = idNessunElementoSelezionato
    idFormatoSupportoSelezionato = idNessunElementoSelezionato
    rientraInDecretoSicurezza = VB_FALSO
    numeroMaxTitoliPerAcquirente = valoreLongNullo
    idTipoVenditaSuLisClickSelezionata = idNessunElementoSelezionato
    idRuoloIlTuoAbbonamento = idNessunElementoSelezionato
    idCausaleUtilizzoCreditiITA = idNessunElementoSelezionato
    isSpesaCreditoITAObbligatoria = VB_FALSO
    IsProdottoTDL = VB_FALSO
    isTDLInVenditaSuALTURA = VB_FALSO
    isTDLInVenditaSuLOTTO = VB_FALSO
    isProdottoFiscale = VB_FALSO
    
    aggiungiOperazioniDefaultANuovoProdotto = VB_FALSO
    
    idTipoTerminaleSelezionato = idNessunElementoSelezionato
    
    isProdottoTMaster = VB_FALSO
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalitaFormCorrente = mf
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetIsProdottoVendibile(isP As Boolean)
    isProdottoVendibile = isP
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
        Case TNCP_FINE
            Unload Me
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    Select Case modalitaFormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Prodotto"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Prodotto configurato - ID: " & idProdottoSelezionato
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Prodotto configurato - ID: " & idProdottoSelezionato
    End Select
    
    fraAzioni.Enabled = (modalitaFormCorrente <> A_NUOVO)
    fraAzioni.Visible = (modalitaFormCorrente <> A_NUOVO)
    cmdConferma.Enabled = txtNome.Text <> "" And _
                        txtDescrizioneAlternativa.Text <> "" And _
                        Trim(txtRateo.Text) <> "" And _
                        Len(Trim(txtCodiceTL.Text)) = 6 And _
                        cmbOrganizzazioni.ListIndex <> idNessunElementoSelezionato And _
                        cmbTipoProdotto.ListIndex <> idNessunElementoSelezionato And _
                        cmbPiante.ListIndex <> idNessunElementoSelezionato And _
                        cmbLivelloGestioneAnagrafiche.ListIndex <> idNessunElementoSelezionato And _
                        txtNome.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtRateo.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtAliquotaIVA.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtDescrizione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtDescrizioneAlternativa.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtDescrizioneStampaVenue.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtDescrizionePOS.Enabled = (modalitaFormCorrente <> A_ELIMINA)
                        txtNumeroMaxTitoliPerAcquirente.Enabled = (modalitaFormCorrente <> A_ELIMINA) And _
                        (idLivelloGestioneAnagrafiche = LGA_LIVELLO_2 Or idLivelloGestioneAnagrafiche = LGA_LIVELLO_3 Or idLivelloGestioneAnagrafiche = LGA_LIVELLO_4)
    
    txtQuantit�AccessiConsentiti.Enabled = (modalitaFormCorrente <> A_ELIMINA And (isProdottoBOOpen = VB_VERO Or idClasseProdottoSelezionata = CPR_OPEN))
    If isProdottoBOOpen = VB_FALSO Then
        txtQuantit�AccessiConsentiti.Text = ""
    End If
    txtCodiceTL.Enabled = (modalitaFormCorrente <> A_ELIMINA) And _
                            (cmbOrganizzazioni.Text <> "")
    txtCodiceTLOrganizzazione.Enabled = False
    txtCodiceTLTipoProdotto.Enabled = False
    lblNome.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblRateo.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblAliquotaIVA.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblDescrizioneAlternativa.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblDescrizioneStampaVenue.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblDescrizionePOS.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblNumeroMaxTitoliPerAcquirente.Enabled = (modalitaFormCorrente <> A_ELIMINA) And _
            (idLivelloGestioneAnagrafiche = LGA_LIVELLO_2 Or idLivelloGestioneAnagrafiche = LGA_LIVELLO_3 Or idLivelloGestioneAnagrafiche = LGA_LIVELLO_4)
    lblQuantit�AccessiConsentiti.Enabled = (modalitaFormCorrente <> A_ELIMINA) And (isProdottoBOOpen = VB_VERO Or idClasseProdottoSelezionata = CPR_OPEN)
    cmbStagione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblStagione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    cmbManifestazione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblManifestazione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    cmbContratto.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblContratto.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblCodiceTerminaleLotto.Enabled = (modalitaFormCorrente <> A_ELIMINA) And _
        (cmbOrganizzazioni.Text <> "")
    chkPropagazioneProtezioni.Enabled = (modalitaFormCorrente <> A_ELIMINA) And ((idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO))
    If (modalitaFormCorrente = A_NUOVO) And idLivelloGestioneAnagrafiche <> LGA_LIVELLO_4 Then
        chkRientraInDecretoSicurezza.Value = False
    End If
    chkRientraInDecretoSicurezza.Enabled = False
    
    cmbOrganizzazioni.Enabled = (modalitaFormCorrente = A_NUOVO)
    cmbPiante.Enabled = (modalitaFormCorrente = A_NUOVO)
    cmbClasseProdotto.Enabled = (modalitaFormCorrente = A_NUOVO)
    cmbPianteSIAE.Enabled = (modalitaFormCorrente = A_NUOVO)
    cmbTipoProdotto.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    cmbFormatoSupporto.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    cmbLivelloGestioneAnagrafiche.Enabled = (modalitaFormCorrente <> A_ELIMINA)
'    cmbLivelloGestioneContatti.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblOrganizzazione.Enabled = (modalitaFormCorrente = A_NUOVO)
    lblPianta.Enabled = (modalitaFormCorrente = A_NUOVO)
    lblClasseProdotto.Enabled = (modalitaFormCorrente = A_NUOVO)
    lblPianteSIAE.Enabled = (modalitaFormCorrente = A_NUOVO)
    lblTipoProdotto.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    fraAzioni.Enabled = (modalitaFormCorrente = A_MODIFICA)
    fraAzioni.Visible = (modalitaFormCorrente <> A_NUOVO)
    cmdDurateRiservazioni.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdClassiSuperAree.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdDateOperazioni.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdAbilitazioniSupportiDigitali.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdAssociazioneSupporti.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdAssociazioneLayoutRicevute.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdPrezzi.Enabled = (modalitaFormCorrente = A_MODIFICA)
    If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
        cmdRappresentazioni.Caption = "        Scelta rappresentazioni"
        cmdCapienze.Enabled = True
    Else
        cmdRappresentazioni.Caption = "Rappresentazioni"
        cmdCapienze.Enabled = False
    End If
    cmdRappresentazioni.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdChiaviProdotto.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdStampeAggiuntive.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdSistemaDiEmissione.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdTariffe.Enabled = (modalitaFormCorrente = A_MODIFICA)
'    cmdTitoliDiServizio.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdPeriodiCommerciali.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdImpostazioneProtezioni.Enabled = (modalitaFormCorrente = A_MODIFICA) And ((idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO))
    cmdMigliorPosto.Enabled = (modalitaFormCorrente = A_MODIFICA) And ((idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO))
    cmdModalitaFornitura.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdCausaliProtezione.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdCapienze.Enabled = (modalitaFormCorrente = A_MODIFICA) And (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO)
    cmdCambioUtilizzatoreSuSuperaree.Enabled = (modalitaFormCorrente = A_MODIFICA) And ((idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO) Or (idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO))
    chkProdottoOpen.Visible = (idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA)
    If (idClasseProdottoSelezionata = CPR_OPEN) And (quantit�AccessiConsentiti = 0) Then
        quantit�AccessiConsentiti = 1
        txtQuantit�AccessiConsentiti.Text = quantit�AccessiConsentiti
    End If
    txtQuantit�AccessiConsentiti.Visible = (idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA) Or (idClasseProdottoSelezionata = CPR_OPEN)
    lblQuantit�AccessiConsentiti.Visible = (idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA) Or (idClasseProdottoSelezionata = CPR_OPEN)
    chkGestioneTariffeOmaggio.Enabled = (modalitaFormCorrente <> A_ELIMINA And gestEccedenzeOmaggioEvolutaOrganizzazione = VB_VERO)
    If (idRuoloIlTuoAbbonamento = 1 Or idRuoloIlTuoAbbonamento = 2) Then
        cmbCausaleUtilizzoCreditiITA.Enabled = True
        chkSpesaCreditoObbligatoria.Enabled = True
    Else
        cmbCausaleUtilizzoCreditiITA.Enabled = False
        idCausaleUtilizzoCreditiITA = idNessunElementoSelezionato
        chkSpesaCreditoObbligatoria.Enabled = False
    End If
    chkFiscale.Visible = True
    If (chkFiscale.Value = vbUnchecked) Then
        lblMinutiAnnullamentoTitTradiz.Visible = True
        lblMinutiAnnullamentoTitTradiz2.Visible = True
        txtMinutiAnnullamentoTitTradiz.Visible = True
        txtMinutiAnnullamentoTitTradiz.Enabled = True
        lblMinutiAnnullamentoDaFineEvento.Visible = True
        lblMinutiAnnullamentoDaFineEvento2.Visible = True
        txtMinutiAnnullamentoDaFineEvento.Visible = True
        txtMinutiAnnullamentoDaFineEvento.Enabled = True
    Else
        lblMinutiAnnullamentoTitTradiz.Visible = False
        lblMinutiAnnullamentoTitTradiz2.Visible = False
        txtMinutiAnnullamentoTitTradiz.Visible = False
        lblMinutiAnnullamentoDaFineEvento.Visible = False
        lblMinutiAnnullamentoDaFineEvento2.Visible = False
        txtMinutiAnnullamentoDaFineEvento.Visible = False
    End If
    If (IsFiscalitaModificabile(idProdottoSelezionato)) Then
        chkFiscale.Enabled = True
    Else
        chkFiscale.Enabled = False
    End If
    cmdSuperareeTipiSupporto.Enabled = (modalitaFormCorrente = A_MODIFICA) And (idClasseProdottoSelezionata = CPR_MEDIUM)
    
    Select Case modalitaFormCorrente
        Case A_NUOVO
            cmdPrecedente.Enabled = False
            cmdEsci.Caption = "Abbandona"
            cmdConferma.Enabled = (Not prodottoSalvatoInBD) And _
                                    Trim(txtNome.Text) <> "" And _
                                    Trim(txtDescrizioneAlternativa.Text) <> "" And _
                                    Trim(txtRateo.Text) <> "" And _
                                    Len(Trim(txtCodiceTL.Text)) = 6 And _
                                    cmbOrganizzazioni.ListIndex <> idNessunElementoSelezionato And _
                                    cmbTipoProdotto.ListIndex <> idNessunElementoSelezionato And _
                                    cmbPiante.ListIndex <> idNessunElementoSelezionato And _
                                    cmbClasseProdotto.ListIndex <> idNessunElementoSelezionato And _
                                    cmbStagione.ListIndex <> idNessunElementoSelezionato And _
                                    cmbFormatoSupporto.ListIndex <> idNessunElementoSelezionato And _
                                    cmbTipoVenditaSuLisClick.ListIndex <> idNessunElementoSelezionato And _
                                    cmbRuoloIlTuoAbbonamento.ListIndex <> idNessunElementoSelezionato And _
                                    cmbLivelloGestioneAnagrafiche.ListIndex <> idNessunElementoSelezionato
            If ((idRuoloIlTuoAbbonamento = 1 Or idRuoloIlTuoAbbonamento = 2) And idCausaleUtilizzoCreditiITA = idNessunElementoSelezionato) Then
                cmdConferma.Enabled = False
            End If
            
            cmdAnnulla.Enabled = (gestioneExitCode <> EC_CONFERMA)
            cmdSuccessivo.Enabled = (Trim(txtNome.Text) <> "" And _
                                    (txtDescrizioneAlternativa.Text) <> "" And _
                                    (txtRateo.Text) <> "" And _
                                    Len(Trim(txtCodiceTL.Text)) = 6 And _
                                    cmbOrganizzazioni.ListIndex <> idNessunElementoSelezionato And _
                                    cmbClasseProdotto.ListIndex <> idNessunElementoSelezionato And _
                                    cmbPiante.ListIndex <> idNessunElementoSelezionato And _
                                    cmbFormatoSupporto.ListIndex <> idNessunElementoSelezionato And _
                                    gestioneExitCode = EC_CONFERMA)
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Visible = False
            cmdConferma.Visible = True
            cmdAnnulla.Visible = True
'            cmdConferma.Enabled = True
            cmdAnnulla.Enabled = True
            fraNavigazioneProdotto.Visible = False
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Visible = False
            cmdConferma.Visible = True
            cmdAnnulla.Visible = True
            cmdConferma.Enabled = True
            cmdAnnulla.Enabled = True
            fraNavigazioneProdotto.Visible = False
        Case Else
            'Do Nothing
    End Select
    
    ' Se si tratta di un prodotto "TMaster" alcuni campi non possono essere modificati
    If isProdottoTMaster Then
        txtNome.Enabled = False
        txtDescrizioneStampaVenue.Enabled = False
        txtDescrizione.Enabled = False
        txtDescrizionePOS.Enabled = False
        txtDescrizioneAlternativa.Enabled = False
        cmbStagione.Enabled = False
        cmbClasseProdotto.Enabled = False
        chkProdottoOpen.Enabled = False
        chkSospesoSuCPVInternet.Enabled = False
        txtQuantit�AccessiConsentiti.Enabled = False
        cmbTipoProdotto.Enabled = False
        cmbFormatoSupporto.Enabled = False
        cmbOrganizzazioni.Enabled = False
        cmbPiante.Enabled = False
        cmbSito.Enabled = False
        cmbTipoVenditaSuLisClick.Enabled = False
        cmbRuoloIlTuoAbbonamento.Enabled = False
        cmbCausaleUtilizzoCreditiITA.Enabled = False
        chkGestioneTariffeOmaggio.Enabled = False
        cmbPianteSIAE.Enabled = False
        txtCodiceTL.Enabled = False
        txtAliquotaIVA.Enabled = False
        txtRateo.Enabled = False
        chkPropagazioneProtezioni.Enabled = False
        cmbLivelloGestioneAnagrafiche.Enabled = False
'        cmbLivelloGestioneContatti.Enabled = False
        txtNumeroMaxTitoliPerAcquirente.Enabled = False
        chkRientraInDecretoSicurezza.Enabled = False
        
        cmdCausaliProtezione.Enabled = False
        cmdAssociazioneLayoutRicevute.Enabled = False
        cmdChiaviProdotto.Enabled = False
        cmdCambioUtilizzatoreSuSuperaree.Enabled = False
        cmdImpostazioneProtezioni.Enabled = False
        cmdMigliorPosto.Enabled = False
        cmdAbilitazioniSupportiDigitali.Enabled = False
    End If
    
    If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO Then
        txtNumeroMassimoCessioniRateo.Enabled = True
        lblNumeroMassimoCessioniRateo.Enabled = True
        lblNumeroMassimoCessioniRateo2.Enabled = True
    Else
        txtNumeroMassimoCessioniRateo.Enabled = False
        lblNumeroMassimoCessioniRateo.Enabled = False
        lblNumeroMassimoCessioniRateo2.Enabled = False
    End If
    
    ' Se si tratta di un prodotto "TDL" praticamente tutti i campi non possono essere modificati
    ' per il momento disabilito solo il conferma e i bottoni....
    If IsProdottoTDL Then
        cmdClassiSuperAree.Enabled = False
        cmdStampeAggiuntive.Enabled = False
        cmdDurateRiservazioni.Enabled = False
        cmdSistemaDiEmissione.Enabled = False
        cmdCapienze.Enabled = False
        cmdCausaliProtezione.Enabled = False
        cmdAssociazioneLayoutRicevute.Enabled = False
        cmdChiaviProdotto.Enabled = False
        cmdCambioUtilizzatoreSuSuperaree.Enabled = False
        cmdImpostazioneProtezioni.Enabled = False
        cmdMigliorPosto.Enabled = False
        cmdAbilitazioniSupportiDigitali.Enabled = False
        cmdModalitaFornitura.Enabled = False
        cmdLayoutEMailAcquistiInternet.Enabled = False
        
'        cmdConferma.Enabled = False
    End If
                         
End Sub

Private Sub chkFiscale_Click()
    If Not internalEvent Then
        Call chkFiscale_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkProdottoOpen_Click()
    If Not internalEvent Then
        Call chkProdottoOpen_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkSospesoSuCPVInternet_Click()
    If Not internalEvent Then
        Call chkSospesoSuCPVInternet_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub chkRientraInDecretoSicurezza_Click()
    If Not internalEvent Then
        Call chkRientraInDecretoSicurezza_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbContratto_Click()
    If Not internalEvent Then
        Call cmbContratto_Update
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbContratto_Update()
    Dim internalEventOld As Boolean
    
    idContrattoSelezionato = cmbContratto.ItemData(cmbContratto.ListIndex)
    Call CaricaFormDettagliContratto
    If frmDettagliContratto.GetExitCode = EC_ANNULLA Then
        idContrattoSelezionato = idNessunElementoSelezionato
        internalEventOld = internalEvent
        internalEvent = True
            Call SelezionaElementoSuCombo(cmbContratto, idNessunElementoSelezionato)
        internalEvent = internalEventOld
    End If
End Sub

Private Sub CaricaFormDettagliContratto()
    Call frmDettagliContratto.SetIdContrattoSelezionato(idContrattoSelezionato)
    Call frmDettagliContratto.Init
End Sub

Private Sub cmbFormatoSupporto_Click()
    If Not internalEvent Then
        idFormatoSupportoSelezionato = cmbFormatoSupporto.ItemData(cmbFormatoSupporto.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbManifestazione_Click()
    If Not internalEvent Then
        idManifestazioneSelezionata = cmbManifestazione.ItemData(cmbManifestazione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbPianteSIAE_Click()
    If Not internalEvent Then
        idPiantaSIAESelezionata = cmbPianteSIAE.ItemData(cmbPianteSIAE.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbSito_Click()
    If Not internalEvent Then
        idSitoSelezionato = cmbSito.ItemData(cmbSito.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbTipoVenditaSuLisClick_Click()
    If Not internalEvent Then
        idTipoVenditaSuLisClickSelezionata = cmbTipoVenditaSuLisClick.ItemData(cmbTipoVenditaSuLisClick.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbRuoloIlTuoAbbonamento_Click()
    If Not internalEvent Then
        idRuoloIlTuoAbbonamento = cmbRuoloIlTuoAbbonamento.ItemData(cmbRuoloIlTuoAbbonamento.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbCausaleUtilizzoCreditiITA_Click()
    If Not internalEvent Then
        idCausaleUtilizzoCreditiITA = cmbCausaleUtilizzoCreditiITA.ItemData(cmbCausaleUtilizzoCreditiITA.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAbilitazionePuntiVendita_Click()
    Call ConfiguraAbilitazionePuntiVendita
End Sub

Private Sub ConfiguraAbilitazionePuntiVendita()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormAbilitazionePuntiVendita
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Dim stringaNota As String

    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    Call SetGestioneExitCode(EC_ANNULLA)
    Select Case modalitaFormCorrente
        Case A_NUOVO
            Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, , stringaNota)
        Case Else
            'Do Nothing
    End Select
    Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idProdottoSelezionato, isProdottoBloccatoDaUtente)
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    Dim cont As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
    sql = "SELECT P.NOME, P.DESCRIZIONE, DESCRIZIONESTAMPAVENUE, P.DESCRIZIONEPOS, DESCRIZIONEALTERNATIVA,"
    sql = sql & " RATEO, ALIQUOTAIVA, P.CODICETERMINALELOTTO, IDPIANTASIAE, IDCONTRATTO,"
    sql = sql & " SIGILLOFISCALESUTITOLIVENDUTI, PROGRVENDITESUTITOLIVENDUTI, IDCLASSEPRODOTTO,"
    sql = sql & " PROGRTIPOGRSUTITOLIVENDUTI, PROPAGAZIOPROTEZIONICONSENTITA,"
    sql = sql & " DATAINIZIOCONSEGNATITSTAMPSUCC, DATAFINECONSEGNATITSTAMPSUCC, P.IDORGANIZZAZIONE,"
    sql = sql & " IDTIPOSTATOPRODOTTO, IDPIANTA, P.IDTIPOPRODOTTO, P.IDFORMATOSUPPORTO, IDMANIFESTAZIONE,"
    sql = sql & " NUMEROMASSIMOTITOLIPERACQ, RIENTRADECRETOSICUREZZA,"
    sql = sql & " IDSTAGIONE, O.CODICETERMINALELOTTO COD, O.NOME ORGAN, TP.CODICETERMINALELOTTO TIP,"
    sql = sql & " PRODOTTOOPEN, QUANTITAACCESSICONSENTITI,PRODOTTOSOSPESOSUCPVINTERNET,"
    sql = sql & " O.GESTECCEDENZEOMAGGIOEVOLUTA GESTECCEDENZEOMAGGIOEVOLUTAORG, P.GESTECCEDENZEOMAGGIOEVOLUTA GESTECCEDENZEOMAGGIOEVOLUTAPR, IDSITO, IDLIVELLOGESTIONEANAGRAFICHE,"
    sql = sql & " IDTIPOVENDITACANALEINTERNET, IDRUOLOILTUOABBONAMENTO, IDCAUSALEUTILIZZOCREDITIITA, SPESACREDITOITAOBBLIGATORIA, FISCALE, MINUTIANNULLAMENTOTITTRADIZ, MINUTIANNULLAMENTOFINEEVENTO, NUMEROMASSIMOCESSIONIRATEO"
    sql = sql & " FROM PRODOTTO P, ORGANIZZAZIONE O, TIPOPRODOTTO TP"
    sql = sql & " WHERE P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE"
    sql = sql & " AND P.IDTIPOPRODOTTO = TP.IDTIPOPRODOTTO"
    sql = sql & " AND IDPRODOTTO = " & idProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeProdotto = rec("NOME")
        descrizioneProdotto = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        descrizioneStampaVenueProdotto = IIf(IsNull(rec("DESCRIZIONESTAMPAVENUE")), "", rec("DESCRIZIONESTAMPAVENUE"))
        descrizionePOSProdotto = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        descrizioneAlternativaProdotto = rec("DESCRIZIONEALTERNATIVA")
        rateo = rec("RATEO")
        aliquotaIVA = IIf(IsNull(rec("ALIQUOTAIVA")), idNessunElementoSelezionato, rec("ALIQUOTAIVA"))
        codiceTL = IIf(IsNull(rec("CODICETERMINALELOTTO")), "", rec("CODICETERMINALELOTTO"))
        sigilloFiscale = rec("SIGILLOFISCALESUTITOLIVENDUTI")
        progressivoVendite = rec("PROGRVENDITESUTITOLIVENDUTI")
        progressivoTipografico = rec("PROGRTIPOGRSUTITOLIVENDUTI")
        propagazioneProtezioni = rec("PROPAGAZIOPROTEZIONICONSENTITA")
        dataInizioConsegna = IIf(IsNull(rec("DATAINIZIOCONSEGNATITSTAMPSUCC")), dataNulla, rec("DATAINIZIOCONSEGNATITSTAMPSUCC"))
        dataFineConsegna = IIf(IsNull(rec("DATAFINECONSEGNATITSTAMPSUCC")), dataNulla, rec("DATAFINECONSEGNATITSTAMPSUCC"))
        idOrganizzazioneSelezionata = rec("IDORGANIZZAZIONE")
        tipoStatoProdotto = rec("IDTIPOSTATOPRODOTTO")
        idPiantaSelezionata = rec("IDPIANTA")
        idSitoSelezionato = IIf(IsNull(rec("IDSITO")), idNessunElementoSelezionato, rec("IDSITO"))
        idTipoProdottoSelezionato = rec("IDTIPOPRODOTTO")
        idFormatoSupportoSelezionato = rec("IDFORMATOSUPPORTO")
        idContrattoSelezionato = IIf(IsNull(rec("IDCONTRATTO")), idNessunElementoSelezionato, rec("IDCONTRATTO"))
        idManifestazioneSelezionata = IIf(IsNull(rec("IDMANIFESTAZIONE")), idNessunElementoSelezionato, rec("IDMANIFESTAZIONE"))
        idStagioneSelezionata = rec("IDSTAGIONE")
        nomeOrganizzazioneSelezionata = rec("ORGAN")
        codiceTLOrganizzazioneSelezionata = IIf(IsNull(rec("COD")), "", rec("COD"))
        codiceTLTipoProdottoSelezionato = IIf(IsNull(rec("TIP")), "", rec("TIP"))
        idPiantaSIAESelezionata = rec("IDPIANTASIAE")
        idClasseProdottoSelezionata = rec("IDCLASSEPRODOTTO")
        numeroMaxTitoliPerAcquirente = IIf(IsNull(rec("NUMEROMASSIMOTITOLIPERACQ")), valoreLongNullo, rec("NUMEROMASSIMOTITOLIPERACQ"))
        rientraInDecretoSicurezza = IIf(rec("RIENTRADECRETOSICUREZZA") = 0, VB_FALSO, VB_VERO)
        idLivelloGestioneAnagrafiche = rec("IDLIVELLOGESTIONEANAGRAFICHE")
'        idLivelloGestioneContatti = rec("IDLIVELLOGESTIONECONTATTI")
        If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
            isProdottoBOOpen = rec("PRODOTTOOPEN")
            If Not IsNull(rec("QUANTITAACCESSICONSENTITI")) Then
                quantit�AccessiConsentiti = rec("QUANTITAACCESSICONSENTITI")
            End If
        End If
        isSospesoSuCPVInternet = rec("PRODOTTOSOSPESOSUCPVINTERNET")
        gestEccedenzeOmaggioEvolutaOrganizzazione = rec("GESTECCEDENZEOMAGGIOEVOLUTAORG")
        gestEccedenzeOmaggioEvolutaProdotto = rec("GESTECCEDENZEOMAGGIOEVOLUTAPR")
        idTipoVenditaSuLisClickSelezionata = rec("IDTIPOVENDITACANALEINTERNET")
        idRuoloIlTuoAbbonamento = rec("IDRUOLOILTUOABBONAMENTO")
        If Not IsNull(rec("IDCAUSALEUTILIZZOCREDITIITA")) Then
            idCausaleUtilizzoCreditiITA = rec("IDCAUSALEUTILIZZOCREDITIITA")
        End If
        If Not IsNull(rec("SPESACREDITOITAOBBLIGATORIA")) Then
            isSpesaCreditoITAObbligatoria = IIf(rec("SPESACREDITOITAOBBLIGATORIA") = 0, VB_FALSO, VB_VERO)
        End If
        isProdottoFiscale = rec("FISCALE")
        minutiAnnullamentoTitTradiz = IIf(IsNull(rec("MINUTIANNULLAMENTOTITTRADIZ")), quantitaNulla, rec("MINUTIANNULLAMENTOTITTRADIZ"))
        minutiAnnullamentoFineEvento = IIf(IsNull(rec("MINUTIANNULLAMENTOFINEEVENTO")), quantitaNulla, rec("MINUTIANNULLAMENTOFINEEVENTO"))
        numeroMassimoCessioniRateo = IIf(IsNull(rec("NUMEROMASSIMOCESSIONIRATEO")), quantitaNulla, rec("NUMEROMASSIMOCESSIONIRATEO"))
        
    End If
    rec.Close
    
    isProdottoTMaster = VB_FALSO
'    sql = "SELECT COUNT(*) CONT FROM TMASTERPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        cont = rec("CONT")
'    End If
'    rec.Close
'
'    If cont = 1 Then
'        isProdottoTMaster = VB_VERO
'    Else
'        If cont > 1 Then
'            MsgBox "Impossibile verificare se si tratta di un prodotto TMaster!"
'        End If
'    End If
        
    IsProdottoTDL = VB_FALSO
'    sql = "SELECT COUNT(*) CONT FROM TDLPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        cont = rec("CONT")
'    End If
'    rec.Close
'    If cont = 1 Then
'        IsProdottoTDL = VB_VERO
'    End If
'
'    If IsProdottoTDL Then
'        sql = "SELECT TDL_INVENDITASUALTURA, TDL_INVENDITASULOTTO"
'        sql = sql & " FROM PRODOTTO P"
'        sql = sql & " WHERE P.IDPRODOTTO = " & idProdottoSelezionato
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            If Not IsNull(rec("TDL_INVENDITASUALTURA")) Then
'                If rec("TDL_INVENDITASUALTURA") = 1 Then
'                    isTDLInVenditaSuALTURA = VB_VERO
'                Else
'                    isTDLInVenditaSuALTURA = VB_FALSO
'                End If
'            End If
'            If Not IsNull(rec("TDL_INVENDITASULOTTO")) Then
'                If rec("TDL_INVENDITASULOTTO") = 1 Then
'                    isTDLInVenditaSuLOTTO = VB_VERO
'                Else
'                    isTDLInVenditaSuLOTTO = VB_FALSO
'                End If
'            End If
'        End If
'        rec.Close
'    End If
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = nomeProdotto
    txtRateo.Text = IIf(rateo = 0, "", rateo)
'    txtAliquotaIVA.Text = IIf(aliquotaIVA = 2000, "", CStr(aliquotaIVA / 100))
    txtAliquotaIVA.Text = IIf(aliquotaIVA = idNessunElementoSelezionato, "", CStr(aliquotaIVA / 100))
'    txtAliquotaIVA.Text = CStr(aliquotaIVA / 100)
    txtDescrizione.Text = descrizioneProdotto
    txtDescrizioneAlternativa.Text = descrizioneAlternativaProdotto
    txtDescrizioneStampaVenue.Text = descrizioneStampaVenueProdotto
    txtDescrizionePOS.Text = descrizionePOSProdotto
    txtCodiceTL.Text = codiceTL
    txtCodiceTLTipoProdotto.Text = codiceTLTipoProdottoSelezionato
    txtCodiceTLOrganizzazione.Text = codiceTLOrganizzazioneSelezionata
'    txtNumeroMaxTitoliPerAcquirente.Text = IIf(rientraInDecretoSicurezza = VB_VERO, numeroMaxTitoliPerAcquirente, "")
    txtNumeroMaxTitoliPerAcquirente.Text = IIf(numeroMaxTitoliPerAcquirente <> valoreLongNullo, numeroMaxTitoliPerAcquirente, "")
    chkPropagazioneProtezioni.Value = propagazioneProtezioni
    chkPropagazioneProtezioni.Value = propagazioneProtezioni
    If rientraInDecretoSicurezza = VB_VERO Then
        chkRientraInDecretoSicurezza.Value = vbChecked
    Else
        chkRientraInDecretoSicurezza.Value = vbUnchecked
    End If
     
    Call SelezionaElementoSuCombo(cmbOrganizzazioni, idOrganizzazioneSelezionata)
    Call SelezionaElementoSuCombo(cmbPiante, idPiantaSelezionata)
    Call SelezionaElementoSuCombo(cmbSito, idSitoSelezionato)
    Call SelezionaElementoSuCombo(cmbContratto, idContrattoSelezionato)
    Call SelezionaElementoSuCombo(cmbTipoProdotto, idTipoProdottoSelezionato)
    Call SelezionaElementoSuCombo(cmbStagione, idStagioneSelezionata)
    Call SelezionaElementoSuCombo(cmbPianteSIAE, idPiantaSIAESelezionata)
    Call SelezionaElementoSuCombo(cmbManifestazione, idManifestazioneSelezionata)
    Call SelezionaElementoSuCombo(cmbClasseProdotto, idClasseProdottoSelezionata)
    Call SelezionaElementoSuCombo(cmbFormatoSupporto, idFormatoSupportoSelezionato)
    Call SelezionaElementoSuCombo(cmbLivelloGestioneAnagrafiche, idLivelloGestioneAnagrafiche)
    'Call SelezionaElementoSuCombo(cmbLivelloGestioneContatti, idLivelloGestioneContatti)
    Call SelezionaElementoSuCombo(cmbTipoVenditaSuLisClick, idTipoVenditaSuLisClickSelezionata)
    Call SelezionaElementoSuCombo(cmbRuoloIlTuoAbbonamento, idRuoloIlTuoAbbonamento)
    If idRuoloIlTuoAbbonamento = 1 Or idRuoloIlTuoAbbonamento = 2 Then
        Call SelezionaElementoSuCombo(cmbCausaleUtilizzoCreditiITA, idCausaleUtilizzoCreditiITA)
        If isSpesaCreditoITAObbligatoria = VB_VERO Then
            chkSpesaCreditoObbligatoria.Value = vbChecked
        Else
            chkSpesaCreditoObbligatoria.Value = vbUnchecked
        End If
    End If

    If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
        chkProdottoOpen.Value = isProdottoBOOpen
        txtQuantit�AccessiConsentiti.Text = IIf(isProdottoBOOpen = VB_VERO, quantit�AccessiConsentiti, "")
    End If
    If idClasseProdottoSelezionata = CPR_OPEN Then
        txtQuantit�AccessiConsentiti.Text = quantit�AccessiConsentiti
    End If
    
'    chkProdottoSpedibile.Value = isProdottoSpedibile
'    If isProdottoSpedibile Then
'        txtNumGiorniNecessariPerSpedizione = numGiorniNecessariPerSpediz
'    End If
    chkSospesoSuCPVInternet.Value = isSospesoSuCPVInternet
    chkGestioneTariffeOmaggio.Value = gestEccedenzeOmaggioEvolutaProdotto
    
'    chkTitoliDigitaliAbilitati.Value = titoliDigitaliAbilitati
'    chkRitiroTitoliPermesso.Value = ritiroTitoliPermesso
    
    chkProdottoTDL.Value = IsProdottoTDL
    If IsProdottoTDL Then
        frmReteVenditaProdottoTDL.Enabled = True
        chkTDLReteALTURA.Value = isTDLInVenditaSuALTURA
        chkTDLReteLOTTO.Value = isTDLInVenditaSuLOTTO
    Else
        frmReteVenditaProdottoTDL.Enabled = False
        chkTDLReteALTURA.Enabled = False
        chkTDLReteLOTTO.Enabled = False
    End If
    
    chkFiscale.Value = isProdottoFiscale
    If isProdottoFiscale = VB_VERO Then
        txtMinutiAnnullamentoTitTradiz.Enabled = False
        txtMinutiAnnullamentoDaFineEvento.Enabled = False
    Else
        txtMinutiAnnullamentoTitTradiz.Text = IIf(minutiAnnullamentoTitTradiz = quantitaNulla, "", minutiAnnullamentoTitTradiz)
        txtMinutiAnnullamentoDaFineEvento.Text = IIf(minutiAnnullamentoFineEvento = quantitaNulla, "", minutiAnnullamentoFineEvento)
    End If
    
    txtNumeroMassimoCessioniRateo.Text = IIf(numeroMassimoCessioniRateo = quantitaNulla, "", numeroMassimoCessioniRateo)
    
    internalEvent = internalEventOld
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoProdotto As Long
    Dim idNuovaClasseSuperareaProdotto As Long
    Dim i As Integer
    Dim j As Integer
    Dim tt As clsTipoTerm
    Dim n As Long
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim numeroOperazioniDefinite As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    
    idNuovoProdotto = OttieniIdentificatoreDaSequenza("SQ_PRODOTTO")
    sql = "INSERT INTO PRODOTTO (IDPRODOTTO, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
    sql = sql & " SIGILLOFISCALESUTITOLIVENDUTI, PROGRVENDITESUTITOLIVENDUTI, PROGRTIPOGRSUTITOLIVENDUTI,"
    sql = sql & " CODICETERMINALELOTTO, DESCRIZIONESTAMPAVENUE, RATEO, ALIQUOTAIVA, IDMANIFESTAZIONE,"
    sql = sql & " PROPAGAZIOPROTEZIONICONSENTITA, DATAINIZIOCONSEGNATITSTAMPSUCC, DATAFINECONSEGNATITSTAMPSUCC,"
    sql = sql & " IDTIPOSTATOPRODOTTO, IDCONTRATTO, IDTIPOPRODOTTO, IDORGANIZZAZIONE, IDPIANTA, IDSTAGIONE,"
    sql = sql & " IDPIANTASIAE, IDCLASSEPRODOTTO, PRODOTTOOPEN, QUANTITAACCESSICONSENTITI,PRODOTTOSOSPESOSUCPVINTERNET,"
    sql = sql & " RIENTRADECRETOSICUREZZA, NUMEROMASSIMOTITOLIPERACQ, IDFORMATOSUPPORTO,"
    sql = sql & " GESTECCEDENZEOMAGGIOEVOLUTA, IDSITO, IDTIPOVENDITACANALEINTERNET, IDRUOLOILTUOABBONAMENTO, IDLIVELLOGESTIONEANAGRAFICHE, IDCAUSALEUTILIZZOCREDITIITA, SPESACREDITOITAOBBLIGATORIA, FISCALE, MINUTIANNULLAMENTOTITTRADIZ, MINUTIANNULLAMENTOFINEEVENTO, NUMEROMASSIMOCESSIONIRATEO)"
    sql = sql & " VALUES (" & idNuovoProdotto & ", "
    sql = sql & SqlStringValue(nomeProdotto) & ", "
    sql = sql & SqlStringValue(descrizioneProdotto) & ", "
    sql = sql & SqlStringValue(descrizioneAlternativaProdotto) & ", "
    sql = sql & SqlStringValue(descrizionePOSProdotto) & ", "
    sql = sql & sigilloFiscale & ", " & progressivoVendite & ", " & progressivoTipografico & ", "
    sql = sql & SqlStringValue(UCase(codiceTL)) & ", "
    sql = sql & SqlStringValue(descrizioneStampaVenueProdotto) & ", "
    sql = sql & rateo & ", "
    sql = sql & IIf(aliquotaIVA = idNessunElementoSelezionato, "NULL", aliquotaIVA) & ", "
    sql = sql & IIf(idManifestazioneSelezionata = idNessunElementoSelezionato, "NULL", idManifestazioneSelezionata) & ", "
    sql = sql & propagazioneProtezioni & ", "
    sql = sql & IIf(dataInizioConsegna = dataNulla, "NULL", SqlDateValue(dataInizioConsegna)) & ", "
    sql = sql & IIf(dataFineConsegna = dataNulla, "NULL", SqlDateValue(dataFineConsegna)) & ", "
    sql = sql & tipoStatoProdotto & ", "
    sql = sql & IIf(idContrattoSelezionato = idNessunElementoSelezionato, "NULL", idContrattoSelezionato) & ", "
    sql = sql & idTipoProdottoSelezionato & ", " & idOrganizzazioneSelezionata & ", " & idPiantaSelezionata & ", "
    sql = sql & idStagioneSelezionata & ", " & idPiantaSIAESelezionata & ", " & idClasseProdottoSelezionata & ", "
    sql = sql & isProdottoBOOpen & ", "
    If idClasseProdottoSelezionata = CPR_OPEN Then
        sql = sql & quantit�AccessiConsentiti & ", "
    Else
        sql = sql & IIf(isProdottoBOOpen = VB_FALSO, "NULL", quantit�AccessiConsentiti) & ", "
    End If
    sql = sql & isSospesoSuCPVInternet & ", "
    sql = sql & rientraInDecretoSicurezza & ", "
'    sql = sql & IIf(rientraInDecretoSicurezza = VB_VERO, numeroMaxTitoliPerAcquirente, "NULL") & ", "
    sql = sql & IIf(numeroMaxTitoliPerAcquirente <> valoreLongNullo, numeroMaxTitoliPerAcquirente, "NULL") & ", "
    sql = sql & idFormatoSupportoSelezionato & ", "
'    sql = sql & isProdottoSpedibile & ", "
'    sql = sql & numGiorniNecessariPerSpediz & ", "
    sql = sql & gestEccedenzeOmaggioEvolutaProdotto & ", "
    sql = sql & IIf(idSitoSelezionato = idNessunElementoSelezionato, "NULL", idSitoSelezionato) & ", "
    sql = sql & idTipoVenditaSuLisClickSelezionata & ", "
    sql = sql & idRuoloIlTuoAbbonamento & ", "
    sql = sql & idLivelloGestioneAnagrafiche & ", "
'    sql = sql & idLivelloGestioneContatti & ", "
    If idCausaleUtilizzoCreditiITA = idNessunElementoSelezionato Then
        sql = sql & "NULL,"
    Else
        sql = sql & idCausaleUtilizzoCreditiITA & ", "
    End If
'    sql = sql & titoliDigitaliAbilitati & ", "
'    sql = sql & ritiroTitoliPermesso
    sql = sql & IIf(chkSpesaCreditoObbligatoria.Value = 1, 1, 0) & ", "
    sql = sql & isProdottoFiscale & ", "
    If minutiAnnullamentoTitTradiz = quantitaNulla Then
        sql = sql & "NULL,"
    Else
        sql = sql & minutiAnnullamentoTitTradiz & ", "
    End If
    If minutiAnnullamentoFineEvento = quantitaNulla Then
        sql = sql & "NULL,"
    Else
        sql = sql & minutiAnnullamentoFineEvento & ", "
    End If
    If numeroMassimoCessioniRateo = quantitaNulla Then
        sql = sql & "NULL"
    Else
        sql = sql & numeroMassimoCessioniRateo
    End If
    
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    numeroOperazioniDefinite = 0
'''''''''    For i = 1 To listaTipiTerminale.count
'''''''''        Set tt = listaTipiTerminale(i)
'''''''''        For j = 1 To tt.collTipiOperazioneSelezionate.count
'''''''''            dataOraInizio = tt.collTipiOperazioneSelezionate(j).dataOraInizio
'''''''''            dataOraFine = tt.collTipiOperazioneSelezionate(j).dataOraFine
'''''''''            sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE,"
'''''''''            sql = sql & " IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
'''''''''            sql = sql & " VALUES (" & idNuovoProdotto & ", "
'''''''''            sql = sql & tt.idTipoTerminale & ", "
'''''''''            sql = sql & tt.collTipiOperazioneSelezionate(j).idTipoOperazione & ", "
'''''''''            sql = sql & IIf(dataOraInizio = dataNulla, "NULL", SqlDateTimeValue(dataOraInizio)) & ", "
'''''''''            sql = sql & IIf(dataOraFine = dataNulla, "NULL", SqlDateTimeValue(dataOraFine)) & ")"
'''''''''            SETAConnection.Execute sql, n, adCmdText
'''''''''
'''''''''            numeroOperazioniDefinite = numeroOperazioniDefinite + 1
'''''''''        Next j
'''''''''    Next i
    
    sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI)"
    sql = sql & " VALUES (" & idNuovoProdotto & ", " & TMFT_STANDARD & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    If numeroOperazioniDefinite = 0 And aggiungiOperazioniDefaultANuovoProdotto = VB_VERO Then
        Call inserisciDefaultTipiOperazione(idNuovoProdotto, rientraInDecretoSicurezza)
    End If
    
'    ' PRODOTTO_CANALEDIVENDITA eredita i dati di PRODOTTO_TIPOTERMIN_TIPOOPERAZ
'    sql = "INSERT INTO PRODOTTO_CANALEDIVENDITA"
'    sql = sql & " SELECT IDPRODOTTO, C.IDCANALEDIVENDITA, MIN(PTT.DATAORAINIZIOVALIDITA), MAX(PTT.DATAORAFINEVALIDITA)"
'    sql = sql & " FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ PTT, CANALEDIVENDITA C"
'    sql = sql & " WHERE PTT.IDPRODOTTO = " & idProdottoSelezionato
'    sql = sql & " AND PTT.IDTIPOOPERAZIONE IN (" & TO_VENDITA & "," & TO_RISERVAZIONE & ")"
'    sql = sql & " AND PTT.DATAORAINIZIOVALIDITA IS NOT NULL"
'    sql = sql & " AND"
'    sql = sql & " ("
'    sql = sql & " IDCANALEDIVENDITA = " & TCDV_RETE_LIS & " AND PTT.IDTIPOTERMINALE = " & TT_TERMINALE_POS
'    sql = sql & " OR IDCANALEDIVENDITA = " & TCDV_RETE_ISTITUZIONALE & " AND PTT.IDTIPOTERMINALE = " & TT_TERMINALE_POS
'    sql = sql & " OR IDCANALEDIVENDITA = " & TCDV_CALL_CENTER_LIS & " AND PTT.IDTIPOTERMINALE = " & TT_TERMINALE_WEB
'    sql = sql & " OR IDCANALEDIVENDITA = " & TCDV_ALTRI_CALL_CENTER & " AND PTT.IDTIPOTERMINALE = " & TT_TERMINALE_TOTEM
'    sql = sql & " OR IDCANALEDIVENDITA = " & TCDV_INTERNET & " AND PTT.IDTIPOTERMINALE = " & TT_TERMINALE_WEB
'    sql = sql & " )"
'    sql = sql & " GROUP BY IDPRODOTTO, C.IDCANALEDIVENDITA"
    
    ' CLASSESUPERAREAPRODOTTO: tutte le superaree nella classe 'SETTORI ORDINARI' di default
    idNuovaClasseSuperareaProdotto = OttieniIdentificatoreDaSequenza("SQ_CLASSESUPERAREAPRODOTTO")
    sql = "INSERT INTO CLASSESUPERAREAPRODOTTO (IDCLASSESUPERAREAPRODOTTO, IDCLASSESUPERAREA, IDPRODOTTO)" & _
        " SELECT " & idNuovaClasseSuperareaProdotto & ", IDCLASSESUPERAREA, " & idNuovoProdotto & _
        " FROM CLASSESUPERAREA CSA" & _
        " WHERE CSA.IDPIANTA = " & idPiantaSelezionata & _
        " AND CSA.CLASSEPRINCIPALE = 1"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "INSERT INTO CLASSESUPERAREAPROD_SUPERAREA (IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA)" & _
        " SELECT " & idNuovaClasseSuperareaProdotto & ", IDAREA" & _
        " FROM AREA" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND IDTIPOAREA IN (" & TA_SUPERAREA_NON_NUMERATA & ", " & TA_SUPERAREA_NUMERATA & ")"
    SETAConnection.Execute sql, n, adCmdText
        
    ' CLASSESAPROD_PUNTOVENDITA eredita per default i dati dell'organizzazione
    sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
        " SELECT " & idNuovaClasseSuperareaProdotto & ", IDPUNTOVENDITA" & _
        " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND IDPUNTOVENDITA NOT IN (" & _
        " SELECT IDPUNTOVENDITA FROM CLASSESAPROD_PUNTOVENDITA WHERE IDCLASSESUPERAREAPRODOTTO = " & idNuovaClasseSuperareaProdotto & _
        ")"
    SETAConnection.Execute sql, n, adCmdText
    
    ' tipi anagrafica di default
    ' si assume che:
    ' - il tipo COGNOME-NOME abbia ID = 3
    ' - il tipo ANAGRAFICA-CEN abbia ID = 1
    If (idLivelloGestioneAnagrafiche = LGA_LIVELLO_1 Or idLivelloGestioneAnagrafiche = LGA_LIVELLO_3) Then
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idNuovoProdotto & ", " & RA_TITOLARE_FISICO & ", 3)"
        SETAConnection.Execute sql, n, adCmdText
    End If
    If (idLivelloGestioneAnagrafiche = LGA_LIVELLO_2 Or idLivelloGestioneAnagrafiche = LGA_LIVELLO_3) Then
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idNuovoProdotto & ", " & RA_ACQUIRENTE_FISICO & ", 3)"
        SETAConnection.Execute sql, n, adCmdText
    End If
    If (idLivelloGestioneAnagrafiche = LGA_LIVELLO_4) Then
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idNuovoProdotto & ", " & RA_TITOLARE_FISICO & ", 1)"
        SETAConnection.Execute sql, n, adCmdText
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idNuovoProdotto & ", " & RA_ACQUIRENTE_FISICO & ", 1)"
        SETAConnection.Execute sql, n, adCmdText
    End If
        
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call frmSceltaProdotto.SetIdRecordSelezionato(idNuovoProdotto)
    Call SetIdProdottoSelezionato(idNuovoProdotto)
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
'    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub ConfiguraCausaliProtezione()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneCausaliProtezione
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdCausaliProtezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraCausaliProtezione
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneClassiSuperAree
End Sub

Private Sub cmdClassiSuperAree_Click()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneClassiSuperAree
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdDateOperazioni_Click()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneDateOperazioni
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdDefaultOperazioniPerClassiPVSelezionate_Click()
    Call definisciDefaultTipiOperazionePerClassiPuntoVendita
End Sub

Private Sub definisciDefaultTipiOperazione()
    Dim risposta As VbMsgBoxResult
    
    risposta = MsgBox("Sei sicuro di voler associare i default e perdere le configurazioni eventualmente esistenti?", vbYesNo)
    If risposta = vbYes Then
        If idProdottoSelezionato = idNessunElementoSelezionato Then
            aggiungiOperazioniDefaultANuovoProdotto = VB_VERO
        Else
            aggiungiOperazioniDefaultANuovoProdotto = VB_FALSO
            Call inserisciDefaultTipiOperazione(idProdottoSelezionato, rientraInDecretoSicurezza)
        End If
    End If
End Sub

Private Sub definisciDefaultTipiOperazionePerClassiPuntoVendita()
MsgBox "Da implementare!"
'    If idProdottoSelezionato = idNessunElementoSelezionato Then
'        aggiungiOperazioniDefaultANuovoProdotto = VB_VERO
'    Else
'        aggiungiOperazioniDefaultANuovoProdotto = VB_FALSO
'        Call inserisciDefaultTipiOperazionePerTipoTerminale(idProdottoSelezionato, rientraInDecretoSicurezza, idTipoTerminaleSelezionato)
'    End If
End Sub

Private Sub cmdDefaultTipiOperazioni_Click()
    Call definisciDefaultTipiOperazione
End Sub

Private Sub cmdDefaultTipiOperazioniPerClassiPVSelezionate_Click()
    Call definisciDefaultTipiOperazionePerClassiPuntoVendita
End Sub
'
'Private Sub cmdDirittiOperatori_Click()
'    Call ConfiguraDirittiOperatori
'End Sub
'
'Private Sub ConfiguraDirittiOperatori()
'    If isProdottoBloccatoDaUtente Then
'        Call CaricaFormDirittiOperatori
'    Else
'        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
'    End If
'End Sub
'
'Private Sub CaricaFormDirittiOperatori()
'    Call frmConfigurazioneProdottoOperatore.SetIdProdottoSelezionato(idProdottoSelezionato)
'    Call frmConfigurazioneProdottoOperatore.SetNomeProdottoSelezionato(nomeProdotto)
'    Call frmConfigurazioneProdottoOperatore.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoOperatore.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoOperatore.SetIdPiantaSelezionata(idPiantaSelezionata)
'    Call frmConfigurazioneProdottoOperatore.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazioneProdottoOperatore.SetModalit�Form(modalitaFormCorrente)
'    Call frmConfigurazioneProdottoOperatore.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
'    Call frmConfigurazioneProdottoOperatore.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
'    Call frmConfigurazioneProdottoOperatore.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
'    Call frmConfigurazioneProdottoOperatore.Init
'End Sub

Private Sub cmdAbilitazioniSupportiDigitali_Click()
    Call ConfiguraAbilitazioniSupportiDigitali
End Sub

Private Sub ConfiguraAbilitazioniSupportiDigitali()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormAbilitazioniSupportiDigitali
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub CaricaFormAbilitazioniSupportiDigitali()
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.Init
End Sub

Private Sub cmdModalitaFornitura_Click()
    Call ConfiguraModalitaFornitura
End Sub

Private Sub ConfiguraModalitaFornitura()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormModalitaFornitura
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdLayoutEMailAcquistiInternet_Click()
    Call ConfiguraLayoutEMailAcquistiInternet
End Sub

Private Sub ConfiguraLayoutEMailAcquistiInternet()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormLayoutEMailAcquistiInternet
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdTipoStampante_Click()
    Call ConfiguraTipoStampante
End Sub

Private Sub ConfiguraTipoStampante()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormTipoStampante
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdGestioneAnagrafiche_Click()
    Call ConfiguraGestioneAnagrafiche
End Sub

Private Sub ConfiguraGestioneAnagrafiche()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormGestioneAnagrafiche
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdSuperareeTipiSupporto_Click()
    Call ConfiguraSuperareeTipiSupporto
End Sub

Private Sub ConfiguraSuperareeTipiSupporto()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormGestioneSuperareeTipiSupporto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        DoEvents
        Call Annulla
    End If
End Sub

Private Sub cmdImpostazioneProtezioni_Click()
    Call ConfiguraImpostazioneProtezioni
End Sub

Private Sub ConfiguraImpostazioneProtezioni()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormImpostazioneProtezioni
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdMigliorPosto_Click()
    Call ConfiguraPostiMigliori
End Sub

Private Sub ConfiguraPostiMigliori()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormPostiMigliori
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdStampeAggiuntive_Click()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneStampeAggiuntive
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub Command1_Click()

End Sub

Private Sub txtCodiceTL_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call ValorizzaAutomaticamenteCampoCorrelato
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
'        Call ValorizzaAutomaticamenteCampoCorrelatoDescrizione
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub ValorizzaAutomaticamenteCampoCorrelato()
    txtDescrizioneAlternativa.Text = Left$(txtNome.Text, 30)
End Sub

Private Sub txtDescrizioneAlternativa_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim j As Integer
    Dim tt As clsTipoTerm
    Dim campiDataConsegnaTitoli As String
    Dim n As Long
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim condizioniSQL As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'    campiDataConsegnaTitoli = ""
'    If Not StampaAbilitataDaTL Then
'        campiDataConsegnaTitoli = " DATAINIZIOCONSEGNATITSTAMPSUCC = " & _
'            IIf((dataInizioConsegna = dataNulla), "NULL,", "TO_DATE('" & dataInizioConsegna & "','DD/MM/YYYY'),")
'        campiDataConsegnaTitoli = campiDataConsegnaTitoli & _
'            " DATAFINECONSEGNATITSTAMPSUCC = " & _
'            IIf((dataFineConsegna = dataNulla), "NULL,", "TO_DATE('" & dataFineConsegna & "','DD/MM/YYYY'),")
'    End If
    
    SETAConnection.BeginTrans
    sql = " UPDATE PRODOTTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeProdotto) & ","
    sql = sql & " DESCRIZIONEALTERNATIVA = " & SqlStringValue(descrizioneAlternativaProdotto) & ","
    sql = sql & " CODICETERMINALELOTTO = " & SqlStringValue(codiceTL) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneProdotto) & ","
    sql = sql & " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOSProdotto) & ","
    sql = sql & " DESCRIZIONESTAMPAVENUE = " & SqlStringValue(descrizioneStampaVenueProdotto) & ","
    sql = sql & " ALIQUOTAIVA = " & IIf(aliquotaIVA = idNessunElementoSelezionato, "NULL", aliquotaIVA) & ","
    sql = sql & campiDataConsegnaTitoli
    sql = sql & " SIGILLOFISCALESUTITOLIVENDUTI = " & sigilloFiscale & ","
    sql = sql & " PROGRVENDITESUTITOLIVENDUTI = " & progressivoVendite & ","
    sql = sql & " PROGRTIPOGRSUTITOLIVENDUTI = " & progressivoTipografico & ","
    sql = sql & " PROPAGAZIOPROTEZIONICONSENTITA = " & propagazioneProtezioni & ","
    sql = sql & " RATEO = " & rateo & ","
    sql = sql & " IDPIANTASIAE = " & idPiantaSIAESelezionata & ","
    sql = sql & " IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ","
    sql = sql & " IDPIANTA = " & idPiantaSelezionata & ","
    sql = sql & " IDSITO = " & IIf(idSitoSelezionato = idNessunElementoSelezionato, "NULL", idSitoSelezionato) & ","
    sql = sql & " IDTIPOVENDITACANALEINTERNET = " & idTipoVenditaSuLisClickSelezionata & ","
    sql = sql & " IDRUOLOILTUOABBONAMENTO = " & idRuoloIlTuoAbbonamento & ","
    sql = sql & " IDMANIFESTAZIONE = " & IIf(idManifestazioneSelezionata = idNessunElementoSelezionato, "NULL", idManifestazioneSelezionata) & ","
    sql = sql & " IDCONTRATTO = " & IIf(idContrattoSelezionato = idNessunElementoSelezionato, "NULL", idContrattoSelezionato) & ","
    sql = sql & " IDTIPOPRODOTTO = " & idTipoProdottoSelezionato & ","
    sql = sql & " IDTIPOSTATOPRODOTTO = " & tipoStatoProdotto & ","
    sql = sql & " IDSTAGIONE = " & idStagioneSelezionata & ", "
    sql = sql & " IDCLASSEPRODOTTO = " & idClasseProdottoSelezionata & ", "
    sql = sql & " PRODOTTOOPEN = " & isProdottoBOOpen & ", "
    sql = sql & " PRODOTTOSOSPESOSUCPVINTERNET = " & isSospesoSuCPVInternet & ", "
    sql = sql & " RIENTRADECRETOSICUREZZA = " & rientraInDecretoSicurezza & ", "
'    sql = sql & " NUMEROMASSIMOTITOLIPERACQ = " & IIf(rientraInDecretoSicurezza = VB_VERO, numeroMaxTitoliPerAcquirente, "NULL") & ", "
    sql = sql & " NUMEROMASSIMOTITOLIPERACQ = " & IIf(numeroMaxTitoliPerAcquirente <> valoreLongNullo, numeroMaxTitoliPerAcquirente, "NULL") & ", "
    If idClasseProdottoSelezionata = CPR_OPEN Then
        sql = sql & " QUANTITAACCESSICONSENTITI = " & quantit�AccessiConsentiti & ", "
    Else
        sql = sql & " QUANTITAACCESSICONSENTITI = " & IIf(isProdottoBOOpen = VB_FALSO, "NULL", quantit�AccessiConsentiti) & ", "
    End If
    sql = sql & " IDFORMATOSUPPORTO = " & idFormatoSupportoSelezionato & ", "
'    sql = sql & " SPEDIZIONETITOLIPERMESSA = " & isProdottoSpedibile & ", "
'    sql = sql & " NUMGIORNINECESSARIPERSPEDIZ = " & numGiorniNecessariPerSpediz & ", "
'    sql = sql & " GESTECCEDENZEOMAGGIOEVOLUTA = " & gestEccedenzeOmaggioEvolutaProdotto & ", "
    sql = sql & " GESTECCEDENZEOMAGGIOEVOLUTA = " & gestEccedenzeOmaggioEvolutaProdotto & ", "
'    sql = sql & " TITOLIDIGITALIABILITATI = " & titoliDigitaliAbilitati & ", "
'    sql = sql & " RITIROTITOLIPERMESSO = " & ritiroTitoliPermesso
    sql = sql & " IDLIVELLOGESTIONEANAGRAFICHE = " & idLivelloGestioneAnagrafiche & ", "
'    sql = sql & " IDLIVELLOGESTIONECONTATTI = " & idLivelloGestioneContatti & ", "
    If idCausaleUtilizzoCreditiITA = idNessunElementoSelezionato Then
        sql = sql & " IDCAUSALEUTILIZZOCREDITIITA = NULL,"
    Else
        sql = sql & " IDCAUSALEUTILIZZOCREDITIITA = " & idCausaleUtilizzoCreditiITA & ", "
    End If
    sql = sql & " SPESACREDITOITAOBBLIGATORIA = " & IIf(chkSpesaCreditoObbligatoria.Value = 1, 1, 0) & ", "
    sql = sql & " FISCALE = " & isProdottoFiscale & ", "
    If (isProdottoFiscale = VB_VERO) Then
        sql = sql & " MINUTIANNULLAMENTOTITTRADIZ = NULL, "
        sql = sql & " MINUTIANNULLAMENTOFINEEVENTO = NULL,"
    Else
        If minutiAnnullamentoTitTradiz = quantitaNulla Then
            sql = sql & " MINUTIANNULLAMENTOTITTRADIZ = NULL, "
        Else
            sql = sql & " MINUTIANNULLAMENTOTITTRADIZ = " & minutiAnnullamentoTitTradiz & ", "
        End If
        If minutiAnnullamentoFineEvento = quantitaNulla Then
            sql = sql & " MINUTIANNULLAMENTOFINEEVENTO = NULL, "
        Else
            sql = sql & " MINUTIANNULLAMENTOFINEEVENTO = " & minutiAnnullamentoFineEvento & ", "
        End If
    End If
    If numeroMassimoCessioniRateo = quantitaNulla Then
        sql = sql & " NUMEROMASSIMOCESSIONIRATEO = NULL"
    Else
        sql = sql & " NUMEROMASSIMOCESSIONIRATEO = " & numeroMassimoCessioniRateo
    End If
    
    sql = sql & " WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    ' Questo codice � replicato nella confermaDateOperazioni
'    sql = "DELETE FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO = " & idProdottoSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    
'    For i = 1 To listaTipiTerminale.count
'        Set tt = listaTipiTerminale(i)
'        For j = 1 To tt.collTipiOperazioneSelezionate.count
'            dataOraInizio = tt.collTipiOperazioneSelezionate(j).dataOraInizio
'            dataOraFine = tt.collTipiOperazioneSelezionate(j).dataOraFine
'            sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE,"
'            sql = sql & " IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
'            sql = sql & " VALUES (" & idProdottoSelezionato & ", "
'            sql = sql & tt.idTipoTerminale & ", "
'            sql = sql & tt.collTipiOperazioneSelezionate(j).idTipoOperazione & ", "
'            sql = sql & IIf(dataOraInizio = dataNulla, "NULL", SqlDateTimeValue(dataOraInizio)) & ", "
'            sql = sql & IIf(dataOraFine = dataNulla, "NULL", SqlDateTimeValue(dataOraFine)) & ")"
'            SETAConnection.Execute sql, n, adCmdText
'        Next j
'    Next i
    ' replicato fino a qui!
    
    ' per ora non faccio nulla su PRODOTTO_CANALEDIVENDITA
    
    ' in caso di prodotti TDL bisogna gestire i flag e la rete di vendita
    If IsProdottoTDL Then
        sql = "UPDATE PRODOTTO"
        If chkTDLReteALTURA.Value = 1 Then
            sql = sql & " SET TDL_INVENDITASUALTURA = 1,"
        Else
            sql = sql & " SET TDL_INVENDITASUALTURA = 0,"
        End If
        If chkTDLReteLOTTO.Value = 1 Then
            sql = sql & " TDL_INVENDITASULOTTO = 1"
        Else
            sql = sql & " TDL_INVENDITASULOTTO = 0"
        End If
        sql = sql & " WHERE IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
                " WHERE IDCLASSESUPERAREAPRODOTTO IN (" & _
                " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                " FROM CLASSESUPERAREAPRODOTTO" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                ")"
        SETAConnection.Execute sql, n, adCmdText
        
        If chkTDLReteALTURA.Value = 1 Then
            sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA(IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
                    " SELECT CSP.IDCLASSESUPERAREAPRODOTTO, OCP.IDPUNTOVENDITA" & _
                    " FROM PRODOTTO P, CLASSESUPERAREAPRODOTTO CSP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, SETA_GESTIONE.PUNTOVENDITA_SERVIZIO_STATO PVSS" & _
                    " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
                    " AND P.IDORGANIZZAZIONE = OCP.IDORGANIZZAZIONE" & _
                    " AND OCP.IDCLASSEPUNTOVENDITA = " & TCPV_RETE_LISTICKET & _
                    " AND OCP.IDPUNTOVENDITA = PVSS.IDPUNTOVENDITA" & _
                    " AND P.IDPRODOTTO = CSP.IDPRODOTTO" & _
                    " AND PVSS.TIPOTERMINALE = 'ALTURA'"
            SETAConnection.Execute sql, n, adCmdText
        End If
        If chkTDLReteLOTTO.Value = 1 Then
            sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA(IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
                    " SELECT CSP.IDCLASSESUPERAREAPRODOTTO, OCP.IDPUNTOVENDITA" & _
                    " FROM PRODOTTO P, CLASSESUPERAREAPRODOTTO CSP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, SETA_GESTIONE.PUNTOVENDITA_SERVIZIO_STATO PVSS" & _
                    " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
                    " AND P.IDORGANIZZAZIONE = OCP.IDORGANIZZAZIONE" & _
                    " AND OCP.IDCLASSEPUNTOVENDITA = " & TCPV_RETE_LISTICKET & _
                    " AND OCP.IDPUNTOVENDITA = PVSS.IDPUNTOVENDITA" & _
                    " AND P.IDPRODOTTO = CSP.IDPRODOTTO" & _
                    " AND PVSS.TIPOTERMINALE = 'LOTTO'"
            SETAConnection.Execute sql, n, adCmdText
        End If

    End If
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Public Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As String

    Call ApriConnessioneBD
    
'    sql = "DELETE FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO = " & idProdottoSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_CANALEDIVENDITA WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_CAUSALERISERVAZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM SELPOSTIMIGLIORI_CAUSPROTEZ WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM SELPOSTIMIGLIORI_MAXCONTIGUI WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM OPERATORE_CAUSALEPROTEZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM OPERATORE_PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_MODALITAEMISSIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_CHIAVEPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
        sql = "DELETE FROM CAPIENZATURNOLIBERO WHERE IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
    End If
    Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idProdottoSelezionato, isProdottoBloccatoDaUtente)
    sql = "DELETE FROM PRODOTTO_TIPOMODALITAFORNITURA WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM UTILIZZOTEMPLATE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
        " WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM CLASSESUPERAREAPROD_SUPERAREA" & _
        " WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    ' Modifica eseguita da Giovanni e Lorenzo su segnalazione di Dario Scarfina
    sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
        " WHERE IDCLASSESUPERAREAPRODOTTO IN" & _
        " (SELECT IDCLASSESUPERAREAPRODOTTO FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
        
    sql = "DELETE FROM CLASSESUPERAREAPRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM CLASSEPV_CAUSALEPROTEZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM TSDIGOBBLIGOIDENTIFICAZIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM UTILIZZOTEMPLATE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_HTLAYOUT WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM ATTIVITAPIANIFICATA WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM SOLDOUT WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_LAYOUTALFRESCO WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PROD_TERM_TIPOSTAMPANTE WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM PRODOTTO_RUOLO_TIPOANAGRAFICA WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD

End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String, includiNessuno As Boolean)
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim i As Integer

'    Call ApriConnessioneBD
    Call ApriConnessioneBD_ORA

    sql = strSQL

'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

    If includiNessuno Then
        If i <= 0 Then
            i = 1
        Else
            'Do Nothing
        End If
        cmb.AddItem "<nessuno>"
        cmb.ItemData(i - 1) = idNessunElementoSelezionato
    End If

'    Call ChiudiConnessioneBD
    Call ChiudiConnessioneBD_ORA

End Sub
'
'Private Sub chkTitoliDigitaliAbilitati_Click()
'    If Not internalEvent Then
'        Call chkTitoliDigitaliAbilitati_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub chkRitiroTitoliPermesso_Click()
'    If Not internalEvent Then
'        Call chkRitiroTitoliPermesso_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub

Private Sub chkRientraInDecretoSicurezza_Update()
    rientraInDecretoSicurezza = chkRientraInDecretoSicurezza.Value
End Sub

Private Sub chkProdottoOpen_Update()
    isProdottoBOOpen = chkProdottoOpen.Value
End Sub

Private Sub chkFiscale_Update()
    If (IsFiscalitaModificabile(idProdottoSelezionato)) Then
        isProdottoFiscale = chkFiscale.Value
    Else
        MsgBox "Impossibile modificare la fiscalita' del prodotto! (Esistono titoli)"
    End If
End Sub

Private Sub chkSospesoSuCPVInternet_Update()
    isSospesoSuCPVInternet = chkSospesoSuCPVInternet.Value
End Sub
'
'Private Sub chkProdottoSpedibile_Update()
'    isProdottoSpedibile = chkProdottoSpedibile.Value
'End Sub
'
'Private Sub chkTitoliDigitaliAbilitati_Update()
'    titoliDigitaliAbilitati = chkTitoliDigitaliAbilitati.Value
'End Sub
'
'Private Sub chkRitiroTitoliPermesso_Update()
'    ritiroTitoliPermesso = chkRitiroTitoliPermesso.Value
'End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub cmbOrganizzazioni_Click()
    If Not internalEvent Then
        Call CaricaComboDipendentiDaOrganizzazione
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaComboDipendentiDaOrganizzazione()
    Dim sql As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    idOrganizzazioneSelezionata = cmbOrganizzazioni.ItemData(cmbOrganizzazioni.ListIndex)
    nomeOrganizzazioneSelezionata = cmbOrganizzazioni.Text
    sql = "SELECT IDCONTRATTO ID, NOME FROM SETA_REP.CONTRATTO" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY NOME"
    Call CercaPiantePerOrganizzazione
    Call CaricaValoriCombo(cmbContratto, sql, "NOME", False)
    Call SelezionaCodiceTLOrganizzazione
    Call SelezionaCodiceTLTipoProdotto
    Call AssegnaValoreCodiceTLOrganizzazione
    Call AssegnaValoreCheckGestioneTariffeOmaggio
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub AssegnaValoreCheckGestioneTariffeOmaggio()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim gestioneEccedenzeOmaggio As Boolean
    
    Call ApriConnessioneBD

    sql = "SELECT GESTECCEDENZEOMAGGIOEVOLUTA FROM ORGANIZZAZIONE" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        gestioneEccedenzeOmaggio = (rec("GESTECCEDENZEOMAGGIOEVOLUTA") = 1)
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    chkGestioneTariffeOmaggio.Enabled = gestioneEccedenzeOmaggio
End Sub

Private Sub AssegnaValoreCodiceTLOrganizzazione()
    txtCodiceTLOrganizzazione.Text = UCase(codiceTLOrganizzazioneSelezionata)
End Sub

Private Sub AssegnaValoreCodiceTLTipoProdotto()
    txtCodiceTLTipoProdotto.Text = UCase(codiceTLTipoProdottoSelezionato)
End Sub

Private Sub cmbPiante_Click()
    If Not internalEvent Then
        idPiantaSelezionata = cmbPiante.ItemData(cmbPiante.ListIndex)
        Call CaricaComboPianteSIAE
        Call CaricaComboSiti
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaComboPianteSIAE()
    Dim sql As String
    
    Call cmbPianteSIAE.Clear
    sql = "SELECT IDPIANTASIAE ID, NOME FROM PIANTASIAE" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbPianteSIAE, sql, "NOME", False)
End Sub

Private Sub CaricaComboSiti()
    Dim sql As String
    
    Call cmbSito.Clear
    sql = "SELECT S.IDSITO ID, S.NOME" & _
        " FROM SITO S, VENUE_SITO VS, VENUE_PIANTA VP" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND VP.IDVENUE = VS.IDVENUE" & _
        " AND VS.IDSITO = S.IDSITO" & _
        " ORDER BY S.NOME"
    Call CaricaValoriCombo(cmbSito, sql, "NOME", True)
End Sub

' la vendita in pianta va abilitata solo se � possibile
Private Sub CaricaComboTipoVenditaSuLisClick()
    Dim sql As String
    
    Call cmbTipoVenditaSuLisClick.Clear
    sql = "SELECT IDTIPOVENDITACANALEINTERNET ID, NOME" & _
            " FROM TIPOVENDITACANALEINTERNET" & _
            " WHERE IDTIPOVENDITACANALEINTERNET = 1" & _
            " UNION" & _
            " SELECT TV.IDTIPOVENDITACANALEINTERNET ID, TV.NOME" & _
            " FROM TIPOVENDITACANALEINTERNET TV, PRODOTTO PR, PIANTA PT" & _
            " WHERE TV.IDTIPOVENDITACANALEINTERNET <> 1" & _
            " AND PR.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PR.IDPIANTA = PT.IDPIANTA" & _
            " AND PT.IDMAPPAIMPIANTO IS NOT NULL" & _
            " AND PT.PIANTAINTESTSULISCLICK = 0" & _
            " ORDER BY NOME"
    Call CaricaValoriCombo(cmbTipoVenditaSuLisClick, sql, "NOME", False)
End Sub

Private Sub CaricaComboRuoloIlTuoAbbonamento()
    Dim sql As String
    
    Call cmbRuoloIlTuoAbbonamento.Clear
    sql = "SELECT IDRUOLOILTUOABBONAMENTO ID, DESCRIZIONE NOME" & _
            " FROM RUOLOILTUOABBONAMENTO" & _
            " ORDER BY IDRUOLOILTUOABBONAMENTO"
    Call CaricaValoriCombo(cmbRuoloIlTuoAbbonamento, sql, "NOME", False)
End Sub

Private Sub CaricaComboCausaleUtilizzoCreditiITA()
    Dim sql As String
    
    Call cmbCausaleUtilizzoCreditiITA.Clear
    sql = "SELECT IDCAUSALEUTILIZZOCREDITIITA ID, DESCRIZIONE NOME" & _
            " FROM SETA_INTEGRA.CAUSALEUTILIZZOCREDITIITA" & _
            " ORDER BY IDCAUSALEUTILIZZOCREDITIITA"
    Call CaricaValoriCombo(cmbCausaleUtilizzoCreditiITA, sql, "NOME", False)
End Sub

Private Sub cmbStagione_Click()
    If Not internalEvent Then
        idStagioneSelezionata = cmbStagione.ItemData(cmbStagione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbClasseProdotto_Click()
    If Not internalEvent Then
        idClasseProdottoSelezionata = cmbClasseProdotto.ItemData(cmbClasseProdotto.ListIndex)
        If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO Then
             chkPropagazioneProtezioni.Value = vbChecked
        End If
        If idClasseProdottoSelezionata = CPR_GIORNALIERO Or idClasseProdottoSelezionata = CPR_TESSERA Or idClasseProdottoSelezionata = CPR_OPEN Then
             chkPropagazioneProtezioni.Enabled = False
        Else
             chkPropagazioneProtezioni.Enabled = True
        End If
        
        AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmbLivelloGestioneAnagrafiche_Click()
    If Not internalEvent Then
        AggiornaLivelloGestioneAnagrafiche
    End If
End Sub
'
'Private Sub cmbLivelloGestioneContatti_Click()
'    If Not internalEvent Then
'        AggiornaLivelloGestioneContatti
'    End If
'End Sub
'
'Private Sub AggiornaLivelloGestioneContatti()
'    idLivelloGestioneContatti = cmbLivelloGestioneContatti.ItemData(cmbLivelloGestioneContatti.ListIndex)
'    AggiornaAbilitazioneControlli
'End Sub

Private Sub AggiornaLivelloGestioneAnagrafiche()
    idLivelloGestioneAnagrafiche = cmbLivelloGestioneAnagrafiche.ItemData(cmbLivelloGestioneAnagrafiche.ListIndex)
    If idLivelloGestioneAnagrafiche = LGA_LIVELLO_4 Then
        chkRientraInDecretoSicurezza.Value = vbChecked
        rientraInDecretoSicurezza = VB_VERO
'        chkRientraInDecretoSicurezza.Enabled = True
    Else
        chkRientraInDecretoSicurezza.Value = vbUnchecked
        rientraInDecretoSicurezza = VB_FALSO
        chkRientraInDecretoSicurezza.Enabled = False
    End If
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmbTipoProdotto_Click()
    If Not internalEvent Then
        idTipoProdottoSelezionato = cmbTipoProdotto.ItemData(cmbTipoProdotto.ListIndex)
        Call SelezionaCodiceTLTipoProdotto
        Call AssegnaValoreCodiceTLTipoProdotto
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdDurateRiservazioni_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraDurateRiservazioni
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraDurateRiservazioni()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneDurateRiservazioni
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdRappresentazioni_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraRappresentazioni
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraRappresentazioni()
    If isProdottoBloccatoDaUtente Then
        If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
            Call CaricaFormConfigurazioneSceltaRappresentazioni
        Else
            Call CaricaFormConfigurazioneRappresentazioni
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub CaricaFormConfigurazioneDurateRiservazioni()
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoDurateRiservazioni.SetIsProdottoTMaster(isProdottoTMaster)
    Call frmConfigurazioneProdottoDurateRiservazioni.Init
End Sub

Private Sub CaricaFormConfigurazioneClassiSuperAree()
    Call frmConfigurazioneProdottoClassiSuperAree.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoClassiSuperAree.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoClassiSuperAree.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoClassiSuperAree.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoClassiSuperAree.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoClassiSuperAree.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoClassiSuperAree.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoClassiSuperAree.SetRateo(rateo)
    Call frmConfigurazioneProdottoClassiSuperAree.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoClassiSuperAree.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoClassiSuperAree.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoClassiSuperAree.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoClassiSuperAree.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoClassiSuperAree.Init
End Sub

Private Sub CaricaFormConfigurazioneDateOperazioni()
    Call frmConfigurazioneProdottoDateOperazioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoDateOperazioni.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoDateOperazioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoDateOperazioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoDateOperazioni.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoDateOperazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoDateOperazioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoDateOperazioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoDateOperazioni.Init
End Sub

Private Sub CaricaFormPostiMigliori()
    Call frmConfigurazioneProdottoPostiMigliori.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoPostiMigliori.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoPostiMigliori.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoPostiMigliori.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoPostiMigliori.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoPostiMigliori.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoPostiMigliori.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoPostiMigliori.Init
End Sub

Private Sub CaricaFormConfigurazioneStampeAggiuntive()
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetRateo(rateo)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoStampeAggiuntive.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoStampeAggiuntive.Init
End Sub

Private Sub CaricaFormConfigurazioneCausaliProtezione()
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoCausaliProtezione.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoCausaliProtezione.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoCausaliProtezione.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoCausaliProtezione.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoCausaliProtezione.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoCausaliProtezione.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoCausaliProtezione.Init
End Sub

Private Sub CaricaFormConfigurazioneRappresentazioni()
    Call frmConfigurazioneProdottoRappresentazioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoRappresentazioni.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoRappresentazioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoRappresentazioni.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoRappresentazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoRappresentazioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoRappresentazioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoRappresentazioni.SetIsProdottoTMaster(isProdottoTMaster)
    Call frmConfigurazioneProdottoRappresentazioni.SetIsProdottoTDL(IsProdottoTDL)
    Call frmConfigurazioneProdottoRappresentazioni.Init
End Sub

Private Sub CaricaFormConfigurazioneSceltaRappresentazioni()
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetRateo(rateo)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoScelteRappresentazione.Init
End Sub

Private Sub CaricaFormModalitaFornitura()
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoModalitaFornitura.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoModalitaFornitura.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoModalitaFornitura.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoModalitaFornitura.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoModalitaFornitura.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoModalitaFornitura.Init
End Sub

Private Sub CaricaFormConfigurazioneTariffe()
    Call frmConfigurazioneProdottoTariffe.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoTariffe.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoTariffe.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoTariffe.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoTariffe.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoTariffe.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoTariffe.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoTariffe.SetIsProdottoTMaster(isProdottoTMaster)
    Call frmConfigurazioneProdottoTariffe.SetIsProdottoTDL(IsProdottoTDL)
    Call frmConfigurazioneProdottoTariffe.Init
End Sub

Private Sub CaricaFormLayoutEMailAcquistiInternet()
'    Call frmConfigurazioneProdottoTemplate.SetIdProdottoSelezionato(idProdottoSelezionato)
'    Call frmConfigurazioneProdottoTemplate.SetNomeProdottoSelezionato(nomeProdotto)
'    Call frmConfigurazioneProdottoTemplate.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoTemplate.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoTemplate.SetIdPiantaSelezionata(idPiantaSelezionata)
'    Call frmConfigurazioneProdottoTemplate.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazioneProdottoTemplate.SetModalit�Form(modalitaFormCorrente)
'    Call frmConfigurazioneProdottoTemplate.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
'    Call frmConfigurazioneProdottoTemplate.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoTemplate.Init(idPiantaSelezionata, idProdottoSelezionato, nomeProdotto, idOrganizzazioneSelezionata, nomeOrganizzazioneSelezionata)
End Sub

Private Sub CaricaFormTipoStampante()
    Call frmConfigurazioneProdottoTipoStampante.Init(idPiantaSelezionata, idProdottoSelezionato, nomeProdotto, idOrganizzazioneSelezionata, nomeOrganizzazioneSelezionata)
End Sub

Private Sub CaricaFormGestioneAnagrafiche()
    Call frmConfigurazioneProdottoGestioneAnagrafiche.Init(idPiantaSelezionata, idProdottoSelezionato, nomeProdotto, idOrganizzazioneSelezionata, nomeOrganizzazioneSelezionata, idLivelloGestioneAnagrafiche)
End Sub

Private Sub CaricaFormGestioneSuperareeTipiSupporto()
    Call frmConfigurazioneProdottoSuperareeTipiMedium.Init(idPiantaSelezionata, idProdottoSelezionato, nomeProdotto, idOrganizzazioneSelezionata, nomeOrganizzazioneSelezionata)
End Sub
Private Sub CaricaFormConfigurazionePrezzi()
    Call frmConfigurazioneProdottoPrezzi.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoPrezzi.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoPrezzi.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoPrezzi.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoPrezzi.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoPrezzi.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoPrezzi.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoPrezzi.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoPrezzi.SetIsProdottoTMaster(isProdottoTMaster)
    Call frmConfigurazioneProdottoPrezzi.SetIsProdottoTDL(IsProdottoTDL)
    Call frmConfigurazioneProdottoPrezzi.Init
End Sub

Private Sub CaricaFormImpostazioneProtezioni()
    Call frmConfigurazioneProdottoProtezioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoProtezioni.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoProtezioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoProtezioni.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoProtezioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoProtezioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoProtezioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoProtezioni.Init
End Sub

Private Sub CaricaFormConfigurazioneProdottoAssociazioneTipiLayoutSupporto()
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.Init
End Sub

Private Sub CaricaFormConfigurazioneProdottoAssociazioneLayoutRicevute()
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.Init
End Sub

Private Sub CaricaFormConfigurazionePeriodiCommerciali()
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIsProdottoMaster(isProdottoTMaster)
    Call frmConfigurazioneProdottoPeriodiCommerciali.SetIsProdottoTDL(IsProdottoTDL)
    Call frmConfigurazioneProdottoPeriodiCommerciali.Init
End Sub

Private Sub CaricaFormChiaviProdotto()
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoChiaveProdotto.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoChiaveProdotto.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoChiaveProdotto.Init
End Sub

Private Sub CaricaFormConfigurazioneProdottoSistemiDiEmissione()
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoSistemiDiEmissione.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoSistemiDiEmissione.Init
End Sub

Private Sub CaricaFormConfiguraCambioUtilizzatoreSuSuperaree()
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree.Init
End Sub

Private Sub CaricaFormConfigurazioneCapienzeTurnoLibero()
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoCapienzeTurnoLibero.Init
End Sub

Private Sub CaricaFormAbilitazionePuntiVendita()
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNomeProdottoSelezionato(nomeProdotto)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetModalit�Form(modalitaFormCorrente)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.Init
End Sub

Private Sub cmdTariffe_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTariffe
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraTariffe()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneTariffe
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdPrezzi_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraPrezzi
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraPrezzi()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazionePrezzi
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdAssociazioneSupporti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraAssociazioneSupporti
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraAssociazioneSupporti()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneProdottoAssociazioneTipiLayoutSupporto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdAssociazioneLayoutRicevute_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraAssociazioneLayoutRicevute
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraAssociazioneLayoutRicevute()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneProdottoAssociazioneLayoutRicevute
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdPeriodiCommerciali_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraPeriodiCommerciali
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraPeriodiCommerciali()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazionePeriodiCommerciali
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub CercaPiantePerOrganizzazione()
    Dim sql As String
    
    Call cmbPiante.Clear
    If idOrganizzazioneSelezionata <> idNessunElementoSelezionato And idClasseProdottoSelezionata <> CPR_NON_SPECIFICATA Then
        sql = "SELECT PIANTA.IDPIANTA AS ""ID""," & _
            " PIANTA.NOME AS ""NOME"", VENUE.NOME AS ""VENUE""" & _
            " FROM PIANTA, VENUE_PIANTA, VENUE, ORGANIZZAZIONE_PIANTA" & _
            " WHERE PIANTA.IDPIANTA = ORGANIZZAZIONE_PIANTA.IDPIANTA" & _
            " AND PIANTA.IDPIANTA = VENUE_PIANTA.IDPIANTA" & _
            " AND VENUE.IDVENUE = VENUE_PIANTA.IDVENUE" & _
            " AND ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
        If idClasseProdottoSelezionata = CPR_GIORNALIERO Or idClasseProdottoSelezionata = CPR_TESSERA Or idClasseProdottoSelezionata = CPR_OPEN Then
            sql = sql & " AND CAPIENZAILLIMITATA = " & VB_VERO
        Else
            sql = sql & " AND CAPIENZAILLIMITATA = " & VB_FALSO
        End If
        sql = sql & " ORDER BY VENUE, NOME"
        Call CaricaListaPianteComplete(cmbPiante, sql)
        Call SelezionaElementoSuCombo(cmbPiante, idNessunElementoSelezionato)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdSistemaDiEmissione_Click()
    Dim mousePointerOld As Integer

    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SistemiDiEmissione
    
    MousePointer = mousePointerOld
End Sub

Private Sub SistemiDiEmissione()
    Call CaricaFormConfigurazioneProdottoSistemiDiEmissione
End Sub

'Private Function RilevaValoriCampi() As Boolean
'    Dim numero As Long
'    Dim listaNonConformit� As Collection
'
'    RilevaValoriCampi = True
'    Set listaNonConformit� = New Collection
'    nomeProdotto = Trim(txtNome.Text)
'    If ViolataUnicit�("PRODOTTO", "NOME = " & SqlStringValue(nomeProdotto), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
'        "IDSTAGIONE = " & idStagioneSelezionata, "IDPRODOTTO <> " & idProdottoSelezionato, "") Then
'        RilevaValoriCampi = False
'        Call listaNonConformit�.Add("- il valore NOME = " & SqlStringValue(nomeProdotto) & _
'            " � gi� presente in DB per la stessa Stagione e la stessa Organizzazione;")
'    End If
'    codiceTL = Trim(txtCodiceTL.Text)
'    If ViolataUnicit�("PRODOTTO", "CODICETERMINALELOTTO = " & SqlStringValue(codiceTL), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
'        "IDPRODOTTO <> " & idProdottoSelezionato, "", "") Then
'        RilevaValoriCampi = False
'        Call listaNonConformit�.Add("- il valore CODICETERMINALELOTTO = " & SqlStringValue(codiceTL) & _
'            " � gi� presente in DB per la stessa Organizzazione;")
'    End If
'    If IsCampoNumericoCorretto(txtRateo) Then
'        rateo = CLng(Trim(txtRateo.Text))
'        If modalitaFormCorrente = A_MODIFICA Or modalitaFormCorrente = A_CLONA Then
'            numero = NumeroRappresentazioniAssociate
'            If rateo <> numero Then
'                Call frmMessaggio.Visualizza("ConfermaRateoNonCorretto", numero)
'                If frmMessaggio.exitCode = EC_ANNULLA Then
'                    RilevaValoriCampi = False
'                    Exit Function
'                End If
'            End If
'        End If
'    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Rateo")
'        RilevaValoriCampi = False
'        Exit Function
'    End If
'    If txtAliquotaIVA.Text <> "" Then
'        If IsCampoNumericoCorretto(txtAliquotaIVA) Then
'            aliquotaIVA = CLng(CDbl(txtAliquotaIVA.Text) * 100)
'            If aliquotaIVA >= 0 And aliquotaIVA <= 10000 Then
''                If aliquotaIVA <> 2000 Then
'                    Call frmMessaggio.Visualizza("ConfermaValoreAliquotaIVAImpostato")
'                    If frmMessaggio.exitCode = EC_ANNULLA Then
''                        aliquotaIVA = 2000
'                        aliquotaIVA = idNessunElementoSelezionato
'                    End If
''                Else
''                    aliquotaIVA = 2000
''                End If
'            Else
'                Call frmMessaggio.Visualizza("ErroreFormatoPercentuale", "Aliquota IVA")
'                RilevaValoriCampi = False
'                Exit Function
'            End If
'        Else
'            Call frmMessaggio.Visualizza("ErroreFormatoDatiDouble", "Aliquota IVA")
'            RilevaValoriCampi = False
'            Exit Function
'        End If
'    Else
'        aliquotaIVA = idNessunElementoSelezionato
'    End If
'    descrizioneProdotto = Trim(txtDescrizione.Text)
'    descrizioneAlternativaProdotto = Trim(txtDescrizioneAlternativa.Text)
'    descrizioneStampaVenueProdotto = Trim(txtDescrizioneStampaVenue.Text)
'    sigilloFiscale = VB_VERO
'    progressivoVendite = VB_VERO
'    progressivoTipografico = VB_VERO
'    propagazioneProtezioni = chkPropagazioneProtezioni.Value
'    dataInizioConsegna = IIf(IsNull(dtpDataInizioConsegna.Value), dataNulla, FormatDateTime(dtpDataInizioConsegna, vbShortDate))
'    dataFineConsegna = IIf(IsNull(dtpDataFineConsegna.Value), dataNulla, FormatDateTime(dtpDataFineConsegna, vbShortDate))
'    If dataInizioConsegna > dataFineConsegna Then
'        Call frmMessaggio.Visualizza("ErroreSuccessioneDate", "data Inizio Consegna", "data Fine Consegna")
'        RilevaValoriCampi = False
'    End If
'
'End Function
'
Private Function NumeroRappresentazioniAssociate() As Long
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim cont As Long
    
    Call ApriConnessioneBD

    If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
        sql = "SELECT DISTINCT COUNT(IDSCELTARAPPRESENTAZIONE) NUMERO" & _
            " FROM SCELTARAPPRESENTAZIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
    Else
        sql = "SELECT DISTINCT COUNT(IDRAPPRESENTAZIONE) NUMERO" & _
            " FROM PRODOTTO_RAPPRESENTAZIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    cont = rec("NUMERO")
    rec.Close
    
    Call ChiudiConnessioneBD
    
    NumeroRappresentazioniAssociate = cont
End Function

Private Sub SelezionaCodiceTLOrganizzazione()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = "SELECT CODICETERMINALELOTTO AS COD FROM ORGANIZZAZIONE" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        codiceTLOrganizzazioneSelezionata = rec("COD")
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
End Sub

Private Sub SelezionaCodiceTLTipoProdotto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = "SELECT CODICETERMINALELOTTO AS COD FROM TIPOPRODOTTO" & _
        " WHERE IDTIPOPRODOTTO = " & idTipoProdottoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        codiceTLTipoProdottoSelezionato = rec("COD")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
End Sub

Private Sub CaricaListaPianteComplete(cmb As ComboBox, strSQL As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim id As Long
    Dim venue As String
    Dim pianta As String
    Dim stringaVisualizzata As String
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            id = rec("ID").Value
            pianta = rec("NOME")
            venue = rec("VENUE")
            stringaVisualizzata = venue & " - " & pianta
            If modalitaFormCorrente = A_NUOVO Then
                If IsPiantaCompleta(id, False, True) Then
                    cmb.AddItem stringaVisualizzata
                    cmb.ItemData(i - 1) = rec("ID").Value
                    i = i + 1
                End If
            Else
                cmb.AddItem stringaVisualizzata
                cmb.ItemData(i - 1) = rec("ID").Value
                i = i + 1
            End If
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Function GetTipoTerminaleDaLista(idTT As Long) As clsTipoTerm
    Dim i As Integer
    Dim tt As clsTipoTerm
    
    Set tt = New clsTipoTerm
    For i = 1 To listaTipiTerminale.count
        If idTT = listaTipiTerminale(i).idTipoTerminale Then
            Set tt = listaTipiTerminale(i)
        End If
    Next i
    Set GetTipoTerminaleDaLista = tt
End Function

Private Function StampaAbilitataDaTL() As Boolean
    Dim tt As clsTipoTerm
    Dim tos As clsTipoOperaz
    Dim trovato As Boolean
    
    Set tt = listaTipiTerminale(TT_TERMINALE_LOTTO)
    For Each tos In tt.collTipiOperazioneSelezionate
        If tos.idTipoOperazione = TO_VENDITA Then
            trovato = True
            Exit For
        Else
            trovato = False
        End If
    Next tos
    StampaAbilitataDaTL = trovato
End Function

Private Sub cmdChiaviProdotto_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraChiaviProdotto
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraChiaviProdotto()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormChiaviProdotto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdCambioUtilizzatoreSuSuperaree_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraCambioUtilizzatoreSuSuperaree
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraCambioUtilizzatoreSuSuperaree()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfiguraCambioUtilizzatoreSuSuperaree
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub txtRateo_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdCapienze_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraCapienzeTurnoLibero
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraCapienzeTurnoLibero()
    If isProdottoBloccatoDaUtente Then
        Call CaricaFormConfigurazioneCapienzeTurnoLibero
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Function valoriCampiOK() As Boolean
    Dim numero As Long
    Dim listaNonConformit� As Collection
    Dim listaValoriDaConfermare As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    Set listaNonConformit� = New Collection
    Set listaValoriDaConfermare = New Collection
    condizioneSql = ""
    If modalitaFormCorrente = A_MODIFICA Or modalitaFormCorrente = A_ELIMINA Then
        condizioneSql = "IDPRODOTTO <> " & idProdottoSelezionato
    End If
    
    nomeProdotto = Trim(txtNome.Text)
'    If ViolataUnicit�("PRODOTTO", "NOME = " & SqlStringValue(nomeProdotto), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
'        condizioneSql, "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomeProdotto) & _
'            " � gi� presente in DB per la stessa Stagione e la stessa Organizzazione;")
'    End If
    codiceTL = Trim(txtCodiceTL.Text)
    If ViolataUnicit�("PRODOTTO", "CODICETERMINALELOTTO = " & SqlStringValue(codiceTL), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformit�.Add("- il valore codice TL = " & SqlStringValue(codiceTL) & _
            " � gi� presente in DB per la stessa Organizzazione;")
    End If
    If IsCampoNumericoCorretto(txtRateo) Then
        rateo = CLng(Trim(txtRateo.Text))
        If modalitaFormCorrente = A_MODIFICA Or modalitaFormCorrente = A_CLONA Then
            numero = NumeroRappresentazioniAssociate
            If rateo <> numero Then
                Call listaValoriDaConfermare.Add("- il valore Rateo non � uguale al numero di rappresentazioni/scelte rappr. associate (" & numero & ");")
            End If
        End If
    Else
        valoriCampiOK = False
        Call listaNonConformit�.Add("- il valore Rateo non � intero;")
    End If
    If txtAliquotaIVA.Text <> "" Then
        If IsCampoNumericoCorretto(txtAliquotaIVA) Then
            aliquotaIVA = CLng(CDbl(txtAliquotaIVA.Text) * 100)
            If aliquotaIVA >= 0 And aliquotaIVA <= 10000 Then
                Call listaValoriDaConfermare.Add("- Il campo Aliquota IVA va valorizzato solo nei casi in cui non sar� utilizzata la procedura standard per il calcolo dell'IVA;")
            Else
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore Aliquota IVA non � una percentuale (tipo double compreso tra 0 e 100);")
            End If
        Else
            valoriCampiOK = False
            Call listaNonConformit�.Add("- il valore Aliquota IVA non � di tipo double;")
        End If
    Else
        aliquotaIVA = idNessunElementoSelezionato
    End If
    
    If isProdottoBOOpen = VB_VERO Or idClasseProdottoSelezionata = CPR_OPEN Then
        If IsCampoNumericoCorretto(txtQuantit�AccessiConsentiti) Then
            If IsCampoInteroCorretto(txtQuantit�AccessiConsentiti) Then
                quantit�AccessiConsentiti = CLng(Trim(txtQuantit�AccessiConsentiti.Text))
                If quantit�AccessiConsentiti < 1 Then
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- Il campo Q.t� accessi deve essere maggiore di zero;")
                End If
            Else
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore Q.t� accessi non � di tipo numerico intero;")
            End If
        Else
            valoriCampiOK = False
            Call listaNonConformit�.Add("- il valore Q.t� accessi non � di tipo numerico intero;")
        End If
    End If
    
'    If isProdottoSpedibile = VB_VERO Then
'        If IsCampoNumericoCorretto(txtNumGiorniNecessariPerSpedizione) Then
'            If IsCampoInteroCorretto(txtNumGiorniNecessariPerSpedizione) Then
'                numGiorniNecessariPerSpediz = CInt(Trim(txtNumGiorniNecessariPerSpedizione.Text))
'                If numGiorniNecessariPerSpediz < 1 Then
'                    ValoriCampiOK = False
'                    Call listaNonConformit�.Add("- Il campo numero giorni necessari per spedizione deve essere maggiore di zero;")
'                End If
'            Else
'                ValoriCampiOK = False
'                Call listaNonConformit�.Add("- il valore numero giorni necessari per spedizione non � di tipo numerico intero;")
'            End If
'        Else
'            ValoriCampiOK = False
'            Call listaNonConformit�.Add("- il valore numero giorni necessari per spedizione non � di tipo numerico intero;")
'        End If
'    End If
'
    If rientraInDecretoSicurezza = VB_VERO Then
        If IsCampoNumericoCorretto(txtNumeroMaxTitoliPerAcquirente) Then
            If IsCampoInteroCorretto(txtNumeroMaxTitoliPerAcquirente) Then
                numeroMaxTitoliPerAcquirente = CInt(Trim(txtNumeroMaxTitoliPerAcquirente.Text))
                If numeroMaxTitoliPerAcquirente > NUMERO_MAX_TITOLI_PER_ACQUIRENTE Or numeroMaxTitoliPerAcquirente < 1 Then
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- Il campo Numero max titoli per acquirente deve essere compreso tra 1 e il massimo consentito per legge (estremi inclusi);")
                End If
            Else
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore Numero max titoli per acquirente non � di tipo numerico intero;")
            End If
        Else
            valoriCampiOK = False
            Call listaNonConformit�.Add("- il valore Numero max titoli per acquirente non � di tipo numerico intero;")
        End If
    Else
        If txtNumeroMaxTitoliPerAcquirente.Text = "" Then
            numeroMaxTitoliPerAcquirente = valoreLongNullo
        Else
            If IsCampoNumericoCorretto(txtNumeroMaxTitoliPerAcquirente) Then
                If IsCampoInteroCorretto(txtNumeroMaxTitoliPerAcquirente) Then
                    numeroMaxTitoliPerAcquirente = CInt(Trim(txtNumeroMaxTitoliPerAcquirente.Text))
                    If numeroMaxTitoliPerAcquirente < 1 Then
                        valoriCampiOK = False
                        Call listaNonConformit�.Add("- Il campo Numero max titoli per acquirente deve essere maggiore di 1;")
                    End If
                Else
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- il valore Numero max titoli per acquirente non � di tipo numerico intero;")
                End If
            Else
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore Numero max titoli per acquirente non � di tipo numerico intero;")
            End If
        End If
    End If
    
    minutiAnnullamentoTitTradiz = quantitaNulla
    minutiAnnullamentoFineEvento = quantitaNulla
    If isProdottoFiscale = VB_FALSO Then
        If txtMinutiAnnullamentoTitTradiz <> "" Then
            If Not IsCampoInteroCorretto(txtMinutiAnnullamentoTitTradiz) Then
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore minuti annullamento titoli tradizionali non � di tipo numerico intero;")
            Else
                If CLng(txtMinutiAnnullamentoTitTradiz) < 0 Then
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- il valore minuti annullamento titoli tradizionali non � maggiore o uguale a zero;")
                Else
                    minutiAnnullamentoTitTradiz = CLng(txtMinutiAnnullamentoTitTradiz.Text)
                End If
            End If
        End If
        
        If txtMinutiAnnullamentoDaFineEvento <> "" Then
            If Not IsCampoInteroCorretto(txtMinutiAnnullamentoDaFineEvento) Then
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore minuti annullamento da fine evento non � di tipo numerico intero;")
            Else
                If CLng(txtMinutiAnnullamentoDaFineEvento) < 0 Then
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- il valore minuti annullamento da fine evento non � maggiore o uguale a zero;")
                Else
                    minutiAnnullamentoFineEvento = CLng(txtMinutiAnnullamentoDaFineEvento.Text)
                End If
            End If
        End If
    End If
    
    numeroMassimoCessioniRateo = quantitaNulla
    If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO Then
        If txtNumeroMassimoCessioniRateo <> "" Then
            If Not IsCampoInteroCorretto(txtNumeroMassimoCessioniRateo) Then
                valoriCampiOK = False
                Call listaNonConformit�.Add("- il valore numero massimo di cessioni ratei non � di tipo numerico intero;")
            Else
                If CLng(txtNumeroMassimoCessioniRateo) < 0 Then
                    valoriCampiOK = False
                    Call listaNonConformit�.Add("- il valore numero massimo di cessioni rateo non � maggiore o uguale a zero;")
                Else
                    numeroMassimoCessioniRateo = CLng(txtNumeroMassimoCessioniRateo.Text)
                End If
            End If
        End If
    End If
    
    descrizioneProdotto = Trim(txtDescrizione.Text)
    descrizionePOSProdotto = Trim(txtDescrizionePOS.Text)
    descrizioneAlternativaProdotto = Trim(txtDescrizioneAlternativa.Text)
    descrizioneStampaVenueProdotto = Trim(txtDescrizioneStampaVenue.Text)
'    numeroMaxTitoliPerAcquirente = CInt(Trim(txtNumeroMaxTitoliPerAcquirente.Text))
    sigilloFiscale = VB_VERO
    progressivoVendite = VB_VERO
    progressivoTipografico = VB_VERO
    propagazioneProtezioni = chkPropagazioneProtezioni.Value
'    rientraInDecretoSicurezza = chkRientraInDecretoSicurezza.Value
    If idClasseProdottoSelezionata = CPR_BIGLIETTERIA_ORDINARIA Then
        isProdottoBOOpen = chkProdottoOpen.Value
    Else
        isProdottoBOOpen = VB_FALSO
    End If
'    isProdottoSpedibile = chkProdottoSpedibile.Value
'    titoliDigitaliAbilitati = chkTitoliDigitaliAbilitati.Value
'    ritiroTitoliPermesso = chkRitiroTitoliPermesso.Value
    gestEccedenzeOmaggioEvolutaProdotto = chkGestioneTariffeOmaggio.Value
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    Else
        If listaValoriDaConfermare.count > 0 Then
            Call frmMessaggio.Visualizza("ConfermaValori", ArgomentoMessaggio(listaValoriDaConfermare))
            If frmMessaggio.exitCode = EC_ANNULLA Then
                valoriCampiOK = False
            End If
        End If
    End If

End Function

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""

    Call SetGestioneExitCode(EC_CONFERMA)
    If isRecordEditabile Then
        If modalitaFormCorrente = A_ELIMINA Then
            Call EliminaDallaBaseDati
            Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PRODOTTO, CCDA_PRODOTTO, "IDPRODOTTO = " & idProdottoSelezionato, idProdottoSelezionato)
            Call Annulla
        Else
            If valoriCampiOK Then
                If modalitaFormCorrente = A_NUOVO Then
                    Call InserisciNellaBaseDati
                    Call AggiornaRegistroUltimiProdotti
                    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idProdottoSelezionato, isProdottoBloccatoDaUtente)
                    Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PRODOTTO, "IDPRODOTTO = " & idProdottoSelezionato, idProdottoSelezionato)
                    prodottoSalvatoInBD = True
                ElseIf modalitaFormCorrente = A_MODIFICA Then
                    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
                        isConfigurabile = True
                        If tipoStatoProdotto = TSP_ATTIVO Then
                            Call frmMessaggio.Visualizza("ConfermaEditabilit�ProdottoAttivo")
                            If frmMessaggio.exitCode <> EC_CONFERMA Then
                                isConfigurabile = False
                            End If
                        End If
                        If isConfigurabile Then
                            Call AggiornaNellaBaseDati
                            Call AggiornaRegistroUltimiProdotti
                            Call frmMessaggio.Visualizza("NotificaModificaDati")
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_PRODOTTO, "IDPRODOTTO = " & idProdottoSelezionato, idProdottoSelezionato)
                            Call Annulla
                        End If
                    Else
                        Call frmMessaggio.Visualizza("NotificaNonEditabilit�Campi", causaNonEditabilita)
                    End If
                End If
            End If
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub
'
'Private Sub ConfermaDateOperazioni()
'    Dim sql As String
'    Dim i As Integer
'    Dim j As Integer
'    Dim tt As clsTipoTerm
'    Dim n As Long
'    Dim dataOraInizio As Date
'    Dim dataOraFine As Date
'
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
'
'    ' Questo codice � replicato nella aggiornaNellaBaseDati
'    sql = "DELETE FROM PRODOTTO_TIPOTERMIN_TIPOOPERAZ WHERE IDPRODOTTO = " & idProdottoSelezionato
'    SETAConnection.Execute sql, n, adCmdText
'
'    For i = 1 To listaTipiTerminale.count
'        Set tt = listaTipiTerminale(i)
'        For j = 1 To tt.collTipiOperazioneSelezionate.count
'            dataOraInizio = tt.collTipiOperazioneSelezionate(j).dataOraInizio
'            dataOraFine = tt.collTipiOperazioneSelezionate(j).dataOraFine
'            sql = "INSERT INTO PRODOTTO_TIPOTERMIN_TIPOOPERAZ (IDPRODOTTO, IDTIPOTERMINALE,"
'            sql = sql & " IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
'            sql = sql & " VALUES (" & idProdottoSelezionato & ", "
'            sql = sql & tt.idTipoTerminale & ", "
'            sql = sql & tt.collTipiOperazioneSelezionate(j).idTipoOperazione & ", "
'            sql = sql & IIf(dataOraInizio = dataNulla, "NULL", SqlDateTimeValue(dataOraInizio)) & ", "
'            sql = sql & IIf(dataOraFine = dataNulla, "NULL", SqlDateTimeValue(dataOraFine)) & ")"
'            SETAConnection.Execute sql, n, adCmdText
'        Next j
'    Next i
'    ' replicato fino a qui!
'
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
'
'End Sub

Private Sub AggiornaRegistroUltimiProdotti()
    Dim idUltimoProdotto As Long
    
    idUltimoProdotto = LeggiIdUltimoProdottoDaRegistro(GetSetting(App.EXEName, "UltimiProdotti", "Id1", ""))
    
    If idUltimoProdotto <> idProdottoSelezionato Then
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione10", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione9", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id10", GetSetting(App.EXEName, "UltimiProdotti", "Id9", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione9", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione8", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id9", GetSetting(App.EXEName, "UltimiProdotti", "Id8", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione8", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione7", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id8", GetSetting(App.EXEName, "UltimiProdotti", "Id7", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione7", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione6", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id7", GetSetting(App.EXEName, "UltimiProdotti", "Id6", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione6", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione5", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id6", GetSetting(App.EXEName, "UltimiProdotti", "Id5", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione5", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione4", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id5", GetSetting(App.EXEName, "UltimiProdotti", "Id4", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione4", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione3", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id4", GetSetting(App.EXEName, "UltimiProdotti", "Id3", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione3", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione2", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id3", GetSetting(App.EXEName, "UltimiProdotti", "Id2", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione2", GetSetting(App.EXEName, "UltimiProdotti", "Descrizione1", idProdottoSelezionato))
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id2", GetSetting(App.EXEName, "UltimiProdotti", "Id1", idProdottoSelezionato))
        
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Descrizione1", idProdottoSelezionato & " - " & nomeOrganizzazioneSelezionata & " - " & nomeProdotto)
        Call SaveSetting(App.EXEName, "UltimiProdotti", "Id1", idProdottoSelezionato)
    End If
    
End Sub

