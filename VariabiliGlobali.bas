Attribute VB_Name = "VariabiliGlobali"
Option Explicit

Global userID As String
Global passWord As String
Global dataSource As String
Global server As String
Global nomeSchema As String
'Global elencoAreeAssociateAPianta() As InfoArea
Global listaAreeAssociateAPianta As Collection
Global isProdottoBloccatoDaUtente As Boolean
Global isOffertaPacchettoBloccataDaUtente As Boolean
Global isPiantaBloccataDaUtente As Boolean
Global isOrganizzazioneBloccataDaUtente As Boolean
Global isAreaBloccataDaUtente As Boolean

Global colori(1 To NUMERO_COLORI, 2) As String


Public pathApplicazione As String
Public versioneRilascio As String
Public tipoStatoProdotto As TipoStatoProdottoEnum
Public tipoStatoOrganizzazione As TipoStatoOrganizzazioneEnum
Public tipoModalitąConfigurazione As TipoModalitąConfigurazioneEnum
Public tipoSessioneConfigurazione As TipoSessioneConfigurazioneEnum
Public idSessioneConfigurazioneCorrente As Long

Public Type InfoArea
    idArea As Long
    nomeArea As String
    descrizioneArea As String
End Type

Public Type InfoPosto
    idPosto As Long
    nomeFila As String
    nomePosto As String
    xPosto As Integer
    yPosto As Integer
    indiceDiPreferibilita As Long
    idArea As Long
    nomeArea As String
End Type

Public Type InfoRecord
    id As Long
    nome As String
    descrizione As String
End Type

Public Type InfoOperatoreVenditore
    id As Long
    userName As String
    passWord As String
End Type

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Sub Const_Init()
    Dim i As Long
    
    colori(GIALLO_1, 1) = COLORE_GIALLO_1
    colori(GIALLO_2, 1) = COLORE_GIALLO_2
    colori(GIALLO_3, 1) = COLORE_GIALLO_3
    colori(GIALLO_4, 1) = COLORE_GIALLO_4
    colori(ARANCIO_1, 1) = COLORE_ARANCIO_1
    colori(ROSA_1, 1) = COLORE_ROSA_1
    colori(ROSA_2, 1) = COLORE_ROSA_2
    colori(ROSA_3, 1) = COLORE_ROSA_3
    colori(LEGNO_1, 1) = COLORE_LEGNO_1
    colori(LEGNO_2, 1) = COLORE_LEGNO_2
    colori(LEGNO_3, 1) = COLORE_LEGNO_3
    colori(ACQUA_1, 1) = COLORE_ACQUA_1
    colori(ACQUA_2, 1) = COLORE_ACQUA_2
    colori(ACQUA_3, 1) = COLORE_ACQUA_3
    colori(BLU_1, 1) = COLORE_BLU_1
    colori(BLU_2, 1) = COLORE_BLU_2
    colori(BLU_3, 1) = COLORE_BLU_3
    colori(VIOLA_1, 1) = COLORE_VIOLA_1
    colori(VIOLA_2, 1) = COLORE_VIOLA_2
    colori(VIOLA_3, 1) = COLORE_VIOLA_3
    colori(FUXIA_1, 1) = COLORE_FUXIA_1
    colori(VERDE_1, 1) = COLORE_VERDE_1
    colori(VERDE_2, 1) = COLORE_VERDE_2
    colori(VERDE_3, 1) = COLORE_VERDE_3

    colori(GIALLO_1, 2) = "FFFF33"
    colori(GIALLO_2, 2) = "EEEE21"
    colori(GIALLO_3, 2) = "FFCC00"
    colori(GIALLO_4, 2) = "FF9900"
    colori(ARANCIO_1, 2) = "FF6633"
    colori(ROSA_1, 2) = "FFCCCC"
    colori(ROSA_2, 2) = "FF9D9D"
    colori(ROSA_3, 2) = "FF6464"
    colori(LEGNO_1, 2) = "FFCD92"
    colori(LEGNO_2, 2) = "FFA751"
    colori(LEGNO_3, 2) = "E18400"
    colori(ACQUA_1, 2) = "AEFAFF"
    colori(ACQUA_2, 2) = "3EF4FF"
    colori(ACQUA_3, 2) = "00D9E7"
    colori(BLU_1, 2) = "AEC9FF"
    colori(BLU_2, 2) = "74A2FF"
    colori(BLU_3, 2) = "3D7DFF"
    colori(VIOLA_1, 2) = "DABEFF"
    colori(VIOLA_2, 2) = "BE8FFF"
    colori(VIOLA_3, 2) = "A563FF"
    colori(FUXIA_1, 2) = "FF33FF"
    colori(VERDE_1, 2) = "90FFD3"
    colori(VERDE_2, 2) = "2FE89E"
    colori(VERDE_3, 2) = "00C777"

    pathApplicazione = App.Path
    If Right(pathApplicazione, 1) <> "\" Then pathApplicazione = pathApplicazione & "\"
    
    versioneRilascio = "0.2.0"

End Sub
