VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneImpiantoMappa 
   Caption         =   "Impianto - mappa superaree"
   ClientHeight    =   12615
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14010
   LinkTopic       =   "Form1"
   ScaleHeight     =   12615
   ScaleWidth      =   14010
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstSuperaree 
      Height          =   2205
      Left            =   120
      TabIndex        =   27
      Top             =   9240
      Width           =   3975
   End
   Begin VB.CommandButton cmdAssociaColore 
      Caption         =   "Associa colore"
      Height          =   375
      Left            =   4200
      TabIndex        =   26
      Top             =   12120
      Width           =   2415
   End
   Begin VB.Frame frmSuperaree 
      Caption         =   "Superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6375
      Left            =   4200
      TabIndex        =   24
      Top             =   5640
      Width           =   9735
      Begin VB.CheckBox chkSuperArea 
         BackColor       =   &H00FFFFFF&
         Caption         =   "chkSuperArea"
         Height          =   280
         Index           =   0
         Left            =   120
         TabIndex        =   25
         Top             =   360
         Visible         =   0   'False
         Width           =   3200
      End
   End
   Begin VB.Frame frmSceltaPianta 
      Caption         =   "Scelta della pianta dalla quale copiare le superaree"
      Height          =   2415
      Left            =   120
      TabIndex        =   17
      Top             =   6360
      Width           =   3975
      Begin VB.CommandButton cmdSceltaPianta 
         Caption         =   "Importa"
         Height          =   435
         Left            =   2640
         TabIndex        =   22
         Top             =   1800
         Width           =   1155
      End
      Begin VB.ComboBox cmbPianta 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1320
         Width           =   3660
      End
      Begin VB.ComboBox cmbVenue 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   660
         Width           =   3675
      End
      Begin VB.Label lblAvanzamento 
         Height          =   375
         Left            =   120
         TabIndex        =   23
         Top             =   1920
         Width           =   3735
      End
      Begin VB.Label lblPianta 
         Caption         =   "Pianta"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   1110
         Width           =   2025
      End
      Begin VB.Label lblVenue 
         Caption         =   "Venue"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   1470
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   14
      Top             =   11640
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   16
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   12720
      TabIndex        =   7
      Top             =   12120
      Width           =   1155
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   6
      Top             =   5880
      Width           =   3795
   End
   Begin VB.Frame fraAzioniSuGrigliaDati 
      Height          =   915
      Left            =   120
      TabIndex        =   1
      Top             =   4620
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10140
      TabIndex        =   0
      Top             =   240
      Width           =   3735
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneImpiantoMappa 
      Height          =   330
      Left            =   6240
      Top             =   120
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneImpiantoMappa 
      Height          =   3675
      Left            =   120
      TabIndex        =   8
      Top             =   600
      Width           =   13755
      _ExtentX        =   24262
      _ExtentY        =   6482
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblListaSuperaree 
      Caption         =   "Superaree"
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   9000
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Mappe superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   4380
      Width           =   1815
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   5640
      Width           =   1695
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   10
      Top             =   4380
      Width           =   2775
   End
   Begin VB.Label lblInfo1 
      Caption         =   "Impianto"
      Height          =   255
      Left            =   10140
      TabIndex        =   9
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneImpiantoMappa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idImpiantoSelezionato As Long
Private nomeRecordSelezionato As String
Private nomeImpiantoSelezionato As String
Private isAttributiMappaModificati As Boolean
Private idVenueSelezionato As Long
Private idPiantaSelezionata As Long
Private qtaSuperaree As Long


Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Impianto"
    txtInfo1.Text = nomeImpiantoSelezionato
    txtInfo1.Enabled = False
    dgrConfigurazioneImpiantoMappa.Caption = "MAPPE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneImpiantoMappa.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtNome.Enabled = False
        lblNome.Enabled = False
        
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdSceltaPianta.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneImpiantoMappa.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdSceltaPianta.Enabled = Trim(txtNome.Text) <> ""
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                frmSceltaPianta.Enabled = False
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                frmSceltaPianta.Enabled = False
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneImpiantoMappa.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        
        txtNome.Text = ""
        txtNome.Enabled = False
        lblNome.Enabled = False
        
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdSceltaPianta.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    End If
    
    cmdInserisciDaSelezione.Enabled = False
    cmdElimina.Enabled = False
    
End Sub

Public Sub SetIdImpiantoSelezionato(id As Long)
    idImpiantoSelezionato = id
End Sub

Public Sub SetNomeImpiantoSelezionato(nome As String)
    nomeImpiantoSelezionato = nome
End Sub
'
'Private Sub chkSuperArea_Click(Index As Integer)
'    Call CaricaFormDettagliSuperareaImpianto(idRecordSelezionato, chkSuperArea(Index).Caption)
'End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Dim i As Integer
    
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    lstSuperaree.Clear
    Call CancellaChkSuperaree
End Sub

Private Sub cmdAssociaColore_Click()
    Dim i As Long
    Dim c As New Collection
    Dim a As classeArea
    
    For i = 1 To qtaSuperaree
        If chkSuperArea(i).Value = VB_VERO Then
            Set a = New classeArea
            a.nomeArea = chkSuperArea(i).Caption
            Call c.Add(a)
        End If
    Next i
    Call CaricaFormDettagliSuperareaImpianto(idRecordSelezionato, c)
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub conferma()

    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    
    If ValoriCampiOK Then
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
'                Call InserisciNellaBaseDati
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneImpiantoMappa_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneImpiantoMappa_Init
            Case ASG_INSERISCI_DA_SELEZIONE
                Call InserisciNellaBaseDati
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneImpiantoMappa_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneImpiantoMappa_Init
            Case ASG_MODIFICA
                Call AggiornaNellaBaseDati
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneImpiantoMappa_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneImpiantoMappa_Init
            Case ASG_ELIMINA
                Call EliminaDallaBaseDati
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneImpiantoMappa_Init
                Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                Call dgrConfigurazioneImpiantoMappa_Init
        End Select
        Call AggiornaAbilitazioneControlli
    End If
    lstSuperaree.Clear
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaComboVenue
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneImpiantoMappa.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim rec As New ADODB.Recordset
    
    Set rec = adcConfigurazioneImpiantoMappa.Recordset
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSceltaPianta_Click()
    Call Importa
End Sub

Private Sub dgrConfigurazioneImpiantoMappa_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneImpiantoMappa_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneImpiantoMappa_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneImpiantoMappa.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneImpiantoMappa_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneImpiantoMappa
    
    sql = "SELECT IDMAPPAIMPIANTO ID, NOME ""Nome"""
    sql = sql & " FROM MAPPAIMPIANTO"
    sql = sql & " WHERE IDIMPIANTO = " & idImpiantoSelezionato
    sql = sql & " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneImpiantoMappa.dataSource = d
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Importa()
    Call InserisciNellaBaseDati
    Call PopolaListaSuperaree
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim idNuovaMappa As Long
    Dim nomeFile As String
    
    If idPiantaSelezionata = idNessunElementoSelezionato Then
        MsgBox "Selezionare una pianta"
    Else
        Call ApriConnessioneBD_ORA
        Call ORADB.BeginTrans
        
On Error GoTo gestioneErrori
        
        idNuovaMappa = OttieniIdentificatoreDaSequenza("SQ_MAPPAIMPIANTO")
        sql = "INSERT INTO MAPPAIMPIANTO (IDMAPPAIMPIANTO, NOME, IDIMPIANTO)"
        sql = sql & " VALUES ("
        sql = sql & idNuovaMappa & ", "
        sql = sql & "'" & txtNome.Text & "', "
        sql = sql & idImpiantoSelezionato & ")"
        n = ORADB.ExecuteSQL(sql)
        
        sql = "INSERT INTO SUPERAREAIMPIANTO (IDSUPERAREAIMPIANTO, NOME, IDIMPIANTO, RGBCOLORE, RGBCOLOREFONT, IDMAPPAIMPIANTO)" & _
                " SELECT SQ_SUPERAREAIMPIANTO.NEXTVAL, NOME, " & idImpiantoSelezionato & ", 'nero', 'nero', " & idNuovaMappa & _
                " FROM AREA" & _
                " WHERE IDPIANTA = " & idPiantaSelezionata & _
                " AND IDTIPOAREA = 5"
        n = ORADB.ExecuteSQL(sql)
        
        Call SetIdRecordSelezionato(idNuovaMappa)
        
        Call ORADB.CommitTrans
        
        Call ChiudiConnessioneBD_ORA
        
        nomeFile = EsportaDatiMappaImpiantoSuExcel(idNuovaMappa)
        MsgBox "Il file con i dati per la creazione del file JSon delle superaree e': " & nomeFile
    
        Call AggiornaAbilitazioneControlli
    End If
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
On Error GoTo gestioneErrori

    sql = "SELECT NOME"
    sql = sql & " FROM MAPPAIMPIANTO"
    sql = sql & " WHERE IDMAPPAIMPIANTO = " & idRecordSelezionato
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = nomeRecordSelezionato
    PopolaListaSuperaree
        
    internalEvent = internalEventOld

End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
'    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
'        condizioneSql = "IDMAPPAIMPIANTO <> " & idRecordSelezionato
'    End If
'
'    nomeRecordSelezionato = Trim(txtNome.Text)
'    If ViolataUnicitā("MAPPAIMPIANTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDIMPIANTO = " & idImpiantoSelezionato, _
'        condizioneSql, "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
'            " č giā presente in DB per la stessa mappa;")
'    End If
'
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim varco As clsElementoLista

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "UPDATE MAPPAIMPIANTO SET"
    sql = sql & " NOME = " & SqlStringValue(txtNome.Text)
    sql = sql & " WHERE IDMAPPAIMPIANTO = " & idRecordSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim mappaEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Set listaTabelleCorrelate = New Collection

    mappaEliminabile = True
    If Not IsRecordEliminabile("IDMAPPAIMPIANTO", "SUPERAREAIMPIANTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("SUPERAREAIMPIANTO")
        mappaEliminabile = False
    End If
    
    If mappaEliminabile Then
    
        Call ORADB.BeginTrans
    
        sql = "DELETE FROM MAPPAIMPIANTO WHERE IDMAPPAIMPIANTO = " & idRecordSelezionato
        n = ORADB.ExecuteSQL(sql)
        
        Call ORADB.CommitTrans
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "L'area selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub dgrConfigurazioneImpiantoMappa_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneImpiantoMappa
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 2
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub
'
'Private Sub lstSuperaree_Click()
'    CaricaFormDettagliSuperareaImpianto (lstSuperaree.ItemData(lstSuperaree.ListIndex))
'End Sub
'
'Private Sub CaricaFormDettagliSuperareaImpianto(idSuperareaImpianto As Long)
'    Call frmDettagliSuperareaImpianto.SetIdSuperareaImpianto(idSuperareaImpianto)
'    Call frmDettagliSuperareaImpianto.Init
'    Call PopolaListaSuperaree
'End Sub

Private Sub CaricaFormDettagliSuperareaImpianto(idMappaImpianto As Long, c As Collection)
    Call frmDettagliSuperareaImpianto.SetIdMappaImpianto(idMappaImpianto)
    Call frmDettagliSuperareaImpianto.SetCollNomiSuperareaImpianto(c)
    Call frmDettagliSuperareaImpianto.Init
    Call CancellaChkSuperaree
    Call PopolaListaSuperaree
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub PopolaListaSuperaree()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim nome As String
    Dim colonna As Long
    Dim righePerColonna As Long
    Dim colore As String

    Call lstSuperaree.Clear
   
    sql = "SELECT SI.IDSUPERAREAIMPIANTO ID, SI.NOME || ': ' || RGBCOLORE NOME" & _
            " FROM SUPERAREAIMPIANTO SI" & _
            " WHERE SI.IDMAPPAIMPIANTO = " & idRecordSelezionato & _
            " ORDER BY SI.NOME"
    Call PopolaLista(lstSuperaree, sql)
    
    sql = "SELECT SI.IDSUPERAREAIMPIANTO ID, SI.NOME, SI.RGBCOLORE" & _
            " FROM SUPERAREAIMPIANTO SI" & _
            " WHERE SI.IDMAPPAIMPIANTO = " & idRecordSelezionato & _
            " ORDER BY SI.NOME"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        chkSuperArea(0).Visible = False
        colonna = 0
        righePerColonna = 22
            
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            Load chkSuperArea(i)
            chkSuperArea(i).Caption = rec("NOME")
            chkSuperArea(i).Left = 50 + colonna * 3200
            chkSuperArea(i).tOp = 280 * (i - colonna * righePerColonna)
            chkSuperArea(i).Visible = True
            If IsNull(rec("RGBCOLORE")) Then
                colore = ""
            Else
                colore = rec("RGBCOLORE")
            End If
            chkSuperArea(i).BackColor = CalcolaRGB(0, colore)
            
            If i = (colonna + 1) * righePerColonna Then
                colonna = colonna + 1
            End If

            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    qtaSuperaree = i - 1
End Sub

Private Sub CaricaComboVenue()
    Dim sql As String
    
    sql = "SELECT IDVENUE ID, NOME"
    sql = sql & " FROM VENUE"
    sql = sql & " ORDER BY NOME ASC"

    idVenueSelezionato = idNessunElementoSelezionato
    idPiantaSelezionata = idNessunElementoSelezionato
    cmbVenue.Clear
    cmbPianta.Clear
    
    Call CaricaValoriCombo2(cmbVenue, sql, "NOME", False)

End Sub

Private Sub cmbVenue_Click()
    Call venue_Update
End Sub

Private Sub venue_Update()
    Dim sql As String

    idVenueSelezionato = cmbVenue.ItemData(cmbVenue.ListIndex)
    
    sql = "SELECT PT.IDPIANTA ID, PT.NOME" & _
            " FROM PIANTA PT, VENUE_PIANTA VP" & _
            " WHERE VP.IDVENUE = " & idVenueSelezionato & _
            " AND VP.IDPIANTA = PT.IDPIANTA" & _
            " ORDER BY PT.NOME"
    
    cmbPianta.Clear
    Call CaricaValoriCombo2(cmbPianta, sql, "NOME", False)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbPianta_Click()
    Call pianta_Update
End Sub

Private Sub pianta_Update()
    idPiantaSelezionata = cmbPianta.ItemData(cmbPianta.ListIndex)
End Sub

Private Function EsportaDatiMappaImpiantoSuExcel(idMappaImpianto As Long) As String
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim nomeFileBat As String
    Dim mousePointerOld As Integer
    Dim s As String
    Dim i As Long
    Dim qtaSuperaree As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    If idMappaImpianto = idNessunElementoSelezionato Then
        MsgBox "Nessuna mappa selezionata!"
    Else
        Call ApriConnessioneBD
        
        qtaSuperaree = 0
        sql = "SELECT COUNT(IDSUPERAREAIMPIANTO) QTASUPERAREE" & _
                " FROM SUPERAREAIMPIANTO" & _
                " WHERE IDMAPPAIMPIANTO = " & idMappaImpianto
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                qtaSuperaree = rec("QTASUPERAREE")
                rec.MoveNext
            Wend
        End If
        rec.Close
        lblAvanzamento.Caption = "Esportazione di " & qtaSuperaree & " superaree ....."
        DoEvents
    
        nomeFile = App.Path & "\" & idMappaImpianto & "-" & "-ElencoSuperareeMappa-"

        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        sql = "SELECT IDMAPPAIMPIANTO, IDSUPERAREAIMPIANTO, NOME" & _
                " FROM SUPERAREAIMPIANTO" & _
                " WHERE IDMAPPAIMPIANTO = " & idMappaImpianto & _
                " ORDER BY NOME"
    
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet

        xlsSheet.Rows.Cells(1, 1) = "IDMAPPAIMPIANTO"
        xlsSheet.Rows.Cells(1, 2) = "IDSUPERAREAIMPIANTO"
        xlsSheet.Rows.Cells(1, 3) = "NOME"

        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 2
            While Not rec.EOF
                xlsSheet.Rows.Cells(i, 1) = rec("IDMAPPAIMPIANTO")
                xlsSheet.Rows.Cells(i, 2) = rec("IDSUPERAREAIMPIANTO")
                xlsSheet.Rows.Cells(i, 3) = rec("NOME")
                
                i = i + 1
                rec.MoveNext
                
                lblAvanzamento.Caption = "Superarea " & i - 2 & " di " & qtaSuperaree
                DoEvents
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing

        MousePointer = mousePointerOld

    End If
    MousePointer = mousePointerOld
    
    EsportaDatiMappaImpiantoSuExcel = nomeFile

End Function

Public Sub CancellaChkSuperaree()
    Dim i As Long
    
    For i = 1 To qtaSuperaree
        Unload chkSuperArea(i)
    Next i
    qtaSuperaree = 0
End Sub
