VERSION 5.00
Begin VB.Form frmInizialeOperatore 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Operatore"
   ClientHeight    =   9975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9975
   ScaleWidth      =   14880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   8235
      Left            =   120
      TabIndex        =   7
      Top             =   600
      Width           =   12855
      Begin VB.CheckBox chkOperatoreAIVAPreassoltaObbligatoria 
         Alignment       =   1  'Right Justify
         Caption         =   "Operatore a IVA preassolta obbligatoria"
         Height          =   255
         Left            =   6360
         TabIndex        =   36
         Top             =   2280
         Width           =   3435
      End
      Begin VB.TextBox txtEMail 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2520
         TabIndex        =   34
         Top             =   3120
         Width           =   3315
      End
      Begin VB.CommandButton txtResetPassword 
         Caption         =   "Reset password"
         Height          =   495
         Left            =   10560
         TabIndex        =   33
         Top             =   3120
         Width           =   1575
      End
      Begin VB.ComboBox cmbTipoOperativita 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2520
         TabIndex        =   31
         Top             =   2280
         Width           =   3375
      End
      Begin VB.ListBox lstOrganizzazioni 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3630
         Left            =   480
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   4320
         Width           =   5175
      End
      Begin VB.CommandButton cmdCercaPuntiVendita 
         Caption         =   "Cerca punti vendita"
         Height          =   375
         Left            =   6000
         TabIndex        =   28
         Top             =   1680
         Width           =   1575
      End
      Begin VB.ComboBox cmbPuntiVendita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8520
         TabIndex        =   26
         Top             =   1680
         Width           =   3615
      End
      Begin VB.TextBox txtPuntoVenditaElettivo 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2520
         MaxLength       =   30
         TabIndex        =   24
         Top             =   1680
         Width           =   3315
      End
      Begin VB.TextBox txtPassWord 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   7140
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   17
         Top             =   480
         Width           =   3315
      End
      Begin VB.TextBox txtUserName 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         MaxLength       =   30
         TabIndex        =   16
         Top             =   480
         Width           =   3315
      End
      Begin VB.CheckBox chkOperatoreAbilitato 
         Alignment       =   1  'Right Justify
         Caption         =   "Operatore abilitato"
         Height          =   255
         Left            =   6360
         TabIndex        =   15
         Top             =   960
         Width           =   1635
      End
      Begin VB.TextBox txtConfermaPassword 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2520
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   14
         Top             =   960
         Width           =   3315
      End
      Begin VB.CommandButton cmdSvuotaOrgDisponibili 
         Caption         =   ">>"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   11160
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   4260
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.ListBox lstOrganizzazioniSelezionate 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   11760
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   4260
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmdOrgSelezionato 
         Caption         =   ">"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   11220
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   5220
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.CommandButton cmdOrgDisponibile 
         Caption         =   "<"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   11220
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   6180
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.CommandButton cmdSvuotaOrgSelezionati 
         Caption         =   "<<"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   11220
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   7140
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.ListBox lstOrganizzazioniDisponibili 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   9960
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   4260
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "e-Mail"
         Height          =   255
         Left            =   480
         TabIndex        =   35
         Top             =   3180
         Width           =   1935
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo operativitā"
         Height          =   255
         Left            =   840
         TabIndex        =   32
         Top             =   2280
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Organizzazioni per cui l'operatore e' configurato"
         Height          =   195
         Left            =   480
         TabIndex        =   29
         Top             =   4080
         Width           =   4215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "seleziona"
         Height          =   255
         Left            =   7560
         TabIndex        =   27
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Punto vendita elettivo"
         Height          =   255
         Left            =   480
         TabIndex        =   25
         Top             =   1740
         Width           =   1935
      End
      Begin VB.Label lblPassword 
         Alignment       =   1  'Right Justify
         Caption         =   "Password"
         Height          =   255
         Left            =   6120
         TabIndex        =   23
         Top             =   540
         Width           =   855
      End
      Begin VB.Label lblUserName 
         Alignment       =   1  'Right Justify
         Caption         =   "Username"
         Height          =   255
         Left            =   480
         TabIndex        =   22
         Top             =   540
         Width           =   1875
      End
      Begin VB.Label lblConfermaPassword 
         Alignment       =   1  'Right Justify
         Caption         =   "Conferma password"
         Height          =   255
         Left            =   420
         TabIndex        =   21
         Top             =   1020
         Width           =   1935
      End
      Begin VB.Label lblOrganizzazioni 
         Alignment       =   2  'Center
         Caption         =   "Organizzazioni"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   8760
         TabIndex        =   20
         Top             =   3780
         Visible         =   0   'False
         Width           =   3855
      End
      Begin VB.Label lblDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Enabled         =   0   'False
         Height          =   195
         Left            =   9960
         TabIndex        =   19
         Top             =   4020
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblSelezionati 
         Alignment       =   2  'Center
         Caption         =   "Selezionate"
         Enabled         =   0   'False
         Height          =   195
         Left            =   11760
         TabIndex        =   18
         Top             =   3960
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   1395
      Left            =   13080
      TabIndex        =   6
      Top             =   600
      Width           =   1695
      Begin VB.CommandButton cmdRelazioniTraOperatori 
         Caption         =   "Relazioni tra operatori"
         Height          =   435
         Left            =   120
         TabIndex        =   3
         Top             =   780
         Width           =   1395
      End
      Begin VB.CommandButton cmdAnagrafica 
         Caption         =   "Anagrafica"
         Height          =   435
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   5
      Top             =   8880
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeOperatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOperatoreSelezionato As Long
Private idCausaleProtezionePersonaleSelezionata As Long
Private idPersonaOperatore As Long
Private userName As String
Private passWord As String
Private operatoreAbilitato As Integer
Private operatoreAIVAPreassoltaObbligatoria As Integer
Private idTipoOperativita As Long
Private idPuntoVenditaElettivo As Long
Private nomePuntoVenditaElettivo As String
Private idOrganizzazioneSelezionata As Long
Private eMail As String

Private listaOrganizzazioniDisponibili As Collection
Private listaOrganizzazioniSelezionate As Collection
Private isRecordEditabile As Boolean
Private listaDirittiOperatoreSuOrganizzazione As Collection

Private internalEvent As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalitāFormCorrente As AzioneEnum
'Private faseConfigurazioneOperatoriControllati As Boolean

Public Sub SetIdOrganizzazioneSelezionata(idOrg As Long)
    idOrganizzazioneSelezionata = idOrg
End Sub
    
Public Sub Init()
    
    Call Variabili_Init
    Call lstOrganizzazioniDisponibili.Clear
    Call lstOrganizzazioniSelezionate.Clear
    Call CaricaValoriComboTipiOperativita
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            idOperatoreSelezionato = idNessunElementoSelezionato
            idPersonaOperatore = idNessunElementoSelezionato
            idTipoOperativita = idNessunElementoSelezionato
            isRecordEditabile = True
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
    End Select
    
    Call CaricaValoriLstOrganizzazioni
'    Call CaricaValoriLstOrganizzazioniDisponibili
'    Call CaricaValoriLstOrganizzazioniSelezionati
    
    Call AggiornaAbilitazioneControlli
    
    Call frmInizialeOperatore.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    Set listaDirittiOperatoreSuOrganizzazione = New Collection
    userName = ""
    passWord = ""
    idPuntoVenditaElettivo = idNessunElementoSelezionato
    nomePuntoVenditaElettivo = ""
    eMail = ""
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetIdOperatoreSelezionato(id As Long)
    idOperatoreSelezionato = id
End Sub

Public Sub SetIdPersonaOperatore(idP As Long)
    idPersonaOperatore = idP
End Sub

Public Sub SetOperatoreAbilitato(opAb As Integer)
    operatoreAbilitato = opAb
End Sub

Public Sub SetOperatoreAIVAPreassoltaObbligatoria(opAIVA As Integer)
    operatoreAIVAPreassoltaObbligatoria = opAIVA
End Sub

Private Sub AggiornaAbilitazioneControlli()

    cmdSvuotaOrgDisponibili.Enabled = False

    Select Case modalitāFormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Operatore"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Operatore configurato"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Operatore configurato"
    End Select

    If modalitāFormCorrente = A_NUOVO Then
        cmdConferma.Enabled = (Trim(txtUserName.Text) <> "" And _
            Trim(txtPassWord.Text) <> "" And _
            txtPassWord.Text = txtConfermaPassword.Text And _
            idTipoOperativita <> idNessunElementoSelezionato)
    Else
        cmdConferma.Enabled = (Trim(txtUserName.Text) <> "" And _
            idTipoOperativita <> idNessunElementoSelezionato)
    End If
            
'    cmdCaricaOperatori.Enabled = (modalitāFormCorrente <> A_NUOVO)

'    fraOperatoriControllati.Enabled = (modalitāFormCorrente <> A_ELIMINA)
'    lstOperatoriDisponibili.Enabled = faseConfigurazioneOperatoriControllati
'    lstOperatoriSelezionati.Enabled = faseConfigurazioneOperatoriControllati
'    cmdOpDisponibile.Enabled = faseConfigurazioneOperatoriControllati
'    cmdOpSelezionato.Enabled = faseConfigurazioneOperatoriControllati
'    cmdSvuotaOpSelezionati.Enabled = faseConfigurazioneOperatoriControllati
'    cmdSvuotaOpDisponibili.Enabled = faseConfigurazioneOperatoriControllati
'    lblOperatoriSelezionati.Enabled = faseConfigurazioneOperatoriControllati
'    lblOperatoriDisponibili.Enabled = faseConfigurazioneOperatoriControllati
'    If Not faseConfigurazioneOperatoriControllati Then
'        Call lstOperatoriDisponibili.Clear
'        Call lstOperatoriSelezionati.Clear
'    End If
    fraDati.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    txtUserName.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    lblPassword.Visible = (modalitāFormCorrente = A_NUOVO)
    txtPassWord.Enabled = (modalitāFormCorrente = A_NUOVO)
    txtPassWord.Visible = (modalitāFormCorrente = A_NUOVO)
    lblConfermaPassword.Visible = (modalitāFormCorrente = A_NUOVO)
    txtConfermaPassword.Enabled = (modalitāFormCorrente = A_NUOVO)
    txtConfermaPassword.Visible = (modalitāFormCorrente = A_NUOVO)
    lblUserName.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    lblPassword.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    lblConfermaPassword.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    chkOperatoreAbilitato.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    chkOperatoreAIVAPreassoltaObbligatoria.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    txtEMail.Enabled = (modalitāFormCorrente <> A_ELIMINA)
    
'    fraOperatoriControllati.Visible = (modalitāFormCorrente <> A_NUOVO)
    
    fraAzioni.Visible = (modalitāFormCorrente = A_MODIFICA)
                                
End Sub

Private Sub CaricaValoriComboPuntiVendita()
    Dim sql As String
    
    sql = "SELECT IDPUNTOVENDITA ID, NOME" & _
        " FROM PUNTOVENDITA" & _
        " WHERE NOME LIKE '" & txtPuntoVenditaElettivo & "%'" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo3(cmbPuntiVendita, sql, "NOME", 100)
End Sub

Private Sub CaricaValoriComboTipiOperativita()
    Dim sql As String
    
    sql = "SELECT IDTIPOOPERATIVITACLIENT ID, NOME FROM TIPOOPERATIVITACLIENT" & _
        " ORDER BY IDTIPOOPERATIVITACLIENT"
    Call CaricaValoriCombo3(cmbTipoOperativita, sql, "NOME", 100)
End Sub

Private Sub chkOperatoreAIVAPreassoltaObbligatoria_Click()
    If chkOperatoreAIVAPreassoltaObbligatoria = 1 Then
        chkOperatoreAIVAPreassoltaObbligatoria.ForeColor = &HFF&
    Else
        chkOperatoreAIVAPreassoltaObbligatoria.ForeColor = &H80000012
    End If
End Sub

Private Sub cmbPuntiVendita_Click()
    If Not internalEvent Then
        idPuntoVenditaElettivo = cmbPuntiVendita.ItemData(cmbPuntiVendita.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
        
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaOperatore.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
        sql = "SELECT USERNAME, '******' PASSWORD, ABILITATO, IDPERSONA, PV.IDPUNTOVENDITA, PV.NOME, IDTIPOOPERATIVITACLIENT, O.EMAIL, O.IVAPREASSOLTAOBBLIGATORIA" & _
            " FROM OPERATORE O, PUNTOVENDITA PV" & _
            " WHERE IDOPERATORE = " & idOperatoreSelezionato & _
            " AND O.IDPUNTOVENDITAELETTIVO = PV.IDPUNTOVENDITA(+)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        userName = rec("USERNAME")
        passWord = rec("PASSWORD")
        operatoreAbilitato = rec("ABILITATO")
        operatoreAIVAPreassoltaObbligatoria = rec("IVAPREASSOLTAOBBLIGATORIA")
        idTipoOperativita = rec("IDTIPOOPERATIVITACLIENT")
        idPersonaOperatore = IIf(IsNull(rec("IDPERSONA")), idNessunElementoSelezionato, rec("IDPERSONA"))
        idPuntoVenditaElettivo = IIf(IsNull(rec("IDPUNTOVENDITA")), idNessunElementoSelezionato, rec("IDPUNTOVENDITA"))
        nomePuntoVenditaElettivo = IIf(IsNull(rec("NOME")), "", rec("NOME"))
        eMail = IIf(IsNull(rec("EMAIL")), "", rec("EMAIL"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    If operatoreAIVAPreassoltaObbligatoria = 1 Then
        chkOperatoreAIVAPreassoltaObbligatoria.ForeColor = &HFF&
    End If
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtUserName.Text = ""
    txtPassWord.Text = ""
    txtUserName.Text = userName
    txtPassWord.Text = passWord
    txtConfermaPassword.Text = passWord
    chkOperatoreAbilitato.Value = IIf(operatoreAbilitato = 0, vbUnchecked, vbChecked)
    chkOperatoreAIVAPreassoltaObbligatoria.Value = IIf(operatoreAIVAPreassoltaObbligatoria = 0, vbUnchecked, vbChecked)
    Call SelezionaElementoSuCombo(cmbTipoOperativita, idTipoOperativita)
    txtPuntoVenditaElettivo.Text = nomePuntoVenditaElettivo
    txtEMail.Text = eMail
    
    If idOrganizzazioneSelezionata = idNessunElementoSelezionato Then
        sql = "SELECT IDPUNTOVENDITA ID, NOME" & _
            " FROM PUNTOVENDITA" & _
            " WHERE IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " ORDER BY NOME"
    Else
        sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
            " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, PUNTOVENDITA PV" & _
            " WHERE OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " ORDER BY NOME"
    End If
    Call CaricaValoriCombo3(cmbPuntiVendita, sql, "NOME", 100)
    
    internalEvent = internalEventOld
    
End Sub

'Private Sub InserisciNellaBaseDati_old()
'    Dim sql As String
'    Dim i As Integer
'    Dim idNuovoOperatore As Long
'    Dim n As Long
'    Dim organizzazione As clsElementoLista
'    Dim dirittiOperatore As clsDiritOperOrg
'    Const STATO_CASSA_CHIUSA As Integer = 2
'
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
'    idNuovoOperatore = OttieniIdentificatoreDaSequenza("SQ_OPERATORE")
'    sql = "INSERT INTO OPERATORE (IDOPERATORE, USERNAME, PASSWORD, ABILITATO, IVAPREASSOLTAOBBLIGATORIA,"
'    sql = sql & " IDPUNTOVENDITA, DATAORAULTIMAAPERTURACASSA, DATAORAULTIMACHIUSURACASSA,"
'    sql = sql & " IMPORTOAPERTURACASSA, IDTIPOSTATOCASSA, IDPERSONA)"
'    sql = sql & " VALUES ("
'    sql = sql & idNuovoOperatore & ", "
'    sql = sql & SqlStringValue(userName) & ", "
'    sql = sql & SqlStringValue(passWord) & ", "
'    sql = sql & operatoreAbilitato & ", "
'    sql = sql & operatoreAIVAPreassoltaObbligatoria & ", "
'    sql = sql & "NULL, "
'    sql = sql & "NULL, "
'    sql = sql & "NULL, "
'    sql = sql & "NULL, "
'    sql = sql & STATO_CASSA_CHIUSA & ", "
'    sql = sql & "NULL)"
'    SETAConnection.Execute sql, n, adCmdText
'
'    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
'        For Each organizzazione In listaOrganizzazioniSelezionate
'            sql = "INSERT INTO OPERATORE_ORGANIZZAZIONE (IDOPERATORE, IDORGANIZZAZIONE)" & _
'                " VALUES (" & _
'                idNuovoOperatore & ", " & _
'                organizzazione.idElementoLista & ")"
'            SETAConnection.Execute sql, n, adCmdText
'            Set dirittiOperatore = DirittiOperatoreSuOrganizzazione(organizzazione.idElementoLista)
'            If Not (dirittiOperatore Is Nothing) Then
'                sql = " INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)"
'                sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDPRODOTTO"
'                sql = sql & " FROM PRODOTTO"
'                sql = sql & " WHERE IDPRODOTTO IN"
'                sql = sql & " ("
'                sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                sql = sql & " )"
'                SETAConnection.Execute sql, n, adCmdText
'                If dirittiOperatore.selezionaDirittiDaOperatoreMaster Then
'                    If dirittiOperatore.importaDirittiSuTutteLeProtezioni Then
'                        sql = " INSERT INTO OPERATORE_CAUSALEPROTEZIONE ("
'                        sql = sql & " IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
'                        sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", OC.IDCAUSALEPROTEZIONE, OC.IDPRODOTTO"
'                        sql = sql & " FROM OPERATORE_CAUSALEPROTEZIONE OC, CAUSALEPROTEZIONE C"
'                        sql = sql & " WHERE C.IDCAUSALEPROTEZIONE = OC.IDCAUSALEPROTEZIONE"
'                        sql = sql & " AND OC.IDPRODOTTO IN"
'                        sql = sql & " ("
'                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                        sql = sql & " )"
'                        sql = sql & " AND OC.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
'                        sql = sql & " AND C.IDOPERATORE IS NULL"
'                        SETAConnection.Execute sql, n, adCmdText
'                    End If
'                    If dirittiOperatore.importaDirittiSuTutteLeTariffe Then
'                        sql = " INSERT INTO OPERATORE_TARIFFA ("
'                        sql = sql & " IDOPERATORE, IDTARIFFA)"
'                        sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", OT.IDTARIFFA"
'                        sql = sql & " FROM TARIFFA T, OPERATORE_TARIFFA OT"
'                        sql = sql & " WHERE T.IDTARIFFA = OT.IDTARIFFA"
'                        sql = sql & " AND OT.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
'                        sql = sql & " AND IDPRODOTTO IN"
'                        sql = sql & " ("
'                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                        sql = sql & " )"
'                        SETAConnection.Execute sql, n, adCmdText
'                    End If
'                    sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
'                    sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO) "
'                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDTIPOOPERAZIONE, IDPRODOTTO"
'                    sql = sql & " FROM OPERATORE_TIPOOPERAZIONE OT"
'                    sql = sql & " WHERE OT.IDPRODOTTO IN"
'                    sql = sql & " ("
'                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                    sql = sql & " )"
'                    sql = sql & " AND OT.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
'                    SETAConnection.Execute sql, n, adCmdText
'                ElseIf dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto Then
'                    sql = " INSERT INTO OPERATORE_CAUSALEPROTEZIONE ("
'                    sql = sql & " IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
'                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDCAUSALEPROTEZIONE, IDPRODOTTO"
'                    sql = sql & " FROM CAUSALEPROTEZIONE C, PRODOTTO P"
'                    sql = sql & " WHERE C.IDORGANIZZAZIONE = " & organizzazione.idElementoLista
'                    sql = sql & " AND IDPRODOTTO IN"
'                    sql = sql & " ("
'                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                    sql = sql & " )"
'                    sql = sql & " AND C.IDOPERATORE IS NULL"
'                    SETAConnection.Execute sql, n, adCmdText
'                    sql = " INSERT INTO OPERATORE_TARIFFA ("
'                    sql = sql & " IDOPERATORE, IDTARIFFA)"
'                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDTARIFFA"
'                    sql = sql & " FROM TARIFFA T"
'                    sql = sql & " WHERE IDPRODOTTO IN"
'                    sql = sql & " ("
'                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                    sql = sql & " )"
'                    SETAConnection.Execute sql, n, adCmdText
'                    sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
'                    sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO)"
'                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDTIPOOPERAZIONE, IDPRODOTTO"
'                    sql = sql & " FROM OPERATORE_TIPOOPERAZIONE OT"
'                    sql = sql & " WHERE OT.IDPRODOTTO IN"
'                    sql = sql & " ("
'                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
'                    sql = sql & " )"
'                    sql = sql & " AND OT.IDTIPOOPERAZIONE IN"
'                    sql = sql & " ("
'                    sql = sql & TO_VENDITA & ", " & TO_STAMPA & ", " & TO_ANNULLAMENTO_VENDITA & ", " & TO_ANNULLAMENTO_STAMPA
'                    sql = sql & " )"
'                    SETAConnection.Execute sql, n, adCmdText
'                End If
'            End If
'        Next organizzazione
''        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''            tipoStatoRecordSelezionato = TSR_NUOVO
''            Call AggiornaParametriSessioneSuRecord("OPERATORE_ORGANIZZAZIONE", "IDOPERATORE", idNuovoOperatore, TSR_NUOVO)
''            If Not (dirittiOperatore Is Nothing) Then
''                Call AggiornaParametriSessioneSuRecord("OPERATORE_PRODOTTO", "IDOPERATORE", idNuovoOperatore, TSR_NUOVO)
''                Call AggiornaParametriSessioneSuRecord("OPERATORE_CAUSALEPROTEZIONE", "IDOPERATORE", idNuovoOperatore, TSR_NUOVO)
''                Call AggiornaParametriSessioneSuRecord("OPERATORE_TARIFFA", "IDOPERATORE", idNuovoOperatore, TSR_NUOVO)
''                Call AggiornaParametriSessioneSuRecord("OPERATORE_TIPOOPERAZIONE", "IDOPERATORE", idNuovoOperatore, TSR_NUOVO)
''            End If
''        End If
'    End If
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Call frmSceltaOperatore.SetIdRecordSelezionato(idNuovoOperatore)
'    Call SetIdOperatoreSelezionato(idNuovoOperatore)
'    Call AggiornaAbilitazioneControlli
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
'
'End Sub

'
'Private Sub cmdCaricaOperatori_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call CaricaOperatori
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub CaricaOperatori()
'    faseConfigurazioneOperatoriControllati = True
'    Call lstOperatoriDisponibili.Clear
'    Call lstOperatoriSelezionati.Clear
'    Call CaricaValorilstOperatoriDisponibili
'    Call CaricaValorilstOperatoriSelezionati
'    Call AggiornaAbilitazioneControlli
'End Sub

Private Sub cmdCercaPuntiVendita_Click()
    CaricaValoriComboPuntiVendita
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDOPERATORE = " & idOperatoreSelezionato
    
    If txtPuntoVenditaElettivo.Text = "" Then
        idPuntoVenditaElettivo = idNessunElementoSelezionato
    End If
    
    If isRecordEditabile Then
        Select Case modalitāFormCorrente
            Case A_NUOVO
                If valoriCampiOK = True Then
                    Call InserisciNellaBaseDati
                    stringaNota = "IDOPERATORE = " & idOperatoreSelezionato
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
                    Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                    Call SetModalitāForm(A_MODIFICA)
                    Call AggiornaAbilitazioneControlli
                End If
            Case A_MODIFICA
                If valoriCampiOK = True Then
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
                    Call frmMessaggio.Visualizza("NotificaModificaDati")
                    Call Esci
                End If
            Case A_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
                Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
                Call Esci
        End Select
    End If
    Call frmSceltaOperatore.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub
'
'Private Sub cmdOpDisponibile_Click()
'    Call SpostaInLstOperatoriDisponibili
'End Sub

Private Sub cmdOrgDisponibile_Click()
    Call SpostaInLstOrganizzazioniDisponibili
'    faseConfigurazioneOperatoriControllati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdOrgSelezionato_Click()
    Call SpostaInLstOrganizzazioniSelezionate
'    faseConfigurazioneOperatoriControllati = False
    Call AggiornaAbilitazioneControlli
End Sub
'
'Private Sub cmdOpSelezionato_Click()
'    Call SpostaInLstOperatoriSelezionati
'End Sub

Private Sub cmdRelazioniTraOperatori_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraRelazioniTraOperatori
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraRelazioniTraOperatori()
    Call CaricaFormConfigurazioneOperatoreOperatori
End Sub

Private Sub CaricaFormConfigurazioneOperatoreOperatori()
    Call frmConfigurazioneOperatoreOperatori.SetIdOperatoreSelezionato(idOperatoreSelezionato)
    Call frmConfigurazioneOperatoreOperatori.SetUserNameOperatoreSelezionato(userName)
    If idPersonaOperatore = idNessunElementoSelezionato Then
        Call frmConfigurazioneOperatoreOperatori.SetModalitāForm(A_NUOVO)
    Else
        Call frmConfigurazioneOperatoreOperatori.SetModalitāForm(A_MODIFICA)
    End If
    Call frmConfigurazioneOperatoreOperatori.SetIsOperatoreEditabile(isRecordEditabile)
    Call frmConfigurazioneOperatoreOperatori.Init
End Sub

Private Sub cmdSvuotaOrgSelezionati_Click()
    Call SvuotaOrganizzazioniSelezionate
'    faseConfigurazioneOperatoriControllati = False
    Call AggiornaAbilitazioneControlli
End Sub
'
'Private Sub cmdSvuotaOpSelezionati_Click()
'    Call SvuotaOperatoriSelezionati
'End Sub
'
'Private Sub lstOperatoriDisponibili_Click()
'    Call VisualizzaListBoxToolTip(lstOperatoriDisponibili, lstOperatoriDisponibili.Text)
'End Sub
'
'Private Sub lstOperatoriSelezionati_Click()
'    Call VisualizzaListBoxToolTip(lstOperatoriSelezionati, lstOperatoriSelezionati.Text)
'End Sub

Private Sub lstOrganizzazioniDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstOrganizzazioniDisponibili, lstOrganizzazioniDisponibili.Text)
End Sub

Private Sub lstOrganizzazioniSelezionate_Click()
    Call VisualizzaListBoxToolTip(lstOrganizzazioniSelezionate, lstOrganizzazioniSelezionate.Text)
End Sub

Private Sub txtResetPassword_Click()
    Call ResetPassword
End Sub

Private Sub txtUsername_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtPassWord_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtConfermaPassword_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim condizioniSQL As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM OPERATORE_OPERATORE WHERE"
'            sql = sql & " IDOPERATORECONTROLLORE = " & idOperatoreSelezionato & " OR"
'            sql = sql & " IDOPERATORECONTROLLATO = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM OPERATORE_CAUSALEPROTEZIONE WHERE IDOPERATORE = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM OPERATORE_ORGANIZZAZIONE WHERE IDOPERATORE = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM OPERATORE_PRODOTTO WHERE IDOPERATORE = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM OPERATORE_TARIFFA WHERE IDOPERATORE = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE WHERE IDOPERATORE = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM OPERATORE WHERE IDOPERATORE = " & idOperatoreSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM PERSONA WHERE IDPERSONA =" & idPersonaOperatore
'            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM CAUSALEPROTEZIONE WHERE IDCAUSALEPROTEZIONE = " & idCausaleProtezionePersonaleSelezionata
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            condizioniSQL = " OR IDOPERATORECONTROLLATO = " & idOperatoreSelezionato
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_OPERATORE", "IDOPERATORECONTROLLORE", idOperatoreSelezionato, TSR_ELIMINATO, condizioniSQL)
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_CAUSALEPROTEZIONE", "IDOPERATORE", idOperatoreSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_ORGANIZZAZIONE", "IDOPERATORE", idOperatoreSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_PRODOTTO", "IDOPERATORE", idOperatoreSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_TARIFFA", "IDOPERATORE", idOperatoreSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("OPERATORE_TIPOOPERAZIONE", "IDOPERATORE", idOperatoreSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("OPERATORE", "IDOPERATORE", idOperatoreSelezionato, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("PERSONA", "IDPERSONA", idPersonaOperatore, TSR_ELIMINATO)
'            Call AggiornaParametriSessioneSuRecord("CAUSALEPROTEZIONE", "IDCAUSALEPROTEZIONE", idCausaleProtezionePersonaleSelezionata, TSR_ELIMINATO)
'        End If
'    Else
        sql = "DELETE FROM OPERATORE_OPERATORE WHERE"
        sql = sql & " IDOPERATORECONTROLLORE = " & idOperatoreSelezionato & " OR"
        sql = sql & " IDOPERATORECONTROLLATO = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE_CAUSALEPROTEZIONE WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE_ORGANIZZAZIONE WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE_PRODOTTO WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE_TARIFFA WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM OPERATORE WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM PERSONA WHERE IDPERSONA =" & idPersonaOperatore
        SETAConnection.Execute sql, n, adCmdText
        sql = "DELETE FROM CAUSALEPROTEZIONE WHERE IDCAUSALEPROTEZIONE = " & idCausaleProtezionePersonaleSelezionata
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

'Private Sub CaricaValorilstOperatoriDisponibili()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim i As Integer
'    Dim elencoIdOrganizzazioni As String
'    Dim chiaveOperatore As String
'    Dim operatoreCorrente As clsElementoLista
'    Dim organizzazioneCorrente As clsElementoLista
'
'    Call ApriConnessioneBD
'
'    Set listaOperatoriDisponibili = New Collection
'    elencoIdOrganizzazioni = ""
'
'    For Each organizzazioneCorrente In listaOrganizzazioniSelezionate
'        elencoIdOrganizzazioni = IIf(elencoIdOrganizzazioni = "", _
'            CStr(organizzazioneCorrente.idElementoLista), _
'            elencoIdOrganizzazioni & ", " & CStr(organizzazioneCorrente.idElementoLista))
'    Next organizzazioneCorrente
'    If listaOrganizzazioniSelezionate.count > 0 Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT OP.IDOPERATORE AS ""ID"", OP.USERNAME" & _
'               " FROM OPERATORE OP, OPERATORE_ORGANIZZAZIONE ORG, OPERATORE_OPERATORE OP_OP" & _
'               " WHERE OP.IDOPERATORE = ORG.IDOPERATORE" & _
'               " AND OP.IDOPERATORE <> " & idOperatoreSelezionato & _
'               " AND ORG.IDORGANIZZAZIONE IN (" & elencoIdOrganizzazioni & ")" & _
'               " AND OP.IDOPERATORE = OP_OP.IDOPERATORECONTROLLATO(+)" & _
'               " AND (OP_OP.IDOPERATORECONTROLLATO IS NULL OR OP_OP.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")" & _
'               " AND OP_OP.IDOPERATORECONTROLLORE(+) = " & idOperatoreSelezionato & _
'               " ORDER BY USERNAME"
'        Else
'            sql = "SELECT DISTINCT OP.IDOPERATORE AS ""ID"", OP.USERNAME" & _
'               " FROM OPERATORE OP, OPERATORE_ORGANIZZAZIONE ORG, OPERATORE_OPERATORE OP_OP" & _
'               " WHERE OP.IDOPERATORE = ORG.IDOPERATORE" & _
'               " AND OP.IDOPERATORE <> " & idOperatoreSelezionato & _
'               " AND ORG.IDORGANIZZAZIONE IN (" & elencoIdOrganizzazioni & ")" & _
'               " AND OP.IDOPERATORE = OP_OP.IDOPERATORECONTROLLATO(+)" & _
'               " AND OP_OP.IDOPERATORECONTROLLATO IS NULL" & _
'               " AND OP_OP.IDOPERATORECONTROLLORE(+) = " & idOperatoreSelezionato & _
'               " ORDER BY USERNAME"
'        End If
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            While Not rec.EOF
'                Set operatoreCorrente = New clsElementoLista
'                operatoreCorrente.idElementoLista = rec("ID").Value
'                operatoreCorrente.nomeElementoLista = rec("USERNAME")
'                operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
'                chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
'                Call listaOperatoriDisponibili.Add(operatoreCorrente, chiaveOperatore)
'                rec.MoveNext
'            Wend
'        End If
'        rec.Close
'    End If
'
'    Call ChiudiConnessioneBD
'
'    Call lstOperatoriDisponibili_Init
'
'End Sub
'
'Private Sub CaricaValorilstOperatoriSelezionati()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim i As Integer
'    Dim elencoIdOrganizzazioni As String
'    Dim chiaveOperatore As String
'    Dim operatoreCorrente As clsElementoLista
'    Dim organizzazioneCorrente As clsElementoLista
'
'    Call ApriConnessioneBD
'
'    Set listaOperatoriSelezionati = New Collection
'    elencoIdOrganizzazioni = ""
'
'    For Each organizzazioneCorrente In listaOrganizzazioniSelezionate
'        elencoIdOrganizzazioni = IIf(elencoIdOrganizzazioni = "", _
'            CStr(organizzazioneCorrente.idElementoLista), _
'            elencoIdOrganizzazioni & ", " & CStr(organizzazioneCorrente.idElementoLista))
'    Next organizzazioneCorrente
'    If listaOrganizzazioniSelezionate.count > 0 Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT O.IDOPERATORE AS ""ID"", O.USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_OPERATORE OO, OPERATORE_ORGANIZZAZIONE OG" & _
'                " WHERE O.IDOPERATORE = OO.IDOPERATORECONTROLLATO" & _
'                " AND O.IDOPERATORE = OG.IDOPERATORE" & _
'                " AND (OO.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OO.IDTIPOSTATORECORD IS NULL)" & _
'                " AND OO.IDOPERATORECONTROLLORE = " & idOperatoreSelezionato & _
'                " AND OG.IDORGANIZZAZIONE IN (" & elencoIdOrganizzazioni & ")" & _
'                " ORDER BY USERNAME"
'        Else
'            sql = "SELECT DISTINCT O.IDOPERATORE AS ""ID"", O.USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_OPERATORE OO, OPERATORE_ORGANIZZAZIONE OG" & _
'                " WHERE O.IDOPERATORE = OO.IDOPERATORECONTROLLATO" & _
'                " AND O.IDOPERATORE = OG.IDOPERATORE" & _
'                " AND OO.IDOPERATORECONTROLLORE = " & idOperatoreSelezionato & _
'                " AND OG.IDORGANIZZAZIONE IN (" & elencoIdOrganizzazioni & ")" & _
'                " ORDER BY USERNAME"
'        End If
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            While Not rec.EOF
'                Set operatoreCorrente = New clsElementoLista
'                operatoreCorrente.idElementoLista = rec("ID").Value
'                operatoreCorrente.nomeElementoLista = rec("USERNAME")
'                operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
'                chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
'                Call listaOperatoriSelezionati.Add(operatoreCorrente, chiaveOperatore)
'                rec.MoveNext
'            Wend
'        End If
'        rec.Close
'    End If
'
'    Call ChiudiConnessioneBD
'
'    Call lstOperatoriSelezionati_Init
'
'End Sub
'
'Private Sub lstOperatoriDisponibili_Init()
'    Dim internalEventOld As Boolean
'    Dim i As Integer
'    Dim operatore As clsElementoLista
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    lstOperatoriDisponibili.Clear
'
'    If Not (listaOperatoriDisponibili Is Nothing) Then
'        i = 1
'        For Each operatore In listaOperatoriDisponibili
'            lstOperatoriDisponibili.AddItem operatore.descrizioneElementoLista
'            lstOperatoriDisponibili.ItemData(i - 1) = operatore.idElementoLista
'            i = i + 1
'        Next operatore
'    End If
'
'    internalEvent = internalEventOld
'
'End Sub
'
'Private Sub lstOperatoriSelezionati_Init()
'    Dim internalEventOld As Boolean
'    Dim i As Integer
'    Dim operatore As clsElementoLista
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    lstOperatoriSelezionati.Clear
'
'    If Not (listaOperatoriSelezionati Is Nothing) Then
'        i = 1
'        For Each operatore In listaOperatoriSelezionati
'            lstOperatoriSelezionati.AddItem operatore.descrizioneElementoLista
'            lstOperatoriSelezionati.ItemData(i - 1) = operatore.idElementoLista
'            i = i + 1
'        Next operatore
'    End If
'
'    internalEvent = internalEventOld
'
'End Sub
'
'Private Sub SpostaInLstOperatoriDisponibili()
'    Dim i As Integer
'    Dim idOperatore As Long
'    Dim operatore As clsElementoLista
'    Dim chiaveOperatore As String
'
'    For i = 1 To lstOperatoriSelezionati.ListCount
'        If lstOperatoriSelezionati.Selected(i - 1) Then
'            idOperatore = lstOperatoriSelezionati.ItemData(i - 1)
'            chiaveOperatore = ChiaveId(idOperatore)
'            Set operatore = listaOperatoriSelezionati.Item(chiaveOperatore)
'            Call listaOperatoriDisponibili.Add(operatore, chiaveOperatore)
'            Call listaOperatoriSelezionati.Remove(chiaveOperatore)
'        End If
'    Next i
'    Call lstOperatoriDisponibili_Init
'    Call lstOperatoriSelezionati_Init
'End Sub
'
'Private Sub SpostaInLstOperatoriSelezionati()
'    Dim i As Integer
'    Dim idOperatore As Long
'    Dim operatore As clsElementoLista
'    Dim chiaveOperatore As String
'
'    For i = 1 To lstOperatoriDisponibili.ListCount
'        If lstOperatoriDisponibili.Selected(i - 1) Then
'            idOperatore = lstOperatoriDisponibili.ItemData(i - 1)
'            chiaveOperatore = ChiaveId(idOperatore)
'            Set operatore = listaOperatoriDisponibili.Item(chiaveOperatore)
'            Call listaOperatoriSelezionati.Add(operatore, chiaveOperatore)
'            Call listaOperatoriDisponibili.Remove(chiaveOperatore)
'        End If
'    Next i
'    Call lstOperatoriDisponibili_Init
'    Call lstOperatoriSelezionati_Init
'End Sub
'
'Private Sub SvuotaOperatoriSelezionati()
'    Dim operatore As clsElementoLista
'    Dim chiaveOperatore As String
'
'    For Each operatore In listaOperatoriSelezionati
'        chiaveOperatore = ChiaveId(operatore.idElementoLista)
'        Call listaOperatoriDisponibili.Add(operatore, chiaveOperatore)
'    Next operatore
'    Set listaOperatoriSelezionati = Nothing
'    Set listaOperatoriSelezionati = New Collection
'
'    Call lstOperatoriDisponibili_Init
'    Call lstOperatoriSelezionati_Init
'End Sub
'
Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
On Error Resume Next

    valoriCampiOK = True
    Set listaNonConformitā = New Collection
    condizioneSql = ""
    If modalitāFormCorrente = A_MODIFICA Or modalitāFormCorrente = A_ELIMINA Then
        condizioneSql = "IDOPERATORE <> " & idOperatoreSelezionato
    End If
    
    userName = Trim(txtUserName.Text)
    If ViolataUnicitā("OPERATORE", "USERNAME = " & SqlStringValue(userName), "", _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore username = " & SqlStringValue(userName) & _
            " č giā presente in DB;")
    End If
    passWord = txtPassWord.Text
    operatoreAbilitato = chkOperatoreAbilitato.Value
    operatoreAIVAPreassoltaObbligatoria = chkOperatoreAIVAPreassoltaObbligatoria.Value
    eMail = txtEMail.Text
    
    Call ApriConnessioneBD
    
    ' verifica che l'operativita indicata sia compatibile con gli altri operatori dello stesso puntovendita
    sql = "SELECT COUNT(*) QTA" & _
            " FROM OPERATORE OC, OPERATORE O2" & _
            " WHERE OC.IDOPERATORE = " & idOperatoreSelezionato & _
            " AND OC.IDPUNTOVENDITAELETTIVO = O2.IDPUNTOVENDITAELETTIVO" & _
            " AND OC.IDOPERATORE <> O2.IDOPERATORE" & _
            " AND O2.IDTIPOOPERATIVITACLIENT <> 0" & _
            " AND O2.IDTIPOOPERATIVITACLIENT <> " & idTipoOperativita
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("QTA") > 0 Then
                valoriCampiOK = False
                Call listaNonConformitā.Add("- l'operativita' indicata non e' compatibile con gli altri operatori del punto vendita;")
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    ' se a IVA preassolta obbligatoria verifica che esso sia compreso nella lista degli operatori abilitabili
    If operatoreAIVAPreassoltaObbligatoria = 1 Then
        sql = "SELECT COUNT(*) QTA" & _
                " FROM OPABILITABILEIVAPREASSOLTAOBBL" & _
                " WHERE IDOPERATORE = " & idOperatoreSelezionato
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                If rec("QTA") = 0 Then
                    valoriCampiOK = False
                    Call listaNonConformitā.Add("- l'operatore non puo' essere configurato come a IVA preassolta obbligatoria;")
                End If
                rec.MoveNext
            Wend
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Private Sub cmdAnagrafica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraAnagrafica
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraAnagrafica()
    Call CaricaFormConfigurazioneOperatoreAnagrafica
End Sub

Private Sub CaricaFormConfigurazioneOperatoreAnagrafica()
    Call frmConfigurazioneOperatoreAnagrafica.SetIdOperatoreSelezionato(idOperatoreSelezionato)
    Call frmConfigurazioneOperatoreAnagrafica.SetUserNameOperatoreSelezionato(userName)
    If idPersonaOperatore = idNessunElementoSelezionato Then
        Call frmConfigurazioneOperatoreAnagrafica.SetModalitāFormCorrente(A_NUOVO)
    Else
        Call frmConfigurazioneOperatoreAnagrafica.SetModalitāFormCorrente(A_MODIFICA)
    End If
    Call frmConfigurazioneOperatoreAnagrafica.Init
End Sub
'
'Private Sub cmdSvuotaOpDisponibili_Click()
'    Call SvuotaOperatoriDisponibili
'End Sub
'
'Private Sub SvuotaOperatoriDisponibili()
'    Dim operatore As clsElementoLista
'    Dim chiaveOperatore As String
'
'    For Each operatore In listaOperatoriDisponibili
'        chiaveOperatore = ChiaveId(operatore.idElementoLista)
'        Call listaOperatoriSelezionati.Add(operatore, chiaveOperatore)
'    Next operatore
'    Set listaOperatoriDisponibili = Nothing
'    Set listaOperatoriDisponibili = New Collection
'
'    Call lstOperatoriDisponibili_Init
'    Call lstOperatoriSelezionati_Init
'End Sub

Private Sub CaricaValoriLstOrganizzazioni()
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    sql = "SELECT DISTINCT ORG.NOME || '- ' || CPV.NOME NOME" & _
        " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, ORGANIZZAZIONE ORG, CLASSEPUNTOVENDITA CPV" & _
        " WHERE OP.IDOPERATORE = " & idOperatoreSelezionato & _
        " AND OP.IDPUNTOVENDITAELETTIVO = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = ORG.IDORGANIZZAZIONE" & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = CPV.IDCLASSEPUNTOVENDITA" & _
        " ORDER BY ORG.NOME || '- ' || CPV.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            lstOrganizzazioni.AddItem rec("NOME").Value
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CaricaValoriLstOrganizzazioniDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    Dim idTipoStatoOrganizzazione As TipoStatoOrganizzazioneEnum
    Dim dirittiOperatore As clsDiritOperOrg
        
    Call ApriConnessioneBD

    Set listaOrganizzazioniSelezionate = New Collection
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT ORG.IDORGANIZZAZIONE ID," & _
'            " ORG.NOME || ' - ' || TSO.NOME ORG, ORG.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO" & _
'            " FROM ORGANIZZAZIONE ORG, OPERATORE_ORGANIZZAZIONE OP, TIPOSTATOORGANIZZAZIONE TSO" & _
'            " WHERE OP.IDOPERATORE = " & idOperatoreSelezionato & _
'            " AND ORG.IDORGANIZZAZIONE = OP.IDORGANIZZAZIONE" & _
'            " AND (OP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR OP.IDTIPOSTATORECORD IS NULL)" & _
'            " AND ORG.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
'            " ORDER BY ORG"
'    Else
        sql = "SELECT ORG.IDORGANIZZAZIONE ID," & _
            " ORG.NOME || ' - ' || TSO.NOME ORG, ORG.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO" & _
            " FROM ORGANIZZAZIONE ORG, OPERATORE_ORGANIZZAZIONE OP, TIPOSTATOORGANIZZAZIONE TSO" & _
            " WHERE OP.IDOPERATORE = " & idOperatoreSelezionato & _
            " AND ORG.IDORGANIZZAZIONE = OP.IDORGANIZZAZIONE" & _
            " AND ORG.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
            " ORDER BY ORG"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            Set dirittiOperatore = New clsDiritOperOrg
            Call dirittiOperatore.Init
            organizzazioneCorrente.nomeElementoLista = rec("ORG")
            organizzazioneCorrente.descrizioneElementoLista = rec("ORG")
            organizzazioneCorrente.idElementoLista = rec("ID").Value
            organizzazioneCorrente.idAttributoElementoLista = rec("TIPOSTATO").Value
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            dirittiOperatore.idOrganizzazione = organizzazioneCorrente.idElementoLista
            Call listaOrganizzazioniSelezionate.Add(organizzazioneCorrente, chiaveOrganizzazione)
            Call listaDirittiOperatoreSuOrganizzazione.Add(dirittiOperatore, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOrganizzazioniSelezionate_Init
    
End Sub

Private Sub CaricaValoriLstOrganizzazioniSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    Dim idTipoStatoOrganizzazione As TipoStatoOrganizzazioneEnum
    Dim dirittiOperatore As clsDiritOperOrg
        
    Call ApriConnessioneBD

    Set listaOrganizzazioniSelezionate = New Collection
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT ORG.IDORGANIZZAZIONE ID," & _
'            " ORG.NOME || ' - ' || TSO.NOME ORG, ORG.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO" & _
'            " FROM ORGANIZZAZIONE ORG, OPERATORE_ORGANIZZAZIONE OP, TIPOSTATOORGANIZZAZIONE TSO" & _
'            " WHERE OP.IDOPERATORE = " & idOperatoreSelezionato & _
'            " AND ORG.IDORGANIZZAZIONE = OP.IDORGANIZZAZIONE" & _
'            " AND (OP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR OP.IDTIPOSTATORECORD IS NULL)" & _
'            " AND ORG.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
'            " ORDER BY ORG"
'    Else
        sql = "SELECT ORG.IDORGANIZZAZIONE ID," & _
            " ORG.NOME || ' - ' || TSO.NOME ORG, ORG.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO" & _
            " FROM ORGANIZZAZIONE ORG, OPERATORE_ORGANIZZAZIONE OP, TIPOSTATOORGANIZZAZIONE TSO" & _
            " WHERE OP.IDOPERATORE = " & idOperatoreSelezionato & _
            " AND ORG.IDORGANIZZAZIONE = OP.IDORGANIZZAZIONE" & _
            " AND ORG.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
            " ORDER BY ORG"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            Set dirittiOperatore = New clsDiritOperOrg
            Call dirittiOperatore.Init
            organizzazioneCorrente.nomeElementoLista = rec("ORG")
            organizzazioneCorrente.descrizioneElementoLista = rec("ORG")
            organizzazioneCorrente.idElementoLista = rec("ID").Value
            organizzazioneCorrente.idAttributoElementoLista = rec("TIPOSTATO").Value
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            dirittiOperatore.idOrganizzazione = organizzazioneCorrente.idElementoLista
            Call listaOrganizzazioniSelezionate.Add(organizzazioneCorrente, chiaveOrganizzazione)
            Call listaDirittiOperatoreSuOrganizzazione.Add(dirittiOperatore, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOrganizzazioniSelezionate_Init
    
End Sub

Private Sub lstOrganizzazioniDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    Call lstOrganizzazioniDisponibili.Clear

    If Not (listaOrganizzazioniDisponibili Is Nothing) Then
        i = 1
        For Each organizzazione In listaOrganizzazioniDisponibili
            lstOrganizzazioniDisponibili.AddItem organizzazione.descrizioneElementoLista
            lstOrganizzazioniDisponibili.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOrganizzazioniSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    Call lstOrganizzazioniSelezionate.Clear

    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        i = 1
        For Each organizzazione In listaOrganizzazioniSelezionate
            lstOrganizzazioniSelezionate.AddItem organizzazione.descrizioneElementoLista
            lstOrganizzazioniSelezionate.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSvuotaOrgDisponibili_Click()
    Call SvuotaOrganizzazioniDisponibili
'    faseConfigurazioneOperatoriControllati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaOrganizzazioniDisponibili()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaOrganizzazioniDisponibili
        isConfigurabile = True
        Select Case organizzazione.idAttributoElementoLista
            Case TSO_IN_CONFIGURAZIONE
                isConfigurabile = True
            Case TSO_ATTIVA
                Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            Case TSO_ATTIVABILE
                isConfigurabile = False
        End Select
        If isConfigurabile Then
            chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
            Call listaOrganizzazioniSelezionate.Add(organizzazione, chiaveOrganizzazione)
            Call listaOrganizzazioniDisponibili.Remove(chiaveOrganizzazione)
        End If
    Next organizzazione
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub SpostaInLstOrganizzazioniSelezionate_old()
    Dim i As Integer
    Dim isConfigurabile As Boolean
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    Dim dirittiOperatore As clsDiritOperOrg
    
    For i = 1 To lstOrganizzazioniDisponibili.ListCount
        If lstOrganizzazioniDisponibili.Selected(i - 1) Then
            isConfigurabile = True
            idOrganizzazione = lstOrganizzazioniDisponibili.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaOrganizzazioniDisponibili.Item(chiaveOrganizzazione)
            Select Case organizzazione.idAttributoElementoLista
                Case TSO_IN_CONFIGURAZIONE
                    isConfigurabile = True
                Case TSO_ATTIVA
                    Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                Case TSO_ATTIVABILE
                    isConfigurabile = False
            End Select
            If isConfigurabile Then
                Call CaricaFormDettagliDirittiOperatore(organizzazione.idElementoLista)
                If frmDettagliDirittiOperatore.GetExitCode = EC_CONFERMA Then
                    Set dirittiOperatore = frmDettagliDirittiOperatore.GetDirittiOperatore
                    Call listaOrganizzazioniSelezionate.Add(organizzazione, chiaveOrganizzazione)
                    Call listaDirittiOperatoreSuOrganizzazione.Add(dirittiOperatore, chiaveOrganizzazione)
                    Call listaOrganizzazioniDisponibili.Remove(chiaveOrganizzazione)
                End If
            End If
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub CaricaFormDettagliDirittiOperatore(idOrganizzazione As Long)
    Call frmDettagliDirittiOperatore.SetIdOrganizzazioneCorrente(idOrganizzazione)
    Call frmDettagliDirittiOperatore.Init
End Sub

Private Sub SpostaInLstOrganizzazioniDisponibili()
    Dim i As Integer
    Dim j As Integer
    Dim isConfigurabile As Boolean
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    Dim dirittiOperatore As clsDiritOperOrg
    
    For i = 1 To lstOrganizzazioniSelezionate.ListCount
        If lstOrganizzazioniSelezionate.Selected(i - 1) Then
            isConfigurabile = True
            idOrganizzazione = lstOrganizzazioniSelezionate.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaOrganizzazioniSelezionate.Item(chiaveOrganizzazione)
            
            Call listaDirittiOperatoreSuOrganizzazione.Remove(chiaveOrganizzazione)
            Set dirittiOperatore = New clsDiritOperOrg
            Call dirittiOperatore.Init
            dirittiOperatore.aggiorna = True
            dirittiOperatore.idOrganizzazione = organizzazione.idElementoLista
            Call listaDirittiOperatoreSuOrganizzazione.Add(dirittiOperatore, chiaveOrganizzazione)
            
            Select Case organizzazione.idAttributoElementoLista
                Case TSO_IN_CONFIGURAZIONE
                    isConfigurabile = True
                Case TSO_ATTIVA
                    Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                Case TSO_ATTIVABILE
                    isConfigurabile = False
            End Select
            If isConfigurabile Then
                Call listaOrganizzazioniDisponibili.Add(organizzazione, chiaveOrganizzazione)
                Call listaOrganizzazioniSelezionate.Remove(chiaveOrganizzazione)
'                Call RimuoviDirittiOperatore(chiaveOrganizzazione)
            End If
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub RimuoviDirittiOperatore(chiaveOrganizzazione As String)
On Error Resume Next
    Call listaDirittiOperatoreSuOrganizzazione.Remove(chiaveOrganizzazione)
End Sub

Private Sub SvuotaOrganizzazioniSelezionate()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaOrganizzazioniSelezionate
        isConfigurabile = True
        Select Case organizzazione.idAttributoElementoLista
            Case TSO_IN_CONFIGURAZIONE
                isConfigurabile = True
            Case TSO_ATTIVA
                Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            Case TSO_ATTIVABILE
                isConfigurabile = False
        End Select
        If isConfigurabile Then
            chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
            Call listaOrganizzazioniDisponibili.Add(organizzazione, chiaveOrganizzazione)
            Call listaOrganizzazioniSelezionate.Remove(chiaveOrganizzazione)
            Call RimuoviDirittiOperatore(chiaveOrganizzazione)
        End If
    Next organizzazione
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Function DirittiOperatoreSuOrganizzazione(idOrganizzazione As Long) As clsDiritOperOrg
On Error GoTo gestioneErrori

    Set DirittiOperatoreSuOrganizzazione = listaDirittiOperatoreSuOrganizzazione.Item(ChiaveId(idOrganizzazione))
    
    Exit Function
    
gestioneErrori:
    Set DirittiOperatoreSuOrganizzazione = Nothing
End Function

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim numeroVociLista As Long
    Dim numeroRecordInseritiSimultaneamente As Integer
    Dim posizione As Long
    Dim operatore As clsElementoLista
    Dim organizzazione As clsElementoLista
    Dim condizioneSql As String
    Dim sqlProd As String
    Dim sqlTar As String
    Dim sqlTipoOp As String
    Dim sql0 As String
    Dim sql1 As String
    Dim dirittiOperatore As clsDiritOperOrg

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    Call SETAConnection.BeginTrans

'   AGGIORNAMENTO TABELLA OPERATORE
    sql = "UPDATE OPERATORE SET"
    sql = sql & " USERNAME = " & SqlStringValue(userName) & ","
'    sql = sql & " CRYPTO_HASH = SETA.GET_HASH(" & SqlStringValue(passWord) & "),"
    sql = sql & " EMAIL = " & SqlStringValue(eMail) & ","
    sql = sql & " ABILITATO = " & operatoreAbilitato & ","
    sql = sql & " IVAPREASSOLTAOBBLIGATORIA = " & operatoreAIVAPreassoltaObbligatoria & ","
    If idPuntoVenditaElettivo = idNessunElementoSelezionato Then
        sql = sql & " IDPUNTOVENDITAELETTIVO = NULL,"
    Else
        sql = sql & " IDPUNTOVENDITAELETTIVO = " & idPuntoVenditaElettivo & ","
    End If
    If idTipoOperativita = idNessunElementoSelezionato Then
        sql = sql & " IDTIPOOPERATIVITACLIENT = NULL"
    Else
        sql = sql & " IDTIPOOPERATIVITACLIENT = " & idTipoOperativita
    End If
    sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
    SETAConnection.Execute sql, n, adCmdText

'   AGGIORNAMENTO TABELLA OPERATORE_ORGANIZZAZIONE
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        sql = "DELETE FROM OPERATORE_ORGANIZZAZIONE"
        sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        If listaOrganizzazioniSelezionate.count > 0 Then
            sql = "INSERT INTO OPERATORE_ORGANIZZAZIONE (IDOPERATORE, IDORGANIZZAZIONE)" & _
                " SELECT " & idOperatoreSelezionato & "," & _
                " IDORGANIZZAZIONE FROM ORGANIZZAZIONE WHERE IDORGANIZZAZIONE IN ("
            For i = 1 To listaOrganizzazioniSelezionate.count - 1
                Set organizzazione = listaOrganizzazioniSelezionate.Item(i)
                sql = sql & organizzazione.idElementoLista & ","
            Next i
            Set organizzazione = listaOrganizzazioniSelezionate.Item(i)
            sql = sql & organizzazione.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
    End If

'   AGGIORNAMENTO TABELLE DIRITTI OPERATORE SU PRODOTTI
    sqlProd = ""
    sqlTar = ""
    sqlTipoOp = ""
    If Not (listaDirittiOperatoreSuOrganizzazione Is Nothing) Then
        For Each dirittiOperatore In listaDirittiOperatoreSuOrganizzazione
            If dirittiOperatore.aggiorna Then
                sql = " DELETE FROM OPERATORE_PRODOTTO"
                sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
                sql = sql & " AND IDPRODOTTO IN"
                sql = sql & " ("
                sql = sql & " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & dirittiOperatore.idOrganizzazione
                sql = sql & ")"
                SETAConnection.Execute sql, n, adCmdText
                sql = " DELETE FROM OPERATORE_CAUSALEPROTEZIONE"
                sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
                sql = sql & " AND IDPRODOTTO IN"
                sql = sql & " ("
                sql = sql & " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & dirittiOperatore.idOrganizzazione
                sql = sql & ")"
                SETAConnection.Execute sql, n, adCmdText
                sql = " DELETE FROM OPERATORE_TARIFFA"
                sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
                sql = sql & " AND IDTARIFFA IN"
                sql = sql & " ("
                sql = sql & " SELECT DISTINCT IDTARIFFA"
                sql = sql & " FROM TARIFFA"
                sql = sql & " WHERE IDPRODOTTO IN"
                sql = sql & " ("
                sql = sql & " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & dirittiOperatore.idOrganizzazione
                sql = sql & " )"
                sql = sql & " )"
                SETAConnection.Execute sql, n, adCmdText
                    
' QUESTA QUERY RIMANE COMMENTATA A IMPERITURO BIASIMO DEL GRUPPO DI LAVORO
'                    sql = " DELETE FROM OPERATORE_TIPOOPERAZIONE"
'                    sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
'                    sql = sql & " AND IDTIPOOPERAZIONE IN"
'                    sql = sql & " ("
'                    sql = sql & " SELECT DISTINCT IDTIPOOPERAZIONE"
'                    sql = sql & " FROM OPERATORE_TIPOOPERAZIONE"
'                    sql = sql & " WHERE IDPRODOTTO IN"
'                    sql = sql & " ("
'                    sql = sql & " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & dirittiOperatore.idOrganizzazione
'                    sql = sql & " )"
'                    sql = sql & " )"
'                    SETAConnection.Execute sql, n, adCmdText
                    
                sql = " DELETE FROM OPERATORE_TIPOOPERAZIONE"
                sql = sql & " WHERE IDOPERATORE = " & idOperatoreSelezionato
                sql = sql & " AND IDPRODOTTO IN"
                sql = sql & " ("
                sql = sql & " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & dirittiOperatore.idOrganizzazione
                sql = sql & " )"
                SETAConnection.Execute sql, n, adCmdText
            End If

            condizioneSql = ""
            If listaOrganizzazioniSelezionate.count > 0 Then
                If dirittiOperatore.aggiorna Then
                    sql = " INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)"
                    sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & ", IDPRODOTTO"
                    sql = sql & " FROM PRODOTTO"
                    sql = sql & " WHERE IDPRODOTTO IN"
                    sql = sql & " ("
                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                    sql = sql & " )"
                    SETAConnection.Execute sql, n, adCmdText
                    If dirittiOperatore.selezionaDirittiDaOperatoreMaster Then
                        sql = " INSERT INTO OPERATORE_CAUSALEPROTEZIONE ("
                        sql = sql & " IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
                        sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & ", OC.IDCAUSALEPROTEZIONE, OC.IDPRODOTTO"
                        sql = sql & " FROM OPERATORE_CAUSALEPROTEZIONE OC, CAUSALEPROTEZIONE C"
                        sql = sql & " WHERE C.IDCAUSALEPROTEZIONE = OC.IDCAUSALEPROTEZIONE"
                        sql = sql & " AND OC.IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        sql = sql & " AND OC.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
                        sql = sql & " AND C.IDOPERATORE IS NULL"
                        SETAConnection.Execute sql, n, adCmdText
                        sql = " INSERT INTO OPERATORE_TARIFFA ("
                        sql = sql & " IDOPERATORE, IDTARIFFA)"
                        sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & ", OT.IDTARIFFA"
                        sql = sql & " FROM TARIFFA T, OPERATORE_TARIFFA OT"
                        sql = sql & " WHERE T.IDTARIFFA = OT.IDTARIFFA"
                        sql = sql & " AND OT.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
                        sql = sql & " AND IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        SETAConnection.Execute sql, n, adCmdText
                        sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
                        sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO) "
                        sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & ", IDTIPOOPERAZIONE, IDPRODOTTO"
                        sql = sql & " FROM OPERATORE_TIPOOPERAZIONE OT"
                        sql = sql & " WHERE OT.IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        sql = sql & " AND OT.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
                        SETAConnection.Execute sql, n, adCmdText
                    ElseIf dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto Then
                        If dirittiOperatore.importaDirittiSuTutteLeProtezioni = VB_VERO Then
                            sql = " INSERT INTO OPERATORE_CAUSALEPROTEZIONE ("
                            sql = sql & " IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
                            sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & ", IDCAUSALEPROTEZIONE, IDPRODOTTO"
                            sql = sql & " FROM CAUSALEPROTEZIONE C, PRODOTTO P"
                            sql = sql & " WHERE C.IDORGANIZZAZIONE = " & dirittiOperatore.idOrganizzazione
                            sql = sql & " AND IDPRODOTTO IN"
                            sql = sql & " ("
                            sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                            sql = sql & " )"
                            sql = sql & " AND C.IDOPERATORE IS NULL"
                            SETAConnection.Execute sql, n, adCmdText
                        End If
                        If dirittiOperatore.importaDirittiSuTutteLeTariffe = VB_VERO Then
                            sql = " INSERT INTO OPERATORE_TARIFFA ("
                            sql = sql & " IDOPERATORE, IDTARIFFA)"
                            sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & ", IDTARIFFA"
                            sql = sql & " FROM TARIFFA T"
                            sql = sql & " WHERE IDPRODOTTO IN"
                            sql = sql & " ("
                            sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                            sql = sql & " )"
                            SETAConnection.Execute sql, n, adCmdText
                        End If
                        If dirittiOperatore.importaDirittiSuTutteLeOperazioni Then
                            sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
                            sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO)"
                            sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & " IDOPERATORE, "
                            sql = sql & " IDTIPOOPERAZIONE, IDPRODOTTO"
                            sql = sql & " FROM PRODOTTO P, TIPOOPERAZIONE T"
                            sql = sql & " WHERE IDPRODOTTO IN"
                            sql = sql & " ("
                            sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                            sql = sql & " )"
                            sql = sql & " AND CONFIGURABILEPEROPERATORE = " & VB_VERO
                            SETAConnection.Execute sql, n, adCmdText
                        ElseIf dirittiOperatore.importaDirittiSuSetMinimoOperazioni Then
                            sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
                            sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO)"
                            sql = sql & " SELECT DISTINCT " & idOperatoreSelezionato & " IDOPERATORE, "
                            sql = sql & " IDTIPOOPERAZIONE, IDPRODOTTO"
                            sql = sql & " FROM PRODOTTO P, TIPOOPERAZIONE T"
                            sql = sql & " WHERE IDPRODOTTO IN"
                            sql = sql & " ("
                            sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                            sql = sql & " )"
                            sql = sql & " AND IDTIPOOPERAZIONE IN"
                            sql = sql & " ("
                            sql = sql & TO_VENDITA & ", " & TO_STAMPA & ", " & TO_ANNULLAMENTO_VENDITA & ", " & TO_ANNULLAMENTO_STAMPA
                            sql = sql & " )"
                            SETAConnection.Execute sql, n, adCmdText
                        End If
                    End If
                End If
            End If
        Next dirittiOperatore
    End If

    Call SETAConnection.CommitTrans
    Call ChiudiConnessioneBD

    Exit Sub

gestioneErrori:
    Call SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub SpostaInLstOrganizzazioniSelezionate()
    Dim i As Integer
    Dim isConfigurabile As Boolean
    Dim idOrganizzazione As Long
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    Dim dirittiOperatore As clsDiritOperOrg
    
    For i = 1 To lstOrganizzazioniDisponibili.ListCount
        If lstOrganizzazioniDisponibili.Selected(i - 1) Then
            isConfigurabile = True
            idOrganizzazione = lstOrganizzazioniDisponibili.ItemData(i - 1)
            chiaveOrganizzazione = ChiaveId(idOrganizzazione)
            Set organizzazione = listaOrganizzazioniDisponibili.Item(chiaveOrganizzazione)
            Select Case organizzazione.idAttributoElementoLista
                Case TSO_IN_CONFIGURAZIONE
                    isConfigurabile = True
                Case TSO_ATTIVA
                    Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                Case TSO_ATTIVABILE
                    isConfigurabile = False
            End Select
            If isConfigurabile Then
                Call CaricaFormDettagliDirittiOperatore(organizzazione.idElementoLista)
                If frmDettagliDirittiOperatore.GetExitCode = EC_CONFERMA Then
                    Set dirittiOperatore = frmDettagliDirittiOperatore.GetDirittiOperatore
                    Call RimuoviDirittiOperatore(chiaveOrganizzazione)
                    Call listaDirittiOperatoreSuOrganizzazione.Add(dirittiOperatore, chiaveOrganizzazione)
                End If
                Call listaOrganizzazioniSelezionate.Add(organizzazione, chiaveOrganizzazione)
                Call listaOrganizzazioniDisponibili.Remove(chiaveOrganizzazione)
            End If
        End If
    Next i
    Call lstOrganizzazioniDisponibili_Init
    Call lstOrganizzazioniSelezionate_Init
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim idNuovoOperatore As Long
    Dim n As Long
    Dim organizzazione As clsElementoLista
    Dim dirittiOperatore As clsDiritOperOrg
    Const STATO_CASSA_CHIUSA As Integer = 2
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovoOperatore = OttieniIdentificatoreDaSequenza("SQ_OPERATORE")
    sql = "INSERT INTO OPERATORE (IDOPERATORE, USERNAME, CRYPTO_HASH, ABILITATO, IVAPREASSOLTAOBBLIGATORIA,"
    sql = sql & " IDPUNTOVENDITA, DATAORAULTIMAAPERTURACASSA, DATAORAULTIMACHIUSURACASSA,"
    sql = sql & " IMPORTOAPERTURACASSA, IDTIPOSTATOCASSA, IDPERSONA, IDPUNTOVENDITAELETTIVO, IDTIPOOPERATIVITACLIENT, EMAIL)"
    sql = sql & " VALUES ("
    sql = sql & idNuovoOperatore & ", "
    sql = sql & SqlStringValue(userName) & ", "
    sql = sql & "SETA.GET_HASH(" & SqlStringValue(passWord) & "), "
    sql = sql & operatoreAbilitato & ", "
    sql = sql & operatoreAIVAPreassoltaObbligatoria & ", "
    sql = sql & "NULL, "
    sql = sql & "NULL, "
    sql = sql & "NULL, "
    sql = sql & "NULL, "
    sql = sql & STATO_CASSA_CHIUSA & ", "
    sql = sql & "NULL, "
    If idPuntoVenditaElettivo = idNessunElementoSelezionato Then
        sql = sql & "NULL, "
    Else
        sql = sql & idPuntoVenditaElettivo & ","
    End If
    If idTipoOperativita = idNessunElementoSelezionato Then
        sql = sql & "NULL, "
    Else
        sql = sql & idTipoOperativita & ", "
    End If
    sql = sql & SqlStringValue(eMail) & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        For Each organizzazione In listaOrganizzazioniSelezionate
            sql = "INSERT INTO OPERATORE_ORGANIZZAZIONE (IDOPERATORE, IDORGANIZZAZIONE)" & _
                " VALUES (" & _
                idNuovoOperatore & ", " & _
                organizzazione.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
            Set dirittiOperatore = DirittiOperatoreSuOrganizzazione(organizzazione.idElementoLista)
            If Not (dirittiOperatore Is Nothing) Then
                sql = " INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)"
                sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDPRODOTTO"
                sql = sql & " FROM PRODOTTO"
                sql = sql & " WHERE IDPRODOTTO IN"
                sql = sql & " ("
                sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                sql = sql & " )"
                SETAConnection.Execute sql, n, adCmdText
                If dirittiOperatore.selezionaDirittiDaOperatoreMaster Then
                    sql = " INSERT INTO OPERATORE_CAUSALEPROTEZIONE ("
                    sql = sql & " IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", OC.IDCAUSALEPROTEZIONE, OC.IDPRODOTTO"
                    sql = sql & " FROM OPERATORE_CAUSALEPROTEZIONE OC, CAUSALEPROTEZIONE C"
                    sql = sql & " WHERE C.IDCAUSALEPROTEZIONE = OC.IDCAUSALEPROTEZIONE"
                    sql = sql & " AND OC.IDPRODOTTO IN"
                    sql = sql & " ("
                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                    sql = sql & " )"
                    sql = sql & " AND OC.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
                    sql = sql & " AND C.IDOPERATORE IS NULL"
                    SETAConnection.Execute sql, n, adCmdText
                    sql = " INSERT INTO OPERATORE_TARIFFA ("
                    sql = sql & " IDOPERATORE, IDTARIFFA)"
                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", OT.IDTARIFFA"
                    sql = sql & " FROM TARIFFA T, OPERATORE_TARIFFA OT"
                    sql = sql & " WHERE T.IDTARIFFA = OT.IDTARIFFA"
                    sql = sql & " AND OT.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
                    sql = sql & " AND IDPRODOTTO IN"
                    sql = sql & " ("
                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                    sql = sql & " )"
                    SETAConnection.Execute sql, n, adCmdText
                    sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
                    sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO) "
                    sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDTIPOOPERAZIONE, IDPRODOTTO"
                    sql = sql & " FROM OPERATORE_TIPOOPERAZIONE OT"
                    sql = sql & " WHERE OT.IDPRODOTTO IN"
                    sql = sql & " ("
                    sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                    sql = sql & " )"
                    sql = sql & " AND OT.IDOPERATORE = " & dirittiOperatore.idOperatoreSelezionato
                    SETAConnection.Execute sql, n, adCmdText
                ElseIf dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto Then
                    If dirittiOperatore.importaDirittiSuTutteLeProtezioni = VB_VERO Then
                        sql = " INSERT INTO OPERATORE_CAUSALEPROTEZIONE ("
                        sql = sql & " IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
                        sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDCAUSALEPROTEZIONE, IDPRODOTTO"
                        sql = sql & " FROM CAUSALEPROTEZIONE C, PRODOTTO P"
                        sql = sql & " WHERE C.IDORGANIZZAZIONE = " & organizzazione.idElementoLista
                        sql = sql & " AND IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        sql = sql & " AND C.IDOPERATORE IS NULL"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                    If dirittiOperatore.importaDirittiSuTutteLeTariffe = VB_VERO Then
                        sql = " INSERT INTO OPERATORE_TARIFFA ("
                        sql = sql & " IDOPERATORE, IDTARIFFA)"
                        sql = sql & " SELECT DISTINCT " & idNuovoOperatore & ", IDTARIFFA"
                        sql = sql & " FROM TARIFFA T"
                        sql = sql & " WHERE IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                    If dirittiOperatore.importaDirittiSuTutteLeOperazioni Then
                        sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
                        sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO)"
                        sql = sql & " SELECT DISTINCT " & idNuovoOperatore & " IDOPERATORE, "
                        sql = sql & " IDTIPOOPERAZIONE, IDPRODOTTO"
                        sql = sql & " FROM PRODOTTO P, TIPOOPERAZIONE T"
                        sql = sql & " WHERE IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        sql = sql & " AND CONFIGURABILEPEROPERATORE = " & VB_VERO
                        SETAConnection.Execute sql, n, adCmdText
                    ElseIf dirittiOperatore.importaDirittiSuSetMinimoOperazioni Then
                        sql = " INSERT INTO OPERATORE_TIPOOPERAZIONE ("
                        sql = sql & " IDOPERATORE, IDTIPOOPERAZIONE, IDPRODOTTO)"
                        sql = sql & " SELECT DISTINCT " & idNuovoOperatore & " IDOPERATORE, "
                        sql = sql & " IDTIPOOPERAZIONE, IDPRODOTTO"
                        sql = sql & " FROM PRODOTTO P, TIPOOPERAZIONE T"
                        sql = sql & " WHERE IDPRODOTTO IN"
                        sql = sql & " ("
                        sql = sql & ElencoDaListaElementi(dirittiOperatore.listaProdottiSelezionati, CEL_ID_ELEMENTO_LISTA)
                        sql = sql & " )"
                        sql = sql & " AND IDTIPOOPERAZIONE IN"
                        sql = sql & " ("
                        sql = sql & TO_VENDITA & ", " & TO_STAMPA & ", " & TO_ANNULLAMENTO_VENDITA & ", " & TO_ANNULLAMENTO_STAMPA
                        sql = sql & " )"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                End If
            End If
        Next organizzazione
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call frmSceltaOperatore.SetIdRecordSelezionato(idNuovoOperatore)
    Call SetIdOperatoreSelezionato(idNuovoOperatore)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmbTipoOperativita_Click()
    If Not internalEvent Then
        idTipoOperativita = cmbTipoOperativita.ItemData(cmbTipoOperativita.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ResetPassword()
    Dim sql As String
    Dim rec As ADODB.Recordset
    Dim n As Long
    Dim risposta As VbMsgBoxResult
    Dim nuovaPassword As String
    Dim idNuovoMessaggioEMail As Long
    Dim testoItaliano As String
    Dim testoInglese As String

    If eMail = "" Then
        MsgBox ("Impossibile resettare la password: email operatore non configurata")
    Else
        risposta = MsgBox("Sei sicuro di resettare la password?", vbYesNo)
        If risposta = vbYes Then
            nuovaPassword = generaStringaCasuale(8)
            
            Call ApriConnessioneBD
            SETAConnection.BeginTrans
            
            sql = " UPDATE OPERATORE SET DATAULTIMAMODIFICAPASSWORD = SYSDATE, RESETPASSWORDINCORSO = 1, CRYPTO_HASH = SETA.GET_HASH('" & nuovaPassword & "')" & _
                    " WHERE IDOPERATORE = " & idOperatoreSelezionato
            SETAConnection.Execute sql, n, adCmdText
            If n <> 1 Then
                MsgBox ("Operatore non modificato")
                SETAConnection.RollbackTrans
            Else
                idNuovoMessaggioEMail = OttieniIdentificatoreDaSequenza("SQ_MESSAGGIOEMAIL")

                ' idtipomessaggioemail vale 17
                testoItaliano = "Le comunichiamo che e` stato effettuato il rest della password per l`operatore " & userName & ". La nuova password e`: " & nuovaPassword
                testoInglese = "-"
                sql = "INSERT INTO MESSAGGIOEMAIL (IDMESSAGGIOEMAIL, IDTIPOMESSAGGIOEMAIL, INDIRIZZOEMAILDESTINATARIO," & _
                        " TESTOITALIANO, TESTOINGLESE, DATAORAINSERIMENTO, DATAORAINVIO, ERRORE)" & _
                        " VALUES (" & idNuovoMessaggioEMail & ", 17, '" & eMail & "', '" & testoItaliano & "', '" & testoInglese & "', SYSDATE, NULL, NULL)"
                SETAConnection.Execute sql, n, adCmdText
                If n <> 1 Then
                    MsgBox ("Operatore non modificato")
                    SETAConnection.RollbackTrans
                Else
                    MsgBox ("Reset effettuato e email accodata per la spedizione")
                    SETAConnection.CommitTrans
                End If
            End If
        End If
    End If

End Sub
