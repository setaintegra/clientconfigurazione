VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAssocArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idArea As Long
Public nomeArea As String
Public codiceArea As String
Public labelArea As String
Public collTariffe As New Collection
Public collTipoTerm As New Collection
Public collTipiSupporto As New Collection

Public Function OttieniTariffaDaIdTariffa(idT As Long, ByRef newTariffa As clsAssocTariffa) As Boolean
    Dim t As clsAssocTariffa
    Dim iTariffa As Integer
    Dim trovato As Boolean
    
    iTariffa = 1
    trovato = False
    While iTariffa <= collTariffe.count And Not trovato
        Set t = collTariffe(iTariffa)
        If idT = t.idTariffa Then
            trovato = True
            Set newTariffa = t
        End If
        iTariffa = iTariffa + 1
    Wend
    If Not trovato Then
        Set newTariffa = New clsAssocTariffa
    End If
    OttieniTariffaDaIdTariffa = trovato
End Function

Public Function IsTariffaUtilizzataDaArea(idT As Long) As Boolean
    Dim t As clsAssocTariffa
    Dim iTariffa As Integer
    Dim trovato As Boolean
    
    iTariffa = 1
    trovato = False
    While iTariffa < collTariffe.count + 1 And Not trovato
        Set t = collTariffe(iTariffa)
        If idT = t.idTariffa Then
            trovato = True
        End If
        iTariffa = iTariffa + 1
    Wend
    IsTariffaUtilizzataDaArea = trovato
End Function

Public Sub EliminaTariffaDaArea(idT As Long)
    Dim t As clsAssocTariffa
    Dim i As Integer
    Dim index As Integer
    
    i = 1
    For Each t In collTariffe
        If t.idTariffa = idT Then
            index = i
            ' come si esce dai foreach?
        End If
        i = i + 1
    Next t
    Call collTariffe.Remove(index)
End Sub

Public Sub EliminaTipoTerminaleDaArea(idTT As Long)
    Dim t As clsAssocTipoTerm
    Dim i As Integer
    Dim index As Integer
    
    i = 1
    For Each t In collTipoTerm
        If t.idTipoTerm = idTT Then
            index = i
            ' come si esce dai foreach?
        End If
        i = i + 1
    Next t
    Call collTipoTerm.Remove(index)
End Sub

Public Sub EliminaTipoSupportoDaArea(idTS As Long)
    Dim s As clsAssocSupporto
    Dim i As Integer
    Dim index As Integer
    
    i = 1
    For Each s In collTipiSupporto
        If s.idTipoSupporto = idTS Then
            index = i
            ' come si esce dai foreach?
        End If
        i = i + 1
    Next s
    Call collTipiSupporto.Remove(index)
End Sub

Public Sub AggiungiTariffaAdArea(tariffa As clsAssocTariffa)
    Call collTariffe.Add(tariffa)
End Sub

Public Sub AggiungiTipoTerminaleAdArea(tipoTerminale As clsAssocTipoTerm)
    Call collTipoTerm.Add(tipoTerminale)
End Sub

Public Sub AggiungiTipoSupportoAdArea(tipoSupporto As clsAssocSupporto)
    Call collTipiSupporto.Add(tipoSupporto)
End Sub

Public Function OttieniTipoSupportoDaIdTipoSupporto(idTS As Long, ByRef newTipoSupporto As clsAssocSupporto) As Boolean
    Dim ts As clsAssocSupporto
    Dim iTipoSupporto As Integer
    Dim trovato As Boolean
    
    iTipoSupporto = 1
    trovato = False
'    While iTipoSupporto < collTipiSupporto.count + 1 And Not trovato
    While iTipoSupporto <= collTipiSupporto.count And Not trovato
        Set ts = collTipiSupporto(iTipoSupporto)
        If idTS = ts.idTipoSupporto Then
            trovato = True
            Set newTipoSupporto = ts
        End If
        iTipoSupporto = iTipoSupporto + 1
    Wend
    If Not trovato Then
        Set newTipoSupporto = New clsAssocSupporto
    End If
    OttieniTipoSupportoDaIdTipoSupporto = trovato
End Function

Public Function OttieniTipoTerminaleDaIdTipoTerminale(idTT As Long, ByRef newTipoTerminale As clsAssocTipoTerm) As Boolean
    Dim tt As clsAssocTipoTerm
    Dim iTipoTerminale As Integer
    Dim trovato As Boolean
    
    iTipoTerminale = 1
    trovato = False
    While iTipoTerminale <= collTipoTerm.count And Not trovato
        Set tt = collTipoTerm(iTipoTerminale)
        If idTT = tt.idTipoTerm Then
            trovato = True
            Set newTipoTerminale = tt
        End If
        iTipoTerminale = iTipoTerminale + 1
    Wend
    If Not trovato Then
        Set newTipoTerminale = New clsAssocTipoTerm
    End If
    OttieniTipoTerminaleDaIdTipoTerminale = trovato
End Function

Public Function Clona() As clsAssocArea
    Dim b As clsAssocArea
    Dim i As Integer
'    Dim t As clsAssocTariffa
'    Dim tt As clsAssocTipoTerm
'    Dim ts As clsAssocSupporto

    Set b = New clsAssocArea

    b.idArea = idArea
    b.codiceArea = codiceArea
    b.nomeArea = nomeArea
    b.labelArea = labelArea
    For i = 1 To collTariffe.count
        Call b.collTariffe.Add(collTariffe(i).Clona)
    Next i
    For i = 1 To collTipoTerm.count
        Call b.collTipoTerm.Add(collTipoTerm(i).Clona)
    Next i
    For i = 1 To collTipiSupporto.count
        Call b.collTipiSupporto.Add(collTipiSupporto(i).Clona)
    Next i
    Set Clona = b
End Function

Public Sub inizializza(idProd As Long, idA As Long)
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim sql2 As String
    Dim rec2 As New ADODB.Recordset
    Dim area As clsAssocArea
    Dim tariffa As clsAssocTariffa
    Dim tipoSupporto As clsAssocSupporto
    Dim tipoTerm As clsAssocTipoTerm
    
'    Set collAree = New Collection
'    sql1 = "SELECT DISTINCT A.IDAREA, A.NOME NOMEAREA, A.CODICE CODAREA" & _
'        " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T, AREA A WHERE" & _
'        " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & _
'        " (ULS.IDAREA = " & idA & ") AND" & _
'        " (T.IDPRODOTTO = " & idProd & ")"
    sql1 = "SELECT * FROM AREA WHERE IDAREA = " & idA
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.EOF And rec1.BOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
'            Set area = New clsAssocArea
            idArea = rec1("IDAREA")
            codiceArea = rec1("CODICE")
            nomeArea = rec1("NOME")
            Set collTariffe = New Collection
            Set collTipoTerm = New Collection
            Set collTipiSupporto = New Collection
            'tariffa
            sql2 = "SELECT DISTINCT T.IDTARIFFA, T.NOME NOMETARIFFA, T.CODICE CODTARIFFA" & _
                " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T WHERE" & _
                " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & _
                " (T.IDPRODOTTO = " & idProd & ") AND" & _
                " (ULS.IDAREA = " & idArea & ")"
            rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
            If Not (rec2.BOF And rec2.EOF) Then
                rec2.MoveFirst
                While Not rec2.EOF
                    Set tariffa = New clsAssocTariffa
                    tariffa.idTariffa = rec2("IDTARIFFA")
'                    tariffa.nomeTariffa = rec2("NOMETARIFFA")
'                    tariffa.codiceTariffa = rec2("CODTARIFFA")
'                    tariffa.labelTariffa = tariffa.codiceTariffa & " - " & tariffa.nomeTariffa
                    tariffa.labelTariffa = CStr(rec2("CODTARIFFA")) & " - " & CStr(rec2("NOMETARIFFA"))
                    Call collTariffe.Add(tariffa)
                    rec2.MoveNext
                Wend
            End If
            rec2.Close
            If collTariffe.count <> 0 Then
'                'tipo terminale
'                sql2 = "SELECT DISTINCT ULS.IDTIPOTERMINALE IDTS, TT.NOME NOMETT" & _
'                    " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, TIPOTERMINALE TT WHERE" & _
'                    " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & _
'                    " (ULS.IDTIPOTERMINALE = TT.IDTIPOTERMINALE) AND" & _
'                    " (T.IDPRODOTTO = " & idProd & ") AND" & _
'                    " (ULS.IDAREA = " & idArea & ") AND" & _
'                    " (ULS.IDTARIFFA = " & tariffa.idTariffa & ")"
'                rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
'                If Not (rec2.BOF And rec2.EOF) Then
'                    rec2.MoveFirst
'                    While Not rec2.EOF
'                        Set tipoTerm = New clsAssocTipoTerm
'                        tipoTerm.idTipoTerm = rec2("IDTS")
'                        tipoTerm.labelTipoTerm = rec2("NOMETT")
'                        Call collTipoTerm.Add(tipoTerm)
'                        rec2.MoveNext
'                    Wend
'                End If
'                rec2.Close
                'tipo supporto e layout supporto
                sql2 = "SELECT ULS.IDTIPOSUPPORTO IDTS, ULS.IDLAYOUTSUPPORTO IDLS," & _
                    " TS.NOME NOMETS, TS.CODICE CODTS," & _
                    " LS.NOME NOMELS, LS.CODICE CODLS" & _
                    " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, TIPOSUPPORTO TS, LAYOUTSUPPORTO LS WHERE" & _
                    " (ULS.IDTARIFFA = T.IDTARIFFA) AND" & _
                    " (ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO) AND" & _
                    " (ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO) AND" & _
                    " (T.IDPRODOTTO = " & idProd & ") AND" & _
                    " (ULS.IDAREA = " & idArea & ") AND" & _
                    " (ULS.IDTARIFFA = " & tariffa.idTariffa & ")"
                rec2.Open sql2, SETAConnection, adOpenDynamic, adLockOptimistic
                If Not (rec2.BOF And rec2.EOF) Then
                    rec2.MoveFirst
                    While Not rec2.EOF
                        Set tipoSupporto = New clsAssocSupporto
                        tipoSupporto.idTipoSupporto = rec2("IDTS")
                        tipoSupporto.idLayoutSupporto = rec2("IDLS")
    '                    tipoSupporto.codiceTipoSupporto = rec2("CODTS")
    '                    tipoSupporto.codiceLayoutSupporto = rec2("CODLS")
    '                    tipoSupporto.nomeTipoSupporto = rec2("NOMETS")
    '                    tipoSupporto.nomeLayoutSupporto = rec2("NOMELS")
    '                    tipoSupporto.labelTipoSupporto = tipoSupporto.codiceTipoSupporto & " - " & tipoSupporto.nomeTipoSupporto
    '                    tipoSupporto.labelLayoutSupporto = tipoSupporto.codiceLayoutSupporto & " - " & tipoSupporto.nomeLayoutSupporto
                        tipoSupporto.labelTipoSupporto = CStr(rec2("CODTS")) & " - " & CStr(rec2("NOMETS"))
                        tipoSupporto.labelLayoutSupporto = CStr(rec2("CODLS")) & " - " & CStr(rec2("NOMELS"))
                        Call collTipiSupporto.Add(tipoSupporto)
                        rec2.MoveNext
                    Wend
                End If
                rec2.Close
            End If
'            Call collAree.Add(area)
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
End Sub




