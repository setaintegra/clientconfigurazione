VERSION 5.00
Begin VB.Form frmInizialeSito 
   Caption         =   "Sito"
   ClientHeight    =   6840
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9915
   LinkTopic       =   "Form1"
   ScaleHeight     =   6840
   ScaleWidth      =   9915
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   2175
      Left            =   0
      TabIndex        =   8
      Top             =   420
      Width           =   9855
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1380
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   840
         Width           =   7635
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1380
         MaxLength       =   30
         TabIndex        =   9
         Top             =   420
         Width           =   3255
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   180
         TabIndex        =   11
         Top             =   480
         Width           =   1035
      End
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   0
      TabIndex        =   5
      Top             =   5760
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstVenDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Left            =   0
      MultiSelect     =   2  'Extended
      TabIndex        =   4
      Top             =   3240
      Width           =   4275
   End
   Begin VB.CommandButton cmdVenSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   4380
      TabIndex        =   3
      Top             =   4320
      Width           =   435
   End
   Begin VB.CommandButton cmdVenDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   4380
      TabIndex        =   2
      Top             =   3780
      Width           =   435
   End
   Begin VB.CommandButton cmdVenSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   4380
      TabIndex        =   1
      Top             =   3240
      Width           =   435
   End
   Begin VB.ListBox lstVenSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Left            =   4860
      MultiSelect     =   2  'Extended
      TabIndex        =   0
      Top             =   3240
      Width           =   4275
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   5655
   End
   Begin VB.Label lblVenSelezionati 
      Alignment       =   2  'Center
      Caption         =   "lblVenSelezionati"
      Height          =   195
      Left            =   4800
      TabIndex        =   15
      Top             =   2940
      Width           =   2475
   End
   Begin VB.Label lblVenDisponibili 
      Alignment       =   2  'Center
      Caption         =   "lblVenDisponibili"
      Height          =   195
      Left            =   1500
      TabIndex        =   14
      Top             =   2940
      Width           =   2475
   End
   Begin VB.Label lblVenue 
      Alignment       =   2  'Center
      Caption         =   "lblVenue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1440
      TabIndex        =   13
      Top             =   2700
      Width           =   5475
   End
End
Attribute VB_Name = "frmInizialeSito"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idSitoSelezionato As Long
Private nomeSito As String
Private descrizioneSito As String
Private listaVenueSelezionati As Collection
Private listaVenueDisponibili As Collection

Private internalEvent As Boolean

Private modalit�FormCorrente As AzioneEnum
Private exitCodeFormDettagli As ExitCodeEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init()

    Call Variabili_Init
    Call AggiornaAbilitazioneControlli
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            idSitoSelezionato = idNessunElementoSelezionato
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriLstVenDisponibili
    Call CaricaValoriLstVenSelezionati
    Call AggiornaAbilitazioneControlli
    Call frmInizialeSito.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    nomeSito = ""
    descrizioneSito = ""
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdSitoSelezionato(id As Long)
    idSitoSelezionato = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Sito"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Sito configurato"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Sito configurato"
    End Select

    cmdConferma.Enabled = (Trim(txtNome.Text) <> "")
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstVenDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstVenSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdVenDidsponibile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdVenSelezionato.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdVenSvuotaSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenue.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenue.Caption = "VENUE"
    lblVenDisponibili.Caption = "Disponibili"
    lblVenSelezionati.Caption = "Selezionati"
        
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaSito.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = "SELECT NOME, DESCRIZIONE"
    sql = sql & " FROM SITO"
    sql = sql & " WHERE IDSITO = " & idSitoSelezionato
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        nomeSito = rec("NOME")
        descrizioneSito = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeSito
    txtDescrizione.Text = descrizioneSito
    
    internalEvent = internalEventOld
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim idNuovoSito As Long
    Dim venue As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
'   INSERIMENTO IN TABELLA PIANTA
    idNuovoSito = OttieniIdentificatoreDaSequenza("SQ_SITO")
    sql = "INSERT INTO SITO (IDSITO, NOME, DESCRIZIONE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovoSito & ", "
    sql = sql & SqlStringValue(nomeSito) & ", "
    sql = sql & SqlStringValue(descrizioneSito) & ")"
    n = ORADB.ExecuteSQL(sql)

'   INSERIMENTO IN TABELLA VENUE_SITO
    If Not (listaVenueSelezionati Is Nothing) Then
        For Each venue In listaVenueSelezionati
            sql = "INSERT INTO VENUE_SITO (IDVENUE, IDSITO)"
            sql = sql & " VALUES (" & venue.idElementoLista & ", "
            sql = sql & idNuovoSito & ")"
            n = ORADB.ExecuteSQL(sql)
        Next venue
    End If
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call frmSceltaSito.SetIdRecordSelezionato(idNuovoSito)
    Call SetIdSitoSelezionato(idNuovoSito)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim isModificabile As Boolean
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            If ValoriCampiOK = True Then
                Call InserisciNellaBaseDati
                Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                Call SetModalit�Form(A_MODIFICA)
                Call AggiornaAbilitazioneControlli
            End If
        Case A_MODIFICA
            If ValoriCampiOK = True Then
                Call AggiornaNellaBaseDati
                Call frmMessaggio.Visualizza("NotificaModificaDati")
                Call Esci
            End If
        Case A_ELIMINA
            Call EliminaDallaBaseDati
            Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
            Call Esci
    End Select
    Call frmSceltaPianta.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub cmdVenDidsponibile_Click()
    Call SpostaInLstVenDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdVenSelezionato_Click()
    Call SpostaInLstVenSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdVenSvuotaSelezionati_Click()
    Call SvuotaVenSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstVenDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstVenDisponibili, lstVenDisponibili.Text)
End Sub

Private Sub lstVenSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstVenSelezionati, lstVenSelezionati.Text)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim venue As clsElementoLista
    Dim organizzazione As clsElementoLista

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori

'   AGGIORNAMENTO IN TABELLA PIANTA
    sql = "UPDATE SITO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeSito) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneSito)
    sql = sql & " WHERE IDSITO = " & idSitoSelezionato
    n = ORADB.ExecuteSQL(sql)
    
'   AGGIORNAMENTO IN TABELLA VENUE_SITO
    If Not (listaVenueSelezionati Is Nothing) Then
        sql = "DELETE FROM VENUE_SITO WHERE IDSITO = " & idSitoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each venue In listaVenueSelezionati
            sql = "INSERT INTO VENUE_SITO (IDSITO, IDVENUE)"
            sql = sql & " VALUES (" & idSitoSelezionato & ", "
            sql = sql & venue.idElementoLista & ")"
            n = ORADB.ExecuteSQL(sql)
        Next venue
    End If
            
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM VENUE_SITO WHERE IDSITO = " & idSitoSelezionato
    n = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM SITO WHERE IDSITO = " & idSitoSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub CaricaValoriLstVenDisponibili()
    Dim sql As String
    Dim rec As OraDynaset
    Dim chiaveVenue As String
    Dim venueCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    Set listaVenueDisponibili = New Collection
    If idSitoSelezionato >= 0 Then
        sql = "SELECT V.IDVENUE ID, NOME FROM VENUE V, VENUE_SITO V_S"
        sql = sql & " WHERE V.IDVENUE = V_S.IDVENUE(+)"
        sql = sql & " AND V_S.IDVENUE IS NULL"
        sql = sql & " AND V_S.IDSITO(+) = " & idSitoSelezionato
        sql = sql & " ORDER BY NOME"
    Else
        sql = "SELECT IDVENUE ID, NOME FROM VENUE ORDER BY NOME"
    End If
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set venueCorrente = New clsElementoLista
            venueCorrente.idElementoLista = rec("ID").Value
            venueCorrente.nomeElementoLista = rec("NOME")
            venueCorrente.descrizioneElementoLista = rec("NOME")
            chiaveVenue = ChiaveId(venueCorrente.idElementoLista)
            Call listaVenueDisponibili.Add(venueCorrente, chiaveVenue)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call lstVenDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstVenSelezionati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim chiaveVenue As String
    Dim venueCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    Set listaVenueSelezionati = New Collection
    If idSitoSelezionato >= 0 Then
        sql = "SELECT VS.IDVENUE ID, NOME"
        sql = sql & " FROM VENUE V, VENUE_SITO VS"
        sql = sql & " WHERE VS.IDSITO = " & idSitoSelezionato
        sql = sql & " AND VS.IDVENUE = V.IDVENUE"
        sql = sql & " ORDER BY NOME"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set venueCorrente = New clsElementoLista
                venueCorrente.idElementoLista = rec("ID").Value
                venueCorrente.nomeElementoLista = rec("NOME")
                venueCorrente.descrizioneElementoLista = rec("NOME")
                chiaveVenue = ChiaveId(venueCorrente.idElementoLista)
                Call listaVenueSelezionati.Add(venueCorrente, chiaveVenue)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ORADB.CommitTrans
        
        Call ChiudiConnessioneBD_ORA
        
        Call lstVenSelezionati_Init
    End If
    
End Sub

Private Sub lstVenDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim venue As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstVenDisponibili.Clear

    If Not (listaVenueDisponibili Is Nothing) Then
        i = 1
        For Each venue In listaVenueDisponibili
            lstVenDisponibili.AddItem venue.descrizioneElementoLista
            lstVenDisponibili.ItemData(i - 1) = venue.idElementoLista
            i = i + 1
        Next venue
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstVenSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim venue As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstVenSelezionati.Clear

    If Not (listaVenueSelezionati Is Nothing) Then
        i = 1
        For Each venue In listaVenueSelezionati
            lstVenSelezionati.AddItem venue.descrizioneElementoLista
            lstVenSelezionati.ItemData(i - 1) = venue.idElementoLista
            i = i + 1
        Next venue
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaInLstVenDisponibili()
    Dim i As Integer
    Dim idVenue As Long
    Dim venue As clsElementoLista
    Dim chiaveVenue As String
    
    For i = 1 To lstVenSelezionati.ListCount
        If lstVenSelezionati.Selected(i - 1) Then
            idVenue = lstVenSelezionati.ItemData(i - 1)
            chiaveVenue = ChiaveId(idVenue)
            Set venue = listaVenueSelezionati.Item(chiaveVenue)
            Call listaVenueDisponibili.Add(venue, chiaveVenue)
            Call listaVenueSelezionati.Remove(chiaveVenue)
        End If
    Next i
    Call lstVenDisponibili_Init
    Call lstVenSelezionati_Init
End Sub

Private Sub SpostaInLstVenSelezionati()
    Dim i As Integer
    Dim idVenue As Long
    Dim venue As clsElementoLista
    Dim chiaveVenue As String
    
    For i = 1 To lstVenDisponibili.ListCount
        If lstVenDisponibili.Selected(i - 1) Then
            idVenue = lstVenDisponibili.ItemData(i - 1)
            chiaveVenue = ChiaveId(idVenue)
            Set venue = listaVenueDisponibili.Item(chiaveVenue)
            Call listaVenueSelezionati.Add(venue, chiaveVenue)
            Call listaVenueDisponibili.Remove(chiaveVenue)
        End If
    Next i
    Call lstVenDisponibili_Init
    Call lstVenSelezionati_Init
End Sub

Private Sub SvuotaVenSelezionati()
    Dim venue As clsElementoLista
    Dim chiaveVenue As String
    
    For Each venue In listaVenueSelezionati
        chiaveVenue = ChiaveId(venue.idElementoLista)
        Call listaVenueDisponibili.Add(venue, chiaveVenue)
    Next venue
    Set listaVenueSelezionati = Nothing
    Set listaVenueSelezionati = New Collection
    
    Call lstVenDisponibili_Init
    Call lstVenSelezionati_Init
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    Set listaNonConformit� = New Collection
    condizioneSql = ""
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_ELIMINA Then
        condizioneSql = "IDSITO <> " & idSitoSelezionato
    End If
    
    nomeSito = Trim(txtNome.Text)
    If ViolataUnicit�("SITO", "NOME = " & SqlStringValue(nomeSito), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomeSito) & _
            " � gi� presente in DB;")
    End If
    descrizioneSito = Trim(txtDescrizione.Text)
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function


