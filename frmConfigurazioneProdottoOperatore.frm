VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfigurazioneProdottoOperatore 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSelezionaFile 
      Caption         =   "Importa ricevitorie da ..."
      Height          =   375
      Left            =   4980
      TabIndex        =   28
      Top             =   6360
      Width           =   1875
   End
   Begin VB.CommandButton cmdSvuotaRicevitorieDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5700
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   4545
      Width           =   435
   End
   Begin VB.ListBox lstRicevitorieSelezionate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   6900
      MultiSelect     =   2  'Extended
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   4515
      Width           =   4860
   End
   Begin VB.CommandButton cmdRicevitoriaSelezionata 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5700
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   4995
      Width           =   435
   End
   Begin VB.CommandButton cmdRicevitoriaDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5700
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   5445
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaRicevitorieSelezionate 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5700
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5895
      Width           =   435
   End
   Begin VB.ListBox lstRicevitorieDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   150
      MultiSelect     =   2  'Extended
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   4515
      Width           =   4755
   End
   Begin VB.ListBox lstOperatoriDisponibiliNonDiRicevitoria 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   180
      MultiSelect     =   2  'Extended
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1740
      Width           =   4710
   End
   Begin VB.CommandButton cmdSvuotaOperatoreSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5730
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   3300
      Width           =   435
   End
   Begin VB.CommandButton cmdOperatoreDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5730
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2850
      Width           =   435
   End
   Begin VB.CommandButton cmdOperatoreSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5730
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2400
      Width           =   435
   End
   Begin VB.ListBox lstOperatoriSelezionatiNonDiRicevitoria 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   6900
      MultiSelect     =   2  'Extended
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   1740
      Width           =   4860
   End
   Begin VB.CommandButton cmdSvuotaOperatoriDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5730
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   1950
      Width           =   435
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   5
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   4
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   0
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSComDlg.CommonDialog cdlFileImport 
      Left            =   5700
      Top             =   6780
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblRicevitorieDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Ricevitorie Disponibili"
      Height          =   195
      Left            =   750
      TabIndex        =   27
      Top             =   4275
      Width           =   3555
   End
   Begin VB.Label lblRicevitorieSelezionate 
      Alignment       =   2  'Center
      Caption         =   "Ricevitorie Selezionate"
      Height          =   195
      Left            =   7530
      TabIndex        =   26
      Top             =   4275
      Width           =   3555
   End
   Begin VB.Label lblOperatoriSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Operatori NON di ricevitoria Selezionati"
      Height          =   195
      Left            =   7560
      TabIndex        =   19
      Top             =   1500
      Width           =   3555
   End
   Begin VB.Label lblOperatoriDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Operatori NON di ricevitoria Disponibili"
      Height          =   195
      Left            =   705
      TabIndex        =   18
      Top             =   1500
      Width           =   3555
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Diritti Operatore sul Prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   10
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   9
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoOperatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
'Private mostraTuttiGliOperatori As Boolean
'Private mostraSoloOperatoriTA As Boolean
'Private mostraSoloOperatoriTL As Boolean
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
'Private listaDisponibili As Collection
'Private listaSelezionati As Collection
Private listaOperatoriDisponibiliNonDiRicevitoria As Collection '+
Private listaOperatoriSelezionatiNonDiRicevitoria As Collection '+
Private listaRicevitorieDisponibili As Collection '+
Private listaRicevitorieSelezionate As Collection '+

'Private criteriSelezioneImpostati As Boolean
'Private operatoriPOSeTOTEMespliciti As Boolean

Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
'    listaOperatoriDisponibiliNonDiRicevitoria.Enabled = criteriSelezioneImpostati
'    lblDisponibili.Enabled = criteriSelezioneImpostati
'    listaOperatoriSelezionatiNonDiRicevitoria.Enabled = criteriSelezioneImpostati
'    lblSelezionati.Enabled = criteriSelezioneImpostati
'    cmdCarica.Enabled = criteriSelezioneImpostati
'    cmdDidsponibile.Enabled = criteriSelezioneImpostati
'    cmdSelezionato.Enabled = criteriSelezioneImpostati
'    cmdSvuotaDisponibili.Enabled = criteriSelezioneImpostati
'    cmdSvuotaSelezionati.Enabled = criteriSelezioneImpostati
'    cmdConferma.Enabled = criteriSelezioneImpostati
'    If gestioneExitCode <> EC_NON_SPECIFICATO Then
'        Call listaOperatoriDisponibiliNonDiRicevitoria.Clear
'        Call listaOperatoriSelezionatiNonDiRicevitoria.Clear
'    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Successivo"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    mostraTuttiGliOperatori = False
'    mostraSoloOperatoriTA = False
'    mostraSoloOperatoriTL = False
'    criteriSelezioneImpostati = False
'    operatoriPOSeTOTEMespliciti = False
    Call CaricaValoriLst_OperatoriNonDiRicevitoria_Ricevitorie_Disponibili
    Call CaricaValoriLst_OperatoriNonDiRicevitoria_Ricevitorie_Selezionati
'    Call CaricaValoreCheckBoxPOS
'    Call CaricaValoreCheckBoxTOTEM
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub CaricaValoriLst_OperatoriNonDiRicevitoria_Ricevitorie_Disponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim chiaveRicevitoria As String
    Dim operatoreCorrente As clsElementoLista
    Dim ricevitoriaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaOperatoriDisponibiliNonDiRicevitoria = New Collection
    
    'lista in alto a sinistra
    sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO"
    sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
    sql = sql + " AND O.ABILITATO = " & VB_VERO
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql + " AND OP.IDOPERATORE IS NULL "
    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA AND PV.IDTIPOPUNTOVENDITA = 1" ' SPECIALE
    sql = sql + " UNION"
    sql = sql + " SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO"
    sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
    sql = sql + " AND O.ABILITATO = " & VB_VERO
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql + " AND OP.IDOPERATORE IS NULL"
    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
    sql = sql + " AND O.IDPUNTOVENDITA IS NULL"
    sql = sql + " ORDER BY USERNAME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            operatoreCorrente.idElementoLista = rec("ID").Value
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriDisponibiliNonDiRicevitoria.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
'    If operatoriPOSeTOTEMespliciti = True Then
'        sql = sql + " UNION"
    
    Set listaRicevitorieDisponibili = New Collection

        'lista in basso
    sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
    sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
    sql = sql + " AND O.ABILITATO = " & VB_VERO
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql + " AND (T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2 OR T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3)" 'POS e RICEVITORIA oppure TOTEM e filiale
    sql = sql + " AND OP.IDOPERATORE IS NULL"
    sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
    sql = sql + " ORDER BY USERNAME"
    'End If

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set ricevitoriaCorrente = New clsElementoLista
            ricevitoriaCorrente.nomeElementoLista = rec("USERNAME")
            ricevitoriaCorrente.descrizioneElementoLista = rec("USERNAME")
            ricevitoriaCorrente.idElementoLista = rec("ID").Value
            chiaveRicevitoria = ChiaveId(ricevitoriaCorrente.idElementoLista)
            Call listaRicevitorieDisponibili.Add(ricevitoriaCorrente, chiaveRicevitoria)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
    Call listaRicevitorieDisponibili_Init
        
End Sub

Private Sub CaricaValoriLst_OperatoriNonDiRicevitoria_Ricevitorie_Selezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim chiaveRicevitoria As String
    Dim operatoreCorrente As clsElementoLista
    Dim ricevitoriaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaOperatoriSelezionatiNonDiRicevitoria = New Collection
    
    'lista in alto a destra
    sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO"
    sql = sql + " WHERE OP.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql + " AND O.ABILITATO = " & VB_VERO
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE"
    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA AND PV.IDTIPOPUNTOVENDITA = 1" 'SPECIALE
    sql = sql + " UNION"
    sql = sql + " SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO"
    sql = sql + " WHERE OP.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql + " AND O.ABILITATO = " & VB_VERO
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE"
    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
    sql = sql + " AND O.IDPUNTOVENDITA IS NULL"
    sql = sql + " ORDER BY USERNAME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            operatoreCorrente.idElementoLista = rec("ID").Value
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriSelezionatiNonDiRicevitoria.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
        
'    If operatoriPOSeTOTEMespliciti = True Then
'        sql = sql + " UNION"
    
    Set listaRicevitorieSelezionate = New Collection

    sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
    sql = sql + " WHERE OP.IDPRODOTTO = " & idProdottoSelezionato
    sql = sql + " AND O.ABILITATO = " & VB_VERO
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql + " AND (T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2 OR T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3)" 'POS e RICEVITORIA oppure TOTEM e filiale
    sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE"
    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
    sql = sql + " ORDER BY USERNAME"
'    End If

    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set ricevitoriaCorrente = New clsElementoLista
            ricevitoriaCorrente.nomeElementoLista = rec("USERNAME")
            ricevitoriaCorrente.descrizioneElementoLista = rec("USERNAME")
            ricevitoriaCorrente.idElementoLista = rec("ID").Value
            chiaveRicevitoria = ChiaveId(ricevitoriaCorrente.idElementoLista)
            Call listaRicevitorieSelezionate.Add(ricevitoriaCorrente, chiaveRicevitoria)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
    Call listaRicevitorieSelezionate_Init
    
End Sub

' LB 05-06-2006
'Private Sub CaricaValoreCheckBoxPOS()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim numeroDisponibili As Long
'    Dim numeroSelezionati As Long
'
'    If operatoriPOSeTOTEMespliciti = True Then
'        chkReteOperatoriPOS.Enabled = False
'    Else
'        chkReteOperatoriPOS.Enabled = True
'
'        Call ApriConnessioneBD
'
'        sql = "SELECT COUNT(*) AS NUMERODISPONIBILI"
'        sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
'        sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
'        sql = sql + " AND O.ABILITATO = " & VB_VERO
'        sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'        sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql + " AND T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2"
'        sql = sql + " AND OP.IDOPERATORE IS NULL"
'        sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
'        sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
'        sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'        sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            numeroDisponibili = rec("NUMERODISPONIBILI").Value
'        End If
'        rec.Close
'
'        sql = "SELECT COUNT(*) AS NUMEROSELEZIONATI"
'        sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
'        sql = sql + " WHERE OP.IDPRODOTTO = " & idProdottoSelezionato
'        sql = sql + " AND O.ABILITATO = " & VB_VERO
'        sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'        sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql + " AND T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2"
'        sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
'        sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE"
'        sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'        sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            numeroSelezionati = rec("NUMEROSELEZIONATI").Value
'        End If
'        rec.Close
'
'        Call ChiudiConnessioneBD
'
'    ' Di default sono Grayed, cha associo al fatto che alcuni operatori sono selezionati altri no,
'    ' una situazione non consistente
'        If numeroDisponibili = 0 And numeroSelezionati = 0 Then
'            chkReteOperatoriPOS.Enabled = False
'        End If
'        If numeroDisponibili > 0 And numeroSelezionati > 0 Then
'        ' niente, rimane grayed
'            chkReteOperatoriPOS.Enabled = False
'        Else
'            If numeroDisponibili > 0 Then
'                chkReteOperatoriPOS.Value = Unchecked
'            End If
'            If numeroSelezionati > 0 Then
'                chkReteOperatoriPOS.Value = Checked
'            End If
'        End If
'    End If
'
'End Sub
' LB 05-06-2006
'Private Sub CaricaValoreCheckBoxTOTEM()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim numeroDisponibili As Long
'    Dim numeroSelezionati As Long
'
'    If operatoriPOSeTOTEMespliciti = True Then
'        chkReteOperatoriTOTEM.Enabled = False
'    Else
'        chkReteOperatoriTOTEM.Enabled = True
'
'        Call ApriConnessioneBD
'
'        sql = "SELECT COUNT(*) AS NUMERODISPONIBILI"
'        sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
'        sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
'        sql = sql + " AND O.ABILITATO = " & VB_VERO
'        sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'        sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql + " AND T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3"
'        sql = sql + " AND OP.IDOPERATORE IS NULL"
'        sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
'        sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
'        sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'        sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            numeroDisponibili = rec("NUMERODISPONIBILI").Value
'        End If
'        rec.Close
'
'        sql = "SELECT COUNT(*) AS NUMEROSELEZIONATI"
'        sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
'        sql = sql + " WHERE OP.IDPRODOTTO = " & idProdottoSelezionato
'        sql = sql + " AND O.ABILITATO = " & VB_VERO
'        sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'        sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql + " AND T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3"
'        sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
'        sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE"
'        sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'        sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'        sql = sql + " ORDER BY USERNAME"
'
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            numeroSelezionati = rec("NUMEROSELEZIONATI").Value
'        End If
'        rec.Close
'
'        Call ChiudiConnessioneBD
'
'    ' Di default sono Grayed, cha associo al fatto che alcuni operatori sono selezionati altri no,
'    ' una situazione non consistente
'        If numeroDisponibili = 0 And numeroSelezionati = 0 Then
'            chkReteOperatoriTOTEM.Enabled = False
'        End If
'        If numeroDisponibili > 0 And numeroSelezionati > 0 Then
'        ' niente, rimane grayed
'            chkReteOperatoriTOTEM.Enabled = False
'        Else
'            If numeroDisponibili > 0 Then
'                chkReteOperatoriTOTEM.Value = Unchecked
'            End If
'            If numeroSelezionati > 0 Then
'                chkReteOperatoriTOTEM.Value = Checked
'            End If
'        End If
'    End If
'
'End Sub

'Private Sub chkOperatoriPOSeTOTEMespliciti_Click()
'    operatoriPOSeTOTEMespliciti = chkOperatoriPOSeTOTEMespliciti.Value
'
'    Call CaricaValoriLstOperatoriDisponibiliNonDiRicevitoria
'    Call CaricaValoriLstOperatoriSelezionatiNonDiRicevitoria
'    Call CaricaValoreCheckBoxPOS
'    Call CaricaValoreCheckBoxTOTEM
'
'End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

'Private Sub cmdDisponibile_Click()
'    Call SpostaInlistaOperatoriDisponibiliNonDiRicevitoria
'End Sub

'Private Sub cmdSelezionato_Click()
'    Call SpostaInlistaOperatoriSelezionatiNonDiRicevitoria
'End Sub

Private Sub cmdOperatoreDisponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaOperatoriDisponibiliNonDiRicevitoria
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdOperatoreSelezionato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaOperatoriSelezionatiNonDiRicevitoria
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdRicevitoriaDisponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaRicevitorieDisponibili
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdRicevitoriaSelezionata_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaRicevitorieSelezionate
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionaFile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ImportaRicevitorie
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    criteriSelezioneImpostati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            Call InserisciNellaBaseDati
            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, stringaNota, idProdottoSelezionato)
'                criteriSelezioneImpostati = False
            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
'    Call AzionePercorsoGuidato(TNCP_FINE)
    Call CaricaFormAbilitazioniSupportiDigitali
End Sub

Private Sub CaricaFormAbilitazioniSupportiDigitali()
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.Init
End Sub

Private Sub listaOperatoriDisponibiliNonDiRicevitoria_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriDisponibiliNonDiRicevitoria.Clear
    
    If Not (listaOperatoriDisponibiliNonDiRicevitoria Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriDisponibiliNonDiRicevitoria
            lstOperatoriDisponibiliNonDiRicevitoria.AddItem operatore.descrizioneElementoLista
            lstOperatoriDisponibiliNonDiRicevitoria.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub listaRicevitorieDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ricevitoria As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstRicevitorieDisponibili.Clear
    
    If Not (listaRicevitorieDisponibili Is Nothing) Then
        i = 1
        For Each ricevitoria In listaRicevitorieDisponibili
            lstRicevitorieDisponibili.AddItem ricevitoria.descrizioneElementoLista
            lstRicevitorieDisponibili.ItemData(i - 1) = ricevitoria.idElementoLista
            i = i + 1
        Next ricevitoria
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub listaOperatoriSelezionatiNonDiRicevitoria_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriSelezionatiNonDiRicevitoria.Clear
    
    If Not (listaOperatoriSelezionatiNonDiRicevitoria Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriSelezionatiNonDiRicevitoria
            lstOperatoriSelezionatiNonDiRicevitoria.AddItem operatore.descrizioneElementoLista
            lstOperatoriSelezionatiNonDiRicevitoria.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub listaRicevitorieSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ricevitoria As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstRicevitorieSelezionate.Clear
    
    If Not (listaRicevitorieSelezionate Is Nothing) Then
        i = 1
        For Each ricevitoria In listaRicevitorieSelezionate
            lstRicevitorieSelezionate.AddItem ricevitoria.descrizioneElementoLista
            lstRicevitorieSelezionate.ItemData(i - 1) = ricevitoria.idElementoLista
            i = i + 1
        Next ricevitoria
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SvuotaOperatoriDisponibili()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriDisponibiliNonDiRicevitoria
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriSelezionatiNonDiRicevitoria.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriDisponibiliNonDiRicevitoria = Nothing
    Set listaOperatoriDisponibiliNonDiRicevitoria = New Collection
    
    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SvuotaRicevitorieDisponibili()
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
    For Each ricevitoria In listaRicevitorieDisponibili
        chiaveRicevitoria = ChiaveId(ricevitoria.idElementoLista)
        Call listaRicevitorieSelezionate.Add(ricevitoria, chiaveRicevitoria)
    Next ricevitoria
    Set listaRicevitorieDisponibili = Nothing
    Set listaRicevitorieDisponibili = New Collection
    
    Call listaRicevitorieDisponibili_Init
    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub SvuotaOperatoriSelezionati()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriSelezionatiNonDiRicevitoria
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriDisponibiliNonDiRicevitoria.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriSelezionatiNonDiRicevitoria = Nothing
    Set listaOperatoriSelezionatiNonDiRicevitoria = New Collection
    
    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SvuotaRicevitorieSelezionate()
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
    For Each ricevitoria In listaRicevitorieSelezionate
        chiaveRicevitoria = ChiaveId(ricevitoria.idElementoLista)
        Call listaRicevitorieDisponibili.Add(ricevitoria, chiaveRicevitoria)
    Next ricevitoria
    Set listaRicevitorieSelezionate = Nothing
    Set listaRicevitorieSelezionate = New Collection
    
    Call listaRicevitorieDisponibili_Init
    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub SpostaInlistaOperatoriDisponibiliNonDiRicevitoria()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriSelezionatiNonDiRicevitoria.ListCount
        If lstOperatoriSelezionatiNonDiRicevitoria.Selected(i - 1) Then
            idOperatore = lstOperatoriSelezionatiNonDiRicevitoria.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriSelezionatiNonDiRicevitoria.Item(chiaveOperatore)
            Call listaOperatoriDisponibiliNonDiRicevitoria.Add(operatore, chiaveOperatore)
            Call listaOperatoriSelezionatiNonDiRicevitoria.Remove(chiaveOperatore)
        End If
    Next i
    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SpostaInlistaRicevitorieDisponibili()
    Dim i As Integer
    Dim idRicevitoria As Long
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
    For i = 1 To lstRicevitorieSelezionate.ListCount
        If lstRicevitorieSelezionate.Selected(i - 1) Then
            idRicevitoria = lstRicevitorieSelezionate.ItemData(i - 1)
            chiaveRicevitoria = ChiaveId(idRicevitoria)
            Set ricevitoria = listaRicevitorieSelezionate.Item(chiaveRicevitoria)
            Call listaRicevitorieDisponibili.Add(ricevitoria, chiaveRicevitoria)
            Call listaRicevitorieSelezionate.Remove(chiaveRicevitoria)
        End If
    Next i
    Call listaRicevitorieDisponibili_Init
    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub SpostaInlistaOperatoriSelezionatiNonDiRicevitoria()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriDisponibiliNonDiRicevitoria.ListCount
        If lstOperatoriDisponibiliNonDiRicevitoria.Selected(i - 1) Then
            idOperatore = lstOperatoriDisponibiliNonDiRicevitoria.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriDisponibiliNonDiRicevitoria.Item(chiaveOperatore)
            Call listaOperatoriSelezionatiNonDiRicevitoria.Add(operatore, chiaveOperatore)
            Call listaOperatoriDisponibiliNonDiRicevitoria.Remove(chiaveOperatore)
        End If
    Next i
    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SpostaInlistaRicevitorieSelezionate()
    Dim i As Integer
    Dim idRicevitoria As Long
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
    For i = 1 To lstRicevitorieDisponibili.ListCount
        If lstRicevitorieDisponibili.Selected(i - 1) Then
            idRicevitoria = lstRicevitorieDisponibili.ItemData(i - 1)
            chiaveRicevitoria = ChiaveId(idRicevitoria)
            Set ricevitoria = listaRicevitorieDisponibili.Item(chiaveRicevitoria)
            Call listaRicevitorieSelezionate.Add(ricevitoria, chiaveRicevitoria)
            Call listaRicevitorieDisponibili.Remove(chiaveRicevitoria)
        End If
    Next i
    Call listaRicevitorieDisponibili_Init
    Call listaRicevitorieSelezionate_Init
End Sub
'Private Sub cmdSvuotaDisponibili_Click()
'    Call SvuotaDisponibili
'End Sub

'Private Sub cmdSvuotaSelezionati_Click()
'    Call SvuotaSelezionati
'End Sub

Private Sub listaOperatoriDisponibiliNonDiRicevitoria_Click()
    Call VisualizzaListBoxToolTip(listaOperatoriDisponibiliNonDiRicevitoria, listaOperatoriDisponibiliNonDiRicevitoria.Text)
End Sub

Private Sub listaOperatoriSelezionatiNonDiRicevitoria_Click()
    Call VisualizzaListBoxToolTip(listaOperatoriSelezionatiNonDiRicevitoria, listaOperatoriSelezionatiNonDiRicevitoria.Text)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim operatore As clsElementoLista
    Dim ricevitoria As clsElementoLista
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    
    sql = "DELETE FROM OPERATORE_PRODOTTO" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDOPERATORE IN (" & _
        " SELECT O.IDOPERATORE" & _
        " FROM OPERATORE O, OPERATORE_ORGANIZZAZIONE OO" & _
        " WHERE IDPERFEZIONATORE IS NULL" & _
        " AND O.IDOPERATORE = OO.IDOPERATORE" & _
        " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        ")"
    SETAConnection.Execute sql, n, adCmdText

    If Not (listaOperatoriSelezionatiNonDiRicevitoria Is Nothing) Then
        For Each operatore In listaOperatoriSelezionatiNonDiRicevitoria
            sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)" & _
                " VALUES (" & operatore.idElementoLista & ", " & _
                idProdottoSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next operatore
    End If
    
    If Not (listaRicevitorieSelezionate Is Nothing) Then
        For Each ricevitoria In listaRicevitorieSelezionate
            sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)" & _
                " VALUES (" & ricevitoria.idElementoLista & ", " & _
                idProdottoSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next ricevitoria
    End If
    
' POS
'    If chkReteOperatoriPOS.Enabled = True Then
'        sql = "DELETE FROM OPERATORE_PRODOTTO" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDOPERATORE IN (" & _
'            " SELECT DISTINCT O.IDOPERATORE " & _
'            " FROM OPERATORE O, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T" & _
'            " WHERE O.ABILITATO = 1" & _
'            " AND O.IDPERFEZIONATORE IS NULL" & _
'            " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2" & _
'            " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA" & _
'            " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'            " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA)"
'        SETAConnection.Execute sql, n, adCmdText
'
'        If chkReteOperatoriPOS.Value = Checked Then
'            sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)" & _
'                " SELECT DISTINCT O.IDOPERATORE, " & idProdottoSelezionato & _
'                " FROM OPERATORE O, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T" & _
'                " WHERE O.ABILITATO = 1" & _
'                " AND O.IDPERFEZIONATORE IS NULL" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2" & _
'                " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA" & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'            SETAConnection.Execute sql, n, adCmdText
'        End If
'    End If
    
' TOTEM
'    If chkReteOperatoriTOTEM.Enabled = True Then
'        sql = "DELETE FROM OPERATORE_PRODOTTO" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDOPERATORE IN (" & _
'            " SELECT DISTINCT O.IDOPERATORE " & _
'            " FROM OPERATORE O, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T" & _
'            " WHERE O.ABILITATO = 1" & _
'            " AND O.IDPERFEZIONATORE IS NULL" & _
'            " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3" & _
'            " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA" & _
'            " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'            " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA)"
'        SETAConnection.Execute sql, n, adCmdText
'
'        If chkReteOperatoriTOTEM.Value = Checked Then
'            sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)" & _
'                " SELECT DISTINCT O.IDOPERATORE, " & idProdottoSelezionato & _
'                " FROM OPERATORE O, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T" & _
'                " WHERE O.ABILITATO = 1" & _
'                " AND O.IDPERFEZIONATORE IS NULL" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3" & _
'                " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA" & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'            SETAConnection.Execute sql, n, adCmdText
'        End If
'    End If
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub cmdSvuotaOperatoreSelezionati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaOperatoriSelezionati
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaOperatoriDisponibili_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaOperatoriDisponibili
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaRicevitorieDisponibili_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaRicevitorieDisponibili
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaRicevitorieSelezionate_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaRicevitorieSelezionate
    
    MousePointer = mousePointerOld
End Sub
    
Private Sub ImportaRicevitorie()
    Call SvuotaRicevitorieSelezionate
    Call ApriFileImportazioneRicevitorie
    Call listaRicevitorieDisponibili_Init
    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub ApriFileImportazioneRicevitorie()
    
On Error GoTo gestioneErroreAperturaFileExcel

    DoEvents
    
    cdlFileImport.InitDir = App.Path
    cdlFileImport.Filter = "Excel (*.xls)|*.xls"
    cdlFileImport.DefaultExt = "xls"
    cdlFileImport.Flags = cdlOFNFileMustExist Or cdlOFNExplorer
    cdlFileImport.FileName = ""
    cdlFileImport.CancelError = True
    cdlFileImport.ShowOpen
    
    nomeFileImportazione = cdlFileImport.FileName
    DoEvents
    Call LeggiRicevitorieDaFileExcel
    
    Exit Sub
    
gestioneErroreAperturaFileExcel:
    'do nothing
End Sub

Private Sub LeggiRicevitorieDaFileExcel()
    Dim workbook As workbook
    Dim workSheet As workSheet
    Dim nomeRicevitoria As String
    Dim ricevitoria As clsElementoLista
    Dim chiave As String
    Dim i As Long
    Dim numeroNulli As Long
    Dim numeroValori As Long
    Dim rangeTotale As range
    Dim quantitaRicevitorieInserite As Long
    Dim quantitaDatiErrati As Long
    Dim listaErrori As String
    
    Set workbook = excImportazione.Workbooks.Open(nomeFileImportazione)
    Set workSheet = workbook.ActiveSheet
    
On Error Resume Next
    
    i = 1
    nomeRicevitoria = ""
    quantitaRicevitorieInserite = 0
    quantitaDatiErrati = 0
    listaErrori = ""
    
    Set rangeTotale = workSheet.range("A:A")
    numeroNulli = excImportazione.WorksheetFunction.CountBlank(rangeTotale)
    numeroValori = 65536 - numeroNulli
    
'    Call pgbAvanzamento_Init(numeroValori)
    
    While workSheet.Rows.Cells(i, 1) <> ""
        nomeRicevitoria = workSheet.Rows.Cells(i, 1)
        Set ricevitoria = getOperatoreRicevitoria(nomeRicevitoria)
        If ricevitoria Is Nothing Then
            quantitaDatiErrati = quantitaDatiErrati + 1
            listaErrori = listaErrori & " " & nomeRicevitoria
        Else
            chiave = ChiaveId(ricevitoria.idElementoLista)
            Call listaRicevitorieDisponibili.Remove(chiave)
            Call listaRicevitorieSelezionate.Add(ricevitoria, chiave)
            quantitaRicevitorieInserite = quantitaRicevitorieInserite + 1
        End If
        i = i + 1
'        Call pgbAvanzamento_Update(i)
'        Call lblAvanzamento_Update("Lettura dati da File: " & _
'            i - 2 & " di " & numeroValori)
        DoEvents
    Wend
    MsgBox "Trovate " & quantitaRicevitorieInserite & " ricevitorie; trovati " & quantitaDatiErrati & " errori (" & listaErrori & ")"
    
    Call excImportazione.Quit
End Sub

Private Function getOperatoreRicevitoria(pNome As String) As clsElementoLista
'    Dim sql As String
'    Dim rec As OraDynaset
'    Dim ricevitoria As clsElementoLista
'
'    Set getOperatoreRicevitoria = Nothing
'
'    sql = "SELECT DISTINCT USERNAME, O.IDOPERATORE AS ID"
'    sql = sql + " FROM OPERATORE O, TERMINALE T, PUNTOVENDITA PV"
'    sql = sql + " WHERE O.USERNAME = '" & nome & "'"
'    sql = sql + " AND O.IDPERFEZIONATORE IS NULL"
'    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'    sql = sql + " AND PV.IDTIPOPUNTOVENDITA = 2"
'    sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
'    sql = sql + " AND T.IDTIPOTERMINALE = 5"
'
'    Set rec = ORADB.CreateDynaset(sql, 0&)
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'
'        Set ricevitoria = New clsElementoLista
'        ricevitoria.nomeElementoLista = rec("USERNAME")
'        ricevitoria.descrizioneElementoLista = rec("USERNAME")
'        ricevitoria.idElementoLista = rec("ID").Value
'        Set getOperatoreRicevitoria = ricevitoria
'    End If
'    rec.Close
'
    Dim sql As String
    Dim rec As OraDynaset
    Dim ricevitoria As clsElementoLista

    Set getOperatoreRicevitoria = Nothing

    sql = "SELECT DISTINCT USERNAME, O.IDOPERATORE AS ID"
    sql = sql + " FROM OPERATORE O, TERMINALE T, PUNTOVENDITA PV"
    sql = sql + " WHERE O.USERNAME = :nome"
    sql = sql + " AND O.IDPERFEZIONATORE IS NULL"
    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
    sql = sql + " AND PV.IDTIPOPUNTOVENDITA = 2"
    sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
    sql = sql + " AND T.IDTIPOTERMINALE = 5"
    
    ORADB.Parameters.Add "nome", pNome, 1
    Set rec = ORADB.dbCreateDynaset(sql, 0&)
    If rec.EOF = False Then
        rec.MoveFirst
        
        Set ricevitoria = New clsElementoLista
        ricevitoria.nomeElementoLista = rec("USERNAME")
        ricevitoria.descrizioneElementoLista = rec("USERNAME")
        ricevitoria.idElementoLista = rec("ID").Value
        Set getOperatoreRicevitoria = ricevitoria
    End If
    rec.Close
    ORADB.Parameters.Remove "nome"

End Function
