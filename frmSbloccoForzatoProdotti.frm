VERSION 5.00
Begin VB.Form frmSbloccoForzatoProdotti 
   Caption         =   "Prodotti"
   ClientHeight    =   6195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7530
   LinkTopic       =   "Form1"
   ScaleHeight     =   6195
   ScaleWidth      =   7530
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   2370
      Width           =   3795
   End
   Begin VB.ComboBox cmbProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   3045
      Width           =   5580
   End
   Begin VB.Frame fraPulsanti 
      Height          =   915
      Left            =   75
      TabIndex        =   1
      Top             =   5130
      Width           =   1740
      Begin VB.CommandButton cmdSbloccoForzatoProdotto 
         Caption         =   "Sblocca"
         Height          =   390
         Left            =   225
         TabIndex        =   2
         Top             =   300
         Width           =   1290
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   465
      Left            =   5550
      TabIndex        =   0
      Top             =   5430
      Width           =   1140
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Sblocco forzato di un prodotto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   150
      TabIndex        =   9
      Top             =   150
      Width           =   5775
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazioni"
      Height          =   255
      Left            =   75
      TabIndex        =   8
      Top             =   2070
      Width           =   1470
   End
   Begin VB.Label lblProdotto 
      Caption         =   "Prodotto da sbloccare"
      Height          =   195
      Left            =   75
      TabIndex        =   7
      Top             =   2820
      Width           =   2025
   End
   Begin VB.Label lblMessaggio 
      Caption         =   "Questa maschera � utilizzata per sbloccare forzatamente un prodotto ."
      Height          =   315
      Left            =   75
      TabIndex        =   6
      Top             =   675
      Width           =   7365
   End
   Begin VB.Label Label1 
      Height          =   615
      Left            =   75
      TabIndex        =   5
      Top             =   1080
      Width           =   7365
   End
End
Attribute VB_Name = "frmSbloccoForzatoProdotti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long

Private Sub cmbOrganizzazione_Click()
    Call cmbProdotto_Update
End Sub

Private Sub cmbProdotto_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
    sql = "SELECT P.idprodotto ID, P.nome || ' - ' || PU.UTENTE || ' - ' || to_char(PU.DATAORA,'DD/MM/YYYY HH24:MI:SS') as NOME"
    sql = sql & " FROM prodotto P, cc_prodottoutilizzato PU"
    sql = sql & " WHERE P.idprodotto = PU.idprodotto"
    sql = sql & " AND P.idorganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY P.nome ASC"
    
    Call CaricaValoriCombo(cmbProdotto, sql, "NOME")
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbProdotto_Click()
    Call pulsante_Update
End Sub

Private Sub pulsante_Update()

    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
    
    If cmbProdotto.ListIndex > -1 Then
        idRecordSelezionato = idProdottoSelezionato
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
        
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Public Sub Init()
    Dim sql As String

    sql = "SELECT O.idorganizzazione ID, O.nome NOME"
    sql = sql & " From organizzazione O, tipoStatoOrganizzazione TSO"
    sql = sql & " WHERE ((TSO.idtipostatoorganizzazione ="
    sql = sql & " O.idTipoStatoOrganizzazione"
    sql = sql & " )"
    sql = sql & " AND (TSO.idtipostatoorganizzazione = " & TSO_ATTIVA & ")"
    sql = sql & " )"
    sql = sql & " ORDER BY O.nome ASC"

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idProdottoSelezionato = idNessunElementoSelezionato
    
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdSbloccoForzatoProdotto.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
'    cmb.AddItem "<tutti>"
'    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub cmdSbloccoForzatoProdotto_Click()
    Call SbloccoForzatoProdotto
End Sub

Private Sub SbloccoForzatoProdotto()
    Dim sql As String
    Dim n As Long
    Dim msg As String
    Dim rec As New ADODB.Recordset
    
    On Error GoTo gestioneErrori
    
    sql = " DELETE FROM CC_PRODOTTOUTILIZZATO WHERE IDPRODOTTO = " & idProdottoSelezionato

    Call ApriConnessioneBD
    SETAConnection.BeginTrans
    
    SETAConnection.Execute sql, n, adCmdText
    
    If (n > 0) Then
        Call frmMessaggio.Visualizza("ConfermaSbloccoProdotto")
        
        If frmMessaggio.exitCode = EC_CONFERMA Then
            SETAConnection.CommitTrans
            MsgBox "Avvenuto sblocco", vbInformation, "Prodotto"
        Else
            SETAConnection.RollbackTrans
            MsgBox "Operazione annullata", vbInformation, "Prodotto"
        End If
    Else
        SETAConnection.RollbackTrans
        msg = "Errore nello sblocco: sono stati sbloccati " & n & " record!" & vbCrLf
        msg = msg & "Operazione annullata"
        MsgBox msg, vbCritical, "Attenzione"
    End If
    
    Call ChiudiConnessioneBD
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub
