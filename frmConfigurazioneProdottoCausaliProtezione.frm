VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoCausaliProtezione 
   Caption         =   "Prodotto"
   ClientHeight    =   7020
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11940
   LinkTopic       =   "Form1"
   ScaleHeight     =   7020
   ScaleWidth      =   11940
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstClassiPuntiVendita 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3000
      Left            =   180
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1080
      Width           =   2415
   End
   Begin VB.ListBox lstCausaliProtezione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4620
      Left            =   2760
      Style           =   1  'Checkbox
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1080
      Width           =   3735
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   6000
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   5
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   4
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   0
      Top             =   6000
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblSelezionati 
      Caption         =   "Causali protezione"
      Height          =   195
      Left            =   2760
      TabIndex        =   15
      Top             =   840
      Width           =   2775
   End
   Begin VB.Label lblDisponibili 
      Caption         =   "Classi punti vendita"
      Height          =   195
      Left            =   180
      TabIndex        =   14
      Top             =   840
      Width           =   2415
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle causali protezione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   7935
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   12
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   11
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoCausaliProtezione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private rateo As Long
Private numeroMaxTitoliEmettibiliPerSuperarea As Long

Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private idClassePuntoVenditaSelezionata As Long

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = True
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLstClassiPuntoVendita
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Private Sub CaricaValoriLstClassiPuntoVendita()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    idClassePuntoVenditaSelezionata = idNessunElementoSelezionato
    
    Call ApriConnessioneBD
    
    sql = "SELECT IDCLASSEPUNTOVENDITA ID, NOME" & _
        " FROM CLASSEPUNTOVENDITA" & _
        " WHERE DIRITTIOPERATOREGESTIBILI = 0" & _
        " ORDER BY IDCLASSEPUNTOVENDITA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstClassiPuntiVendita.AddItem rec("NOME")
            lstClassiPuntiVendita.ItemData(i - 1) = rec("ID")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
End Sub

Private Sub CaricaValoriLstCausaliProtezione()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    Call ApriConnessioneBD
    
    lstCausaliProtezione.Clear
    sql = "SELECT CP.IDCAUSALEPROTEZIONE, SIMBOLOSUPIANTA || ' - ' || NOME CAUSALE, CC.IDCAUSALEPROTEZIONE SELEZIONATO" & _
        " FROM CAUSALEPROTEZIONE CP," & _
        " (" & _
        " SELECT IDCAUSALEPROTEZIONE" & _
        " FROM CLASSEPV_CAUSALEPROTEZIONE" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
        " ) CC" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND CP.IDCAUSALEPROTEZIONE = CC.IDCAUSALEPROTEZIONE(+)" & _
        " ORDER BY CP.SIMBOLOSUPIANTA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstCausaliProtezione.AddItem rec("CAUSALE")
            lstCausaliProtezione.ItemData(i - 1) = rec("IDCAUSALEPROTEZIONE")
            If IsNull(rec("SELEZIONATO")) Then
                lstCausaliProtezione.Selected(i - 1) = False
            Else
                lstCausaliProtezione.Selected(i - 1) = True
            End If
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
                Call frmConfigurazioneProdottoCapienzeTurnoLibero.AzionePercorsoGuidato(TNCP_ABBANDONA)
            Else
                Call frmConfigurazioneProdottoRappresentazioni.AzionePercorsoGuidato(TNCP_ABBANDONA)
            End If
        Case TNCP_FINE
            Unload Me
            If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
                Call frmConfigurazioneProdottoCapienzeTurnoLibero.AzionePercorsoGuidato(TNCP_FINE)
            Else
                Call frmConfigurazioneProdottoRappresentazioni.AzionePercorsoGuidato(TNCP_FINE)
            End If
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            Call InserisciNellaBaseDati
            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_CAPIENZA_SUPERAREA, stringaNota, idProdottoSelezionato)
            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneTariffe
End Sub

Private Sub CaricaFormConfigurazioneTariffe()
    Call frmConfigurazioneProdottoTariffe.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoTariffe.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoTariffe.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoTariffe.SetRateo(rateo)
    Call frmConfigurazioneProdottoTariffe.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoTariffe.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoTariffe.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoTariffe.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoTariffe.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoTariffe.Init
    
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Long
    Dim n As Long
    Dim area As clsElementoLista
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM CLASSEPV_CAUSALEPROTEZIONE" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata
    SETAConnection.Execute sql, n, adCmdText

    If lstCausaliProtezione.SelCount > 0 Then
        For i = 0 To lstCausaliProtezione.ListCount - 1
            If lstCausaliProtezione.Selected(i) Then
            sql = "INSERT INTO CLASSEPV_CAUSALEPROTEZIONE (IDPRODOTTO, IDCAUSALEPROTEZIONE, IDCLASSEPUNTOVENDITA)" & _
                " VALUES (" & _
                idProdottoSelezionato & ", " & _
                lstCausaliProtezione.ItemData(i) & ", " & _
                idClassePuntoVenditaSelezionata & ")"
            SETAConnection.Execute sql, n, adCmdText
            End If
        Next i
    End If

    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub CaricaFormDettagliMaxTitoliEmettibili()
    Call frmDettagliMaxTitoliPerSuperarea.Init
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld

End Sub

Private Sub lstClassiPuntiVendita_Click()
    idClassePuntoVenditaSelezionata = lstClassiPuntiVendita.ItemData(lstClassiPuntiVendita.ListIndex)
    Call CaricaValoriLstCausaliProtezione
End Sub

