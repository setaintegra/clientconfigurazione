VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrincipale 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Client di Configurazione"
   ClientHeight    =   6270
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   12705
   ControlBox      =   0   'False
   Icon            =   "frmPrincipale.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmPrincipale.frx":0443
   ScaleHeight     =   6270
   ScaleWidth      =   12705
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrRipristinoConnessione 
      Left            =   60
      Top             =   5460
   End
   Begin MSComctlLib.StatusBar sbrConnessione 
      Align           =   2  'Align Bottom
      Height          =   435
      Left            =   0
      TabIndex        =   0
      Top             =   5835
      Width           =   12705
      _ExtentX        =   22410
      _ExtentY        =   767
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8819
            MinWidth        =   8819
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileEsci 
         Caption         =   "Esci"
      End
   End
   Begin VB.Menu mnuConfigurazione 
      Caption         =   "&Configurazione"
      Begin VB.Menu mnuTipoSupporto 
         Caption         =   "&Tipo Supporto"
      End
      Begin VB.Menu mnuLayoutSupporto 
         Caption         =   "&Layout Supporto"
      End
      Begin VB.Menu mnuHTLayout 
         Caption         =   "Layout Ticketing Mail"
      End
      Begin VB.Menu mnuS99 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSottorete 
         Caption         =   "&Sottorete"
      End
      Begin VB.Menu mnuS15 
         Caption         =   "-"
      End
      Begin VB.Menu mnuVenditoriEsterni 
         Caption         =   "Venditori &esterni"
      End
      Begin VB.Menu mnuS9 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAttributiStandard 
         Caption         =   "&Attributi standard"
         Begin VB.Menu mnuTariffa 
            Caption         =   "&Tariffa"
         End
         Begin VB.Menu mnuModalitąPagamento 
            Caption         =   "&Modalitą di pagamento"
         End
      End
      Begin VB.Menu mnuS1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPuntoVendita 
         Caption         =   "Pu&nto Vendita"
      End
      Begin VB.Menu mnuS7 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOperatore 
         Caption         =   "Oper&atore"
      End
      Begin VB.Menu mnuOperatoreAbilitabileAIVAPreassoltaObbligatoria 
         Caption         =   "Operatore abilitabile per IVA preassolta obbligatoria"
      End
      Begin VB.Menu mnuS8 
         Caption         =   "-"
      End
      Begin VB.Menu mnuGenereCartaDiPagamento 
         Caption         =   "Genere Carta di pagamento"
      End
      Begin VB.Menu mnuS10 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOrganizzazione 
         Caption         =   "&Organizzazione"
      End
      Begin VB.Menu mnuS4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSito 
         Caption         =   "&Sito"
      End
      Begin VB.Menu mnuVenue 
         Caption         =   "&Venue"
      End
      Begin VB.Menu mnuPianta 
         Caption         =   "&Pianta"
      End
      Begin VB.Menu mnuPiantaCapienzaIllimitata 
         Caption         =   "Pianta a capienza illimitata"
      End
      Begin VB.Menu mnuPiantaSIAE 
         Caption         =   "Pianta &SIAE"
      End
      Begin VB.Menu mnu99 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImpianto 
         Caption         =   "Impianto"
      End
      Begin VB.Menu mnuS5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTurno 
         Caption         =   "Turno &Rappresentazione"
      End
      Begin VB.Menu mnuStagione 
         Caption         =   "Sta&gione"
      End
      Begin VB.Menu mnuSpettacolo 
         Caption         =   "Sp&ettacolo"
      End
      Begin VB.Menu mnuS6 
         Caption         =   "-"
      End
      Begin VB.Menu mnuManifestazione 
         Caption         =   "&Manifestazione"
      End
      Begin VB.Menu mnuProdotto 
         Caption         =   "Pro&dotto"
      End
      Begin VB.Menu mnuGruppoProdotti 
         Caption         =   "Grupp&o Prodotti"
      End
      Begin VB.Menu mnuOffertaPacchetto 
         Caption         =   "Offerta Pacc&hetto"
      End
      Begin VB.Menu mnuS11 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSistemiEsterni 
         Caption         =   "Sistemi esterni"
      End
   End
   Begin VB.Menu mnuAggiornaStato 
      Caption         =   "&Aggiorna stato"
      Begin VB.Menu mnuAggiornaStatoProdotto 
         Caption         =   "&Prodotto"
      End
      Begin VB.Menu mnuAggiornaStatoOrganizzazione 
         Caption         =   "&Organizzazione"
      End
   End
   Begin VB.Menu mnuUtilita 
      Caption         =   "&Utilitą"
      Begin VB.Menu mnuSostituzioneTipoSupporto 
         Caption         =   "&Sostituzione Tipo Supporto"
      End
      Begin VB.Menu mnuSostituzioneLayoutSupporto 
         Caption         =   "&Sostituzione Layout Supporto"
      End
      Begin VB.Menu mnuResetPassword 
         Caption         =   "&Reset password"
      End
      Begin VB.Menu mnuInserimentoProdottoRappresentazione 
         Caption         =   "&Inserimento Prodotto Rappresentazione"
      End
      Begin VB.Menu mnuModificaDataRappresentazione 
         Caption         =   "&Modifica data ora Rappresentazione"
      End
      Begin VB.Menu mnuSbloccoForzatoProdotto 
         Caption         =   "&Sblocco forzato di un prodotto"
      End
      Begin VB.Menu mnuReportStatoPosti 
         Caption         =   "Report stato posti"
      End
      Begin VB.Menu mnuControlloAbilitazioniPV 
         Caption         =   "Controllo abilitazioni punto vendita"
      End
      Begin VB.Menu mnuReportPianta 
         Caption         =   "Report pianta"
      End
      Begin VB.Menu mnuCopiaCPVTraOrganizzazioni 
         Caption         =   "Copia CPV tra organizzazioni"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu mnuHTDaStampare 
         Caption         =   "HT da stampare"
      End
      Begin VB.Menu mnuReportRimborsi 
         Caption         =   "Report per rimborsi"
      End
      Begin VB.Menu mnuAlitalia 
         Caption         =   "Alitalia"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu mnuClonazionePianta 
         Caption         =   "Clonazione pianta"
         Begin VB.Menu mnuConfermaClonazione 
            Caption         =   "Conferma clonazione"
         End
         Begin VB.Menu mnuEliminaVecchiaClonazione 
            Caption         =   "Elimina vecchia clonazione"
         End
      End
      Begin VB.Menu mnuClonazioneParzialeProdotto 
         Caption         =   "Clonazione parziale prodotto"
      End
      Begin VB.Menu mnuPresaInCaricoProdottiTDL 
         Caption         =   "Presa in carico prodotti TDL"
      End
   End
   Begin VB.Menu mnuPianificazione 
      Caption         =   "Pianificazione"
      Begin VB.Menu mnuSoldOut 
         Caption         =   "Sold out"
      End
   End
   Begin VB.Menu mnuLogAttivitą 
      Caption         =   "&Log attivitą"
   End
   Begin VB.Menu mnuInfo 
      Caption         =   "&?"
   End
End
Attribute VB_Name = "frmPrincipale"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private listaRappresentazioniConUtilizzatoriDaEliminare As Collection

Private Sub CaricaFormConfermaSessione()
    Call frmConfermaSessione.Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormConfigurazioneNuovaSessione()
    Call frmConfigurazioneNuovaSessione.Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Form_Load()
    Dim n As Long
    Dim elencoUtilizzatoriDaEliminare
    
    Call Const_Init
    Call tmrRipristinoConnessione_Init
    If App.PrevInstance = False Then
        Call Init
        Call frmLogin.Init
        Call frmLogin.Show(vbModal)
        sbrConnessione.Panels(1).Text = "CC 17.6.0: 09 dicembre 2019 - Utente: " & userID & " - DB: " & dataSource
        
        '20051014 DP - NOTA: scommentando le righe che seguono si abilita il controllo e
        'l'eventuale eliminazione degli utilizzatori trascorsi 7 giorni
        'dalla rappresentazione.
        'Il controllo non e' stato testato e non e' chiaro se serva effettivamente.
        
        'If EsistonoUtilizzatoriDaEliminare Then
        '    elencoUtilizzatoriDaEliminare = ArgomentoMessaggio(listaRappresentazioniConUtilizzatoriDaEliminare)
        '    Call frmMessaggio.Visualizza("ConfermaEliminazioneUtilizzatori", elencoUtilizzatoriDaEliminare)
        '    If frmMessaggio.exitCode = EC_CONFERMA Then
        '        Call EliminaUtilizzatoriRappresentazioni(n)
        '    End If
        'End If
    Else
        Call frmMessaggio.Visualizza("NotificaApplicazioneGiaLanciata")
        Call Esci
    End If
End Sub

Private Function EsistonoUtilizzatoriDaEliminare() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim elementoLista As String
    
    elementoLista = ""
    EsistonoUtilizzatoriDaEliminare = False
    sql = " SELECT S.NOME, R.DATAORAINIZIO, R.DURATAINMINUTI,"
    sql = sql & " R.DATAORAINIZIO + R.DURATAINMINUTI/1440 + 7 SCADENZA"
    sql = sql & " FROM UTILIZZATORE U, RAPPRESENTAZIONE R, SPETTACOLO S"
    sql = sql & " WHERE U.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " AND R.IDSPETTACOLO = S.IDSPETTACOLO"
    sql = sql & " AND R.UTILIZZATORIDAMANTENERE = " & VB_FALSO
    sql = sql & " AND (R.DATAORAINIZIO + R.DURATAINMINUTI/1440 + 7) < SYSDATE"
    sql = sql & " ORDER BY R.DATAORAINIZIO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        Set listaRappresentazioniConUtilizzatoriDaEliminare = New Collection
        EsistonoUtilizzatoriDaEliminare = True
        rec.MoveFirst
        While Not rec.EOF
            elementoLista = rec("NOME") & " - " & rec("DATAORAINIZIO") & " - " & _
                rec("DURATAINMINUTI") & " - " & rec("SCADENZA")
            Call listaRappresentazioniConUtilizzatoriDaEliminare.Add(elementoLista)
            rec.MoveNext
        Wend
    End If
    
End Function

Private Sub mnuAggiornaStatoOrganizzazione_Click()
    Call CaricaFormGestioneStatoOrganizzazione
End Sub

Private Sub CaricaFormGestioneStatoOrganizzazione()
    Call frmGestioneStatoOrganizzazione.Init
End Sub

Private Sub mnuAggiornaStatoProdotto_Click()
    Call CaricaFormGestioneStatoProdotto
End Sub

Private Sub CaricaFormGestioneStatoProdotto()
    Call frmGestioneStatoProdotto.Init
End Sub

Private Sub mnuApriSessione_Click()
    Call CaricaFormAperturaSessione
End Sub

Private Sub CaricaFormAperturaSessione()
    Call frmApriSessione.Init
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormLogAttivitą()
    Call frmLogAttivitą.Init
End Sub

Private Sub mnuConfermaSessione_Click()
    Call CaricaFormConfermaSessione
End Sub
'
'Private Sub mnuAlitalia_Click()
'    Call CaricaFormAlitalia
'End Sub
'
'Private Sub CaricaFormAlitalia()
'    Call frmAlitalia.Init
'End Sub

Private Sub mnuClonazioneParzialeProdotto_Click()
    Call frmClonazioneParzialeProdotto.Init
End Sub

Private Sub mnuConfermaClonazione_Click()
    Call frmClonazionePiantaConclusione.Init(VB_VERO)
End Sub

Private Sub mnuEliminaVecchiaClonazione_Click()
    Call frmClonazionePiantaConclusione.Init(VB_FALSO)
End Sub

Private Sub mnuControlloAbilitazioniPV_Click()
    Call CaricaFormControlloAbilitazioniPV
End Sub

Private Sub CaricaFormControlloAbilitazioniPV()
    Call frmControlloAbilitazioniPV.Init
End Sub

Private Sub mnuCopiaCPVTraOrganizzazioni_Click()
    Call CaricaFormCopiaCPVTraOrganizzazioni
End Sub

Private Sub CaricaFormCopiaCPVTraOrganizzazioni()
    Call frmCopiaCPVTraOrganizzazioni.Init
End Sub

Private Sub mnuFileEsci_Click()
    Call Esci
End Sub

Public Sub Esci()
    Dim stringaNota As String
    
    If userID <> "" Then
        stringaNota = "USERID = " & userID
        Call ScriviLog(CCTA_LOGOUT, CCDA_NON_SPECIFICATO, , stringaNota)
    End If
    Unload Me
    End
End Sub

Private Sub mnuHTDaStampare_Click()
    Call CaricaFormHTDaStampare
End Sub

Private Sub CaricaFormHTDaStampare()
    Call frmVerificaHTDaStampare.Init
End Sub

Private Sub mnuHTLayout_Click()
    Call CaricaFormConfigurazioneHTLayout
End Sub

Private Sub CaricaFormConfigurazioneHTLayout()
    Call frmConfigurazioneHTLayout.Init
End Sub

Private Sub mnuInfo_Click()
    Call CaricaFormAutori
End Sub

Private Sub CaricaFormAutori()
    Call frmAutori.Init
End Sub

Private Sub mnuGenereCartaDiPagamento_Click()
    Call CaricaFormGenereCartaDiPagamento
End Sub

Private Sub CaricaFormGenereCartaDiPagamento()
    Call frmSceltaGenereCartaDiPagamento.Init
End Sub

Private Sub mnuInserimentoProdottoRappresentazione_Click()
    Call CaricaFormInserimentoProdottoRappresentazione
End Sub

Private Sub CaricaFormInserimentoProdottoRappresentazione()
    Call frmInserimentoProdottoRappresentazione.Init
End Sub

Private Sub mnuLogAttivitą_Click()
    Call CaricaFormLogAttivitą
End Sub

Private Sub mnuManifestazione_Click()
    Call CaricaFormConfigurazioneManifestazione
End Sub

Private Sub CaricaFormConfigurazioneManifestazione()
    Call frmConfigurazioneManifestazione.Init
End Sub

Private Sub mnuModalitąPagamento_Click()
    Call CaricaFormConfigurazioneModalitaDiPagamento
End Sub

Private Sub CaricaFormResetPassword()
    Call frmResetPassword.Init
End Sub

Private Sub CaricaFormConfigurazioneModalitaDiPagamento()
    Call frmConfigurazioneModalitaDiPagamento.Init
End Sub

Private Sub mnuModificaDataRappresentazione_Click()
    Call CaricaFormModificaDataRappresentazione
End Sub

Private Sub CaricaFormModificaDataRappresentazione()
    Call frmModificaDataRappresentazione.Init
End Sub
Private Sub mnuNuovaSessione_Click()
    Call CaricaFormConfigurazioneNuovaSessione
End Sub

Private Sub mnuOrganizzazione_Click()
    Call CaricaFormSceltaOrganizzazione
End Sub

Private Sub CaricaFormSceltaOrganizzazione()
    Call frmSceltaOrganizzazione.Init
End Sub

Private Sub mnuOrganizzazioneLayoutSupporto_Click()
    CaricaFormConfigurazioneLayoutSupporto
End Sub

Private Sub mnuOrganizzazioneOperatore_Click()
    Call CaricaFormSceltaOperatore
End Sub

Private Sub mnuOrganizzazioneOrganizzazione_Click()
    Call CaricaFormSceltaOrganizzazione
End Sub

Private Sub mnuOffertaPacchetto_Click()
    Call CaricaFormSceltaOffertaPacchetto
End Sub

Private Sub CaricaFormSceltaOffertaPacchetto()
    Call frmSceltaPacchetto.Init
End Sub

Private Sub mnuReportRimborsi_Click()
    Call CaricaFormReportRimborsi
End Sub

Private Sub CaricaFormReportRimborsi()
    Call frmReportRimborsi.Init
End Sub

Private Sub mnuSito_Click()
    Call CaricaFormSceltaSito
End Sub

Private Sub CaricaFormSceltaSito()
    Call frmSceltaSito.Init
End Sub

Private Sub mnuImpianto_Click()
    Call CaricaFormSceltaImpianto
End Sub

Private Sub CaricaFormSceltaImpianto()
    Call frmSceltaImpianto.Init
End Sub

Private Sub mnuPianta_Click()
    Call CaricaFormSceltaPianta
End Sub

Private Sub CaricaFormSceltaPianta()
    Call frmSceltaPianta.Init(VB_FALSO)
End Sub

Private Sub mnuPiantaCapienzaIllimitata_Click()
    Call CaricaFormSceltaPiantaCapienzaIllimitata
End Sub

Private Sub CaricaFormSceltaPiantaCapienzaIllimitata()
    Call frmSceltaPianta.Init(VB_VERO)
End Sub

Private Sub mnuPiantaPianta_Click()
    Call CaricaFormSceltaPianta
End Sub

Private Sub mnuPiantaPiantaSIAE_Click()
    Call CaricaFormSceltaPiantaSIAE
End Sub

Private Sub mnuPiantaSIAE_Click()
    Call CaricaFormSceltaPiantaSIAE
End Sub

Private Sub CaricaFormSceltaPiantaSIAE()
    Call frmSceltaPiantaSIAE.Init
End Sub

Private Sub mnuProdotto_Click()
    Call CaricaFormSceltaProdotto
End Sub

Private Sub CaricaFormSceltaProdotto()
    Call frmSceltaProdotto.Init
End Sub

Private Sub mnuGruppoProdotti_Click()
    Call CaricaFormConfigurazioneGruppoProdotti
End Sub

Private Sub CaricaFormConfigurazioneGruppoProdotti()
    Call frmConfigurazioneGruppoProdotti.Init
End Sub

Private Sub mnuProdottoGruppoProdotti_Click()
    Call CaricaFormConfigurazioneGruppoProdotti
End Sub

Private Sub mnuProdottoOffertaPacchetto_Click()
    Call CaricaFormSceltaOffertaPacchetto
End Sub

Private Sub mnuProdottoProdotto_Click()
    Call CaricaFormSceltaProdotto
End Sub

Private Sub mnuProdottoStagione_Click()
    Call CaricaFormConfigurazioneStagione
End Sub

Private Sub mnuResetPassword_Click()
'    Call CaricaFormResetPassword
End Sub

Private Sub mnuSbloccoForzatoProdotto_Click()
    Call CaricaFormSbloccoForzatoProdotto
End Sub

Private Sub CaricaFormSbloccoForzatoProdotto()
    Call frmSbloccoForzatoProdotti.Init
End Sub

Private Sub mnuReportStatoPosti_Click()
    Call CaricaFormReportStatoPosti
End Sub

Private Sub CaricaFormReportStatoPosti()
    Call frmReportStatoPosti.Init
End Sub

Private Sub mnuReportPianta_Click()
    Call CaricaFormReportPianta
End Sub

Private Sub CaricaFormReportPianta()
    Call frmReportPianta.Init
End Sub

Private Sub mnuSistemaModalitąPagamento_Click()
    Call CaricaFormConfigurazioneModalitaDiPagamento
End Sub

Private Sub mnuSistemaPuntoVendita_Click()
    Call CaricaFormSceltaPuntoVendita
End Sub

Private Sub mnuSistemaSottorete_Click()
    Call CaricaFormConfigurazioneSottorete
End Sub

Private Sub mnuSistemaSpettacolo_Click()
    Call CaricaFormSceltaSpettacolo
End Sub

Private Sub mnuSistemaTariffa_Click()
    Call CaricaFormConfigurazioneTariffa
End Sub

Private Sub mnuSistemaTipoSupporto_Click()
    Call CaricaFormConfigurazioneTipoSupporto
End Sub

Private Sub mnuSistemaTurnoRappresentazione_Click()
    Call CaricaFormConfigurazioneTurnoRappresentazione
End Sub

Private Sub mnuSistemaVenditoreEsterno_Click()
    Call CaricaFormConfigurazioneVenditoriEsterni
End Sub

Private Sub mnuSistemaVenue_Click()
    Call CaricaFormConfigurazioneVenue
End Sub

Private Sub mnuSostituzioneLayoutSupporto_Click()
    Call CaricaFormModificaAssociazioneLayoutSupporto
End Sub

Private Sub CaricaFormModificaAssociazioneLayoutSupporto()
    Call frmModificaAssociazioneLayoutSupporto.Init
End Sub

Private Sub mnuSostituzioneTipoSupporto_Click()
    Call CaricaFormModificaAssociazioneTipoSupporto
End Sub

Private Sub CaricaFormModificaAssociazioneTipoSupporto()
    Call frmModificaAssociazioneTipoSupporto.Init
End Sub

Private Sub mnuSottorete_Click()
    Call CaricaFormConfigurazioneSottorete
End Sub

Private Sub CaricaFormConfigurazioneSottorete()
    Call frmConfigurazioneSottorete.Init
End Sub

Private Sub mnuSpettacolo_Click()
    Call CaricaFormSceltaSpettacolo
End Sub

Private Sub CaricaFormSceltaSpettacolo()
    Call frmSceltaSpettacolo.Init
End Sub

Private Sub mnuStagione_Click()
    Call CaricaFormConfigurazioneStagione
End Sub

Private Sub CaricaFormConfigurazioneStagione()
    Call frmConfigurazioneStagione.Init
End Sub

Private Sub mnuTariffa_Click()
Call CaricaFormConfigurazioneTariffa
End Sub

Private Sub CaricaFormConfigurazioneTariffa()
    Call frmConfigurazioneTariffe.Init
End Sub
'
'Private Sub mnuTemplate_Click()
'    Call CaricaFormConfigurazioneTemplate
'End Sub
'
'Private Sub CaricaFormConfigurazioneTemplate()
'    Call frmConfigurazioneTemplate.Init
'End Sub
Private Sub mnuTipoSupporto_Click()
    Call CaricaFormConfigurazioneTipoSupporto
End Sub

Private Sub CaricaFormConfigurazioneTipoSupporto()
    Call frmConfigurazioneTipoSupporto.Init
End Sub

Private Sub mnuLayoutSupporto_Click()
    Call CaricaFormConfigurazioneLayoutSupporto
End Sub

Private Sub CaricaFormConfigurazioneLayoutSupporto()
    Call frmConfigurazioneLayoutSupporto.Init
End Sub

Private Sub mnuPuntoVendita_Click()
    Call CaricaFormSceltaPuntoVendita
End Sub

Private Sub CaricaFormSceltaPuntoVendita()
    Call frmSceltaPuntoVendita.Init
End Sub

Private Sub mnuTurno_Click()
    Call CaricaFormConfigurazioneTurnoRappresentazione
End Sub

Private Sub CaricaFormConfigurazioneTurnoRappresentazione()
    Call frmConfigurazioneTurnoRappresentazione.Init
End Sub

Private Sub mnuVenditoriEsterni_Click()
    Call CaricaFormConfigurazioneVenditoriEsterni
End Sub

Private Sub CaricaFormConfigurazioneVenditoriEsterni()
    Call frmConfigurazioneVenditoriEsterni.Init
End Sub

Private Sub mnuVenue_Click()
    Call CaricaFormConfigurazioneVenue
End Sub

Private Sub CaricaFormConfigurazioneVenue()
    Call frmConfigurazioneVenue.Init
End Sub

Private Sub mnuOperatore_Click()
    Call CaricaFormSceltaOperatore
End Sub

Private Sub CaricaFormSceltaOperatore()
    Call frmSceltaOperatore.Init
End Sub

Private Sub mnuOperatoreAbilitabileAIVAPreassoltaObbligatoria_Click()
    Call CaricaFormSceltaOperatoreAbilitabileAIVAPreassoltaObbligatoria
End Sub

Private Sub CaricaFormSceltaOperatoreAbilitabileAIVAPreassoltaObbligatoria()
    Call frmSceltaOperatoreAbilitabileAIVAPreassoltaObbligatoria.Init
End Sub

Private Sub mnuSistemiEsterni_Click()
    CaricaFormSistemiEsterni
End Sub

Private Sub CaricaFormSistemiEsterni()
    Call frmSistemiEsterni.Init
End Sub

Private Sub mnuSoldOut_Click()
    Call CaricaFormPianificazioneSoldOut
End Sub

Private Sub CaricaFormPianificazioneSoldOut()
    Call frmPianificazioneOperazioni.Init
End Sub

Public Sub Init()
    idSessioneConfigurazioneCorrente = idNessunElementoSelezionato
    Call AggiornaAbilitazioneControlli
    Call Me.Show
End Sub

Private Sub tmrRipristinoConnessione_Init()
    tmrRipristinoConnessione.Interval = INTERVALLO_RIPRISTINO_CONNESSIONE
End Sub

Private Sub tmrRipristinoConnessione_Timer()
    Call RipristinaConnessione
End Sub

Private Sub AggiornaAbilitazioneControlli()
    Select Case tipoModalitąConfigurazione
        Case TMC_DIRETTA
            mnuConfigurazione.Visible = True
            mnuAggiornaStato.Visible = False
    End Select
End Sub
