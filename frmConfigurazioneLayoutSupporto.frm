VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneLayoutSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Layout Supporto"
   ClientHeight    =   9930
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9930
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkValidoPerTitoloDiAccesso 
      Alignment       =   1  'Right Justify
      Caption         =   "Valido per titolo d'accesso"
      Height          =   255
      Left            =   1320
      TabIndex        =   32
      Top             =   7560
      Width           =   2295
   End
   Begin VB.CheckBox chkAttivo 
      Alignment       =   1  'Right Justify
      Caption         =   "Attivo"
      Height          =   255
      Left            =   2640
      TabIndex        =   31
      Top             =   7080
      Width           =   975
   End
   Begin VB.CheckBox chkLayoutSupportiAttivi 
      Caption         =   "solo layout supporti attivi"
      Height          =   195
      Left            =   4440
      TabIndex        =   30
      Top             =   960
      Value           =   1  'Checked
      Width           =   2655
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   28
      Top             =   960
      Width           =   4095
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   6780
      Width           =   435
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   15
      Top             =   9360
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   17
      Top             =   8880
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   4
      TabIndex        =   5
      Top             =   7200
      Width           =   615
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   16
      Top             =   5280
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   8160
      Width           =   3615
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   6540
      Width           =   3555
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   4140
      MultiSelect     =   2  'Extended
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   6720
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   8040
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   7620
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   7200
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   8220
      MultiSelect     =   2  'Extended
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   6720
      Width           =   3555
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneLayoutSupporto 
      Height          =   330
      Left            =   240
      Top             =   4500
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneLayoutSupporto 
      Height          =   3495
      Left            =   120
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1440
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6165
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   29
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Layout Supporto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   27
      Top             =   120
      Width           =   8595
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   6960
      Width           =   675
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   25
      Top             =   5040
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   7920
      Width           =   1575
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   6300
      Width           =   1695
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati"
      Height          =   195
      Left            =   8220
      TabIndex        =   21
      Top             =   6480
      Width           =   3555
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   4140
      TabIndex        =   20
      Top             =   6480
      Width           =   3555
   End
   Begin VB.Label lblTipiSupporto 
      Alignment       =   2  'Center
      Caption         =   "TIPI SUPPORTO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4140
      TabIndex        =   19
      Top             =   6240
      Width           =   7635
   End
End
Attribute VB_Name = "frmConfigurazioneLayoutSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private isRecordEditabile As Boolean
Private nomeRecordSelezionato As String
Private codiceRecordSelezionato As String
Private attivoRecordSelezionato As ValoreBooleanoEnum
Private validoPerAccessoRecordSelezionato As ValoreBooleanoEnum
Private descrizioneRecordSelezionato As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private listaAppoggioLayoutTipiSupporto As Collection
'Private listaCampiValoriUnici As Collection
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
        
    dgrConfigurazioneLayoutSupporto.Caption = "LAYOUT SUPPORTO CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneLayoutSupporto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtCodice.Text = ""
        txtDescrizione.Text = ""
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtNome.Enabled = False
        txtCodice.Enabled = False
        chkAttivo.Enabled = False
        chkValidoPerTitoloDiAccesso.Enabled = False
        txtDescrizione.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblCodice.Enabled = False
        lblTipiSupporto.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneLayoutSupporto.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkAttivo.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkValidoPerTitoloDiAccesso.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipiSupporto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                               Len(Trim(txtCodice)) = 4)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneLayoutSupporto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtCodice.Text = ""
        txtDescrizione.Text = ""
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        txtNome.Enabled = False
        txtCodice.Enabled = False
        chkAttivo.Enabled = False
        chkValidoPerTitoloDiAccesso.Enabled = False
        txtDescrizione.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblCodice.Enabled = False
        lblTipiSupporto.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String

    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggioLayoutTipiSupporto
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String

    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    isRecordEditabile = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    nomeRecordSelezionato = ""
    descrizioneRecordSelezionato = ""
    attivoRecordSelezionato = VB_VERO
    chkAttivo.Value = attivoRecordSelezionato
    validoPerAccessoRecordSelezionato = VB_VERO
    chkValidoPerTitoloDiAccesso.Value = validoPerAccessoRecordSelezionato
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String

    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneLayoutSupporto_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    idOrganizzazioneSelezionata = idNessunElementoSelezionato

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioLayoutTipiSupporto
    
    Call CaricaOrganizzazioni
    Call AggiornaAbilitazioneControlli
    
'    Call adcConfigurazioneLayoutSupporto_Init
'    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
'    Call dgrConfigurazioneLayoutSupporto_Init
    Call Me.Show(vbModal)

End Sub

Private Sub CaricaOrganizzazioni()
    Dim sql As String
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE"
    sql = sql & " ORDER BY NOME"
    Call CaricaValoriComboConOpzioneTutti(cmbOrganizzazione, sql, "NOME")

    MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = Me.MousePointer
    Me.MousePointer = vbHourglass
    Call cmbOrganizzazione_Update
    
    Me.MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Update()
    
    idOrganizzazioneSelezionata = Me.cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    Call adcConfigurazioneLayoutSupporto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneLayoutSupporto_Init
    
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneLayoutSupporto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub adcConfigurazioneLayoutSupporto_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioLayoutTipiSupporto
    
    Set d = adcConfigurazioneLayoutSupporto
    
'    sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO AS ""ID""," & _
'       " LS.CODICE AS ""Codice""," & _
'       " LS.NOME AS ""Nome""," & _
'       " LS.DESCRIZIONE AS ""Descrizione""," & _
'       " TMP.NOME AS ""Tipi associati""," & _
'       " DECODE(R.IDLAYOUTSUPPORTO, NULL, 'NO', 'SI') ""Comandi stampa definiti""" & _
'       " FROM LAYOUTSUPPORTO LS, RIGALAYOUTSUPPORTO R, " & nomeTabellaTemporanea & " TMP" & _
'       " WHERE TMP.IDLAYOUT(+) = LS.IDLAYOUTSUPPORTO" & _
'       " AND LS.IDLAYOUTSUPPORTO = R.IDLAYOUTSUPPORTO(+)" & _
'       " ORDER BY ""Codice"""
'    MsgBox "adcConfigurazioneLayoutSupporto_Init: " & ContaRecord(sql)
    
    sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO AS ""ID""," & _
       " LS.CODICE AS ""Codice""," & _
       " LS.NOME AS ""Nome""," & _
       " LS.DESCRIZIONE AS ""Descrizione""," & _
       " TMP.NOME AS ""Tipi associati""," & _
       " DECODE(R.IDLAYOUTSUPPORTO, NULL, 'NO', 'SI') ""Comandi stampa definiti""," & _
       " DECODE(ATTIVO, 0, 'NO', 'SI') ""Attivo""" & _
       " FROM LAYOUTSUPPORTO LS, RIGALAYOUTSUPPORTO R, " & nomeTabellaTemporanea & " TMP" & _
       " WHERE TMP.IDLAYOUT = LS.IDLAYOUTSUPPORTO" & _
       " AND LS.IDLAYOUTSUPPORTO = R.IDLAYOUTSUPPORTO(+)"
       
    If chkLayoutSupportiAttivi.Value = vbChecked Then
        sql = sql & " AND LS.ATTIVO = 1"
    End If
    
    sql = sql & " ORDER BY ""Codice"""
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh

    Set dgrConfigurazioneLayoutSupporto.dataSource = d

    internalEvent = internalEventOld
    
'    MsgBox "adcConfigurazioneLayoutSupporto_Init: " & ContaRecord(sql)
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoLayoutSupporto As Long
    Dim n As Long
    Dim tipo As clsElementoLista
    
    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovoLayoutSupporto = OttieniIdentificatoreDaSequenza("SQ_LAYOUTSUPPORTO")
    sql = "INSERT INTO LAYOUTSUPPORTO (IDLAYOUTSUPPORTO, NOME, DESCRIZIONE, CODICE, ATTIVO, VALIDOPERTITOLODIACCESSO)" & _
        " VALUES (" & _
        idNuovoLayoutSupporto & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        SqlStringValue(codiceRecordSelezionato) & ", " & _
        IIf(attivoRecordSelezionato = VB_VERO, "1", "0") & ", " & _
        IIf(validoPerAccessoRecordSelezionato = VB_VERO, "1", "0") & ")"
    SETAConnection.Execute sql, n, adCmdText
    
'   INSERIMENTO IN TABELLA LAYOUTSUPPORTO_TIPOSUPPORTO
    If Not (listaSelezionati Is Nothing) Then
        For Each tipo In listaSelezionati
            sql = "INSERT INTO LAYOUTSUPPORTO_TIPOSUPPORTO (IDLAYOUTSUPPORTO, IDTIPOSUPPORTO)" & _
                " VALUES (" & idNuovoLayoutSupporto & ", " & _
                tipo.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next tipo
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoLayoutSupporto)
    Call AggiornaAbilitazioneControlli
    
Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    isRecordEditabile = True
    sql = "SELECT NOME, CODICE, DESCRIZIONE, ATTIVO, VALIDOPERTITOLODIACCESSO" & _
        " FROM LAYOUTSUPPORTO" & _
        " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        codiceRecordSelezionato = rec("CODICE")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        attivoRecordSelezionato = IIf(rec("ATTIVO") = 1, VB_VERO, VB_FALSO)
        validoPerAccessoRecordSelezionato = IIf(rec("VALIDOPERTITOLODIACCESSO") = 1, VB_VERO, VB_FALSO)
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = nomeRecordSelezionato
    txtCodice.Text = codiceRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    chkAttivo.Value = attivoRecordSelezionato
    chkValidoPerTitoloDiAccesso.Value = validoPerAccessoRecordSelezionato

    internalEvent = internalEventOld

End Sub

Private Sub dgrConfigurazioneLayoutSupporto_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneLayoutSupporto
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 6
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneLayoutSupporto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitą As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitą = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDLAYOUTSUPPORTO <> " & idRecordSelezionato
    End If
    
    attivoRecordSelezionato = IIf(chkAttivo.Value = vbChecked, VB_VERO, VB_FALSO)
    validoPerAccessoRecordSelezionato = IIf(chkValidoPerTitoloDiAccesso.Value = vbChecked, VB_VERO, VB_FALSO)
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If attivoRecordSelezionato = VB_VERO Then
        If ViolataUnicitą("LAYOUTSUPPORTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "", _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
                " č gią presente in DB;")
        End If
    End If
    
    codiceRecordSelezionato = Trim(txtCodice.Text)
    If attivoRecordSelezionato = VB_VERO Then
        If ViolataUnicitą("LAYOUTSUPPORTO", "CODICE = " & SqlStringValue(codiceRecordSelezionato), "", _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore codice = " & SqlStringValue(codiceRecordSelezionato) & _
                " č gią presente in DB;")
        End If
    End If
    
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCodice_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'    Call listaCampiValoriUnici.Add("CODICE = " & SqlStringValue(codiceRecordSelezionato))
'End Sub

Private Sub cmdDidsponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstDisponibili
    
    MousePointer = mousePointerOld
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim listaTipiNonEliminabili As Collection
    Dim idTipo As Long
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    Dim condizioneSql As String
    
    Set listaTipiNonEliminabili = New Collection
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idTipo = lstSelezionati.ItemData(i - 1)
            chiaveTipo = ChiaveId(idTipo)
            Set tipo = listaSelezionati.Item(chiaveTipo)
            condizioneSql = " AND IDLAYOUTSUPPORTO = " & idRecordSelezionato
            If IsRecordEliminabile("IDTIPOSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idTipo), condizioneSql) Then
                Call listaDisponibili.Add(tipo, chiaveTipo)
                Call listaSelezionati.Remove(chiaveTipo)
            Else
                Call listaTipiNonEliminabili.Add(tipo.descrizioneElementoLista)
            End If
        End If
    Next i
    If listaTipiNonEliminabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", ArgomentoMessaggio(listaTipiNonEliminabili), "ORGANIZ_TIPOSUP_LAYOUTSUP")
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim labelTipo As String
    Dim chiaveTipo As String
    Dim tipoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Then
        sql = "SELECT NOME, CODICE, IDTIPOSUPPORTO IDTS" & _
            " FROM TIPOSUPPORTO ORDER BY CODICE"
    Else
        sql = "SELECT DISTINCT TS.NOME, TS.CODICE, TS.IDTIPOSUPPORTO IDTS" & _
            " FROM TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LT" & _
            " WHERE TS.IDTIPOSUPPORTO = LT.IDTIPOSUPPORTO(+)" & _
            " AND LT.IDTIPOSUPPORTO IS NULL" & _
            " AND LT.IDLAYOUTSUPPORTO(+) = " & idRecordSelezionato & _
            " ORDER BY CODICE"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoCorrente = New clsElementoLista
            tipoCorrente.nomeElementoLista = rec("NOME")
            tipoCorrente.codiceElementoLista = rec("CODICE")
            labelTipo = rec("CODICE") & " - " & rec("NOME")
            tipoCorrente.descrizioneElementoLista = labelTipo
            tipoCorrente.idElementoLista = rec("IDTS").Value
            chiaveTipo = ChiaveId(tipoCorrente.idElementoLista)
            Call listaDisponibili.Add(tipoCorrente, chiaveTipo)
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim labelTipo As String
    Dim chiaveTipo As String
    Dim tipoCorrente As clsElementoLista
    
    Call ApriConnessioneBD

    Set listaSelezionati = New Collection
    If gestioneRecordGriglia <> ASG_INSERISCI_NUOVO Then
        sql = "SELECT LSTS.IDLAYOUTSUPPORTO," & _
            " TS.IDTIPOSUPPORTO AS ""IDTIPOSUPPORTO"", TS.NOME, TS.CODICE" & _
            " FROM LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
            " AND TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
            " AND LSTS.IDLAYOUTSUPPORTO = " & idRecordSelezionato & _
            " ORDER BY CODICE"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set tipoCorrente = New clsElementoLista
                tipoCorrente.nomeElementoLista = rec("NOME")
                tipoCorrente.codiceElementoLista = rec("CODICE")
                labelTipo = rec("CODICE") & " - " & rec("NOME")
                tipoCorrente.descrizioneElementoLista = labelTipo
                tipoCorrente.idElementoLista = rec("IDTIPOSUPPORTO").Value
                chiaveTipo = ChiaveId(tipoCorrente.idElementoLista)
                Call listaSelezionati.Add(tipoCorrente, chiaveTipo)
                rec.MoveNext
            Wend
        End If
        
        rec.Close
        Call ChiudiConnessioneBD
        
        Call lstSelezionati_Init
    Else
        'do Nothing
    End If
    
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tipo As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each tipo In listaDisponibili
            lstDisponibili.AddItem tipo.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = tipo.idElementoLista
            i = i + 1
        Next tipo
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tipo As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each tipo In listaSelezionati
            lstSelezionati.AddItem tipo.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = tipo.idElementoLista
            i = i + 1
        Next tipo
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idTipo As Long
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idTipo = lstDisponibili.ItemData(i - 1)
            chiaveTipo = ChiaveId(idTipo)
            Set tipo = listaDisponibili.Item(chiaveTipo)
            Call listaSelezionati.Add(tipo, chiaveTipo)
            Call listaDisponibili.Remove(chiaveTipo)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaSelezionati
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaSelezionati()
    Dim idTipo As Long
    Dim listaTipiNonEliminabili As Collection
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    Dim condizioneSql As String
    
    Set listaTipiNonEliminabili = New Collection
    For Each tipo In listaSelezionati
        idTipo = tipo.idElementoLista
        condizioneSql = " AND IDLAYOUTSUPPORTO = " & idRecordSelezionato
        If IsRecordEliminabile("IDTIPOSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idTipo), condizioneSql) Then
            chiaveTipo = ChiaveId(idTipo)
            Call listaDisponibili.Add(tipo, chiaveTipo)
            Call listaSelezionati.Remove(chiaveTipo)
        Else
            Call listaTipiNonEliminabili.Add(tipo.descrizioneElementoLista)
        End If
    Next tipo
    If listaTipiNonEliminabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", ArgomentoMessaggio(listaTipiNonEliminabili), "ORGANIZ_TIPOSUP_LAYOUTSUP")
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub CreaTabellaAppoggioLayoutTipiSupporto()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_LAYOUTTIPI_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDLAYOUT NUMBER(10), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggioLayoutTipiSupporto
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioLayoutTipiSupporto()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioLayoutTipiSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idLayoutSupp As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Integer
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioLayoutTipiSupporto = New Collection
    
'    sql = "SELECT DISTINCT LSTS.IDLAYOUTSUPPORTO ID" & _
'        " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
'        " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO"
'    MsgBox "PopolaTabellaAppoggioLayoutTipiSupporto (1): " & ContaRecord(sql)

    If idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT LSTS.IDLAYOUTSUPPORTO ID" & _
            " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
            " AND LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO" & _
            " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    Else
        sql = "SELECT DISTINCT LSTS.IDLAYOUTSUPPORTO ID" & _
            " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("ID").Value
            Call listaAppoggioLayoutTipiSupporto.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
'    MsgBox "PopolaTabellaAppoggioLayoutTipiSupporto (2): " & ContaRecord(sql)
    
    For Each recordTemporaneo In listaAppoggioLayoutTipiSupporto
        campoNome = ""
        ' aggiunta una condizione rownum per non inserire nulla
        sql = "SELECT LS.IDLAYOUTSUPPORTO, TS.IDTIPOSUPPORTO, TS.CODICE CODICE" & _
            " FROM LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
            " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
            " AND LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
            " AND LS.IDLAYOUTSUPPORTO = " & recordTemporaneo.idElementoLista & _
            " AND ROWNUM < 0" & _
            " ORDER BY TS.NOME"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("CODICE"), campoNome & "; " & rec("CODICE"))
                rec.MoveNext
            Wend
'            Call listaNomeAppoggioLayoutTipiSupporto.Add(campoNome)
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qui sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sł
    For Each recordTemporaneo In listaAppoggioLayoutTipiSupporto
        idLayoutSupp = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDLAYOUT, NOME)" & _
            " VALUES (" & idLayoutSupp & ", " & _
            SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub EliminaDallaBaseDati()
    Dim layoutEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim sql As String
    Dim n As Long
    
    Set listaTabelleCorrelate = New Collection
    
    layoutEliminabile = True
    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "ORGANIZ_TIPOSUP_LAYOUTSUP", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("ORGANIZ_TIPOSUP_LAYOUTSUP")
        layoutEliminabile = False
    End If
    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "UTILIZZOLAYOUTSUPPORTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTO")
        layoutEliminabile = False
    End If
    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("UTILIZZOLAYOUTSUPPORTOCPV")
        layoutEliminabile = False
    End If
    If Not IsRecordEliminabile("IDLAYOUTSUPPORTO", "STATOTITOLO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("STATOTITOLO")
        layoutEliminabile = False
    End If
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
        
    If layoutEliminabile Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                sql = "DELETE FROM LAYOUTSUPPORTO_TIPOSUPPORTO" & _
'                    " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'                sql = "DELETE FROM RIGALAYOUTSUPPORTO" & _
'                    " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'                sql = "DELETE FROM LAYOUTSUPPORTO WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'            Else
'                Call AggiornaParametriSessioneSuRecord("LAYOUTSUPPORTO_TIPOSUPPORTO", "IDLAYOUTSUPPORTO", idRecordSelezionato, TSR_ELIMINATO)
'                Call AggiornaParametriSessioneSuRecord("RIGALAYOUTSUPPORTO", "IDRIGALAYOUTSUPPORTO", idRecordSelezionato, TSR_ELIMINATO)
'                Call AggiornaParametriSessioneSuRecord("LAYOUTSUPPORTO", "IDLAYOUTSUPPORTO", idRecordSelezionato, TSR_ELIMINATO)
'            End If
'        Else
            sql = "DELETE FROM LAYOUTSUPPORTO_TIPOSUPPORTO" & _
                " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
            sql = "DELETE FROM RIGALAYOUTSUPPORTO" & _
                " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
            sql = "DELETE FROM LAYOUTSUPPORTO WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il Layout Supporto selezionato", _
                                        tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Sub SvuotaDisponibili()
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    
    For Each tipo In listaDisponibili
        chiaveTipo = ChiaveId(tipo.idElementoLista)
        Call listaSelezionati.Add(tipo, chiaveTipo)
    Next tipo
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim tipo As clsElementoLista
    Dim condizioneSql As String

    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE LAYOUTSUPPORTO SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " CODICE = " & SqlStringValue(codiceRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " ATTIVO = " & IIf(attivoRecordSelezionato = VB_VERO, 1, 0) & "," & _
        " VALIDOPERTITOLODIACCESSO = " & IIf(validoPerAccessoRecordSelezionato = VB_VERO, 1, 0) & _
        " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
'   AGGIORNAMENTO TABELLA LAYOUTSUPPORTO_TIPOSUPPORTO
    If Not (listaSelezionati Is Nothing) Then
        sql = "DELETE FROM LAYOUTSUPPORTO_TIPOSUPPORTO" & _
            " WHERE IDLAYOUTSUPPORTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        For Each tipo In listaSelezionati
            sql = "INSERT INTO LAYOUTSUPPORTO_TIPOSUPPORTO (IDLAYOUTSUPPORTO, IDTIPOSUPPORTO)" & _
                " VALUES (" & idRecordSelezionato & ", " & _
                tipo.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next tipo
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    stringaNota = "IDLAYOUTSUPPORTO = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli

    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_LAYOUT_SUPPORTO, CCDA_LAYOUT_SUPPORTO, stringaNota, idRecordSelezionato)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazioneLayoutSupporto_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazioneLayoutSupporto_Init
    Else
        If ValoriCampiOK Then
            If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_LAYOUT_SUPPORTO, CCDA_LAYOUT_SUPPORTO, stringaNota, idRecordSelezionato)
            ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_LAYOUT_SUPPORTO, CCDA_LAYOUT_SUPPORTO, stringaNota, idRecordSelezionato)
            End If
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazioneLayoutSupporto_Init
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
            Call dgrConfigurazioneLayoutSupporto_Init
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub



