VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazionePiantaClassiSuperaree 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   6675
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6675
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkPrincipale 
      Caption         =   "Principale"
      Height          =   375
      Left            =   7440
      TabIndex        =   19
      Top             =   5040
      Width           =   1935
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10560
      TabIndex        =   18
      Top             =   5760
      Width           =   1155
   End
   Begin VB.TextBox txtDescrizione 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3840
      MaxLength       =   30
      TabIndex        =   5
      Top             =   5100
      Width           =   3315
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9600
      TabIndex        =   15
      Top             =   240
      Width           =   2235
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   12
      Top             =   3780
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   5100
      Width           =   3315
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   8
      Top             =   5640
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePiantaClassiSuperaree 
      Height          =   330
      Left            =   6720
      Top             =   120
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePiantaClassiSuperaree 
      Height          =   2835
      Left            =   120
      TabIndex        =   17
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   5001
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9600
      TabIndex        =   16
      Top             =   0
      Width           =   2235
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   3540
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   13
      Top             =   3540
      Width           =   2775
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   3840
      TabIndex        =   11
      Top             =   4860
      Width           =   1695
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   4860
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Classi di Superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazionePiantaClassiSuperaree"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idPiantaSelezionata As Long
Private isRecordEditabile As Boolean
Private nomeRecordSelezionato As String
Private descrizioneRecordSelezionato As String
Private principale As ValoreBooleanoEnum
Private nomePiantaSelezionata As String

Private idNuovaClasseSuperarea As Long

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Pianta"
    txtInfo1.Text = nomePiantaSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazionePiantaClassiSuperaree.Caption = "CLASSI SUPERAREE CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaClassiSuperaree.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaClassiSuperaree.Enabled = False
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
                
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePiantaClassiSuperaree.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Esci"
        Case A_MODIFICA
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Esci()
    Unload Me
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetNomePiantaSelezionata(nome As String)
    nomePiantaSelezionata = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli

    If valoriCampiOK Then
        If isRecordEditabile Then
            stringaNota = "Classe superarea"
            Select Case gestioneRecordGriglia
                Case ASG_INSERISCI_NUOVO
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA, CCDA_SUPERAREA, stringaNota, idPiantaSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazionePiantaClassiSuperaree_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazionePiantaClassiSuperaree_Init
                Case ASG_INSERISCI_DA_SELEZIONE
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA, CCDA_SUPERAREA, stringaNota, idPiantaSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazionePiantaClassiSuperaree_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazionePiantaClassiSuperaree_Init
                Case ASG_MODIFICA
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_PIANTA, CCDA_SUPERAREA, stringaNota, idPiantaSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazionePiantaClassiSuperaree_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazionePiantaClassiSuperaree_Init
                Case ASG_ELIMINA
                    Call EliminaDallaBaseDati
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PIANTA, CCDA_SUPERAREA, stringaNota, idPiantaSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazionePiantaClassiSuperaree_Init
                    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                    Call dgrConfigurazionePiantaClassiSuperaree_Init
            End Select
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    
    isRecordEditabile = True
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePiantaClassiSuperaree.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
        
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazionePiantaClassiSuperaree_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    internalEvent = False
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazionePiantaClassiSuperaree_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePiantaClassiSuperaree_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePiantaClassiSuperaree.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        If Not IsNull(rec("ID")) Then
            idRecordSelezionato = rec("ID").Value
        Else
            idRecordSelezionato = idNessunElementoSelezionato
        End If
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePiantaClassiSuperaree_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazionePiantaClassiSuperaree

    sql = "SELECT IDCLASSESUPERAREA ID, NOME ""Nome"", DESCRIZIONE ""Descrizione"", CLASSEPRINCIPALE ""Principale"""
    sql = sql & " FROM CLASSESUPERAREA"
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
    sql = sql & " ORDER BY ""Nome"""
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazionePiantaClassiSuperaree.dataSource = d

    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA CLASSESUPERAREA
    idNuovaClasseSuperarea = OttieniIdentificatoreDaSequenza("SQ_CLASSESUPERAREA")
    sql = "INSERT INTO CLASSESUPERAREA (IDCLASSESUPERAREA, NOME, DESCRIZIONE, IDPIANTA, CLASSEPRINCIPALE)" & _
        " VALUES (" & _
        idNuovaClasseSuperarea & ", " & _
        SqlStringValue(nomeRecordSelezionato) & ", " & _
        SqlStringValue(descrizioneRecordSelezionato) & ", " & _
        idPiantaSelezionata & ", " & _
        IIf(principale, "1)", "0)")
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaClasseSuperarea)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
    sql = "SELECT NOME, DESCRIZIONE, CLASSEPRINCIPALE" & _
        " FROM CLASSESUPERAREA" & _
        " WHERE IDCLASSESUPERAREA = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = IIf(IsNull(rec("NOME")), "", rec("NOME"))
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        principale = IIf(rec("CLASSEPRINCIPALE") = 1, VB_VERO, VB_FALSO)
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    chkPrincipale.Value = principale
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    sql = "UPDATE CLASSESUPERAREA SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & _
        ", DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & _
        ", CLASSEPRINCIPALE = " & IIf(principale, "1", "0") & _
        " WHERE IDCLASSESUPERAREA = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazionePiantaClassiSuperaree_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePiantaClassiSuperaree
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call ValorizzaAutomaticamenteCampoCorrelato
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub ValorizzaAutomaticamenteCampoCorrelato()
    txtDescrizione.Text = Left$(txtNome.Text, 30)
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    principale = IIf(chkPrincipale.Value = vbChecked, VB_VERO, VB_FALSO)
    
'    Set listaNonConformitā = New Collection
'    condizioneSql = ""
'    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
'        condizioneSql = "IDSTAMPAAGGIUNTIVA <> " & idRecordSelezionato
'    End If
'
'    valoreRecordSelezionato = Trim(txtValore.Text)
'    If IsCampoInteroCorretto(txtCoordinataX) Then
'        coordinataXRecordSelezionato = CInt(Trim(txtCoordinataX.Text))
'        If coordinataXRecordSelezionato <= 0 Or coordinataXRecordSelezionato > 255 Then
''            Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata X", "con valori compresi tra 1 e 255")
'            ValoriCampiOK = False
'            Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata X deve essere numerico di tipo intero e compreso tra 1 e 255;")
'        End If
'    Else
''        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata X", "con valori compresi tra 1 e 255")
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata X deve essere numerico di tipo intero e compreso tra 1 e 255;")
'    End If
'    If IsCampoInteroCorretto(txtCoordinataY) Then
'        coordinataYRecordSelezionato = CInt(Trim(txtCoordinataY.Text))
'        If coordinataYRecordSelezionato <= 0 Or coordinataYRecordSelezionato > 255 Then
''            Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata Y", "con valori compresi tra 1 e 255")
'            ValoriCampiOK = False
'            Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata Y deve essere numerico di tipo intero e compreso tra 1 e 255;")
'        End If
'    Else
''        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Coordinata Y", "con valori compresi tra 1 e 255")
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore immesso sul campo Coordinata Y deve essere numerico di tipo intero e compreso tra 1 e 255;")
'    End If
'    If opt1.Value = True Then
'        progressivoRecordSelezionato = 1
'    ElseIf opt2.Value = True Then
'        progressivoRecordSelezionato = 2
'    ElseIf opt3.Value = True Then
'        progressivoRecordSelezionato = 3
'    End If
'    If ViolataUnicitā("STAMPAAGGIUNTIVA", "PROGRESSIVO = " & progressivoRecordSelezionato, "IDPRODOTTO = " & idProdottoSelezionato, _
'        condizioneSql, "", "") Then
'        ValoriCampiOK = False
'        Call listaNonConformitā.Add("- il valore progressivo = " & progressivoRecordSelezionato & _
'            " č giā presente in DB per lo stesso Prodotto;")
'    End If
'
'    If listaNonConformitā.count > 0 Then
'        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
'    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    Dim classeUtilizzata As Boolean
    Dim mess As String
    Dim numeroProdotti As Long

    Call ApriConnessioneBD
            
    classeUtilizzata = False
    numeroProdotti = 0
    sql = "SELECT P.IDPRODOTTO, P.NOME" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO P" & _
        " where IDCLASSESUPERAREA = " & idRecordSelezionato & _
        " AND CSP.IDPRODOTTO = P.IDPRODOTTO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        classeUtilizzata = True
        mess = "Impossibile cancellare l'elemento selezionato; e' utilizzato nei seguenti prodotti: "
        rec.MoveFirst
        While Not rec.EOF
            If numeroProdotti <= 10 Then
                mess = mess & rec("IDPRODOTTO") & " - " & rec("NOME") & ","
            End If
            numeroProdotti = numeroProdotti + 1
            rec.MoveNext
        Wend
        mess = mess & "... per un totale di " & numeroProdotti & " prodotti."
        MsgBox mess
    End If
    rec.Close
            
    If classeUtilizzata = False Then
        sql = "DELETE FROM CLASSESUPERAREA WHERE IDCLASSESUPERAREA = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
    End If
    
    Call ChiudiConnessioneBD

End Sub

