VERSION 5.00
Begin VB.Form frmClonazioneParzialeProdotto 
   Caption         =   "Clonazione parziale prodotto"
   ClientHeight    =   8235
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15855
   LinkTopic       =   "Form1"
   ScaleHeight     =   8235
   ScaleWidth      =   15855
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmProdottiSlave 
      Caption         =   "Prodotti slave"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Left            =   9480
      TabIndex        =   11
      Top             =   720
      Width           =   6255
      Begin VB.CommandButton cmdSelezionaTutti 
         Caption         =   "Seleziona tutti"
         Height          =   495
         Left            =   120
         TabIndex        =   17
         Top             =   5400
         Width           =   1455
      End
      Begin VB.ListBox lstProdottiSlave 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4620
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   12
         Top             =   660
         Width           =   6000
      End
      Begin VB.Label lblProdottiSlave 
         Caption         =   "Prodotti non scaduti"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   420
         Width           =   5775
      End
   End
   Begin VB.Frame frmDati 
      Caption         =   "Informazioni da clonare"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Left            =   6480
      TabIndex        =   9
      Top             =   720
      Width           =   2775
      Begin VB.Frame frmDataTermine 
         Caption         =   "Data di termine"
         Height          =   1335
         Left            =   120
         TabIndex        =   23
         Top             =   3000
         Width           =   2535
         Begin VB.OptionButton optTermineStessaData 
            Caption         =   "Stessa data"
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   240
            Width           =   2175
         End
         Begin VB.OptionButton optTermineInizioSpettacolo 
            Caption         =   "In base a inizio spettacolo"
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   600
            Value           =   -1  'True
            Width           =   2295
         End
         Begin VB.OptionButton optTermineFineSpettacolo 
            Caption         =   "In base a fine spettacolo"
            Height          =   255
            Left            =   120
            TabIndex        =   24
            Top             =   960
            Width           =   2175
         End
      End
      Begin VB.Frame frmDataPartenza 
         Caption         =   "Data di partenza"
         Height          =   1335
         Left            =   120
         TabIndex        =   19
         Top             =   1560
         Width           =   2535
         Begin VB.OptionButton optPartenzaFineSpettacolo 
            Caption         =   "In base a fine spettacolo"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   960
            Width           =   2175
         End
         Begin VB.OptionButton optPartenzaInizioSpettacolo 
            Caption         =   "In base a inizio spettacolo"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   600
            Width           =   2295
         End
         Begin VB.OptionButton optPartenzaStessaData 
            Caption         =   "Stessa data"
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   240
            Value           =   -1  'True
            Width           =   2175
         End
      End
      Begin VB.CheckBox chkAbilitazioniTariffeRetiDiVendita 
         Caption         =   "Abilitazione tariffe a reti di vendita"
         Height          =   495
         Left            =   120
         TabIndex        =   16
         Top             =   5400
         Width           =   2295
      End
      Begin VB.CheckBox chkLayoutSupporto 
         Caption         =   "Associazione layout supporti"
         Height          =   495
         Left            =   120
         TabIndex        =   15
         Top             =   4680
         Width           =   2295
      End
      Begin VB.CheckBox chkDateOperazioni 
         Caption         =   "Date operazioni"
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   1080
         Width           =   2295
      End
      Begin VB.CheckBox chkRetiVendita 
         Caption         =   "Reti di vendita"
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.Frame frmProdottoMaster 
      Caption         =   "Prodotto master"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   6255
      Begin VB.ComboBox cmbOrganizzazioni 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   600
         Width           =   6000
      End
      Begin VB.ListBox lstProdotti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4470
         Left            =   120
         TabIndex        =   5
         Top             =   1380
         Width           =   6000
      End
      Begin VB.Label lblOrganizzazioni 
         Caption         =   "Organizzazioni attive"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label lblProdottiNonScaduti 
         Caption         =   "Prodotti non scaduti"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   1140
         Width           =   3255
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   12600
      TabIndex        =   0
      Top             =   7200
      Width           =   3135
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Esci"
         Height          =   435
         Left            =   1680
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   240
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblAvanzamento 
      Height          =   375
      Left            =   120
      TabIndex        =   18
      Top             =   6960
      Width           =   10695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Clonazione parziale di prodotti esistenti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   7275
   End
End
Attribute VB_Name = "frmClonazioneParzialeProdotto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean
Private gestioneExitCode As ExitCodeEnum
Private idOrganizzazioneSelezionata As Long
Private idProdottoMaster As Long
Private listaIdProdottiSlave As Collection

Public Sub Init()
    ResettaControlli
    CaricaValoriCmbOrganizzazioni
    AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
'    cmdConferma.Enabled = (idNuovoTipoSupportoSelezionato <> valoreLongNullo)
End Sub

Private Sub ResettaControlli()
    OrganizzazioneSelezionata_Reset
    ProdottoMaster_Reset
    lblAvanzamento.Caption = ""
End Sub

Private Sub OrganizzazioneSelezionata_Reset()
    idOrganizzazioneSelezionata = valoreLongNullo
    cmbOrganizzazioni.Clear
End Sub

Private Sub ProdottoMaster_Reset()
    idProdottoMaster = valoreLongNullo
    lstProdotti.Clear
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub chkRetiVendita_Click()
    Call CaricaValoriLstProdottiSlave
End Sub

Private Sub chkDateOperazioni_Click()
    Call CaricaValoriLstProdottiSlave
End Sub

Private Sub chkLayoutSupporto_Click()
    Call CaricaValoriLstProdottiSlave
End Sub

Private Sub chkAbilitazioniTariffeRetiDiVendita_Click()
    Call CaricaValoriLstProdottiSlave
End Sub

Private Sub cmbOrganizzazioni_Click()
    idOrganizzazioneSelezionata = cmbOrganizzazioni.ItemData(cmbOrganizzazioni.ListIndex)
    
    idProdottoMaster = valoreLongNullo
    lstProdotti.Clear
    lstProdottiSlave.Clear
    
    Call CaricaValoriLstProdotti
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    ResettaControlli
    CaricaValoriCmbOrganizzazioni
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim risposta As VbMsgBoxResult
    Dim sql As String
    Dim sqlCatalogo As String
    Dim stringaIdProdottiSelezionati As String
    Dim righeAggiornate As Long
    
On Error GoTo gestioneErroriConfermaClonazioneParziale
    
    stringaIdProdottiSelezionati = SelezionatiSuListBoxToStringa(lstProdottiSlave, ",")
    
    risposta = MsgBox("Sei sicuro di voler procedere con la clonazione cancellando i dati esistenti?", vbYesNo)
    If risposta = vbYes Then
        Call ApriConnessioneBD_ORA
        Call ORADB.BeginTrans
    
        If chkRetiVendita.Value = vbChecked Then
            lblAvanzamento.Caption = "Clonazione reti di vendita ..."
            DoEvents
            
            sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
                " WHERE IDCLASSESUPERAREAPRODOTTO IN (" & _
                " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                " FROM CLASSESUPERAREAPRODOTTO" & _
                " WHERE IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                " )"
            righeAggiornate = ORADB.ExecuteSQL(sql)
    
            sql = "INSERT INTO CLASSESAPROD_PUNTOVENDITA (IDCLASSESUPERAREAPRODOTTO, IDPUNTOVENDITA)" & _
                " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, CPV_M.IDPUNTOVENDITA" & _
                " FROM CLASSESAPROD_PUNTOVENDITA CPV_M, CLASSESUPERAREAPRODOTTO CSP_M, CLASSESUPERAREAPRODOTTO CSP_S" & _
                " WHERE CSP_M.IDPRODOTTO = " & idProdottoMaster & _
                " AND CSP_M.IDCLASSESUPERAREAPRODOTTO = CPV_M.IDCLASSESUPERAREAPRODOTTO" & _
                " AND CSP_M.IDCLASSESUPERAREA = CSP_S.IDCLASSESUPERAREA" & _
                " AND CSP_S.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")"
            righeAggiornate = ORADB.ExecuteSQL(sql)
        End If
    
        If chkDateOperazioni.Value = vbChecked Then
            lblAvanzamento.Caption = "Clonazione date operazioni ..."
            DoEvents
            
            sql = "DELETE FROM PRODOTTO_CLASSEPV_TIPOOPERAZ" & _
                " WHERE IDCLASSESUPERAREAPRODOTTO IN (" & _
                " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                " FROM CLASSESUPERAREAPRODOTTO" & _
                " WHERE IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                " )"
            righeAggiornate = ORADB.ExecuteSQL(sql)
            
            sqlCatalogo = "DELETE FROM PRODOTTO_CANALEDIVENDITA" & _
                " WHERE IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")"
            righeAggiornate = ORADB.ExecuteSQL(sqlCatalogo)
            
            If optPartenzaStessaData.Value = True And optTermineStessaData.Value = True Then
                sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                    " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PCT_M.DATAORAINIZIOVALIDITA, PCT_M.DATAORAFINEVALIDITA" & _
                    " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCT_M, CLASSESUPERAREAPRODOTTO CSP_M, CLASSESUPERAREAPRODOTTO CSP_S" & _
                    " WHERE CSP_M.IDPRODOTTO = " & idProdottoMaster & _
                    " AND CSP_M.IDCLASSESUPERAREAPRODOTTO = PCT_M.IDCLASSESUPERAREAPRODOTTO" & _
                    " AND CSP_M.IDCLASSESUPERAREA = CSP_S.IDCLASSESUPERAREA" & _
                    " AND CSP_S.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")"
                righeAggiornate = ORADB.ExecuteSQL(sql)
                
                sqlCatalogo = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)" & _
                    " SELECT PS.IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE" & _
                    " FROM PRODOTTO_CANALEDIVENDITA PCV_M, PRODOTTO PS" & _
                    " WHERE PCV_M.IDPRODOTTO = " & idProdottoMaster & _
                    " AND PS.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")"
                righeAggiornate = ORADB.ExecuteSQL(sqlCatalogo)
            Else
                ' copia le stesse date per le operazioni illimitate
                sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)" & _
                    " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PCT_M.DATAORAINIZIOVALIDITA, PCT_M.DATAORAFINEVALIDITA" & _
                    " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCT_M, CLASSESUPERAREAPRODOTTO CSP_M, CLASSESUPERAREAPRODOTTO CSP_S" & _
                    " WHERE CSP_M.IDPRODOTTO = " & idProdottoMaster & _
                    " AND CSP_M.IDCLASSESUPERAREAPRODOTTO = PCT_M.IDCLASSESUPERAREAPRODOTTO" & _
                    " AND CSP_M.IDCLASSESUPERAREA = CSP_S.IDCLASSESUPERAREA" & _
                    " AND CSP_S.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                    " AND (PCT_M.DATAORAINIZIOVALIDITA = TO_DATE('01/01/1970 00.00.01', 'DD/MM/YYYY HH24.MI.SS') OR" & _
                    " PCT_M.DATAORAFINEVALIDITA = TO_DATE('31/12/2050 23.59.59', 'DD/MM/YYYY HH24.MI.SS'))"
                righeAggiornate = ORADB.ExecuteSQL(sql)
                
                ' per le altre operazioni dipende dalle opzioni indicate
                sql = "INSERT INTO PRODOTTO_CLASSEPV_TIPOOPERAZ (IDCLASSESUPERAREAPRODOTTO, IDCLASSEPUNTOVENDITA, IDTIPOOPERAZIONE, DATAORAINIZIOVALIDITA, DATAORAFINEVALIDITA)"
                
                If optPartenzaStessaData.Value = True And optTermineInizioSpettacolo.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PCT_M.DATAORAINIZIOVALIDITA, PS.INIZIO - (PM.INIZIO - PCT_M.DATAORAFINEVALIDITA)"
                ElseIf optPartenzaStessaData.Value = True And optTermineFineSpettacolo.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PCT_M.DATAORAINIZIOVALIDITA, PS.FINE - (PM.FINE - PCT_M.DATAORAFINEVALIDITA)"
                ElseIf optPartenzaInizioSpettacolo.Value = True And optTermineStessaData.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PS.INIZIO - (PM.INIZIO - PCT_M.DATAORAINIZIOVALIDITA), PCT_M.DATAORAFINEVALIDITA"
                ElseIf optPartenzaInizioSpettacolo.Value = True And optTermineInizioSpettacolo.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PS.INIZIO - (PM.INIZIO - PCT_M.DATAORAINIZIOVALIDITA), PS.INIZIO - (PM.INIZIO - PCT_M.DATAORAFINEVALIDITA)"
                ElseIf optPartenzaInizioSpettacolo.Value = True And optTermineFineSpettacolo.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PS.INIZIO - (PM.INIZIO - PCT_M.DATAORAINIZIOVALIDITA), PS.FINE - (PM.FINE - PCT_M.DATAORAFINEVALIDITA)"
                ElseIf optPartenzaFineSpettacolo.Value = True And optTermineStessaData.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PS.FINE - (PM.FINE - PCT_M.DATAORAINIZIOVALIDITA), PCT_M.DATAORAFINEVALIDITA"
                ElseIf optPartenzaFineSpettacolo.Value = True And optTermineInizioSpettacolo.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PS.FINE - (PM.FINE - PCT_M.DATAORAINIZIOVALIDITA), PS.INIZIO - (PM.INIZIO - PCT_M.DATAORAFINEVALIDITA)"
                ElseIf optPartenzaFineSpettacolo.Value = True And optTermineFineSpettacolo.Value = True Then
                    sql = sql & " SELECT CSP_S.IDCLASSESUPERAREAPRODOTTO, PCT_M.IDCLASSEPUNTOVENDITA, PCT_M.IDTIPOOPERAZIONE, PS.FINE - (PM.FINE - PCT_M.DATAORAINIZIOVALIDITA), PS.FINE - (PM.FINE - PCT_M.DATAORAFINEVALIDITA)"
                End If
                
                sql = sql & " FROM PRODOTTO_CLASSEPV_TIPOOPERAZ PCT_M, CLASSESUPERAREAPRODOTTO CSP_M, CLASSESUPERAREAPRODOTTO CSP_S," & _
                    " (SELECT IDPRODOTTO, MIN(DATAORAINIZIO) INIZIO, MAX(DATAORAINIZIO + DURATAINMINUTI / 60 / 24) FINE" & _
                    " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
                    " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
                    " AND IDPRODOTTO = " & idProdottoMaster & _
                    " GROUP BY IDPRODOTTO" & _
                    " ) PM," & _
                    " (SELECT IDPRODOTTO, MIN(DATAORAINIZIO) INIZIO, MAX(DATAORAINIZIO + DURATAINMINUTI / 60 / 24) FINE" & _
                    " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
                    " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
                    " GROUP BY IDPRODOTTO" & _
                    " ) PS" & _
                    " WHERE CSP_M.IDPRODOTTO = " & idProdottoMaster & _
                    " AND CSP_M.IDCLASSESUPERAREAPRODOTTO = PCT_M.IDCLASSESUPERAREAPRODOTTO" & _
                    " AND (PCT_M.DATAORAINIZIOVALIDITA <> TO_DATE('01/01/1970 00.00.01', 'DD/MM/YYYY HH24.MI.SS') OR" & _
                    " PCT_M.DATAORAFINEVALIDITA <> TO_DATE('31/12/2050 23.59.59', 'DD/MM/YYYY HH24.MI.SS'))" & _
                    " AND CSP_M.IDPRODOTTO = PM.IDPRODOTTO" & _
                    " AND CSP_M.IDCLASSESUPERAREA = CSP_S.IDCLASSESUPERAREA" & _
                    " AND CSP_S.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                    " AND CSP_S.IDPRODOTTO = PS.IDPRODOTTO"
                righeAggiornate = ORADB.ExecuteSQL(sql)
                
                ' Catalogo web
                sqlCatalogo = "INSERT INTO PRODOTTO_CANALEDIVENDITA (IDPRODOTTO, IDCANALEDIVENDITA, DATAORAINIZIO, DATAORAFINE)"
                
                If optPartenzaStessaData.Value = True And optTermineInizioSpettacolo.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, PCV_M.DATAORAINIZIO, RS.INIZIO - (RM.INIZIO - PCV_M.DATAORAFINE)"
                ElseIf optPartenzaStessaData.Value = True And optTermineFineSpettacolo.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, PCV_M.DATAORAINIZIO, RS.FINE - (RM.FINE - PCV_M.DATAORAFINE)"
                ElseIf optPartenzaInizioSpettacolo.Value = True And optTermineStessaData.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, RS.INIZIO - (RM.INIZIO - PCV_M.DATAORAINIZIO), PCV_M.DATAORAFINE"
                ElseIf optPartenzaInizioSpettacolo.Value = True And optTermineInizioSpettacolo.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, RS.INIZIO - (RM.INIZIO - PCV_M.DATAORAINIZIO), RS.INIZIO - (RM.INIZIO - PCV_M.DATAORAFINE)"
                ElseIf optPartenzaInizioSpettacolo.Value = True And optTermineFineSpettacolo.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, RS.INIZIO - (RM.INIZIO - PCV_M.DATAORAINIZIO), RS.FINE - (RM.FINE - PCV_M.DATAORAFINE)"
                ElseIf optPartenzaFineSpettacolo.Value = True And optTermineStessaData.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, RS.FINE - (RM.FINE - PCV_M.DATAORAINIZIO), RCV_M.DATAORAFINE"
                ElseIf optPartenzaFineSpettacolo.Value = True And optTermineInizioSpettacolo.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, RS.FINE - (RM.FINE - PCV_M.DATAORAINIZIO), RS.INIZIO - (RM.INIZIO - PCV_M.DATAORAFINE)"
                ElseIf optPartenzaFineSpettacolo.Value = True And optTermineFineSpettacolo.Value = True Then
                    sqlCatalogo = sqlCatalogo & " SELECT PS.IDPRODOTTO, PCV_M.IDCANALEDIVENDITA, RS.FINE - (RM.FINE - PCV_M.DATAORAINIZIO), RS.FINE - (RM.FINE - PCV_M.DATAORAFINE)"
                End If
                
                sqlCatalogo = sqlCatalogo & " FROM PRODOTTO_CANALEDIVENDITA PCV_M, PRODOTTO PS," & _
                    " (SELECT IDPRODOTTO, MIN(DATAORAINIZIO) INIZIO, MAX(DATAORAINIZIO + DURATAINMINUTI / 60 / 24) FINE" & _
                    " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
                    " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
                    " AND IDPRODOTTO = " & idProdottoMaster & _
                    " GROUP BY IDPRODOTTO" & _
                    " ) RM," & _
                    " (SELECT IDPRODOTTO, MIN(DATAORAINIZIO) INIZIO, MAX(DATAORAINIZIO + DURATAINMINUTI / 60 / 24) FINE" & _
                    " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
                    " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
                    " GROUP BY IDPRODOTTO" & _
                    " ) RS" & _
                    " WHERE PCV_M.IDPRODOTTO = " & idProdottoMaster & _
                    " AND (PCV_M.DATAORAINIZIO <> TO_DATE('01/01/1970 00.00.01', 'DD/MM/YYYY HH24.MI.SS') OR" & _
                    " PCV_M.DATAORAFINE <> TO_DATE('31/12/2050 23.59.59', 'DD/MM/YYYY HH24.MI.SS'))" & _
                    " AND PCV_M.IDPRODOTTO = RM.IDPRODOTTO" & _
                    " AND PS.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                    " AND PS.IDPRODOTTO = RS.IDPRODOTTO"
                righeAggiornate = ORADB.ExecuteSQL(sqlCatalogo)
            End If
        End If
        
        If chkLayoutSupporto.Value = vbChecked Then
            lblAvanzamento.Caption = "Clonazione associazione layout supporto ..."
            DoEvents
            
            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV" & _
                " WHERE IDTARIFFA IN (" & _
                " SELECT IDTARIFFA" & _
                " FROM TARIFFA" & _
                " WHERE IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                " )"
            righeAggiornate = ORADB.ExecuteSQL(sql)
    
            sql = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
                " SELECT IDAREA, TS.IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO" & _
                " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA TM, TARIFFA TS" & _
                " WHERE ULS.IDTARIFFA = TM.IDTARIFFA" & _
                " AND TM.IDPRODOTTO = " & idProdottoMaster & _
                " AND TM.CODICE = TS.CODICE" & _
                " AND TS.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")"
            righeAggiornate = ORADB.ExecuteSQL(sql)
        End If
        
        If chkAbilitazioniTariffeRetiDiVendita.Value = vbChecked Then
            lblAvanzamento.Caption = "Clonazione abilitazioni tariffe ..."
            DoEvents
            
            sql = "DELETE FROM CLASSEPV_TARIFFA" & _
                " WHERE IDTARIFFA IN (" & _
                " SELECT IDTARIFFA" & _
                " FROM TARIFFA" & _
                " WHERE IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")" & _
                " )"
            righeAggiornate = ORADB.ExecuteSQL(sql)
    
            sql = "INSERT INTO CLASSEPV_TARIFFA (IDCLASSEPUNTOVENDITA, IDTARIFFA)" & _
                " SELECT IDCLASSEPUNTOVENDITA, TS.IDTARIFFA" & _
                " FROM CLASSEPV_TARIFFA CT, TARIFFA TM, TARIFFA TS" & _
                " WHERE CT.IDTARIFFA = TM.IDTARIFFA" & _
                " AND TM.IDPRODOTTO = " & idProdottoMaster & _
                " AND TM.CODICE = TS.CODICE" & _
                " AND TS.IDPRODOTTO IN (" & stringaIdProdottiSelezionati & ")"
            righeAggiornate = ORADB.ExecuteSQL(sql)
        End If
        
        Call ORADB.CommitTrans
        Call ChiudiConnessioneBD_ORA
        
        lblAvanzamento.Caption = "Operazione terminata."
        DoEvents
    
    End If
    Exit Sub
    
gestioneErroriConfermaClonazioneParziale:
    ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    MsgBox "Errore nella transazione: " & Err.Description

End Sub

Private Sub CaricaValoriCmbOrganizzazioni()
    Dim sql As String
    
    sql = "SELECT IDORGANIZZAZIONE AS ""ID"", NOME, CODICETERMINALELOTTO" & _
        " FROM ORGANIZZAZIONE" & _
        " WHERE IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVA & _
        " ORDER BY NOME"

    Call CaricaValoriCombo2(cmbOrganizzazioni, sql, "NOME", False)
End Sub

Private Sub CaricaValoriLstProdotti()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT P.IDPRODOTTO AS ""ID"", NOME, MIN(R.DATAORAINIZIO) AS DATAORAPRIMARAPPRESENTAZIONE" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
        " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " AND R.DATAORAINIZIO > SYSDATE" & _
        " GROUP BY P.IDPRODOTTO, NOME" & _
        " ORDER BY NOME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstProdotti.AddItem rec("ID") & " - " & rec("NOME") & " - " & rec("DATAORAPRIMARAPPRESENTAZIONE")
            lstProdotti.ItemData(i) = rec("ID")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CaricaValoriLstProdottiSlave()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    lstProdottiSlave.Clear
    
    If chkRetiVendita.Value = vbChecked Or chkDateOperazioni.Value = vbChecked Or chkLayoutSupporto.Value = vbChecked Then
        lblProdottiSlave.Caption = "Prodotti non scaduti stessa organizzazione e stessa pianta"
        sql = "SELECT P.IDPRODOTTO AS ""ID"", NOME, MIN(R.DATAORAINIZIO) AS DATAORAPRIMARAPPRESENTAZIONE" & _
            " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
            " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
            " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND R.DATAORAINIZIO > SYSDATE" & _
            " AND P.IDPRODOTTO <> " & idProdottoMaster & _
            " AND IDPIANTA = (SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoMaster & ")" & _
            " AND P.IDTIPOSTATOPRODOTTO = 1" & _
            " GROUP BY P.IDPRODOTTO, NOME" & _
            " ORDER BY NOME"
        sql = "SELECT P.IDPRODOTTO AS ""ID"", NOME, MIN(R.DATAORAINIZIO) AS DATAORAPRIMARAPPRESENTAZIONE" & _
            " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
            " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
            " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND R.DATAORAINIZIO > SYSDATE" & _
            " AND P.IDPRODOTTO <> " & idProdottoMaster & _
            " AND IDPIANTA = (SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoMaster & ")" & _
            " GROUP BY P.IDPRODOTTO, NOME" & _
            " ORDER BY NOME"
    Else
        If chkAbilitazioniTariffeRetiDiVendita.Value = vbChecked Then
            lblProdottiSlave.Caption = "Prodotti non scaduti stessa organizzazione"
            sql = "SELECT P.IDPRODOTTO AS ""ID"", NOME, MIN(R.DATAORAINIZIO) AS DATAORAPRIMARAPPRESENTAZIONE" & _
                " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
                " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
                " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
                " AND R.DATAORAINIZIO > SYSDATE" & _
                " AND P.IDPRODOTTO <> " & idProdottoMaster & _
                " AND P.IDTIPOSTATOPRODOTTO = 1" & _
                " GROUP BY P.IDPRODOTTO, NOME" & _
                " ORDER BY NOME"
            sql = "SELECT P.IDPRODOTTO AS ""ID"", NOME, MIN(R.DATAORAINIZIO) AS DATAORAPRIMARAPPRESENTAZIONE" & _
                " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
                " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
                " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
                " AND R.DATAORAINIZIO > SYSDATE" & _
                " AND P.IDPRODOTTO <> " & idProdottoMaster & _
                " GROUP BY P.IDPRODOTTO, NOME" & _
                " ORDER BY NOME"
        Else
            sql = ""
        End If
    End If
    
    If sql <> "" Then
        Call ApriConnessioneBD
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 0
            While Not rec.EOF
                lstProdottiSlave.AddItem rec("ID") & " - " & rec("NOME") & " - " & rec("DATAORAPRIMARAPPRESENTAZIONE")
                lstProdottiSlave.ItemData(i) = rec("ID")
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD
    End If
        
End Sub

Private Sub cmdSelezionaTutti_Click()
    SelezionaTuttiSlave
End Sub

Private Sub lstProdotti_Click()
    SelezionaProdottoMaster
End Sub

Private Sub SelezionaProdottoMaster()
    Dim i As Integer
    
    idProdottoMaster = idNessunElementoSelezionato
    For i = 0 To lstProdotti.ListCount - 1
        If lstProdotti.Selected(i) Then
            idProdottoMaster = lstProdotti.ItemData(i)
        End If
    Next i
    
    CaricaValoriLstProdottiSlave
End Sub

Private Sub SelezionaTuttiSlave()
    Dim i As Long
    
    For i = 0 To lstProdottiSlave.ListCount - 1
        lstProdottiSlave.Selected(i) = True
    Next i

End Sub

