VERSION 5.00
Begin VB.Form frmDettagliRettangoloPosti 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli Rettangolo Posti"
   ClientHeight    =   5520
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5520
   ScaleWidth      =   3960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtLetteraFinalePosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2880
      MaxLength       =   1
      TabIndex        =   6
      Top             =   3840
      Width           =   495
   End
   Begin VB.ComboBox cmbArea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1560
      TabIndex        =   7
      Top             =   4440
      Width           =   2115
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2220
      TabIndex        =   9
      Top             =   4980
      Width           =   1035
   End
   Begin VB.TextBox txtStepFile 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      TabIndex        =   2
      Top             =   2100
      Width           =   735
   End
   Begin VB.TextBox txtNomePrimaFila 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   2
      TabIndex        =   0
      Top             =   1260
      Width           =   735
   End
   Begin VB.TextBox txtNomeUltimaFila 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   2
      TabIndex        =   1
      Top             =   1680
      Width           =   735
   End
   Begin VB.TextBox txtNomePrimoPosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   3
      TabIndex        =   3
      Top             =   2520
      Width           =   735
   End
   Begin VB.TextBox txtNomeUltimoPosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   3
      TabIndex        =   4
      Top             =   2940
      Width           =   735
   End
   Begin VB.TextBox txtStepPosti 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      TabIndex        =   5
      Top             =   3360
      Width           =   735
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   540
      TabIndex        =   8
      Top             =   4980
      Width           =   1035
   End
   Begin VB.Label lblLetteraFinalePosto 
      Alignment       =   1  'Right Justify
      Caption         =   "Lettera finale da aggiungere al posto"
      Height          =   195
      Left            =   120
      TabIndex        =   23
      Top             =   3900
      Width           =   2595
   End
   Begin VB.Label lblArea 
      Alignment       =   1  'Right Justify
      Caption         =   "Area"
      Height          =   195
      Left            =   480
      TabIndex        =   22
      Top             =   4500
      Width           =   975
   End
   Begin VB.Label lblStepFile 
      Alignment       =   1  'Right Justify
      Caption         =   "Step File"
      Height          =   195
      Left            =   660
      TabIndex        =   21
      Top             =   2160
      Width           =   1515
   End
   Begin VB.Label lblNomePrimaFila 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome prima Fila"
      Height          =   195
      Left            =   660
      TabIndex        =   20
      Top             =   1320
      Width           =   1515
   End
   Begin VB.Label lblNumeroTotalePosti1 
      Caption         =   "per un numero totale Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   720
      Width           =   2835
   End
   Begin VB.Label lblNumeroTotalePosti2 
      Caption         =   "999"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   3000
      TabIndex        =   18
      Top             =   720
      Width           =   675
   End
   Begin VB.Label lblNumeroColonne1 
      Caption         =   "Numero max Colonne inseribili"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   420
      Width           =   3255
   End
   Begin VB.Label lblNumeroColonne2 
      Caption         =   "999"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   3420
      TabIndex        =   16
      Top             =   420
      Width           =   375
   End
   Begin VB.Label lblNomeUltimaFila 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome ultima Fila"
      Height          =   195
      Left            =   660
      TabIndex        =   15
      Top             =   1740
      Width           =   1515
   End
   Begin VB.Label lblNomePrimoPosto 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome primo Posto"
      Height          =   195
      Left            =   660
      TabIndex        =   14
      Top             =   2580
      Width           =   1515
   End
   Begin VB.Label lblNomeUltimoPosto 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome ultimo Posto"
      Height          =   195
      Left            =   660
      TabIndex        =   13
      Top             =   3000
      Width           =   1515
   End
   Begin VB.Label lblStepPosti 
      Alignment       =   1  'Right Justify
      Caption         =   "Step Posti"
      Height          =   195
      Left            =   660
      TabIndex        =   12
      Top             =   3420
      Width           =   1515
   End
   Begin VB.Label lblNumeroFile1 
      Caption         =   "Numero max File inseribili"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label lblNumeroFile2 
      Caption         =   "999"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   3060
      TabIndex        =   10
      Top             =   120
      Width           =   375
   End
End
Attribute VB_Name = "frmDettagliRettangoloPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomePrimaFila As String
Private nomeUltimaFila As String
Private nomePrimoPosto As String
Private nomeUltimoPosto As String
Private stepPosti As Integer
Private stepFile As Integer
Private numeroMaxPostiInFila As Integer
Private numeroMaxFileInRettangolo As Integer
Private numeroPostiInFila As Integer
Private numeroFileInRettangolo As Integer
Private idAreaSelezionata As Long
Private nomeAreaSelezionata As String
Private nomiPostoLunghiPermessi As Boolean

Private letteraFinalePosto As String

Private modalita As ModalitaFormDettagliEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init(parNomiPostoLunghiPermessi As Boolean)
    Call AggiornaAbilitazioneControlli
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        Call cmbArea.Clear
        Call CaricaValoriCombo(cmbArea)
    End If
    nomiPostoLunghiPermessi = parNomiPostoLunghiPermessi
    If parNomiPostoLunghiPermessi Then
        txtNomePrimoPosto.MaxLength = 4
        txtNomeUltimoPosto.MaxLength = 4
    End If
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    Select Case modalita
        Case MFC_CREA_POSTI
            frmDettagliRettangoloPosti.Caption = "Creazione Rettangolo Posti"
            lblNomePrimaFila.Enabled = True
            txtNomePrimaFila.Enabled = True
            txtNomeUltimaFila.Enabled = True
            lblNomeUltimaFila.Enabled = True
            txtNomePrimoPosto.Enabled = True
            lblNomePrimoPosto.Enabled = True
            txtNomeUltimoPosto.Enabled = True
            lblNomeUltimoPosto.Enabled = True
            txtStepFile.Enabled = True
            lblStepFile.Enabled = True
            txtStepPosti.Enabled = True
            lblStepPosti.Enabled = True
            txtLetteraFinalePosto.Enabled = True
            lblLetteraFinalePosto.Enabled = True
            cmdConferma.Enabled = (Len(Trim(txtNomePrimaFila.Text)) > 0 And _
                Len(Trim(txtNomeUltimaFila.Text)) > 0 And _
                Len(Trim(txtNomePrimoPosto.Text)) > 0 And _
                Len(Trim(txtNomeUltimoPosto.Text)) > 0 And _
                Len(Trim(txtStepFile.Text)) > 0 And _
                Len(Trim(txtStepPosti.Text)) > 0 And _
                cmbArea.Text <> "")
            lblNumeroFile2.Caption = CStr(numeroMaxFileInRettangolo)
            lblNumeroColonne2.Caption = CStr(numeroMaxPostiInFila)
            lblNumeroTotalePosti2.Caption = CStr(numeroMaxFileInRettangolo * numeroMaxPostiInFila)
            If tipoGriglia = TG_GRANDI_IMPIANTI Then
                cmbArea.Text = nomeAreaSelezionata
                cmbArea.Enabled = False
            End If
        Case MFC_MODIFICA_ATTRIBUTI_POSTO
            frmDettagliRettangoloPosti.Caption = "Modifica Rettangolo Posti"
            txtNomePrimaFila.Enabled = False
            lblNomePrimaFila.Enabled = False
            txtNomeUltimaFila.Enabled = False
            lblNomeUltimaFila.Enabled = False
            txtNomePrimoPosto.Enabled = False
            lblNomePrimoPosto.Enabled = False
            txtNomeUltimoPosto.Enabled = False
            lblNomeUltimoPosto.Enabled = False
            txtStepFile.Enabled = False
            lblStepFile.Enabled = False
            txtStepPosti.Enabled = False
            lblStepPosti.Enabled = False
            txtLetteraFinalePosto.Enabled = False
            lblLetteraFinalePosto.Enabled = False
        Case Else
    End Select
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaValoriDaConfermare As Collection

On Error Resume Next

    ValoriCampiOK = True
    Set listaValoriDaConfermare = New Collection
    
    nomePrimaFila = Trim(txtNomePrimaFila.Text)
    nomeUltimaFila = Trim(txtNomeUltimaFila.Text)
    nomePrimoPosto = Trim(txtNomePrimoPosto.Text)
    nomeUltimoPosto = Trim(txtNomeUltimoPosto.Text)
    stepFile = CLng(Trim(txtStepFile.Text))
    stepPosti = CLng(Trim(txtStepPosti.Text))
    letteraFinalePosto = Trim(txtLetteraFinalePosto.Text)
    nomeAreaSelezionata = CStr(Trim(cmbArea.Text))
    Call CalcolaNumeroPostiInFila
    If numeroPostiInFila > numeroMaxPostiInFila Then
'        Call frmMessaggio.Visualizza("AvvertimentoSuperamentoLimitePosti")
'        If frmMessaggio.exitCode = EC_ANNULLA Then
'            RilevaValoriCampi = False
            Call listaValoriDaConfermare.Add("- Il numero posti � maggiore del massimo; i posti verranno disegnati sovrapposti;")
'        End If
    End If
    Call CalcolaNumeroFileInRettangolo
    If numeroFileInRettangolo > numeroMaxFileInRettangolo Then
'        Call frmMessaggio.Visualizza("AvvertimentoSuperamentoLimiteFile")
'        If frmMessaggio.exitCode = EC_ANNULLA Then
'            RilevaValoriCampi = False
            Call listaValoriDaConfermare.Add("- Il numero file � maggiore del massimo; i posti verranno disegnati sovrapposti;")
'        End If
    End If
    
    If Len(Trim(txtLetteraFinalePosto.Text)) < 0 Or Len(Trim(txtLetteraFinalePosto.Text)) > 1 Then
        Call listaValoriDaConfermare.Add("- La lettera finale deve essere una")
    Else
        If Len(Trim(txtLetteraFinalePosto.Text)) = 1 Then
            If Len(Trim(txtNomePrimoPosto.Text)) > 2 Or Len(Trim(txtNomeUltimoPosto.Text)) > 2 Then
                Call listaValoriDaConfermare.Add("- Se � specificata la lettera finale i posti possono essere lunghi al massimo due caratteri")
                ValoriCampiOK = False
            End If
        End If
    End If
'    If listaNonConformit�.count > 0 Then
'        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
'    Else
        If listaValoriDaConfermare.count > 0 Then
            Call frmMessaggio.Visualizza("ConfermaValori", ArgomentoMessaggio(listaValoriDaConfermare))
            If frmMessaggio.exitCode = EC_ANNULLA Then
                ValoriCampiOK = False
            End If
        End If
'    End If
End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call frmConfigurazionePiantaGrigliaPosti.SetExitCodeDettagliPosti(EC_DP_ANNULLA)
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub txtNomePrimaFila_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomeUltimaFila_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomePrimoPosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomeUltimoPosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtStepFile_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtStepPosti_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtIndiceDiPreferibilita_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetNumeroMaxPostiInFila(numP As Integer)
    numeroMaxPostiInFila = numP
End Sub

Public Sub SetNumeroMaxFileInRettangolo(numF As Integer)
    numeroMaxFileInRettangolo = numF
End Sub

Private Sub CalcolaNumeroPostiInFila()
    Dim a As String
    Dim b As String
    Dim primo As Integer
    Dim ultimo As Integer

    a = Mid$(Trim(UCase(nomePrimoPosto)), 1, 1)
    b = Mid$(Trim(UCase(nomeUltimoPosto)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        primo = Intero(nomePrimoPosto)
        ultimo = Intero(nomeUltimoPosto)
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        primo = Intero(Asc(a))
        ultimo = Intero(Asc(b))
    End If
    
    numeroPostiInFila = Abs(Int((primo - ultimo) / stepPosti)) + 1 '+1 perch� estremi compresi
End Sub

Private Sub CalcolaNumeroFileInRettangolo()
    Dim a As String
    Dim b As String
    Dim prima As Integer
    Dim ultima As Integer

    a = Mid$(Trim(UCase(nomePrimaFila)), 1, 1)
    b = Mid$(Trim(UCase(nomeUltimaFila)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        prima = Intero(nomePrimaFila)
        ultima = Intero(nomeUltimaFila)
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        prima = Intero(Asc(a))
        ultima = Intero(Asc(b))
    End If
    
    numeroFileInRettangolo = Abs(Int((prima - ultima) / stepFile)) + 1 '+1 perch� estremi compresi
End Sub

Private Sub Conferma()
    If ValoriCampiOK Then
        If IsUltimoNomeFilaCorretto(nomePrimaFila, numeroFileInRettangolo, stepFile) And _
            IsUltimoNomePostoCorretto(nomePrimoPosto, numeroPostiInFila, stepPosti, nomiPostoLunghiPermessi) Then
            Call frmConfigurazionePiantaGrigliaPosti.SetExitCodeDettagliPosti(EC_DP_CONFERMA)
            Call frmConfigurazionePiantaGrigliaPosti.SetNumeroPostiInFila(numeroPostiInFila)
            Call frmConfigurazionePiantaGrigliaPosti.SetNumeroFileInRettangolo(numeroFileInRettangolo)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomePrimoPosto(nomePrimoPosto)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomeUltimoPosto(nomeUltimoPosto)
            Call frmConfigurazionePiantaGrigliaPosti.SetLetteraFinalePosto(letteraFinalePosto)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomePrimaFila(nomePrimaFila)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomeUltimaFila(nomeUltimaFila)
            Call frmConfigurazionePiantaGrigliaPosti.SetStepPosti(stepPosti)
            Call frmConfigurazionePiantaGrigliaPosti.SetStepFile(stepFile)
            Call frmConfigurazionePiantaGrigliaPosti.SetIdAreaSelezionata(idAreaSelezionata)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
            Call frmConfigurazionePiantaGrigliaPosti.SetXShift(0)
            Call frmConfigurazionePiantaGrigliaPosti.SetYShift(0)
            Call frmConfigurazionePiantaGrigliaPosti.SetXIntervalloPosti(1)
            Call frmConfigurazionePiantaGrigliaPosti.SetYIntervalloPosti(1)
            Unload Me
        Else
            Call frmMessaggio.Visualizza("NotificaNomiNonCorretti")
        End If
    End If
End Sub

Public Sub SetModalitaForm(moda As ModalitaFormDettagliEnum)
    modalita = moda
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox)
    Dim i As Integer
    Dim area As classeArea
    
'    For i = 1 To UBound(elencoAreeAssociateAPianta)
'        cmb.AddItem (elencoAreeAssociateAPianta(i).nomeArea)
'        cmb.ItemData(i - 1) = elencoAreeAssociateAPianta(i).idArea
'    Next i
    i = 1
    For Each area In listaAreeAssociateAPianta
        cmb.AddItem area.nomeArea
        cmb.ItemData(i - 1) = area.idArea
        i = i + 1
    Next area
End Sub

Private Sub cmbArea_Click()
    Call cmbArea_Update
End Sub

Private Sub cmbArea_Update()
    idAreaSelezionata = cmbArea.ItemData(cmbArea.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub
