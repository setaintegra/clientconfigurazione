Attribute VB_Name = "CostantiGlobali"
Option Explicit

'COSTANTI
'data iniziale di riferimento, uguale in tutto il progetto SETA
Global Const dataNulla As Date = #1/1/1900#

'date limite per i periodi
Global Const dataInferioreMinima As Date = #1/1/1970 12:00:01 AM#
Global Const dataSuperioreMassima As Date = #12/31/2050 11:59:59 PM#

'numero di tipi operazioni: deve essere uguale al numero di righe della tabella TIPOOPERAZIONE
Global Const NUMERO_TIPI_OPERAZIONI As Long = 38

'valore da usare se serve l'id di un elemento selezionato ma non c'� alcun
'elemento selezionato; deve essere sempre pari al min id possibile meno 1
Global Const idNessunElementoSelezionato As Long = -1

'valore ItemData per la selezione di TUTTI gli elementi di una combo;
'pari al precedente meno 1
Global Const idTuttiGliElementiSelezionati As Long = -2

'valore ItemData per passare dalla selezione di un elemento all'aggiunta di un elemento nuovo;
'pari al precedente meno 1
Global Const idAggiungiNuovo As Long = -3

'valore Long da far corrispondere a Null in Base Dati
Global Const valoreLongNullo As Long = -2147483648#

'valore Integer da far corrispondere a Null in Base Dati
Global Const valoreIntegerNullo As Integer = -32768

'valore Long da far corrispondere a Null in Base Dati
Global Const numeroMassimoElementiInCombo As Long = 32767

''numero comunicato da Andrea; modificabile
'Global Const numeroMaxTariffeUtilizzabiliDaTL As Integer = 10

'valore di IDTIPOPRODOTTO, per ora fisso
Global Const idTipoProdottoCalcio As Long = 0

'valore di IDTIPOPRODOTTO, per ora fisso
Global Const INTERVALLO_RIPRISTINO_CONNESSIONE As Long = 65535

'File esterni utilizzati dal Client di Configurazione
Public Const FILE_MESSAGGI As Integer = 1

'Tempo massimo concesso per l'esecuzione delle query (in secondi)
Public Const TIME_OUT As Long = 600

'codfice fiscale da inserire per default nelle maskedEditBox
Public Const codiceFiscaleNullo As String = "________________"

Public Const TIPO_TARIFFA_INTERO As Integer = 1
Public Const TIPO_TARIFFA_OMAGGIO As Integer = 1
Public Const TIPO_TARIFFA_SERVIZIO As Integer = 3
Public Const TIPO_TARIFFA_RIDOTTO As Integer = 4

Public Const IDTIPOTARIFFA_INGRESSO_ABBONATO As Long = 5
Public Const IDTIPOTARIFFASIAE_INGRESSO_ABBONATO As Long = 25
Public Const IDTIPOTARIFFASIAE_OMAGGIO As Integer = 16
Public Const IDTIPOTARIFFASIAE_SERVIZIO As Integer = 17
Public Const IDTIPOTARIFFASIAE_INVITO As Integer = 96
Public Const IDTIPOTARIFFASIAE_INTERO As Integer = 1
Public Const NUMERO_MAX_TITOLI_PER_ACQUIRENTE As Integer = 4 '10
Public Const NUMERO_MIN_TITOLI_PER_ACQUIRENTE As Integer = 1

Public Const IDGENERESIAE_CALCIO_AB_INTERN As Integer = 2
Public Const IDGENERESIAE_CALCIO_C_INF As Integer = 3
Public Const IDGENERESIAE_INVITO As Integer = 55

Public Const IDTIPOSUPPORTOSIAE_BIGLIETTO_TRADIZIONALE As Integer = 1
Public Const IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE As Integer = 6
Public Const IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO As Integer = 8
Public Const IDTIPOSUPPORTOSIAE_PASSBOOK As Integer = 10

Public Const IDPARAMETROANAGRAFICA_COGNOME As Integer = 1
Public Const IDPARAMETROANAGRAFICA_NOME As Integer = 2
Public Const IDPARAMETROANAGRAFICA_DENOMINAZIONE As Integer = 3

'Public Const IDCLASSEPRODOTTO_BIGLIETTERIA_ORDINARIA As Long = 1
'Public Const IDCLASSEPRODOTTO_ABBONAMENTO_A_TURNO_FISSO_E_POSTO_FISSO As Long = 2
'Public Const IDCLASSEPRODOTTO_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO As Long = 3

'stringa da inserire davanti al nome della tabelle sullo schema SETA_CC
Public Const nomeSchemaConfigurazione As String = "SETA_CC."

'Valore speciale per i campi Testo con valore Null durante il trasferimento dei dati
Public Const VALORE_NULLO_TESTO As String = ""

''Pi greco
'Public Const PI_GRECO As Double = 3.14159265358979

'Valori colori impianti
Public Const COLORE_GIALLO_1 As String = "GIALLO1"
Public Const COLORE_GIALLO_2 As String = "GIALLO2"
Public Const COLORE_GIALLO_3 As String = "GIALLO3"
Public Const COLORE_GIALLO_4 As String = "GIALLO4"
Public Const COLORE_ARANCIO_1 As String = "ARANCIO1"
Public Const COLORE_ROSA_1 As String = "ROSA1"
Public Const COLORE_ROSA_2 As String = "ROSA2"
Public Const COLORE_ROSA_3 As String = "ROSA3"
Public Const COLORE_LEGNO_1 As String = "LEGNO1"
Public Const COLORE_LEGNO_2 As String = "LEGNO2"
Public Const COLORE_LEGNO_3 As String = "LEGNO3"
Public Const COLORE_ACQUA_1 As String = "ACQUA1"
Public Const COLORE_ACQUA_2 As String = "ACQUA2"
Public Const COLORE_ACQUA_3 As String = "ACQUA3"
Public Const COLORE_BLU_1 As String = "BLU1"
Public Const COLORE_BLU_2 As String = "BLU2"
Public Const COLORE_BLU_3 As String = "BLU3"
Public Const COLORE_VIOLA_1 As String = "VIOLA1"
Public Const COLORE_VIOLA_2 As String = "VIOLA2"
Public Const COLORE_VIOLA_3 As String = "VIOLA3"
Public Const COLORE_FUXIA_1 As String = "FUXIA1"
Public Const COLORE_VERDE_1 As String = "VERDE1"
Public Const COLORE_VERDE_2 As String = "VERDE2"
Public Const COLORE_VERDE_3 As String = "VERDE3"

Public Const NUMERO_COLORI As Long = 24
Public Const GIALLO_1 As Long = 1
Public Const GIALLO_2 As Long = 2
Public Const GIALLO_3 As Long = 3
Public Const GIALLO_4 As Long = 4
Public Const ARANCIO_1 As Long = 5
Public Const ROSA_1 As Long = 6
Public Const ROSA_2 As Long = 7
Public Const ROSA_3 As Long = 8
Public Const LEGNO_1 As Long = 9
Public Const LEGNO_2 As Long = 10
Public Const LEGNO_3 As Long = 11
Public Const ACQUA_1 As Long = 12
Public Const ACQUA_2 As Long = 13
Public Const ACQUA_3 As Long = 14
Public Const BLU_1 As Long = 15
Public Const BLU_2 As Long = 16
Public Const BLU_3 As Long = 17
Public Const VIOLA_1 As Long = 18
Public Const VIOLA_2 As Long = 19
Public Const VIOLA_3 As Long = 20
Public Const FUXIA_1 As Long = 21
Public Const VERDE_1 As Long = 22
Public Const VERDE_2 As Long = 23
Public Const VERDE_3 As Long = 24

'---------------------------------------------------------------------------------------------

'VARIABILI
'Connessione alla base dati dell'applicazione, attiva durante tutta la sessione di lavoro
Public SETAConnection As New ADODB.Connection

'Sessione di lavoro attiva mediante librerie Oracle, attiva durante tutta la sessione di lavoro
Public ORASession As ORASession

'Oggetto DataBase istanziato dalle librerie Oracle
Public ORADB As OraDatabase

'Numero di connessioni virtuali alla base dati
Public numeroConnessioniVirtualmenteAperte As Long

'Numero di connessioni Oracle virtuali alla base dati
Public numeroConnessioniOracleVirtualmenteAperte As Long

