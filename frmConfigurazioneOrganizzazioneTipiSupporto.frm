VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneOrganizzazioneTipiSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   8940
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   6180
      Width           =   435
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   23
      Top             =   240
      Width           =   3255
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   8220
      MultiSelect     =   2  'Extended
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6120
      Width           =   3555
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   6600
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   7020
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   7440
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   4140
      MultiSelect     =   2  'Extended
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   6120
      Width           =   3555
   End
   Begin VB.ComboBox cmbNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   6120
      Width           =   3915
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   16
      Top             =   4860
      Width           =   4035
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   13
      Top             =   7860
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   12
      Top             =   8340
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneOrganizzazioneTipiSupporto 
      Height          =   330
      Left            =   360
      Top             =   3780
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneOrganizzazioneTipiSupporto 
      Height          =   3795
      Left            =   120
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6694
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   24
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblLayoutSupporto 
      Alignment       =   2  'Center
      Caption         =   "LAYOUT SUPPORTO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4140
      TabIndex        =   22
      Top             =   5640
      Width           =   7635
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   4140
      TabIndex        =   21
      Top             =   5880
      Width           =   3555
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati"
      Height          =   195
      Left            =   8220
      TabIndex        =   20
      Top             =   5880
      Width           =   3555
   End
   Begin VB.Label lblNome 
      Caption         =   "Codice - Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   5880
      Width           =   1695
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   18
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione dei Tipi / Layout Supporto all'Organizzazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   7155
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneTipiSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idTipoProtCausaleSelezionata As Long
Private SQLCaricamentoCombo As String
Private nomeRecordSelezionato As String
Private codiceRecordSelezionato As String
Private descrizioneRecordSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private listaAppoggioTipiSupportoLayout As Collection
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazioneOrganizzazioneTipiSupporto.Caption = "TIPI DI SUPPORTO CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTipiSupporto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbNome.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        cmbNome.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblNome.Enabled = False
        lblLayoutSupporto.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTipiSupporto.Enabled = False
        cmbNome.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        lblLayoutSupporto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdModifica.Enabled = False
        If listaSelezionati Is Nothing Then
            cmdConferma.Enabled = False
        Else
            cmdConferma.Enabled = (cmbNome.ListIndex <> idNessunElementoSelezionato And _
                listaSelezionati.count > 0)
        End If
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneTipiSupporto.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbNome.Clear
        Call lstDisponibili.Clear
        Call lstSelezionati.Clear
        cmbNome.Enabled = False
        lstDisponibili.Enabled = False
        lstSelezionati.Enabled = False
        lblNome.Enabled = False
        lblLayoutSupporto.Enabled = False
        lblDisponibili.Enabled = False
        lblSelezionati.Enabled = False
        cmdSelezionato.Enabled = False
        cmdDidsponibile.Enabled = False
        cmdSvuotaSelezionati.Enabled = False
        cmdSvuotaDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
    gestioneFormCorrente = gs
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim causaNonEditabilita As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        "; IDTIPOSUPPORTO = " & idRecordSelezionato
        
    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoOrganizzazione = TSO_ATTIVA Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitąOrganizzazioneAttiva")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            Select Case gestioneRecordGriglia
                Case ASG_INSERISCI_NUOVO
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI, stringaNota, idOrganizzazioneSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazioneOrganizzazioneTipiSupporto_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazioneOrganizzazioneTipiSupporto_Init
                Case ASG_MODIFICA
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_ORGANIZZAZIONE, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI, stringaNota, idOrganizzazioneSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazioneOrganizzazioneTipiSupporto_Init
                    Call SelezionaElementoSuGriglia(idRecordSelezionato)
                    Call dgrConfigurazioneOrganizzazioneTipiSupporto_Init
                Case ASG_ELIMINA
                    Call EliminaDallaBaseDati
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI, stringaNota, idOrganizzazioneSelezionata)
                    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                    Call adcConfigurazioneOrganizzazioneTipiSupporto_Init
                    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                    Call dgrConfigurazioneOrganizzazioneTipiSupporto_Init
                Case Else
                    'Do Nothing
            End Select
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitąCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDTIPOSUPPORTO AS ID, CODICE || ' - ' || NOME LABEL FROM TIPOSUPPORTO" & _
        " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaValoriCombo(cmbNome, sql, "LABEL")
    Call SelezionaElementoSuCombo(cmbNome, idRecordSelezionato)
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggioTipiSupportoLayout
    Unload Me
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
'    If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT TS.IDTIPOSUPPORTO ID, TS.CODICE || ' - ' || TS.NOME LABEL, TS.DESCRIZIONE" & _
'            " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
'            " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO(+)" & _
'            " AND (OTL.IDTIPOSUPPORTO IS NULL OR OTL.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")" & _
'            " AND OTL.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata & _
'            " AND (OTL.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR OTL.IDTIPOSTATORECORD IS NULL)" & _
'            " ORDER BY LABEL"
'    Else
        sql = "SELECT TS.IDTIPOSUPPORTO ID, TS.CODICE || ' - ' || TS.NOME LABEL, TS.DESCRIZIONE" & _
            " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO(+)" & _
            " AND OTL.IDTIPOSUPPORTO IS NULL" & _
            " AND OTL.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata & _
            " ORDER BY LABEL"
'    End If
    Call CaricaValoriCombo(cmbNome, sql, "LABEL")
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmbNome_Click()
    Call CaricaValoriListe
End Sub

Private Sub CaricaValoriListe()
    idRecordSelezionato = cmbNome.ItemData(cmbNome.ListIndex)
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneOrganizzazioneTipiSupporto_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call CreaTabellaAppoggioTipiSupportoLayout
    Call adcConfigurazioneOrganizzazioneTipiSupporto_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneOrganizzazioneTipiSupporto_Init
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneOrganizzazioneTipiSupporto.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec(0).Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneOrganizzazioneTipiSupporto_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call PopolaTabellaAppoggioTipiSupportoLayout
    
    Set d = adcConfigurazioneOrganizzazioneTipiSupporto
    
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO AS ""ID""," & _
        " TS.CODICE AS ""Codice"", TS.NOME AS ""Nome"", TS.DESCRIZIONE AS ""Descrizione""," & _
        " TMP.NOME AS ""Layout associati""" & _
        " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL, " & nomeTabellaTemporanea & " TMP WHERE" & _
        " (TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO) AND" & _
        " (TS.IDTIPOSUPPORTO = TMP.IDTIPO(+)) AND" & _
        " (OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")" & _
        " ORDER BY ""Codice"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneOrganizzazioneTipiSupporto.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim layout As clsElementoLista
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
'   INSERIMENTO IN TABELLA ORGANIZ_TIPOSUP_LAYOUTSUP
    If Not (listaSelezionati Is Nothing) Then
        sql = "DELETE FROM ORGANIZ_TIPOSUP_LAYOUTSUP WHERE" & _
            " IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND IDTIPOSUPPORTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each layout In listaSelezionati
            sql = "INSERT INTO ORGANIZ_TIPOSUP_LAYOUTSUP (IDORGANIZZAZIONE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
                " VALUES (" & idOrganizzazioneSelezionata & ", " & _
                idRecordSelezionato & ", " & _
                layout.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next layout
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = TSR_NUOVO
'            condizioneSql = " AND IDTIPOSUPPORTO = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("ORGANIZ_TIPOSUP_LAYOUTSUP", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_NUOVO, condizioneSql)
'        End If
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim condizioneSql As String
    Dim SqlConstraints As String
    Dim n As Long
    Dim risposta As VbMsgBoxResult

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
'VERIFICARE CHE IL PRODOTTO SU UTILIZZAZIONELAY... SIA DELL'ORGANIZZAZIONE CORRENTE
    condizioneSql = " AND IDTARIFFA IN (SELECT IDTARIFFA FROM TARIFFA T, PRODOTTO P" & _
        " WHERE T.IDPRODOTTO = P.IDPRODOTTO AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")"
    If IsRecordEliminabile("IDTIPOSUPPORTO", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idRecordSelezionato), condizioneSql) Then
        sql = "DELETE FROM ORGANIZ_TIPOSUP_LAYOUTSUP" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND IDTIPOSUPPORTO" & " = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
    Else
'        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il Layout Supporto selezionato", "UTILIZZOLAYOUTSUPPORTO")
        risposta = MsgBox("Esistono record collegati nella tabella UTILIZZOLAYOUTSUPPORTOCPV relativi a prodotti dell'organizzazione: eliminare anche questa informazione?", vbYesNo)
        If risposta = vbYes Then
            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV" & _
                " WHERE IDTIPOSUPPORTO" & " = " & idRecordSelezionato & _
                " AND IDTARIFFA IN (" & _
                " SELECT IDTARIFFA" & _
                " FROM TARIFFA T, PRODOTTO P" & _
                " WHERE T.IDPRODOTTO = P.IDPRODOTTO AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                ")"
            SETAConnection.Execute sql, n, adCmdText
            
            sql = "DELETE FROM ORGANIZ_TIPOSUP_LAYOUTSUP" & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND IDTIPOSUPPORTO" & " = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
        End If
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazioneOrganizzazioneTipiSupporto_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneOrganizzazioneTipiSupporto
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneOrganizzazioneTipiSupporto.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub CreaTabellaAppoggioTipiSupportoLayout()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_ORGTIPILAYOUT_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDTIPO NUMBER(10), NOME VARCHAR2(4000))"
    
    Call EliminaTabellaAppoggioTipiSupportoLayout
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioTipiSupportoLayout()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioTipiSupportoLayout()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idTipoSupp As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioTipiSupportoLayout = New Collection
    
    sql = "SELECT DISTINCT OTL.IDTIPOSUPPORTO ID" & _
        " FROM ORGANIZ_TIPOSUP_LAYOUTSUP OTL WHERE" & _
        " OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("ID").Value
            Call listaAppoggioTipiSupportoLayout.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioTipiSupportoLayout
        campoNome = ""
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT OTL.IDTIPOSUPPORTO," & _
'                " OTL.IDLAYOUTSUPPORTO," & _
'                " LS.NOME FROM" & _
'                " LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL WHERE" & _
'                " LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO AND" & _
'                " OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & " AND" & _
'                " OTL.IDTIPOSUPPORTO = " & recordTemporaneo.idElementoLista & _
'                " AND (OTL.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OTL.IDTIPOSTATORECORD IS NULL)" & _
'                " ORDER BY LS.NOME"
'        Else
            sql = "SELECT OTL.IDTIPOSUPPORTO," & _
                " OTL.IDLAYOUTSUPPORTO," & _
                " LS.NOME FROM" & _
                " LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL WHERE" & _
                " LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO AND" & _
                " OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & " AND" & _
                " OTL.IDTIPOSUPPORTO= " & recordTemporaneo.idElementoLista & _
                " ORDER BY LS.NOME"
'        End If
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("NOME"), campoNome & "; " & rec("NOME"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qui sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, , adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sł
    For Each recordTemporaneo In listaAppoggioTipiSupportoLayout
        idTipoSupp = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & _
            " VALUES (" & idTipoSupp & ", " & _
            SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdDidsponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim listaLayoutNonEliminabili As Collection
    Dim condizioneSql As String
    Dim idLayout As Long
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    
    condizioneSql = " AND IDTARIFFA IN (SELECT IDTARIFFA FROM TARIFFA T, PRODOTTO P" & _
        " WHERE T.IDPRODOTTO = P.IDPRODOTTO AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")"
    Set listaLayoutNonEliminabili = New Collection
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idLayout = lstSelezionati.ItemData(i - 1)
            chiaveLayout = ChiaveId(idLayout)
            Set layout = listaSelezionati.Item(chiaveLayout)
            If IsRecordEliminabile("IDLAYOUTSUPPORTO", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idLayout), condizioneSql) Then
                Call listaDisponibili.Add(layout, chiaveLayout)
                Call listaSelezionati.Remove(chiaveLayout)
            Else
                Call listaLayoutNonEliminabili.Add(layout.descrizioneElementoLista)
            End If
        End If
    Next i
    If listaLayoutNonEliminabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", ArgomentoMessaggio(listaLayoutNonEliminabili), "UTILIZZOLAYOUTSUPPORTOCPV")
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveLayout As String
    Dim layoutCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Then
        sql = "SELECT LS.IDLAYOUTSUPPORTO, NOME, DESCRIZIONE, CODICE" & _
            " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS WHERE" & _
            " (LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO) AND" & _
            " (LSTS.IDTIPOSUPPORTO = " & idRecordSelezionato & ")" & _
            " ORDER BY NOME"
    Else
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT LS.IDLAYOUTSUPPORTO, NOME, DESCRIZIONE, CODICE" & _
'                " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
'                " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
'                " AND LSTS.IDTIPOSUPPORTO = " & idRecordSelezionato & _
'                " AND NOT EXISTS" & _
'                " (SELECT OTL.IDLAYOUTSUPPORTO" & _
'                " FROM ORGANIZ_TIPOSUP_LAYOUTSUP OTL, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
'                " WHERE OTL.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
'                " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND LSTS.IDTIPOSUPPORTO = " & idRecordSelezionato & _
'                " AND (OTL.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OTL.IDTIPOSTATORECORD IS NULL)" & _
'                " AND LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO)" & _
'                " ORDER BY NOME"
'        Else
            sql = "SELECT LS.IDLAYOUTSUPPORTO, NOME, DESCRIZIONE, CODICE" & _
                " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
                " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO" & _
                " AND LSTS.IDTIPOSUPPORTO = " & idRecordSelezionato & _
                " AND NOT EXISTS" & _
                " (SELECT OTL.IDLAYOUTSUPPORTO" & _
                " FROM ORGANIZ_TIPOSUP_LAYOUTSUP OTL, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS" & _
                " WHERE OTL.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO" & _
                " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND LSTS.IDTIPOSUPPORTO = " & idRecordSelezionato & _
                " AND LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO)" & _
                " ORDER BY NOME"
'        End If
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set layoutCorrente = New clsElementoLista
            layoutCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO").Value
            layoutCorrente.nomeElementoLista = rec("NOME")
            layoutCorrente.descrizioneElementoLista = rec("NOME")
            chiaveLayout = ChiaveId(layoutCorrente.idElementoLista)
            Call listaDisponibili.Add(layoutCorrente, chiaveLayout)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveLayout As String
    Dim layoutCorrente As clsElementoLista
    
    Call ApriConnessioneBD

    Set listaSelezionati = New Collection
    If gestioneRecordGriglia <> ASG_INSERISCI_NUOVO Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT" & _
'                " LS.IDLAYOUTSUPPORTO AS ""IDLAYOUTSUPPORTO"", LS.NOME, LS.DESCRIZIONE" & _
'                " FROM LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
'                " WHERE LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO" & _
'                " AND OTL.IDTIPOSUPPORTO = " & idRecordSelezionato & _
'                " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND (OTL.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OTL.IDTIPOSTATORECORD IS NULL)" & _
'                " ORDER BY NOME"
'        Else
            sql = "SELECT" & _
                " LS.IDLAYOUTSUPPORTO AS ""IDLAYOUTSUPPORTO"", LS.NOME, LS.DESCRIZIONE" & _
                " FROM LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
                " WHERE LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO" & _
                " AND OTL.IDTIPOSUPPORTO = " & idRecordSelezionato & _
                " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " ORDER BY NOME"
'        End If
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set layoutCorrente = New clsElementoLista
                layoutCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO").Value
                layoutCorrente.nomeElementoLista = rec("NOME")
                layoutCorrente.descrizioneElementoLista = rec("NOME")
                chiaveLayout = ChiaveId(layoutCorrente.idElementoLista)
                Call listaSelezionati.Add(layoutCorrente, chiaveLayout)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ChiudiConnessioneBD
        
        Call lstSelezionati_Init
    Else
        'do Nothing
    End If
    
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim layout As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each layout In listaDisponibili
            lstDisponibili.AddItem layout.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = layout.idElementoLista
            i = i + 1
        Next layout
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim layout As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each layout In listaSelezionati
            lstSelezionati.AddItem layout.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = layout.idElementoLista
            i = i + 1
        Next layout
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idLayout As Long
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idLayout = lstDisponibili.ItemData(i - 1)
            chiaveLayout = ChiaveId(idLayout)
            Set layout = listaDisponibili.Item(chiaveLayout)
            Call listaSelezionati.Add(layout, chiaveLayout)
            Call listaDisponibili.Remove(chiaveLayout)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub SvuotaSelezionati()
    Dim i As Integer
    Dim listaLayoutNonEliminabili As Collection
    Dim condizioneSql As String
    Dim idLayout As Long
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    
    condizioneSql = " AND IDTARIFFA IN (SELECT IDTARIFFA FROM TARIFFA T, PRODOTTO P" & _
        " WHERE T.IDPRODOTTO = P.IDPRODOTTO AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")"
    Set listaLayoutNonEliminabili = New Collection
    For i = 1 To lstSelezionati.ListCount
        idLayout = lstSelezionati.ItemData(i - 1)
        chiaveLayout = ChiaveId(idLayout)
        Set layout = listaSelezionati.Item(chiaveLayout)
        If IsRecordEliminabile("IDLAYOUTSUPPORTO", "UTILIZZOLAYOUTSUPPORTOCPV", CStr(idLayout), condizioneSql) Then
            Call listaDisponibili.Add(layout, chiaveLayout)
            Call listaSelezionati.Remove(chiaveLayout)
        Else
            Call listaLayoutNonEliminabili.Add(layout.descrizioneElementoLista)
        End If
    Next i
    If listaLayoutNonEliminabili.count > 0 Then
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", ArgomentoMessaggio(listaLayoutNonEliminabili), "UTILIZZOLAYOUTSUPPORTOCPV")
    End If
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDTIPOSUPPORTO AS ID, CODICE || ' - ' || NOME LABEL FROM TIPOSUPPORTO" & _
        " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaValoriCombo(cmbNome, sql, "LABEL")
    Call SelezionaElementoSuCombo(cmbNome, idRecordSelezionato)
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati_OLD()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim layout As clsElementoLista
    Dim condizioneSql As String

    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori
    
'   AGGIORNAMENTO TABELLA ORGANIZZAZIONE_LAYOUTSUPPORTO
    If Not (listaSelezionati Is Nothing) Then
        sql = "DELETE FROM ORGANIZ_TIPOSUP_LAYOUTSUP" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND IDTIPOSUPPORTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each layout In listaSelezionati
            sql = "INSERT INTO ORGANIZ_TIPOSUP_LAYOUTSUP (IDORGANIZZAZIONE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
                " VALUES (" & idOrganizzazioneSelezionata & ", " & _
                idRecordSelezionato & ", " & _
                layout.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next layout
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = TSR_NUOVO
'            condizioneSql = " AND IDTIPOSUPPORTO = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("ORGANIZ_TIPOSUP_LAYOUTSUP", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_NUOVO, condizioneSql)
'        End If
    End If
    
    Call ChiudiConnessioneBD
    
Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaDisponibili()
    Dim layout As clsElementoLista
    Dim chiaveLayout As String
    
    For Each layout In listaDisponibili
        chiaveLayout = ChiaveId(layout.idElementoLista)
        Call listaSelezionati.Add(layout, chiaveLayout)
    Next layout
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim layout As clsElementoLista
    Dim condizioneSql As String

    Call ApriConnessioneBD
        
On Error GoTo gestioneErrori
    
'   AGGIORNAMENTO TABELLA ORGANIZZAZIONE_LAYOUTSUPPORTO
    If Not (listaSelezionati Is Nothing) Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            condizioneSql = " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'            Call AggiornaParametriSessioneSuRecord("ORGANIZ_TIPOSUP_LAYOUTSUP", "IDTIPOSUPPORTO", idRecordSelezionato, TSR_ELIMINATO, condizioneSql)
'        Else
            sql = "DELETE FROM ORGANIZ_TIPOSUP_LAYOUTSUP" & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND IDTIPOSUPPORTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
        For Each layout In listaSelezionati
'            If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'                sql = "INSERT INTO ORGANIZ_TIPOSUP_LAYOUTSUP" & _
'                    " (IDORGANIZZAZIONE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
'                    " SELECT " & _
'                    idOrganizzazioneSelezionata & " IDORGANIZZAZIONE, " & _
'                    idRecordSelezionato & " IDTIPOSUPPORTO, " & _
'                    layout.idElementoLista & " IDLAYOUTSUPPORTO" & _
'                    " FROM DUAL" & _
'                    " MINUS" & _
'                    " SELECT IDORGANIZZAZIONE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO" & _
'                    " FROM ORGANIZ_TIPOSUP_LAYOUTSUP" & _
'                    " WHERE IDTIPOSUPPORTO = " & idRecordSelezionato & _
'                    " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                    " AND IDLAYOUTSUPPORTO = " & layout.idElementoLista
'                SETAConnection.Execute sql, n, adCmdText
'                condizioneSql = " AND IDLAYOUTSUPPORTO = " & layout.idElementoLista & _
'                    " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'                Call AggiornaParametriSessioneSuRecord("ORGANIZ_TIPOSUP_LAYOUTSUP", "IDTIPOSUPPORTO", idRecordSelezionato, TSR_NUOVO, condizioneSql)
'            Else
                sql = "INSERT INTO ORGANIZ_TIPOSUP_LAYOUTSUP (IDORGANIZZAZIONE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
                    " VALUES (" & idOrganizzazioneSelezionata & ", " & _
                    idRecordSelezionato & ", " & _
                    layout.idElementoLista & ")"
'            End If
            SETAConnection.Execute sql, n, adCmdText
        Next layout
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = TSR_NUOVO
'            condizioneSql = " AND IDTIPOSUPPORTO = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("ORGANIZ_TIPOSUP_LAYOUTSUP", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_NUOVO, condizioneSql)
'        End If
    End If
    
    Call ChiudiConnessioneBD
    
Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub
