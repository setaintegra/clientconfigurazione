VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazionePiantaGrigliaPosti 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   11130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14085
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11130
   ScaleWidth      =   14085
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkLunghezzaStringaPosto 
      Caption         =   "4 caratteri per nome posto"
      Height          =   375
      Left            =   11400
      TabIndex        =   74
      Top             =   600
      Width           =   2415
   End
   Begin VB.PictureBox pctGriglia 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   8415
      Left            =   60
      ScaleHeight     =   8355
      ScaleWidth      =   10875
      TabIndex        =   49
      Top             =   600
      Width           =   10935
   End
   Begin VB.Frame fraPostiSelezionati 
      Caption         =   "Posti Selezionati"
      Height          =   1155
      Left            =   11460
      TabIndex        =   44
      Top             =   2880
      Width           =   2475
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Eli&mina"
         Height          =   315
         Left            =   180
         TabIndex        =   48
         Top             =   300
         Width           =   1035
      End
      Begin VB.CommandButton cmdSposta 
         Caption         =   "&Sposta"
         Height          =   315
         Left            =   180
         TabIndex        =   47
         Top             =   720
         Width           =   1035
      End
      Begin VB.CommandButton cmdFlipY 
         Caption         =   "Flip &Y"
         Height          =   315
         Left            =   1320
         TabIndex        =   46
         Top             =   720
         Width           =   1035
      End
      Begin VB.CommandButton cmdFlipX 
         Caption         =   "Flip &X"
         Height          =   315
         Left            =   1320
         TabIndex        =   45
         Top             =   300
         Width           =   1035
      End
   End
   Begin VB.HScrollBar scbOrizzontale 
      Height          =   255
      Left            =   60
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   9000
      Width           =   10935
   End
   Begin VB.VScrollBar scbVerticale 
      Height          =   8415
      Left            =   10980
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   600
      Width           =   255
   End
   Begin VB.HScrollBar scbZoom 
      Height          =   255
      Left            =   11400
      TabIndex        =   41
      TabStop         =   0   'False
      Top             =   1260
      Width           =   1875
   End
   Begin VB.Frame fraCreaPosti 
      Height          =   4935
      Left            =   11460
      TabIndex        =   10
      Top             =   4380
      Width           =   2475
      Begin VB.CommandButton cmdConfermaCreazione 
         Caption         =   "Conferma"
         Height          =   315
         Left            =   720
         TabIndex        =   24
         Top             =   4500
         Width           =   1035
      End
      Begin VB.TextBox txtXIniziale 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Width           =   675
      End
      Begin VB.TextBox txtYIniziale 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   675
      End
      Begin VB.TextBox txtNumeroFile 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   21
         Top             =   960
         Width           =   675
      End
      Begin VB.TextBox txtNumeroPosti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   20
         Top             =   2040
         Width           =   675
      End
      Begin VB.ComboBox cmbArea 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   19
         Top             =   4080
         Width           =   2175
      End
      Begin VB.TextBox txtNomePrimaFila 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   18
         Top             =   1320
         Width           =   675
      End
      Begin VB.TextBox txtNomePrimoPosto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   17
         Top             =   2400
         Width           =   675
      End
      Begin VB.TextBox txtStepFile 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   16
         Top             =   1680
         Width           =   675
      End
      Begin VB.TextBox txtStepPosti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   15
         Top             =   2760
         Width           =   675
      End
      Begin VB.TextBox txtXshift 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   720
         TabIndex        =   14
         Top             =   3120
         Width           =   435
      End
      Begin VB.TextBox txtXIntervalloPosti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   13
         Top             =   3120
         Width           =   435
      End
      Begin VB.TextBox txtYshift 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   720
         TabIndex        =   12
         Top             =   3480
         Width           =   435
      End
      Begin VB.TextBox txtYIntervalloPosti 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   11
         Top             =   3480
         Width           =   435
      End
      Begin VB.Label lblXIniziale 
         Caption         =   "X iniziale"
         Height          =   195
         Left            =   840
         TabIndex        =   39
         Top             =   300
         Width           =   615
      End
      Begin VB.Label lblYIniziale 
         Caption         =   "Y iniziale"
         Height          =   195
         Left            =   840
         TabIndex        =   38
         Top             =   660
         Width           =   615
      End
      Begin VB.Label lblNumeroFile 
         Caption         =   "num. file"
         Height          =   195
         Left            =   840
         TabIndex        =   37
         Top             =   1020
         Width           =   915
      End
      Begin VB.Label lblNumeroPosti 
         Caption         =   "num. posti"
         Height          =   195
         Left            =   840
         TabIndex        =   36
         Top             =   2100
         Width           =   915
      End
      Begin VB.Label lblArea 
         Caption         =   "Area"
         Height          =   195
         Left            =   120
         TabIndex        =   35
         Top             =   3840
         Width           =   975
      End
      Begin VB.Label lblNomePrimaFila 
         Caption         =   "nome 1� fila"
         Height          =   195
         Left            =   840
         TabIndex        =   34
         Top             =   1380
         Width           =   915
      End
      Begin VB.Label lblNomePrimoPosto 
         Caption         =   "nome 1� posto"
         Height          =   195
         Left            =   840
         TabIndex        =   33
         Top             =   2460
         Width           =   1035
      End
      Begin VB.Label lblStepFile 
         Caption         =   "step file"
         Height          =   195
         Left            =   840
         TabIndex        =   32
         Top             =   1740
         Width           =   915
      End
      Begin VB.Label lblStepPosti 
         Caption         =   "step posti"
         Height          =   195
         Left            =   840
         TabIndex        =   31
         Top             =   2820
         Width           =   915
      End
      Begin VB.Label lblXShift_01 
         Alignment       =   1  'Right Justify
         Caption         =   "shift X di"
         Height          =   195
         Left            =   60
         TabIndex        =   30
         Top             =   3180
         Width           =   615
      End
      Begin VB.Label lblXShift_02 
         Alignment       =   1  'Right Justify
         Caption         =   "ogni"
         Height          =   195
         Left            =   1200
         TabIndex        =   29
         Top             =   3180
         Width           =   315
      End
      Begin VB.Label lblXShift_03 
         Alignment       =   1  'Right Justify
         Caption         =   "posti"
         Height          =   195
         Left            =   2040
         TabIndex        =   28
         Top             =   3180
         Width           =   315
      End
      Begin VB.Label lblYShift_01 
         Alignment       =   1  'Right Justify
         Caption         =   "shift Y di"
         Height          =   195
         Left            =   60
         TabIndex        =   27
         Top             =   3540
         Width           =   615
      End
      Begin VB.Label lblYShift_02 
         Alignment       =   1  'Right Justify
         Caption         =   "ogni"
         Height          =   195
         Left            =   1200
         TabIndex        =   26
         Top             =   3540
         Width           =   315
      End
      Begin VB.Label lblYShift_03 
         Alignment       =   1  'Right Justify
         Caption         =   "posti"
         Height          =   195
         Left            =   2040
         TabIndex        =   25
         Top             =   3540
         Width           =   315
      End
   End
   Begin VB.CheckBox chkCreazioneDaTastiera 
      Caption         =   "Crea da Tastiera"
      Height          =   195
      Left            =   11460
      TabIndex        =   9
      Top             =   4200
      Width           =   1755
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10740
      TabIndex        =   8
      Top             =   120
      Width           =   3255
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "&Esci"
      Height          =   315
      Left            =   12240
      TabIndex        =   7
      Top             =   10320
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   12240
      TabIndex        =   6
      Top             =   9960
      Width           =   1035
   End
   Begin VB.CommandButton cmdSalvaInBD 
      Caption         =   "Salva in base dati"
      Height          =   315
      Left            =   11940
      TabIndex        =   5
      Top             =   9600
      Width           =   1575
   End
   Begin VB.Frame fraLineeGuida 
      Caption         =   "Linee guida"
      Height          =   1155
      Left            =   11460
      TabIndex        =   0
      Top             =   1620
      Width           =   2475
      Begin VB.CommandButton cmdLinea 
         Caption         =   "&Linea"
         Height          =   315
         Left            =   1320
         TabIndex        =   4
         Top             =   300
         Width           =   1035
      End
      Begin VB.CommandButton cmdArco 
         Caption         =   "&Arco"
         Height          =   315
         Left            =   1320
         TabIndex        =   3
         Top             =   720
         Width           =   1035
      End
      Begin VB.CommandButton cmdRaggio 
         Caption         =   "&Raggio"
         Height          =   315
         Left            =   180
         TabIndex        =   2
         Top             =   720
         Width           =   1035
      End
      Begin VB.CommandButton cmdPunto 
         Caption         =   "&Punto"
         Height          =   315
         Left            =   180
         TabIndex        =   1
         Top             =   300
         Width           =   1035
      End
   End
   Begin MSComctlLib.StatusBar sbrAttributiPosto 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   40
      Top             =   10815
      Width           =   14085
      _ExtentX        =   24844
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2999
            MinWidth        =   2999
            Key             =   "coordXY"
            Object.ToolTipText     =   "coordinate (x, y)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Key             =   "nomeFila"
            Object.ToolTipText     =   "nome Fila"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Key             =   "nomePosto"
            Object.ToolTipText     =   "nome Posto"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "indiceDiPreferibilita"
            Object.ToolTipText     =   "indice di Preferibilit� Area"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "area"
            Object.ToolTipText     =   "area"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
            Key             =   "postiInArea"
            Object.ToolTipText     =   "posti in area:"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
            Key             =   "postiInPianta"
            Object.ToolTipText     =   "posti in pianta:"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCreazionePosti 
      Caption         =   "Creazione Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   73
      Top             =   9360
      Width           =   2535
   End
   Begin VB.Label lblCreazioneSingoloPosto01 
      Caption         =   "Singolo Posto :"
      Height          =   195
      Left            =   120
      TabIndex        =   72
      Top             =   9660
      Width           =   1095
   End
   Begin VB.Label lblCreazioneSingoloPosto02 
      Caption         =   "DX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   1560
      TabIndex        =   71
      Top             =   9660
      Width           =   1155
   End
   Begin VB.Label lblCreazioneFilaPosti01 
      Caption         =   "Fila Posti :"
      Height          =   195
      Left            =   120
      TabIndex        =   70
      Top             =   9900
      Width           =   1035
   End
   Begin VB.Label lblCreazioneFilaPosti02 
      Caption         =   "DX + trascinamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   1560
      TabIndex        =   69
      Top             =   9900
      Width           =   1935
   End
   Begin VB.Label lblCreazioneRettangoloPosti01 
      Caption         =   "Rettangolo Posti :"
      Height          =   195
      Left            =   120
      TabIndex        =   68
      Top             =   10380
      Width           =   1275
   End
   Begin VB.Label Label2 
      Caption         =   "DX + SHIFT + trascinamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   1560
      TabIndex        =   67
      Top             =   10380
      Width           =   2655
   End
   Begin VB.Label lblSelezionePosti 
      Caption         =   "Selezione Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4440
      TabIndex        =   66
      Top             =   9360
      Width           =   2535
   End
   Begin VB.Label lblSelezioneSingoloPosto01 
      Caption         =   "Singolo Posto :"
      Height          =   195
      Left            =   4440
      TabIndex        =   65
      Top             =   9660
      Width           =   1095
   End
   Begin VB.Label lblSelezioneSingoloPosto02 
      Caption         =   "SX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   5880
      TabIndex        =   64
      Top             =   9660
      Width           =   1155
   End
   Begin VB.Label lblSelezioneFilaPosti01 
      Caption         =   "Fila Posti :"
      Height          =   195
      Left            =   4440
      TabIndex        =   63
      Top             =   10140
      Width           =   1035
   End
   Begin VB.Label lblSelezioneFilaPosti02 
      Caption         =   "SX + ALT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   5880
      TabIndex        =   62
      Top             =   10140
      Width           =   1935
   End
   Begin VB.Label lblSelezioneRettangoloPosti01 
      Caption         =   "Rettangolo Posti :"
      Height          =   195
      Left            =   4440
      TabIndex        =   61
      Top             =   10380
      Width           =   1275
   End
   Begin VB.Label lblSelezioneRettangoloPosti02 
      Caption         =   "SX + SHIFT + trascinamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   5880
      TabIndex        =   60
      Top             =   10380
      Width           =   2715
   End
   Begin VB.Label lblSelezioneSingoliPosti01 
      Caption         =   "Singoli Posti :"
      Height          =   195
      Left            =   4440
      TabIndex        =   59
      Top             =   9900
      Width           =   1095
   End
   Begin VB.Label lblSelezioneSingoliPosti02 
      Caption         =   "SX + CTRL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   5880
      TabIndex        =   58
      Top             =   9900
      Width           =   1155
   End
   Begin VB.Label lblZoom01 
      Caption         =   "Zoom :"
      Height          =   195
      Left            =   11460
      TabIndex        =   57
      Top             =   1020
      Width           =   495
   End
   Begin VB.Label lblZoom02 
      Caption         =   "9 X"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   12000
      TabIndex        =   56
      Top             =   1020
      Width           =   495
   End
   Begin VB.Label lblModificaSingoloPosto02 
      Caption         =   "DX su posti selezionati"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   8820
      TabIndex        =   55
      Top             =   9660
      Width           =   1995
   End
   Begin VB.Label lblModificaPost 
      Caption         =   "Modifica Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   8820
      TabIndex        =   54
      Top             =   9360
      Width           =   1455
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione grafica della Griglia Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   53
      Top             =   120
      Width           =   7695
   End
   Begin VB.Label lblInfo1 
      Alignment       =   1  'Right Justify
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9000
      TabIndex        =   52
      Top             =   660
      Width           =   1635
   End
   Begin VB.Label lblCreazioneFilaCurvaPosti02 
      Caption         =   "DX + CTRL + trascinamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   1560
      TabIndex        =   51
      Top             =   10140
      Width           =   2655
   End
   Begin VB.Label lblCreazioneFilaCurvaPosti01 
      Caption         =   "Fila Curva Posti :"
      Height          =   195
      Left            =   120
      TabIndex        =   50
      Top             =   10140
      Width           =   1215
   End
End
Attribute VB_Name = "frmConfigurazionePiantaGrigliaPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private maxCoordOrizzontale As Long
Private maxCoordVerticale As Long
Private dimOrizzontalePosto As Long
Private dimVerticalePosto As Long
Private passo As Long
Private xStartRetta As Long
Private xEndRetta As Long
Private yStartRetta As Long
Private yEndRetta As Long
Private larghezzaGriglia As Long
Private altezzaGriglia As Long
Private numeroMaxPostiInFila As Integer
Private numeroPostiInFila As Integer
Private numeroFileInRettangolo As Integer
Private numeroMaxPostiXInRettangolo As Integer
Private numeroMaxPostiYInRettangolo As Integer
Private idAreaSelezionata As Long
Private idPostoSelezionato As Long
Private postoSelezionato As clsPosto
Private idPiantaSelezionata As Long
Private nomePrimaFila As String
Private nomeUltimaFila As String
Private nomePrimoPosto As String
Private nomeUltimoPosto As String
Private letteraFinalePosto As String
Private stepPosti As Integer
Private stepFile As Integer
Private zoom As Double
Private x0Schermo As Long
Private y0Schermo As Long
Private indiceDiPreferibilitaArea As Long
Private matriceGrigliaPosti() As clsPosto
Private listaPostiSelezionati As Collection
Private spostamentoInCorso As Boolean
Private internalEvent As Boolean
Private nomeAreaSelezionata As String
Private nomePiantaSelezionata As String
Private numeroPostiInPianta As Long
Private numeroPostiPerArea As Collection
Private grigliaPosti As clsGrigliaPosti
Private maxIdPosto As Long
Private xShift As Integer
Private yShift As Integer
Private xIntervalloPosti As Integer
Private yIntervalloPosti As Integer
Private xFlip As Boolean
Private yFlip As Boolean

Private disegnoLineeGuidaInCorso As Boolean
Private tipoDisegnoLineaGuida As TipoDisegnoLineaGuidaEnum
Private progressivoPunto As Integer
Private listaPunti As Collection
Private listaRaggi As Collection
Private listaArchi As Collection
Private listaLinee As Collection
Private alfa As Double 'per la creazione della fila curva posti
Private beta As Double 'per la creazione della fila curva posti
Private raggio As Double 'per la creazione della fila curva posti

Private metodoCreazionePosti As MetodoCreazionePostiEnum
Private metodoSelezionePosti As MetodoSelezionePostiEnum
Private tipoGriglia As TipoGrigliaEnum
Private exitCodeDettagliPosti As ExitCodeDettagliEnum
Private selezione As AzioneSuPostiEnum

Public Sub Init()
    Set grigliaPosti = New clsGrigliaPosti
    Call grigliaPosti.GrigliaPosti_Init(tipoGriglia, idAreaSelezionata, idPiantaSelezionata)
    internalEvent = True
    pctGriglia.Enabled = False
    chkCreazioneDaTastiera.Enabled = True
    sbrAttributiPosto.Enabled = False
    Set listaPostiSelezionati = New Collection
    spostamentoInCorso = False
    
    '---------------------------------------------------------------------
    disegnoLineeGuidaInCorso = False
    tipoDisegnoLineaGuida = TDLG_NON_SPECIFICATO
    progressivoPunto = 0
    Set listaPunti = New Collection
    Set listaArchi = New Collection
    Set listaRaggi = New Collection
    Set listaLinee = New Collection
    alfa = 0
    beta = 0
    raggio = 0
    '---------------------------------------------------------------------
    
    xFlip = False
    yFlip = False
    zoom = 1
    x0Schermo = 1
    y0Schermo = 1
    maxIdPosto = 0
    Call SetMetodoCreazionePosti(MCP_NON_SPECIFICATO)
    Call CalcolaPassoReticolo
    Call ScrollBars_Init
    Call MatricePostiBD_Init
    Call CreaMatricePostiBD
    Call ContaPosti_Init
    Call pctGriglia_Init
    Call AggiornaAbilitazioneControlli
    internalEvent = False
    Call DisegnaGriglia
    pctGriglia.Enabled = True
    sbrAttributiPosto.Enabled = True
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
'--------------------------------------------------------------------
    cmdLinea.Enabled = False
'--------------------------------------------------------------------
    pctGriglia.Enabled = (chkCreazioneDaTastiera.Value = vbUnchecked)
    Select Case tipoGriglia
        Case TG_PICCOLI_IMPIANTI
            lblInfo1.Caption = "Pianta"
            txtInfo1.Text = nomePiantaSelezionata
        Case TG_GRANDI_IMPIANTI
            lblInfo1.Caption = "Area"
            txtInfo1.Text = nomeAreaSelezionata
    End Select
    txtInfo1.Enabled = False
    If listaPostiSelezionati.count > 0 Then
        fraPostiSelezionati.Enabled = True
        cmdElimina.Enabled = (listaPostiSelezionati.count > 0)
        cmdSposta.Enabled = (listaPostiSelezionati.count > 0)
        cmdFlipX.Enabled = (listaPostiSelezionati.count > 0)
        cmdFlipY.Enabled = (listaPostiSelezionati.count > 0)
    Else
        fraPostiSelezionati.Enabled = False
        cmdElimina.Enabled = False
        cmdSposta.Enabled = False
        cmdFlipX.Enabled = False
        cmdFlipY.Enabled = False
    End If
    lblZoom02.Caption = zoom & " X"
    If chkCreazioneDaTastiera.Value = vbUnchecked Then
        txtXIniziale.Text = ""
        txtYIniziale.Text = ""
        txtNumeroFile.Text = ""
        txtNumeroPosti.Text = ""
        txtNomePrimaFila.Text = ""
        txtNomePrimoPosto.Text = ""
        txtStepFile.Text = ""
        txtStepPosti.Text = ""
        txtXshift.Text = ""
        txtYshift.Text = ""
        txtXIntervalloPosti.Text = ""
        txtYIntervalloPosti.Text = ""
        Call cmbArea.Clear
        cmbArea.Text = ""
    End If
    lblXShift_01.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    lblXShift_02.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    lblXShift_03.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    lblYShift_01.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    lblYShift_02.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    lblYShift_03.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    txtXshift.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    txtYshift.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    txtXIntervalloPosti.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    txtYIntervalloPosti.Enabled = (chkCreazioneDaTastiera.Value = vbChecked) And _
        tipoGriglia = TG_PICCOLI_IMPIANTI
    
    lblXIniziale.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtXIniziale.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblYIniziale.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtYIniziale.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblNumeroFile.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtNumeroFile.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblNumeroPosti.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtNumeroPosti.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblNomePrimaFila.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtNomePrimaFila.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblNomePrimoPosto.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtNomePrimoPosto.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblStepFile.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtStepFile.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblStepPosti.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    txtStepPosti.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    lblArea.Enabled = (chkCreazioneDaTastiera.Value = vbChecked)
    cmbArea.Enabled = (tipoGriglia = TG_PICCOLI_IMPIANTI And _
                        chkCreazioneDaTastiera.Value = vbChecked)
    cmdConfermaCreazione.Enabled = (txtXIniziale.Text <> "" And txtYIniziale.Text <> "" And _
                                    txtNumeroFile.Text <> "" And txtNumeroPosti.Text <> "" And _
                                    txtNomePrimaFila.Text <> "" And txtNomePrimoPosto.Text <> "" And _
                                    txtStepFile.Text <> "" And txtStepPosti.Text <> "" And _
                                    cmbArea.Text <> "")
End Sub

Private Sub cmbArea_Click()
    idAreaSelezionata = cmbArea.ItemData(cmbArea.ListIndex)
    nomeAreaSelezionata = cmbArea.Text
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConfermaCreazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfermaCreazioneDaTastiera
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfermaCreazioneDaTastiera()
    If ValoriCampiOK Then
        If xBD(CLng(xEndRetta)) > maxCoordOrizzontale - dimOrizzontalePosto Or _
           yBD(CLng(yEndRetta)) > maxCoordVerticale - dimVerticalePosto Then
            Call frmMessaggio.Visualizza("NotificaPostiNonDisegnabili")
        Else
            If IsUltimoNomeFilaCorretto(nomePrimaFila, numeroFileInRettangolo, stepFile) And _
                IsUltimoNomePostoCorretto(nomePrimoPosto, numeroPostiInFila, stepPosti, False) Then
                Select Case numeroFileInRettangolo
                    Case 1
                        If numeroPostiInFila = 1 Then
                            Call SetMetodoCreazionePosti(MCP_SINGOLO)
                        Else
                            Call SetMetodoCreazionePosti(MCP_FILA)
                        End If
                    Case Is > 1
                        Call SetMetodoCreazionePosti(MCP_RETTANGOLO)
                    Case Else
                End Select
                Call CreaPosti
            Else
                Call frmMessaggio.Visualizza("NotificaNomiNonCorretti")
            End If
        End If
    End If
End Sub

Private Function ValoriCampiOK() As Boolean

    ValoriCampiOK = True
    xStartRetta = xSchermo(CInt(Trim(txtXIniziale.Text)))
    yStartRetta = ySchermo(CInt(Trim(txtYIniziale.Text)))
    numeroFileInRettangolo = CInt(Trim(txtNumeroFile.Text))
    numeroPostiInFila = CInt(Trim(txtNumeroPosti.Text))
    nomePrimaFila = CStr(Trim(txtNomePrimaFila.Text))
    nomePrimoPosto = CStr(Trim(txtNomePrimoPosto.Text))
    stepFile = CInt(Trim(txtStepFile.Text))
    stepPosti = CInt(Trim(txtStepPosti.Text))
    xEndRetta = xStartRetta + _
                    ((numeroPostiInFila - 1) * dimOrizzontalePosto * passo * zoom)
    yEndRetta = yStartRetta + _
                    ((numeroFileInRettangolo - 1) * dimVerticalePosto * passo * zoom)
    If Trim(txtXshift.Text) = "" Then
        xShift = 0
    Else
        xShift = CInt(Trim(txtXshift.Text))
    End If
    If Trim(txtYshift.Text) = "" Then
        yShift = 0
    Else
        yShift = CInt(Trim(txtYshift.Text))
    End If
    If Trim(txtXIntervalloPosti.Text) = "" Then
        xIntervalloPosti = 1
    Else
        xIntervalloPosti = CInt(Trim(txtXIntervalloPosti.Text))
    End If
    If Trim(txtYIntervalloPosti.Text) = "" Then
        yIntervalloPosti = 1
    Else
        yIntervalloPosti = CInt(Trim(txtYIntervalloPosti.Text))
    End If
End Function

Private Sub DisegnaGriglia()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call DisegnaReticoloSuGriglia
    Call ImpostaColoreInsiemePosti(grigliaPosti.listaPostiInGriglia, ASP_DISEGNA)
    '----------------------------------------------------------------------------------------
    Call DisegnaElementiGuida
    '----------------------------------------------------------------------------------------
    
    internalEvent = internalEventOld
End Sub

Private Sub DisegnaElementiGuida()
    Dim punto As clsPunto
    Dim arco As clsArco
    Dim linea As clsLinea
    
    For Each punto In listaPunti
        Call DisegnaPunto(punto.x, punto.Y)
    Next punto
    For Each arco In listaArchi
        Call DisegnaArco(arco)
    Next arco
    For Each linea In listaLinee
        Call DisegnaLinea(linea)
    Next linea
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub EliminaSequenza(idS As Long)
    Dim sql As String
    Dim rec As OraDynaset
    Dim numPosti As Integer
    Dim idF As Long
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    If idS <> idNessunElementoSelezionato Then
        sql = "SELECT COUNT(IDPOSTO) POSTI FROM POSTO WHERE IDSEQUENZAPOSTI = " & idS
        Set rec = ORADB.CreateDynaset(sql, 0&)
        numPosti = rec("POSTI")
        rec.Close
        
        If numPosti = 0 Then
            idF = idNessunElementoSelezionato
            sql = "SELECT IDFASCIAPOSTI FROM SEQUENZAPOSTI WHERE IDSEQUENZAPOSTI = " & idS
            Set rec = ORADB.CreateDynaset(sql, 0&)
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                idF = rec("IDFASCIAPOSTI")
            End If
            rec.Close
            sql = "DELETE FROM SEQUENZAPOSTI WHERE IDSEQUENZAPOSTI = " & idS
            n = ORADB.ExecuteSQL(sql)
            Call EliminaFascia(idF)
        End If
    End If
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub EliminaFascia(idF As Long)
    Dim numSequenze As Integer
    Dim sql As String
    Dim rec As OraDynaset
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = "SELECT COUNT(IDSEQUENZAPOSTI) SEQ FROM SEQUENZAPOSTI WHERE IDFASCIAPOSTI = " & idF
    Set rec = ORADB.CreateDynaset(sql, 0&)
    numSequenze = rec("SEQ")
    rec.Close
    
    If numSequenze = 0 Then
        sql = "DELETE FROM FASCIAPOSTI WHERE IDFASCIAPOSTI = " & idF
        n = ORADB.ExecuteSQL(sql)
    End If
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub Esci()
    Set grigliaPosti.listaPostiEliminati = Nothing
    Set grigliaPosti.listaPostiCreati = Nothing
    Set grigliaPosti.listaPostiModificati = Nothing
    Set grigliaPosti = Nothing
    Unload Me
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetMaxCoordOrizzontale(xMax As Long)
    maxCoordOrizzontale = xMax
End Sub

Public Sub SetMaxCoordVerticale(yMax As Long)
    maxCoordVerticale = yMax
End Sub

Public Sub SetDimOrizzontalePosto(xDim As Long)
    dimOrizzontalePosto = xDim
End Sub

Public Sub SetDimVerticalePosto(yDim As Long)
    dimVerticalePosto = yDim
End Sub

Private Sub CalcolaPassoReticolo()
    Dim passoVerticale As Integer
    Dim passoOrizzontale As Integer
    
    larghezzaGriglia = CLng(pctGriglia.Width)
    altezzaGriglia = CLng(pctGriglia.Height)
    passoOrizzontale = CLng(pctGriglia.Width / maxCoordOrizzontale)
    passoVerticale = CLng(pctGriglia.Height / maxCoordVerticale)
    
    passo = IIf(passoOrizzontale < passoVerticale, passoOrizzontale, passoVerticale)
End Sub

Private Sub DisegnaReticoloSuGriglia()
    Dim i As Integer
    Dim j As Integer
    Dim col As Long
    Dim dxSchermo As Long
    Dim dySchermo As Long

    For i = x0Schermo To maxCoordOrizzontale
        For j = y0Schermo To maxCoordVerticale
            If (i Mod 10 = 0) Or (j Mod 10 = 0) Then
                col = QBColor(12)
            Else
                col = QBColor(0)
            End If
            pctGriglia.PSet (zoom * passo * (i - x0Schermo), zoom * passo * (j - y0Schermo)), col
        Next j
    Next i
    
    pctGriglia.Line (zoom * passo * (maxCoordOrizzontale - x0Schermo), 0)- _
                    (zoom * passo * (maxCoordOrizzontale - x0Schermo), zoom * passo * (maxCoordVerticale - y0Schermo)), _
                    QBColor(0)
    pctGriglia.Line (0, zoom * passo * (maxCoordVerticale - y0Schermo))- _
                    (zoom * passo * (maxCoordOrizzontale - x0Schermo), zoom * passo * (maxCoordVerticale - y0Schermo)), _
                    QBColor(0)
                    
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EsciESalvaInBaseDati
    
    MousePointer = mousePointerOld
End Sub

Private Sub EsciESalvaInBaseDati()
    Dim dom As CCDominioAttivit�Enum
    Dim stringaNota As String
    Dim count As Integer
    
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            dom = CCDA_AREA
            stringaNota = "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idAreaSelezionata
        Case TG_PICCOLI_IMPIANTI
            dom = CCDA_PIANTA
            stringaNota = "IDPIANTA = " & idPiantaSelezionata
    End Select
    
    Call SalvaInBaseDati
    Call ScriviLog(CCTA_MODIFICA, dom, CCDA_GRIGLIA_POSTI, stringaNota)
    Set listaPostiSelezionati = Nothing
    Set listaPostiSelezionati = New Collection
    Call ControllaSovrapposizionePostiInDB(listaPostiSelezionati)
    If listaPostiSelezionati.count > 0 Then
        Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
        Call frmMessaggio.Visualizza("NotificaPostiSovrapposti")
        Call AggiornaAbilitazioneControlli
    Else
        Call Esci
    End If
End Sub

Private Sub cmdFlipX_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call XFlipPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub XFlipPosti()
    spostamentoInCorso = True
    xFlip = True
End Sub

Private Sub cmdFlipY_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call YFlipPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub YFlipPosti()
    spostamentoInCorso = True
    yFlip = True
End Sub

Private Sub cmdPunto_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    disegnoLineeGuidaInCorso = True
    tipoDisegnoLineaGuida = TDLG_PUNTO
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdLinea_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    disegnoLineeGuidaInCorso = True
    tipoDisegnoLineaGuida = TDLG_LINEA
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdArco_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    disegnoLineeGuidaInCorso = True
    tipoDisegnoLineaGuida = TDLG_ARCO
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdRaggio_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    disegnoLineeGuidaInCorso = True
    tipoDisegnoLineaGuida = TDLG_RAGGIO
    
    MousePointer = mousePointerOld
End Sub
'
'Private Sub DisegnaLineaGuida()
'    Select Case tipoDisegnoLineaGuida
'        Case TDLG_PUNTO
'        Case TDLG_LINEA
'        Case TDLG_RAGGIO
'        Case TDLG_ARCO
'    End Select
'    tipoDisegnoLineaGuida = TDLG_NON_SPECIFICATO
'End Sub

Private Sub cmdSalvaInBD_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SalvaInBaseDati
    
    MousePointer = mousePointerOld
End Sub

Private Sub SalvaInBaseDati()
    Dim count As Integer
    Dim p As clsPosto

'SI TOLGONO DA listaPostiCreati e da listaPostiModificati I POSTI PRESENTI SU listaPostiEliminati
    For Each p In grigliaPosti.listaPostiEliminati
        Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiCreati, p.idPosto)
        Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiModificati, p.idPosto)
    Next p
''SI ELIMINANO I POSTI PRESENTI SU listaPostiEliminati
'    Call EliminaDallaBaseDati
'    Set grigliaPosti.listaPostiEliminati = Nothing
'    Set grigliaPosti.listaPostiEliminati = New Collection
'SI INSERISCONO I POSTI PRESENTI SU listaPostiCreati
    Call InserisciNellaBaseDati
    Set grigliaPosti.listaPostiCreati = Nothing
    Set grigliaPosti.listaPostiCreati = New Collection
'SI ELIMINANO I POSTI PRESENTI SU listaPostiEliminati
    Call EliminaDallaBaseDati
    Set grigliaPosti.listaPostiEliminati = Nothing
    Set grigliaPosti.listaPostiEliminati = New Collection
'SI AGGIORNANO I POSTI PRESENTI SU listaPostiModificati
    Call AggiornaInBaseDati
    Set grigliaPosti.listaPostiModificati = Nothing
    Set grigliaPosti.listaPostiModificati = New Collection
End Sub

Private Sub AggiornaInBaseDati()
    Dim sql As String
    Dim p As clsPosto
    Dim idP As Long
    Dim n As Integer
    Dim nomeF As String
    Dim nomeP As String
    Dim sqlSequenza As String
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    For Each p In grigliaPosti.listaPostiModificati
        If (p.idSequenzaPosti = idNessunElementoSelezionato Or p.idSequenzaPosti = 0) Then
            sqlSequenza = " IDSEQUENZAPOSTI = NULL, ORDINEINSEQUENZA = NULL,"
        Else
            sqlSequenza = ""
        End If
        nomeF = SqlStringValue(Trim(UCase(p.nomeFila)))
        nomeP = SqlStringValue(Trim(UCase(p.nomePosto)))
        sql = "UPDATE POSTO SET "
        sql = sql & " COORDINATAORIZZONTALE = " & p.xPosto & ", "
        sql = sql & " COORDINATAVERTICALE = " & p.yPosto & ", "
        sql = sql & " NOMEFILA = " & nomeF & ", "
        sql = sql & " NOMEPOSTO = " & nomeP & ", "
        sql = sql & sqlSequenza
        sql = sql & " IDAREA = " & p.idArea
        sql = sql & " WHERE IDPOSTO = " & p.idPosto
        n = ORADB.ExecuteSQL(sql)
    Next p
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub InserisciNellaBaseDati()
    Dim p As clsPosto
    Dim sql As String
    Dim idNuovoPosto As Long
    Dim newPosto As clsPosto
    Dim postoProvv As clsPosto
    Dim n As Long
    Dim nomeF As String
    Dim nomeP As String
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    For Each p In grigliaPosti.listaPostiCreati
        idNuovoPosto = OttieniIdentificatoreDaSequenza("SQ_POSTO")
        nomeF = SqlStringValue(Trim(UCase(p.nomeFila)))
        nomeP = SqlStringValue(Trim(UCase(p.nomePosto)))
        sql = "INSERT INTO POSTO (IDPOSTO, NOMEFILA, NOMEPOSTO,"
        sql = sql & " COORDINATAORIZZONTALE, COORDINATAVERTICALE, IDAREA,"
        sql = sql & " IDSEQUENZAPOSTI, ORDINEINSEQUENZA)"
        sql = sql & " VALUES ("
        sql = sql & idNuovoPosto & ", "
        sql = sql & nomeF & ", "
        sql = sql & nomeP & ", "
        sql = sql & p.xPosto & ", "
        sql = sql & p.yPosto & ", "
        sql = sql & p.idArea & ", "
        sql = sql & "NULL, NULL)"
        n = ORADB.ExecuteSQL(sql)
        Set newPosto = grigliaPosti.OttieniPostoDaId(grigliaPosti.listaPostiInGriglia, p.idPosto)
        newPosto.idPosto = idNuovoPosto
        Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiInGriglia, p.idPosto)
        Call grigliaPosti.listaPostiInGriglia.Add(newPosto, ChiaveId(newPosto.idPosto))
        If Not (grigliaPosti.listaPostiModificati Is Nothing) Then
            If grigliaPosti.listaPostiModificati.count > 0 Then
                Set postoProvv = grigliaPosti.OttieniPostoDaId(grigliaPosti.listaPostiModificati, p.idPosto)
                If Not (postoProvv Is Nothing) Then
                    Set newPosto = postoProvv.ClonaPosto
                    newPosto.idPosto = idNuovoPosto
                    Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiModificati, p.idPosto)
                    Call grigliaPosti.listaPostiModificati.Add(newPosto, ChiaveId(newPosto.idPosto))
                End If
            End If
        End If
        If Not (grigliaPosti.listaPostiEliminati Is Nothing) Then
            If grigliaPosti.listaPostiEliminati.count > 0 Then
                Set postoProvv = grigliaPosti.OttieniPostoDaId(grigliaPosti.listaPostiEliminati, p.idPosto)
                If Not (postoProvv Is Nothing) Then
                    Set newPosto = postoProvv.ClonaPosto
                    newPosto.idPosto = idNuovoPosto
                    Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiEliminati, p.idPosto)
                    Call grigliaPosti.listaPostiEliminati.Add(newPosto, ChiaveId(newPosto.idPosto))
                End If
            End If
        End If
    Next p
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim p As clsPosto
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    For Each p In grigliaPosti.listaPostiEliminati
'        sql = "DELETE FROM POSTO WHERE IDPOSTO = " & p.idPosto
'        sql = sql & " AND IDAREA = " & p.idArea
'        sql = sql & " AND NOMEFILA = " & SqlStringValue(p.nomeFila)
'        sql = sql & " AND NOMEPOSTO = " & SqlStringValue(p.nomePosto)
'        sql = sql & " AND COORDINATAORIZZONTALE = " & p.xPosto
'        sql = sql & " AND COORDINATAVERTICALE = " & p.yPosto
        
        sql = "DELETE FROM POSTO_POSTOCORRISPONDENTE WHERE IDPOSTO = " & p.idPosto
        n = ORADB.ExecuteSQL(sql)
        
        sql = "DELETE FROM POSTO_POSTOCORRISPONDENTE WHERE IDPOSTOCORRISPONDENTE = " & p.idPosto
        n = ORADB.ExecuteSQL(sql)
        
        sql = "DELETE FROM POSTO WHERE IDPOSTO = " & p.idPosto
        n = ORADB.ExecuteSQL(sql)
        If n = 1 Then
            Call EliminaSequenza(p.idSequenzaPosti)
        End If
        
    Next p
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub cmdSposta_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub SpostaPosti()
    spostamentoInCorso = True
End Sub

Private Sub chkCreazioneDaTastiera_Click()
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        Call CaricaValoriCombo(cmbArea)
    ElseIf tipoGriglia = TG_GRANDI_IMPIANTI Then
        cmbArea.Text = nomeAreaSelezionata
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub pctGriglia_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call AzioneSuGriglia_Update(Button, Shift, x, Y)
End Sub

Private Sub AzioneSuGriglia_Update(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim xSch As Long
    Dim ySch As Long
    Dim idA As Long
    Dim p As clsPosto
    Dim drawModeOld As Integer
    Dim xMin As Integer
    Dim yMin As Integer
    Dim xMax As Integer
    Dim yMax As Integer
    Dim xDistanzaMouse As Integer
    Dim yDistanzaMouse As Integer
    Dim XCoord As Integer
    Dim YCoord As Integer
    
    idA = idNessunElementoSelezionato
    If xBD(CLng(x)) > maxCoordOrizzontale Or xBD(CLng(x)) <= 0 Or _
       yBD(CLng(Y)) > maxCoordVerticale Or yBD(CLng(Y)) <= 0 Then
        'Do Nothing
    Else
        xSch = Intero(x)
        ySch = Intero(Y)
        
        'SPOSTAMENTO GRAFICO POSTI & FLIP (NESSUN PULSANTE PREMUTO)
        If Button <> vbLeftButton And Button <> vbRightButton And spostamentoInCorso Then
            pctGriglia.AutoRedraw = False
            pctGriglia.Refresh
            xMin = MinCoordX(listaPostiSelezionati)
            yMin = MinCoordY(listaPostiSelezionati)
            xMax = MaxCoordX(listaPostiSelezionati)
            yMax = MaxCoordY(listaPostiSelezionati)
            xDistanzaMouse = xBD(xSch) - xMin
            yDistanzaMouse = yBD(ySch) - yMin
            For Each p In listaPostiSelezionati
                If xFlip = True Then
                    XCoord = p.xPosto + xDistanzaMouse + 2 * (-p.xPosto + xMax)
                    YCoord = p.yPosto + yDistanzaMouse
                End If
                If yFlip = True Then
                    XCoord = p.xPosto + xDistanzaMouse
                    YCoord = p.yPosto + yDistanzaMouse + 2 * (-p.yPosto + yMax)
                End If
                If Not (xFlip Or yFlip) Then
                    XCoord = p.xPosto + xDistanzaMouse
                    YCoord = p.yPosto + yDistanzaMouse
                End If
                Call DisegnaPosto(XCoord, YCoord, QBColor(0), True)
            Next p
        End If
        pctGriglia.AutoRedraw = True
        
        'SELEZIONE POSTI
        If Button = vbLeftButton And Shift = vbShiftMask Then
            pctGriglia.AutoRedraw = False
            pctGriglia.DrawStyle = vbDot
            pctGriglia.Refresh
            pctGriglia.Line (xStartRetta, yStartRetta)-(xSch, ySch), QBColor(0), B
        End If
        pctGriglia.AutoRedraw = True
        pctGriglia.DrawStyle = vbSolid
        
        'CREAZIONE POSTI
        If Button = vbRightButton Then
            pctGriglia.AutoRedraw = False
            pctGriglia.DrawStyle = vbDot
            pctGriglia.Refresh
            If Shift = vbShiftMask Then
                pctGriglia.Line (xStartRetta, yStartRetta)-(xSch, ySch), QBColor(0), B
            '----------------------------------------------------------------------------
            ElseIf Shift = vbCtrlMask Then
                Dim centro As clsPunto
                
                Set centro = listaPunti.Item(ChiaveId(progressivoPunto - 1))
                raggio = Sqr((xSchermo(centro.x) - xStartRetta) ^ 2 + (ySchermo(centro.Y) - yStartRetta) ^ 2)
                If centro.x - xBD(xStartRetta) = 0 Then
                    alfa = PI_GRECO / 2 'ma potrebbe essere anche 3/2 pi_greco
                Else
                    alfa = Atn((yStartRetta - ySchermo(centro.Y)) / (xStartRetta - xSchermo(centro.x))) * PI_GRECO * 2
                End If
                If centro.x - xBD(xSch) = 0 Then
                    beta = PI_GRECO / 2 'ma potrebbe essere anche 3/2 pi_greco
                Else
                    beta = Atn((ySch - ySchermo(centro.Y)) / (xSch - xSchermo(centro.x))) * PI_GRECO * 2
                End If
                If Abs(beta) < 2 * PI_GRECO Then
                    pctGriglia.Circle (xSchermo(centro.x), ySchermo(centro.Y)), _
                        raggio, QBColor(0), alfa, beta
                Else
                    pctGriglia.Circle (xSchermo(centro.x), ySchermo(centro.Y)), _
                        raggio, QBColor(0), 2 * PI_GRECO, beta Mod (2 * PI_GRECO)
                End If

'                If centro.X - xBD(xStartRetta) = 0 Then
'                    alfa = PI_GRECO / 2 'ma potrebbe essere anche 3/2 pi_greco
'                Else
'                    alfa = Atn((yStartRetta - ySchermo(centro.y)) / (xStartRetta - xSchermo(centro.X))) * PI_GRECO * 2
'                End If
'                If centro.X - xBD(xSch) = 0 Then
'                    beta = PI_GRECO / 2 'ma potrebbe essere anche 3/2 pi_greco
'                Else
'                    beta = Atn((ySch - ySchermo(centro.y)) / (xSch - xSchermo(centro.X))) * PI_GRECO * 2
'                End If
'                If Abs(beta) < 2 * PI_GRECO Then
'                    If Abs(alfa) <= PI_GRECO / 2 Then
'                        pctGriglia.Circle (xSchermo(centro.X), ySchermo(centro.y)), _
'                            raggio, QBColor(0), alfa, beta
'                    ElseIf Abs(alfa) <= PI_GRECO And Abs(alfa) > PI_GRECO / 2 Then
'                        pctGriglia.Circle (xSchermo(centro.X), ySchermo(centro.y)), _
'                            raggio, QBColor(0), PI_GRECO - alfa, beta
'                    ElseIf Abs(alfa) <= (PI_GRECO * 3 / 2) And Abs(alfa) > PI_GRECO Then
'                        pctGriglia.Circle (xSchermo(centro.X), ySchermo(centro.y)), _
'                            raggio, QBColor(0), PI_GRECO + alfa, beta
'                    ElseIf Abs(alfa) <= PI_GRECO * 2 And Abs(alfa) > (PI_GRECO * 3 / 2) Then
'                        pctGriglia.Circle (xSchermo(centro.X), ySchermo(centro.y)), _
'                            raggio, QBColor(0), 2 * PI_GRECO - alfa, beta
'                    End If
'                Else
'                    pctGriglia.Circle (xSchermo(centro.X), ySchermo(centro.y)), _
'                        raggio, QBColor(0), 2 * PI_GRECO, beta Mod (2 * PI_GRECO)
'                End If
            '----------------------------------------------------------------------------
            Else
                pctGriglia.Line (xStartRetta, yStartRetta)-(xSch, ySch), QBColor(0)
            End If
        End If
        pctGriglia.AutoRedraw = True
        pctGriglia.DrawStyle = vbSolid
                
        If xBD(xSch) > maxCoordOrizzontale Or xBD(xSch) < 0 Or _
           yBD(ySch) > maxCoordVerticale Or yBD(ySch) < 0 Then
            sbrAttributiPosto.Panels(1).Text = ""
            sbrAttributiPosto.Panels(2).Text = ""
            sbrAttributiPosto.Panels(3).Text = ""
            sbrAttributiPosto.Panels(4).Text = ""
            sbrAttributiPosto.Panels(5).Text = ""
            sbrAttributiPosto.Panels(6).Text = ""
            sbrAttributiPosto.Panels(7).Text = ""
        Else
            sbrAttributiPosto.Panels(1).Text = "X = " & CStr(xBD(xSch)) & "; " & "Y = " & CStr(yBD(ySch))
            sbrAttributiPosto.Panels(2).Text = "Fila: " & matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomeFila
            sbrAttributiPosto.Panels(3).Text = "Posto: " & matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomePosto
            sbrAttributiPosto.Panels(4).Text = "Indice di preferibilit� Area: " & IIf(matriceGrigliaPosti(xBD(xSch), yBD(ySch)).indiceDiPreferibilit�Area = _
                        idNessunElementoSelezionato, "", matriceGrigliaPosti(xBD(xSch), yBD(ySch)).indiceDiPreferibilit�Area)
            sbrAttributiPosto.Panels(5).Text = "Area: " & IIf(tipoGriglia = TG_GRANDI_IMPIANTI, nomeAreaSelezionata, _
                        matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomeArea)
            idA = matriceGrigliaPosti(xBD(xSch), yBD(ySch)).idArea
            If idA = idNessunElementoSelezionato Then
                If tipoGriglia = TG_GRANDI_IMPIANTI Then
                    sbrAttributiPosto.Panels(6).Text = "Posti in Area: " & numeroPostiPerArea.Item(ChiaveId(idAreaSelezionata))
                Else
                    sbrAttributiPosto.Panels(6).Text = "Posti in Area: " & "0"
                End If
            Else
                sbrAttributiPosto.Panels(6).Text = "Posti in Area: " & numeroPostiPerArea.Item(ChiaveId(idA))
            End If
            sbrAttributiPosto.Panels(7).Text = "Posti in Pianta: " & numeroPostiInPianta
            If matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomeFila <> "" Or matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomePosto <> "" Then
                pctGriglia.ToolTipText = matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomeFila & " - " & matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomePosto
            Else
                pctGriglia.ToolTipText = ""
            End If
        End If
    End If
    
End Sub

Private Sub pctGriglia_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call AzioneSuGriglia_Start(Button, Shift, x, Y)
End Sub

Private Sub AzioneSuGriglia_Start(Button As Integer, Shift As Integer, x As Single, Y As Single)
    xStartRetta = x
    yStartRetta = Y
End Sub

Private Sub DisegnaPosto(XCoord As Integer, YCoord As Integer, col As Long, bordoNero As Boolean) 'GLI ARGOMENTI SONO DEL TIPO xBD, yBD
    Dim x0 As Long
    Dim y0 As Long
    Dim x1 As Long
    Dim y1 As Long
    Dim deltaX As Double
    Dim deltaY As Double

    deltaX = dimOrizzontalePosto / 10
    deltaY = dimVerticalePosto / 10

    x0 = CLng((XCoord - x0Schermo + deltaX) * passo * zoom)
    y0 = CLng((YCoord - y0Schermo + deltaY) * passo * zoom)
    x1 = CLng((XCoord + dimOrizzontalePosto - x0Schermo - deltaX) * _
        passo * zoom)
    y1 = CLng((YCoord + dimVerticalePosto - y0Schermo - deltaY) * _
        passo * zoom)

    x0 = IIf(x0 < 0, 0, x0): x0 = IIf(x0 > larghezzaGriglia, larghezzaGriglia, x0)
    y0 = IIf(y0 < 0, 0, y0): y0 = IIf(y0 > altezzaGriglia, altezzaGriglia, y0)
    x1 = IIf(x1 < 0, 0, x1): x1 = IIf(x1 > larghezzaGriglia, larghezzaGriglia, x1)
    y1 = IIf(y1 < 0, 0, y1): y1 = IIf(y1 > altezzaGriglia, altezzaGriglia, y1)

    pctGriglia.Line (x0, y0)-(x1, y1), col, BF
    If bordoNero Then
        pctGriglia.Line (x0, y0)-(x1, y1), QBColor(0), B
    End If
            
End Sub

Private Sub CaricaFormDettagliFilaPosti(moda As ModalitaFormDettagliEnum, nomiPostoLunghiPermessi As Boolean)
    Call frmDettagliFilaPosti.SetModalitaForm(moda)
    Call frmDettagliFilaPosti.SetNumeroMaxPostiInFila(numeroMaxPostiInFila)
    Call frmDettagliFilaPosti.SetTipoGriglia(tipoGriglia)
    If tipoGriglia = TG_GRANDI_IMPIANTI Then
        Call frmDettagliFilaPosti.SetIdAreaSelezionata(idAreaSelezionata)
        Call frmDettagliFilaPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
    End If
    Call frmDettagliFilaPosti.Init(nomiPostoLunghiPermessi)
End Sub

Private Sub CalcolaNumeroMaxPostiInFila()
    Dim proiezioneX As Integer
    Dim proiezioneY As Integer
    Dim postiX As Integer
    Dim postiY As Integer
    
    proiezioneX = Abs(xBD(xEndRetta) - xBD(xStartRetta))
    proiezioneY = Abs(yBD(yEndRetta) - yBD(yStartRetta))
    
    postiX = Int(proiezioneX / dimOrizzontalePosto) + 1 'posto con coordinate pari all'ultimo estremo
    postiY = Int(proiezioneY / dimVerticalePosto) + 1 'posto con coordinate pari all'ultimo estremo
    
    numeroMaxPostiInFila = IIf(postiX > postiY, postiX, postiY)
    
End Sub

Private Sub CalcolaNumeroMaxPostiInFilaCurva() 'si tratta di un valore puramente teorico
    Dim ascissaCurvilinea As Double
    Dim diametroPosto As Double
    
    ascissaCurvilinea = Abs(beta - alfa) * raggio / (passo * zoom)
'    ascissaCurvilinea = Abs(beta - alfa) * raggio
    diametroPosto = IIf(dimOrizzontalePosto > dimVerticalePosto, dimOrizzontalePosto * Sqr(2), dimVerticalePosto * Sqr(2))
    numeroMaxPostiInFila = ascissaCurvilinea / (diametroPosto)
    
End Sub

Private Function xBD(xGriglia As Long) As Integer
    Dim valore As Integer
'   CONVERTE DA X(PICTURE BOX) A X CHE SARA' SCRITTO IN BD
    valore = Int(xGriglia / (passo * zoom)) + x0Schermo
    If valore > 0 Then
        If valore < maxCoordOrizzontale Then
            xBD = valore
        Else
            xBD = maxCoordOrizzontale
        End If
    Else
        xBD = 1
    End If
End Function

Private Function yBD(yGriglia As Long) As Integer
    Dim valore As Integer
'   CONVERTE DA Y(PICTURE BOX) A Y CHE SARA' SCRITTO IN BD
    valore = Int(yGriglia / (passo * zoom)) + y0Schermo
    If valore > 0 Then
        If valore < maxCoordVerticale Then
            yBD = valore
        Else
            yBD = maxCoordVerticale
        End If
    Else
        yBD = 1
    End If
End Function

Private Function xSchermo(xInt As Integer) As Long
'   CONVERTE DA X(DATA BASE) A X DEL CONTROLLO PCTBOX
    xSchermo = CLng(xInt - x0Schermo) * passo * zoom
End Function

Private Function ySchermo(yInt As Integer) As Long
'   CONVERTE DA X(DATA BASE) A X DEL CONTROLLO PCTBOX
    ySchermo = CLng(yInt - y0Schermo) * passo * zoom
End Function

Public Sub SetNumeroPostiInFila(num As Integer)
    numeroPostiInFila = num
End Sub

Private Sub pctGriglia_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call AzioneSuGriglia_End(Button, Shift, x, Y)
End Sub

Private Function RilevaIdPostoSelezionato(x As Integer, Y As Integer) As Long
    RilevaIdPostoSelezionato = matriceGrigliaPosti(x, Y).idPosto
End Function

Private Function RilevaPostoSelezionato(x As Integer, Y As Integer) As clsPosto
    Set RilevaPostoSelezionato = matriceGrigliaPosti(x, Y)
End Function

Private Sub CalcolaNumeroMaxPostiInRettangolo()
    Dim proiezioneX As Integer
    Dim proiezioneY As Integer
    Dim ingombroX As Integer
    Dim ingombroY As Integer
    
    proiezioneX = Abs(xBD(xEndRetta) - xBD(xStartRetta))
    proiezioneY = Abs(yBD(yEndRetta) - yBD(yStartRetta))
    
    ingombroX = dimOrizzontalePosto
    ingombroY = dimVerticalePosto
    
    numeroMaxPostiXInRettangolo = Int(proiezioneX / ingombroX) + 1 'posto con coordinate pari all'ultimo estremo
    numeroMaxPostiYInRettangolo = Int(proiezioneY / ingombroY) + 1 'posto con coordinate pari all'ultimo estremo
    
End Sub

Private Sub CaricaFormDettagliRettangoloPosti(moda As ModalitaFormDettagliEnum, nomiPostoLunghiPermessi As Boolean)
    Call frmDettagliRettangoloPosti.SetModalitaForm(moda)
    Call frmDettagliRettangoloPosti.SetNumeroMaxPostiInFila(numeroMaxPostiXInRettangolo)
    Call frmDettagliRettangoloPosti.SetNumeroMaxFileInRettangolo(numeroMaxPostiYInRettangolo)
    Call frmDettagliRettangoloPosti.SetTipoGriglia(tipoGriglia)
    If tipoGriglia = TG_GRANDI_IMPIANTI Then
        Call frmDettagliRettangoloPosti.SetIdAreaSelezionata(idAreaSelezionata)
        Call frmDettagliRettangoloPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
    End If
    Call frmDettagliRettangoloPosti.Init(nomiPostoLunghiPermessi)
End Sub

Public Sub SetNumeroFileInRettangolo(numF As Integer)
    numeroFileInRettangolo = numF
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetIdPiantaSelezionata(idP As Long)
    idPiantaSelezionata = idP
End Sub

Public Sub SetNomePrimaFila(nomePF As String)
    nomePrimaFila = nomePF
End Sub

Public Sub SetNomeUltimaFila(nomeUF As String)
    nomeUltimaFila = nomeUF
End Sub

Public Sub SetNomePrimoPosto(nomePP As String)
    nomePrimoPosto = nomePP
End Sub

Public Sub SetNomeUltimoPosto(nomeUP As String)
    nomeUltimoPosto = nomeUP
End Sub

Public Sub SetStepPosti(stP As Integer)
    stepPosti = stP
End Sub

Public Sub SetStepFile(stF As Integer)
    stepFile = stF
End Sub

Public Sub SetLetteraFinalePosto(letteraFinale As String)
    letteraFinalePosto = letteraFinale
End Sub

Public Sub SetXShift(xSh As Integer)
    xShift = xSh
End Sub

Public Sub SetYShift(ySh As Integer)
    yShift = ySh
End Sub

Public Sub SetXIntervalloPosti(xInt As Integer)
    xIntervalloPosti = xInt
End Sub

Public Sub SetYIntervalloPosti(yInt As Integer)
    yIntervalloPosti = yInt
End Sub

Public Sub SetMetodoCreazionePosti(m As MetodoCreazionePostiEnum)
    metodoCreazionePosti = m
End Sub

Public Sub SetMetodoSelezionePosti(m As MetodoSelezionePostiEnum)
    metodoSelezionePosti = m
End Sub

Public Sub SetExitCodeDettagliPosti(ec As ExitCodeDettagliEnum)
    exitCodeDettagliPosti = ec
End Sub

Private Sub pctGriglia_Init()
    pctGriglia.MousePointer = vbCrosshair
    pctGriglia.DrawWidth = 1
    pctGriglia.Cls
End Sub

Private Sub ScrollBars_Init()
    
    scbOrizzontale.SmallChange = 1
    scbOrizzontale.LargeChange = 10
    scbOrizzontale.Min = 1
    If x0Schermo + Int(larghezzaGriglia / (passo * zoom)) <= maxCoordOrizzontale Then
        scbOrizzontale.max = maxCoordOrizzontale - Int(larghezzaGriglia / (passo * zoom))
    Else
        scbOrizzontale.max = 1
    End If
    
    scbVerticale.SmallChange = 1
    scbVerticale.LargeChange = 10
    scbVerticale.Min = 1
    If y0Schermo + Int(altezzaGriglia / (passo * zoom)) <= maxCoordVerticale Then
        scbVerticale.max = maxCoordVerticale - Int(altezzaGriglia / (passo * zoom))
    Else
        scbVerticale.max = 1
    End If
    
    scbZoom.Min = 1
    scbZoom.max = 5
    scbZoom.Value = 1
End Sub

Private Sub scbZoom_Change()
    If Not internalEvent Then
        Call ApplicaZoom
    End If
End Sub

Private Sub ApplicaZoom()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    zoom = scbZoom.Value
    If x0Schermo + Int(larghezzaGriglia / (passo * zoom)) <= maxCoordOrizzontale Then
        scbOrizzontale.max = maxCoordOrizzontale - Int(larghezzaGriglia / (passo * zoom))
    Else
        scbOrizzontale.max = 1
    End If
    If y0Schermo + Int(altezzaGriglia / (passo * zoom)) <= maxCoordVerticale Then
        scbVerticale.max = maxCoordVerticale - Int(altezzaGriglia / (passo * zoom))
    Else
        scbVerticale.max = 1
    End If
    Call pctGriglia_Init
    Call DisegnaGriglia
    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
    Call AggiornaAbilitazioneControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub scbOrizzontale_Change()
    If Not internalEvent Then
        Call ApplicaScrollOrizzontale
    End If
End Sub

Private Sub ApplicaScrollOrizzontale()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    x0Schermo = scbOrizzontale.Value
    If x0Schermo + Int(larghezzaGriglia / (passo * zoom)) <= maxCoordOrizzontale Then
        scbOrizzontale.max = maxCoordOrizzontale - Int(larghezzaGriglia / (passo * zoom))
    Else
        scbOrizzontale.max = 1
    End If
    Call pctGriglia_Init
    Call DisegnaGriglia
    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
    
    MousePointer = mousePointerOld
End Sub

Private Sub scbVerticale_Change()
    If Not internalEvent Then
        Call ApplicaScrollVerticale
    End If
End Sub

Private Sub ApplicaScrollVerticale()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    y0Schermo = scbVerticale.Value
    If y0Schermo + Int(altezzaGriglia / (passo * zoom)) <= maxCoordVerticale Then
        scbVerticale.max = maxCoordVerticale - Int(altezzaGriglia / (passo * zoom))
    Else
        scbVerticale.max = 1
    End If
    Call pctGriglia_Init
    Call DisegnaGriglia
    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
    
    MousePointer = mousePointerOld
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub

Public Sub SetNomePiantaSelezionata(nomeP As String)
    nomePiantaSelezionata = nomeP
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox)
    Dim i As Integer
    Dim area As classeArea
    
    i = 1
    For Each area In listaAreeAssociateAPianta
        cmb.AddItem area.nomeArea
        cmb.ItemData(i - 1) = area.idArea
        i = i + 1
    Next area
End Sub

Private Sub txtNomePrimaFila_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomePrimoPosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNumeroFile_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNumeroPosti_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtStepFile_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtStepPosti_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtXIniziale_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtYIniziale_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub AggiornaMatricePostiBD(id As Long, idA As Long, x As Integer, Y As Integer, _
                                  nomeF As String, nomeP As String, nomeA As String, _
                                  indice As Long)
    Dim i As Integer
    Dim j As Integer
    
    If dimOrizzontalePosto = 1 And dimVerticalePosto = 1 Then
        If matriceGrigliaPosti(x, Y).idPosto = id Then
            matriceGrigliaPosti(x, Y).nomeFila = nomeF
            matriceGrigliaPosti(x, Y).nomePosto = nomeP
            matriceGrigliaPosti(x, Y).idArea = idA
            matriceGrigliaPosti(x, Y).nomeArea = nomeA
        End If
    Else
        For i = 0 To dimOrizzontalePosto - 1
            For j = 0 To dimVerticalePosto - 1
                If matriceGrigliaPosti(x + i, Y + j).idPosto = id Then
' perch� c'� il prossimo nome?
'                    matriceGrigliaPosti(x + i, Y + j).nomeFila = ProssimoNomeFila(nomeF, 0)
'                    matriceGrigliaPosti(x + i, Y + j).nomePosto = ProssimoNomePosto(nomeP, 0)
                    matriceGrigliaPosti(x + i, Y + j).nomeFila = nomeF
                    matriceGrigliaPosti(x + i, Y + j).nomePosto = nomeP
                    matriceGrigliaPosti(x + i, Y + j).idArea = idA
                    matriceGrigliaPosti(x + i, Y + j).nomeArea = nomeA
                End If
            Next j
        Next i
    End If
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Function IsSequenzaPostiSpostabile(xMin As Integer, yMin As Integer, _
                                           xMax As Integer, yMax As Integer) As Boolean
    If xBD(xEndRetta) + (xMax - xMin) + dimOrizzontalePosto < maxCoordOrizzontale And _
       yBD(yEndRetta) + (yMax - yMin) + dimVerticalePosto < maxCoordVerticale Then
        IsSequenzaPostiSpostabile = True
    Else
        IsSequenzaPostiSpostabile = False
    End If
End Function

Private Function IsSequenzaPostiRiposizionabile(xMin As Integer, yMin As Integer, _
                                           xMax As Integer, yMax As Integer, _
                                           lista As Collection) As Boolean
    IsSequenzaPostiRiposizionabile = False
    If xBD(xEndRetta) + (xMax - xMin) + dimOrizzontalePosto < maxCoordOrizzontale And _
       yBD(yEndRetta) + (yMax - yMin) + dimVerticalePosto < maxCoordVerticale Then
        If lista.count > 1 Then
            IsSequenzaPostiRiposizionabile = True
        Else
            If matriceGrigliaPosti(xBD(xEndRetta), yBD(yEndRetta)).idPosto = idNessunElementoSelezionato Then
                IsSequenzaPostiRiposizionabile = True
            End If
        End If
    End If
End Function

Private Sub MatricePostiBD_Init()
    Dim i As Integer
    Dim j As Integer
    Dim postoNullo As clsPosto
    
    Set postoNullo = New clsPosto
    ReDim matriceGrigliaPosti(1 To maxCoordOrizzontale, 1 To maxCoordVerticale)
    
    postoNullo.idPosto = idNessunElementoSelezionato
    postoNullo.nomeFila = ""
    postoNullo.nomePosto = ""
    postoNullo.indiceDiPreferibilit�Area = 0
    postoNullo.idArea = idNessunElementoSelezionato
    postoNullo.idSequenzaPosti = idNessunElementoSelezionato
    For i = 1 To maxCoordOrizzontale
        For j = 1 To maxCoordVerticale
            Set matriceGrigliaPosti(i, j) = postoNullo
        Next j
    Next i
End Sub

Private Sub CreaMatricePostiBD()
    Dim x As Integer
    Dim Y As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim posto As clsPosto
    
    k = 0
    For Each posto In grigliaPosti.listaPostiInGriglia
        x = posto.xPosto
        Y = posto.yPosto
        For i = 0 To dimOrizzontalePosto - 1
            For j = 0 To dimVerticalePosto - 1
                Set matriceGrigliaPosti(x + i, Y + j) = posto
            Next j
        Next i
        k = k + 1
    Next posto
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Public Sub EliminaPosti()
    Dim i As Integer
    Dim id As Long
    Dim postiEliminati As Long
    Dim postoDaEliminare As clsPosto
    Dim newNumeroPostiInArea As Long
    Dim listaPostiNonEliminabili As Collection
    Dim descrizionePostiNonEliminabili As String
    Dim elencoPostiNonEliminabili As String

    Set listaPostiNonEliminabili = New Collection
    postiEliminati = 0
    For Each postoDaEliminare In listaPostiSelezionati
        If (grigliaPosti.OttieniPostoDaId(grigliaPosti.listaPostiEliminati, postoDaEliminare.idPosto) Is Nothing) Then
            If IsPostoEliminabile(postoDaEliminare.idPosto) Then
                idAreaSelezionata = postoDaEliminare.idArea
                Call grigliaPosti.listaPostiEliminati.Add(postoDaEliminare, ChiaveId(postoDaEliminare.idPosto))
                Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiInGriglia, postoDaEliminare.idPosto)
                newNumeroPostiInArea = numeroPostiPerArea(ChiaveId(idAreaSelezionata)) - 1
                Call numeroPostiPerArea.Remove(ChiaveId(idAreaSelezionata))
                Call numeroPostiPerArea.Add(newNumeroPostiInArea, ChiaveId(idAreaSelezionata))
                postiEliminati = postiEliminati + 1
            Else
                descrizionePostiNonEliminabili = postoDaEliminare.nomeArea & " fila " & postoDaEliminare.nomeFila & "; posto " & postoDaEliminare.nomePosto
                Call listaPostiNonEliminabili.Add(descrizionePostiNonEliminabili)
            End If
        End If
    Next postoDaEliminare
    If listaPostiNonEliminabili.count > 0 Then
        elencoPostiNonEliminabili = ArgomentoMessaggio(listaPostiNonEliminabili)
        Call frmMessaggio.Visualizza("NotificaPostiNonEliminabili", elencoPostiNonEliminabili)
    End If
    
    numeroPostiInPianta = numeroPostiInPianta - postiEliminati
    
    'cancella i posti dalla griglia
    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_CANCELLA)
    'aggiorna matrice
    Call MatricePostiBD_Init
    Call CreaMatricePostiBD
    'disegna nuova griglia
    Call DisegnaGriglia
    Set listaPostiSelezionati = Nothing
    Set listaPostiSelezionati = New Collection
    Call AggiornaAbilitazioneControlli
End Sub

Private Function IsPostoEliminabile(idP As Long) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim esistonoTitoliEmessi As Boolean
    Dim esistonoProtezioniAttive As Boolean
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
'   NON CI SONO TITOLI EMESSI
    esistonoTitoliEmessi = False
    sql = "SELECT COUNT(IDTITOLO) NUMTIT FROM TITOLO WHERE IDPOSTO = " & idP
    Set rec = ORADB.CreateDynaset(sql, 0&)
    esistonoTitoliEmessi = (rec("NUMTIT") > 0)
    rec.Close
    
'   NON CI SONO PROTEZIONI ATTIVE
    esistonoProtezioniAttive = False
    sql = "SELECT DISTINCT IDPROTEZIONEPOSTO"
    sql = sql & " FROM PROTEZIONEPOSTO PP, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R"
    sql = sql & " WHERE PP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
    sql = sql & " AND R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
    sql = sql & " AND P.IDTIPOSTATOPRODOTTO <> " & TSP_ATTIVO
    sql = sql & " AND (R.DATAORAINIZIO + 1) < SYSDATE"
    sql = sql & " AND PP.IDPOSTO = " & idP
    sql = sql & " UNION"
    sql = sql & " SELECT DISTINCT IDPROTEZIONEPOSTO"
    sql = sql & " FROM PROTEZIONEPOSTO PP, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R"
    sql = sql & " WHERE PP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
    sql = sql & " AND R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
    sql = sql & " AND 1 = (SELECT COUNT(PR.IDRAPPRESENTAZIONE) FROM PROTEZIONEPOSTO PP, PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R"
    sql = sql & " WHERE PP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
    sql = sql & " AND R.IDRAPPRESENTAZIONE = PR.IDRAPPRESENTAZIONE"
    sql = sql & " AND PP.IDPOSTO = " & idP & ")"
    sql = sql & " AND PP.IDPOSTO = " & idP
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.EOF And rec.BOF) Then
        esistonoProtezioniAttive = True
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    IsPostoEliminabile = Not esistonoTitoliEmessi And Not esistonoProtezioniAttive
End Function

Private Function MaxCoordX(lista As Collection) As Integer
    Dim p As clsPosto
    Dim maxProvvisorio As Integer
    
    maxProvvisorio = 0
    For Each p In lista
        If p.xPosto > maxProvvisorio Then
            maxProvvisorio = p.xPosto
        End If
    Next p
    MaxCoordX = maxProvvisorio
End Function

Private Function MaxCoordY(lista As Collection) As Integer
    Dim p As clsPosto
    Dim maxProvvisorio As Integer
    
    maxProvvisorio = 0
    For Each p In lista
        If p.yPosto > maxProvvisorio Then
            maxProvvisorio = p.yPosto
        End If
    Next p
    MaxCoordY = maxProvvisorio
End Function

Private Function MinCoordX(lista As Collection) As Integer
    Dim p As clsPosto
    Dim minProvvisorio As Integer
    
    minProvvisorio = 30000
    For Each p In lista
        If p.xPosto < minProvvisorio Then
            minProvvisorio = p.xPosto
        End If
    Next p
    MinCoordX = minProvvisorio
End Function

Private Function MinCoordY(lista As Collection) As Integer
    Dim p As clsPosto
    Dim minProvvisorio As Integer
    
    minProvvisorio = 30000
    For Each p In lista
        If p.yPosto < minProvvisorio Then
            minProvvisorio = p.yPosto
        End If
    Next p
    MinCoordY = minProvvisorio
End Function

Private Sub RicavaPostiAppartenentiAllaStessaFila(id As Long)
    Dim postoCorrente As clsPosto
    Dim p As clsPosto
    
    Set postoCorrente = grigliaPosti.OttieniPostoDaId(grigliaPosti.listaPostiInGriglia, id)
    For Each p In grigliaPosti.listaPostiInGriglia
        If postoCorrente.nomeFila = p.nomeFila And postoCorrente.idArea = p.idArea Then
            Call listaPostiSelezionati.Add(p)
        End If
    Next p
End Sub

Private Sub ImpostaColoreInsiemePosti(lista As Collection, sel As AzioneSuPostiEnum)
    Dim posto As clsPosto
    
    For Each posto In lista
        Select Case sel
            Case ASP_DISEGNA
                Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(3), True)
            Case ASP_SELEZIONA
                Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(10), False)
            Case ASP_DESELEZIONA
                Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(3), True)
            Case ASP_CANCELLA
                Call DisegnaPosto(posto.xPosto, posto.yPosto, &H8000000F, False)
            Case Else
        End Select
    Next posto
    
End Sub

Private Sub RilevaPostiRettangolo()
    Dim stepX As Integer
    Dim stepY As Integer
    Dim p As clsPosto
    
    If Abs(xBD(xEndRetta) - xBD(xStartRetta)) = 0 Then
        stepX = 1
    Else
        stepX = (xBD(xEndRetta) - xBD(xStartRetta)) / Abs(xBD(xEndRetta) - xBD(xStartRetta))
    End If
    If Abs(yBD(yEndRetta) - yBD(yStartRetta)) = 0 Then
        stepY = 1
    Else
        stepY = (yBD(yEndRetta) - yBD(yStartRetta)) / Abs(yBD(yEndRetta) - yBD(yStartRetta))
    End If
    
    For Each p In grigliaPosti.listaPostiInGriglia
        If Sgn(xBD(xStartRetta) - stepX - p.xPosto) = Sgn(p.xPosto - xBD(xEndRetta) - stepX) And _
                Sgn(yBD(yStartRetta) - stepY - p.yPosto) = Sgn(p.yPosto - yBD(yEndRetta) - stepY) Then
            Call listaPostiSelezionati.Add(p)
        End If
    Next p
End Sub

Private Sub ContaPosti_Init()
    Dim sql As String
    Dim rec As OraDynaset
    Dim sql1 As String
    Dim rec1 As OraDynaset
    Dim idA As Long
    Dim numero As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
        
    Set numeroPostiPerArea = New Collection
        
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            sql = "SELECT COUNT(IDPOSTO) NUMERO"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPiantaSelezionata
            Set rec = ORADB.CreateDynaset(sql, 0&)
            rec.MoveFirst
            numeroPostiInPianta = rec("NUMERO")
            rec.Close
            Call numeroPostiPerArea.Add(grigliaPosti.listaPostiInGriglia.count, ChiaveId(idAreaSelezionata))
        Case TG_PICCOLI_IMPIANTI
            numeroPostiInPianta = grigliaPosti.listaPostiInGriglia.count
            sql = "SELECT IDAREA, NOME FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata
            Set rec = ORADB.CreateDynaset(sql, 0&)
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idA = rec("IDAREA")
                    sql1 = "SELECT COUNT(IDPOSTO) NUMERO FROM POSTO WHERE IDAREA = " & idA
                    Set rec1 = ORADB.CreateDynaset(sql1, 0&)
                    rec1.MoveFirst
                    numero = rec1("NUMERO")
                    Call numeroPostiPerArea.Add(numero, ChiaveId(idA))
                    rec1.Close
                    rec.MoveNext
                Wend
            End If
            rec.Close
    End Select
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Public Sub CreaPosti()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim h As Integer
    Dim numeroPosti As Long
    Dim idNuovoPosto As Long
    Dim nuovoPosto As clsPosto
    Dim xSpostamento As Integer
    Dim ySpostamento As Integer
    Dim postoDuplicato As String
    Dim postoSovrapposto As String
    Dim newNumeroPostiInArea As Long
    Dim listaPostiDuplicati As Collection
    Dim listaPostiSovrapposti As Collection
    Dim listaPostiInGriglia_old As Collection
    
    Set listaPostiDuplicati = New Collection
    Set listaPostiSovrapposti = New Collection
    Set listaPostiInGriglia_old = grigliaPosti.ClonaListaPostiInGriglia
    numeroPosti = 0
    Select Case metodoCreazionePosti
        Case MCP_SINGOLO
            Set nuovoPosto = New clsPosto
            maxIdPosto = maxIdPosto + 1
            nuovoPosto.idPosto = maxIdPosto
            nuovoPosto.nomeFila = nomePrimaFila
            nuovoPosto.nomePosto = nomePrimoPosto
            nuovoPosto.idArea = idAreaSelezionata
            nuovoPosto.nomeArea = nomeAreaSelezionata
            nuovoPosto.xPosto = xBD(xStartRetta)
            nuovoPosto.yPosto = yBD(yStartRetta)
            If grigliaPosti.IsPostoDuplicato(nuovoPosto, listaPostiInGriglia_old) Then
                postoDuplicato = "Fila:" & nuovoPosto.nomeFila & "; " & "Posto:" & nuovoPosto.nomePosto
                Call listaPostiDuplicati.Add(postoDuplicato)
            ElseIf matriceGrigliaPosti(nuovoPosto.xPosto, nuovoPosto.yPosto).idPosto <> idNessunElementoSelezionato Then
                postoSovrapposto = "X:" & nuovoPosto.xPosto & "; " & "Y:" & nuovoPosto.yPosto
                Call listaPostiSovrapposti.Add(postoSovrapposto)
            Else
                numeroPosti = numeroPosti + 1
                Call DisegnaPosto(nuovoPosto.xPosto, nuovoPosto.yPosto, QBColor(3), True)
                For k = 0 To dimOrizzontalePosto - 1
                    For h = 0 To dimVerticalePosto - 1
                        Set matriceGrigliaPosti(nuovoPosto.xPosto + k, nuovoPosto.yPosto + h) = nuovoPosto
                    Next h
                Next k
                Call grigliaPosti.listaPostiInGriglia.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                Call grigliaPosti.listaPostiCreati.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                numeroPostiInPianta = numeroPostiInPianta + 1
            End If
        Case MCP_FILA
            xSpostamento = 0
            ySpostamento = 0
            If numeroPostiInFila > 1 Then
                For i = 1 To numeroPostiInFila
                    Set nuovoPosto = New clsPosto
                    maxIdPosto = maxIdPosto + 1
                    nuovoPosto.idPosto = maxIdPosto
                    nuovoPosto.nomeFila = nomePrimaFila
                    nuovoPosto.nomePosto = ProssimoNomePosto(nomePrimoPosto, (i - 1) * stepPosti)
                    nuovoPosto.idArea = idAreaSelezionata
                    nuovoPosto.nomeArea = nomeAreaSelezionata
                    nuovoPosto.xPosto = xBD(xStartRetta) + xSpostamento + Int((i - 0.5) / xIntervalloPosti) * xShift
                    nuovoPosto.yPosto = yBD(yStartRetta) + ySpostamento + Int((i - 0.5) / yIntervalloPosti) * yShift
                    If grigliaPosti.IsPostoDuplicato(nuovoPosto, listaPostiInGriglia_old) Then
                        postoDuplicato = "Fila:" & nuovoPosto.nomeFila & "; " & "Posto:" & nuovoPosto.nomePosto
                        Call listaPostiDuplicati.Add(postoDuplicato)
                    ElseIf matriceGrigliaPosti(nuovoPosto.xPosto, nuovoPosto.yPosto).idPosto <> idNessunElementoSelezionato Then
                        postoSovrapposto = "X:" & nuovoPosto.xPosto & "; " & "Y:" & nuovoPosto.yPosto
                        Call listaPostiSovrapposti.Add(postoSovrapposto)
                    Else
                        numeroPosti = numeroPosti + 1
                        Call DisegnaPosto(nuovoPosto.xPosto, nuovoPosto.yPosto, QBColor(3), True)
                        For k = 0 To dimOrizzontalePosto - 1
                            For h = 0 To dimVerticalePosto - 1
                                Set matriceGrigliaPosti(nuovoPosto.xPosto + k, nuovoPosto.yPosto + h) = nuovoPosto
                            Next h
                        Next k
                        Call grigliaPosti.listaPostiInGriglia.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                        Call grigliaPosti.listaPostiCreati.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                        numeroPostiInPianta = numeroPostiInPianta + 1
                    End If
                    xSpostamento = xSpostamento + Intero((xBD(xEndRetta) - xBD(xStartRetta)) / _
                            (numeroPostiInFila - 1))
                    ySpostamento = ySpostamento + Intero((yBD(yEndRetta) - yBD(yStartRetta)) / _
                            (numeroPostiInFila - 1))
                Next i
            Else
                MsgBox "Selezione non valida"
            End If
        Case MCP_FILA_CURVA
            Dim scartoAngolare As Double
            
            xSpostamento = 0
            ySpostamento = 0
            scartoAngolare = Abs(beta - alfa) / (numeroPostiInFila - 1)
            If numeroPostiInFila > 1 Then
                For i = 1 To numeroPostiInFila
                    Set nuovoPosto = New clsPosto
                    maxIdPosto = maxIdPosto + 1
                    nuovoPosto.idPosto = maxIdPosto
                    nuovoPosto.nomeFila = nomePrimaFila
                    nuovoPosto.nomePosto = ProssimoNomePosto(nomePrimoPosto, (i - 1) * stepPosti)
                    nuovoPosto.idArea = idAreaSelezionata
                    nuovoPosto.nomeArea = nomeAreaSelezionata
                    nuovoPosto.xPosto = xBD(xStartRetta) + xSpostamento + Int((i - 0.5) / xIntervalloPosti) * xShift
                    nuovoPosto.yPosto = yBD(yStartRetta) + ySpostamento + Int((i - 0.5) / yIntervalloPosti) * yShift
                    If grigliaPosti.IsPostoDuplicato(nuovoPosto, listaPostiInGriglia_old) Then
                        postoDuplicato = "Fila:" & nuovoPosto.nomeFila & "; " & "Posto:" & nuovoPosto.nomePosto
                        Call listaPostiDuplicati.Add(postoDuplicato)
                    ElseIf matriceGrigliaPosti(nuovoPosto.xPosto, nuovoPosto.yPosto).idPosto <> idNessunElementoSelezionato Then
                        postoSovrapposto = "X:" & nuovoPosto.xPosto & "; " & "Y:" & nuovoPosto.yPosto
                        Call listaPostiSovrapposti.Add(postoSovrapposto)
                    Else
                        numeroPosti = numeroPosti + 1
                        Call DisegnaPosto(nuovoPosto.xPosto, nuovoPosto.yPosto, QBColor(3), True)
                        For k = 0 To dimOrizzontalePosto - 1
                            For h = 0 To dimVerticalePosto - 1
                                Set matriceGrigliaPosti(nuovoPosto.xPosto + k, nuovoPosto.yPosto + h) = nuovoPosto
                            Next h
                        Next k
                        Call grigliaPosti.listaPostiInGriglia.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                        Call grigliaPosti.listaPostiCreati.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                        numeroPostiInPianta = numeroPostiInPianta + 1
                    End If
                    xSpostamento = -(raggio / (passo * zoom)) + Intero((raggio / (passo * zoom)) * Cos(alfa + i * scartoAngolare))
                    ySpostamento = -Intero((raggio / (passo * zoom)) * sIn(alfa + i * scartoAngolare))
                Next i
            Else
                MsgBox "Selezione non valida"
            End If
        Case MCP_RETTANGOLO
            xSpostamento = 0
            ySpostamento = 0
            If numeroPostiInFila > 1 And numeroFileInRettangolo > 1 Then
                For i = 1 To numeroPostiInFila
                    ySpostamento = 0
                    For j = 1 To numeroFileInRettangolo
                        Set nuovoPosto = New clsPosto
                        maxIdPosto = maxIdPosto + 1
                        nuovoPosto.idPosto = maxIdPosto
                        nuovoPosto.nomeFila = ProssimoNomeFila(nomePrimaFila, (j - 1) * stepFile)
                        nuovoPosto.nomePosto = ProssimoNomePosto(nomePrimoPosto, (i - 1) * stepPosti) & letteraFinalePosto
                        nuovoPosto.idArea = idAreaSelezionata
                        nuovoPosto.nomeArea = nomeAreaSelezionata
                        nuovoPosto.xPosto = xBD(xStartRetta) + xSpostamento + Int((j - 0.5) / xIntervalloPosti) * xShift
                        nuovoPosto.yPosto = yBD(yStartRetta) + ySpostamento + Int((i - 0.5) / yIntervalloPosti) * yShift
                        If grigliaPosti.IsPostoDuplicato(nuovoPosto, listaPostiInGriglia_old) Then
                            postoDuplicato = "Fila:" & nuovoPosto.nomeFila & "; " & "Posto:" & nuovoPosto.nomePosto
                            Call listaPostiDuplicati.Add(postoDuplicato)
                        ElseIf matriceGrigliaPosti(nuovoPosto.xPosto, nuovoPosto.yPosto).idPosto <> idNessunElementoSelezionato Then
                            postoSovrapposto = "X:" & nuovoPosto.xPosto & "; " & "Y:" & nuovoPosto.yPosto
                            Call listaPostiSovrapposti.Add(postoSovrapposto)
                        Else
                            numeroPosti = numeroPosti + 1
                            Call DisegnaPosto(nuovoPosto.xPosto, nuovoPosto.yPosto, QBColor(3), True)
                            For k = 0 To dimOrizzontalePosto - 1
                                For h = 0 To dimVerticalePosto - 1
                                    Set matriceGrigliaPosti(nuovoPosto.xPosto + k, nuovoPosto.yPosto + h) = nuovoPosto
                                Next h
                            Next k
                            Call grigliaPosti.listaPostiInGriglia.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                            Call grigliaPosti.listaPostiCreati.Add(nuovoPosto, ChiaveId(nuovoPosto.idPosto))
                            numeroPostiInPianta = numeroPostiInPianta + 1
                        End If
                        If numeroFileInRettangolo > 1 Then
                            ySpostamento = ySpostamento + Intero((yBD(yEndRetta) - yBD(yStartRetta)) / _
                                    (numeroFileInRettangolo - 1))
                        End If
                    Next j
                    If numeroPostiInFila > 1 Then
                        xSpostamento = xSpostamento + Intero((xBD(xEndRetta) - xBD(xStartRetta)) / _
                            (numeroPostiInFila - 1))
                    End If
                Next i
            Else
                MsgBox "Selezione non valida"
            End If
        Case Else
            'Do Nothing
    End Select
    
    newNumeroPostiInArea = numeroPostiPerArea(ChiaveId(idAreaSelezionata)) + numeroPosti
    Call numeroPostiPerArea.Remove(ChiaveId(idAreaSelezionata))
    Call numeroPostiPerArea.Add(newNumeroPostiInArea, ChiaveId(idAreaSelezionata))
    If listaPostiDuplicati.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreDuplicazionePosto", nomeAreaSelezionata, ArgomentoMessaggio(listaPostiDuplicati))
    End If
    If listaPostiSovrapposti.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreSovrapposizionePosto", nomeAreaSelezionata, ArgomentoMessaggio(listaPostiSovrapposti))
    End If
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub RiposizionaPosti(lista As Collection)
    Dim id As Long
    Dim i As Integer
    Dim xMin As Integer
    Dim yMin As Integer
    Dim xMax As Integer
    Dim yMax As Integer
    Dim xShift As Integer
    Dim yShift As Integer
    Dim newPosto As clsPosto
    Dim xDistanzaMouse As Integer
    Dim yDistanzaMouse As Integer
    Dim postoSpostato As clsPosto
    Dim postoSovrapposto As String
    
    xMin = MinCoordX(lista)
    yMin = MinCoordY(lista)
    xMax = MaxCoordX(lista)
    yMax = MaxCoordY(lista)
    xDistanzaMouse = xBD(xEndRetta) - xMin
    yDistanzaMouse = yBD(yEndRetta) - yMin
    
    'cancella i posti dalla griglia
    Call ImpostaColoreInsiemePosti(lista, ASP_CANCELLA)
    
    If IsSequenzaPostiRiposizionabile(xMin, yMin, xMax, yMax, lista) Then
        For Each postoSpostato In lista
            If xFlip = True Then
                xShift = xDistanzaMouse + 2 * (-postoSpostato.xPosto + xMax)
                yShift = yDistanzaMouse
            End If
            If yFlip = True Then
                xShift = xDistanzaMouse
                yShift = yDistanzaMouse + 2 * (-postoSpostato.yPosto + yMax)
            End If
            If Not (xFlip Or yFlip) Then
                xShift = xDistanzaMouse
                yShift = yDistanzaMouse
            End If
            Set newPosto = postoSpostato.ClonaPosto
            newPosto.xPosto = newPosto.xPosto + xShift
            newPosto.yPosto = newPosto.yPosto + yShift
            Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiInGriglia, postoSpostato.idPosto)
            Call grigliaPosti.listaPostiInGriglia.Add(newPosto, ChiaveId(newPosto.idPosto))
            Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiModificati, postoSpostato.idPosto)
            Call grigliaPosti.listaPostiModificati.Add(newPosto, ChiaveId(newPosto.idPosto))
        Next postoSpostato
        
        'aggiorna matrice
        Call MatricePostiBD_Init
        Call CreaMatricePostiBD
        'disegna nuova griglia
        Call DisegnaGriglia
        Set listaPostiSelezionati = Nothing
        Set listaPostiSelezionati = New Collection
        Call AggiornaAbilitazioneControlli
    Else
        Call ImpostaColoreInsiemePosti(lista, ASP_DISEGNA)
        Call frmMessaggio.Visualizza("NotificaPostiNonRiposizionabili")
    End If
    xFlip = False
    yFlip = False
End Sub

Private Sub ControllaSovrapposizionePostiInDB(listaPosti As Collection)
    Dim sql As String
    Dim rec As OraDynaset
    Dim posto As clsPosto
    Dim i As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    i = 0
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            sql = "SELECT DISTINCT P1.IDPOSTO IDP, P1.IDAREA IDA, P1.NOMEFILA NF, P1.NOMEPOSTO NP,"
            sql = sql & " P1.COORDINATAORIZZONTALE XP, P1.COORDINATAVERTICALE YP"
            sql = sql & " FROM POSTO P1, POSTO P2"
            sql = sql & " WHERE P1.IDAREA = " & idAreaSelezionata
            sql = sql & " AND P1.IDAREA = P2.IDAREA"
            sql = sql & " AND P1.IDPOSTO <> P2.IDPOSTO"
            sql = sql & " AND (P1.COORDINATAORIZZONTALE = P2.COORDINATAORIZZONTALE"
            sql = sql & " AND P1.COORDINATAVERTICALE = P2.COORDINATAVERTICALE)"
            sql = sql & " ORDER BY P1.COORDINATAORIZZONTALE, P1.COORDINATAVERTICALE, P1.IDPOSTO"
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT DISTINCT P1.IDPOSTO, P1.IDAREA, P1.NOMEFILA, P1.NOMEPOSTO,"
            sql = sql & " P1.COORDINATAORIZZONTALE, P1.COORDINATAVERTICALE"
            sql = sql & " FROM POSTO P1, POSTO P2"
            sql = sql & " WHERE P1.IDAREA IN (SELECT IDAREA FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & ")"
            sql = sql & " AND P2.IDAREA IN (SELECT IDAREA FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & ")"
            sql = sql & " AND P1.IDPOSTO <> P2.IDPOSTO"
            sql = sql & " AND (P1.COORDINATAORIZZONTALE = P2.COORDINATAORIZZONTALE"
            sql = sql & " AND P1.COORDINATAVERTICALE = P2.COORDINATAVERTICALE)"
            sql = sql & " ORDER BY P1.COORDINATAORIZZONTALE, P1.COORDINATAVERTICALE, P1.IDPOSTO"
        End Select
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            i = i + 1
            If (i Mod 2) = 0 Then 'PRENDO L'ID MAGGIORE TRA I DUE CON LE STESSE COORDINATE
                Set posto = New clsPosto
                posto.idPosto = rec("IDP")
                posto.idArea = rec("IDA")
                posto.xPosto = rec("XP")
                posto.yPosto = rec("YP")
                posto.nomeFila = rec("NF")
                posto.nomePosto = rec("NP")
                Call listaPosti.Add(posto)
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub AzioneSuGriglia_End(Button As Integer, Shift As Integer, x As Single, Y As Single)
    
    If xBD(CLng(x)) > maxCoordOrizzontale Or xBD(CLng(x)) < 0 Or _
       yBD(CLng(Y)) > maxCoordVerticale Or yBD(CLng(Y)) < 0 Then
        'Do Nothing
    Else
        xEndRetta = x
        yEndRetta = Y
        Select Case Button
            Case vbRightButton
                If xBD(CLng(xEndRetta)) > maxCoordOrizzontale - dimOrizzontalePosto Or _
                   yBD(CLng(yEndRetta)) > maxCoordVerticale - dimVerticalePosto Then
                    Call frmMessaggio.Visualizza("NotificaPostiNonDisegnabili")
                Else
                    If Shift <> vbShiftMask And Shift <> vbCtrlMask And Shift <> vbAltMask Then
                        If xStartRetta = xEndRetta And yStartRetta = yEndRetta Then
                            idPostoSelezionato = matriceGrigliaPosti(xBD(xEndRetta), yBD(yEndRetta)).idPosto
                            If idPostoSelezionato = idNessunElementoSelezionato Then
                                Call CaricaFormDettagliPosto(MFC_CREA_POSTI)
                                Select Case exitCodeDettagliPosti
                                    Case EC_DP_CONFERMA
                                        Call SetMetodoCreazionePosti(MCP_SINGOLO)
                                        Call CreaPosti
                                    Case Else
                                        'Do Nothing
                                End Select
                            Else
                                Dim m As ModalitaFormDettagliEnum
                                Dim p As New clsPosto
                                Set p = grigliaPosti.OttieniPostoDaId(grigliaPosti.listaPostiInGriglia, idPostoSelezionato)
                                If listaPostiSelezionati.count > 0 Then
                                    Select Case metodoSelezionePosti
                                        Case MSP_SINGOLO
                                            m = MFC_MODIFICA_ATTRIBUTI_POSTO
                                        Case MSP_FILA
                                            m = MFC_MODIFICA_ATTRIBUTI_FILA_POSTI
                                        Case MSP_RETTANGOLO
                                            m = MFC_MODIFICA_ATTRIBUTI_RETTANGOLO_POSTI
                                    End Select
                                    Call SetMetodoSelezionePosti(MSP_NON_SPECIFICATO)
                                    Call CaricaFormDettagliPosto(m, p)
                                    Select Case exitCodeDettagliPosti
                                        Case EC_DP_CONFERMA
                                            Call ModificaAttributiPosti(listaPostiSelezionati, m)
                                        Case Else
                                            'Do Nothing
                                    End Select
                                End If
                            End If
                        Else
                            Call CalcolaNumeroMaxPostiInFila
                            Call pctGriglia.Refresh
                            Call CaricaFormDettagliFilaPosti(MFC_CREA_POSTI, chkLunghezzaStringaPosto.Value = 1)
                            Select Case exitCodeDettagliPosti
                                Case EC_DP_CONFERMA
                                    Call SetMetodoCreazionePosti(MCP_FILA)
                                    Call CreaPosti
                                Case Else
                                    'Do Nothing
                            End Select
                        End If
                    ElseIf Shift = vbShiftMask Then
                        Call CalcolaNumeroMaxPostiInRettangolo
                        Call pctGriglia.Refresh
                        Call CaricaFormDettagliRettangoloPosti(MFC_CREA_POSTI, chkLunghezzaStringaPosto.Value = 1)
                        Select Case exitCodeDettagliPosti
                            Case EC_DP_CONFERMA
                                Call SetMetodoCreazionePosti(MCP_RETTANGOLO)
                                Call CreaPosti
                            Case Else
                                'Do Nothing
                        End Select
                    '------------------------------------------------------------------------------
                    ElseIf Shift = vbCtrlMask Then
                        'creazione fila curva di posti
                        Call CalcolaNumeroMaxPostiInFilaCurva
                        Call pctGriglia.Refresh
                        Call CaricaFormDettagliFilaPosti(MFC_CREA_POSTI, chkLunghezzaStringaPosto.Value = 1)
                        Select Case exitCodeDettagliPosti
                            Case EC_DP_CONFERMA
                                Call SetMetodoCreazionePosti(MCP_FILA_CURVA)
                                Call CreaPosti
                            Case Else
                                'Do Nothing
                        End Select
                    '------------------------------------------------------------------------------
                    End If
                End If
            Case vbLeftButton
                If spostamentoInCorso Then
                    spostamentoInCorso = False
                    Call RiposizionaPosti(listaPostiSelezionati)
                    '-----------------------------------------------------------------------------------------------------
                ElseIf disegnoLineeGuidaInCorso Then
                    Select Case tipoDisegnoLineaGuida
                        Case TDLG_PUNTO
                            Dim punto As clsPunto
                            
                            Set punto = New clsPunto
                            Call DisegnaPunto(xBD(CLng(x)), yBD(CLng(Y)))
                            punto.x = xBD(CLng(x))
                            punto.Y = yBD(CLng(Y))
                            punto.idPunto = progressivoPunto
                            punto.nome = CStr("P" & progressivoPunto)
                            Call listaPunti.Add(punto, ChiaveId(punto.idPunto))
                            progressivoPunto = progressivoPunto + 1
                        Case TDLG_LINEA
                        Case TDLG_ARCO
                            Dim arco As clsArco
                            Dim centro As clsPunto
                            
                            If listaPunti.count > 0 Then
                                Set centro = listaPunti.Item(ChiaveId(progressivoPunto - 1))
                                Set arco = New clsArco
                                Set arco.centro = centro
                                arco.raggio = Sqr((xBD(CLng(x)) - centro.x) ^ 2 + (yBD(CLng(Y)) - centro.Y) ^ 2)
                                Call DisegnaArco(arco)
                                Call listaArchi.Add(arco)
                            End If
                        Case TDLG_RAGGIO
                            Dim linea As clsLinea
                            
                            If listaPunti.count > 0 Then
                                Set centro = listaPunti.Item(ChiaveId(progressivoPunto - 1))
                                Set linea = New clsLinea
                                Set linea.puntoFinale = New clsPunto
                                Set linea.puntoIniziale = centro
                                linea.puntoFinale.x = xBD(CLng(x))
                                linea.puntoFinale.Y = yBD(CLng(Y))
                                Call DisegnaLinea(linea)
                                Call listaLinee.Add(linea)
                            End If
                    End Select
                    disegnoLineeGuidaInCorso = False
                    tipoDisegnoLineaGuida = TDLG_NON_SPECIFICATO
                    '-----------------------------------------------------------------------------------------------------
                Else
                    If Shift <> vbShiftMask And Shift <> vbCtrlMask And Shift <> vbAltMask Then
                        Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_DESELEZIONA)
                        Set listaPostiSelezionati = Nothing
                        Set listaPostiSelezionati = New Collection
                        Set postoSelezionato = RilevaPostoSelezionato(xBD(xEndRetta), yBD(yEndRetta))
                        If postoSelezionato.idPosto <> idNessunElementoSelezionato Then
                            Call listaPostiSelezionati.Add(postoSelezionato)
                            Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
                            Call SetMetodoSelezionePosti(MSP_SINGOLO)
                        End If
                        Call AggiornaAbilitazioneControlli
                    Else
                        Select Case Shift
                            Case vbShiftMask
                                pctGriglia.Refresh
                                Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_DESELEZIONA)
                                Set listaPostiSelezionati = Nothing
                                Set listaPostiSelezionati = New Collection
                                Call RilevaPostiRettangolo
                                Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
                                Call SetMetodoSelezionePosti(MSP_RETTANGOLO)
                                Call AggiornaAbilitazioneControlli
                            Case vbCtrlMask
                                Set postoSelezionato = RilevaPostoSelezionato(xBD(xEndRetta), yBD(yEndRetta))
                                If postoSelezionato.idPosto <> idNessunElementoSelezionato Then
                                    Call listaPostiSelezionati.Add(postoSelezionato)
                                    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
                                    Call SetMetodoSelezionePosti(MSP_RETTANGOLO)
                                    Call AggiornaAbilitazioneControlli
                                End If
                            Case vbAltMask
                                Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_DESELEZIONA)
                                Set listaPostiSelezionati = Nothing
                                Set listaPostiSelezionati = New Collection
                                Set postoSelezionato = RilevaPostoSelezionato(xBD(xEndRetta), yBD(yEndRetta))
                                If postoSelezionato.idPosto <> idNessunElementoSelezionato Then
                                    Call RicavaPostiAppartenentiAllaStessaFila(postoSelezionato.idPosto)
                                    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_SELEZIONA)
                                    Call SetMetodoSelezionePosti(MSP_FILA)
                                    Call AggiornaAbilitazioneControlli
                                End If
                            Case Else
                            Set listaPostiSelezionati = Nothing
                            Set listaPostiSelezionati = New Collection
                        End Select
                    End If
                End If
            Case Else
                'Do Nothing
        End Select
    End If
End Sub

Private Sub CaricaFormDettagliPosto(moda As ModalitaFormDettagliEnum, Optional p As clsPosto)
    Call frmDettagliPosto.SetModalitaForm(moda)
    Call frmDettagliPosto.SetTipoGriglia(tipoGriglia)
    Call frmDettagliPosto.SetIdAreaSelezionata(idAreaSelezionata)
    Call frmDettagliPosto.SetNomeAreaSelezionata(nomeAreaSelezionata)
    If Not (p Is Nothing) Then
        Select Case moda
            Case MFC_MODIFICA_ATTRIBUTI_POSTO
                Call frmDettagliPosto.SetNomeFila(p.nomeFila)
                Call frmDettagliPosto.SetNomePosto(p.nomePosto)
                Call frmDettagliPosto.SetIdPosto(p.idPosto)
                Call frmDettagliPosto.SetXPosto(p.xPosto)
                Call frmDettagliPosto.SetYPosto(p.yPosto)
                Call frmDettagliPosto.SetIdAreaSelezionata(p.idArea)
                If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                    Call frmDettagliPosto.SetNomeAreaSelezionata(p.nomeArea)
                End If
            Case MFC_MODIFICA_ATTRIBUTI_FILA_POSTI
                Call frmDettagliPosto.SetNomeFila(p.nomeFila)
                Call frmDettagliPosto.SetIdAreaSelezionata(p.idArea)
                If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                    Call frmDettagliPosto.SetNomeAreaSelezionata(p.nomeArea)
                End If
            Case MFC_MODIFICA_ATTRIBUTI_RETTANGOLO_POSTI
                ' Do Nothing
        End Select
    End If
    Call frmDettagliPosto.Init(chkLunghezzaStringaPosto.Value = 1)
End Sub

Private Sub ModificaAttributiPosti(listaPosti As Collection, modalit� As ModalitaFormDettagliEnum)
    Dim newPosto As clsPosto
    Dim p As clsPosto
    Dim postoDuplicato As String
    Dim listaPostiDuplicati As Collection
    Dim listaPostiNonModificabili As Collection
    Dim descrizionePostiNonModificabili As String
    Dim elencoPostiNonModificabili As String
    
    For Each p In listaPosti
        Set listaPostiDuplicati = New Collection
        Set listaPostiNonModificabili = New Collection
        If IsPostoEliminabile(p.idPosto) Then
            Set newPosto = p.ClonaPosto
            Select Case modalit�
                Case MFC_MODIFICA_ATTRIBUTI_POSTO
                    newPosto.nomeFila = Trim(UCase(nomePrimaFila))
                    newPosto.nomePosto = Trim(UCase(nomePrimoPosto))
                    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                        newPosto.idArea = idAreaSelezionata
                        newPosto.nomeArea = nomeAreaSelezionata
                    End If
                Case MFC_MODIFICA_ATTRIBUTI_FILA_POSTI
                    newPosto.nomeFila = Trim(UCase(nomePrimaFila))
                    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                        newPosto.idArea = idAreaSelezionata
                        newPosto.nomeArea = nomeAreaSelezionata
                    End If
                Case MFC_MODIFICA_ATTRIBUTI_RETTANGOLO_POSTI
                    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                        newPosto.idArea = idAreaSelezionata
                        newPosto.nomeArea = nomeAreaSelezionata
                    End If
            End Select
            newPosto.idSequenzaPosti = idNessunElementoSelezionato
            If Not grigliaPosti.IsPostoDuplicato(newPosto, grigliaPosti.listaPostiInGriglia) Then
                Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiInGriglia, p.idPosto)
                Call grigliaPosti.listaPostiInGriglia.Add(newPosto, ChiaveId(newPosto.idPosto))
                Call grigliaPosti.EliminaElementoDaLista(grigliaPosti.listaPostiModificati, p.idPosto)
                Call grigliaPosti.listaPostiModificati.Add(newPosto, ChiaveId(newPosto.idPosto))
                Call AggiornaMatricePostiBD(newPosto.idPosto, newPosto.idArea, _
                    newPosto.xPosto, newPosto.yPosto, newPosto.nomeFila, newPosto.nomePosto, _
                    newPosto.nomeArea, newPosto.indiceDiPreferibilit�Area)
            Else
                postoDuplicato = "Fila:" & newPosto.nomeFila & "; " & "Posto:" & newPosto.nomePosto
                Call listaPostiDuplicati.Add(postoDuplicato)
            End If
        Else
            descrizionePostiNonModificabili = p.nomeArea & " fila " & p.nomeFila & "; posto " & p.nomePosto
            Call listaPostiNonModificabili.Add(descrizionePostiNonModificabili)
        End If
    Next p
    If listaPostiDuplicati.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreDuplicazionePosto", nomeAreaSelezionata, ArgomentoMessaggio(listaPostiDuplicati))
    End If
    If listaPostiNonModificabili.count > 0 Then
        elencoPostiNonModificabili = ArgomentoMessaggio(listaPostiNonModificabili)
        Call frmMessaggio.Visualizza("NotificaPostiNonModificabili", elencoPostiNonModificabili)
    End If
End Sub

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Private Sub DisegnaPunto(XCoord As Integer, YCoord As Integer)
    Dim xC As Long
    Dim yC As Long
    Dim x0 As Long
    Dim y0 As Long
    Dim x1 As Long
    Dim y1 As Long
    Dim deltaX As Double
    Dim deltaY As Double

    deltaX = dimOrizzontalePosto
    deltaY = dimVerticalePosto
        
    xC = CLng((XCoord - x0Schermo) * passo * zoom)
    yC = CLng((YCoord - y0Schermo) * passo * zoom)
    x0 = CLng((XCoord - x0Schermo - deltaX / 2) * passo * zoom)
    y0 = CLng((YCoord - y0Schermo - deltaY / 2) * passo * zoom)
    x1 = CLng((XCoord - x0Schermo + deltaX / 2) * passo * zoom)
    y1 = CLng((YCoord - y0Schermo + deltaY / 2) * passo * zoom)

    x0 = IIf(x0 < 0, 0, x0): x0 = IIf(x0 > larghezzaGriglia, larghezzaGriglia, x0)
    y0 = IIf(y0 < 0, 0, y0): y0 = IIf(y0 > altezzaGriglia, altezzaGriglia, y0)
    x1 = IIf(x1 < 0, 0, x1): x1 = IIf(x1 > larghezzaGriglia, larghezzaGriglia, x1)
    y1 = IIf(y1 < 0, 0, y1): y1 = IIf(y1 > altezzaGriglia, altezzaGriglia, y1)

    pctGriglia.Line (x0, yC)-(x1, yC), QBColor(12)
    pctGriglia.Line (xC, y0)-(xC, y1), QBColor(12)
End Sub

Private Sub DisegnaArco(arco As clsArco)
    Dim xC As Long
    Dim yC As Long
    Dim raggio As Long
        
    xC = CLng((arco.centro.x - x0Schermo) * passo * zoom)
    yC = CLng((arco.centro.Y - y0Schermo) * passo * zoom)

'    xC = IIf(xC < 0, 0, xC): xC = IIf(xC > larghezzaGriglia, larghezzaGriglia, xC)
'    yC = IIf(yC < 0, 0, yC): yC = IIf(yC > altezzaGriglia, altezzaGriglia, yC)
    
    raggio = arco.raggio * passo * zoom

    pctGriglia.Circle (xC, yC), raggio, QBColor(14)
End Sub

Private Sub DisegnaLinea(linea As clsLinea)
    Dim xC As Long
    Dim yC As Long
    Dim x0 As Long
    Dim y0 As Long
        
    xC = CLng((linea.puntoIniziale.x - x0Schermo) * passo * zoom)
    yC = CLng((linea.puntoIniziale.Y - y0Schermo) * passo * zoom)
    x0 = CLng((linea.puntoFinale.x - x0Schermo) * passo * zoom)
    y0 = CLng((linea.puntoFinale.Y - y0Schermo) * passo * zoom)

'    x0 = IIf(x0 < 0, 0, x0): x0 = IIf(x0 > larghezzaGriglia, larghezzaGriglia, x0)
'    y0 = IIf(y0 < 0, 0, y0): y0 = IIf(y0 > altezzaGriglia, altezzaGriglia, y0)
'    xC = IIf(xC < 0, 0, xC): xC = IIf(xC > larghezzaGriglia, larghezzaGriglia, xC)
'    yC = IIf(yC < 0, 0, yC): yC = IIf(yC > altezzaGriglia, altezzaGriglia, yC)

    pctGriglia.Line (xC, yC)-(x0, y0), QBColor(14)
End Sub



