VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSceltaSpettacolo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Spettacolo"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbStagione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7920
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   900
      Width           =   3795
   End
   Begin VB.ComboBox cmbVenue 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4020
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   900
      Width           =   3795
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   900
      Width           =   3795
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   7
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   9
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   2700
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcSpettacolo 
      Height          =   375
      Left            =   10080
      Top             =   60
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrSpettacoliConfigurati 
      Bindings        =   "frmSceltaSpettacolo.frx":0000
      Height          =   6075
      Left            =   120
      TabIndex        =   3
      Top             =   1320
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10716
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblStagione 
      Caption         =   "Stagione"
      Height          =   255
      Left            =   7920
      TabIndex        =   12
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblVenue 
      Caption         =   "Venue"
      Height          =   255
      Left            =   4020
      TabIndex        =   11
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazione"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione dello Spettacolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   6375
   End
End
Attribute VB_Name = "frmSceltaSpettacolo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idVenueSelezionato As Long
Private idStagioneSelezionata As Long
Private listaAppoggioGeneriSIAE As Collection
Private nomeTabellaTemporanea As String

Private internalEvent As Boolean
Private exitCodeFormIniziale As ExitCodeEnum

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaSpettacolo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If Not internalEvent Then
        Call cmbOrganizzazione_Update
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbOrganizzazione_Update()
    Dim sql As String
    
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
'    sql = " SELECT DISTINCT V.IDVENUE ID, NOME"
'    sql = sql & " FROM VENUE V, VENUE_PIANTA VP, ORGANIZZAZIONE_PIANTA OP"
'    sql = sql & " WHERE V.IDVENUE = VP.IDVENUE"
'    sql = sql & " AND VP.IDPIANTA = OP.IDPIANTA"
'    If idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
'        sql = sql & " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'    End If
'    sql = sql & " ORDER BY NOME"
    sql = "SELECT ID, NOME" & _
            " FROM" & _
            " (" & _
            " SELECT V.IDVENUE ID, NOME" & _
            " FROM VENUE V, VENUE_PIANTA VP, ORGANIZZAZIONE_PIANTA OP" & _
            " WHERE V.IDVENUE = VP.IDVENUE" & _
            " AND VP.IDPIANTA = OP.IDPIANTA" & _
            " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " UNION" & _
            " SELECT V.IDVENUE ID, V.NOME" & _
            " FROM VENUE V, VENUE_PIANTA VP, PRODOTTO P" & _
            " WHERE V.IDVENUE = VP.IDVENUE" & _
            " AND VP.IDPIANTA = P.IDPIANTA" & _
            " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " )" & _
            " ORDER BY NOME"
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbStagione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If Not internalEvent Then
        Call cmbStagione_Update
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbVenue_Update()
    Dim sql As String
    
    idVenueSelezionato = cmbVenue.ItemData(cmbVenue.ListIndex)
    sql = " SELECT DISTINCT S.IDSTAGIONE ID, S.CODICE ||' - '|| S.NOME LABEL"
    sql = sql & " FROM STAGIONE S, PRODOTTO P, VENUE_PIANTA VP"
    sql = sql & " WHERE S.IDSTAGIONE = P.IDSTAGIONE"
    sql = sql & " AND P.IDPIANTA = VP.IDPIANTA"
    If idVenueSelezionato <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND VP.IDVENUE = " & idVenueSelezionato
    End If
    If idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
        sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    End If
    sql = sql & " ORDER BY LABEL"
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo(cmbStagione, sql, "LABEL")
'    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbVenue_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If Not internalEvent Then
        Call cmbVenue_Update
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmbStagione_Update()
    idStagioneSelezionata = cmbStagione.ItemData(cmbStagione.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ModificaSpettacolo
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call NuovoSpettacolo
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub NuovoSpettacolo()
    Call frmInizialeSpettacolo.SetModalit�Form(A_NUOVO)
    Call frmInizialeSpettacolo.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaSpettacolo()
    Call frmInizialeSpettacolo.SetModalit�Form(A_MODIFICA)
    Call frmInizialeSpettacolo.SetIdSpettacoloSelezionato(idRecordSelezionato)
    Call frmInizialeSpettacolo.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaSpettacolo()
    Dim spettacoloEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String

    Call frmInizialeSpettacolo.SetModalit�Form(A_ELIMINA)
    Set listaTabelleCorrelate = New Collection
    
    spettacoloEliminabile = True
    If Not IsRecordEliminabile("IDSPETTACOLO", "RAPPRESENTAZIONE", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("RAPPRESENTAZIONE")
        spettacoloEliminabile = False
    End If
    
    If spettacoloEliminabile Then
        Call frmInizialeSpettacolo.SetIdSpettacoloSelezionato(idRecordSelezionato)
        Call frmInizialeSpettacolo.Init
        If exitCodeFormIniziale = EC_CONFERMA Then
            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
            Call CaricaValoriGriglia
        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", _
                                     "Lo Spettacolo selezionato", tabelleCorrelate)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggioGeneriSIAE
    Unload Me
End Sub

Private Sub AggiornaAbilitazioneControlli()

    dgrSpettacoliConfigurati.Columns(0).Visible = False
    dgrSpettacoliConfigurati.Caption = "SPETTACOLI CONFIGURATI"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
End Sub

Private Sub dgrSpettacoliConfigurati_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrSpettacoliConfigurati, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcSpettacolo.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Public Sub Init()
    Dim sql As String
'    Dim sqlS As String
'    Dim sqlV As String

    sql = "SELECT IDORGANIZZAZIONE ID, NOME"
    sql = sql & " FROM ORGANIZZAZIONE"
    sql = sql & " ORDER BY NOME"
'    sqlS = " SELECT IDSTAGIONE ID, CODICE ||' - '|| NOME LABEL"
'    sqlS = sqlS & " FROM STAGIONE"
'    sqlS = sqlS & " ORDER BY NOME"
'    sqlV = " SELECT IDVENUE ID, NOME"
'    sqlV = sqlV & " FROM VENUE"
'    sqlV = sqlV & " ORDER BY NOME"
    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idStagioneSelezionata = idNessunElementoSelezionato
    idVenueSelezionato = idNessunElementoSelezionato
'    Call CreaTabellaAppoggioGeneriSIAE
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
'    Call CaricaValoriCombo(cmbVenue, sqlV, "NOME")
'    Call CaricaValoriCombo(cmbStagione, sqlS, "LABEL")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcSpettacolo_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call dgrSpettacoliConfigurati_Init
End Sub

Private Sub adcSpettacolo_Init_old()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Call CreaTabellaAppoggioGeneriSIAE
    Call PopolaTabellaAppoggioGeneriSIAE
    
    Set d = adcSpettacolo
    Select Case idOrganizzazioneSelezionata
        Case idTuttiGliElementiSelezionati
            sql = " SELECT DISTINCT SP.IDSPETTACOLO ID, SP.NOME ""Nome"","
            sql = sql & " SP.DESCRIZIONE ""Descrizione"", ST.NOME ""Stagione"","
            sql = sql & " TMP.NOME ""Generi SIAE"","
            sql = sql & " COUNT(DISTINCT R.IDRAPPRESENTAZIONE) ""Rappresentazioni"","
            sql = sql & " MIN(R.DATAORAINIZIO) ""Data /ora inizio"""
            sql = sql & " FROM SPETTACOLO SP, RAPPRESENTAZIONE R, STAGIONE ST,"
            sql = sql & nomeTabellaTemporanea & " TMP"
            sql = sql & " WHERE SP.IDSTAGIONE = ST.IDSTAGIONE"
            sql = sql & " AND R.IDSPETTACOLO(+) = SP.IDSPETTACOLO"
            sql = sql & " AND TMP.IDSPETTACOLO = SP.IDSPETTACOLO"
            sql = sql & " GROUP BY SP.IDSPETTACOLO, SP.NOME, SP.DESCRIZIONE, ST.NOME, TMP.NOME"
            sql = sql & " ORDER BY SP.NOME"
        Case Else
            sql = " SELECT DISTINCT SP.IDSPETTACOLO ID, SP.NOME ""Nome"","
            sql = sql & " SP.DESCRIZIONE ""Descrizione"", ST.NOME ""Stagione"","
            sql = sql & " TMP.NOME ""Generi SIAE"","
            sql = sql & " COUNT(DISTINCT R.IDRAPPRESENTAZIONE) ""Rappresentazioni"","
            sql = sql & " MIN(R.DATAORAINIZIO) ""Data /ora inizio"""
            sql = sql & " FROM SPETTACOLO SP, RAPPRESENTAZIONE R, VENUE_PIANTA VP,"
            sql = sql & " ORGANIZZAZIONE_PIANTA OP, STAGIONE ST,"
            sql = sql & nomeTabellaTemporanea & " TMP"
            sql = sql & " WHERE R.IDSPETTACOLO = SP.IDSPETTACOLO"
            sql = sql & " AND R.IDVENUE = VP.IDVENUE"
            sql = sql & " AND VP.IDPIANTA = OP.IDPIANTA"
            sql = sql & " AND SP.IDSTAGIONE = ST.IDSTAGIONE"
            sql = sql & " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
            sql = sql & " AND R.IDSPETTACOLO(+) = SP.IDSPETTACOLO"
            sql = sql & " AND TMP.IDSPETTACOLO = SP.IDSPETTACOLO"
            sql = sql & " GROUP BY SP.IDSPETTACOLO, SP.NOME, SP.DESCRIZIONE, ST.NOME, TMP.NOME"
            sql = sql & " ORDER BY SP.NOME"
    End Select
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrSpettacoliConfigurati_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrSpettacoliConfigurati
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 6
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcSpettacolo.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Public Sub SetExitCodeFormIniziale(exC As ExitCodeEnum)
    exitCodeFormIniziale = exC
End Sub

Private Sub CreaTabellaAppoggioGeneriSIAE()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_GENSIAE_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDSPETTACOLO NUMBER(10), NOME VARCHAR2(2000))"
    
    Call EliminaTabellaAppoggioGeneriSIAE
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioGeneriSIAE()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
     
    ChiudiConnessioneBD

End Sub

Private Sub PopolaTabellaAppoggioGeneriSIAE()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idSpettacolo As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Integer
    Dim descrizioneCodiceSIAE As String
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioGeneriSIAE = New Collection
    
''    sql = "SELECT DISTINCT LSTS.IDLAYOUTSUPPORTO ID"
''    sql = sql & " FROM LAYOUTSUPPORTO LS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS"
''    sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO"
'    If idOrganizzazioneSelezionata = idTuttiGliElementiSelezionati Then
'        sql = " SELECT IDSPETTACOLO ID FROM SPETTACOLO"
'    Else
'        sql = " SELECT DISTINCT SP.IDSPETTACOLO ID"
'        sql = sql & " FROM SPETTACOLO SP, RAPPRESENTAZIONE R, VENUE_PIANTA VP,"
'        sql = sql & " ORGANIZZAZIONE_PIANTA OP"
'        sql = sql & " WHERE R.IDSPETTACOLO = SP.IDSPETTACOLO"
'        sql = sql & " AND R.IDVENUE = VP.IDVENUE"
'        sql = sql & " AND VP.IDPIANTA = OP.IDPIANTA"
'        sql = sql & " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql & " AND R.IDSPETTACOLO(+) = SP.IDSPETTACOLO"
        sql = " SELECT DISTINCT SP.IDSPETTACOLO ID"
        sql = sql & " FROM SPETTACOLO SP, RAPPRESENTAZIONE R, VENUE_PIANTA VP,"
        sql = sql & " ORGANIZZAZIONE_PIANTA OP, STAGIONE ST"
        sql = sql & " WHERE R.IDSPETTACOLO = SP.IDSPETTACOLO"
        sql = sql & " AND R.IDVENUE = VP.IDVENUE"
        sql = sql & " AND VP.IDPIANTA = OP.IDPIANTA"
        sql = sql & " AND R.IDSPETTACOLO(+) = SP.IDSPETTACOLO"
        sql = sql & " AND ST.IDSTAGIONE = SP.IDSTAGIONE"
        If idOrganizzazioneSelezionata <> idNessunElementoSelezionato And _
            idOrganizzazioneSelezionata <> idTuttiGliElementiSelezionati Then
            sql = sql & " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
        End If
        If idVenueSelezionato <> idNessunElementoSelezionato And _
            idVenueSelezionato <> idTuttiGliElementiSelezionati Then
            sql = sql & " AND VP.IDVENUE = " & idVenueSelezionato
        End If
        If idStagioneSelezionata <> idNessunElementoSelezionato And _
            idStagioneSelezionata <> idTuttiGliElementiSelezionati Then
            sql = sql & " AND ST.IDSTAGIONE = " & idStagioneSelezionata
        End If
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("ID").Value
            Call listaAppoggioGeneriSIAE.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioGeneriSIAE
        campoNome = ""
'        sql = " SELECT LS.IDLAYOUTSUPPORTO, TS.IDTIPOSUPPORTO, TS.CODICE CODICE"
'        sql = sql & " FROM LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, LAYOUTSUPPORTO_TIPOSUPPORTO LSTS"
'        sql = sql & " WHERE TS.IDTIPOSUPPORTO = LSTS.IDTIPOSUPPORTO"
'        sql = sql & " AND LS.IDLAYOUTSUPPORTO = LSTS.IDLAYOUTSUPPORTO"
'        sql = sql & " AND LS.IDLAYOUTSUPPORTO = " & recordTemporaneo.idElementoLista
'        sql = sql & " ORDER BY TS.NOME"
        sql = " SELECT SG.IDSPETTACOLO, G.CODICE, G.NOME, "
        sql = sql & " G.DESCRIZIONE, SG.INCIDENZA"
        sql = sql & " FROM SPETTACOLO_GENERESIAE SG, GENERESIAE G"
        sql = sql & " WHERE SG.IDGENERESIAE = G.IDGENERESIAE "
        sql = sql & " AND SG.IDSPETTACOLO = " & recordTemporaneo.idElementoLista
        sql = sql & " ORDER BY G.CODICE"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                descrizioneCodiceSIAE = rec("CODICE") & " - " & rec("NOME") & " (" & rec("INCIDENZA") & "%)"
                campoNome = IIf(campoNome = "", descrizioneCodiceSIAE, campoNome & "; " & descrizioneCodiceSIAE)
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qui sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati s�
    For Each recordTemporaneo In listaAppoggioGeneriSIAE
        idSpettacolo = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & " (IDSPETTACOLO, NOME)"
        sql = sql & " VALUES (" & idSpettacolo & ", "
        sql = sql & SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub adcSpettacolo_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Call CreaTabellaAppoggioGeneriSIAE
    Call PopolaTabellaAppoggioGeneriSIAE
    
    Set d = adcSpettacolo
    Select Case idOrganizzazioneSelezionata
        Case idTuttiGliElementiSelezionati
            sql = " SELECT DISTINCT SP.IDSPETTACOLO ID, SP.NOME ""Nome"","
            sql = sql & " SP.DESCRIZIONE ""Descrizione"", ST.NOME ""Stagione"","
            sql = sql & " TMP.NOME ""Generi SIAE"","
            sql = sql & " COUNT(DISTINCT R.IDRAPPRESENTAZIONE) ""Rappresentazioni"","
            sql = sql & " MIN(R.DATAORAINIZIO) ""Data /ora inizio"""
            sql = sql & " FROM SPETTACOLO SP, RAPPRESENTAZIONE R, STAGIONE ST,"
            sql = sql & nomeTabellaTemporanea & " TMP"
            sql = sql & " WHERE R.IDSPETTACOLO = SP.IDSPETTACOLO"
            If idVenueSelezionato <> idNessunElementoSelezionato And _
                idVenueSelezionato <> idTuttiGliElementiSelezionati Then
                sql = sql & " AND R.IDVENUE = " & idVenueSelezionato
            End If
            sql = sql & " AND SP.IDSTAGIONE = ST.IDSTAGIONE"
            If idStagioneSelezionata <> idNessunElementoSelezionato And _
                idStagioneSelezionata <> idTuttiGliElementiSelezionati Then
                sql = sql & " AND ST.IDSTAGIONE = " & idStagioneSelezionata
            End If
            sql = sql & " AND R.IDSPETTACOLO(+) = SP.IDSPETTACOLO"
            sql = sql & " AND TMP.IDSPETTACOLO = SP.IDSPETTACOLO"
            sql = sql & " GROUP BY SP.IDSPETTACOLO, SP.NOME, SP.DESCRIZIONE, ST.NOME, TMP.NOME"
            sql = sql & " ORDER BY SP.NOME"
        Case Else
            sql = " SELECT DISTINCT SP.IDSPETTACOLO ID, SP.NOME ""Nome"","
            sql = sql & " SP.DESCRIZIONE ""Descrizione"", ST.NOME ""Stagione"","
            sql = sql & " TMP.NOME ""Generi SIAE"","
            sql = sql & " COUNT(DISTINCT R.IDRAPPRESENTAZIONE) ""Rappresentazioni"","
            sql = sql & " MIN(R.DATAORAINIZIO) ""Data /ora inizio"""
            sql = sql & " FROM SPETTACOLO SP, RAPPRESENTAZIONE R, VENUE_PIANTA VP,"
            sql = sql & " ORGANIZZAZIONE_PIANTA OP, STAGIONE ST,"
            sql = sql & nomeTabellaTemporanea & " TMP"
            sql = sql & " WHERE R.IDSPETTACOLO = SP.IDSPETTACOLO"
            sql = sql & " AND R.IDVENUE = VP.IDVENUE"
            If idVenueSelezionato <> idNessunElementoSelezionato And _
                idVenueSelezionato <> idTuttiGliElementiSelezionati Then
                sql = sql & " AND VP.IDVENUE = " & idVenueSelezionato
            End If
            sql = sql & " AND VP.IDPIANTA = OP.IDPIANTA"
            sql = sql & " AND SP.IDSTAGIONE = ST.IDSTAGIONE"
            If idStagioneSelezionata <> idNessunElementoSelezionato And _
                idStagioneSelezionata <> idTuttiGliElementiSelezionati Then
                sql = sql & " AND ST.IDSTAGIONE = " & idStagioneSelezionata
            End If
            sql = sql & " AND OP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
            sql = sql & " AND R.IDSPETTACOLO(+) = SP.IDSPETTACOLO"
            sql = sql & " AND TMP.IDSPETTACOLO = SP.IDSPETTACOLO"
            sql = sql & " GROUP BY SP.IDSPETTACOLO, SP.NOME, SP.DESCRIZIONE, ST.NOME, TMP.NOME"
            sql = sql & " ORDER BY SP.NOME"
    End Select
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrSpettacoliConfigurati.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

