VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPosto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idPosto As Long
Public nomeFila As String
Public nomePosto As String
Public letteraFinalePosto As String
Public xPosto As Long
Public yPosto As Long
Public idArea As Long
Public nomeArea As String
Public idSequenzaPosti As Long
Public indiceDiPreferibilitÓArea As Long

Public Function ClonaPosto() As clsPosto
    Dim p As clsPosto
    
    Set p = New clsPosto
    p.idPosto = idPosto
    p.nomeFila = nomeFila
    p.nomePosto = nomePosto
    p.letteraFinalePosto = letteraFinalePosto
    p.xPosto = xPosto
    p.yPosto = yPosto
    p.idArea = idArea
    p.nomeArea = nomeArea
    p.idSequenzaPosti = idSequenzaPosti
    p.indiceDiPreferibilitÓArea = indiceDiPreferibilitÓArea
    Set ClonaPosto = p
End Function

