VERSION 5.00
Begin VB.Form frmClonazionePiantaConclusione 
   Caption         =   "Clonazione pianta: conclusione"
   ClientHeight    =   4305
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12705
   LinkTopic       =   "Form1"
   ScaleHeight     =   4305
   ScaleWidth      =   12705
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   495
      Left            =   10800
      TabIndex        =   2
      Top             =   3600
      Width           =   1695
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   495
      Left            =   5520
      TabIndex        =   1
      Top             =   2640
      Width           =   1695
   End
   Begin VB.Label lblDomanda 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   12375
   End
   Begin VB.Label lblAvviso 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   12375
   End
End
Attribute VB_Name = "frmClonazionePiantaConclusione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private conferma As ValoreBooleanoEnum

Public Sub Init(conf As ValoreBooleanoEnum)
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Integer
    
    conferma = conf
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(*) CONT FROM CC_PIANTAINCLONAZIONE"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            cont = rec("CONT").Value
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    lblAvviso.Caption = ""
    lblDomanda.Caption = ""
    cmdEsci.Enabled = True
    If (cont = 0) Then
        lblAvviso.Caption = "Non ci sono clonazioni in corso"
        lblDomanda.Caption = ""
        cmdConferma.Enabled = False
    Else
        sql = "SELECT P.NOME NOME_VECCHIA, CLP.NOME NOME_CLONATA, DATAORAINIZIOCLONAZIONE, DATAORAFINECLONAZIONE" & _
            " FROM CC_PIANTAINCLONAZIONE CL, PIANTA P, CL_PIANTA CLP" & _
            " WHERE CL.IDVECCHIAPIANTA = P.IDPIANTA" & _
            " AND CL.IDPIANTACLONATA = CLP.IDPIANTA(+)"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.EOF And rec.BOF) Then
            rec.MoveFirst
            While Not rec.EOF
                If IsNull(rec("DATAORAFINECLONAZIONE")) Then
                    If (conferma = VB_VERO) Then
                        lblAvviso.Caption = "La clonazione della pianta " & rec("NOME_VECCHIA") & " nella pianta " & rec("NOME_CLONATA") & " iniziata alle ore " & rec("DATAORAINIZIOCLONAZIONE") & " e' ancora in corso!"
                        lblDomanda.Caption = "Impossibile confermare."
                        cmdConferma.Enabled = False
                    Else
                        lblAvviso.Caption = "La clonazione della pianta " & rec("NOME_VECCHIA") & " nella pianta " & rec("NOME_CLONATA") & " iniziata alle ore " & rec("DATAORAINIZIOCLONAZIONE") & " e' ancora in corso! La cancellazione potrebbe portare a dati incongruenti!"
                        lblDomanda.Caption = "Sei sicuro di voler cancellare la clonazione?"
                        cmdConferma.Enabled = True
                    End If
                Else
                    If (conferma = VB_VERO) Then
                        lblAvviso.Caption = "La clonazione della pianta " & rec("NOME_VECCHIA") & " nella pianta " & rec("NOME_CLONATA") & " iniziata alle ore " & rec("DATAORAINIZIOCLONAZIONE") & " e' terminata alle " & rec("DATAORAFINECLONAZIONE") & "."
                        lblDomanda.Caption = "Sei sicuro di confermare la clonazione?"
                        cmdConferma.Enabled = True
                    Else
                        lblAvviso.Caption = "La clonazione della pianta " & rec("NOME_VECCHIA") & " nella pianta " & rec("NOME_CLONATA") & " iniziata alle ore " & rec("DATAORAINIZIOCLONAZIONE") & " e' terminata alle " & rec("DATAORAFINECLONAZIONE") & "."
                        lblDomanda.Caption = "Sei sicuro di voler cancellare la clonazione?"
                        cmdConferma.Enabled = True
                    End If
                End If
                rec.MoveNext
            Wend
        End If
        rec.Close
    End If
    
    Call ChiudiConnessioneBD_ORA
    
    Me.Show
End Sub

Private Sub cmdConferma_Click()
    If (conferma = VB_VERO) Then
        ConfermaClonazione
    Else
        CancellaClonazione
    End If
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub ConfermaClonazione()
    Dim sql As String
    Dim righeAggiornate As Long
    
On Error GoTo error_confermaClonazione

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    sql = "INSERT INTO PIANTA (IDPIANTA, NOME, DESCRIZIONE, NUMEROVERSIONE, IDTIPOSTATOPIANTA, CAPIENZAILLIMITATA)" & _
            " SELECT IDPIANTA, NOME, DESCRIZIONE, NUMEROVERSIONE, IDTIPOSTATOPIANTA, CAPIENZAILLIMITATA FROM CL_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO ORGANIZZAZIONE_PIANTA (IDORGANIZZAZIONE, IDPIANTA, CODICEPIANTA)" & _
            " SELECT IDORGANIZZAZIONE, IDPIANTA, CODICEPIANTA FROM CL_ORGANIZZAZIONE_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO VENUE_PIANTA (IDVENUE, IDPIANTA) SELECT IDVENUE, IDPIANTA FROM CL_VENUE_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO AREA (IDORDINEDIPOSTOSIAE, IDAREA, IDAREA_PADRE, NOME, NUMEROVERSIONE, DESCRIZIONE, DESCRIZIONESTAMPAINGRESSO, DESCRIZIONEALTERNATIVA, CODICE, INDICEDIPREFERIBILITA, DESCRIZIONEPOS, CAPIENZA, NUMERATAAPOSTOUNICO, IDPIANTA, IDTIPOAREA, SOGLIA1, SOGLIA2, VIRTUALE)" & _
            " SELECT IDORDINEDIPOSTOSIAE, IDAREA, IDAREA_PADRE, NOME, NUMEROVERSIONE, DESCRIZIONE, DESCRIZIONESTAMPAINGRESSO, DESCRIZIONEALTERNATIVA, CODICE, INDICEDIPREFERIBILITA, DESCRIZIONEPOS, CAPIENZA, NUMERATAAPOSTOUNICO, IDPIANTA, IDTIPOAREA, SOGLIA1, SOGLIA2, VIRTUALE FROM CL_AREA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO GRIGLIAPOSTI (IDGRIGLIAPOSTI, MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE, DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO, IDAREA, IDPIANTA)" & _
            " SELECT IDGRIGLIAPOSTI, MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE, DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO, IDAREA, IDPIANTA FROM CL_GRIGLIAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO CLASSESUPERAREA (IDCLASSESUPERAREA, NOME, DESCRIZIONE, IDPIANTA, CLASSEPRINCIPALE)" & _
            " SELECT IDCLASSESUPERAREA, NOME, DESCRIZIONE, IDPIANTA, CLASSEPRINCIPALE FROM CL_CLASSESUPERAREA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO FASCIAPOSTI (IDFASCIAPOSTI, IDAREA, INDICEDIPREFERIBILITA)" & _
            " SELECT IDFASCIAPOSTI, IDAREA, INDICEDIPREFERIBILITA FROM CL_FASCIAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO SEQUENZAPOSTI (IDSEQUENZAPOSTI, IDFASCIAPOSTI, ORDINEINFASCIA)" & _
            " SELECT IDSEQUENZAPOSTI, IDFASCIAPOSTI, ORDINEINFASCIA FROM CL_SEQUENZAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO POSTO (IDPOSTO, NOMEFILA, NOMEPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE, IDAREA, IDSEQUENZAPOSTI, ORDINEINSEQUENZA)" & _
            " SELECT IDPOSTO, NOMEFILA, NOMEPOSTO, COORDINATAORIZZONTALE, COORDINATAVERTICALE, IDAREA, IDSEQUENZAPOSTI, ORDINEINSEQUENZA FROM CL_POSTO"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "INSERT INTO ETICHETTAPOSTO (IDPOSTO, ETICHETTA)" & _
            " SELECT IDPOSTO, ETICHETTA FROM CL_ETICHETTAPOSTO"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    
    sql = "DELETE FROM CL_ETICHETTAPOSTO"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_POSTO"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_SEQUENZAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_FASCIAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_CLASSESUPERAREA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_GRIGLIAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_AREA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_VENUE_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_ORGANIZZAZIONE_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CC_PIANTAINCLONAZIONE"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    
    MsgBox ("Conferma completata!")
    Unload Me
    Exit Sub
    
error_confermaClonazione:
    ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    MsgBox ("Conferma NON riuscita!")
    
    Unload Me
    Exit Sub

End Sub

Private Sub CancellaClonazione()
    Dim sql As String
    Dim righeAggiornate As Long
    
On Error GoTo error_cancellaClonazione

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    sql = "DELETE FROM CL_ETICHETTAPOSTO"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_POSTO"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_SEQUENZAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_FASCIAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_CLASSESUPERAREA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_GRIGLIAPOSTI"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_AREA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_VENUE_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_ORGANIZZAZIONE_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CC_PIANTAINCLONAZIONE"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM CL_PIANTA"
    righeAggiornate = ORADB.ExecuteSQL(sql)
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    
    MsgBox ("Cancellazione completata!")
    Unload Me
    Exit Sub
    
error_cancellaClonazione:
    ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    MsgBox ("Cancellazione NON riuscita!")
    
    Unload Me
    Exit Sub

End Sub

