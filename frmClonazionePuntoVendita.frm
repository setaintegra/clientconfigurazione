VERSION 5.00
Begin VB.Form frmClonazionePuntoVendita 
   Caption         =   "Clonazione punto vendita"
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10140
   LinkTopic       =   "Form1"
   ScaleHeight     =   6105
   ScaleWidth      =   10140
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   34
      Top             =   5040
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   8820
      TabIndex        =   15
      Top             =   5520
      Width           =   1155
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   7140
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   300
      Width           =   2835
   End
   Begin VB.Frame Frame1 
      Caption         =   "Caratteristiche"
      Height          =   4095
      Left            =   120
      TabIndex        =   16
      Top             =   780
      Width           =   9855
      Begin VB.TextBox txtTipoPuntoVendita 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7020
         MaxLength       =   30
         TabIndex        =   1
         Top             =   360
         Width           =   2355
      End
      Begin VB.TextBox txtEMail 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5940
         MaxLength       =   32
         TabIndex        =   12
         Top             =   3480
         Width           =   3495
      End
      Begin VB.TextBox txtResponsabile 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   11
         Top             =   3480
         Width           =   3255
      End
      Begin VB.TextBox txtFax 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4140
         MaxLength       =   16
         TabIndex        =   10
         Top             =   3060
         Width           =   1815
      End
      Begin VB.TextBox txtTelefono 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   16
         TabIndex        =   9
         Top             =   3060
         Width           =   1815
      End
      Begin VB.TextBox txtCap 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3480
         MaxLength       =   5
         TabIndex        =   6
         Top             =   2640
         Width           =   675
      End
      Begin VB.TextBox txtCitta 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4920
         MaxLength       =   30
         TabIndex        =   7
         Top             =   2640
         Width           =   3255
      End
      Begin VB.TextBox txtCivico 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   8
         TabIndex        =   5
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox txtIndirizzo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   1920
         Width           =   7635
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   780
         Width           =   7635
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   0
         Top             =   360
         Width           =   3255
      End
      Begin VB.TextBox txtProvincia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9000
         MaxLength       =   2
         TabIndex        =   8
         Top             =   2640
         Width           =   375
      End
      Begin VB.TextBox txtCodiceSAP 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   6
         TabIndex        =   3
         Top             =   1500
         Width           =   855
      End
      Begin VB.Label lblEMail 
         Alignment       =   1  'Right Justify
         Caption         =   "E-mail"
         Height          =   255
         Left            =   5280
         TabIndex        =   30
         Top             =   3540
         Width           =   495
      End
      Begin VB.Label lblResponsabile 
         Alignment       =   1  'Right Justify
         Caption         =   "responsabile"
         Height          =   255
         Left            =   540
         TabIndex        =   29
         Top             =   3540
         Width           =   1035
      End
      Begin VB.Label lblFax 
         Alignment       =   1  'Right Justify
         Caption         =   "fax"
         Height          =   255
         Left            =   3660
         TabIndex        =   28
         Top             =   3120
         Width           =   315
      End
      Begin VB.Label lblTelefono 
         Alignment       =   1  'Right Justify
         Caption         =   "telefono"
         Height          =   255
         Left            =   540
         TabIndex        =   27
         Top             =   3120
         Width           =   1035
      End
      Begin VB.Label lblCap 
         Alignment       =   1  'Right Justify
         Caption         =   "CAP"
         Height          =   255
         Left            =   2880
         TabIndex        =   26
         Top             =   2700
         Width           =   435
      End
      Begin VB.Label lblCitta 
         Alignment       =   1  'Right Justify
         Caption         =   "citt�"
         Height          =   255
         Left            =   4260
         TabIndex        =   25
         Top             =   2700
         Width           =   495
      End
      Begin VB.Label lblCivico 
         Alignment       =   1  'Right Justify
         Caption         =   "civico"
         Height          =   255
         Left            =   540
         TabIndex        =   24
         Top             =   2700
         Width           =   1035
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   480
         TabIndex        =   23
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblIndirizzo 
         Alignment       =   1  'Right Justify
         Caption         =   "indirizzo"
         Height          =   255
         Left            =   540
         TabIndex        =   22
         Top             =   1920
         Width           =   1035
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   540
         TabIndex        =   21
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblProvincia 
         Alignment       =   1  'Right Justify
         Caption         =   "prov."
         Height          =   255
         Left            =   8460
         TabIndex        =   20
         Top             =   2700
         Width           =   375
      End
      Begin VB.Label lblCodiceSAP 
         Alignment       =   1  'Right Justify
         Caption         =   "codice SAP"
         Height          =   255
         Left            =   540
         TabIndex        =   19
         Top             =   1560
         Width           =   1035
      End
      Begin VB.Label lblSpiegazioneCodiceSAP 
         Caption         =   "(lasciare il campo vuoto per escludere il punto vendita dalla contabilizzazione)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2760
         TabIndex        =   18
         Top             =   1560
         Width           =   6855
      End
      Begin VB.Label lblTipoPuntoVendita 
         Alignment       =   1  'Right Justify
         Caption         =   "tipo punto vendita"
         Height          =   255
         Left            =   5580
         TabIndex        =   17
         Top             =   420
         Width           =   1275
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Clonazione del Punto Vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   33
      Top             =   180
      Width           =   6855
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   7140
      TabIndex        =   32
      Top             =   60
      Width           =   1635
   End
End
Attribute VB_Name = "frmClonazionePuntoVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPuntoVenditaSelezionato As Long
Private puntoVenditaSelezionato As String
Private idTipoPuntoVenditaSelezionato As Long
Private tipoPuntoVenditaSelezionato As String
Private nuovoNome As String
Private nuovaDescrizione As String
Private nuovoCodiceSAP As String
Private nuovoIndirizzo As String
Private nuovoCivico As String
Private nuovoCAP As String
Private nuovaCitta As String
Private nuovaProvicia As String
Private nuovoTelefono As String
Private nuovoFax As String
Private nuovoResponsabile As String
Private nuovaEMail As String

Public Sub Init()
    Call CaricaDallaBaseDati
    lblInfo1.Caption = "Punto Vendita"
    txtInfo1.Enabled = False
    txtInfo1.Text = puntoVenditaSelezionato
    txtTipoPuntoVendita.Enabled = False
    txtTipoPuntoVendita.Text = tipoPuntoVenditaSelezionato
    
    Call AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)
End Sub

Public Sub SetIdPuntoVenditaSelezionato(idPV As Long)
    idPuntoVenditaSelezionato = idPV
End Sub

Private Sub ResettaControlli()
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtCodiceSAP.Text = ""
    txtIndirizzo.Text = ""
    txtCivico.Text = ""
    txtCap.Text = ""
    txtCitta.Text = ""
    txtProvincia.Text = ""
    txtTelefono.Text = ""
    txtFax.Text = ""
    txtResponsabile.Text = ""
    txtEMail.Text = ""
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = Trim(txtNome.Text) <> ""
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT PV.NOME NOME, TPV.IDTIPOPUNTOVENDITA ID_TPV, TPV.NOME TPV" & _
        " FROM PUNTOVENDITA PV, TIPOPUNTOVENDITA TPV" & _
        " WHERE PV.IDTIPOPUNTOVENDITA = TPV.IDTIPOPUNTOVENDITA" & _
        " AND PV.IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        puntoVenditaSelezionato = rec("NOME")
        idTipoPuntoVenditaSelezionato = rec("ID_TPV")
        tipoPuntoVenditaSelezionato = rec("TPV")
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    ResettaControlli
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub
Private Sub Conferma()
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim esito As Boolean
    
    If CampiRiempitiCorrettamente Then
        dataOraInizio = FormatDateTime(Now, vbGeneralDate)
        esito = IsClonazionePuntoVenditaOK(idPuntoVenditaSelezionato, nuovoNome, idTipoPuntoVenditaSelezionato, _
                                            nuovaDescrizione, nuovoCodiceSAP, nuovoIndirizzo, nuovoCivico, _
                                            nuovoCAP, nuovaCitta, nuovaProvicia, nuovoTelefono, nuovoFax, _
                                            nuovoResponsabile, nuovaEMail)
        
        If esito Then
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("NotificaClonazionePuntoVendita", nuovoNome, puntoVenditaSelezionato, dataOraInizio, dataOraFine)
            ResettaControlli
            AggiornaAbilitazioneControlli
        Else
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("ErroreClonazione", dataOraInizio, dataOraFine)
        End If
    End If
End Sub

Private Function CampiRiempitiCorrettamente() As Boolean
    Dim listaNonConformita As New Collection
    
    nuovoNome = Trim(txtNome.Text)
    nuovaDescrizione = Trim(txtDescrizione.Text)
    nuovoCodiceSAP = Trim(txtCodiceSAP.Text)
    nuovoIndirizzo = Trim(txtIndirizzo.Text)
    nuovoCivico = Trim(txtCivico.Text)
    nuovoCAP = Trim(txtCap.Text)
    nuovaCitta = Trim(txtCitta.Text)
    nuovaProvicia = Trim(txtProvincia.Text)
    nuovoTelefono = Trim(txtTelefono.Text)
    nuovoFax = Trim(txtFax.Text)
    nuovoResponsabile = Trim(txtResponsabile.Text)
    nuovaEMail = Trim(txtEMail.Text)

    If ViolataUnicit�("PUNTOVENDITA", "NOME = " & SqlStringValue(nuovoNome), "", "", "", "") Then
        Call listaNonConformita.Add("- il valore nome = " _
            & SqlStringValue(nuovoNome) & " � gi� presente in DB;")
    End If
    
    If ViolataUnicit�("PUNTOVENDITA", "CODICESAP = " & SqlStringValue(nuovoCodiceSAP), "", "", "", "") Then
        Call listaNonConformita.Add("- il valore Codice SAP = " _
            & SqlStringValue(nuovoCodiceSAP) & " � gi� presente in DB;")
    End If
    
    If listaNonConformita.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformita))
    End If

    CampiRiempitiCorrettamente = (listaNonConformita.count = 0)
End Function

Private Sub txtNome_Change()
    AggiornaAbilitazioneControlli
End Sub
