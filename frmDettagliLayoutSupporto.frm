VERSION 5.00
Begin VB.Form frmDettagliLayoutSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Associazione con Layout supporto "
   ClientHeight    =   1320
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4650
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1320
   ScaleWidth      =   4650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   900
      TabIndex        =   2
      Top             =   780
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2580
      TabIndex        =   1
      Top             =   780
      Width           =   1035
   End
   Begin VB.ComboBox cmbLayoutSupporto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   300
      Width           =   4275
   End
End
Attribute VB_Name = "frmDettagliLayoutSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idLayoutSupportoSelezionato As Long
Private nomeLayoutSupportoSelezionato As String
Private codiceLayoutSupportoSelezionato As String
Private labelLayoutSupportoSelezionato As String
Private idOrganizzazioneSelezionata As Long

Private exitCode As ExitCodeEnum

Public Sub Init(idTipoSupportoSelezionato As Long)
    Dim listaId As Collection
    Dim listaLabel As Collection
    Dim sql As String

    sql = "SELECT LS.IDLAYOUTSUPPORTO ID, LS.CODICE COD, LS.NOME NOME" & _
        " FROM LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
        " WHERE LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO" & _
        " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OTL.IDTIPOSUPPORTO = " & idTipoSupportoSelezionato & _
        " AND LS.ATTIVO = 1" & _
        " AND LS.VALIDOPERTITOLODIACCESSO = 1" & _
        " ORDER BY COD"
    Call CaricaValoriCombo(cmbLayoutSupporto, sql, "COD", "NOME")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = cmbLayoutSupporto.ListIndex <> idNessunElementoSelezionato
End Sub

Private Sub cmbLayoutSupporto_Click()
    Call cmbLayoutSupporto_Update
End Sub

Private Sub cmbLayoutSupporto_Update()
    idLayoutSupportoSelezionato = cmbLayoutSupporto.ItemData(cmbLayoutSupporto.ListIndex)
    labelLayoutSupportoSelezionato = cmbLayoutSupporto.Text
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Public Sub SetIdOrganizzazioneSelezionata(idO As Long)
    idOrganizzazioneSelezionata = idO
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo1 As String, Optional nomeCampo2 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            If Not IsMissing(nomeCampo2) Then
                descrizione = rec(NomeCampo1) & " - " & rec(nomeCampo2)
            Else
                descrizione = rec(NomeCampo1)
            End If
            cmb.AddItem descrizione
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Function GetIdLayoutSupportoSelezionato() As Long
    GetIdLayoutSupportoSelezionato = idLayoutSupportoSelezionato
End Function

Public Function GetLabelLayoutSupportoSelezionato() As String
    GetLabelLayoutSupportoSelezionato = labelLayoutSupportoSelezionato
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Private Sub CaricaValoriComboDaLista(cmb As ComboBox, listaLabelElementi As Collection, listaIdElementi As Collection)
    Dim i As Integer
    
    For i = 1 To listaIdElementi.count
        cmb.AddItem listaLabelElementi(i)
        cmb.ItemData(i - 1) = listaIdElementi(i)
    Next i
            
End Sub



