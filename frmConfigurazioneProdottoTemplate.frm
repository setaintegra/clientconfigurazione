VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoTemplate 
   Caption         =   "Configurazione abilitazioni per stampe pdf"
   ClientHeight    =   7245
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11640
   LinkTopic       =   "Form1"
   ScaleHeight     =   7245
   ScaleWidth      =   11640
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtLayoutAlfrescoStampaContestualeItaliano 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7080
      TabIndex        =   23
      Top             =   2280
      Width           =   3975
   End
   Begin VB.CommandButton cmdSelezionaStampaContestualeItaliano 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   2280
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionaPassbookInglese 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5880
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionaPassbookItaliano 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   5160
      Width           =   435
   End
   Begin VB.TextBox txtLayoutAlfrescoPassbookInglese 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7080
      TabIndex        =   18
      Top             =   5880
      Width           =   3975
   End
   Begin VB.TextBox txtLayoutAlfrescoPassbookItaliano 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7080
      TabIndex        =   16
      Top             =   5160
      Width           =   3975
   End
   Begin VB.TextBox txtLayoutAlfrescoEmailInglese 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7080
      TabIndex        =   14
      Top             =   4080
      Width           =   3975
   End
   Begin VB.TextBox txtLayoutAlfrescoEmailItaliano 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7080
      TabIndex        =   12
      Top             =   3360
      Width           =   3975
   End
   Begin VB.CommandButton cmdSelezionaEmailItaliano 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   3360
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionaEmailInglese 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   4080
      Width           =   435
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   435
      Left            =   5280
      TabIndex        =   9
      Top             =   6600
      Width           =   1155
   End
   Begin VB.ListBox lstLayoutalfrescoDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1530
      Left            =   180
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2220
      Width           =   5190
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8040
      TabIndex        =   2
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9780
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10320
      TabIndex        =   0
      Top             =   6600
      Width           =   1155
   End
   Begin VB.Label Label7 
      Caption         =   "Layout alfresco per stampa contestuale (italiano)"
      Enabled         =   0   'False
      Height          =   255
      Left            =   7080
      TabIndex        =   24
      Top             =   2040
      Width           =   3975
   End
   Begin VB.Label Label5 
      Caption         =   "Layout alfresco per Passbook (inglese)"
      Enabled         =   0   'False
      Height          =   255
      Left            =   7080
      TabIndex        =   19
      Top             =   5640
      Width           =   3975
   End
   Begin VB.Label Label4 
      Caption         =   "Layout alfresco per Passbook (italiano)"
      Enabled         =   0   'False
      Height          =   255
      Left            =   7080
      TabIndex        =   17
      Top             =   4920
      Width           =   3975
   End
   Begin VB.Label Label3 
      Caption         =   "Layout alfresco per email (inglese)"
      Enabled         =   0   'False
      Height          =   255
      Left            =   7080
      TabIndex        =   15
      Top             =   3840
      Width           =   3975
   End
   Begin VB.Label Label1 
      Caption         =   "Layout alfresco per email (italiano)"
      Enabled         =   0   'False
      Height          =   255
      Left            =   7080
      TabIndex        =   13
      Top             =   3120
      Width           =   3975
   End
   Begin VB.Label lblDisponibli 
      Caption         =   "Layout Alfresco configurati su organizzazione"
      Height          =   195
      Left            =   240
      TabIndex        =   8
      Top             =   1980
      Width           =   3915
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Abilitazioni layout stampe pdf, eMail e Passbook ticketing"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8040
      TabIndex        =   6
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9780
      TabIndex        =   5
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label Label2 
      Caption         =   "I layout Alfresco selezionabili sono soltanto quelli permessi a livello di organizzazione."
      Height          =   315
      Left            =   240
      TabIndex        =   4
      Top             =   1200
      Width           =   10635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private internalEvent As Boolean

Private prodottoRientraDecretoSicurezza As ValoreBooleanoEnum
Private numeroMassimoTitoliPerAcqProdotto As Long

Private stringaListaSuperareeSelezionate As String
Private quantitaSuperareeSelezionate As Long

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private idLayoutAlfrescoStampaContestualeItaliano As Long
Private nomeLayoutAlfrescoStampaContestualeItaliano As String
Private idLayoutAlfrescoEmailItaliano As Long
Private nomeLayoutAlfrescoEmailItaliano As String
Private idLayoutAlfrescoEmailInglese As Long
Private nomeLayoutAlfrescoEmailInglese As String
Private idLayoutAlfrescoPassbookItaliano As Long
Private nomeLayoutAlfrescoPassbookItaliano As String
Private idLayoutAlfrescoPassbookInglese As Long
Private nomeLayoutAlfrescoPassbookInglese As String

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
End Sub

Public Sub Init(idPianta As Long, idProdotto As Long, nomeProdotto As String, idOrganizzazione As Long, nomeOrganizzazione As String)

    idPiantaSelezionata = idPianta
    idProdottoSelezionato = idProdotto
    nomeProdottoSelezionato = nomeProdotto
    idOrganizzazioneSelezionata = idOrganizzazione
    nomeOrganizzazioneSelezionata = nomeOrganizzazione

    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLst_LayoutAlfresco
    Call CaricaValori
    Call AggiornaVisualizzazioneValoriCorrenti
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    prodottoRientraDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMassimoTitoliPerAcqProdotto = n
End Sub

Private Sub CaricaValoriLst_LayoutAlfresco()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    lstLayoutalfrescoDisponibili.Clear
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT DISTINCT HTL.IDLAYOUTALFRESCO ID, HTL.NOME NOME" & _
        " FROM LAYOUTALFRESCO HTL, ORGANIZZAZIONE_LAYOUTALFRESCO OHTL" & _
        " WHERE OHTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OHTL.IDLAYOUTALFRESCO = HTL.IDLAYOUTALFRESCO" & _
        " ORDER BY HTL.NOME"
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstLayoutalfrescoDisponibili.AddItem rec("NOME")
            lstLayoutalfrescoDisponibili.ItemData(i) = rec("ID")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub CaricaValori()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim chiaveElemento As String
    Dim elemento As clsElementoLista
    
    idLayoutAlfrescoStampaContestualeItaliano = idNessunElementoSelezionato
    idLayoutAlfrescoEmailItaliano = idNessunElementoSelezionato
    idLayoutAlfrescoEmailInglese = idNessunElementoSelezionato
    idLayoutAlfrescoPassbookItaliano = idNessunElementoSelezionato
    idLayoutAlfrescoPassbookInglese = idNessunElementoSelezionato
    txtLayoutAlfrescoStampaContestualeItaliano = ""
    txtLayoutAlfrescoEmailItaliano = ""
    txtLayoutAlfrescoEmailInglese = ""
    txtLayoutAlfrescoPassbookItaliano = ""
    txtLayoutAlfrescoPassbookInglese = ""
                
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT DISTINCT IDLAYOUTALFRESCO, LA.NOME" & _
            " FROM PRODOTTO_LAYOUTALFRESCO PLA, LAYOUTALFRESCO LA" & _
            " WHERE PLA.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PLA.IDLAYOUTALFRESCO_ITA = LA.IDLAYOUTALFRESCO" & _
            " AND PLA.IDTIPOMODALITAFORNITURATITOLI = 1"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idLayoutAlfrescoStampaContestualeItaliano = rec("IDLAYOUTALFRESCO")
            nomeLayoutAlfrescoStampaContestualeItaliano = rec("NOME")
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    sql = "SELECT DISTINCT IDLAYOUTALFRESCO, LA.NOME" & _
            " FROM PRODOTTO_LAYOUTALFRESCO PLA, LAYOUTALFRESCO LA" & _
            " WHERE PLA.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PLA.IDLAYOUTALFRESCO_ITA = LA.IDLAYOUTALFRESCO" & _
            " AND PLA.IDTIPOMODALITAFORNITURATITOLI IN (2, 3, 4, 5)"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idLayoutAlfrescoEmailItaliano = rec("IDLAYOUTALFRESCO")
            nomeLayoutAlfrescoEmailItaliano = rec("NOME")
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    sql = "SELECT DISTINCT IDLAYOUTALFRESCO, LA.NOME" & _
            " FROM PRODOTTO_LAYOUTALFRESCO PLA, LAYOUTALFRESCO LA" & _
            " WHERE PLA.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PLA.IDLAYOUTALFRESCO_ING = LA.IDLAYOUTALFRESCO" & _
            " AND PLA.IDTIPOMODALITAFORNITURATITOLI IN (2, 3, 4, 5)"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idLayoutAlfrescoEmailInglese = rec("IDLAYOUTALFRESCO")
            nomeLayoutAlfrescoEmailInglese = rec("NOME")
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    sql = "SELECT DISTINCT IDLAYOUTALFRESCO, LA.NOME" & _
            " FROM PRODOTTO_LAYOUTALFRESCO PLA, LAYOUTALFRESCO LA" & _
            " WHERE PLA.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PLA.IDLAYOUTALFRESCO_ITA = LA.IDLAYOUTALFRESCO" & _
            " AND PLA.IDTIPOMODALITAFORNITURATITOLI = 6"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idLayoutAlfrescoPassbookItaliano = rec("IDLAYOUTALFRESCO")
            nomeLayoutAlfrescoPassbookItaliano = rec("NOME")
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    sql = "SELECT DISTINCT IDLAYOUTALFRESCO, LA.NOME" & _
            " FROM PRODOTTO_LAYOUTALFRESCO PLA, LAYOUTALFRESCO LA" & _
            " WHERE PLA.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PLA.IDLAYOUTALFRESCO_ING = LA.IDLAYOUTALFRESCO" & _
            " AND PLA.IDTIPOMODALITAFORNITURATITOLI = 6"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idLayoutAlfrescoPassbookInglese = rec("IDLAYOUTALFRESCO")
            nomeLayoutAlfrescoPassbookInglese = rec("NOME")
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub AggiornaVisualizzazioneValoriCorrenti()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim chiaveElemento As String
    Dim elemento As clsElementoLista
    
    txtLayoutAlfrescoStampaContestualeItaliano.Text = nomeLayoutAlfrescoStampaContestualeItaliano
    txtLayoutAlfrescoEmailItaliano.Text = nomeLayoutAlfrescoEmailItaliano
    txtLayoutAlfrescoEmailInglese.Text = nomeLayoutAlfrescoEmailInglese
    txtLayoutAlfrescoPassbookItaliano.Text = nomeLayoutAlfrescoPassbookItaliano
    txtLayoutAlfrescoPassbookInglese.Text = nomeLayoutAlfrescoPassbookInglese
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call InserisciNellaBaseDati
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idSuperarea As Long
    Dim idTemplate As Long
    Dim i As Long
    Dim j As Long
    
On Error GoTo errori_HTLayout_InserisciNellaBaseDati

    If idLayoutAlfrescoEmailItaliano <> idNessunElementoSelezionato And idLayoutAlfrescoEmailInglese = idNessunElementoSelezionato Or idLayoutAlfrescoEmailItaliano = idNessunElementoSelezionato And idLayoutAlfrescoEmailInglese <> idNessunElementoSelezionato Or _
        idLayoutAlfrescoPassbookItaliano <> idNessunElementoSelezionato And idLayoutAlfrescoPassbookInglese = idNessunElementoSelezionato Or idLayoutAlfrescoPassbookItaliano = idNessunElementoSelezionato And idLayoutAlfrescoPassbookInglese <> idNessunElementoSelezionato Then
        MsgBox "Impostazioni non congruenti: e' necessario impostare i layout per entrambe le lingue"
    Else
        Call ApriConnessioneBD_ORA
        ORADB.BeginTrans
    
        sql = "DELETE FROM PRODOTTO_LAYOUTALFRESCO WHERE IDPRODOTTO = " & idProdottoSelezionato
        n = ORADB.ExecuteSQL(sql)
            
        If idLayoutAlfrescoStampaContestualeItaliano <> idNessunElementoSelezionato Then
            sql = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & idLayoutAlfrescoStampaContestualeItaliano & ", " & idLayoutAlfrescoStampaContestualeItaliano & ", 1)"
            n = ORADB.ExecuteSQL(sql)
        End If
        If idLayoutAlfrescoEmailItaliano <> idNessunElementoSelezionato And idLayoutAlfrescoEmailInglese <> idNessunElementoSelezionato Then
            sql = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & idLayoutAlfrescoEmailItaliano & ", " & idLayoutAlfrescoEmailInglese & ", 2)"
            n = ORADB.ExecuteSQL(sql)
            sql = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & idLayoutAlfrescoEmailItaliano & ", " & idLayoutAlfrescoEmailInglese & ", 3)"
            n = ORADB.ExecuteSQL(sql)
            sql = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & idLayoutAlfrescoEmailItaliano & ", " & idLayoutAlfrescoEmailInglese & ", 4)"
            n = ORADB.ExecuteSQL(sql)
            sql = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & idLayoutAlfrescoEmailItaliano & ", " & idLayoutAlfrescoEmailInglese & ", 5)"
            n = ORADB.ExecuteSQL(sql)
        End If
        If idLayoutAlfrescoPassbookItaliano <> idNessunElementoSelezionato And idLayoutAlfrescoPassbookInglese <> idNessunElementoSelezionato Then
            sql = "INSERT INTO PRODOTTO_LAYOUTALFRESCO (IDPRODOTTO, IDLAYOUTALFRESCO_ITA, IDLAYOUTALFRESCO_ING, IDTIPOMODALITAFORNITURATITOLI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & idLayoutAlfrescoPassbookItaliano & ", " & idLayoutAlfrescoPassbookInglese & ", 6)"
            n = ORADB.ExecuteSQL(sql)
        End If
    
        ORADB.CommitTrans
        Call ChiudiConnessioneBD_ORA
    End If

    Call AggiornaAbilitazioneControlli

    Exit Sub

errori_HTLayout_InserisciNellaBaseDati:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdSelezionaStampaContestualeItaliano_Click()
    Dim id As Long
    Dim nome As String
    
    If lstLayoutalfrescoDisponibili.ListIndex >= 0 Then
        If isLayoutAlfrescoCompatibilePDF(lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)) Then
            idLayoutAlfrescoStampaContestualeItaliano = lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)
            nomeLayoutAlfrescoStampaContestualeItaliano = lstLayoutalfrescoDisponibili
            AggiornaVisualizzazioneValoriCorrenti
        Else
            MsgBox "Layout Alfresco non compatibile con la modalita' stampa contestuale!"
        End If
    Else
        MsgBox "Selezionare un layout Alfresco!"
    End If
End Sub

Private Sub cmdSelezionaEmailItaliano_Click()
    Dim id As Long
    Dim nome As String
    
    If lstLayoutalfrescoDisponibili.ListIndex >= 0 Then
        If isLayoutAlfrescoCompatibilePDF(lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)) Then
            idLayoutAlfrescoEmailItaliano = lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)
            nomeLayoutAlfrescoEmailItaliano = lstLayoutalfrescoDisponibili
            AggiornaVisualizzazioneValoriCorrenti
        Else
            MsgBox "Layout Alfresco non compatibile con la modalita' email!"
        End If
    Else
        MsgBox "Selezionare un layout Alfresco!"
    End If
End Sub

Private Sub cmdSelezionaEmailInglese_Click()
    If lstLayoutalfrescoDisponibili.ListIndex >= 0 Then
        If isLayoutAlfrescoCompatibilePDF(lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)) Then
            idLayoutAlfrescoEmailInglese = lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)
            nomeLayoutAlfrescoEmailInglese = lstLayoutalfrescoDisponibili
            AggiornaVisualizzazioneValoriCorrenti
        Else
            MsgBox "Layout Alfresco non compatibile con la modalita' email!"
        End If
    Else
        MsgBox "Selezionare un layout Alfresco!"
    End If
End Sub

Private Sub cmdSelezionaPassbookItaliano_Click()
    If lstLayoutalfrescoDisponibili.ListIndex >= 0 Then
        If isLayoutAlfrescoCompatibilePassbook(lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)) Then
            idLayoutAlfrescoPassbookItaliano = lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)
            nomeLayoutAlfrescoPassbookItaliano = lstLayoutalfrescoDisponibili
            AggiornaVisualizzazioneValoriCorrenti
        Else
            MsgBox "Layout Alfresco non compatibile con la modalita' Passbook!"
        End If
    Else
        MsgBox "Selezionare un layout Alfresco!"
    End If
End Sub

Private Sub cmdSelezionaPassbookInglese_Click()
    If lstLayoutalfrescoDisponibili.ListIndex >= 0 Then
        If isLayoutAlfrescoCompatibilePassbook(lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)) Then
            idLayoutAlfrescoPassbookInglese = lstLayoutalfrescoDisponibili.ItemData(lstLayoutalfrescoDisponibili.ListIndex)
            nomeLayoutAlfrescoPassbookInglese = lstLayoutalfrescoDisponibili
            AggiornaVisualizzazioneValoriCorrenti
        Else
            MsgBox "Layout Alfresco non compatibile con la modalita' Passbook!"
        End If
    Else
        MsgBox "Selezionare un layout Alfresco!"
    End If
End Sub

Private Function isLayoutAlfrescoCompatibilePDF(id As Long) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim chiaveElemento As String
    Dim elemento As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    isLayoutAlfrescoCompatibilePDF = True
    sql = "SELECT COUNT(*) CONT" & _
            " FROM LAYOUTALFRESCO LA, EC_EVENT_CATALOG_TB EV" & _
            " WHERE LA.IDLAYOUTALFRESCO = " & id & _
            " AND LA.NOME = EV.EVENT_ID" & _
            " AND PDF_SUPPORT = 1"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("CONT") = 0 Then
                isLayoutAlfrescoCompatibilePDF = False
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
End Function

Private Function isLayoutAlfrescoCompatibilePassbook(id As Long) As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim chiaveElemento As String
    Dim elemento As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    isLayoutAlfrescoCompatibilePassbook = True
    sql = "SELECT COUNT(*) CONT" & _
            " FROM LAYOUTALFRESCO LA, EC_EVENT_CATALOG_TB EV" & _
            " WHERE LA.IDLAYOUTALFRESCO = " & id & _
            " AND LA.NOME = EV.EVENT_ID" & _
            " AND PASSBOOK_SUPPORT = 1"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("CONT") = 0 Then
                isLayoutAlfrescoCompatibilePassbook = False
            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
End Function

