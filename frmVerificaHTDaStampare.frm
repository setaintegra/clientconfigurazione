VERSION 5.00
Begin VB.Form frmVerificaHTDaStampare 
   Caption         =   "Quantita' totale:"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8355
   LinkTopic       =   "Form1"
   ScaleHeight     =   6945
   ScaleWidth      =   8355
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbProdotto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   1785
      Width           =   5580
   End
   Begin VB.CommandButton cmdEsporta 
      Caption         =   "Esporta lista"
      Height          =   375
      Left            =   5760
      TabIndex        =   9
      Top             =   1800
      Width           =   2415
   End
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca HT da Stampare"
      Height          =   375
      Left            =   5760
      TabIndex        =   8
      Top             =   1080
      Width           =   2415
   End
   Begin VB.ListBox lstProdotti 
      Height          =   2595
      Left            =   120
      TabIndex        =   7
      Top             =   3600
      Width           =   8055
   End
   Begin VB.TextBox txtQuantitaTotale 
      Height          =   375
      Left            =   2520
      TabIndex        =   6
      Top             =   2640
      Width           =   1815
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1050
      Width           =   5580
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   465
      Left            =   7080
      TabIndex        =   0
      Top             =   6360
      Width           =   1140
   End
   Begin VB.Label lblProdotto 
      Caption         =   "Prodotti"
      Height          =   195
      Left            =   75
      TabIndex        =   11
      Top             =   1560
      Width           =   2025
   End
   Begin VB.Label Label2 
      Caption         =   "Lista Prodotti"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   3240
      Width           =   1470
   End
   Begin VB.Label Label1 
      Caption         =   "Quantita' totale da stampare:"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2640
      Width           =   2295
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Verifica home ticket"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   150
      TabIndex        =   3
      Top             =   120
      Width           =   5775
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazioni"
      Height          =   255
      Left            =   75
      TabIndex        =   2
      Top             =   750
      Width           =   1470
   End
End
Attribute VB_Name = "frmVerificaHTDaStampare"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long

Private Const FILE_BAT As Integer = 1

Private Sub cerca()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    Call selezionaOrganizzazione
    MousePointer = mousePointerOld

End Sub

Private Sub selezionaOrganizzazione()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim qtaTotale As Long

    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
    qtaTotale = 0
    lstProdotti.Clear

    sql = "SELECT P.IDPRODOTTO || ' - ' || RPAD(P.NOME, 30, ' ') NOME,  COUNT(*) QTA" & _
        " FROM ORDINE O, ELEMENTOORDINE EO, TITOLO T, STATOTITOLO ST, PRODOTTO P" & _
        " Where NUMEROTIPOGRAFICOHOMETICKET Is Not Null" & _
        " AND NOMEFILEHOMETICKET IS NULL" & _
        " AND EO.IDORDINE = O.IDORDINE" & _
        " AND O.IDTIPOSTATOORDINE = 2" & _
        " AND IDTIPOMODALITAFORNITURATITOLI = 5" & _
        " AND EO.IDTITOLO = T.IDTITOLO" & _
        " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
        " AND ST.IDTIPOSTATOTITOLO = 2" & _
        " AND T.IDPRODOTTO > 18000" & _
        " AND T.IDPRODOTTO = P.IDPRODOTTO" & _
        " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " GROUP BY P.IDPRODOTTO, P.NOME" & _
        " ORDER BY P.IDPRODOTTO"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            qtaTotale = qtaTotale + rec("QTA")
            lstProdotti.AddItem rec("NOME") & " : " & rec("QTA")
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    txtQuantitaTotale.Text = qtaTotale
    
End Sub

Private Sub cmbOrganizzazione_Click()
    Call cmbProdotto_Update
End Sub

Private Sub cmbProdotto_Update()
    Dim sql As String

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    
    sql = "SELECT P.idprodotto ID, P.idprodotto || ' - ' || P.NOME as NOME"
    sql = sql & " FROM prodotto P"
    sql = sql & " WHERE P.idtipostatoprodotto = 3"
    sql = sql & " AND P.idorganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY P.NOME, P.IDPRODOTTO ASC"
    
    Call CaricaValoriCombo(cmbProdotto, sql, "NOME")
End Sub

Private Sub cmbProdotto_Click()
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
End Sub

Private Sub cmdCerca_Click()
    Call cerca
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Public Sub Init()
    Dim sql As String

    sql = "SELECT O.idorganizzazione ID, O.nome NOME"
    sql = sql & " From organizzazione O, tipoStatoOrganizzazione TSO"
    sql = sql & " WHERE ((TSO.idtipostatoorganizzazione ="
    sql = sql & " O.idTipoStatoOrganizzazione"
    sql = sql & " )"
    sql = sql & " AND (TSO.idtipostatoorganizzazione = " & TSO_ATTIVA & ")"
    sql = sql & " )"
    sql = sql & " ORDER BY O.nome ASC"

    idRecordSelezionato = idNessunElementoSelezionato
    idOrganizzazioneSelezionata = idNessunElementoSelezionato
    idProdottoSelezionato = idNessunElementoSelezionato
    
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME")
    
    Call Me.Show(vbModal)
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    Call cmb.Clear
    sql = strSQL
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
'    cmb.AddItem "<tutti>"
'    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub cmdEsporta_Click()
    Call EsportaDatiSuExcel
End Sub

Private Sub EsportaDatiSuExcel()
    Dim xlsApp As New Excel.Application
    Dim xlsDoc As Excel.workbook
    Dim xlsSheet As Excel.workSheet
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeFile As String
    Dim nomeFileBat As String
    Dim mousePointerOld As Integer
    Dim s As String
    Dim i As Long
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    If idOrganizzazioneSelezionata = idNessunElementoSelezionato Then
        MsgBox "Nessuna organizzazione selezionata!"
    Else
    
        nomeFile = App.Path & "\" & idOrganizzazioneSelezionata & "-" & idProdottoSelezionato & "-ElencoHT-"

        nomeFile = nomeFile & _
            CompletaStringaConZeri(CStr(Year(Now)), 2) & _
            CompletaStringaConZeri(CStr(Month(Now)), 2) & _
            CompletaStringaConZeri(CStr(Day(Now)), 2) & _
            CompletaStringaConZeri(CStr(Hour(Now)), 2) & _
            CompletaStringaConZeri(CStr(Minute(Now)), 2) & _
            CompletaStringaConZeri(CStr(Second(Now)), 2)

        nomeFileBat = nomeFile & ".bat"

        If idProdottoSelezionato = idNessunElementoSelezionato Then
            MsgBox "Nessun prodotto selezionato!"
        Else
        sql = "SELECT PO.IDPRODOTTO IDPRODOTTO, PO.DESCRIZIONE PRODOTTO, R.DATAORAPRIMARAPPR," & _
            " EO.NOMEFILEHOMETICKET, S.NUMEROTIPOGRAFICO," & _
            " P.COGNOME COGNOME_ACQUIRENTE, P.NOME NOME_ACQUIRENTE," & _
            " P.DATANASCITA DATANASCITA_ACQUIRENTE," & _
            " C.NOME COMUNENASCITA_ACQUIRENTE, N.NOME NAZIONENASCITA_ACQUIRENTE," & _
            " U.EMAIL EMAIL_UTENTE," & _
            " O.IDORDINE" & _
            " FROM TITOLO T, ELEMENTOORDINE EO, ORDINE O," & _
            " ACQUIRENTE A," & _
            " PERSONA P," & _
            " PRODOTTO PO," & _
            " COMUNE C," & _
            " NAZIONE N," & _
            " STATOTITOLO ST," & _
            " SUPPORTO S, UTENTE U," & _
            " (SELECT PR.IDPRODOTTO, TO_CHAR(MIN(DATAORAINIZIO), 'DD-MM-YYYY HH24.MI.SS') DATAORAPRIMARAPPR" & _
            " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
            " WHERE PR.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " GROUP BY PR.IDPRODOTTO) R"
        sql = sql & _
            " WHERE T.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND T.IDTITOLO = EO.IDTITOLO" & _
            " AND O.IDTIPOMODALITAFORNITURATITOLI = 5" & _
            " AND EO.IDORDINE = O.IDORDINE" & _
            " AND O.IDACQUIRENTE = A.IDACQUIRENTE" & _
            " AND A.IDPERSONA = P.IDPERSONA" & _
            " AND T.IDPRODOTTO = PO.IDPRODOTTO" & _
            " AND P.IDCOMUNENASCITA = C.IDCOMUNE(+)" & _
            " AND P.IDNAZIONENASCITA = N.IDNAZIONE" & _
            " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
            " AND ST.IDSUPPORTO = S.IDSUPPORTO" & _
            " AND ST.IDTIPOSTATOTITOLO <> 6" & _
            " AND ST.IDTIPOSTATOTITOLO <> 7" & _
            " AND O.IDUTENTE = U.IDUTENTE(+)" & _
            " AND PO.IDPRODOTTO = R.IDPRODOTTO" & _
            " ORDER BY P.COGNOME"
    
        'Apro file xls
        Set xlsDoc = xlsApp.Workbooks.Add
        Set xlsSheet = xlsDoc.ActiveSheet

        xlsSheet.Rows.Cells(1, 1) = "IDPRODOTTO"
        xlsSheet.Rows.Cells(1, 2) = "PRODOTTO"
        xlsSheet.Rows.Cells(1, 3) = "DATAORAPRIMARAPPR"
        xlsSheet.Rows.Cells(1, 4) = "NOMEFILEHOMETICKET"
        xlsSheet.Rows.Cells(1, 5) = "NUMEROTIPOGRAFICO"
        xlsSheet.Rows.Cells(1, 6) = "COGNOME_ACQUIRENTE"
        xlsSheet.Rows.Cells(1, 7) = "NOME_ACQUIRENTE"
        xlsSheet.Rows.Cells(1, 8) = "DATANASCITA_ACQUIRENTE"
        xlsSheet.Rows.Cells(1, 9) = "COMUNENASCITA_ACQUIRENTE"
        xlsSheet.Rows.Cells(1, 10) = "NAZIONENASCITA_ACQUIRENTE"
        xlsSheet.Rows.Cells(1, 11) = "EMAIL_UTENTE"
        xlsSheet.Rows.Cells(1, 12) = "IDORDINE"

        Open nomeFileBat For Append As #FILE_BAT

        Call ApriConnessioneBD
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            i = 2
            While Not rec.EOF
                xlsSheet.Rows.Cells(i, 1) = rec("IDPRODOTTO")
                xlsSheet.Rows.Cells(i, 2) = rec("PRODOTTO")
                xlsSheet.Rows.Cells(i, 3) = rec("DATAORAPRIMARAPPR")
                xlsSheet.Rows.Cells(i, 4) = rec("NOMEFILEHOMETICKET")
                xlsSheet.Rows.Cells(i, 5) = rec("NUMEROTIPOGRAFICO")
                xlsSheet.Rows.Cells(i, 6) = rec("COGNOME_ACQUIRENTE")
                xlsSheet.Rows.Cells(i, 7) = rec("NOME_ACQUIRENTE")
                xlsSheet.Rows.Cells(i, 8) = rec("DATANASCITA_ACQUIRENTE")
                xlsSheet.Rows.Cells(i, 9) = rec("COMUNENASCITA_ACQUIRENTE")
                xlsSheet.Rows.Cells(i, 10) = rec("NAZIONENASCITA_ACQUIRENTE")
                xlsSheet.Rows.Cells(i, 11) = rec("EMAIL_UTENTE")
                xlsSheet.Rows.Cells(i, 12) = rec("IDORDINE")
                
                Print #FILE_BAT, "copy " & rec("NOMEFILEHOMETICKET") & " \PDFselezionati"
                
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        Call ChiudiConnessioneBD

        Call xlsDoc.SaveAs(nomeFile)
        Call xlsDoc.Close
        Call xlsApp.Quit
        Set xlsApp = Nothing

        Close #FILE_BAT

        MsgBox "Report salvato sul file: " & nomeFile

        MousePointer = mousePointerOld

        End If
    End If
    MousePointer = mousePointerOld

End Sub

