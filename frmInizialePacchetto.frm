VERSION 5.00
Begin VB.Form frmInizialePacchetto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pacchetto"
   ClientHeight    =   11835
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13080
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11835
   ScaleWidth      =   13080
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   10155
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   12855
      Begin VB.Frame frmContingenti 
         Caption         =   "Contingenti (operazioni da eseguire a prodoti non attivi)"
         Height          =   975
         Left            =   120
         TabIndex        =   39
         Top             =   9000
         Width           =   12615
         Begin VB.CommandButton cmdControllaCausaliProtezione 
            Caption         =   "Controlla causali di protezione"
            Enabled         =   0   'False
            Height          =   375
            Left            =   3000
            TabIndex        =   41
            Top             =   360
            Width           =   2415
         End
         Begin VB.CommandButton cmdPropagaCausaliProtezione 
            Caption         =   "Propaga da prodotto fittizio"
            Enabled         =   0   'False
            Height          =   375
            Left            =   120
            TabIndex        =   40
            Top             =   360
            Width           =   2415
         End
      End
      Begin VB.CommandButton cmdConfermaTariffeSuperaree 
         Caption         =   "Conferma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   11520
         TabIndex        =   38
         Top             =   8160
         Width           =   1215
      End
      Begin VB.ListBox lstSuperaree 
         Height          =   1425
         Left            =   5880
         MultiSelect     =   2  'Extended
         TabIndex        =   36
         Top             =   7200
         Width           =   3495
      End
      Begin VB.ListBox lstTariffe 
         Height          =   1425
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   34
         Top             =   7200
         Width           =   3495
      End
      Begin VB.CommandButton cmdConfermaSceltaProdotti 
         Caption         =   "Conferma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   11520
         TabIndex        =   33
         Top             =   6360
         Width           =   1215
      End
      Begin VB.TextBox txtNuovaQuantitaProdottiSelezionabili 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7920
         MaxLength       =   30
         TabIndex        =   31
         Top             =   3960
         Width           =   495
      End
      Begin VB.CommandButton cmdAggiungiOffertaPacchetto 
         Caption         =   "Aggiungi"
         Height          =   375
         Left            =   11520
         TabIndex        =   29
         Top             =   1800
         Width           =   1215
      End
      Begin VB.CommandButton cmdAggiungiSceltaProdotto 
         Caption         =   "Aggiungi"
         Height          =   375
         Left            =   8760
         TabIndex        =   28
         Top             =   3960
         Width           =   975
      End
      Begin VB.TextBox txtNuovaSceltaProdotti 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2760
         MaxLength       =   30
         TabIndex        =   27
         Top             =   3960
         Width           =   2775
      End
      Begin VB.TextBox txtCausaleProtezioneDedicata 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6600
         MaxLength       =   30
         TabIndex        =   25
         Top             =   2760
         Width           =   3015
      End
      Begin VB.TextBox txtOrganizzazioneSelezionata 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   23
         Top             =   2760
         Width           =   3255
      End
      Begin VB.ListBox lstProdottiSelezionati 
         Height          =   1425
         Left            =   8040
         MultiSelect     =   2  'Extended
         TabIndex        =   19
         Top             =   4800
         Width           =   3375
      End
      Begin VB.CommandButton cmdRimuoviProdotto 
         Caption         =   "Rimuovi"
         Height          =   375
         Left            =   8040
         TabIndex        =   18
         Top             =   6360
         Width           =   1215
      End
      Begin VB.ListBox lstProdottiDisponibili 
         Height          =   1425
         Left            =   4440
         MultiSelect     =   2  'Extended
         TabIndex        =   17
         Top             =   4800
         Width           =   3495
      End
      Begin VB.ListBox lstSceltaProdotti 
         Height          =   1035
         Left            =   1680
         TabIndex        =   16
         Top             =   4800
         Width           =   2535
      End
      Begin VB.CommandButton cmdAggiungiProdotto 
         Caption         =   "Aggiungi"
         Height          =   375
         Left            =   4440
         TabIndex        =   15
         Top             =   6360
         Width           =   1215
      End
      Begin VB.TextBox txtProdottoFittizio 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   14
         Top             =   3240
         Width           =   3255
      End
      Begin VB.ComboBox cmbCausaleProtezione 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6600
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   1800
         Width           =   3015
      End
      Begin VB.ComboBox cmbOrganizzazioni 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1800
         Width           =   3255
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   0
         Top             =   300
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   1680
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   720
         Width           =   7995
      End
      Begin VB.Label Superaree 
         Caption         =   "Superaree"
         Height          =   255
         Left            =   5880
         TabIndex        =   37
         Top             =   6960
         Width           =   1815
      End
      Begin VB.Label lblTariffe 
         Caption         =   "Tariffe"
         Height          =   255
         Left            =   1560
         TabIndex        =   35
         Top             =   6960
         Width           =   1815
      End
      Begin VB.Label lblQuantita 
         Alignment       =   1  'Right Justify
         Caption         =   "Quantitā prodotti selezionabili"
         Height          =   255
         Left            =   5520
         TabIndex        =   32
         Top             =   3960
         Width           =   2295
      End
      Begin VB.Label lbl 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   1920
         TabIndex        =   30
         Top             =   3960
         Width           =   735
      End
      Begin VB.Label lblCausaleProtezioneDedicata 
         Caption         =   "Causale protezione"
         Height          =   255
         Left            =   5160
         TabIndex        =   26
         Top             =   2760
         Width           =   1455
      End
      Begin VB.Label lblOrganizzazioneSelezionata 
         Alignment       =   1  'Right Justify
         Caption         =   "Organizzazione"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   2760
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Prodotti disponibili"
         Height          =   255
         Left            =   4440
         TabIndex        =   22
         Top             =   4560
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Prodotti selezionati"
         Height          =   255
         Left            =   8040
         TabIndex        =   21
         Top             =   4560
         Width           =   1815
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nuova scelta prodotti"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   3960
         Width           =   1575
      End
      Begin VB.Label lblProdottoFittizio 
         Alignment       =   1  'Right Justify
         Caption         =   "Prodotto fittizio"
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   3240
         Width           =   1095
      End
      Begin VB.Label lblCausaleProtezione 
         Alignment       =   1  'Right Justify
         Caption         =   "Causale protezione"
         Height          =   255
         Left            =   5040
         TabIndex        =   12
         Top             =   1860
         Width           =   1455
      End
      Begin VB.Label lblOrganizzazione 
         Alignment       =   1  'Right Justify
         Caption         =   "Organizzazione"
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1860
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "Descrizione"
         Height          =   255
         Left            =   300
         TabIndex        =   6
         Top             =   780
         Width           =   1095
      End
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   4
      Top             =   10800
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialePacchetto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOffertaPacchettoSelezionata As Long
Private idCausaleProtezioneSelezionata As Long
Private idOrganizzazioneSelezionata As Long
Private isRecordEditabile As Boolean
Private nomeOffertaPacchetto As String
Private descrizioneOffertaPacchetto As String
Private idOrganizzazione As Long
Private idCausaleProtezioneDedicata As Long
Private idProdottoDaClonare As Long
Private idProdottoFittizio As Long
Private nomeProdottoFittizio As String
Private idSceltaProdottoSelezionata As Long
Private quantitaProdottiSelezionabili As Long

Private pacchettoSalvatoInBD As Boolean

Private internalEvent As Boolean

Private tipoOffertaPacchettoSelezionata As TipoOffertaPacchettoEnum
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitaFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Public Sub Init()
    Call Variabili_Init
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
        
    Select Case modalitaFormCorrente
        Case A_NUOVO
            isRecordEditabile = True
            pacchettoSalvatoInBD = False
            Call SetIdOffertaPacchettoSelezionata(idNessunElementoSelezionato)
            'Call AssegnaValoriCampi
            CaricaComboOrganizzazioni
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AggiornaListaSceltaProdotti
            Call InizializzaListaTariffe
            Call InizializzaListaSuperaree
            lstTariffe.Enabled = True
            lstSuperaree.Enabled = True
'            cmdConfermaTariffeSuperaree.Enabled = True
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call AggiornaAbilitazioneControlli
    Call frmInizialePacchetto.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    If modalitaFormCorrente = A_NUOVO Then
        idOffertaPacchettoSelezionata = idNessunElementoSelezionato
        idOrganizzazioneSelezionata = idNessunElementoSelezionato
        idCausaleProtezioneSelezionata = idNessunElementoSelezionato
    End If
    nomeOffertaPacchetto = ""
    descrizioneOffertaPacchetto = ""
    tipoOffertaPacchettoSelezionata = TOP_NON_SPECIFICATO
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitaFormCorrente = mf
End Sub

Public Sub SetIdOffertaPacchettoSelezionata(id As Long)
    idOffertaPacchettoSelezionata = id
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
        Case TNCP_FINE
            Unload Me
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    Select Case modalitaFormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuova Offerta Pacchetto"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Offerta Pacchetto configurata"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Offerta Pacchetto configurata"
    End Select
    
'    fraAzioni.Enabled = (modalitaFormCorrente <> A_NUOVO)
'    fraAzioni.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtNome.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalitaFormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalitaFormCorrente <> A_ELIMINA)
'    optPrezziFissi.Enabled = (modalitaFormCorrente = A_NUOVO)
'    optPrezziVariabili.Enabled = (modalitaFormCorrente = A_NUOVO)
'    optPrezziRateizzabili.Enabled = (modalitaFormCorrente = A_NUOVO)
'    optPrezziRateizzabili.Enabled = (modalitaFormCorrente = A_NUOVO)
    
    lblOrganizzazione.Visible = (modalitaFormCorrente = A_NUOVO)
    cmbOrganizzazioni.Visible = (modalitaFormCorrente = A_NUOVO)
    lblCausaleProtezione.Visible = (modalitaFormCorrente = A_NUOVO)
    cmbCausaleProtezione.Visible = (modalitaFormCorrente = A_NUOVO)
    lblOrganizzazioneSelezionata.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtOrganizzazioneSelezionata.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtOrganizzazioneSelezionata.Enabled = False
    lblCausaleProtezioneDedicata.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtCausaleProtezioneDedicata.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtCausaleProtezioneDedicata.Enabled = False
    lblProdottoFittizio.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtProdottoFittizio.Visible = (modalitaFormCorrente <> A_NUOVO)
    txtProdottoFittizio.Enabled = False
    
    txtNuovaSceltaProdotti.Enabled = False
    txtNuovaQuantitaProdottiSelezionabili.Enabled = False
    cmdAggiungiSceltaProdotto.Enabled = False
    cmdAggiungiProdotto.Enabled = False
    cmdRimuoviProdotto.Enabled = False
    cmdAggiungiSceltaProdotto.Enabled = False
    
    lstTariffe.Enabled = (modalitaFormCorrente = A_MODIFICA)
    lstSuperaree.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdConfermaTariffeSuperaree.Enabled = False
    
    cmdPropagaCausaliProtezione.Enabled = (modalitaFormCorrente = A_MODIFICA)
    cmdControllaCausaliProtezione.Enabled = (modalitaFormCorrente = A_MODIFICA)
    
'    fraAzioni.Enabled = (modalitaFormCorrente = A_MODIFICA)
'    fraAzioni.Visible = (modalitaFormCorrente <> A_NUOVO)
'    cmdTariffePacchetto.Enabled = (modalitaFormCorrente = A_MODIFICA)
'    cmdAreePacchetto.Enabled = (modalitaFormCorrente = A_MODIFICA)
'    cmdSceltaProdotti.Enabled = (modalitaFormCorrente = A_MODIFICA)
'    cmdOrdineSceltaProdotti.Enabled = (modalitaFormCorrente = A_MODIFICA)
'    cmdOperatori.Enabled = (modalitaFormCorrente = A_MODIFICA)
    
    Select Case modalitaFormCorrente
        Case A_NUOVO
            'cmdPrecedente.Enabled = False
            'cmdEsci.Caption = "Abbandona"
            cmdConferma.Enabled = (Not pacchettoSalvatoInBD) And _
                (Trim(txtNome.Text) <> "" And _
                tipoOffertaPacchettoSelezionata <> TOP_NON_SPECIFICATO)
            cmdAnnulla.Enabled = (gestioneExitCode <> EC_CONFERMA)
            'cmdSuccessivo.Enabled = pacchettoSalvatoInBD
        Case A_MODIFICA
            cmdAggiungiOffertaPacchetto.Caption = "Modifica"
            'cmdPrecedente.Visible = False
            'cmdSuccessivo.Visible = False
            'cmdEsci.Visible = False
            cmdConferma.Visible = True
            cmdAnnulla.Visible = True
            cmdAnnulla.Enabled = True
            'fraNavigazioneProdotto.Visible = False
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'cmdPrecedente.Visible = False
            'cmdSuccessivo.Visible = False
            'cmdEsci.Visible = False
            cmdConferma.Visible = True
            cmdAnnulla.Visible = True
            cmdConferma.Enabled = True
            cmdAnnulla.Enabled = True
            'fraNavigazioneProdotto.Visible = False
    End Select
                         
End Sub

Private Sub cmbOrganizzazioni_Click()
    If Not internalEvent Then
        idOrganizzazioneSelezionata = cmbOrganizzazioni.ItemData(cmbOrganizzazioni.ListIndex)
        Call CaricaComboCausaliProtezione
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbCausaleProtezione_Click()
    If Not internalEvent Then
        idCausaleProtezioneSelezionata = cmbCausaleProtezione.ItemData(cmbCausaleProtezione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaComboOrganizzazioni()
    Dim sql As String
    
    Call cmbOrganizzazioni.Clear
    sql = "SELECT IDORGANIZZAZIONE ID, NOME" & _
        " FROM ORGANIZZAZIONE" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbOrganizzazioni, sql, "NOME")
End Sub

Private Sub CaricaComboCausaliProtezione()
    Dim sql As String
    
    Call cmbCausaleProtezione.Clear
    sql = "SELECT IDCAUSALEPROTEZIONE ID, SIMBOLOSUPIANTA || ' - ' || NOME AS NOME" & _
        " FROM CAUSALEPROTEZIONE" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY SIMBOLOSUPIANTA"
    Call CaricaValoriCombo(cmbCausaleProtezione, sql, "NOME")
End Sub

Private Sub cmdAggiungiOffertaPacchetto_Click()
    If (modalitaFormCorrente = A_NUOVO) Then
        If AggiungiOffertaPacchetto = True Then
            cmdAggiungiOffertaPacchetto.Enabled = False
            txtNuovaSceltaProdotti.Enabled = True
            txtNuovaQuantitaProdottiSelezionabili.Enabled = True
            cmdAggiungiSceltaProdotto.Enabled = True
            cmdConfermaSceltaProdotti.Enabled = True
            cmdConferma.Enabled = True
        End If
    ElseIf (modalitaFormCorrente = A_MODIFICA) Then
        Call ModificaOffertaPacchetto
    End If
     
End Sub

Private Sub cmdAggiungiProdotto_Click()
    Call AggiungiProdotti
    Call InizializzaListaProdottiDisponibili
    Call InizializzaListaProdottiSelezionati
End Sub

Private Sub cmdAggiungiSceltaProdotto_Click()
    Call AggiungiSceltaProdotto
    Call AggiornaListaSceltaProdotti
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Dim stringaNota As String

    stringaNota = "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    Call SetGestioneExitCode(EC_ANNULLA)
    Select Case modalitaFormCorrente
        Case A_NUOVO
            Call EliminaOffertaPacchettoDallaBaseDati(idOffertaPacchettoSelezionata)
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OFFERTA_PACCHETTO, , stringaNota)
    End Select
    Call SbloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idOffertaPacchettoSelezionata, isOffertaPacchettoBloccataDaUtente)
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    Dim nomeOrganizzazione As String
    Dim causaleProtezioneDedicata As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
    sql = "SELECT DISTINCT OP.NOME, OP.DESCRIZIONE, O.IDORGANIZZAZIONE, O.NOME NOMEORGANIZZAZIONE, PF.IDPRODOTTO IDPRODOTTOFITTIZIO, PF.NOME NOMEPRODOTTOFITTIZIO, CP.IDCAUSALEPROTEZIONE IDCAUSALEPROTEZIONEDEDICATA, CP.NOME CAUSALEPROTEZIONEDEDICATA"
    sql = sql & " FROM OFFERTAPACCHETTO OP, SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP, PRODOTTO P, ORGANIZZAZIONE O, PRODOTTO PF, CAUSALEPROTEZIONE CP"
    sql = sql & " WHERE OP.iDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " AND OP.IDOFFERTAPACCHETTO = SP.IDOFFERTAPACCHETTO"
    sql = sql & " AND SP.IDSCELTAPRODOTTO = SPP.IDSCELTAPRODOTTO"
    sql = sql & " AND SPP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " AND P.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE"
    sql = sql & " AND OP.IDPRODOTTOFITTIZIO = PF.IDPRODOTTO"
    sql = sql & " AND OP.IDCAUSALEPROTEZIONEDEDICATA = CP.IDCAUSALEPROTEZIONE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeOffertaPacchetto = rec("NOME")
        descrizioneOffertaPacchetto = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        idOrganizzazione = rec("IDORGANIZZAZIONE")
        nomeOrganizzazione = rec("NOMEORGANIZZAZIONE")
        idCausaleProtezioneDedicata = rec("IDCAUSALEPROTEZIONEDEDICATA")
        causaleProtezioneDedicata = rec("CAUSALEPROTEZIONEDEDICATA")
        idProdottoFittizio = rec("IDPRODOTTOFITTIZIO")
        nomeProdottoFittizio = rec("NOMEPRODOTTOFITTIZIO")
    End If
    rec.Close
    
    txtOrganizzazioneSelezionata.Text = nomeOrganizzazione
    txtCausaleProtezioneDedicata.Text = causaleProtezioneDedicata

    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = nomeOffertaPacchetto
    txtDescrizione.Text = descrizioneOffertaPacchetto
    txtProdottoFittizio.Text = idProdottoFittizio & " - " & nomeProdottoFittizio
    
'    If tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE Then
'        optPrezziRateizzabili.Value = True
'    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO Then
'        optPrezziFissi.Value = True
'    ElseIf tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE Then
'        optPrezziVariabili.Value = True
'    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Function TrovaQuantitaProdottiSelezionabili() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaQuantitaProdottiSelezionabili = 0
    sql = "SELECT NUMEROPRODOTTISELEZIONABILI FROM SCELTAPRODOTTO WHERE IDSCELTAPRODOTTO = " & idSceltaProdottoSelezionata
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        TrovaQuantitaProdottiSelezionabili = rec("NUMEROPRODOTTISELEZIONABILI")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function TrovaOrdineSceltaProdotto() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaOrdineSceltaProdotto = 0
    
    sql = "select max(ordine) + 1 NUOVOORDINE"
    sql = sql & " from sceltaprodotto"
    sql = sql & " Where idOffertaPacchetto = " & idOffertaPacchettoSelezionata
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        If IsNull(rec("NUOVOORDINE")) Then
            TrovaOrdineSceltaProdotto = 1
        Else
            TrovaOrdineSceltaProdotto = rec("NUOVOORDINE")
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function TrovaIdProdottoDaClonare() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaIdProdottoDaClonare = idNessunElementoSelezionato
    
    sql = "select pr.idprodotto"
    sql = sql & " from sceltaprodotto sp, sceltaprodotto_prodotto spp, prodotto_rappresentazione pr, rappresentazione r"
    sql = sql & " Where sp.idOffertaPacchetto = " & idOffertaPacchettoSelezionata
    sql = sql & " and sp.idsceltaprodotto = spp.idsceltaprodotto"
    sql = sql & " and spp.idprodotto = pr.idprodotto"
    sql = sql & " and pr.idrappresentazione = r.idrappresentazione"
    sql = sql & " order by dataorainizio desc"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        TrovaIdProdottoDaClonare = rec("IDPRODOTTO")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function TrovaIdProdottoFittizio() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaIdProdottoFittizio = idNessunElementoSelezionato
    sql = "SELECT IDPRODOTTOFITTIZIO"
    sql = sql & " FROM OFFERTAPACCHETTO"
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        If Not IsNull(rec("IDPRODOTTOFITTIZIO")) Then
            TrovaIdProdottoFittizio = rec("IDPRODOTTOFITTIZIO")
        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function TrovaIdRappresentazioneFittizia() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaIdRappresentazioneFittizia = idNessunElementoSelezionato
    sql = "SELECT IDRAPPRESENTAZIONE"
    sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE"
    sql = sql & " WHERE IDPRODOTTO = " & idProdottoFittizio
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        TrovaIdRappresentazioneFittizia = rec("IDRAPPRESENTAZIONE")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function TrovaIdNuovoProdottoFittizio() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaIdNuovoProdottoFittizio = idNessunElementoSelezionato
    sql = "SELECT IDPRODOTTO"
    sql = sql & " FROM PRODOTTO"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND NOME = 'PROD FITT " & nomeOffertaPacchetto & "'"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        TrovaIdNuovoProdottoFittizio = rec("IDPRODOTTO")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function TrovaQuantitaProdottiInOffertaPacchetto() As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    TrovaQuantitaProdottiInOffertaPacchetto = 0
    sql = "SELECT SUM(NUMEROPRODOTTISELEZIONABILI) QTA"
    sql = sql + " FROM SCELTAPRODOTTO"
    sql = sql + " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        TrovaQuantitaProdottiInOffertaPacchetto = rec("QTA")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    Exit Function

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Function AggiungiOffertaPacchetto() As Boolean
    Dim sql As String
    Dim n As Long
    
    AggiungiOffertaPacchetto = False
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    idOffertaPacchettoSelezionata = OttieniIdentificatoreDaSequenza("SQ_OFFERTAPACCHETTO")
    
    nomeOffertaPacchetto = txtNome.Text
    
    sql = "INSERT INTO OFFERTAPACCHETTO ("
    sql = sql & " IDOFFERTAPACCHETTO, NOME, DESCRIZIONE, "
    sql = sql & " IDTIPOOFFERTAPACCHETTO, IDCAUSALEPROTEZIONEDEDICATA) "
    sql = sql & " VALUES ( " & idOffertaPacchettoSelezionata & ", "
    sql = sql & SqlStringValue(txtNome.Text) & ", "
    sql = sql & SqlStringValue(txtDescrizione.Text) & ", "
    sql = sql & tipoOffertaPacchettoSelezionata & ", "
    sql = sql & idCausaleProtezioneSelezionata & ")"
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    If n = 1 Then
        AggiungiOffertaPacchetto = True
    End If
    
    Exit Function
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Function

Private Sub ModificaOffertaPacchetto()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    nomeOffertaPacchetto = txtNome.Text
    
    sql = "UPDATE OFFERTAPACCHETTO SET NOME=" & SqlStringValue(txtNome.Text) & ", DESCRIZIONE=" & SqlStringValue(txtDescrizione.Text)
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AggiungiSceltaProdotto()
    Dim sql As String
    Dim n As Long
    Dim idNuovaSceltaProdotto As Long
    Dim ordine As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    idNuovaSceltaProdotto = OttieniIdentificatoreDaSequenza("SQ_SCELTAPRODOTTO")
    
    ordine = TrovaOrdineSceltaProdotto
    
    sql = " INSERT INTO SCELTAPRODOTTO (IDSCELTAPRODOTTO, IDOFFERTAPACCHETTO, NUMEROPRODOTTISELEZIONABILI, ORDINE, NOME)"
    sql = sql & " VALUES ( " & idNuovaSceltaProdotto & ", "
    sql = sql & idOffertaPacchettoSelezionata & ", "
    sql = sql & txtNuovaQuantitaProdottiSelezionabili.Text & ", "
    sql = sql & ordine & ", "
    sql = sql & SqlStringValue(txtNuovaSceltaProdotti.Text) & ")"
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AggiungiProdotti()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    SETAConnection.BeginTrans
    
    For i = 1 To lstProdottiDisponibili.ListCount
        If lstProdottiDisponibili.Selected(i - 1) Then
            sql = "INSERT INTO SCELTAPRODOTTO_PRODOTTO (IDSCELTAPRODOTTO, IDPRODOTTO)"
            sql = sql & " VALUES ( " & idSceltaProdottoSelezionata & ", " & lstProdottiDisponibili.ItemData(i - 1) & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
    Next i
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub RimuoviProdotti()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    
    For i = 1 To lstProdottiSelezionati.ListCount
        If lstProdottiSelezionati.Selected(i - 1) Then
            sql = "DELETE FROM SCELTAPRODOTTO_PRODOTTO"
            sql = sql & " WHERE IDSCELTAPRODOTTO = " & idSceltaProdottoSelezionata
            sql = sql & " AND IDPRODOTTO = " & lstProdottiSelezionati.ItemData(i - 1)
            SETAConnection.Execute sql, n, adCmdText
        End If
    Next i
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub aggiornaOffertaPacchettoConIdProdottoFittizio()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    SETAConnection.BeginTrans

    sql = "UPDATE OFFERTAPACCHETTO SET IDPRODOTTOFITTIZIO = " & idProdottoFittizio
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub aggiungiRappresentazioneAProdottoFittizio()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    Dim idNuovoSpettacolo As Long
    Dim idNuovaRappresentazione As Long
    Dim nome As String
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    SETAConnection.BeginTrans
    
    idNuovoSpettacolo = OttieniIdentificatoreDaSequenza("SQ_SPETTACOLO")
    nome = Mid("SPETT FITT " & nomeOffertaPacchetto, 1, 30)
    
    sql = "INSERT INTO SPETTACOLO (IDSPETTACOLO, NOME, IDSTAGIONE)"
    sql = sql & " SELECT " & idNuovoSpettacolo & ", '" & nome & "', IDSTAGIONE"
    sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, SPETTACOLO S"
    sql = sql & " WHERE PR.IDPRODOTTO = " & idProdottoDaClonare
    sql = sql & " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " AND R.IDSPETTACOLO = S.IDSPETTACOLO"
    SETAConnection.Execute sql, n, adCmdText
    
    idNuovaRappresentazione = OttieniIdentificatoreDaSequenza("SQ_RAPPRESENTAZIONE")
    
    sql = "INSERT INTO RAPPRESENTAZIONE (IDRAPPRESENTAZIONE, DATAORAINIZIO, DURATAINMINUTI, CONTROLLOACCESSIABILITATO, IDVENUE, IDSPETTACOLO)"
    sql = sql & " SELECT " & idNuovaRappresentazione & ", DATAORAINIZIO - 1 / 24 / 60, DURATAINMINUTI, CONTROLLOACCESSIABILITATO, IDVENUE, 123"
    sql = sql & " FROM RAPPRESENTAZIONE"
    sql = sql & " WHERE IDRAPPRESENTAZIONE = ("
    sql = sql & " SELECT IDRAPPRESENTAZIONE FROM PRODOTTO_RAPPRESENTAZIONE WHERE IDPRODOTTO = " & idProdottoDaClonare
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub abilitaCausaleDedicataSuProdottoFittizio()
    Dim sql As String
    Dim n As Long
    Dim i As Long
    Dim idNuovoSpettacolo As Long
    Dim idNuovaRappresentazione As Long
    Dim nome As String
    
On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    SETAConnection.BeginTrans
    
    sql = "INSERT INTO OPERATORE_CAUSALEPROTEZIONE (IDOPERATORE, IDCAUSALEPROTEZIONE, IDPRODOTTO)"
    sql = sql & " SELECT CP.IDOPERATORE, CP.IDCAUSALEPROTEZIONE, OP.IDPRODOTTOFITTIZIO"
    sql = sql & " FROM OFFERTAPACCHETTO OP, CAUSALEPROTEZIONE CP"
    sql = sql & " WHERE OP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & " AND OP.IDCAUSALEPROTEZIONEDEDICATA = CP.IDCAUSALEPROTEZIONE"
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaTariffaProdottoFittizio(codice As String)
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim idTariffa As Long
    Dim n As Long
    
    sql = "SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoFittizio & " AND CODICE = '" & codice & "'"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        idTariffa = rec("IDTARIFFA")
    End If
    rec.Close
    
    sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDTARIFFA = " & idTariffa
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM PREZZOTITOLOPRODOTTO WHERE IDTARIFFA = " & idTariffa
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM CLASSEPV_TARIFFA WHERE IDTARIFFA = " & idTariffa
    SETAConnection.Execute sql, n, adCmdText

    sql = "DELETE FROM TARIFFA WHERE IDTARIFFA = " & idTariffa
    SETAConnection.Execute sql, n, adCmdText

End Sub

Private Sub ModificaPrezziTitoloFittizio()
    Dim sInTariffe As String
    Dim sInSuperaree As String
    Dim i As Long
    Dim n As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim qtaProdotti As Long
    Dim na As Long

On Error GoTo gestioneErrori

    Call ApriConnessioneBD
    SETAConnection.BeginTrans
    
    sInTariffe = "(0"
    For i = 1 To lstTariffe.ListCount
        If lstTariffe.Selected(i - 1) Then
            sInTariffe = sInTariffe & ", " & lstTariffe.ItemData(i - 1)
        End If
    Next i
    sInTariffe = sInTariffe & ")"
    
    sInSuperaree = "(0"
    For i = 1 To lstSuperaree.ListCount
        If lstSuperaree.Selected(i - 1) Then
            sInSuperaree = sInSuperaree & ", " & lstSuperaree.ItemData(i - 1)
        End If
    Next i
    sInSuperaree = sInSuperaree & ")"
    
' importazione di prezzi, periodi comm. e tariffe dal prodotto scelto
    sql = "INSERT INTO PRODOTTO_MODALITAEMISSIONE (IDPRODOTTO, IDMODALITAEMISSIONE)" & _
        " SELECT " & idProdottoFittizio & ", " & _
        " IDMODALITAEMISSIONE" & _
        " FROM PRODOTTO_MODALITAEMISSIONE" & _
        " WHERE IDPRODOTTO = " & idProdottoDaClonare
    SETAConnection.Execute sql, n, adCmdText
    Call IsImportazioneTariffePeriodiPrezziOK(idProdottoFittizio, idProdottoDaClonare, sInTariffe)
    
    qtaProdotti = TrovaQuantitaProdottiInOffertaPacchetto
    
    sql = "DELETE FROM PREZZOTITOLOPRODOTTO"
    sql = sql & " WHERE IDPRODOTTO = " & idProdottoFittizio & " AND IDAREA NOT IN " & sInSuperaree
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "UPDATE PREZZOTITOLOPRODOTTO SET IMPORTOBASE = IMPORTOBASE * " & qtaProdotti & ", IMPORTOSERVIZIOPREVENDITA = IMPORTOSERVIZIOPREVENDITA * " & qtaProdotti
    sql = sql & " WHERE IDTARIFFA IN (SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoFittizio & " AND CODICE IN (SELECT CODICE FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoDaClonare & " AND IDTARIFFA IN " & sInTariffe & "))"
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoPacchetto As Long
    Dim i As Integer
    Dim j As Integer
    Dim tt As clsTipoTerm
    Dim n As Long
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    Dim prodotto As clsElementoLista
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    idNuovoPacchetto = OttieniIdentificatoreDaSequenza("SQ_OFFERTAPACCHETTO")
    sql = " INSERT INTO OFFERTAPACCHETTO ("
    sql = sql & " IDOFFERTAPACCHETTO, NOME, DESCRIZIONE, "
    sql = sql & " IDTIPOOFFERTAPACCHETTO, IDCAUSALEPROTEZIONEDEDICATA) "
    sql = sql & " VALUES ( " & idNuovoPacchetto & ", "
    sql = sql & SqlStringValue(nomeOffertaPacchetto) & ", "
    sql = sql & SqlStringValue(descrizioneOffertaPacchetto) & ", "
    sql = sql & tipoOffertaPacchettoSelezionata & ", "
    sql = sql & idCausaleProtezioneSelezionata & ")"
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call frmSceltaPacchetto.SetIdRecordSelezionato(idNuovoPacchetto)
    Call SetIdOffertaPacchettoSelezionata(idNuovoPacchetto)
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneScelteProdotto
End Sub

Private Sub CaricaFormConfigurazioneAreePacchetto()
    Call frmConfigurazionePacchettoAree.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoAree.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchetto)
    Call frmConfigurazionePacchettoAree.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoAree.SetNumeroProdottiAssociati(numeroProdottiAssociati)
    Call frmConfigurazionePacchettoAree.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoAree.SetModalitāForm(modalitaFormCorrente)
    Call frmConfigurazionePacchettoAree.Init
End Sub

Private Sub cmdAreePacchetto_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraAreePacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConfermaSceltaProdotti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call confermaSceltaProdotti
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        DoEvents
        Call Annulla
    End If
End Sub

Private Sub cmdOrdineSceltaProdotti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraOrdineScelteProdotto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSceltaProdotti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraScelteProdotto
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraScelteProdotto()
    If isOffertaPacchettoBloccataDaUtente Then
        Call CaricaFormConfigurazioneScelteProdotto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub ConfiguraOrdineScelteProdotto()
    If isOffertaPacchettoBloccataDaUtente Then
        Call CaricaFormConfigurazioneOrdineScelteProdotto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub ConfiguraAreePacchetto()
    If isOffertaPacchettoBloccataDaUtente Then
        Call CaricaFormConfigurazioneAreePacchetto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub ConfiguraTariffePacchetto()
    If isOffertaPacchettoBloccataDaUtente Then
        Call CaricaFormConfigurazioneTariffePacchetto
    Else
        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
    End If
End Sub

Private Sub cmdTariffePacchetto_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTariffePacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaFormConfigurazioneTariffePacchetto()
    Call frmConfigurazionePacchettoTariffe.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoTariffe.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchetto)
    Call frmConfigurazionePacchettoTariffe.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoTariffe.SetNumeroProdottiAssociati(numeroProdottiAssociati)
    Call frmConfigurazionePacchettoTariffe.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoTariffe.SetModalitāForm(modalitaFormCorrente)
    Call frmConfigurazionePacchettoTariffe.Init
End Sub

Private Sub CaricaFormConfigurazioneScelteProdotto()
    Call frmConfigurazionePacchettoScelteProdotto.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoScelteProdotto.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchetto)
    Call frmConfigurazionePacchettoScelteProdotto.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoScelteProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoScelteProdotto.SetModalitāForm(modalitaFormCorrente)
    Call frmConfigurazionePacchettoScelteProdotto.Init
End Sub

Private Sub CaricaFormConfigurazioneOrdineScelteProdotto()
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchetto)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.SetModalitāForm(modalitaFormCorrente)
    Call frmConfigurazionePacchettoOrdiniScelteProdotto.Init
End Sub

Private Sub cmdConfermaTariffeSuperaree_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    cmdConfermaTariffeSuperaree.Enabled = False
    Call ModificaPrezziTitoloFittizio
    cmdConferma.Enabled = True
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdPropagaCausaliProtezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call PropagaCausaliProtezioneDaProdottoFittizio
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdControllaCausaliProtezione_Click()
    If ControllaCausaliProtezione Then
        MsgBox "Contingenti correttamente impostati"
    Else
        MsgBox "Contingenti NON correttamente impostati"
    End If
End Sub

Private Sub PropagaCausaliProtezioneDaProdottoFittizio()
    Dim sql As String
    Dim rec As OraDynaset
    Dim idProdotto As Long
    Dim n As Long
    Dim qtaCausaliProtezioneDaInserire As Long
    Dim qtaCausaliProtezioneInserite As Long
    
    qtaCausaliProtezioneInserite = 0
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(IDPOSTO) QTAPROTEZIONI" & _
            " FROM PROTEZIONEPOSTO" & _
            " WHERE IDPRODOTTO = " & idProdottoFittizio & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaCausaliProtezioneDaInserire = rec("QTAPROTEZIONI")
    End If
    rec.Close
    
    sql = "SELECT SPP.IDPRODOTTO" & _
            " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP" & _
            " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
            " AND SP.IDSCELTAPRODOTTO =SPP.IDSCELTAPRODOTTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idProdotto = rec("IDPRODOTTO")
        
            sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                " SELECT SQ_PROTEZIONEPOSTO.NEXTVAL, PP.IDPOSTO, " & idProdotto & ", " & idCausaleProtezioneDedicata & _
                " FROM PROTEZIONEPOSTO PP," & _
                " (SELECT T.IDPOSTO FROM TITOLO T, STATOTITOLO ST WHERE T.IDPRODOTTO = " & idProdotto & " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO AND ST.IDTIPOSTATOTITOLO < 6) V," & _
                " (SELECT IDPOSTO FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdotto & ") P" & _
                " WHERE PP.idProdotto = " & idProdottoFittizio & " AND PP.IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & _
                " AND PP.IDPOSTO = V.IDPOSTO(+) AND V.IDPOSTO IS NULL" & _
                " AND PP.IDPOSTO = P.IDPOSTO(+) AND P.IDPOSTO IS NULL"
            SETAConnection.Execute sql, n, adCmdText
            
            If n <> qtaCausaliProtezioneDaInserire Then
                MsgBox "Per il prodotto " & idProdotto & " sono state inserite " & n & " causali di protezione invece di " & qtaCausaliProtezioneDaInserire
            End If

            rec.MoveNext
        Wend
    End If
    rec.Close
    
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    
    MsgBox "Attivita' terminata."

End Sub

Private Sub PropagaCausaliProtezioneDaProdottoFittizioOld()
    Dim sql As String
    Dim rec As OraDynaset
    Dim rec1 As OraDynaset
    Dim rec2 As OraDynaset
    Dim qtaProdotti As Long
    Dim qtaPosti As Long
    Dim qtaTitoli As Long
    Dim qtaCausaliProtezione As Long
    Dim n As Long
    Dim qtaCausaliProtezioneInserite As Long
    
    qtaCausaliProtezioneInserite = 0
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(SPP.IDPRODOTTO) QTAPRODOTTI" & _
            " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP" & _
            " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
            " AND SP.IDSCELTAPRODOTTO =SPP.IDSCELTAPRODOTTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaProdotti = rec("QTAPRODOTTI")
    End If
    rec.Close
    
    ' POSTI PER I QUALI NON CI SONO TUTTE LE CAUSALI PROTEZIONI DEL PRODOTTO FITTIZIO NEI PRODOTTI
    sql = "SELECT P.IDPOSTO, A.CODICE, P.NOMEFILA, P.NOMEPOSTO" & _
            " FROM PROTEZIONEPOSTO PP, POSTO P, AREA A" & _
            " WHERE IDPRODOTTO = " & idProdottoFittizio & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & _
            " AND PP.IDPOSTO = P.IDPOSTO" & _
            " AND P.IDAREA = A.IDAREA" & _
            " AND PP.IDPOSTO NOT IN (" & _
            " SELECT IDPOSTO" & _
            " FROM (" & _
            " SELECT IDPOSTO, COUNT(*) QTAPRODOTTI" & _
            " FROM PROTEZIONEPOSTO" & _
            " WHERE IDPRODOTTO IN" & _
            " (" & _
            " select spp.idprodotto" & _
            " from sceltaprodotto sp, sceltaprodotto_prodotto spp" & _
            " Where sp.idOffertaPacchetto = " & idOffertaPacchettoSelezionata & _
            " and sp.idsceltaprodotto = spp.idsceltaprodotto" & _
            " )" & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & _
            " GROUP BY IDPOSTO" & _
            " )" & _
            " WHERE QTAPRODOTTI = " & qtaProdotti & _
            ")"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
        '  verifica se si puō impostare la causale di protezione sui prodotti dove manca
        ' cerca i prodotti dove manca
            sql = "SELECT IDPRODOTTO" & _
                    " FROM sceltaprodotto sp, sceltaprodotto_prodotto spp" & _
                    " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
                    " AND SP.IDSCELTAPRODOTTO = SPP.IDSCELTAPRODOTTO" & _
                    " AND IDPRODOTTO NOT IN (" & _
                    " SELECT IDPRODOTTO" & _
                    " FROM PROTEZIONEPOSTO" & _
                    " WHERE IDPOSTO = " & rec("IDPOSTO") & _
                    " AND IDPRODOTTO IN (" & _
                    " select spp.idprodotto" & _
                    " from sceltaprodotto sp, sceltaprodotto_prodotto spp" & _
                    " Where sp.idOffertaPacchetto = " & idOffertaPacchettoSelezionata & _
                    " and sp.idsceltaprodotto = spp.idsceltaprodotto" & _
                    " )" & _
                    " )"
            Set rec1 = ORADB.CreateDynaset(sql, 0&)
            If Not (rec1.BOF And rec1.EOF) Then
                rec1.MoveFirst
                ' verifica che non sia venduto
                qtaTitoli = 0
                sql = "SELECT COUNT(T.IDTITOLO) QTATITOLI" & _
                        " FROM TITOLO T, STATOTITOLO ST" & _
                        " WHERE T.IDPOSTO = " & rec("IDPOSTO") & _
                        " AND T.IDPRODOTTO = " & rec1("IDPRODOTTO") & _
                        " AND T.IDSTATOTITOLOCORRENTE = ST.IDSTATOTITOLO" & _
                        " AND ST.IDTIPOSTATOTITOLO < 6"
                Set rec2 = ORADB.CreateDynaset(sql, 0&)
                If Not (rec2.BOF And rec2.EOF) Then
                    rec2.MoveFirst
                    qtaTitoli = rec2("QTATITOLI")
                End If
                rec2.Close
                
                If (qtaTitoli > 0) Then
                    MsgBox "Impossibile propagare la protezione sul posto " & rec("CODICE") & " " & rec("NOMEFILA") & " " & rec("NOMEPOSTO") & ": esistono titoli venduti nel prodotto " & rec1("IDPRODOTTO")
                Else
                    ' verifica che non ci sia una causale di protezione
                    qtaCausaliProtezione = 0
                    sql = "SELECT COUNT(IDCAUSALEPROTEZIONE) QTACAUSALIPROTEZIONE" & _
                            " FROM PROTEZIONEPOSTO PP" & _
                            " WHERE PP.IDPOSTO = " & rec("IDPOSTO") & _
                            " AND PP.IDPRODOTTO = " & rec1("IDPRODOTTO")
                    Set rec2 = ORADB.CreateDynaset(sql, 0&)
                    If Not (rec2.BOF And rec2.EOF) Then
                        rec2.MoveFirst
                        qtaTitoli = rec2("QTACAUSALIPROTEZIONE")
                    End If
                    rec2.Close
                
                    If (qtaCausaliProtezione > 0) Then
                        MsgBox "Impossibile propagare la protezione sul posto " & rec("CODICE") & " " & rec("NOMEFILA") & " " & rec("NOMEPOSTO") & ": posto gia' protetto nel prodotto " & rec1("IDPRODOTTO")
                    Else
                        ' aggiunge la causale
                        sql = "INSERT INTO PROTEZIONEPOSTO (IDPROTEZIONEPOSTO, IDPOSTO, IDPRODOTTO, IDCAUSALEPROTEZIONE)" & _
                                " VALUES (SQ_PROTEZIONEPOSTO.NEXTVAL, " & rec("IDPOSTO") & ", " & rec1("IDPRODOTTO") & ", " & idCausaleProtezioneDedicata & ")"
                        SETAConnection.Execute sql, n, adCmdText
                        
                        If n = 1 Then
                            qtaCausaliProtezioneInserite = qtaCausaliProtezioneInserite + 1
                        Else
                            MsgBox "Non e' stato possibile inserire la causale di protezione nel posto " & rec("CODICE") & " " & rec("NOMEFILA") & " " & rec("NOMEPOSTO") & " del prodotto " & rec1("IDPRODOTTO")
                        End If
                                
                    End If
                End If
            
            End If
            
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA
    
    MsgBox qtaCausaliProtezioneInserite & " causali di protezione inserite su " & qtaProdotti & " prodotti"
    
End Sub

Private Function ControllaPostiLiberiSuProdotti() As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim qtaProdotti As Long
    Dim qtaPosti As Long
    
    ControllaPostiLiberiSuProdotti = True
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(SPP.IDPRODOTTO) QTAPRODOTTI" & _
            " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP" & _
            " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
            " AND SP.IDSCELTAPRODOTTO =SPP.IDSCELTAPRODOTTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaProdotti = rec("QTAPRODOTTI")
    End If
    rec.Close
    
    sql = "SELECT COUNT(IDPOSTO) QTAPOSTI" & _
            " FROM PROTEZIONEPOSTO" & _
            " WHERE IDPRODOTTO = " & idProdottoFittizio & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaPosti = rec("QTAPOSTI")
    End If
    rec.Close
    
    sql = "SELECT A.CODICE, NOMEFILA, NOMEPOSTO, COUNT(*) QTAPROTEZIONI" & _
            " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP, PROTEZIONEPOSTO PP, POSTO P, AREA A" & _
            " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
            " AND SP.IDSCELTAPRODOTTO = SPP.IDSCELTAPRODOTTO" & _
            " AND SPP.IDPRODOTTO = PP.IDPRODOTTO" & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & _
            " AND PP.IDPOSTO IN (SELECT IDPOSTO FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoFittizio & " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & ")" & _
            " AND PP.IDPOSTO = P.IDPOSTO" & _
            " AND P.IDAREA = A.IDAREA" & _
            " GROUP BY PP.IDPOSTO, A.CODICE, NOMEFILA, NOMEPOSTO" & _
            " ORDER BY A.CODICE, NOMEFILA, NOMEPOSTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If (rec.RecordCount < qtaPosti) Then
            MsgBox "Causali protezione non presenti su tutti i posti; prodotto fittizio: " & qtaPosti & ", prodotti: " & rec.RecordCount
            ControllaPostiLiberiSuProdotti = False
        End If
        rec.MoveFirst
        While Not rec.EOF
            If (rec("QTAPROTEZIONI") < qtaProdotti) Then
                MsgBox "Causali protezione non sufficienti sul posto " & rec("CODICE") & " " & rec("NOMEFILA") & " " & rec("NOMEPOSTO") & ": mancanti su " & qtaProdotti - rec("QTAPROTEZIONI") & " prodotti"
                ControllaPostiLiberiSuProdotti = False
            End If
            rec.MoveNext
        Wend
    Else
        MsgBox "Contingenti non presenti su prodotti"
        ControllaPostiLiberiSuProdotti = False
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA

End Function

Private Function ControllaCausaliProtezione() As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim qtaProdotti As Long
    Dim qtaPosti As Long
    
    ControllaCausaliProtezione = True
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT COUNT(SPP.IDPRODOTTO) QTAPRODOTTI" & _
            " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP" & _
            " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
            " AND SP.IDSCELTAPRODOTTO =SPP.IDSCELTAPRODOTTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaProdotti = rec("QTAPRODOTTI")
    End If
    rec.Close
    
    sql = "SELECT COUNT(IDPOSTO) QTAPOSTI" & _
            " FROM PROTEZIONEPOSTO" & _
            " WHERE IDPRODOTTO = " & idProdottoFittizio & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaPosti = rec("QTAPOSTI")
    End If
    rec.Close
    
    sql = "SELECT A.CODICE, NOMEFILA, NOMEPOSTO, COUNT(*) QTAPROTEZIONI" & _
            " FROM SCELTAPRODOTTO SP, SCELTAPRODOTTO_PRODOTTO SPP, PROTEZIONEPOSTO PP, POSTO P, AREA A" & _
            " WHERE SP.IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata & _
            " AND SP.IDSCELTAPRODOTTO = SPP.IDSCELTAPRODOTTO" & _
            " AND SPP.IDPRODOTTO = PP.IDPRODOTTO" & _
            " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & _
            " AND PP.IDPOSTO IN (SELECT IDPOSTO FROM PROTEZIONEPOSTO WHERE IDPRODOTTO = " & idProdottoFittizio & " AND IDCAUSALEPROTEZIONE = " & idCausaleProtezioneDedicata & ")" & _
            " AND PP.IDPOSTO = P.IDPOSTO" & _
            " AND P.IDAREA = A.IDAREA" & _
            " GROUP BY PP.IDPOSTO, A.CODICE, NOMEFILA, NOMEPOSTO" & _
            " ORDER BY A.CODICE, NOMEFILA, NOMEPOSTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If (rec.RecordCount < qtaPosti) Then
            MsgBox "Causali protezione non presenti su tutti i posti; prodotto fittizio: " & qtaPosti & ", prodotti: " & rec.RecordCount
            ControllaCausaliProtezione = False
        End If
        rec.MoveFirst
        While Not rec.EOF
            If (rec("QTAPROTEZIONI") < qtaProdotti) Then
                MsgBox "Causali protezione non sufficienti sul posto " & rec("CODICE") & " " & rec("NOMEFILA") & " " & rec("NOMEPOSTO") & ": mancanti su " & qtaProdotti - rec("QTAPROTEZIONI") & " prodotti"
                ControllaCausaliProtezione = False
            End If
            rec.MoveNext
        Wend
    Else
        MsgBox "Contingenti non presenti su prodotti"
        ControllaCausaliProtezione = False
    End If
    rec.Close
    
    Call ChiudiConnessioneBD_ORA

End Function

Private Sub cmdRimuoviProdotto_Click()
    Call RimuoviProdotti
    Call InizializzaListaProdottiDisponibili
    Call InizializzaListaProdottiSelezionati
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub optPrezziFissi_Click()
    If Not internalEvent Then
        Call TipoOffertaPacchetto_Update
    End If
End Sub

Private Sub optPrezziVariabili_Click()
    If Not internalEvent Then
        Call TipoOffertaPacchetto_Update
    End If
End Sub

Private Sub optPrezziRateizzabili_Click()
    If Not internalEvent Then
        Call TipoOffertaPacchetto_Update
    End If
End Sub

Private Sub TipoOffertaPacchetto_Update()
'    tipoOffertaPacchettoSelezionata = TOP_NON_SPECIFICATO
'    If optPrezziRateizzabili.Value = True Then
'        tipoOffertaPacchettoSelezionata = TOP_PREZZO_RATEIZZABILE
'    ElseIf optPrezziFissi.Value = True Then
'        tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO
'    ElseIf optPrezziVariabili.Value = True Then
'        tipoOffertaPacchettoSelezionata = TOP_PREZZO_VARIABILE
'    End If
    tipoOffertaPacchettoSelezionata = TOP_PREZZO_FISSO
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstSceltaProdotti_Click()
    Call SelezionaSceltaProdotti
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim prodotto As clsElementoLista
    Dim condizioneSql As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE OFFERTAPACCHETTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeOffertaPacchetto) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneOffertaPacchetto) & ","
    sql = sql & " IDTIPOOFFERTAPACCHETTO = " & tipoOffertaPacchettoSelezionata
    sql = sql & " WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Public Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idRappresentazioneFittizia As Long
    
    Call ApriConnessioneBD

    Call SbloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idOffertaPacchettoSelezionata, isOffertaPacchettoBloccataDaUtente)
    
    idProdottoFittizio = TrovaIdProdottoFittizio
    If idProdottoFittizio <> idNessunElementoSelezionato Then
        idRappresentazioneFittizia = TrovaIdRappresentazioneFittizia
        
        sql = "DELETE FROM PRODOTTO_RAPPRESENTAZIONE"
        sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRappresentazioneFittizia
        SETAConnection.Execute sql, n, adCmdText
        
        sql = "DELETE FROM SPETTACOLO"
        sql = sql & " WHERE IDSPETTACOLO = ("
        sql = sql & " SELECT IDSPETTACOLO"
        sql = sql & " FROM RAPPRESENTAZIONE"
        sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRappresentazioneFittizia
        sql = sql & ")"
        SETAConnection.Execute sql, n, adCmdText
        
        sql = "DELETE FROM RAPPRESENTAZIONE"
        sql = sql & " WHERE IDRAPPRESENTAZIONE = " & idRappresentazioneFittizia
        SETAConnection.Execute sql, n, adCmdText
    End If
        
    sql = "DELETE FROM SCELTAPRODOTTO_PRODOTTO"
    sql = sql & " WHERE IDSCELTAPRODOTTO IN ("
    sql = sql & " SELECT IDSCELTAPRODOTTO FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM OFFERTAPACCHETTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD

End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Function valoriCampiOK() As Boolean
    Dim numero As Long
    Dim listaNonConformitā As Collection
'    Dim listaValoriDaConfermare As Collection

    valoriCampiOK = True
    Set listaNonConformitā = New Collection
'    Set listaValoriDaConfermare = New Collection
    nomeOffertaPacchetto = Trim(txtNome.Text)
    If ViolataUnicitā("OFFERTAPACCHETTO", "NOME = " & SqlStringValue(nomeOffertaPacchetto), "IDOFFERTAPACCHETTO <> " & idOffertaPacchettoSelezionata, _
        "", "", "") Then
        valoriCampiOK = False
        Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeOffertaPacchetto) & _
            " č giā presente in DB;")
    End If
    descrizioneOffertaPacchetto = Trim(txtDescrizione.Text)
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
'    Else
'        If listaValoriDaConfermare.count > 0 Then
'            Call frmMessaggio.Visualizza("ConfermaValori", ArgomentoMessaggio(listaValoriDaConfermare))
'            If frmMessaggio.exitCode = EC_ANNULLA Then
'                ValoriCampiOK = False
'            End If
'        End If
    End If

End Function

Private Sub confermaSceltaProdotti()
    cmdConfermaSceltaProdotti.Enabled = False
    Call verificaConsistenzaSceltaProdotti
    idProdottoDaClonare = TrovaIdProdottoDaClonare
    Call creaProdottoFittizio
    Call InizializzaListaTariffe
    Call InizializzaListaSuperaree
    cmdConfermaTariffeSuperaree.Enabled = True
    lstTariffe.Enabled = True
    lstSuperaree.Enabled = True
End Sub

Private Sub verificaConsistenzaSceltaProdotti()

End Sub

Private Sub creaProdottoFittizio()
    If IsClonazioneProdottoOK(idProdottoDaClonare, _
                                        "PROD FITT " & nomeOffertaPacchetto, _
                                        "PROD FITT " & nomeOffertaPacchetto, _
                                        "PROD FITT " & nomeOffertaPacchetto, _
                                        "PF " & idOffertaPacchettoSelezionata, _
                                        CStr(idOffertaPacchettoSelezionata), _
                                        False, _
                                        idNessunElementoSelezionato, _
                                        idProdottoDaClonare, _
                                        idNessunElementoSelezionato, _
                                        idNessunElementoSelezionato, _
                                        idProdottoDaClonare, _
                                        idNessunElementoSelezionato) Then
        idProdottoFittizio = TrovaIdNuovoProdottoFittizio
        Call aggiornaOffertaPacchettoConIdProdottoFittizio
        Call aggiungiRappresentazioneAProdottoFittizio
        Call abilitaCausaleDedicataSuProdottoFittizio
    Else
        MsgBox "KO"
    End If

End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = "l'Offerta pacchetto č in lavorazione da parte di un altro utente."

    If valoriCampiOK Then
        Call SetGestioneExitCode(EC_CONFERMA)
        If isRecordEditabile Then
            Select Case modalitaFormCorrente
                Case A_NUOVO
'                    Call InserisciNellaBaseDati
'                    Call BloccaDominioPerUtente(CCDA_OFFERTA_PACCHETTO, idOffertaPacchettoSelezionata, isOffertaPacchettoBloccataDaUtente)
'                    Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
'                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_OFFERTA_PACCHETTO, CCDA_OFFERTA_PACCHETTO, "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata, idOffertaPacchettoSelezionata)
'                    pacchettoSalvatoInBD = True
                    'Call AggiornaAbilitazioneControlli
                    Unload Me
                Case A_MODIFICA
'                    If IsProdottoEditabile(idOffertaPacchettoSelezionata, causaNonEditabilita) Then
                    If isOffertaPacchettoBloccataDaUtente Then
'                        isConfigurabile = True
'                        If tipoStatoProdotto = TSP_ATTIVO Then
'                            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
'                            If frmMessaggio.exitCode <> EC_CONFERMA Then
'                                isConfigurabile = False
'                            End If
'                        End If
'                        If isConfigurabile Then
                            Call AggiornaNellaBaseDati
                            Call frmMessaggio.Visualizza("NotificaModificaDati")
                            Call ScriviLog(CCTA_MODIFICA, CCDA_OFFERTA_PACCHETTO, CCDA_OFFERTA_PACCHETTO, "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata, idOffertaPacchettoSelezionata)
                            Call Annulla
'                        End If
                    Else
                        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
                    End If
                Case A_ELIMINA
                    Call EliminaDallaBaseDati
                    Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
                    Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OFFERTA_PACCHETTO, CCDA_OFFERTA_PACCHETTO, "IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata, idOffertaPacchettoSelezionata)
                    Call Annulla
            End Select
        End If
    End If
End Sub

Private Sub AggiornaListaSceltaProdotti()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    lstSceltaProdotti.Clear
    
    Call ApriConnessioneBD_ORA
    sql = "SELECT IDSCELTAPRODOTTO, NOME FROM SCELTAPRODOTTO WHERE IDOFFERTAPACCHETTO = " & idOffertaPacchettoSelezionata
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstSceltaProdotti.AddItem(rec("NOME"), i)
            lstSceltaProdotti.ItemData(i) = rec("IDSCELTAPRODOTTO")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub InizializzaLst(lst As listBox, sql As String)
    Dim rec As OraDynaset
    Dim i As Long
    
    lst.Clear
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lst.AddItem(rec("NOME"), i)
            lst.ItemData(i) = rec("ID")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

End Sub

Private Sub InizializzaListConSelezioni(lst As listBox, sql As String, sqlSel As String)
    Dim rec As OraDynaset
    Dim i As Long
    Dim idElementoSelezionato As Long
    
    Call InizializzaLst(lst, sql)
    
    Set rec = ORADB.CreateDynaset(sqlSel, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idElementoSelezionato = rec("ID")
            For i = 0 To lst.ListCount - 1
                If (lst.ItemData(i) = idElementoSelezionato) Then
                    lst.Selected(i) = True
                End If
            Next i
            rec.MoveNext
        Wend
    End If
    rec.Close

End Sub

Private Sub SelezionaSceltaProdotti()
    Dim i As Integer
Dim sql As String
    
    For i = 1 To lstSceltaProdotti.ListCount
        If lstSceltaProdotti.Selected(i - 1) Then
            idSceltaProdottoSelezionata = lstSceltaProdotti.ItemData(i - 1)
        End If
    Next i
    
    quantitaProdottiSelezionabili = TrovaQuantitaProdottiSelezionabili()
    
    Call InizializzaListaProdottiDisponibili
    Call InizializzaListaProdottiSelezionati
    cmdAggiungiProdotto.Enabled = True
    cmdRimuoviProdotto.Enabled = True
End Sub

Private Sub InizializzaListaProdottiDisponibili()
    Dim sql As String
    
    sql = " SELECT P.IDPRODOTTO ID, P.NOME"
    sql = sql & " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R,"
    sql = sql & " ("
    sql = sql & " SELECT P.IDPRODOTTO"
    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP, PRODOTTO P"
    sql = sql & " WHERE SPP.IDSCELTAPRODOTTO = " & idSceltaProdottoSelezionata
    sql = sql & " AND SPP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " ) PS"
    sql = sql & " Where p.idOrganizzazione = " & idOrganizzazioneSelezionata
    sql = sql & " AND P.IDCLASSEPRODOTTO = 1"
    sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO"
    sql = sql & " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
    sql = sql & " AND R.DATAORAINIZIO > SYSDATE"
    sql = sql & " AND P.IDPRODOTTO = PS.IDPRODOTTO(+)"
    sql = sql & " AND PS.IDPRODOTTO IS NULL"
    sql = sql & " ORDER BY P.NOME"
    Call InizializzaLst(lstProdottiDisponibili, sql)
End Sub

Private Sub InizializzaListaProdottiSelezionati()
    Dim sql As String
    
    sql = "SELECT P.IDPRODOTTO ID, P.NOME"
    sql = sql & " FROM SCELTAPRODOTTO_PRODOTTO SPP, PRODOTTO P"
    sql = sql & " WHERE SPP.IDSCELTAPRODOTTO = " & idSceltaProdottoSelezionata
    sql = sql & " AND SPP.IDPRODOTTO = P.IDPRODOTTO"
    sql = sql & " ORDER BY P.NOME"
    Call InizializzaLst(lstProdottiSelezionati, sql)
End Sub

Private Sub InizializzaListaTariffe()
    Dim sql As String
    Dim sqlSelezionate As String
    
    sql = " SELECT TF.IDTARIFFA ID, TF.CODICE || ' - ' || TF.NOME NOME"
    sql = sql & " FROM TARIFFA TF"
    sql = sql & " WHERE TF.IDPRODOTTO = " & TrovaIdProdottoDaClonare
    sql = sql & " ORDER BY TF.CODICE"
    
    sqlSelezionate = "SELECT DISTINCT TF.IDTARIFFA ID, TF.CODICE || ' - ' || TF.NOME NOME"
    sqlSelezionate = sqlSelezionate & " FROM TARIFFA TF, PREZZOTITOLOPRODOTTO PTP, AREA SA, TARIFFA TFF"
    sqlSelezionate = sqlSelezionate & " WHERE TF.IDPRODOTTO = " & TrovaIdProdottoDaClonare
    sqlSelezionate = sqlSelezionate & " AND TF.IDTARIFFA = PTP.IDTARIFFA"
    sqlSelezionate = sqlSelezionate & " AND PTP.IDAREA = SA.IDAREA"
    sqlSelezionate = sqlSelezionate & " AND TF.CODICE = TFF.CODICE"
    sqlSelezionate = sqlSelezionate & " AND TFF.IDPRODOTTO = " & idProdottoFittizio
    
    Call InizializzaListConSelezioni(lstTariffe, sql, sqlSelezionate)
End Sub

Private Sub InizializzaListaSuperaree()
    Dim sql As String
    Dim sqlSelezionate As String
    
    sql = " SELECT DISTINCT SA.IDAREA ID, SA.NOME NOME"
    sql = sql & " FROM TARIFFA TF, PREZZOTITOLOPRODOTTO PTP, AREA SA"
    sql = sql & " WHERE TF.IDPRODOTTO = " & TrovaIdProdottoDaClonare
    sql = sql & " AND TF.IDTARIFFA = PTP.IDTARIFFA"
    sql = sql & " AND PTP.IDAREA = SA.IDAREA"
    sql = sql & " ORDER BY SA.NOME"
    
    sqlSelezionate = " SELECT DISTINCT SA.IDAREA ID, SA.NOME NOME"
    sqlSelezionate = sqlSelezionate & " FROM PREZZOTITOLOPRODOTTO PTP, AREA SA"
    sqlSelezionate = sqlSelezionate & " WHERE PTP.IDPRODOTTO = " & idProdottoFittizio
    sqlSelezionate = sqlSelezionate & " AND PTP.IDAREA = SA.IDAREA"
    
    Call InizializzaListConSelezioni(lstSuperaree, sql, sqlSelezionate)
End Sub

Private Sub cmdOperatori_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraOperatoriPacchetto
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraOperatoriPacchetto()
    Call CaricaFormConfigurazionePacchettoOperatori
End Sub

Private Sub CaricaFormConfigurazionePacchettoOperatori()
    Call frmConfigurazionePacchettoOperatori.SetIdOffertaPacchettoSelezionata(idOffertaPacchettoSelezionata)
    Call frmConfigurazionePacchettoOperatori.SetNomeOffertaPacchettoSelezionata(nomeOffertaPacchetto)
    Call frmConfigurazionePacchettoOperatori.SetTipoOffertaPacchettoSelezionata(tipoOffertaPacchettoSelezionata)
'    Call frmConfigurazionePacchettoTariffe.SetNumeroProdottiAssociati(numeroProdottiAssociati)
    Call frmConfigurazionePacchettoOperatori.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePacchettoOperatori.SetModalitāForm(modalitaFormCorrente)
    Call frmConfigurazionePacchettoOperatori.Init
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Integer

    Call ApriConnessioneBD_ORA

    sql = strSQL

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

    Call ChiudiConnessioneBD_ORA

End Sub

' Cosa molto brutta: questa funzione e' "copiata" da funzioni comuni.....
Public Function IsImportazioneTariffePeriodiPrezziOK(idProdottoCorrente As Long, idProdottoMaster As Long, listaIn As String) As Boolean
    Dim sql1 As String
    Dim sql2 As String
    Dim rec1 As New ADODB.Recordset
    Dim count As Integer
    Dim righeAggiornate As Long
    Dim coppiaId As clsCoppiaIdent
    Dim listaCoppieIdentificatoriTariffe As New Collection
    Dim listaCoppieIdentificatoriPeriodiCommerciali As New Collection
    Dim idNuovaTariffa As Long
    Dim idNuovoPeriodoCommerciale As Long
    Dim idNuovoPrezzo As Long
    Dim n As Long
    
' inserimento in TARIFFA
    sql1 = "SELECT IDTARIFFA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
    sql1 = sql1 & " IDTIPOTARIFFA, IDORGANIZZAZIONE, CODICE, IDMODALITAEMISSIONE,"
    sql1 = sql1 & " IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC, APPLICABILEAPOSTOCONRINUNCIA,"
    sql1 = sql1 & " DATANASCITATITOLAREMINIMA, DATANASCITATITOLAREMASSIMA, CAMBIOUTILIZZATOREAPPLICABILE, NUMEROMASSIMOTITOLIPERACQ,"
    sql1 = sql1 & " IDPROMOZIONE, IVAPREASSOLTAOBBLIGATORIA, TITOLOCORPORATEOBBLIGATORIO"
    sql1 = sql1 & " FROM TARIFFA"
    sql1 = sql1 & " WHERE IDPRODOTTO = " & idProdottoMaster
    sql1 = sql1 & " AND IDTARIFFA IN " & listaIn
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovaTariffa = OttieniIdentificatoreDaSequenza("SQ_TARIFFA")
            Set coppiaId = New clsCoppiaIdent
            coppiaId.idVecchio = rec1("IDTARIFFA").Value
            coppiaId.idNuovo = idNuovaTariffa
            Call listaCoppieIdentificatoriTariffe.Add(coppiaId)
            
            sql1 = "INSERT INTO TARIFFA("
            sql1 = sql1 & " IDTARIFFA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, DESCRIZIONEPOS,"
            sql1 = sql1 & " IDTIPOTARIFFA, IDORGANIZZAZIONE, IDPRODOTTO, IDMODALITAEMISSIONE,"
            sql1 = sql1 & " CODICE, IDAMBITOAPPLICABILITATARIFFA, IDCLASSETARIFFACONTROLLOACC, APPLICABILEAPOSTOCONRINUNCIA,"
            sql1 = sql1 & " DATANASCITATITOLAREMINIMA, DATANASCITATITOLAREMASSIMA, CAMBIOUTILIZZATOREAPPLICABILE, NUMEROMASSIMOTITOLIPERACQ,"
            sql1 = sql1 & " IDPROMOZIONE, IVAPREASSOLTAOBBLIGATORIA, TITOLOCORPORATEOBBLIGATORIO"
            sql1 = sql1 & ") VALUES ("
            sql1 = sql1 & idNuovaTariffa & ","
            sql1 = sql1 & " '" & rec1("NOME") & "', "
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONE")), "NULL", " '" & rec1("DESCRIZIONE") & "'") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONEALTERNATIVA")), "NULL", " '" & rec1("DESCRIZIONEALTERNATIVA") & "'") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONEPOS")), "NULL", " '" & rec1("DESCRIZIONEPOS") & "'") & ", "
            sql1 = sql1 & rec1("IDTIPOTARIFFA") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("IDORGANIZZAZIONE")), "NULL", rec1("IDORGANIZZAZIONE")) & ", "
            sql1 = sql1 & idProdottoCorrente & ", "
            sql1 = sql1 & rec1("IDMODALITAEMISSIONE") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("CODICE")), "NULL)", " '" & rec1("CODICE") & "', ")
            sql1 = sql1 & rec1("IDAMBITOAPPLICABILITATARIFFA") & ", "
            sql1 = sql1 & rec1("IDCLASSETARIFFACONTROLLOACC") & ", "
            sql1 = sql1 & rec1("APPLICABILEAPOSTOCONRINUNCIA") & ", "
            
            If (IsNull(rec1("DATANASCITATITOLAREMINIMA"))) Then
                sql1 = sql1 & "NULL" & ", "
            Else
                sql1 = sql1 & SqlDateValue(rec1("DATANASCITATITOLAREMINIMA")) & ", "
            End If
            If (IsNull(rec1("DATANASCITATITOLAREMASSIMA"))) Then
                sql1 = sql1 & "NULL" & ", "
            Else
                sql1 = sql1 & SqlDateValue(rec1("DATANASCITATITOLAREMASSIMA")) & ", "
            End If

            sql1 = sql1 & rec1("CAMBIOUTILIZZATOREAPPLICABILE") & ", "
            sql1 = sql1 & IIf(IsNull(rec1("NUMEROMASSIMOTITOLIPERACQ")), "NULL", rec1("NUMEROMASSIMOTITOLIPERACQ")) & ", "
            sql1 = sql1 & IIf(IsNull(rec1("IDPROMOZIONE")), "NULL", rec1("IDPROMOZIONE")) & ", "
            sql1 = sql1 & rec1("IVAPREASSOLTAOBBLIGATORIA") & ", "
            sql1 = sql1 & rec1("TITOLOCORPORATEOBBLIGATORIO") & ")"
            
            SETAConnection.Execute sql1, righeAggiornate, adCmdText
            
            ' inserimento in OPERATORE_TARIFFA
            n = 0
            sql2 = "INSERT INTO OPERATORE_TARIFFA (IDOPERATORE, IDTARIFFA)"
            sql2 = sql2 & " (SELECT IDOPERATORE, "
            sql2 = sql2 & idNuovaTariffa & " FROM OPERATORE_TARIFFA WHERE"
            sql2 = sql2 & " IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText
            
            ' inserimento in TARIFFA_TIPOTERMINALE
            n = 0
            sql2 = "INSERT INTO TARIFFA_TIPOTERMINALE (IDTIPOTERMINALE, IDTASTOTIPOTERMINALE,"
            sql2 = sql2 & " IDTARIFFA) (SELECT IDTIPOTERMINALE, IDTASTOTIPOTERMINALE, "
            sql2 = sql2 & idNuovaTariffa & " FROM TARIFFA_TIPOTERMINALE WHERE"
            sql2 = sql2 & " IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            ' inserimento in CLASSEPV_TARIFFA
            n = 0
            sql2 = "INSERT INTO CLASSEPV_TARIFFA (IDCLASSEPUNTOVENDITA, IDTARIFFA)"
            sql2 = sql2 & "(SELECT IDCLASSEPUNTOVENDITA, " & idNuovaTariffa
            sql2 = sql2 & " FROM CLASSEPV_TARIFFA"
            sql2 = sql2 & " WHERE IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            ' inserimento in TAR_CATEGORIACARTADIPAGAMENTO
            n = 0
            sql2 = "INSERT INTO TAR_CATEGORIACARTADIPAGAMENTO (IDTARIFFA, IDCATEGORIACARTADIPAGAMENTO)"
            sql2 = sql2 & " (SELECT " & idNuovaTariffa & ", IDCATEGORIACARTADIPAGAMENTO"
            sql2 = sql2 & " FROM TAR_CATEGORIACARTADIPAGAMENTO"
            sql2 = sql2 & " WHERE IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            ' inserimento in TARIFFA_TIPOSUPPORTODIGITALE
            n = 0
            sql2 = "INSERT INTO TARIFFA_TIPOSUPPORTODIGITALE (IDTARIFFA, IDTIPOSUPPORTO)"
            sql2 = sql2 & " (SELECT " & idNuovaTariffa & ", IDTIPOSUPPORTO"
            sql2 = sql2 & " FROM TARIFFA_TIPOSUPPORTODIGITALE"
            sql2 = sql2 & " WHERE IDTARIFFA = " & coppiaId.idVecchio & ")"
            SETAConnection.Execute sql2, n, adCmdText

            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
' agggiorna tariffe_riferimento
    sql1 = "SELECT IDTARIFFA, IDTARIFFA_RIFERIMENTO"
    sql1 = sql1 & " FROM TARIFFA"
    sql1 = sql1 & " WHERE IDPRODOTTO = " & idProdottoMaster
    sql1 = sql1 & " AND IDTARIFFA_RIFERIMENTO IS NOT NULL"
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            n = 0
            sql2 = "UPDATE TARIFFA SET IDTARIFFA_RIFERIMENTO = " & OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA_RIFERIMENTO"))
            sql2 = sql2 & " WHERE IDTARIFFA= " & OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA"))
            SETAConnection.Execute sql2, n, adCmdText

            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
' inserimento in PERIODOCOMMERCIALE
    sql1 = "SELECT IDPERIODOCOMMERCIALE,"
    sql1 = sql1 & " NOME, DESCRIZIONE,"
    sql1 = sql1 & " DATAORAINIZIO, DATAORAFINEPREVENDITA, DATAORAFINE"
    sql1 = sql1 & " FROM PERIODOCOMMERCIALE WHERE IDPRODOTTO = " & idProdottoMaster
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovoPeriodoCommerciale = OttieniIdentificatoreDaSequenza("SQ_PERIODOCOMMERCIALE")
            Set coppiaId = New clsCoppiaIdent
            coppiaId.idVecchio = rec1("IDPERIODOCOMMERCIALE").Value
            coppiaId.idNuovo = idNuovoPeriodoCommerciale
            Call listaCoppieIdentificatoriPeriodiCommerciali.Add(coppiaId)
            
            sql1 = "INSERT INTO PERIODOCOMMERCIALE("
            sql1 = sql1 & " IDPERIODOCOMMERCIALE,"
            sql1 = sql1 & " NOME, DESCRIZIONE,"
            sql1 = sql1 & " DATAORAINIZIO, DATAORAFINEPREVENDITA, DATAORAFINE, IDPRODOTTO"
            sql1 = sql1 & ") VALUES ("
            sql1 = sql1 & idNuovoPeriodoCommerciale & ","
            sql1 = sql1 & " '" & rec1("NOME") & "',"
            sql1 = sql1 & IIf(IsNull(rec1("DESCRIZIONE")), "NULL", " '" & rec1("DESCRIZIONE") & "'") & ", "
            sql1 = sql1 & SqlDateTimeValue(rec1("DATAORAINIZIO")) & ","
            sql1 = sql1 & SqlDateTimeValue(rec1("DATAORAFINEPREVENDITA")) & ","
            sql1 = sql1 & SqlDateTimeValue(rec1("DATAORAFINE")) & ","
            sql1 = sql1 & idProdottoCorrente & ")"
            SETAConnection.Execute sql1, righeAggiornate, adCmdText

            rec1.MoveNext
        Wend
        rec1.Close

' inserimento in PREZZOTITOLOPRODOTTO
        sql1 = "SELECT"
        sql1 = sql1 & " IDPREZZOTITOLOPRODOTTO,"
        sql1 = sql1 & " IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
        sql1 = sql1 & " IDTARIFFA,"
        sql1 = sql1 & " IDAREA,"
        sql1 = sql1 & " IDTIPOTARIFFASIAE,"
        sql1 = sql1 & " IDPERIODOCOMMERCIALE"
        sql1 = sql1 & " FROM PREZZOTITOLOPRODOTTO WHERE IDPRODOTTO = " & idProdottoMaster
        sql1 = sql1 & " AND IDTARIFFA IN " & listaIn
        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec1.BOF And rec1.EOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idNuovoPrezzo = OttieniIdentificatoreDaSequenza("SQ_PREZZOTITOLOPRODOTTO")
                idNuovaTariffa = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA"))
                idNuovoPeriodoCommerciale = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriPeriodiCommerciali, rec1("IDPERIODOCOMMERCIALE"))
                
                sql1 = "INSERT INTO PREZZOTITOLOPRODOTTO("
                sql1 = sql1 & " IDPREZZOTITOLOPRODOTTO,"
                sql1 = sql1 & " IMPORTOBASE, IMPORTOSERVIZIOPREVENDITA,"
                sql1 = sql1 & " IDPRODOTTO,"
                sql1 = sql1 & " IDTARIFFA,"
                sql1 = sql1 & " IDAREA,"
                sql1 = sql1 & " IDTIPOTARIFFASIAE,"
                sql1 = sql1 & " IDPERIODOCOMMERCIALE"
                sql1 = sql1 & ") VALUES ("
                sql1 = sql1 & idNuovoPrezzo & ","
                sql1 = sql1 & StringaConPuntoDecimale(rec1("IMPORTOBASE")) & ", "
                sql1 = sql1 & StringaConPuntoDecimale(rec1("IMPORTOSERVIZIOPREVENDITA")) & ", "
                sql1 = sql1 & idProdottoCorrente & ", "
                sql1 = sql1 & idNuovaTariffa & ", "
                sql1 = sql1 & rec1("IDAREA") & ", "
                sql1 = sql1 & rec1("IDTIPOTARIFFASIAE") & ", "
                sql1 = sql1 & idNuovoPeriodoCommerciale & ")"
                SETAConnection.Execute sql1, righeAggiornate, adCmdText
    
                rec1.MoveNext
            Wend
        End If
        rec1.Close
    End If

' inserimento in UTILIZZOLAYOUTSUPPORTOCPV (IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO, IDTIPOTERMINALE)
    sql1 = "SELECT"
    sql1 = sql1 & " ULS.IDAREA, ULS.IDTARIFFA, ULS.IDTIPOSUPPORTO, ULS.IDLAYOUTSUPPORTO"
    sql1 = sql1 & " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T"
    sql1 = sql1 & " WHERE ULS.IDTARIFFA=T.IDTARIFFA AND T.IDPRODOTTO = " & idProdottoMaster
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idNuovaTariffa = OttieniNuovoIdNellaLista(listaCoppieIdentificatoriTariffe, rec1("IDTARIFFA"))

            sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV("
            sql1 = sql1 & " IDAREA,"
            sql1 = sql1 & " IDTARIFFA,"
            sql1 = sql1 & " IDTIPOSUPPORTO,"
            sql1 = sql1 & " IDLAYOUTSUPPORTO"
            sql1 = sql1 & ") VALUES ("
            sql1 = sql1 & rec1("IDAREA") & ", "
            sql1 = sql1 & idNuovaTariffa & ", "
            sql1 = sql1 & rec1("IDTIPOSUPPORTO") & ", "
            sql1 = sql1 & rec1("IDLAYOUTSUPPORTO") & ")"

            rec1.MoveNext
        Wend
    End If
    rec1.Close

    IsImportazioneTariffePeriodiPrezziOK = True
End Function

Private Function OttieniNuovoIdNellaLista(lista As Collection, vecchioId As Long) As Long
Dim i As Integer
Dim trovato As Boolean

    i = 1
    trovato = False
    
    OttieniNuovoIdNellaLista = idNessunElementoSelezionato
    While i <= lista.count And Not trovato
        If lista(i).idVecchio = vecchioId Then
            trovato = True
            OttieniNuovoIdNellaLista = lista(i).idNuovo
        End If
        i = i + 1
    Wend

End Function

