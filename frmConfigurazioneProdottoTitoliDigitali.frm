VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoTitoliDigitali 
   Caption         =   "Configurazione abilitazioni per titoli digitali"
   ClientHeight    =   5985
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   ScaleHeight     =   5985
   ScaleWidth      =   11955
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   8
      Top             =   4920
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10140
      TabIndex        =   7
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8400
      TabIndex        =   6
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   180
      TabIndex        =   3
      Top             =   4920
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstTipiSupportiSIAEDigitali 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1980
      Left            =   3720
      Style           =   1  'Checkbox
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1860
      Width           =   3885
   End
   Begin VB.ListBox lstSuperaree 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   180
      MultiSelect     =   2  'Extended
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1860
      Width           =   2670
   End
   Begin VB.ListBox lstTipiSupportiDigitali 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1980
      Left            =   7920
      Style           =   1  'Checkbox
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1860
      Width           =   3915
   End
   Begin VB.Label Label2 
      Caption         =   $"frmConfigurazioneProdottoTitoliDigitali.frx":0000
      Height          =   315
      Left            =   240
      TabIndex        =   20
      Top             =   1080
      Width           =   10635
   End
   Begin VB.Label lblConfigurazioneSuperarea 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   180
      TabIndex        =   19
      Top             =   3960
      Width           =   11595
   End
   Begin VB.Label Label1 
      Caption         =   "Se per una superarea non esiste alcuna riga significa che tutti i tipi supporto digitali sono consentiti sulla superarea."
      Height          =   315
      Left            =   240
      TabIndex        =   18
      Top             =   840
      Width           =   10635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10140
      TabIndex        =   17
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8400
      TabIndex        =   16
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Abilitazioni supporti digitali per superarea"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblSuperaree 
      Alignment       =   2  'Center
      Caption         =   "Superaree"
      Height          =   195
      Left            =   240
      TabIndex        =   14
      Top             =   1620
      Width           =   2595
   End
   Begin VB.Label lblTipiSupportiSIAE 
      Alignment       =   2  'Center
      Caption         =   "Tipi supporto SIAE digitali"
      Height          =   195
      Left            =   3720
      TabIndex        =   13
      Top             =   1620
      Width           =   3855
   End
   Begin VB.Label lblTipiSupporto 
      Alignment       =   2  'Center
      Caption         =   "Tipi supporto digitale"
      Height          =   195
      Left            =   8040
      TabIndex        =   12
      Top             =   1620
      Width           =   3735
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoTitoliDigitali"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private listaSuperaree As Collection
Private listaSuperareeSelezionate As Collection

Private stringaListaSuperareeSelezionate As String
Private quantitaSuperareeSelezionate As Long

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
'    listaOperatoriDisponibiliNonDiRicevitoria.Enabled = criteriSelezioneImpostati
'    lblDisponibili.Enabled = criteriSelezioneImpostati
'    listaOperatoriSelezionatiNonDiRicevitoria.Enabled = criteriSelezioneImpostati
'    lblSelezionati.Enabled = criteriSelezioneImpostati
'    cmdCarica.Enabled = criteriSelezioneImpostati
'    cmdDidsponibile.Enabled = criteriSelezioneImpostati
'    cmdSelezionato.Enabled = criteriSelezioneImpostati
'    cmdSvuotaDisponibili.Enabled = criteriSelezioneImpostati
'    cmdSvuotaSelezionati.Enabled = criteriSelezioneImpostati
'    cmdConferma.Enabled = criteriSelezioneImpostati
'    If gestioneExitCode <> EC_NON_SPECIFICATO Then
'        Call listaOperatoriDisponibiliNonDiRicevitoria.Clear
'        Call listaOperatoriSelezionatiNonDiRicevitoria.Clear
'    End If
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Fine"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLst_Superaree
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    'rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    'numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub CaricaValoriLst_OperatoriNonDiRicevitoria_Ricevitorie_Disponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim chiaveRicevitoria As String
    Dim operatoreCorrente As clsElementoLista
    Dim ricevitoriaCorrente As clsElementoLista
    
'    Call ApriConnessioneBD
'
'    Set listaOperatoriDisponibiliNonDiRicevitoria = New Collection
'
'    'lista in alto a sinistra
'    sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
'    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO"
'    sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
'    sql = sql + " AND O.ABILITATO = " & VB_VERO
'    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'    sql = sql + " AND OP.IDOPERATORE IS NULL "
'    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
'    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA AND PV.IDTIPOPUNTOVENDITA = 1" ' SPECIALE
'    sql = sql + " UNION"
'    sql = sql + " SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
'    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO"
'    sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
'    sql = sql + " AND O.ABILITATO = " & VB_VERO
'    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'    sql = sql + " AND OP.IDOPERATORE IS NULL"
'    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
'    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'    sql = sql + " AND O.IDPUNTOVENDITA IS NULL"
'    sql = sql + " ORDER BY USERNAME"
'
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set operatoreCorrente = New clsElementoLista
'            operatoreCorrente.nomeElementoLista = rec("USERNAME")
'            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
'            operatoreCorrente.idElementoLista = rec("ID").Value
'            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
'            Call listaOperatoriDisponibiliNonDiRicevitoria.Add(operatoreCorrente, chiaveOperatore)
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
''    If operatoriPOSeTOTEMespliciti = True Then
''        sql = sql + " UNION"
'
'    Set listaRicevitorieDisponibili = New Collection
'
'        'lista in basso
'    sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
'    sql = sql + " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA PV, OPERATORE_ORGANIZZAZIONE OO, TERMINALE T"
'    sql = sql + " WHERE OP.IDPRODOTTO(+) = " & idProdottoSelezionato
'    sql = sql + " AND O.ABILITATO = " & VB_VERO
'    sql = sql + " AND O.IDPERFEZIONATORE IS NULL "
'    sql = sql + " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'    sql = sql + " AND (T.IDTIPOTERMINALE = 5 AND PV.IDTIPOPUNTOVENDITA = 2 OR T.IDTIPOTERMINALE = 6 AND PV.IDTIPOPUNTOVENDITA = 3)" 'POS e RICEVITORIA oppure TOTEM e filiale
'    sql = sql + " AND OP.IDOPERATORE IS NULL"
'    sql = sql + " AND PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA"
'    sql = sql + " AND O.IDOPERATORE = OP.IDOPERATORE(+)"
'    sql = sql + " AND O.IDOPERATORE = OO.IDOPERATORE"
'    sql = sql + " AND O.IDPUNTOVENDITA = PV.IDPUNTOVENDITA"
'    sql = sql + " ORDER BY USERNAME"
'    'End If
'
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            Set ricevitoriaCorrente = New clsElementoLista
'            ricevitoriaCorrente.nomeElementoLista = rec("USERNAME")
'            ricevitoriaCorrente.descrizioneElementoLista = rec("USERNAME")
'            ricevitoriaCorrente.idElementoLista = rec("ID").Value
'            chiaveRicevitoria = ChiaveId(ricevitoriaCorrente.idElementoLista)
'            Call listaRicevitorieDisponibili.Add(ricevitoriaCorrente, chiaveRicevitoria)
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
'    Call ChiudiConnessioneBD
'
'    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
'    Call listaRicevitorieDisponibili_Init
        
End Sub

Private Sub CaricaValoriLst_Superaree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveSuperarea As String
    Dim superareaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSuperaree = New Collection
    
    sql = "SELECT DISTINCT IDAREA ID, CODICE, NOME"
    sql = sql + " FROM AREA"
    sql = sql + " WHERE IDPIANTA = " & idPiantaSelezionata
    sql = sql + " AND IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")"
    sql = sql + " ORDER BY CODICE"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set superareaCorrente = New clsElementoLista
            superareaCorrente.nomeElementoLista = rec("NOME")
            superareaCorrente.descrizioneElementoLista = rec("NOME")
            superareaCorrente.idElementoLista = rec("ID").Value
            chiaveSuperarea = ChiaveId(superareaCorrente.idElementoLista)
            Call listaSuperaree.Add(superareaCorrente, chiaveSuperarea)
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
    
    Call listaSuperaree_Init
    
End Sub

Private Sub cmdAccettaSelezioniSuperaree_Click()
    Call AccettaSelezioniSuperaree
End Sub

Private Sub AccettaSelezioniSuperaree()
    Dim i As Long
    Dim idSuperarea As Long
    
'    lstTipiSupportiSIAEDigitali.Enabled = True
'    lstTipiSupportiDigitali.Enabled = True
    
    Set listaSuperareeSelezionate = Nothing
    Set listaSuperareeSelezionate = New Collection
    
    stringaListaSuperareeSelezionate = ""
    quantitaSuperareeSelezionate = 0
    For i = 1 To lstSuperaree.ListCount
        If lstSuperaree.Selected(i - 1) Then
            idSuperarea = lstSuperaree.ItemData(i - 1)
            Call listaSuperaree.Add(idSuperarea)
            stringaListaSuperareeSelezionate = stringaListaSuperareeSelezionate & idSuperarea & ","
            quantitaSuperareeSelezionate = quantitaSuperareeSelezionate + 1
        End If
    Next i
    
    If stringaListaSuperareeSelezionate <> "" Then
        stringaListaSuperareeSelezionate = Left(stringaListaSuperareeSelezionate, Len(stringaListaSuperareeSelezionate) - 1)
    
        Call InizializzaListaTipiSupportiSIAEDigitali
        Call InizializzaListaTipiSupportiDigitali
    End If
    
End Sub

Private Sub CancellaSelezioni()
    Dim i As Long
    
    Set listaSuperareeSelezionate = Nothing
    Set listaSuperareeSelezionate = New Collection
    For i = 1 To lstSuperaree.ListCount
        lstSuperaree.Selected(i - 1) = False
    Next i
    
    For i = 1 To lstTipiSupportiSIAEDigitali.ListCount
        lstTipiSupportiSIAEDigitali.Selected(i - 1) = False
    Next i
    
    For i = 1 To lstTipiSupportiDigitali.ListCount
        lstTipiSupportiDigitali.Selected(i - 1) = False
    Next i
    
End Sub

Private Sub InizializzaListaTipiSupportiSIAEDigitali()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    lstTipiSupportiSIAEDigitali.Clear
    
    Call ApriConnessioneBD
    
    sql = "SELECT IDTIPOSUPPORTOSIAE, CODICESIAE || ' - ' || NOME AS DESCR FROM TIPOSUPPORTOSIAE WHERE ABILITATOPERTITOLODIGITALE = 1"
    sql = sql + " ORDER BY NOME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstTipiSupportiSIAEDigitali.AddItem rec("DESCR")
            lstTipiSupportiSIAEDigitali.ItemData(i - 1) = rec("IDTIPOSUPPORTOSIAE")
            
            If verificaTipoSupportoSIAEDaAbilitare(rec("IDTIPOSUPPORTOSIAE")) Then
                lstTipiSupportiSIAEDigitali.Selected(i - 1) = True
            End If
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
End Sub

Private Sub InizializzaListaTipiSupportiDigitali()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    lstTipiSupportiDigitali.Clear
    
    Call ApriConnessioneBD
    
' va escluso il TTS "Biglietto Tradizionale" con id = 1
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE || ' - ' || TS.NOME AS DESCR" & _
        " FROM TIPOSUPPORTO TS, TIPOSUPPORTOSIAE TSS" & _
        " WHERE TS.IDTIPOSUPPORTOSIAE = TSS.IDTIPOSUPPORTOSIAE" & _
        " AND TSS.IDTIPOSUPPORTOSIAE <> 1" & _
        " ORDER BY TS.IDTIPOSUPPORTO DESC"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lstTipiSupportiDigitali.AddItem rec("DESCR")
            lstTipiSupportiDigitali.ItemData(i - 1) = rec("IDTIPOSUPPORTO")
            
            If verificaTipoSupportoDaAbilitare(rec("IDTIPOSUPPORTO")) Then
                lstTipiSupportiDigitali.Selected(i - 1) = True
            End If
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdOperatoreDisponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaOperatoriDisponibiliNonDiRicevitoria
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdOperatoreSelezionato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaOperatoriSelezionatiNonDiRicevitoria
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdRicevitoriaDisponibile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaRicevitorieDisponibili
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdRicevitoriaSelezionata_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call SpostaInlistaRicevitorieSelezionate
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionaFile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ImportaRicevitorie
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CancellaSelezioni
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalit�FormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call InserisciNellaBaseDati
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub Successivo()
    Call AzionePercorsoGuidato(TNCP_FINE)
End Sub

Private Sub listaSuperaree_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim superarea As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSuperaree.Clear
    
    If Not (listaSuperaree Is Nothing) Then
        i = 1
        For Each superarea In listaSuperaree
            lstSuperaree.AddItem superarea.descrizioneElementoLista
            lstSuperaree.ItemData(i - 1) = superarea.idElementoLista
            i = i + 1
        Next superarea
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub listaRicevitorieDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ricevitoria As clsElementoLista

'    internalEventOld = internalEvent
'    internalEvent = True
'
'    lstRicevitorieDisponibili.Clear
'
'    If Not (listaRicevitorieDisponibili Is Nothing) Then
'        i = 1
'        For Each ricevitoria In listaRicevitorieDisponibili
'            lstRicevitorieDisponibili.AddItem ricevitoria.descrizioneElementoLista
'            lstRicevitorieDisponibili.ItemData(i - 1) = ricevitoria.idElementoLista
'            i = i + 1
'        Next ricevitoria
'    End If
'
'    internalEvent = internalEventOld

End Sub

Private Sub listaOperatoriSelezionatiNonDiRicevitoria_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

'    internalEventOld = internalEvent
'    internalEvent = True
'
'    lstOperatoriSelezionatiNonDiRicevitoria.Clear
'
'    If Not (listaOperatoriSelezionatiNonDiRicevitoria Is Nothing) Then
'        i = 1
'        For Each operatore In listaOperatoriSelezionatiNonDiRicevitoria
'            lstOperatoriSelezionatiNonDiRicevitoria.AddItem operatore.descrizioneElementoLista
'            lstOperatoriSelezionatiNonDiRicevitoria.ItemData(i - 1) = operatore.idElementoLista
'            i = i + 1
'        Next operatore
'    End If
'
'    internalEvent = internalEventOld

End Sub

Private Sub listaRicevitorieSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ricevitoria As clsElementoLista

'    internalEventOld = internalEvent
'    internalEvent = True
'
'    lstRicevitorieSelezionate.Clear
'
'    If Not (listaRicevitorieSelezionate Is Nothing) Then
'        i = 1
'        For Each ricevitoria In listaRicevitorieSelezionate
'            lstRicevitorieSelezionate.AddItem ricevitoria.descrizioneElementoLista
'            lstRicevitorieSelezionate.ItemData(i - 1) = ricevitoria.idElementoLista
'            i = i + 1
'        Next ricevitoria
'    End If
'
'    internalEvent = internalEventOld

End Sub

Private Sub SvuotaOperatoriDisponibili()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
'    For Each operatore In listaOperatoriDisponibiliNonDiRicevitoria
'        chiaveOperatore = ChiaveId(operatore.idElementoLista)
'        Call listaOperatoriSelezionatiNonDiRicevitoria.Add(operatore, chiaveOperatore)
'    Next operatore
'    Set listaOperatoriDisponibiliNonDiRicevitoria = Nothing
'    Set listaOperatoriDisponibiliNonDiRicevitoria = New Collection
'
'    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
'    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SvuotaRicevitorieDisponibili()
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
'    For Each ricevitoria In listaRicevitorieDisponibili
'        chiaveRicevitoria = ChiaveId(ricevitoria.idElementoLista)
'        Call listaRicevitorieSelezionate.Add(ricevitoria, chiaveRicevitoria)
'    Next ricevitoria
'    Set listaRicevitorieDisponibili = Nothing
'    Set listaRicevitorieDisponibili = New Collection
'
'    Call listaRicevitorieDisponibili_Init
'    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub SvuotaOperatoriSelezionati()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
'    For Each operatore In listaOperatoriSelezionatiNonDiRicevitoria
'        chiaveOperatore = ChiaveId(operatore.idElementoLista)
'        Call listaOperatoriDisponibiliNonDiRicevitoria.Add(operatore, chiaveOperatore)
'    Next operatore
'    Set listaOperatoriSelezionatiNonDiRicevitoria = Nothing
'    Set listaOperatoriSelezionatiNonDiRicevitoria = New Collection
'
'    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
'    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SvuotaRicevitorieSelezionate()
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
'    For Each ricevitoria In listaRicevitorieSelezionate
'        chiaveRicevitoria = ChiaveId(ricevitoria.idElementoLista)
'        Call listaRicevitorieDisponibili.Add(ricevitoria, chiaveRicevitoria)
'    Next ricevitoria
'    Set listaRicevitorieSelezionate = Nothing
'    Set listaRicevitorieSelezionate = New Collection
'
'    Call listaRicevitorieDisponibili_Init
'    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub SpostaInlistaOperatoriDisponibiliNonDiRicevitoria()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
'    For i = 1 To lstOperatoriSelezionatiNonDiRicevitoria.ListCount
'        If lstOperatoriSelezionatiNonDiRicevitoria.Selected(i - 1) Then
'            idOperatore = lstOperatoriSelezionatiNonDiRicevitoria.ItemData(i - 1)
'            chiaveOperatore = ChiaveId(idOperatore)
'            Set operatore = listaOperatoriSelezionatiNonDiRicevitoria.Item(chiaveOperatore)
'            Call listaOperatoriDisponibiliNonDiRicevitoria.Add(operatore, chiaveOperatore)
'            Call listaOperatoriSelezionatiNonDiRicevitoria.Remove(chiaveOperatore)
'        End If
'    Next i
'    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
'    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SpostaInlistaRicevitorieDisponibili()
    Dim i As Integer
    Dim idRicevitoria As Long
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
'    For i = 1 To lstRicevitorieSelezionate.ListCount
'        If lstRicevitorieSelezionate.Selected(i - 1) Then
'            idRicevitoria = lstRicevitorieSelezionate.ItemData(i - 1)
'            chiaveRicevitoria = ChiaveId(idRicevitoria)
'            Set ricevitoria = listaRicevitorieSelezionate.Item(chiaveRicevitoria)
'            Call listaRicevitorieDisponibili.Add(ricevitoria, chiaveRicevitoria)
'            Call listaRicevitorieSelezionate.Remove(chiaveRicevitoria)
'        End If
'    Next i
'    Call listaRicevitorieDisponibili_Init
'    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub SpostaInlistaOperatoriSelezionatiNonDiRicevitoria()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
'    For i = 1 To lstOperatoriDisponibiliNonDiRicevitoria.ListCount
'        If lstOperatoriDisponibiliNonDiRicevitoria.Selected(i - 1) Then
'            idOperatore = lstOperatoriDisponibiliNonDiRicevitoria.ItemData(i - 1)
'            chiaveOperatore = ChiaveId(idOperatore)
'            Set operatore = listaOperatoriDisponibiliNonDiRicevitoria.Item(chiaveOperatore)
'            Call listaOperatoriSelezionatiNonDiRicevitoria.Add(operatore, chiaveOperatore)
'            Call listaOperatoriDisponibiliNonDiRicevitoria.Remove(chiaveOperatore)
'        End If
'    Next i
'    Call listaOperatoriDisponibiliNonDiRicevitoria_Init
'    Call listaOperatoriSelezionatiNonDiRicevitoria_Init
End Sub

Private Sub SpostaInlistaRicevitorieSelezionate()
    Dim i As Integer
    Dim idRicevitoria As Long
    Dim ricevitoria As clsElementoLista
    Dim chiaveRicevitoria As String
    
'    For i = 1 To lstRicevitorieDisponibili.ListCount
'        If lstRicevitorieDisponibili.Selected(i - 1) Then
'            idRicevitoria = lstRicevitorieDisponibili.ItemData(i - 1)
'            chiaveRicevitoria = ChiaveId(idRicevitoria)
'            Set ricevitoria = listaRicevitorieDisponibili.Item(chiaveRicevitoria)
'            Call listaRicevitorieSelezionate.Add(ricevitoria, chiaveRicevitoria)
'            Call listaRicevitorieDisponibili.Remove(chiaveRicevitoria)
'        End If
'    Next i
'    Call listaRicevitorieDisponibili_Init
'    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub listaOperatoriDisponibiliNonDiRicevitoria_Click()
'    Call VisualizzaListBoxToolTip(listaOperatoriDisponibiliNonDiRicevitoria, listaOperatoriDisponibiliNonDiRicevitoria.Text)
End Sub

Private Sub listaOperatoriSelezionatiNonDiRicevitoria_Click()
'    Call VisualizzaListBoxToolTip(listaOperatoriSelezionatiNonDiRicevitoria, listaOperatoriSelezionatiNonDiRicevitoria.Text)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim idSuperarea As Long
    Dim idTipoSupportoSIAE As Long
    Dim idTipoSupporto As Long
    Dim i As Long
    Dim j As Long
    
    If stringaListaSuperareeSelezionate = "" Then
        MsgBox "Nessuna superarea selezionata"
    Else
        Call ApriConnessioneBD

On Error GoTo errori_TitoliDigitali_InserisciNellaBaseDati

        SETAConnection.BeginTrans
    
        sql = "DELETE FROM SETA_INTEGRA.TSDIGCONSENTITOPERSUPERAREA" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")"
        SETAConnection.Execute sql, n, adCmdText
    
        For i = 1 To lstSuperaree.ListCount
            If lstSuperaree.Selected(i - 1) = True Then
                idSuperarea = lstSuperaree.ItemData(i - 1)
            
                For j = 1 To lstTipiSupportiSIAEDigitali.ListCount
                    If lstTipiSupportiSIAEDigitali.Selected(j - 1) = True Then
                        idTipoSupportoSIAE = lstTipiSupportiSIAEDigitali.ItemData(j - 1)
                        sql = "INSERT INTO SETA_INTEGRA.TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                            " VALUES (" & idTipoSupportoSIAE & ", NULL, " & idProdottoSelezionato & ", " & idSuperarea & ")"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                Next j
                
                For j = 1 To lstTipiSupportiDigitali.ListCount
                    If lstTipiSupportiDigitali.Selected(j - 1) = True Then
                        idTipoSupporto = lstTipiSupportiDigitali.ItemData(j - 1)
                        sql = "INSERT INTO SETA_INTEGRA.TSDIGCONSENTITOPERSUPERAREA (IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO, IDPRODOTTO, IDSUPERAREA)" & _
                            " VALUES (NULL, " & idTipoSupporto & ", " & idProdottoSelezionato & ", " & idSuperarea & ")"
                        SETAConnection.Execute sql, n, adCmdText
                    End If
                Next j
                
            End If
        Next i
    
        SETAConnection.CommitTrans
    
        Call ChiudiConnessioneBD
    End If

    Call AggiornaAbilitazioneControlli
    Call CancellaSelezioni

    Exit Sub

errori_TitoliDigitali_InserisciNellaBaseDati:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    Call CancellaSelezioni

End Sub

Private Sub cmdSvuotaOperatoreSelezionati_Click()
    Dim mousePointerOld As Integer
    
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call SvuotaOperatoriSelezionati
'
'    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaOperatoriDisponibili_Click()
    Dim mousePointerOld As Integer
    
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call SvuotaOperatoriDisponibili
'
'    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaRicevitorieDisponibili_Click()
    Dim mousePointerOld As Integer
    
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call SvuotaRicevitorieDisponibili
'
'    MousePointer = mousePointerOld
End Sub

Private Sub cmdSvuotaRicevitorieSelezionate_Click()
    Dim mousePointerOld As Integer
    
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call SvuotaRicevitorieSelezionate
'
'    MousePointer = mousePointerOld
End Sub
    
Private Sub ImportaRicevitorie()
'    Call SvuotaRicevitorieSelezionate
'    Call ApriFileImportazioneRicevitorie
'    Call listaRicevitorieDisponibili_Init
'    Call listaRicevitorieSelezionate_Init
End Sub

Private Sub lstSuperaree_Click()
'    lstTipiSupportiSIAEDigitali.Enabled = False
'    lstTipiSupportiDigitali.Enabled = False
    Call AccettaSelezioniSuperaree
'    Call AggiornaLabelConfigurazioneSuperarea
End Sub

Private Sub AggiornaLabelConfigurazioneSuperarea()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSuperareaSelezionata As Long
    
    idSuperareaSelezionata = lstSuperaree.ItemData(lstSuperaree.ListIndex)
    
End Sub

Private Function verificaTipoSupportoSIAEDaAbilitare(idTipoSupportoSIAE As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    verificaTipoSupportoSIAEDaAbilitare = False
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM SETA_INTEGRA.TSDIGCONSENTITOPERSUPERAREA" & _
        " WHERE IDTIPOSUPPORTOSIAE = " & idTipoSupportoSIAE & _
        " AND IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = quantitaSuperareeSelezionate Then
            verificaTipoSupportoSIAEDaAbilitare = True
        End If
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
End Function

Private Function verificaTipoSupportoDaAbilitare(idTipoSupporto As Long) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
    verificaTipoSupportoDaAbilitare = False
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(*) AS CONT" & _
        " FROM SETA_INTEGRA.TSDIGCONSENTITOPERSUPERAREA" & _
        " WHERE IDTIPOSUPPORTO = " & idTipoSupporto & _
        " AND IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDSUPERAREA IN (" & stringaListaSuperareeSelezionate & ")"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = quantitaSuperareeSelezionate Then
            verificaTipoSupportoDaAbilitare = True
        End If
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
End Function

