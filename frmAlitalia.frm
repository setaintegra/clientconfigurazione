VERSION 5.00
Begin VB.Form frmAlitalia 
   Caption         =   "Gestione promozione Alitalia"
   ClientHeight    =   5085
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15045
   LinkTopic       =   "Form1"
   ScaleHeight     =   5085
   ScaleWidth      =   15045
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdElimina 
      Caption         =   "Elimina fruizioni selezionate"
      Height          =   375
      Left            =   12600
      TabIndex        =   6
      Top             =   1200
      Width           =   2295
   End
   Begin VB.ListBox lstFruizioni 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   4
      Top             =   1200
      Width           =   12375
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   465
      Left            =   13680
      TabIndex        =   3
      Top             =   4440
      Width           =   1140
   End
   Begin VB.CommandButton cmdCercaFruizioni 
      Caption         =   "Cerca fruizioni"
      Height          =   375
      Left            =   5280
      TabIndex        =   2
      Top             =   240
      Width           =   2295
   End
   Begin VB.TextBox txtStringaSupporto 
      Height          =   375
      Left            =   2160
      TabIndex        =   0
      Top             =   240
      Width           =   3015
   End
   Begin VB.Label Label2 
      Caption         =   "Lista fruizioni:"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "Stringa supporto fruito:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   1935
   End
End
Attribute VB_Name = "frmAlitalia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub Init()
    Call Me.Show(vbModal)
End Sub

Private Sub cmdElimina_Click()
    elimina
End Sub

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub cmdCercaFruizioni_Click()
    cercaFruizioni
End Sub

Private Sub cercaFruizioni()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim stringaSupporto As String
    Dim s As String

    stringaSupporto = Trim(txtStringaSupporto.Text)
    lstFruizioni.Clear

    If Mid(stringaSupporto, 1, 1) >= "0" And Mid(stringaSupporto, 1, 1) <= "9" Then
        sql = "SELECT RPAD(A.IDTITOLO, 10, ' ') || ' ' || RPAD(A.IDRAPPRESENTAZIONE, 10, ' ') || ' ' || RPAD('" & stringaSupporto & "', 20, ' ') || ' ' ||" & _
            " TO_CHAR(DATAORAINIZIO, 'DD-MM-YYYY HH24:MI:SS') || ' - ' || TO_CHAR(DATAORAINIZIO + NUMTODSINTERVAL(R.DURATAINMINUTI,'MINUTE'), 'DD-MM-YYYY HH24:MI:SS') || ' - ' || DATAORAFRUIZIONE || ' - ' || RPAD(USERNAME, 20, ' ')  || ' - ' || BIGLIETTOALITALIA S" & _
            " FROM SUPPORTO S, STATOTITOLO ST, SETA_INTEGRA.TITOLOFRUITOPERALITALIA A, RAPPRESENTAZIONE R, OPERATORE OPT" & _
            " WHERE (S.NUMEROTIPOGRAFICO = " & stringaSupporto & " OR IDENTIFICATIVOSUPPORTODIGITALE = '" & stringaSupporto & "')" & _
            " AND S.IDSUPPORTO = ST.IDSUPPORTO" & _
            " AND ST.IDTITOLO = A.IDTITOLO" & _
            " AND A.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
            " AND A.IDOPERATORE = OPT.IDOPERATORE" & _
            " UNION" & _
            " SELECT RPAD(' ', 10, ' ') || ' ' || RPAD(' ', 10, ' ') || ' ' || RPAD(STRINGASUPPORTO, 20, ' ') || ' ' ||" & _
            " TO_CHAR(DATAINIZIOVALIDITA, 'DD-MM-YYYY HH24:MI:SS') || ' - ' || DATAFINEVALIDITA || ' - ' || DATAORAFRUIZIONE || ' - ' || RPAD(USERNAME, 20, ' ')  || ' - ' || BIGLIETTOALITALIA S" & _
            " FROM SETA_INTEGRA.TITOLOESTERNOPERALITALIA A, OPERATORE OPT" & _
            " WHERE A.STRINGASUPPORTO = '" & stringaSupporto & "'" & _
            " AND A.IDOPERATORE = OPT.IDOPERATORE"
    Else
        sql = "SELECT RPAD(A.STRINGASUPPORTO, 20, ' ') || ' ' || DATAINIZIOVALIDITA || ' - ' || DATAFINEVALIDITA || ' - ' || DATAORAFRUIZIONE || ' - ' || RPAD(USERNAME, 20, ' ')  || ' - ' || BIGLIETTOALITALIA S" & _
            " FROM SETA_INTEGRA.TITOLOESTERNOPERALITALIA A, OPERATORE OPT" & _
            " WHERE A.STRINGASUPPORTO = '" & Trim(stringaSupporto) & "'" & _
            " AND A.IDOPERATORE = OPT.IDOPERATORE"
    End If
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            s = rec("S")
            lstFruizioni.AddItem s
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub elimina()
    Dim sql As String
    Dim i As Long
    Dim idTitolo As Long
    Dim idRappresentazione As Long
    Dim stringaSupporto As String
    Dim n As Integer
    Dim qtaRigheAggiornate As Integer
    Dim riga As String

    qtaRigheAggiornate = 0
    For i = 1 To lstFruizioni.ListCount
        If lstFruizioni.Selected(i - 1) = True Then
            riga = lstFruizioni.List(i - 1)
            If Mid(riga, 1, 1) >= "0" And Mid(riga, 1, 1) <= "9" Then
                idTitolo = Trim(Mid(lstFruizioni.List(i - 1), 1, 10))
                idRappresentazione = Trim(Mid(lstFruizioni.List(i - 1), 11, 10))
                stringaSupporto = Trim(Mid(lstFruizioni.List(i - 1), 21, 20))
                
                sql = "DELETE FROM SETA_INTEGRA.TITOLOFRUITOPERALITALIA" & _
                    " WHERE IDTITOLO = " & idTitolo & _
                    " AND IDRAPPRESENTAZIONE = " & idRappresentazione
                SETAConnection.Execute sql, n, adCmdText
                qtaRigheAggiornate = qtaRigheAggiornate + n
            Else
                stringaSupporto = Trim(Mid(lstFruizioni.List(i - 1), 1, 20))
                sql = "UPDATE SETA_INTEGRA.TITOLOESTERNOPERALITALIA SET DATAORAFRUIZIONE = NULL, IDOPERATORE = NULL, BIGLIETTOALITALIA = NULL" & _
                    " WHERE STRINGASUPPORTO = '" & stringaSupporto & "'"
                SETAConnection.Execute sql, n, adCmdText
                qtaRigheAggiornate = qtaRigheAggiornate + n
            End If
        End If
    Next i
    
    MsgBox ("Righe aggiornate: " & qtaRigheAggiornate)
    
    cercaFruizioni
End Sub

