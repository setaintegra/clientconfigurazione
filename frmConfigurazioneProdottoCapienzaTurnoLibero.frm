VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoCapienzaTurnoLibero 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   11
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   10
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   9
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4740
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1380
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5730
      Left            =   5220
      MultiSelect     =   2  'Extended
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1320
      Width           =   4515
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4740
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2520
      Width           =   435
   End
   Begin VB.CommandButton cmdDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4740
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3660
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4740
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   4800
      Width           =   435
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5730
      Left            =   180
      MultiSelect     =   2  'Extended
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1320
      Width           =   4515
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   19
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   18
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione del numero massimo di titoli emettibili per superarea"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   8235
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Non configurate (codice - nome)"
      Height          =   195
      Left            =   180
      TabIndex        =   16
      Top             =   1080
      Width           =   4515
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Configurate (codice - nome / numero)"
      Height          =   195
      Left            =   5220
      TabIndex        =   15
      Top             =   1080
      Width           =   4515
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoCapienzaTurnoLibero"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
'Private mostraTuttiGliOperatori As Boolean
'Private mostraSoloOperatoriTA As Boolean
'Private mostraSoloOperatoriTL As Boolean
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private listaDisponibili As Collection
Private listaSelezionati As Collection
'Private criteriSelezioneImpostati As Boolean

Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
'    lstDisponibili.Enabled = criteriSelezioneImpostati
'    lblDisponibili.Enabled = criteriSelezioneImpostati
'    lstSelezionati.Enabled = criteriSelezioneImpostati
'    lblSelezionati.Enabled = criteriSelezioneImpostati
'    cmdCarica.Enabled = criteriSelezioneImpostati
'    cmdDidsponibile.Enabled = criteriSelezioneImpostati
'    cmdSelezionato.Enabled = criteriSelezioneImpostati
'    cmdSvuotaDisponibili.Enabled = criteriSelezioneImpostati
'    cmdSvuotaSelezionati.Enabled = criteriSelezioneImpostati
'    cmdConferma.Enabled = criteriSelezioneImpostati
'    If gestioneExitCode <> EC_NON_SPECIFICATO Then
'        Call lstDisponibili.Clear
'        Call lstSelezionati.Clear
'    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Fine"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    mostraTuttiGliOperatori = False
'    mostraSoloOperatoriTA = False
'    mostraSoloOperatoriTL = False
'    criteriSelezioneImpostati = False
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub
'
'Private Sub cmdCarica_Click()
'    Call CaricaOperatori
'End Sub
'
'Private Sub CaricaOperatori()
'    If RilevaValoriCampi Then
''        criteriSelezioneImpostati = True
'        Call CaricaValoriLstDisponibili
'        Call CaricaValoriLstSelezionati
'        Call AggiornaAbilitazioneControlli
'    End If
'End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
'    If mostraTuttiGliOperatori Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)" & _
'                " AND (OP.IDOPERATORE IS NULL OR OP.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")" & _
'                " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " ORDER BY USERNAME"
'        Else
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)" & _
'                " AND OP.IDOPERATORE IS NULL" & _
'                " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " ORDER BY USERNAME"
'        End If
'    End If
'    If mostraSoloOperatoriTL Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)" & _
'                " AND (OP.IDOPERATORE IS NULL OR OP.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")" & _
'                " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
'                " AND P.IDTIPOPUNTOVENDITA = " & TPV_RICEVITORIA & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " ORDER BY USERNAME"
'        Else
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)" & _
'                " AND OP.IDOPERATORE IS NULL" & _
'                " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
'                " AND P.IDTIPOPUNTOVENDITA = " & TPV_RICEVITORIA & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " ORDER BY USERNAME"
'        End If
'    End If
'    If mostraSoloOperatoriTA Then
        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
            sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME" & _
                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
                " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)" & _
                " AND (OP.IDOPERATORE IS NULL OR OP.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")" & _
                " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato & _
                " AND O.ABILITATO = " & VB_VERO & _
                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
                " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")" & _
                " OR O.IDPUNTOVENDITA IS NULL)" & _
                " ORDER BY USERNAME"
        Else
            sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME" & _
                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
                " WHERE O.IDOPERATORE = OP.IDOPERATORE(+)" & _
                " AND OP.IDOPERATORE IS NULL" & _
                " AND OP.IDPRODOTTO(+) = " & idProdottoSelezionato & _
                " AND O.ABILITATO = " & VB_VERO & _
                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
                " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")" & _
                " OR O.IDPUNTOVENDITA IS NULL)" & _
                " ORDER BY USERNAME"
        End If
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            operatoreCorrente.idElementoLista = rec("ID").Value
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaDisponibili.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSelezionati = New Collection
    
'    If mostraTuttiGliOperatori Then 'RIVEDERE BENE
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE" & _
'                " AND OP.IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND (OP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OP.IDTIPOSTATORECORD IS NULL)" & _
'                " ORDER BY USERNAME"
'        Else
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE" & _
'                " AND OP.IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " ORDER BY USERNAME"
'        End If
'    End If
'    If mostraSoloOperatoriTL Then 'RIVEDERE BENE
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE" & _
'                " AND OP.IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
'                " AND P.IDTIPOPUNTOVENDITA = " & TPV_RICEVITORIA & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND (OP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OP.IDTIPOSTATORECORD IS NULL)" & _
'                " ORDER BY USERNAME"
'        Else
'            sql = "SELECT O.IDOPERATORE ID, USERNAME" & _
'                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
'                " WHERE O.IDOPERATORE = OP.IDOPERATORE" & _
'                " AND OP.IDPRODOTTO = " & idProdottoSelezionato & _
'                " AND O.ABILITATO = " & VB_VERO & _
'                " AND O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
'                " AND P.IDTIPOPUNTOVENDITA = " & TPV_RICEVITORIA & _
'                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
'                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " ORDER BY USERNAME"
'        End If
'    End If
'    If mostraSoloOperatoriTA Then 'RIFARE COME LE ALTRE
        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
            sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME" & _
                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
                " WHERE O.IDOPERATORE = OP.IDOPERATORE" & _
                " AND OP.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND O.ABILITATO = " & VB_VERO & _
                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
                " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")" & _
                " OR O.IDPUNTOVENDITA IS NULL)" & _
                " AND (OP.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
                " OR OP.IDTIPOSTATORECORD IS NULL)" & _
                " ORDER BY USERNAME"
        Else
            sql = "SELECT DISTINCT O.IDOPERATORE ID, USERNAME" & _
                " FROM OPERATORE O, OPERATORE_PRODOTTO OP, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO" & _
                " WHERE O.IDOPERATORE = OP.IDOPERATORE" & _
                " AND OP.IDPRODOTTO = " & idProdottoSelezionato & _
                " AND O.ABILITATO = " & VB_VERO & _
                " AND O.IDOPERATORE = OO.IDOPERATORE" & _
                " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA" & _
                " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")" & _
                " OR O.IDPUNTOVENDITA IS NULL)" & _
                " ORDER BY USERNAME"
        End If
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            operatoreCorrente.idElementoLista = rec("ID").Value
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaSelezionati.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstSelezionati_Init
        
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    criteriSelezioneImpostati = False
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoPostiMigliori.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
                If StatoAttualeProdottoSuSchemaProduzione(idProdottoSelezionato) = TSP_IN_CONFIGURAZIONE Then
                    Call SetGestioneExitCode(EC_CONFERMA)
                    Call AggiornaAbilitazioneControlli
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, stringaNota, idProdottoSelezionato)
'                    criteriSelezioneImpostati = False
                    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
                    Call AggiornaAbilitazioneControlli
                Else
                    Call frmMessaggio.Visualizza("NotificaOperazioneNonEseguibile")
                End If
            Else
                Call SetGestioneExitCode(EC_CONFERMA)
                Call AggiornaAbilitazioneControlli
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_OPERATORE, stringaNota, idProdottoSelezionato)
'                criteriSelezioneImpostati = False
                Call SetGestioneExitCode(EC_NON_SPECIFICATO)
                Call AggiornaAbilitazioneControlli
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
    Call AzionePercorsoGuidato(TNCP_FINE)
End Sub

'Private Function RilevaValoriCampi() As Boolean
'    RilevaValoriCampi = True
'    mostraTuttiGliOperatori = optTuttiGliOperatori.Value
'    mostraSoloOperatoriTA = optSoloOperatoriTA.Value
'    mostraSoloOperatoriTL = optSoloOperatoriTL.Value
'End Function

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear
    
    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each operatore In listaDisponibili
            lstDisponibili.AddItem operatore.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear
    
    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each operatore In listaSelezionati
            lstSelezionati.AddItem operatore.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SvuotaDisponibili()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaDisponibili
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaSelezionati.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SvuotaSelezionati()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaSelezionati
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaDisponibili.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idOperatore = lstSelezionati.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaSelezionati.Item(chiaveOperatore)
            Call listaDisponibili.Add(operatore, chiaveOperatore)
            Call listaSelezionati.Remove(chiaveOperatore)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idOperatore = lstDisponibili.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaDisponibili.Item(chiaveOperatore)
            Call listaSelezionati.Add(operatore, chiaveOperatore)
            Call listaDisponibili.Remove(chiaveOperatore)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
End Sub

Private Sub InserisciNellaBaseDati_old()
    Dim sql As String
    Dim n As Long
    Dim operatore As clsElementoLista
'    Dim SqlConditions As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    If Not (listaSelezionati Is Nothing) Then
        sql = "DELETE FROM OPERATORE_PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each operatore In listaSelezionati
            sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)" & _
                " VALUES (" & operatore.idElementoLista & ", " & _
                idProdottoSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                tipoStatoRecordSelezionato = TSR_NUOVO
'                SqlConditions = " AND IDOPERATORE = " & operatore.idElementoLista
'                Call AggiornaParametriSessioneSuRecord("OPERATORE_PRODOTTO", "IDPRODOTTO", idProdottoSelezionato, TSR_NUOVO, SqlConditions)
'            End If
        Next operatore
    End If
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim operatore As clsElementoLista
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    If Not (listaSelezionati Is Nothing) Then
        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
            Call AggiornaParametriSessioneSuRecord("OPERATORE_PRODOTTO", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO)
        Else
            sql = "DELETE FROM OPERATORE_PRODOTTO WHERE IDPRODOTTO = " & idProdottoSelezionato
            SETAConnection.Execute sql, n, adCmdText
        End If
        
        For Each operatore In listaSelezionati
            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
                sql = "INSERT INTO OPERATORE_PRODOTTO" & _
                    " (IDPRODOTTO, IDOPERATORE)" & _
                    " SELECT " & _
                    idProdottoSelezionato & " IDPRODOTTO, " & _
                    operatore.idElementoLista & " IDOPERATORE" & _
                    " FROM DUAL" & _
                    " MINUS" & _
                    " SELECT IDPRODOTTO, IDOPERATORE" & _
                    " FROM OPERATORE_PRODOTTO" & _
                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    " AND IDOPERATORE = " & operatore.idElementoLista
                SETAConnection.Execute sql, n, adCmdText
                condizioneSql = " AND IDOPERATORE = " & operatore.idElementoLista
                Call AggiornaParametriSessioneSuRecord("OPERATORE_PRODOTTO", "IDPRODOTTO", idProdottoSelezionato, TSR_NUOVO, condizioneSql)
            Else
                sql = "INSERT INTO OPERATORE_PRODOTTO (IDOPERATORE, IDPRODOTTO)" & _
                    " VALUES (" & operatore.idElementoLista & ", " & _
                    idProdottoSelezionato & ")"
                SETAConnection.Execute sql, n, adCmdText
            End If
        Next operatore
    End If
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub
'
'Private Sub optSoloOperatoriTA_Click()
'    criteriSelezioneImpostati = True
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub optSoloOperatoriTL_Click()
'    criteriSelezioneImpostati = True
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub optTuttiGliOperatori_Click()
'    criteriSelezioneImpostati = True
'    Call AggiornaAbilitazioneControlli
'End Sub




