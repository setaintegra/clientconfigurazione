VERSION 5.00
Begin VB.Form frmInizialeSpettacolo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Spettacolo"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Caratteristiche"
      Height          =   2775
      Left            =   120
      TabIndex        =   8
      Top             =   540
      Width           =   9855
      Begin VB.ComboBox cmbStagione 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1740
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1920
         Width           =   4095
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   0
         Top             =   480
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   900
         Width           =   7635
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   540
         TabIndex        =   13
         Top             =   540
         Width           =   1035
      End
      Begin VB.Label lblDecsrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblStagione 
         Alignment       =   1  'Right Justify
         Caption         =   "stagione"
         Height          =   255
         Left            =   540
         TabIndex        =   10
         Top             =   1980
         Width           =   1095
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   1755
      Left            =   10140
      TabIndex        =   7
      Top             =   540
      Width           =   1695
      Begin VB.CommandButton cmdCalendario 
         Caption         =   "Calendario"
         Height          =   435
         Left            =   120
         TabIndex        =   14
         Top             =   1200
         Width           =   1395
      End
      Begin VB.CommandButton cmdGeneriSiae 
         Caption         =   "Generi SIAE"
         Height          =   435
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   1395
      End
      Begin VB.CommandButton cmdRappresentazioni 
         Caption         =   "Rappresentazioni"
         Height          =   435
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1395
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeSpettacolo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idSpettacoloSelezionato As Long
Private nomeSpettacolo As String
Private descrizioneSpettacolo As String
Private nomeStagioneSpettacoloSelezionato As String
Private idStagioneSpettacoloSelezionato As Long

Private internalEvent As Boolean

Private modalit�FormCorrente As AzioneEnum

Public Sub Init()
    Dim sql As String
    
    Call Variabili_Init
    Call AggiornaAbilitazioneControlli

    sql = "SELECT IDSTAGIONE AS ""ID"", NOME FROM STAGIONE ORDER BY NOME"

    Select Case modalit�FormCorrente
        Case A_NUOVO
            Call CaricaValoriCombo(cmbStagione, sql, "NOME")
            idSpettacoloSelezionato = idNessunElementoSelezionato
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call CaricaValoriCombo(cmbStagione, sql, "NOME")
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call CaricaValoriCombo(cmbStagione, sql, "NOME")
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call AggiornaAbilitazioneControlli
    Call frmInizialeSpettacolo.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    nomeSpettacolo = ""
    descrizioneSpettacolo = ""
    nomeStagioneSpettacoloSelezionato = ""
    idStagioneSpettacoloSelezionato = idNessunElementoSelezionato
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdSpettacoloSelezionato(id As Long)
    idSpettacoloSelezionato = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Spettacolo"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Spettacolo configurato"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Spettacolo configurato"
    End Select
    fraAzioni.Visible = (modalit�FormCorrente = A_MODIFICA)
    cmdConferma.Enabled = (txtNome.Text <> "" And cmbStagione.ListIndex <> idNessunElementoSelezionato)
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDecsrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmbStagione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblStagione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    
    cmdRappresentazioni.Enabled = True
    cmdCalendario.Enabled = True
        
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub Annulla()
    Call frmSceltaSpettacolo.SetIdRecordSelezionato(idSpettacoloSelezionato)
    Call frmSceltaSpettacolo.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Public Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT NOME, DESCRIZIONE, IDSTAGIONE FROM SPETTACOLO" & _
        " WHERE IDSPETTACOLO = " & idSpettacoloSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeSpettacolo = rec("NOME")
        descrizioneSpettacolo = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        idStagioneSpettacoloSelezionato = IIf(IsNull(rec("IDSTAGIONE")), idNessunElementoSelezionato, rec("IDSTAGIONE"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeSpettacolo
    txtDescrizione.Text = descrizioneSpettacolo
    Call SelezionaElementoSuCombo(cmbStagione, idStagioneSpettacoloSelezionato)
    
    internalEvent = internalEventOld
    
End Sub

Public Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoSpettacolo As Long
    Dim max As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovoSpettacolo = OttieniIdentificatoreDaSequenza("SQ_SPETTACOLO")
    
    sql = "INSERT INTO SPETTACOLO (IDSPETTACOLO, NOME, DESCRIZIONE, IDSTAGIONE)" & _
        " VALUES (" & _
        idNuovoSpettacolo & ", " & _
        SqlStringValue(nomeSpettacolo) & ", " & _
        SqlStringValue(descrizioneSpettacolo) & ", " & _
        idStagioneSpettacoloSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetIdSpettacoloSelezionato(idNuovoSpettacolo)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdRappresentazioni_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If IsDefinitoGenereSIAE Then
        Call ConfiguraRappresentazioni
    Else
        MsgBox "Genere SIAE non ancora definito"
    End If
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub cmdCalendario_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If IsDefinitoGenereSIAE Then
        Call ConfiguraRappresentazioniCalendario
    Else
        MsgBox "Genere SIAE non ancora definito"
    End If
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub ConfiguraRappresentazioni()
    Call CaricaFormConfigurazioneSpettacolo
End Sub

Private Sub ConfiguraRappresentazioniCalendario()
    Call CaricaFormConfigurazioneRappresentazioniCalendario
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    stringaNota = "IDSPETTACOLO = " & idSpettacoloSelezionato
    Select Case modalit�FormCorrente
        Case A_NUOVO
            If ValoriCampiOK Then
                Call InserisciNellaBaseDati
                stringaNota = "IDSPETTACOLO = " & idSpettacoloSelezionato
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_SPETTACOLO, , stringaNota)
                Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                Call SetModalit�Form(A_MODIFICA)
                Call AggiornaAbilitazioneControlli
                Call frmSceltaSpettacolo.SetExitCodeFormIniziale(EC_CONFERMA)
            End If
        Case A_MODIFICA
            If ValoriCampiOK Then
                If IsSpettacoloCompleto(idSpettacoloSelezionato) Then
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_SPETTACOLO, , stringaNota)
                    Call frmMessaggio.Visualizza("NotificaModificaDati")
                    Call Esci
                    Call frmSceltaSpettacolo.SetExitCodeFormIniziale(EC_CONFERMA)
                End If
            End If
        Case A_ELIMINA
            Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_SPETTACOLO, , stringaNota)
            Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
            Call Esci
            Call frmSceltaSpettacolo.SetExitCodeFormIniziale(EC_CONFERMA)
        Case Else
            'Do Nothing
    End Select
'    Call frmSceltaSpettacolo.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub CaricaFormConfigurazioneSpettacolo()
    Call frmConfigurazioneSpettacolo.SetIdSpettacoloSelezionato(idSpettacoloSelezionato)
    Call frmConfigurazioneSpettacolo.SetNomeSpettacoloSelezionato(nomeSpettacolo)
    Call frmConfigurazioneSpettacolo.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneSpettacolo.Init
End Sub

Private Sub CaricaFormConfigurazioneRappresentazioniCalendario()
    Call frmConfigurazioneRappresentazioniCalendario.SetIdSpettacoloSelezionato(idSpettacoloSelezionato)
    Call frmConfigurazioneRappresentazioniCalendario.SetNomeSpettacoloSelezionato(nomeSpettacolo)
    Call frmConfigurazioneRappresentazioniCalendario.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneRappresentazioniCalendario.Init
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As String
'    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

'    sql = "SELECT * FROM SPETTACOLO WHERE IDSPETTACOLO = " & idSpettacoloSelezionato
'    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
'    rec("NOME") = nomeSpettacolo
'    rec("DESCRIZIONE") = IIf((descrizioneSpettacolo = ""), Null, descrizioneSpettacolo)
'    rec("IDSTAGIONE") = idStagioneSpettacoloSelezionato
'    rec.Update
'    rec.Close
    sql = "UPDATE SPETTACOLO SET" & _
        " NOME = " & SqlStringValue(nomeSpettacolo) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneSpettacolo) & "," & _
        " IDSTAGIONE = " & idStagioneSpettacoloSelezionato & _
        " WHERE IDSPETTACOLO = " & idSpettacoloSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Public Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
    sql = "DELETE FROM SPETTACOLO_GENERESIAE WHERE IDSPETTACOLO = " & idSpettacoloSelezionato
    SETAConnection.Execute sql, n, adCmdText
    sql = "DELETE FROM SPETTACOLO WHERE IDSPETTACOLO = " & idSpettacoloSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD

End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub cmbStagione_Click()
    If Not internalEvent Then
        idStagioneSpettacoloSelezionato = cmbStagione.ItemData(cmbStagione.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Function ValoriCampiOK() As Boolean

On Error Resume Next

    ValoriCampiOK = True
    nomeSpettacolo = Trim(txtNome.Text)
    descrizioneSpettacolo = Trim(txtDescrizione.Text)

End Function

Private Sub cmdGeneriSiae_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraRappresentazioniGeneriSiae
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub ConfiguraRappresentazioniGeneriSiae()
    Call CaricaFormConfigurazioneSpettacoloGeneriSiae
End Sub

Private Sub CaricaFormConfigurazioneSpettacoloGeneriSiae()
    Call frmConfigurazioneSpettacoloGenereSiae.SetIdSpettacoloSelezionato(idSpettacoloSelezionato)
    Call frmConfigurazioneSpettacoloGenereSiae.SetNomeSpettacoloSelezionato(nomeSpettacolo)
    Call frmConfigurazioneSpettacoloGenereSiae.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneSpettacoloGenereSiae.Init
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Function IsDefinitoGenereSIAE() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim somma As Integer

    Call ApriConnessioneBD

    sql = "SELECT SUM(INCIDENZA) SOMMA" & _
        " FROM SPETTACOLO_GENERESIAE" & _
        " WHERE IDSPETTACOLO = " & idSpettacoloSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        If IsNull(rec("SOMMA")) Then
            somma = 0
        Else
            somma = rec("SOMMA").Value
        End If
    End If
    rec.Close

    Call ChiudiConnessioneBD

    IsDefinitoGenereSIAE = (somma = 100)
End Function
