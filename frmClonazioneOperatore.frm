VERSION 5.00
Begin VB.Form frmClonazioneOperatore 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Operatore"
   ClientHeight    =   4125
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8790
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4125
   ScaleWidth      =   8790
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   6300
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   300
      Width           =   2295
   End
   Begin VB.Frame fraDatiNuovoOperatore 
      Caption         =   "Dati nuovo operatore"
      Height          =   2115
      Left            =   180
      TabIndex        =   4
      Top             =   780
      Width           =   8415
      Begin VB.TextBox txtConfermaPwd 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2408
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   12
         Top             =   1260
         Width           =   3975
      End
      Begin VB.TextBox txtPwd 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2408
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   10
         Top             =   840
         Width           =   3975
      End
      Begin VB.TextBox txtUsername 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2408
         MaxLength       =   30
         TabIndex        =   5
         Top             =   420
         Width           =   3975
      End
      Begin VB.Label lblConfermaPwd 
         Alignment       =   1  'Right Justify
         Caption         =   "Conferma password"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label lblPwd 
         Alignment       =   1  'Right Justify
         Caption         =   "Password"
         Height          =   255
         Left            =   1260
         TabIndex        =   11
         Top             =   900
         Width           =   1035
      End
      Begin VB.Label lblUsername 
         Alignment       =   1  'Right Justify
         Caption         =   "Username"
         Height          =   255
         Left            =   1260
         TabIndex        =   6
         Top             =   480
         Width           =   1035
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   7440
      TabIndex        =   3
      Top             =   3540
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   180
      TabIndex        =   0
      Top             =   3060
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   6300
      TabIndex        =   9
      Top             =   60
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Clonazione dell'Operatore"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      TabIndex        =   8
      Top             =   180
      Width           =   5355
   End
End
Attribute VB_Name = "frmClonazioneOperatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOperatoreSelezionato As Long
Private usernameOperatoreSelezionato As String
Private idNuovoOperatore As Long
Private nuovoUsername As String
Private nuovaPwd As String
Private confermaPwd As String

Public Sub Init()
    Call CaricaDallaBaseDati
    lblInfo1.Caption = "Operatore"
    txtInfo1.Enabled = False
    txtInfo1.Text = usernameOperatoreSelezionato
    
    Call AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)
End Sub

Public Sub SetIdOperatoreSelezionato(idO As Long)
    idOperatoreSelezionato = idO
End Sub

Private Sub ResettaControlli()
    txtUsername.Text = ""
    txtPwd.Text = ""
    txtConfermaPwd.Text = ""
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = Trim(txtUsername.Text) <> "" _
        And Trim(txtPwd.Text) <> "" _
        And txtPwd.Text = txtConfermaPwd.Text
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "SELECT USERNAME" & _
        " FROM OPERATORE" & _
        " WHERE IDOPERATORE = " & idOperatoreSelezionato
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        usernameOperatoreSelezionato = rec("USERNAME")
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub cmdAnnulla_Click()
    
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    ResettaControlli
    AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim dataOraInizio As Date
    Dim dataOraFine As Date
    
    If CampiRiempitiCorrettamente Then
        dataOraInizio = FormatDateTime(Now, vbGeneralDate)
        
        If IsClonazioneOperatoreOK(idOperatoreSelezionato, nuovoUsername, nuovaPwd) Then
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("NotificaClonazioneOperatore", nuovoUsername, usernameOperatoreSelezionato, dataOraInizio, dataOraFine)
            ResettaControlli
            AggiornaAbilitazioneControlli
        Else
            dataOraFine = FormatDateTime(Now, vbGeneralDate)
            Call frmMessaggio.Visualizza("ErroreClonazione", dataOraInizio, dataOraFine)
        End If
    End If
End Sub

Private Function CampiRiempitiCorrettamente() As Boolean
    Dim listaNonConformita As New Collection
    
    nuovoUsername = Trim(txtUsername.Text)
    nuovaPwd = Trim(txtPwd.Text)
    confermaPwd = Trim(txtConfermaPwd.Text)
    
    If ViolataUnicit�("OPERATORE", "USERNAME = " & SqlStringValue(nuovoUsername), "", "", "", "") Then
        Call listaNonConformita.Add("- il valore nome = " _
            & SqlStringValue(nuovoUsername) & " � gi� presente in DB;")
    End If
    
    If InStr(1, nuovaPwd, " ", vbTextCompare) > 0 Then
        Call listaNonConformita.Add("- la password non � conforme;") 'contiene spazi
    End If
    
    If listaNonConformita.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformita))
    End If

    CampiRiempitiCorrettamente = (listaNonConformita.count = 0)
End Function

Private Sub cmdEsci_Click()
    Unload Me
End Sub

Private Sub txtUsername_Change()
    AggiornaAbilitazioneControlli
End Sub

Private Sub txtPwd_Change()
    AggiornaAbilitazioneControlli
End Sub

Private Sub txtConfermaPwd_Change()
    AggiornaAbilitazioneControlli
End Sub

