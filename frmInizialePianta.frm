VERSION 5.00
Begin VB.Form frmInizialePianta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   9105
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9105
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstOrgSelezionate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Left            =   9240
      TabIndex        =   14
      Top             =   5400
      Width           =   2475
   End
   Begin VB.CommandButton cmdOrgSelezionata 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8760
      TabIndex        =   11
      Top             =   5580
      Width           =   435
   End
   Begin VB.CommandButton cmdOrgDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8760
      TabIndex        =   12
      Top             =   6120
      Width           =   435
   End
   Begin VB.CommandButton cmdOrgSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8760
      TabIndex        =   13
      Top             =   6660
      Width           =   435
   End
   Begin VB.ListBox lstOrgDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Left            =   6240
      TabIndex        =   10
      Top             =   5400
      Width           =   2475
   End
   Begin VB.ListBox lstVenSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Left            =   3240
      MultiSelect     =   2  'Extended
      TabIndex        =   9
      Top             =   5400
      Width           =   2475
   End
   Begin VB.CommandButton cmdVenSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   2760
      TabIndex        =   6
      Top             =   5580
      Width           =   435
   End
   Begin VB.CommandButton cmdVenDidsponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   2760
      TabIndex        =   7
      Top             =   6120
      Width           =   435
   End
   Begin VB.CommandButton cmdVenSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   2760
      TabIndex        =   8
      Top             =   6660
      Width           =   435
   End
   Begin VB.ListBox lstVenDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Left            =   240
      MultiSelect     =   2  'Extended
      TabIndex        =   5
      Top             =   5400
      Width           =   2475
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   24
      Top             =   8040
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   16
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   4095
      Left            =   10140
      TabIndex        =   21
      Top             =   540
      Width           =   1695
      Begin VB.CommandButton cmdMappaImpianto 
         Caption         =   "Mappa impianto"
         Height          =   435
         Left            =   120
         TabIndex        =   37
         Top             =   3480
         Width           =   1395
      End
      Begin VB.CommandButton cmdClassiSuperAree 
         Caption         =   "Classi superaree"
         Height          =   435
         Left            =   120
         TabIndex        =   34
         Top             =   2760
         Width           =   1395
      End
      Begin VB.CommandButton cmdStruttura 
         Caption         =   "Struttura"
         Height          =   435
         Left            =   120
         TabIndex        =   33
         Top             =   2040
         Width           =   1395
      End
      Begin VB.CommandButton cmdGrigliaPosti 
         Caption         =   "Griglia Posti"
         Height          =   435
         Left            =   120
         TabIndex        =   19
         Top             =   1320
         Width           =   1395
      End
      Begin VB.CommandButton cmdSuperAree 
         Caption         =   "SuperAree"
         Height          =   435
         Left            =   120
         TabIndex        =   18
         Top             =   780
         Width           =   1395
      End
      Begin VB.CommandButton cmdAree 
         Caption         =   "Aree"
         Height          =   435
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   4215
      Left            =   120
      TabIndex        =   20
      Top             =   540
      Width           =   9855
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   1380
         TabIndex        =   35
         Top             =   3360
         Width           =   8175
         Begin VB.CheckBox chkPiantaInTest 
            Caption         =   "Pianta in test su Lisclick"
            Height          =   255
            Left            =   6000
            TabIndex        =   38
            Top             =   240
            Width           =   2055
         End
         Begin VB.Label lblMappaImpianto 
            Caption         =   "Mappa impianto configurata: "
            Height          =   255
            Left            =   120
            TabIndex        =   36
            Top             =   240
            Width           =   5715
         End
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1380
         MaxLength       =   30
         TabIndex        =   0
         Top             =   420
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1380
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   840
         Width           =   8235
      End
      Begin VB.Frame fraComposizione 
         Height          =   1395
         Left            =   1380
         TabIndex        =   23
         Top             =   1920
         Width           =   8175
         Begin VB.OptionButton optNonSpecificata 
            Caption         =   "Associazione non specificata o impianto non numerato"
            Height          =   255
            Left            =   360
            TabIndex        =   4
            Top             =   960
            Width           =   5955
         End
         Begin VB.OptionButton optGrandiImpianti 
            Caption         =   "Associazione con una Griglia Posti per ciascuna Area Numerata (Grandi Impianti)"
            Height          =   255
            Left            =   360
            TabIndex        =   3
            Top             =   600
            Width           =   6135
         End
         Begin VB.OptionButton optPiccoliImpianti 
            Caption         =   "Associazione con una sola Griglia Posti (Piccoli Impianti)"
            Height          =   255
            Left            =   360
            TabIndex        =   2
            Top             =   240
            Width           =   5955
         End
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   180
         TabIndex        =   29
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   900
         Width           =   1095
      End
   End
   Begin VB.Label lblOrganizzazioni 
      Alignment       =   2  'Center
      Caption         =   "lblOrganizzazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   6240
      TabIndex        =   32
      Top             =   4920
      Width           =   5475
   End
   Begin VB.Label lblOrgDisponibili 
      Alignment       =   2  'Center
      Caption         =   "lblOrgDisponibili"
      Height          =   195
      Left            =   6240
      TabIndex        =   31
      Top             =   5160
      Width           =   2475
   End
   Begin VB.Label lblOrgSelezionate 
      Alignment       =   2  'Center
      Caption         =   "lblOrgSelezionate"
      Height          =   195
      Left            =   9240
      TabIndex        =   30
      Top             =   5160
      Width           =   2475
   End
   Begin VB.Label lblVenue 
      Alignment       =   2  'Center
      Caption         =   "lblVenue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   27
      Top             =   4920
      Width           =   5475
   End
   Begin VB.Label lblVenDisponibili 
      Alignment       =   2  'Center
      Caption         =   "lblVenDisponibili"
      Height          =   195
      Left            =   240
      TabIndex        =   26
      Top             =   5160
      Width           =   2475
   End
   Begin VB.Label lblVenSelezionati 
      Alignment       =   2  'Center
      Caption         =   "lblVenSelezionati"
      Height          =   195
      Left            =   3240
      TabIndex        =   25
      Top             =   5160
      Width           =   2475
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialePianta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private isPiantaTMaster As ValoreBooleanoEnum

Private idPiantaSelezionata As Long
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
Private nomePianta As String
Private descrizionePianta As String
Private piantaACapienzaIllimitata As ValoreBooleanoEnum
Private strCodicePiantaOrganizzazioneSelezionata As String
Private esisteGrigliaPostiAssociata As Boolean
Private listaVenueSelezionati As Collection
Private listaVenueDisponibili As Collection
Private listaOrganizzazioniSelezionate As Collection
Private listaOrganizzazioniDisponibili As Collection
'Private listaCampiValoriUnici As Collection
Private numeroVersionePianta As Long
Private isAttributiPiantaModificati As Boolean

Private idMappaImpiantoSelezionata As Long
Private nomeMappaImpiantoSelezionata As String
Private piantaInTestSuLisclick As ValoreBooleanoEnum

Private internalEvent As Boolean

Private modalit�FormCorrente As AzioneEnum
Private exitCodeFormDettagli As ExitCodeEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init(capienzaIllimitata As ValoreBooleanoEnum)

    Call Variabili_Init(capienzaIllimitata)
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            idPiantaSelezionata = idNessunElementoSelezionato
            numeroVersionePianta = 0
            isPiantaTMaster = VB_FALSO
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_CLONA
            'DA FARE
        Case Else
            'Do Nothing
    End Select
    
    Call CaricaValoriLstVenDisponibili
    Call CaricaValoriLstVenSelezionati
    Call CaricaValoriLstOrgDisponibili
    Call CaricaValoriLstOrgSelezionate
    Call AggiornaAbilitazioneControlli
    
    If piantaACapienzaIllimitata = VB_VERO Then
        fraComposizione.Enabled = False
    End If
    
    Call frmInizialePianta.Show(vbModal)
    
End Sub

Private Sub Variabili_Init(capienzaIllimitata As ValoreBooleanoEnum)
    nomePianta = ""
    descrizionePianta = ""
    isAttributiPiantaModificati = False
    isPiantaBloccataDaUtente = True
    piantaACapienzaIllimitata = capienzaIllimitata
    idMappaImpiantoSelezionata = idNessunElementoSelezionato
    nomeMappaImpiantoSelezionata = ""
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuova Pianta"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Pianta configurata"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Pianta configurata"
    End Select
    fraAzioni.Visible = (modalit�FormCorrente = A_MODIFICA)
    
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_NUOVO Then
        If piantaACapienzaIllimitata = VB_VERO Then
            cmdStruttura.Enabled = True
            cmdStruttura.Visible = True
        Else
            cmdStruttura.Enabled = False
            cmdStruttura.Visible = True
        End If
    End If

    cmdConferma.Enabled = (Trim(txtNome.Text) <> "")
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstOrgDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstOrgSelezionate.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstVenDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstVenSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdOrgDisponibile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdOrgSelezionata.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdOrgSvuotaSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdVenDidsponibile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdVenSelezionato.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdVenSvuotaSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    If piantaACapienzaIllimitata = VB_FALSO Then
        cmdAree.Enabled = (modalit�FormCorrente <> A_ELIMINA)
        cmdSuperAree.Enabled = (modalit�FormCorrente <> A_ELIMINA)
        If (listaOrganizzazioniSelezionate Is Nothing) Or (listaVenueSelezionati Is Nothing) Then
            cmdGrigliaPosti.Enabled = False
        Else
            cmdGrigliaPosti.Enabled = (modalit�FormCorrente <> A_ELIMINA And _
                                        tipoGriglia = TG_PICCOLI_IMPIANTI And _
                                        Not EsistonoElementiMancanti)
        End If
    Else
        cmdAree.Enabled = False
        cmdSuperAree.Enabled = False
        cmdGrigliaPosti.Enabled = False
    End If
    optNonSpecificata.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    optPiccoliImpianti.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    optGrandiImpianti.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    If piantaACapienzaIllimitata = VB_VERO Then
        fraComposizione.Enabled = False
        fraComposizione.Visible = False
    Else
        fraComposizione.Enabled = Not esisteGrigliaPostiAssociata
    End If
    lblVenue.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblOrganizzazioni.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblOrgDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblOrgSelezionate.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblVenue.Caption = "VENUE"
    lblVenDisponibili.Caption = "Disponibili"
    lblVenSelezionati.Caption = "Selezionati"
    lblOrganizzazioni.Caption = "ORGANIZZAZIONI"
    lblOrgDisponibili.Caption = "Disponibili (nome)"
    lblOrgSelezionate.Caption = "Selezionate (cod. pianta - nome)"

    If isPiantaTMaster Then
        cmdVenSelezionato.Enabled = False
        cmdVenDidsponibile.Enabled = False
        cmdVenSvuotaSelezionati.Enabled = False
        cmdOrgSelezionata.Enabled = False
        cmdOrgDisponibile.Enabled = False
        cmdOrgSvuotaSelezionati.Enabled = False
        cmdGrigliaPosti.Enabled = False
        ' temporaneo per test
            cmdGrigliaPosti.Enabled = True
    End If
    
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaPianta.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub Esci()
    If isAttributiPiantaModificati Then
        numeroVersionePianta = numeroVersionePianta + 1
        Call AggiornaVersioneInBaseDati
    End If
    If tipoGriglia = TG_PICCOLI_IMPIANTI Or piantaACapienzaIllimitata = VB_VERO Then
        Call SbloccaDominioPerUtente(CCDA_PIANTA, idPiantaSelezionata, isPiantaBloccataDaUtente)
    End If
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As OraDynaset
    Dim cont As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = "SELECT NOME, DESCRIZIONE, NUMEROVERSIONE, CAPIENZAILLIMITATA, PIANTAINTESTSULISCLICK"
    sql = sql & " FROM PIANTA"
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        nomePianta = rec("NOME")
        descrizionePianta = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        numeroVersionePianta = rec("NUMEROVERSIONE")
        piantaACapienzaIllimitata = IIf(rec("CAPIENZAILLIMITATA") = 0, VB_FALSO, VB_VERO)
        piantaInTestSuLisclick = IIf(rec("PIANTAINTESTSULISCLICK") = 0, VB_FALSO, VB_VERO)
    End If
    rec.Close
    
    sql = "SELECT DISTINCT P.IDPIANTA, P.NOME, P.DESCRIZIONE, P.NUMEROVERSIONE,"
    sql = sql & " G.IDPIANTA IDP,"
    sql = sql & " G.IDAREA IDA"
    sql = sql & " FROM PIANTA P, GRIGLIAPOSTI G, AREA A"
    sql = sql & " WHERE P.IDPIANTA = G.IDPIANTA"
    sql = sql & " AND P.IDPIANTA = " & idPiantaSelezionata
    sql = sql & " UNION"
    sql = sql & " SELECT DISTINCT P.IDPIANTA, P.NOME, P.DESCRIZIONE, P.NUMEROVERSIONE,"
    sql = sql & " G.IDPIANTA IDP,"
    sql = sql & " G.IDAREA IDA"
    sql = sql & " FROM PIANTA P, GRIGLIAPOSTI G, AREA A"
    sql = sql & " WHERE A.IDAREA = G.IDAREA"
    sql = sql & " AND A.IDPIANTA = P.IDPIANTA"
    sql = sql & " AND P.IDPIANTA = " & idPiantaSelezionata
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        If Not IsNull(rec("IDP")) Then
            tipoGriglia = TG_PICCOLI_IMPIANTI
            esisteGrigliaPostiAssociata = True
        ElseIf Not IsNull(rec("IDA")) Then
            tipoGriglia = TG_GRANDI_IMPIANTI
            esisteGrigliaPostiAssociata = True
        Else
            tipoGriglia = TG_NON_SPECIFICATO
            esisteGrigliaPostiAssociata = False
        End If
    Else
        tipoGriglia = TG_NON_SPECIFICATO
        esisteGrigliaPostiAssociata = False
    End If
    rec.Close
    
    risolviMappaImpianto
    
    isPiantaTMaster = VB_FALSO
'    sql = "SELECT COUNT(*) CONT FROM TMASTERPIANTA WHERE IDPIANTA = " & idPiantaSelezionata
'    Set rec = ORADB.CreateDynaset(sql, 0&)
'    If Not (rec.BOF And rec.EOF) Then
'        cont = rec("CONT")
'    End If
'    rec.Close
'
'    If cont = 1 Then
'        isPiantaTMaster = VB_VERO
'    Else
'        If cont > 1 Then
'            MsgBox "Impossibile verificare se si tratta di una pianta TMaster!"
'        End If
'    End If
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomePianta
    txtDescrizione.Text = descrizionePianta
    optGrandiImpianti.Value = (tipoGriglia = TG_GRANDI_IMPIANTI)
    optPiccoliImpianti.Value = (tipoGriglia = TG_PICCOLI_IMPIANTI)
    optNonSpecificata.Value = (tipoGriglia = TG_NON_SPECIFICATO)
    chkPiantaInTest = piantaInTestSuLisclick
    
    internalEvent = internalEventOld
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim idNuovaPianta As Long
    Dim venue As clsElementoLista
    Dim organizzazione As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
'   INSERIMENTO IN TABELLA PIANTA
    idNuovaPianta = OttieniIdentificatoreDaSequenza("SQ_PIANTA")
    sql = "INSERT INTO PIANTA (IDPIANTA, NOME, DESCRIZIONE, NUMEROVERSIONE, IDTIPOSTATOPIANTA, CAPIENZAILLIMITATA, PIANTAINTESTSULISCLICK)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaPianta & ", "
    sql = sql & SqlStringValue(nomePianta) & ", "
    sql = sql & SqlStringValue(descrizionePianta) & ", "
    sql = sql & numeroVersionePianta & ", "
    sql = sql & TSPT_ATTIVA & ", "
    sql = sql & IIf(piantaACapienzaIllimitata = VB_FALSO, "0", "1") & ", "
    sql = sql & IIf(piantaInTestSuLisclick = VB_FALSO, "0", "1") & ")"
    n = ORADB.ExecuteSQL(sql)

'   INSERIMENTO IN TABELLA VENUE_PIANTA
    If Not (listaVenueSelezionati Is Nothing) Then
        For Each venue In listaVenueSelezionati
            sql = "INSERT INTO VENUE_PIANTA (IDVENUE, IDPIANTA)"
            sql = sql & " VALUES (" & venue.idElementoLista & ", "
            sql = sql & idNuovaPianta & ")"
            n = ORADB.ExecuteSQL(sql)
        Next venue
    End If
    
'   INSERIMENTO IN TABELLA ORGANIZZAZIONE_PIANTA
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        For Each organizzazione In listaOrganizzazioniSelezionate
            sql = "INSERT INTO ORGANIZZAZIONE_PIANTA (IDORGANIZZAZIONE, CODICEPIANTA, IDPIANTA)"
            sql = sql & " VALUES (" & organizzazione.idElementoLista & ", "
            sql = sql & "'" & CStr(organizzazione.codiceElementoLista) & "', "
            sql = sql & idNuovaPianta & ")"
'            SETAConnection.Execute sql, n, adCmdText
            n = ORADB.ExecuteSQL(sql)
        Next organizzazione
    End If
    
'   INSERIMENTO IN TABELLA ORGANIZZAZIONE_PIANTA
    sql = "INSERT INTO CLASSESUPERAREA (IDCLASSESUPERAREA, NOME, DESCRIZIONE, IDPIANTA)"
    sql = sql & " VALUES (SQ_CLASSESUPERAREA.NEXTVAL, 'SETTORI ORDINARI', 'SETTORI ORDINARI', " & idNuovaPianta & ")"
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call frmSceltaPianta.SetIdRecordSelezionato(idNuovaPianta)
    Call SetIdPiantaSelezionata(idNuovaPianta)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdAree_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraAree
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdClassiSuperAree_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraClassiSuperaree
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraClassiSuperaree()
    Call frmConfigurazionePiantaClassiSuperaree.SetNomePiantaSelezionata(nomePianta)
    Call frmConfigurazionePiantaClassiSuperaree.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazionePiantaClassiSuperaree.Init
End Sub

Private Sub cmdGrigliaPosti_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraGrigliaPosti
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraGrigliaPosti()
    If NumeroAreeAssociateAPianta > 0 Then
        If tipoGriglia = TG_PICCOLI_IMPIANTI Then
            If isPiantaBloccataDaUtente Then
                Call CaricaFormInizialePiantaGrigliaPosti
                Call CaricaDallaBaseDati
                Call AssegnaValoriCampi
            Else
                Call frmMessaggio.Visualizza("NotificaRecordBloccato")
            End If
        Else
            Call CaricaFormInizialePiantaGrigliaPosti
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        End If
    Else
        Call frmMessaggio.Visualizza("AvvertimentoPiantaSenzaAree")
    End If
End Sub

Private Sub CaricaFormInizialePiantaGrigliaPosti()
    Call frmInizialePiantaGrigliaPosti.SetTipoGriglia(tipoGriglia)
    Call frmInizialePiantaGrigliaPosti.SetNomePiantaSelezionata(nomePianta)
    Call frmInizialePiantaGrigliaPosti.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmInizialePiantaGrigliaPosti.SetEsisteGrigliaPostiAssociata(esisteGrigliaPostiAssociata)
    Call frmInizialePiantaGrigliaPosti.Init
End Sub

Private Sub cmdMappaImpianto_Click()
    Call CaricaFormPiantaMappaImpianto
End Sub

Private Sub CaricaFormPiantaMappaImpianto()
    Call frmStrutturaPiantaMappaImpianto.SetNomePiantaSelezionata(nomePianta)
    Call frmStrutturaPiantaMappaImpianto.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmStrutturaPiantaMappaImpianto.Init
    Call risolviMappaImpianto
End Sub

Private Sub cmdStruttura_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraPiantaCapienzaIllimitata
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraPiantaCapienzaIllimitata()
    Call CaricaFormStrutturaPiantaCapienzaIllimitata
End Sub

Private Sub CaricaFormStrutturaPiantaCapienzaIllimitata()
    Call frmStrutturaPiantaCapienzaIllimitata.SetNomePiantaSelezionata(nomePianta)
    Call frmStrutturaPiantaCapienzaIllimitata.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmStrutturaPiantaCapienzaIllimitata.Init
End Sub

Private Sub cmdSuperAree_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraSuperArea
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraSuperArea()
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        If isPiantaBloccataDaUtente Then
            Call CaricaFormConfigurazionePiantaSuperarea
        Else
            Call frmMessaggio.Visualizza("NotificaRecordBloccato")
        End If
    Else
        Call CaricaFormConfigurazionePiantaSuperarea
    End If
End Sub

Private Sub CaricaFormConfigurazionePiantaSuperarea()
    Call frmConfigurazionePiantaSuperarea.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazionePiantaSuperarea.SetNomePiantaSelezionata(nomePianta)
    Call frmConfigurazionePiantaSuperarea.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazionePiantaSuperarea.Init
End Sub

Private Sub ConfiguraAree()
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        If isPiantaBloccataDaUtente Then
            Call frmConfigurazionePiantaArea.SetIdPiantaSelezionata(idPiantaSelezionata)
            Call frmConfigurazionePiantaArea.SetNomePiantaSelezionata(nomePianta)
            Call frmConfigurazionePiantaArea.SetTipoGriglia(tipoGriglia)
            Call frmConfigurazionePiantaArea.SetGestioneExitCode(EC_NON_SPECIFICATO)
            Call frmConfigurazionePiantaArea.Init
        Else
            Call frmMessaggio.Visualizza("NotificaRecordBloccato")
        End If
    Else
        Call frmConfigurazionePiantaArea.SetIdPiantaSelezionata(idPiantaSelezionata)
        Call frmConfigurazionePiantaArea.SetNomePiantaSelezionata(nomePianta)
        Call frmConfigurazionePiantaArea.SetTipoGriglia(tipoGriglia)
        Call frmConfigurazionePiantaArea.SetGestioneExitCode(EC_NON_SPECIFICATO)
        Call frmConfigurazionePiantaArea.Init
    End If
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim isModificabile As Boolean
    
    numeroVersionePianta = numeroVersionePianta + 1
    Select Case modalit�FormCorrente
        Case A_NUOVO
            If valoriCampiOK = True Then
                Call InserisciNellaBaseDati
                Call CreaSuperareaServizio
                If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                    Call BloccaDominioPerUtente(CCDA_PIANTA, idPiantaSelezionata, isPiantaBloccataDaUtente)
                End If
                Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                If EsistonoElementiMancanti Then
                    Call frmMessaggio.Visualizza("AvvertimentoPiantaSenzaOrganizzazioneVenue")
                End If
                Call SetModalit�Form(A_MODIFICA)
                Call AggiornaAbilitazioneControlli
            End If
        Case A_MODIFICA
            isModificabile = True
            If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                If isPiantaBloccataDaUtente Then
                    isModificabile = True
                Else
                    isModificabile = False
                    Call frmMessaggio.Visualizza("NotificaRecordBloccato")
                End If
            End If
            If isModificabile Then
                If valoriCampiOK = True Then
                    Call AggiornaNellaBaseDati
                    Call frmMessaggio.Visualizza("NotificaModificaDati")
                    If EsistonoElementiMancanti Then
                        Call frmMessaggio.Visualizza("AvvertimentoPiantaSenzaOrganizzazioneVenue")
                    End If
                    Call Esci
                End If
            End If
        Case A_ELIMINA
            isModificabile = True
            If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                If isPiantaBloccataDaUtente Then
                    isModificabile = True
                Else
                    isModificabile = False
                    Call frmMessaggio.Visualizza("NotificaRecordBloccato")
                End If
            End If
            If isModificabile Then
                Call EliminaDallaBaseDati
                Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
                Call Esci
            End If
    End Select
    Call frmSceltaPianta.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub cmdOrgDisponibile_Click()
    Call SpostaInLstOrgDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdOrgSelezionata_Click()
    Call SpostaInLstOrgSelezionate
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormDettagliOrganizzazionePianta()
    Call frmDettagliOrganizzazionePianta.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmDettagliOrganizzazionePianta.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmDettagliOrganizzazionePianta.SetNomePianta(nomePianta)
    Call frmDettagliOrganizzazionePianta.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmDettagliOrganizzazionePianta.SetOperazioneSuPianta(A_NUOVO)
    Call frmDettagliOrganizzazionePianta.Init
End Sub

Private Sub cmdOrgSvuotaSelezionati_Click()
    Call SvuotaOrgSelezionate
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdVenDidsponibile_Click()
    Call SpostaInLstVenDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdVenSelezionato_Click()
    Call SpostaInLstVenSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdVenSvuotaSelezionati_Click()
    Call SvuotaVenSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstOrgDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstOrgDisponibili, lstOrgDisponibili.Text)
End Sub

Private Sub lstOrgSelezionate_Click()
    Call VisualizzaListBoxToolTip(lstOrgSelezionate, lstOrgSelezionate.Text)
End Sub

Private Sub lstVenDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstVenDisponibili, lstVenDisponibili.Text)
End Sub

Private Sub lstVenSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstVenSelezionati, lstVenSelezionati.Text)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim venue As clsElementoLista
    Dim organizzazione As clsElementoLista

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori

'   AGGIORNAMENTO IN TABELLA PIANTA
    sql = "UPDATE PIANTA SET"
    sql = sql & " NOME = " & SqlStringValue(nomePianta) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizionePianta) & ","
    sql = sql & " NUMEROVERSIONE = " & numeroVersionePianta & ","
    sql = sql & " PIANTAINTESTSULISCLICK = " & IIf(piantaInTestSuLisclick = VB_FALSO, "0", "1")
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)
    
'   AGGIORNAMENTO IN TABELLA VENUE_PIANTA
    If Not (listaVenueSelezionati Is Nothing) Then
        sql = "DELETE FROM VENUE_PIANTA WHERE IDPIANTA = " & idPiantaSelezionata
        SETAConnection.Execute sql, n, adCmdText
        For Each venue In listaVenueSelezionati
            sql = "INSERT INTO VENUE_PIANTA (IDPIANTA, IDVENUE)"
            sql = sql & " VALUES (" & idPiantaSelezionata & ", "
            sql = sql & venue.idElementoLista & ")"
'            SETAConnection.Execute sql, n, adCmdText
            n = ORADB.ExecuteSQL(sql)
        Next venue
    End If
            
'   AGGIORNAMENTO IN TABELLA ORGANIZZAZIONE_PIANTA
    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        sql = "DELETE FROM ORGANIZZAZIONE_PIANTA WHERE IDPIANTA = " & idPiantaSelezionata
        SETAConnection.Execute sql, n, adCmdText
        For Each organizzazione In listaOrganizzazioniSelezionate
            sql = "INSERT INTO ORGANIZZAZIONE_PIANTA (IDPIANTA, CODICEPIANTA, IDORGANIZZAZIONE)"
            sql = sql & " VALUES (" & idPiantaSelezionata & ", "
            sql = sql & "'" & CStr(organizzazione.codiceElementoLista) & "', "
            sql = sql & organizzazione.idElementoLista & ")"
'            SETAConnection.Execute sql, n, adCmdText
            n = ORADB.ExecuteSQL(sql)
        Next organizzazione
    End If
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "DELETE FROM VENUE_PIANTA WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM ORGANIZZAZIONE_PIANTA WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)
    sql = "DELETE FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)
'    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        sql = "DELETE FROM GRIGLIAPOSTI WHERE IDPIANTA = " & idPiantaSelezionata
        n = ORADB.ExecuteSQL(sql)
        Call SbloccaDominioPerUtente(CCDA_PIANTA, idPiantaSelezionata, isPiantaBloccataDaUtente)
'    End If
    sql = "DELETE FROM PIANTA WHERE IDPIANTA = " & idPiantaSelezionata
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub CaricaValoriLstVenDisponibili()
    Dim sql As String
    Dim rec As OraDynaset
    Dim chiaveVenue As String
    Dim venueCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    Set listaVenueDisponibili = New Collection
    If idPiantaSelezionata >= 0 Then
        sql = "SELECT V.IDVENUE ID, NOME FROM VENUE V, VENUE_PIANTA V_P"
        sql = sql & " WHERE V.IDVENUE = V_P.IDVENUE(+)"
        sql = sql & " AND V_P.IDVENUE IS NULL"
        sql = sql & " AND V_P.IDPIANTA(+) = " & idPiantaSelezionata
        sql = sql & " ORDER BY NOME"
    Else
        sql = "SELECT IDVENUE ID, NOME FROM VENUE ORDER BY NOME"
    End If
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set venueCorrente = New clsElementoLista
            venueCorrente.idElementoLista = rec("ID").Value
            venueCorrente.nomeElementoLista = rec("NOME")
            venueCorrente.descrizioneElementoLista = rec("NOME")
            chiaveVenue = ChiaveId(venueCorrente.idElementoLista)
            Call listaVenueDisponibili.Add(venueCorrente, chiaveVenue)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call lstVenDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstVenSelezionati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim chiaveVenue As String
    Dim venueCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    Set listaVenueSelezionati = New Collection
    If idPiantaSelezionata >= 0 Then
        sql = "SELECT VP.IDVENUE ID, NOME"
        sql = sql & " FROM VENUE V, VENUE_PIANTA VP"
        sql = sql & " WHERE VP.IDPIANTA = " & idPiantaSelezionata
        sql = sql & " AND VP.IDVENUE = V.IDVENUE"
        sql = sql & " ORDER BY NOME"
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set venueCorrente = New clsElementoLista
                venueCorrente.idElementoLista = rec("ID").Value
                venueCorrente.nomeElementoLista = rec("NOME")
                venueCorrente.descrizioneElementoLista = rec("NOME")
                chiaveVenue = ChiaveId(venueCorrente.idElementoLista)
                Call listaVenueSelezionati.Add(venueCorrente, chiaveVenue)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ORADB.CommitTrans
        
        Call ChiudiConnessioneBD_ORA
        
        Call lstVenSelezionati_Init
    End If
    
End Sub

Private Sub lstVenDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim venue As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstVenDisponibili.Clear

    If Not (listaVenueDisponibili Is Nothing) Then
        i = 1
        For Each venue In listaVenueDisponibili
            lstVenDisponibili.AddItem venue.descrizioneElementoLista
            lstVenDisponibili.ItemData(i - 1) = venue.idElementoLista
            i = i + 1
        Next venue
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstVenSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim venue As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstVenSelezionati.Clear

    If Not (listaVenueSelezionati Is Nothing) Then
        i = 1
        For Each venue In listaVenueSelezionati
            lstVenSelezionati.AddItem venue.descrizioneElementoLista
            lstVenSelezionati.ItemData(i - 1) = venue.idElementoLista
            i = i + 1
        Next venue
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaInLstVenDisponibili()
    Dim i As Integer
    Dim idVenue As Long
    Dim venue As clsElementoLista
    Dim chiaveVenue As String
    
    For i = 1 To lstVenSelezionati.ListCount
        If lstVenSelezionati.Selected(i - 1) Then
            idVenue = lstVenSelezionati.ItemData(i - 1)
            chiaveVenue = ChiaveId(idVenue)
            Set venue = listaVenueSelezionati.Item(chiaveVenue)
            Call listaVenueDisponibili.Add(venue, chiaveVenue)
            Call listaVenueSelezionati.Remove(chiaveVenue)
        End If
    Next i
    Call lstVenDisponibili_Init
    Call lstVenSelezionati_Init
End Sub

Private Sub SpostaInLstVenSelezionati()
    Dim i As Integer
    Dim idVenue As Long
    Dim venue As clsElementoLista
    Dim chiaveVenue As String
    
    For i = 1 To lstVenDisponibili.ListCount
        If lstVenDisponibili.Selected(i - 1) Then
            idVenue = lstVenDisponibili.ItemData(i - 1)
            chiaveVenue = ChiaveId(idVenue)
            Set venue = listaVenueDisponibili.Item(chiaveVenue)
            Call listaVenueSelezionati.Add(venue, chiaveVenue)
            Call listaVenueDisponibili.Remove(chiaveVenue)
        End If
    Next i
    Call lstVenDisponibili_Init
    Call lstVenSelezionati_Init
End Sub

Private Sub SvuotaVenSelezionati()
    Dim venue As clsElementoLista
    Dim chiaveVenue As String
    
    For Each venue In listaVenueSelezionati
        chiaveVenue = ChiaveId(venue.idElementoLista)
        Call listaVenueDisponibili.Add(venue, chiaveVenue)
    Next venue
    Set listaVenueSelezionati = Nothing
    Set listaVenueSelezionati = New Collection
    
    Call lstVenDisponibili_Init
    Call lstVenSelezionati_Init
End Sub

Private Sub CaricaValoriLstOrgDisponibili()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    Set listaOrganizzazioniDisponibili = New Collection
    If idPiantaSelezionata >= 0 Then
        sql = "SELECT O.IDORGANIZZAZIONE ID, O.NOME || ' - ' || TSO.NOME ORG, O.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO"
        sql = sql & " FROM ORGANIZZAZIONE O, TIPOSTATOORGANIZZAZIONE TSO, ORGANIZZAZIONE_PIANTA O_P"
        sql = sql & " WHERE O.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE"
        sql = sql & " AND O.IDORGANIZZAZIONE = O_P.IDORGANIZZAZIONE(+)"
        sql = sql & " AND O_P.IDORGANIZZAZIONE IS NULL"
        sql = sql & " AND O_P.IDPIANTA(+) = " & idPiantaSelezionata
        sql = sql & " ORDER BY ORG"
    Else
        sql = "SELECT O.IDORGANIZZAZIONE ID, O.NOME || ' - ' || TSO.NOME ORG, O.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO"
        sql = sql & " FROM ORGANIZZAZIONE O, TIPOSTATOORGANIZZAZIONE TSO"
        sql = sql & " WHERE O.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE"
        sql = sql & " ORDER BY ORG"
    End If
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set organizzazioneCorrente = New clsElementoLista
            organizzazioneCorrente.idElementoLista = rec("ID").Value
            organizzazioneCorrente.idAttributoElementoLista = rec("TIPOSTATO").Value
            organizzazioneCorrente.nomeElementoLista = rec("ORG")
            organizzazioneCorrente.descrizioneElementoLista = rec("ORG")
            chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
            Call listaOrganizzazioniDisponibili.Add(organizzazioneCorrente, chiaveOrganizzazione)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call lstOrgDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstOrgSelezionate()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim nomeOrganizzazione As String
    Dim codicePiantaOrganizzazione As Integer
    Dim idOrganizzazione As Long
    Dim idTipoStatoOrganizzazione As TipoStatoOrganizzazioneEnum
    Dim chiaveOrganizzazione As String
    Dim organizzazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    Set listaOrganizzazioniSelezionate = New Collection
    If idPiantaSelezionata >= 0 Then
        sql = "SELECT OP.IDORGANIZZAZIONE ID, OP.CODICEPIANTA,"
        sql = sql & " O.NOME || ' - ' || TSO.NOME ORG, O.IDTIPOSTATOORGANIZZAZIONE TIPOSTATO"
        sql = sql & " FROM ORGANIZZAZIONE O, ORGANIZZAZIONE_PIANTA OP, TIPOSTATOORGANIZZAZIONE TSO"
        sql = sql & " WHERE O.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE"
        sql = sql & " AND OP.IDPIANTA = " & idPiantaSelezionata
        sql = sql & " AND OP.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE"
        sql = sql & " ORDER BY ORG"
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set organizzazioneCorrente = New clsElementoLista
                organizzazioneCorrente.codiceElementoLista = rec("CODICEPIANTA").Value
                organizzazioneCorrente.nomeElementoLista = rec("ORG")
                organizzazioneCorrente.idElementoLista = rec("ID").Value
                organizzazioneCorrente.idAttributoElementoLista = rec("TIPOSTATO").Value
                organizzazioneCorrente.descrizioneElementoLista = organizzazioneCorrente.codiceElementoLista & _
                    " - " & organizzazioneCorrente.nomeElementoLista
                chiaveOrganizzazione = ChiaveId(organizzazioneCorrente.idElementoLista)
                Call listaOrganizzazioniSelezionate.Add(organizzazioneCorrente, chiaveOrganizzazione)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ORADB.CommitTrans
        
        Call ChiudiConnessioneBD_ORA
        
        Call lstOrgSelezionate_Init
    End If
    
End Sub

Private Sub lstOrgDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOrgDisponibili.Clear

    If Not (listaOrganizzazioniDisponibili Is Nothing) Then
        i = 1
        For Each organizzazione In listaOrganizzazioniDisponibili
            lstOrgDisponibili.AddItem organizzazione.descrizioneElementoLista
            lstOrgDisponibili.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOrgSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim organizzazione As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOrgSelezionate.Clear

    If Not (listaOrganizzazioniSelezionate Is Nothing) Then
        i = 1
        For Each organizzazione In listaOrganizzazioniSelezionate
            lstOrgSelezionate.AddItem organizzazione.descrizioneElementoLista
            lstOrgSelezionate.ItemData(i - 1) = organizzazione.idElementoLista
            i = i + 1
        Next organizzazione
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SvuotaOrgSelezionate()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    For Each organizzazione In listaOrganizzazioniSelezionate
        isConfigurabile = True
        Select Case organizzazione.idAttributoElementoLista
            Case TSO_IN_CONFIGURAZIONE
                isConfigurabile = True
            Case TSO_ATTIVA
                Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            Case TSO_ATTIVABILE
                isConfigurabile = False
        End Select
        If isConfigurabile Then
            chiaveOrganizzazione = ChiaveId(organizzazione.idElementoLista)
            organizzazione.codiceElementoLista = ""
            organizzazione.descrizioneElementoLista = organizzazione.nomeElementoLista
            Call listaOrganizzazioniDisponibili.Add(organizzazione, chiaveOrganizzazione)
            Call listaOrganizzazioniSelezionate.Remove(chiaveOrganizzazione)
        End If
    Next organizzazione
    Call lstOrgDisponibili_Init
    Call lstOrgSelezionate_Init
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    Set listaNonConformit� = New Collection
    condizioneSql = ""
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_ELIMINA Then
        condizioneSql = "IDPIANTA <> " & idPiantaSelezionata
    End If
    
    nomePianta = Trim(txtNome.Text)
    If ViolataUnicit�("PIANTA", "NOME = " & SqlStringValue(nomePianta), "", _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomePianta) & _
            " � gi� presente in DB;")
    End If
    descrizionePianta = Trim(txtDescrizione.Text)
    piantaInTestSuLisclick = IIf(chkPiantaInTest.Value = vbChecked, VB_VERO, VB_FALSO)
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function

Private Function EsistonoElementiMancanti() As Boolean
    
    If (listaOrganizzazioniSelezionate.count = 0) Or (listaVenueSelezionati.count = 0) Then
        EsistonoElementiMancanti = True
    Else
        EsistonoElementiMancanti = False
    End If
End Function

Private Sub optGrandiImpianti_Click()
    tipoGriglia = TG_GRANDI_IMPIANTI
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPiccoliImpianti_Click()
    tipoGriglia = TG_PICCOLI_IMPIANTI
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optNonSpecificata_Click()
    tipoGriglia = TG_NON_SPECIFICATO
    Call AggiornaAbilitazioneControlli
End Sub

Private Function NumeroAreeAssociateAPianta() As Integer
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = "SELECT COUNT(IDAREA) CONT"
    sql = sql & " FROM AREA"
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
    sql = sql & " AND IDTIPOAREA = " & TA_AREA_NUMERATA
'    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    NumeroAreeAssociateAPianta = rec("CONT")
    rec.Close

    Call ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA

End Function

Public Sub SetCodicePiantaOrganizzazioneSelezionata(cod As String)
    strCodicePiantaOrganizzazioneSelezionata = cod
End Sub

Public Sub SetExitCodeFormDettagliPiantaOrganizzazione(exCode As ExitCodeEnum)
    exitCodeFormDettagli = exCode
End Sub

Private Sub CreaSuperareaServizio()
    Dim sql As String
    Dim idNuovaSuperarea As Long
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    idNuovaSuperarea = OttieniIdentificatoreDaSequenza("SQ_AREA")
    sql = "INSERT INTO AREA (IDAREA, NOME, DESCRIZIONE, DESCRIZIONEALTERNATIVA, CODICE,"
    sql = sql & " INDICEDIPREFERIBILITA, CAPIENZA, IDPIANTA, IDTIPOAREA, IDORDINEDIPOSTOSIAE,"
    sql = sql & " IDAREA_PADRE, NUMEROVERSIONE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaSuperarea & ", "
    sql = sql & "'Superarea Servizio', "
    sql = sql & "NULL, "
    sql = sql & "'Superarea Servizio', "
    sql = sql & "'SER', "
    sql = sql & "NULL, "
    sql = sql & "NULL, "
    sql = sql & idPiantaSelezionata & ", "
    sql = sql & TA_SUPERAREA_SERVIZIO & ", "
    sql = sql & "NULL, "
    sql = sql & "NULL, "
    sql = sql & numeroVersionePianta & ")"
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
End Sub

Private Sub AggiornaVersioneInBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

    sql = "UPDATE PIANTA SET NUMEROVERSIONE = " & numeroVersionePianta
    sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    isAttributiPiantaModificati = False
End Sub

Public Sub SetIsAttributiPiantaModificati(isAttrMod As Boolean)
    isAttributiPiantaModificati = isAttrMod
End Sub

Private Sub SpostaInLstOrgSelezionate()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    idOrganizzazioneSelezionata = lstOrgDisponibili.ItemData(lstOrgDisponibili.ListIndex)
    chiaveOrganizzazione = ChiaveId(idOrganizzazioneSelezionata)
    Set organizzazione = listaOrganizzazioniDisponibili.Item(chiaveOrganizzazione)
    nomeOrganizzazioneSelezionata = organizzazione.nomeElementoLista
    Call CaricaFormDettagliOrganizzazionePianta
    If exitCodeFormDettagli = EC_CONFERMA Then
        isConfigurabile = True
        Select Case organizzazione.idAttributoElementoLista
            Case TSO_IN_CONFIGURAZIONE
                isConfigurabile = True
            Case TSO_ATTIVA
                Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            Case TSO_ATTIVABILE
                isConfigurabile = False
        End Select
        If isConfigurabile Then
            organizzazione.codiceElementoLista = strCodicePiantaOrganizzazioneSelezionata
            organizzazione.descrizioneElementoLista = organizzazione.codiceElementoLista & _
                " - " & organizzazione.nomeElementoLista
            Call listaOrganizzazioniSelezionate.Add(organizzazione, chiaveOrganizzazione)
            Call listaOrganizzazioniDisponibili.Remove(chiaveOrganizzazione)
        End If
        Call lstOrgDisponibili_Init
        Call lstOrgSelezionate_Init
    End If
End Sub

Private Sub SpostaInLstOrgDisponibili()
    Dim isConfigurabile As Boolean
    Dim organizzazione As clsElementoLista
    Dim chiaveOrganizzazione As String
    
    idOrganizzazioneSelezionata = lstOrgSelezionate.ItemData(lstOrgSelezionate.ListIndex)
    chiaveOrganizzazione = ChiaveId(idOrganizzazioneSelezionata)
    Set organizzazione = listaOrganizzazioniSelezionate.Item(chiaveOrganizzazione)
    isConfigurabile = True
    Select Case organizzazione.idAttributoElementoLista
        Case TSO_IN_CONFIGURAZIONE
            isConfigurabile = True
        Case TSO_ATTIVA
            Call frmMessaggio.Visualizza("ConfermaEditabilit�OrganizzazioneAttiva")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        Case TSO_ATTIVABILE
            isConfigurabile = False
    End Select
    If isConfigurabile Then
        organizzazione.codiceElementoLista = ""
        organizzazione.descrizioneElementoLista = organizzazione.nomeElementoLista
        Call listaOrganizzazioniDisponibili.Add(organizzazione, chiaveOrganizzazione)
        Call listaOrganizzazioniSelezionate.Remove(chiaveOrganizzazione)
    End If
    Call lstOrgDisponibili_Init
    Call lstOrgSelezionate_Init
End Sub

Private Sub risolviMappaImpianto()
    Dim rec As OraDynaset
    Dim sql As String
    
    idMappaImpiantoSelezionata = idNessunElementoSelezionato
    lblMappaImpianto.Caption = "Mappa impianto configurata: "
    sql = "SELECT MI.IDMAPPAIMPIANTO, MI.NOME" & _
        " FROM PIANTA PT, MAPPAIMPIANTO MI" & _
        " WHERE PT.IDPIANTA = " & idPiantaSelezionata & _
        " AND PT.IDMAPPAIMPIANTO = MI.IDMAPPAIMPIANTO"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idMappaImpiantoSelezionata = rec("IDMAPPAIMPIANTO")
            nomeMappaImpiantoSelezionata = rec("NOME")
            lblMappaImpianto.Caption = "Mappa impianto configurata: " & nomeMappaImpiantoSelezionata
            rec.MoveNext
        Wend
    End If
    rec.Close

    If idMappaImpiantoSelezionata = idNessunElementoSelezionato Then
        chkPiantaInTest.Enabled = False
    Else
        chkPiantaInTest.Enabled = True
    End If

End Sub

