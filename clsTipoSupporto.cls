VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAssocSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idTipoSupporto As Long
'Public nomeTipoSupporto As String
'Public codiceTipoSupporto As String
Public labelTipoSupporto As String
Public idLayoutSupporto As Long
'Public nomeLayoutSupporto As String
'Public codiceLayoutSupporto As String
Public labelLayoutSupporto As String

Public Function Clona() As clsAssocSupporto
    Dim b As clsAssocSupporto

    Set b = New clsAssocSupporto

    b.idTipoSupporto = idTipoSupporto
    b.labelTipoSupporto = labelTipoSupporto
    b.idLayoutSupporto = idLayoutSupporto
    b.labelLayoutSupporto = labelLayoutSupporto

    Set Clona = b
End Function

