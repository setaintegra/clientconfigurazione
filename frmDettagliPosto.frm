VERSION 5.00
Begin VB.Form frmDettagliPosto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli Singolo Posto"
   ClientHeight    =   2205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   3960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbArea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      TabIndex        =   2
      Top             =   1140
      Width           =   2595
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2220
      TabIndex        =   4
      Top             =   1680
      Width           =   1035
   End
   Begin VB.TextBox txtNomeFila 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   2
      TabIndex        =   0
      Top             =   240
      Width           =   735
   End
   Begin VB.TextBox txtNomePosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   3
      TabIndex        =   1
      Top             =   660
      Width           =   735
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   540
      TabIndex        =   3
      Top             =   1680
      Width           =   1035
   End
   Begin VB.Label lblArea 
      Alignment       =   1  'Right Justify
      Caption         =   "Area"
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   855
   End
   Begin VB.Label lbNomeFila 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome Fila"
      Height          =   195
      Left            =   660
      TabIndex        =   6
      Top             =   300
      Width           =   1515
   End
   Begin VB.Label lblNomePosto 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome Posto"
      Height          =   195
      Left            =   660
      TabIndex        =   5
      Top             =   720
      Width           =   1515
   End
End
Attribute VB_Name = "frmDettagliPosto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFila As String
Private nomePosto As String
Private idPosto As Long
Private idAreaSelezionata As Long
Private nomeAreaSelezionata As String
Private xPosto As Integer
Private yPosto As Integer

Private modalita As ModalitaFormDettagliEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init(nomiPostoLunghiPermessi As Boolean)
'    Select Case modalita
'        Case MFC_MODIFICA_ATTRIBUTI_POSTO
'            Call AssegnaValoriCampi
'        Case MFC_VISUALIZZA_DETTAGLI_POSTO
'            Call AssegnaValoriCampi
'    End Select
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        Call cmbArea.Clear
        Call CaricaValoriCombo(cmbArea)
        Call SelezionaElementoSuCombo(cmbArea, idAreaSelezionata)
    End If
    If nomiPostoLunghiPermessi Then
        txtNomePosto.MaxLength = 4
    End If
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()

    lbNomeFila.Enabled = True
    txtNomeFila.Enabled = True
    lblNomePosto.Enabled = True
    txtNomePosto.Enabled = True
    Select Case modalita
        Case MFC_CREA_POSTI
            frmDettagliPosto.Caption = "Creazione Singolo Posto"
            cmdConferma.Enabled = (Len(Trim(txtNomeFila.Text)) > 0 And _
                Len(Trim(txtNomePosto.Text)) > 0 And _
                cmbArea.Text <> "")
            If tipoGriglia = TG_GRANDI_IMPIANTI Then
                cmbArea.Text = nomeAreaSelezionata
                cmbArea.Enabled = False
            End If
        Case MFC_MODIFICA_ATTRIBUTI_POSTO
            frmDettagliPosto.Caption = "Modifica Singolo Posto"
            cmdConferma.Caption = "&Modifica"
            cmdAnnulla.Caption = "&Esci"
            cmdConferma.Enabled = (Len(Trim(txtNomeFila.Text)) > 0 And _
                Len(Trim(txtNomePosto.Text)) > 0 And _
                cmbArea.Text <> "")
            cmbArea.Enabled = (tipoGriglia = TG_PICCOLI_IMPIANTI)
        Case MFC_MODIFICA_ATTRIBUTI_FILA_POSTI
            frmDettagliPosto.Caption = "Modifica Fila Posti"
            cmdConferma.Caption = "&Modifica"
            cmdAnnulla.Caption = "&Esci"
            cmdConferma.Enabled = (Len(Trim(txtNomeFila.Text)) > 0 And _
                cmbArea.Text <> "")
            cmbArea.Enabled = (tipoGriglia = TG_PICCOLI_IMPIANTI)
            txtNomePosto.Enabled = False
            lblNomePosto.Enabled = False
        Case MFC_MODIFICA_ATTRIBUTI_RETTANGOLO_POSTI
            frmDettagliPosto.Caption = "Modifica Area Posti"
            cmdConferma.Caption = "&Modifica"
            cmdAnnulla.Caption = "&Esci"
            cmdConferma.Enabled = cmbArea.Text <> ""
            cmbArea.Enabled = (tipoGriglia = TG_PICCOLI_IMPIANTI)
            txtNomePosto.Enabled = False
            lblNomePosto.Enabled = False
            txtNomeFila.Enabled = False
            lbNomeFila.Enabled = False
        Case MFC_VISUALIZZA_DETTAGLI_POSTO
            frmDettagliPosto.Caption = "Dettagli Singolo Posto"
            lbNomeFila.Enabled = False
            txtNomeFila.Enabled = False
            lblNomePosto.Enabled = False
            txtNomePosto.Enabled = False
            cmdConferma.Visible = False
            cmdAnnulla.Caption = "&Esci"
        Case Else
    End Select
End Sub

Private Function ValoriCampiOK() As Boolean

On Error Resume Next

    ValoriCampiOK = True
    nomeFila = Trim(txtNomeFila.Text)
    nomePosto = Trim(txtNomePosto.Text)
    nomeAreaSelezionata = CStr(Trim(cmbArea.Text))
End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call frmConfigurazionePiantaGrigliaPosti.SetExitCodeDettagliPosti(EC_DP_ANNULLA)
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    If ValoriCampiOK Then
        Call frmConfigurazionePiantaGrigliaPosti.SetExitCodeDettagliPosti(EC_DP_CONFERMA)
        Call frmConfigurazionePiantaGrigliaPosti.SetNomePrimoPosto(nomePosto)
        Call frmConfigurazionePiantaGrigliaPosti.SetNomePrimaFila(nomeFila)
        Call frmConfigurazionePiantaGrigliaPosti.SetIdAreaSelezionata(idAreaSelezionata)
        Call frmConfigurazionePiantaGrigliaPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
        Unload Me
    End If
End Sub

Private Sub txtNomeFila_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomePosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtIndiceDiPreferibilita_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetModalitaForm(moda As ModalitaFormDettagliEnum)
    modalita = moda
End Sub

Public Sub SetNomeFila(n As String)
    nomeFila = n
End Sub

Public Sub SetNomePosto(p As String)
    nomePosto = p
End Sub

Public Sub SetIdPosto(id As Long)
    idPosto = id
End Sub

Private Sub AssegnaValoriCampi()
    txtNomeFila.Text = ""
    txtNomePosto.Text = ""
    cmbArea.Text = ""
    Select Case modalita
        Case MFC_MODIFICA_ATTRIBUTI_POSTO
            txtNomeFila.Text = Trim(nomeFila)
            txtNomePosto.Text = Trim(nomePosto)
            cmbArea.Text = Trim(nomeAreaSelezionata)
        Case MFC_MODIFICA_ATTRIBUTI_FILA_POSTI
            txtNomeFila.Text = Trim(nomeFila)
            cmbArea.Text = Trim(nomeAreaSelezionata)
        Case MFC_MODIFICA_ATTRIBUTI_RETTANGOLO_POSTI
            cmbArea.Text = Trim(nomeAreaSelezionata)
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox)
    Dim i As Integer
    Dim area As classeArea
    
'    For i = 1 To UBound(elencoAreeAssociateAPianta)
'        cmb.AddItem (elencoAreeAssociateAPianta(i).nomeArea)
'        cmb.ItemData(i - 1) = elencoAreeAssociateAPianta(i).idArea
'    Next i
    i = 1
    For Each area In listaAreeAssociateAPianta
        cmb.AddItem area.nomeArea
        cmb.ItemData(i - 1) = area.idArea
        i = i + 1
    Next area
End Sub

Private Sub cmbArea_Click()
    Call cmbArea_Update
End Sub

Private Sub cmbArea_Update()
    idAreaSelezionata = cmbArea.ItemData(cmbArea.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer

    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If

End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetXPosto(xP As Integer)
    xPosto = xP
End Sub

Public Sub SetYPosto(yP As Integer)
    yPosto = yP
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub

