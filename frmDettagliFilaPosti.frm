VERSION 5.00
Begin VB.Form frmDettagliFilaPosti 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli Fila Posti"
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   3960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbArea 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1560
      TabIndex        =   4
      Top             =   2280
      Width           =   2115
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2220
      TabIndex        =   6
      Top             =   2760
      Width           =   1035
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   540
      TabIndex        =   5
      Top             =   2760
      Width           =   1035
   End
   Begin VB.TextBox txtStep 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      TabIndex        =   3
      Top             =   1800
      Width           =   735
   End
   Begin VB.TextBox txtNomeUltimoPosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   3
      TabIndex        =   2
      Top             =   1380
      Width           =   735
   End
   Begin VB.TextBox txtNomePrimoPosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   3
      TabIndex        =   1
      Top             =   960
      Width           =   735
   End
   Begin VB.TextBox txtNomeFila 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      MaxLength       =   2
      TabIndex        =   0
      Top             =   540
      Width           =   735
   End
   Begin VB.Label lblArea 
      Alignment       =   1  'Right Justify
      Caption         =   "Area"
      Height          =   195
      Left            =   480
      TabIndex        =   13
      Top             =   2340
      Width           =   975
   End
   Begin VB.Label lblNumeroPosti2 
      Caption         =   "999"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   3120
      TabIndex        =   12
      Top             =   120
      Width           =   375
   End
   Begin VB.Label lblNumeroPosti1 
      Caption         =   "Numero max Posti inseribili"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   180
      TabIndex        =   11
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label lblStep 
      Alignment       =   1  'Right Justify
      Caption         =   "Step"
      Height          =   195
      Left            =   660
      TabIndex        =   10
      Top             =   1860
      Width           =   1515
   End
   Begin VB.Label lblNomeUltimoPosto 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome Ultimo Posto"
      Height          =   195
      Left            =   660
      TabIndex        =   9
      Top             =   1440
      Width           =   1515
   End
   Begin VB.Label lblNomePrimoPosto 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome primo Posto"
      Height          =   195
      Left            =   660
      TabIndex        =   8
      Top             =   1020
      Width           =   1515
   End
   Begin VB.Label lbNomeFila 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome Fila"
      Height          =   195
      Left            =   660
      TabIndex        =   7
      Top             =   600
      Width           =   1515
   End
End
Attribute VB_Name = "frmDettagliFilaPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFila As String
Private nomePrimoPosto As String
Private nomeUltimoPosto As String
Private step As Integer
Private numeroMaxPostiInFila As Integer
Private numeroPostiInFila As Integer
Private idAreaSelezionata As Long
Private nomeAreaSelezionata As String
Private nomiPostoLunghiPermessi As Boolean

Private modalita As ModalitaFormDettagliEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init(parNomiPostoLunghiPermessi As Boolean)
    Call AggiornaAbilitazioneControlli
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        Call cmbArea.Clear
        Call CaricaValoriCombo(cmbArea)
    End If
    nomiPostoLunghiPermessi = parNomiPostoLunghiPermessi
    If nomiPostoLunghiPermessi Then
        txtNomePrimoPosto.MaxLength = 4
        txtNomeUltimoPosto.MaxLength = 4
    End If
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    lbNomeFila.Enabled = True
    txtNomeFila.Enabled = True
    txtNomePrimoPosto.Enabled = True
    lblNomePrimoPosto.Enabled = True
    txtNomeUltimoPosto.Enabled = True
    lblNomeUltimoPosto.Enabled = True
    txtStep.Enabled = True
    lblStep.Enabled = True
    Select Case modalita
        Case MFC_CREA_POSTI
            frmDettagliFilaPosti.Caption = "Creazione Fila Posti"
            cmdConferma.Enabled = (Len(Trim(txtNomeFila.Text)) > 0 And _
                Len(Trim(txtNomePrimoPosto.Text)) > 0 And _
                Len(Trim(txtNomeUltimoPosto.Text)) > 0 And _
                Len(Trim(txtStep.Text)) > 0 And _
                cmbArea.Text <> "")
            lblNumeroPosti2.Caption = CStr(numeroMaxPostiInFila)
            If tipoGriglia = TG_GRANDI_IMPIANTI Then
                cmbArea.Text = nomeAreaSelezionata
                cmbArea.Enabled = False
            End If
        Case MFC_MODIFICA_ATTRIBUTI_POSTO
            frmDettagliFilaPosti.Caption = "Modifica Fila Posti"
            cmdConferma.Caption = "&Modifica"
            cmdAnnulla.Caption = "&Esci"
            cmbArea.Enabled = (tipoGriglia = TG_PICCOLI_IMPIANTI)
            cmdConferma.Enabled = (Len(Trim(txtNomeFila.Text)) > 0 And _
                Len(Trim(txtNomePrimoPosto.Text)) > 0 And _
                Len(Trim(txtNomeUltimoPosto.Text)) > 0 And _
                Len(Trim(txtStep.Text)) > 0 And _
                cmbArea.Text <> "")
        Case Else
    End Select
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaValoriDaConfermare As Collection

On Error Resume Next

    ValoriCampiOK = True
    Set listaValoriDaConfermare = New Collection
    
    nomeFila = Trim(txtNomeFila.Text)
    nomePrimoPosto = Trim(txtNomePrimoPosto.Text)
    nomeUltimoPosto = Trim(txtNomeUltimoPosto.Text)
    step = CLng(Trim(txtStep.Text))
    nomeAreaSelezionata = CStr(Trim(cmbArea.Text))
    Call CalcolaNumeroPostiInFila
    If numeroPostiInFila > numeroMaxPostiInFila Then
'        Call frmMessaggio.Visualizza("AvvertimentoSuperamentoLimitePosti")
'        If frmMessaggio.exitCode = EC_ANNULLA Then
'            ValoriCampiOK = False
            Call listaValoriDaConfermare.Add("- Il numero posti č maggiore del massimo; i posti verranno disegnati sovrapposti;")
'        End If
    End If
    
'    If listaNonConformitā.count > 0 Then
'        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
'    Else
        If listaValoriDaConfermare.count > 0 Then
            Call frmMessaggio.Visualizza("ConfermaValori", ArgomentoMessaggio(listaValoriDaConfermare))
            If frmMessaggio.exitCode = EC_ANNULLA Then
                ValoriCampiOK = False
            End If
        End If
'    End If
    
End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call frmConfigurazionePiantaGrigliaPosti.SetExitCodeDettagliPosti(EC_DP_ANNULLA)
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    If ValoriCampiOK Then
        If IsUltimoNomePostoCorretto(nomePrimoPosto, numeroPostiInFila, step, nomiPostoLunghiPermessi) Then
            Call frmConfigurazionePiantaGrigliaPosti.SetExitCodeDettagliPosti(EC_DP_CONFERMA)
            Call frmConfigurazionePiantaGrigliaPosti.SetNumeroPostiInFila(numeroPostiInFila)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomePrimoPosto(nomePrimoPosto)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomeUltimoPosto(nomeUltimoPosto)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomePrimaFila(nomeFila)
            Call frmConfigurazionePiantaGrigliaPosti.SetStepPosti(step)
            Call frmConfigurazionePiantaGrigliaPosti.SetIdAreaSelezionata(idAreaSelezionata)
            Call frmConfigurazionePiantaGrigliaPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
            Call frmConfigurazionePiantaGrigliaPosti.SetXShift(0)
            Call frmConfigurazionePiantaGrigliaPosti.SetYShift(0)
            Call frmConfigurazionePiantaGrigliaPosti.SetXIntervalloPosti(1)
            Call frmConfigurazionePiantaGrigliaPosti.SetYIntervalloPosti(1)
            Unload Me
        Else
            Call frmMessaggio.Visualizza("NotificaNomiNonCorretti")
        End If
    End If
End Sub

Private Sub txtNomeFila_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomePrimoPosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomeUltimoPosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtStep_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetNumeroMaxPostiInFila(num As Integer)
    numeroMaxPostiInFila = num
End Sub

Private Sub CalcolaNumeroPostiInFila()
    Dim a As String
    Dim b As String
    Dim primo As Integer
    Dim ultimo As Integer

    a = Mid$(Trim(UCase(nomePrimoPosto)), 1, 1)
    b = Mid$(Trim(UCase(nomeUltimoPosto)), 1, 1)
    If Asc(a) >= 48 And Asc(a) <= 57 Then
        primo = Intero(nomePrimoPosto)
        ultimo = Intero(nomeUltimoPosto)
    ElseIf Asc(a) >= 65 And Asc(a) <= 90 Then
        primo = Intero(Asc(a))
        ultimo = Intero(Asc(b))
    End If
    
    numeroPostiInFila = Abs(Int((primo - ultimo) / step)) + 1 '+1 perché estremi compresi
End Sub

Public Sub SetModalitaForm(moda As ModalitaFormDettagliEnum)
    modalita = moda
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox)
    Dim i As Integer
    Dim area As classeArea
    
'    For i = 1 To UBound(elencoAreeAssociateAPianta)
'        cmb.AddItem (elencoAreeAssociateAPianta(i).nomeArea)
'        cmb.ItemData(i - 1) = elencoAreeAssociateAPianta(i).idArea
'    Next i
    i = 1
    For Each area In listaAreeAssociateAPianta
        cmb.AddItem area.nomeArea
        cmb.ItemData(i - 1) = area.idArea
        i = i + 1
    Next area
End Sub

Private Sub cmbArea_Click()
    idAreaSelezionata = cmbArea.ItemData(cmbArea.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub
