VERSION 5.00
Begin VB.Form frmInizialePiantaSIAE 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta SIAE"
   ClientHeight    =   8670
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8670
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraDati 
      Caption         =   "Caratteristiche"
      Height          =   6255
      Left            =   120
      TabIndex        =   3
      Top             =   540
      Width           =   9855
      Begin VB.ListBox lstDisponibili 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3000
         Left            =   1380
         MultiSelect     =   2  'Extended
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   3000
         Width           =   3555
      End
      Begin VB.CommandButton cmdSvuotaSelezionati 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4980
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   5160
         Width           =   435
      End
      Begin VB.CommandButton cmdDidsponibile 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4980
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   4500
         Width           =   435
      End
      Begin VB.CommandButton cmdSelezionato 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4980
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   3840
         Width           =   435
      End
      Begin VB.ListBox lstSelezionati 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3000
         Left            =   5460
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   3000
         Width           =   3555
      End
      Begin VB.CommandButton cmdSvuotaDisponibili 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4980
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   3180
         Width           =   435
      End
      Begin VB.ComboBox cmbPianta 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1380
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1800
         Width           =   3555
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1380
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   840
         Width           =   7635
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1380
         MaxLength       =   30
         TabIndex        =   4
         Top             =   420
         Width           =   3255
      End
      Begin VB.Label lblSelezionati 
         Alignment       =   2  'Center
         Caption         =   "Associate"
         Height          =   195
         Left            =   5460
         TabIndex        =   19
         Top             =   2760
         Width           =   3555
      End
      Begin VB.Label lblDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Height          =   195
         Left            =   1380
         TabIndex        =   18
         Top             =   2760
         Width           =   3555
      End
      Begin VB.Label lblListe 
         Alignment       =   2  'Center
         Caption         =   "SUPERAREE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1380
         TabIndex        =   17
         Top             =   2520
         Width           =   7635
      End
      Begin VB.Label lblPianta 
         Alignment       =   1  'Right Justify
         Caption         =   "pianta"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1860
         Width           =   1095
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   180
         TabIndex        =   6
         Top             =   480
         Width           =   1035
      End
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   0
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialePiantaSIAE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSIAE As Long
Private idPiantaSelezionata As Long
Private nomePiantaSIAE As String
Private descrizionePiantaSIAE As String
'Private listaCampiValoriUnici As Collection
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private idOrdinediPostoSIAESelezionato As Long
Private codiceOrdinediPostoSIAESelezionato As String

Private internalEvent As Boolean

Private modalit�FormCorrente As AzioneEnum
Private exitCodeFormDettagli As ExitCodeEnum

Public Sub Init()
    Dim sql As String

    Call Variabili_Init
    Call lstDisponibili.Clear
    Call lstSelezionati.Clear
    Call AggiornaAbilitazioneControlli
    
    sql = "SELECT IDPIANTA ID, NOME FROM PIANTA ORDER BY NOME"
    Call CaricaValoriCombo(cmbPianta, sql, "NOME")
    Select Case modalit�FormCorrente
        Case A_NUOVO
            idPiantaSIAE = idNessunElementoSelezionato
            idPiantaSelezionata = idNessunElementoSelezionato
        Case A_MODIFICA
            Call SelezionaElementoSuCombo(cmbPianta, idPiantaSelezionata)
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call SelezionaElementoSuCombo(cmbPianta, idPiantaSelezionata)
            Call CaricaDallaBaseDati
            Call AssegnaValoriCampi
        Case A_CLONA
            'DA FARE
        Case Else
            'Do Nothing
    End Select
    
    Call AggiornaAbilitazioneControlli
    Call frmInizialePiantaSIAE.Show(vbModal)
    
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub Variabili_Init()
    nomePiantaSIAE = ""
    descrizionePiantaSIAE = ""
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdPiantaSIAESelezionata(idSIAE As Long)
    idPiantaSIAE = idSIAE
End Sub

Public Sub SetIdPiantaSelezionata(idP As Long)
    idPiantaSelezionata = idP
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuova Pianta SIAE"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Pianta SIAE configurata"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Pianta SIAE configurata"
    End Select
    
    If modalit�FormCorrente <> A_ELIMINA Then
        If Not (listaSelezionati Is Nothing) Then
            cmdConferma.Enabled = (Trim(txtNome.Text) <> "" And _
                idPiantaSelezionata <> idNessunElementoSelezionato And _
                listaDisponibili.count = 0)
        Else
            cmdConferma.Enabled = False
        End If
    Else
        cmdConferma.Enabled = True
    End If
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lstDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblListe.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdDidsponibile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdSelezionato.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdSvuotaDisponibili.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmdSvuotaSelezionati.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmbPianta.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblPianta.Enabled = (modalit�FormCorrente <> A_ELIMINA)
        
End Sub

Private Sub cmbPianta_Click()
    If Not internalEvent Then
        idPiantaSelezionata = cmbPianta.ItemData(cmbPianta.ListIndex)
        Call CaricaValoriLstDisponibili
        Call CaricaValoriLstSelezionati
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub Annulla()
    Call frmSceltaPiantaSIAE.SetExitCodeFormIniziale(EC_ANNULLA)
    Unload Me
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD

    sql = "SELECT NOME, DESCRIZIONE FROM PIANTASIAE WHERE IDPIANTASIAE = " & idPiantaSIAE
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        nomePiantaSIAE = rec("NOME")
        descrizionePiantaSIAE = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomePiantaSIAE
    txtDescrizione.Text = descrizionePiantaSIAE
    
    internalEvent = internalEventOld
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim idNuovaPiantaSIAE As Long
    Dim n As Long
    Dim area As clsElementoLista
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'   INSERIMENTO IN TABELLA PIANTASIAE
    idNuovaPiantaSIAE = OttieniIdentificatoreDaSequenza("SQ_PIANTASIAE")
    sql = "INSERT INTO PIANTASIAE (IDPIANTASIAE, NOME, DESCRIZIONE, IDPIANTA)" & _
        " VALUES (" & _
        idNuovaPiantaSIAE & ", " & _
        SqlStringValue(nomePiantaSIAE) & ", " & _
        SqlStringValue(descrizionePiantaSIAE) & ", " & _
        idPiantaSelezionata & ")"
    SETAConnection.Execute sql, n, adCmdText

'   INSERIMENTO IN TABELLA PIANTASIAE_AREA_ORDINEDIPOSTO
    If Not (listaSelezionati Is Nothing) Then
        For Each area In listaSelezionati
            sql = "INSERT INTO PIANTASIAE_AREA_ORDINEDIPOSTO (IDPIANTASIAE, IDAREA," & _
                " IDORDINEDIPOSTOSIAE)" & _
                " VALUES (" & _
                idNuovaPiantaSIAE & ", " & _
                area.idElementoLista & ", " & _
                area.idAttributoElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next area
    End If

    Call ChiudiConnessioneBD
    
    Call frmSceltaPiantaSIAE.SetIdRecordSelezionato(idNuovaPiantaSIAE)
    Call SetIdPiantaSIAESelezionata(idNuovaPiantaSIAE)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim nomiProdotti As String
    Dim isModificabile As Boolean

    stringaNota = "IDPIANTASIAE = " & idPiantaSIAE
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            If ValoriCampiOK Then
                Call InserisciNellaBaseDati
                stringaNota = "IDPIANTASIAE = " & idPiantaSIAE
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PIANTA_SIAE, , stringaNota)
                Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                Call SetModalit�Form(A_MODIFICA)
                Call AggiornaAbilitazioneControlli
            End If
        Case A_MODIFICA
            isModificabile = True
            If IsPiantaSIAEAssociataAProdotto(nomiProdotti) Then
                Call frmMessaggio.Visualizza("NotificaNonEditabilit�PiantaSIAE", nomiProdotti)
                isModificabile = (frmMessaggio.exitCode = EC_CONFERMA)
            End If
            If isModificabile Then
                If ValoriCampiOK = True Then
                    Call AggiornaNellaBaseDati
                    Call ScriviLog(CCTA_MODIFICA, CCDA_PIANTA_SIAE, , stringaNota)
                    Call frmMessaggio.Visualizza("NotificaModificaDati")
                    Call Esci
                End If
            End If
        Case A_ELIMINA
            If Not IsPiantaSIAEAssociataAProdotto(nomiProdotti) Then
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PIANTA_SIAE, , stringaNota)
                Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
                Call Esci
            Else
                Call frmMessaggio.Visualizza("NotificaNonEditabilit�PiantaSIAE", nomiProdotti)
            End If
    End Select
    Call frmSceltaPiantaSIAE.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idArea = lstSelezionati.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaSelezionati.Item(chiaveArea)
            area.codiceAttributoElementoLista = ""
            area.idAttributoElementoLista = idNessunElementoSelezionato
            area.descrizioneElementoLista = area.nomeElementoLista
            Call listaDisponibili.Add(area, chiaveArea)
            Call listaSelezionati.Remove(chiaveArea)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim n As Long
    Dim area As clsElementoLista

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

'   AGGIORNAMENTO IN TABELLA PIANTASIAE
    sql = "UPDATE PIANTASIAE SET" & _
        " NOME = " & SqlStringValue(nomePiantaSIAE) & ", " & _
        " DESCRIZIONE = " & SqlStringValue(descrizionePiantaSIAE) & ", " & _
        " IDPIANTA = " & idPiantaSelezionata & _
        " WHERE IDPIANTASIAE = " & idPiantaSIAE
    SETAConnection.Execute sql, n, adCmdText
    
'   AGGIORNAMENTO TABELLA PIANTASIAE_AREA_ORDINEDIPOSTO
    sql = "DELETE FROM PIANTASIAE_AREA_ORDINEDIPOSTO" & _
        " WHERE IDPIANTASIAE = " & idPiantaSIAE
    SETAConnection.Execute sql, n, adCmdText
    If Not (listaSelezionati Is Nothing) Then
        For Each area In listaSelezionati
            sql = "INSERT INTO PIANTASIAE_AREA_ORDINEDIPOSTO (IDPIANTASIAE, IDAREA," & _
                " IDORDINEDIPOSTOSIAE)" & _
                " VALUES (" & _
                idPiantaSIAE & ", " & _
                area.idElementoLista & ", " & _
                area.idAttributoElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next area
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'   ELIMINAZIONE RECORD TABELLA PIANTASIAE_AREA_ORDINEDIPOSTO
    sql = "DELETE FROM PIANTASIAE_AREA_ORDINEDIPOSTO WHERE IDPIANTASIAE = " & idPiantaSIAE
    SETAConnection.Execute sql, n, adCmdText
    
'   ELIMINAZIONE RECORD TABELLA PIANTASIAE
    sql = "DELETE FROM PIANTASIAE WHERE IDPIANTASIAE = " & idPiantaSIAE
    SETAConnection.Execute sql, n, adCmdText

    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    Set listaNonConformit� = New Collection
    condizioneSql = ""
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_ELIMINA Then
        condizioneSql = "IDPIANTASIAE <> " & idPiantaSIAE
    End If
    
    nomePiantaSIAE = Trim(txtNome.Text)
    If ViolataUnicit�("PIANTASIAE", "NOME = " & SqlStringValue(nomePiantaSIAE), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomePiantaSIAE) & _
            " � gi� presente in DB;")
    End If
    descrizionePiantaSIAE = Trim(txtDescrizione.Text)
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim labelArea As String
    Dim idArea As Long
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    sql = "SELECT DISTINCT A.IDAREA ID, A.CODICE COD, A.NOME NAME" & _
        " FROM AREA A, PIANTASIAE_AREA_ORDINEDIPOSTO PAO" & _
        " WHERE A.IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_NON_NUMERATA & ")" & _
        " AND A.IDPIANTA = " & idPiantaSelezionata & _
        " AND A.IDAREA = PAO.IDAREA(+)" & _
        " AND PAO.IDAREA IS NULL" & _
        " AND PAO.IDPIANTASIAE(+) = " & idPiantaSIAE & _
        " ORDER BY COD"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            areaCorrente.nomeElementoLista = "(" & rec("COD") & ") " & rec("NAME")
            areaCorrente.descrizioneElementoLista = "(" & rec("COD") & ") " & rec("NAME")
            areaCorrente.idElementoLista = rec("ID").Value
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaDisponibili.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim labelArea As String
    Dim nomeArea As String
    Dim codOrdine As String
    Dim idArea As Long
    Dim idOrdine As Long
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    
    Call ApriConnessioneBD

    Set listaSelezionati = New Collection
    sql = "SELECT A.IDAREA ID, A.CODICE AREACOD, A.NOME AREANOME, O.CODICE ORDCOD, O.IDORDINEDIPOSTOSIAE IDORD" & _
        " FROM AREA A, PIANTASIAE_AREA_ORDINEDIPOSTO PAO, ORDINEDIPOSTOSIAE O" & _
        " WHERE A.IDAREA = PAO.IDAREA" & _
        " AND O.IDORDINEDIPOSTOSIAE = PAO.IDORDINEDIPOSTOSIAE" & _
        " AND A.IDPIANTA = " & idPiantaSelezionata & _
        " AND PAO.IDPIANTASIAE = " & idPiantaSIAE & _
        " ORDER BY ORDCOD"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            areaCorrente.nomeElementoLista = "(" & rec("AREACOD") & ") " & rec("AREANOME")
            areaCorrente.idElementoLista = rec("ID").Value
            areaCorrente.codiceAttributoElementoLista = rec("ORDCOD")
            areaCorrente.idAttributoElementoLista = rec("IDORD")
            areaCorrente.descrizioneElementoLista = areaCorrente.codiceAttributoElementoLista & " - " & _
                areaCorrente.nomeElementoLista
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaSelezionati.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstSelezionati_Init
    
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear

    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each area In listaDisponibili
            lstDisponibili.AddItem area.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim label As String
    Dim i As Integer
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each area In listaSelezionati
            lstSelezionati.AddItem area.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    Call frmDettagliOrdineDiPostoSIAE.Init
    If frmDettagliOrdineDiPostoSIAE.GetExitCode = EC_CONFERMA Then
        idOrdinediPostoSIAESelezionato = frmDettagliOrdineDiPostoSIAE.GetIdOrdineDiPostoSiaeSelezionato
        codiceOrdinediPostoSIAESelezionato = frmDettagliOrdineDiPostoSIAE.GetCodiceOrdineDiPostoSiaeSelezionato
        For i = 1 To lstDisponibili.ListCount
            If lstDisponibili.Selected(i - 1) Then
                idArea = lstDisponibili.ItemData(i - 1)
                chiaveArea = ChiaveId(idArea)
                Set area = listaDisponibili.Item(chiaveArea)
                area.codiceAttributoElementoLista = codiceOrdinediPostoSIAESelezionato
                area.idAttributoElementoLista = idOrdinediPostoSIAESelezionato
                area.descrizioneElementoLista = area.codiceAttributoElementoLista & _
                    " - " & area.nomeElementoLista
                Call listaSelezionati.Add(area, chiaveArea)
                Call listaDisponibili.Remove(chiaveArea)
            End If
        Next i
        Call lstDisponibili_Init
        Call lstSelezionati_Init
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaSelezionati()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaSelezionati
        chiaveArea = ChiaveId(area.idElementoLista)
        area.codiceAttributoElementoLista = ""
        area.idAttributoElementoLista = idNessunElementoSelezionato
        area.descrizioneElementoLista = area.nomeElementoLista
        Call listaDisponibili.Add(area, chiaveArea)
    Next area
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaDisponibili()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    Call frmDettagliOrdineDiPostoSIAE.Init
    If frmDettagliOrdineDiPostoSIAE.GetExitCode = EC_CONFERMA Then
        idOrdinediPostoSIAESelezionato = frmDettagliOrdineDiPostoSIAE.GetIdOrdineDiPostoSiaeSelezionato
        codiceOrdinediPostoSIAESelezionato = frmDettagliOrdineDiPostoSIAE.GetCodiceOrdineDiPostoSiaeSelezionato
        For Each area In listaDisponibili
            chiaveArea = ChiaveId(area.idElementoLista)
            area.codiceAttributoElementoLista = codiceOrdinediPostoSIAESelezionato
            area.idAttributoElementoLista = idOrdinediPostoSIAESelezionato
            area.descrizioneElementoLista = area.codiceAttributoElementoLista & _
                " - " & area.nomeElementoLista
            Call listaSelezionati.Add(area, chiaveArea)
        Next area
        Set listaDisponibili = Nothing
        Set listaDisponibili = New Collection
        
        Call lstDisponibili_Init
        Call lstSelezionati_Init
    End If
End Sub

Private Function IsPiantaSIAEAssociataAProdotto(nomiProdotti As String) As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nome As String
    Dim listaProdotti As Collection
    Dim trovato As Boolean
    
    trovato = False
    Set listaProdotti = New Collection
    sql = "SELECT NOME FROM PRODOTTO WHERE IDPIANTASIAE = " & idPiantaSIAE
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        trovato = True
        While Not rec.EOF
            nome = rec("NOME")
            Call listaProdotti.Add(nome)
            rec.MoveNext
        Wend
    End If
    rec.Close
    If listaProdotti.count > 0 Then
        nomiProdotti = ArgomentoMessaggio(listaProdotti)
    End If
    IsPiantaSIAEAssociataAProdotto = trovato
End Function
