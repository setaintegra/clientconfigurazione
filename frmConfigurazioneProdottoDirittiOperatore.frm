VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoDirittiOperatore 
   Caption         =   "Diritti operatore"
   ClientHeight    =   10395
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10875
   LinkTopic       =   "Form1"
   ScaleHeight     =   10395
   ScaleWidth      =   10875
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSelezionaNessuno 
      Caption         =   "Nessuno"
      Height          =   315
      Left            =   8280
      TabIndex        =   9
      Top             =   9360
      Width           =   2355
   End
   Begin VB.CommandButton cmdSelezionaTutti 
      Caption         =   "Tutti"
      Height          =   315
      Left            =   5520
      TabIndex        =   8
      Top             =   9360
      Width           =   2715
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   435
      Left            =   5520
      TabIndex        =   7
      Top             =   9840
      Width           =   1155
   End
   Begin VB.ListBox lstTipiOperazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8220
      Left            =   5520
      Style           =   1  'Checkbox
      TabIndex        =   5
      Top             =   1080
      Width           =   5055
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Chiudi"
      Height          =   435
      Left            =   9480
      TabIndex        =   4
      Top             =   9840
      Width           =   1155
   End
   Begin VB.ListBox lstOperatori 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4470
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   5295
   End
   Begin VB.Label Label2 
      Caption         =   "Tipi operazione"
      Height          =   255
      Left            =   5520
      TabIndex        =   6
      Top             =   600
      Width           =   2415
   End
   Begin VB.Label lblListaOperatori 
      Caption         =   "Operatori della CPV"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   4815
   End
   Begin VB.Label Label1 
      Caption         =   "Diritti operatori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   10635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Diritti operatori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2715
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoDirittiOperatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private nomeProdottoSelezionato As String
Private idClassePuntoVenditaSelezionata As Long
Private nomeClassePuntoVenditaSelezionata As String
Private idOperatoreSelezionato As Long

Public Sub Init(idOrg As Long, idPr As Long, nomePr As String, idCPV As Long, nomeCPV As String)
    idOrganizzazioneSelezionata = idOrg
    idProdottoSelezionato = idPr
    nomeProdottoSelezionato = nomePr
    idClassePuntoVenditaSelezionata = idCPV
    nomeClassePuntoVenditaSelezionata = nomeCPV
    
    idOperatoreSelezionato = idNessunElementoSelezionato
    
    lblListaOperatori.Caption = "Diritti operatori di " & nomeCPV & " su: " & nomeProdottoSelezionato
    Call popolaListaOperatori
    
    Me.Show (vbModal)
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub cmdAnnulla_Click()
    Unload Me
End Sub

Private Sub popolaListaOperatori()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    i = 0
    lstOperatori.Clear
    
    Call ApriConnessioneBD_ORA
       
    sql = "SELECT IDOPERATORE, USERNAME || ' - ' || PV.NOME NOME" & _
        " FROM OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, PUNTOVENDITA PV" & _
        " WHERE OP.idPuntoVenditaElettivo = OCPV.idPuntoVendita" & _
        " AND OCPV.IDCLASSEPUNTOVENDITA = " & idClassePuntoVenditaSelezionata & _
        " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
        " AND OP.ABILITATO = 1" & _
        " AND USERNAME NOT LIKE 'MGZ%'" & _
        " ORDER BY USERNAME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            lstOperatori.AddItem (rec("NOME"))
            lstOperatori.ItemData(i) = rec("IDOPERATORE")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub cmdSelezionaTutti_Click()
    Call SelezionaTutti
End Sub

Private Sub cmdSelezionaNessuno_Click()
    Call SelezionaNessuno
End Sub

Private Sub lstOperatori_Click()
    Dim i As Long
    
    idOperatoreSelezionato = idNessunElementoSelezionato
    For i = 1 To lstOperatori.ListCount
        If lstOperatori.Selected(i - 1) Then
            idOperatoreSelezionato = lstOperatori.ItemData(i - 1)
        End If
    Next i
    Call popolaListaTipiOperazione
End Sub

Private Sub popolaListaTipiOperazione()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    i = 0
    lstTipiOperazione.Clear
    Call ApriConnessioneBD_ORA
       
    sql = "SELECT T.IDTIPOOPERAZIONE IDOPERAZ, T.NOME, OTO.IDTIPOOPERAZIONE ABIL" & _
        " FROM TIPOOPERAZIONE T, OPERATORE_TIPOOPERAZIONE OTO" & _
        " WHERE T.IDTIPOOPERAZIONE > 0" & _
        " AND T.IDTIPOOPERAZIONE = OTO.IDTIPOOPERAZIONE(+)" & _
        " AND OTO.IDPRODOTTO(+) = " & idProdottoSelezionato & _
        " AND OTO.IDOPERATORE(+) = " & idOperatoreSelezionato & _
        " AND T.DIRITTICONFIGURABILI = 1" & _
        " ORDER BY T.NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            lstTipiOperazione.AddItem (rec("NOME"))
            lstTipiOperazione.ItemData(i) = rec("IDOPERAZ")
            If IsNull(rec("ABIL")) Then
                lstTipiOperazione.Selected(i) = False
            Else
                lstTipiOperazione.Selected(i) = True
            End If
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub Conferma()
    Dim sql As String
    Dim rec As OraDynaset
    Dim n As Long
    Dim i As Long
    Dim cont As Long
    Dim listaId As String
    
    i = 0
    cont = 0
    
    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    
    listaId = "(-1"
    For i = 0 To lstTipiOperazione.ListCount - 1
        If lstTipiOperazione.Selected(i) Then
            listaId = listaId & ", " & lstTipiOperazione.ItemData(i)
            cont = cont + 1
        End If
    Next i
    listaId = listaId & ")"
       
    sql = "DELETE FROM OPERATORE_TIPOOPERAZIONE" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND IDOPERATORE = " & idOperatoreSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    sql = "INSERT INTO OPERATORE_TIPOOPERAZIONE (IDPRODOTTO, IDOPERATORE, IDTIPOOPERAZIONE)" & _
        " SELECT " & idProdottoSelezionato & ", " & idOperatoreSelezionato & ", IDTIPOOPERAZIONE" & _
        " FROM TIPOOPERAZIONE" & _
        " WHERE IDTIPOOPERAZIONE IN " & listaId
    n = ORADB.ExecuteSQL(sql)

    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub SelezionaTutti()
    Dim i As Long
    
    For i = 0 To lstTipiOperazione.ListCount - 1
        lstTipiOperazione.Selected(i) = True
    Next i

End Sub

Private Sub SelezionaNessuno()
    Dim i As Long
    
    For i = 0 To lstTipiOperazione.ListCount - 1
        lstTipiOperazione.Selected(i) = False
    Next i

End Sub


