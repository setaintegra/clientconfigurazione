VERSION 5.00
Begin VB.Form frmDettagliContratto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli contratto"
   ClientHeight    =   3630
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5580
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   5580
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   960
      TabIndex        =   4
      Top             =   3240
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   3480
      TabIndex        =   5
      Top             =   3240
      Width           =   1035
   End
   Begin VB.TextBox txtDataFineValiditā 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2700
      MaxLength       =   10
      TabIndex        =   3
      Top             =   2640
      Width           =   1215
   End
   Begin VB.TextBox txtDataInizioValiditā 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      MaxLength       =   10
      TabIndex        =   2
      Top             =   2640
      Width           =   1215
   End
   Begin VB.TextBox txtNome 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   360
      Width           =   5295
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   1500
      Width           =   5295
   End
   Begin VB.Label lblDate 
      Alignment       =   1  'Right Justify
      Caption         =   "Date validitā:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   2700
      Width           =   1035
   End
   Begin VB.Label lblDataFineValiditā 
      Caption         =   "Fine"
      Height          =   255
      Left            =   2700
      TabIndex        =   9
      Top             =   2400
      Width           =   915
   End
   Begin VB.Label lblDataInizioValiditā 
      Caption         =   "Inizio"
      Height          =   255
      Left            =   1320
      TabIndex        =   8
      Top             =   2400
      Width           =   915
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1260
      Width           =   1575
   End
End
Attribute VB_Name = "frmDettagliContratto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private codicePianta As Integer
Private idContrattoSelezionato As Long
Private nomeContrattoSelezionato As String
Private descrizioneContrattoSelezionato As String
Private dataInizioValiditāContrattoSelezionato As String
Private dataFineValiditāContrattoSelezionato As String
Private exitCode As ExitCodeEnum
'Private idPiantaSelezionata As Long
'Private nomePianta As String

'Private operazioneSuPianta As AzioneEnum

Public Sub Init()
'    Dim sql As String
'
'    Select Case operazioneSuPianta
'        Case A_NUOVO
'            sql = "SELECT ORGANIZZAZIONE_PIANTA.CODICEPIANTA COD, PIANTA.IDPIANTA ID, PIANTA.NOME NOME" & _
'                " FROM ORGANIZZAZIONE_PIANTA, PIANTA WHERE" & _
'                " (ORGANIZZAZIONE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
'                " (ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") AND" & _
'                " (PIANTA.IDPIANTA <> " & idPiantaSelezionata & ")" & _
'                " ORDER BY COD"
'        Case A_CLONA
'            sql = "SELECT ORGANIZZAZIONE_PIANTA.CODICEPIANTA COD, PIANTA.IDPIANTA ID, PIANTA.NOME NOME" & _
'                " FROM ORGANIZZAZIONE_PIANTA, PIANTA WHERE" & _
'                " (ORGANIZZAZIONE_PIANTA.IDPIANTA = PIANTA.IDPIANTA) AND" & _
'                " (ORGANIZZAZIONE_PIANTA.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ")" & _
'                " ORDER BY COD"
'        Case Else
'            'Do Nothing
'    End Select
    Call Controlli_Init
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub CaricaDallaBaseDati()
    Dim rec As New ADODB.Recordset
    Dim sql As String
    
    Call ApriConnessioneBD

    sql = "SELECT IDCONTRATTO ID, NOME, DESCRIZIONE," & _
        " DATAINIZIOVALIDITA, DATAFINEVALIDITA" & _
        " FROM SETA_REP.CONTRATTO" & _
        " WHERE IDCONTRATTO = " & idContrattoSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        nomeContrattoSelezionato = rec("NOME")
        descrizioneContrattoSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        dataInizioValiditāContrattoSelezionato = IIf(IsNull(rec("DATAINIZIOVALIDITA")), "", rec("DATAINIZIOVALIDITA"))
        dataFineValiditāContrattoSelezionato = IIf(IsNull(rec("DATAFINEVALIDITA")), "", rec("DATAFINEVALIDITA"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AggiornaAbilitazioneControlli()
'    txtPianta.Enabled = False
'    txtOrganizzazione.Enabled = False
'    cmdConferma.Enabled = Trim(txtCodicePianta.Text) <> ""
End Sub
'
'Private Function RilevaValoriCampi() As Boolean
'    RilevaValoriCampi = True
'
'    If IsCampoInteroCorretto(txtCodicePianta) Then
'        codicePianta = CInt(Trim(txtCodicePianta.Text))
'        If Not IsCodiceUnivoco Then
'            Call frmMessaggio.Visualizza("ErroreDuplicazioneCodiceConstraint", codicePianta, nomeOrganizzazioneSelezionata)
'            RilevaValoriCampi = False
'            Exit Function
'        End If
'    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero")
'        RilevaValoriCampi = False
'        Exit Function
'    End If
'End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
'    Call frmInizialePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_ANNULLA)
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
'    If RilevaValoriCampi Then
'        Select Case operazioneSuPianta
'            Case A_NUOVO
'                Call frmInizialePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_CONFERMA)
'                Call frmInizialePianta.SetCodicePiantaOrganizzazioneSelezionata(codicePianta)
'            Case A_CLONA
'                Call frmClonazionePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_CONFERMA)
'                Call frmClonazionePianta.SetCodicePiantaOrganizzazioneSelezionata(codicePianta)
'            Case Else
'        End Select
    exitCode = EC_CONFERMA
        Unload Me
'    End If
    
End Sub
'
'Private Sub txtCodicePianta_Change()
'    Call AggiornaAbilitazioneControlli
'End Sub
''
'Public Sub SetCodicePianta(cod As Integer)
'    codicePianta = cod
'End Sub

Private Sub AssegnaValoriCampi()
    txtNome.Text = nomeContrattoSelezionato
    txtDescrizione.Text = descrizioneContrattoSelezionato
    txtDataInizioValiditā.Text = dataInizioValiditāContrattoSelezionato
    txtDataFineValiditā.Text = dataFineValiditāContrattoSelezionato
End Sub

Private Sub Controlli_Init()
    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtDataInizioValiditā.Text = ""
    txtDataFineValiditā.Text = ""
End Sub

Public Sub SetIdContrattoSelezionato(idC As Long)
    idContrattoSelezionato = idC
End Sub
'
'Public Sub SetIdPiantaSelezionata(idP As Long)
'    idPiantaSelezionata = idP
''End Sub
'
'Public Sub SetNomeOrganizzazioneSelezionata(nomeO As String)
'    nomeOrganizzazioneSelezionata = nomeO
'End Sub
'
'Public Sub SetNomePianta(nomeP As String)
'    nomePianta = nomeP
'End Sub
'
'Private Function IsCodiceUnivoco() As Boolean
'    Dim iCod As Integer
'    Dim trovato As Boolean
'
'    trovato = False
'    iCod = 0
'    While iCod <= (lstCodiciPiantaOrganizzazione.ListCount - 1) And Not trovato
'        If codicePianta = lstCodiciPiantaOrganizzazione.ItemData(iCod) Then
'            trovato = True
'        End If
'        iCod = iCod + 1
'    Wend
'    IsCodiceUnivoco = Not trovato
'End Function
'
'Private Sub CaricaValoriLista(lst As ListBox, strSQL As String, NomeCampo1 As String, Optional nomeCampo2 As String)
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim stringaVisualizzata As String
'    Dim i As Integer
'
'    Call ApriConnessioneBD
'
'    sql = strSQL
'
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        i = 1
'        rec.MoveFirst
'        While Not rec.EOF
'            stringaVisualizzata = rec(NomeCampo1) & " - " & rec(nomeCampo2)
'            lst.AddItem stringaVisualizzata
'            lst.ItemData(i - 1) = rec("COD").Value
'            i = i + 1
'            rec.MoveNext
'        Wend
'    End If
'
'    rec.Close
'    Call ChiudiConnessioneBD
'
'End Sub
'
'Public Sub SetOperazioneSuPianta(operaz As AzioneEnum)
'    operazioneSuPianta = operaz
'End Sub

Public Function GetExitCode()
    GetExitCode = exitCode
End Function

