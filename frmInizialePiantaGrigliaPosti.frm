VERSION 5.00
Begin VB.Form frmInizialePiantaGrigliaPosti 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdmodificaDimensioni 
      Caption         =   "Modifica dimensioni"
      Height          =   435
      Left            =   6600
      TabIndex        =   23
      Top             =   4020
      Width           =   1335
   End
   Begin VB.CommandButton cmdConfiguraFasceSequenze 
      Caption         =   "     Configura fasce/sequenze"
      Height          =   435
      Left            =   9600
      TabIndex        =   8
      Top             =   4020
      Width           =   1335
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   2340
      Width           =   4035
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtMaxCoordOrizzontale 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   10
      TabIndex        =   3
      Top             =   4080
      Width           =   1215
   End
   Begin VB.TextBox txtMaxCoordVerticale 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      MaxLength       =   10
      TabIndex        =   4
      Top             =   4080
      Width           =   1215
   End
   Begin VB.TextBox txtDimOrizzontalePosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3240
      MaxLength       =   10
      TabIndex        =   5
      Top             =   4080
      Width           =   1215
   End
   Begin VB.TextBox txtDimVerticalePosto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   6
      Top             =   4080
      Width           =   1215
   End
   Begin VB.CommandButton cmdDisegnaGriglia 
      Caption         =   "Disegna griglia"
      Height          =   435
      Left            =   8100
      TabIndex        =   7
      Top             =   4020
      Width           =   1335
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   13
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   11
      Top             =   8100
      Width           =   1155
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9540
      TabIndex        =   12
      Top             =   240
      Width           =   2295
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   2100
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   21
      Top             =   2100
      Width           =   3375
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei principali attributi della Griglia Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   6975
   End
   Begin VB.Label lblMaxCoordOrizzontale 
      Caption         =   "Max coord. orizz."
      Height          =   195
      Left            =   120
      TabIndex        =   18
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Label lblMaxCoordVerticale 
      Caption         =   "Max coord. vert."
      Height          =   195
      Left            =   1680
      TabIndex        =   17
      Top             =   3840
      Width           =   1275
   End
   Begin VB.Label lblDimOrizzontalePosto 
      Caption         =   "Dim. Orizz. Posto"
      Height          =   195
      Left            =   3240
      TabIndex        =   16
      Top             =   3840
      Width           =   1275
   End
   Begin VB.Label lblDimVerticalePosto 
      Caption         =   "Dim. Vert. Posto"
      Height          =   195
      Left            =   4800
      TabIndex        =   15
      Top             =   3840
      Width           =   1275
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9540
      TabIndex        =   14
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmInizialePiantaGrigliaPosti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idSuperAreaSelezionata As Long
Private idAreaSelezionata As Long
Private idRecordSelezionato As Long
Private maxCoordOrizzontale As Long
Private maxCoordVerticale As Long
Private dimOrizzontalePosto As Long
Private dimVerticalePosto As Long
Private nomePiantaSelezionata As String
Private nomeAreaSelezionata As String
Private isAttributiAreaModificati As Boolean

Private internalEvent As Boolean
Private esisteGrigliaPostiAssociata As Boolean

Private tipoGriglia As TipoGrigliaEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Public Sub Init()
    Dim sql As String
    
    Call Variabili_Init
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            dimOrizzontalePosto = 1
            dimVerticalePosto = 1
            Call CreaListaAreeAssociateAPianta 'prima non c'era
        Case TG_PICCOLI_IMPIANTI
            Call AggiornaAbilitazioneControlli
            Call CreaListaAreeAssociateAPianta
        Case TG_NON_SPECIFICATO
        Case Else
    End Select
    Call Me.Show(vbModal)
    
End Sub

Private Sub Variabili_Init()
    maxCoordOrizzontale = 0
    maxCoordVerticale = 0
    If tipoGriglia = TG_PICCOLI_IMPIANTI Then
        dimOrizzontalePosto = 0
        dimVerticalePosto = 0
    End If
    idRecordSelezionato = idNessunElementoSelezionato
    isAttributiAreaModificati = False
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModificaDimensioni_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call ModificaDimensioni
    
    MousePointer = mousePointerOld
End Sub
Private Sub cmdDisegnaGriglia_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call DisegnaGriglia
    
    MousePointer = mousePointerOld
End Sub

Private Sub ModificaDimensioni()
    If ValoriCampiOK Then
        isAttributiAreaModificati = True
        Call CaricaFormModificaDimensioniGrigliaPosti
    End If
End Sub

Private Sub DisegnaGriglia()
    If ValoriCampiOK Then
        isAttributiAreaModificati = True
        Call CaricaFormConfigurazionePiantaGrigliaPosti
    End If
End Sub

Private Sub CaricaFormModificaDimensioniGrigliaPosti()
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.SetTipoGriglia(tipoGriglia)
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.SetIdGrigliaPostiSelezionata(idRecordSelezionato)
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.SetMassimaCoordinataOrizzontale(maxCoordOrizzontale)
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.SetMassimaCoordinataVerticale(maxCoordVerticale)
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.SetDimensioneOrizzontalePosto(dimOrizzontalePosto)
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.SetDimensioneVerticalePosto(dimVerticalePosto)
    Call frmConfigurazionePiantaDimensioniGrigliaPosti.Init
    
    ReInizializzaDimensioniGrigliaPosti
End Sub

Private Sub ReInizializzaDimensioniGrigliaPosti()
    Dim sql As String
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE" & _
        " FROM GRIGLIAPOSTI G " & _
        " WHERE IDGRIGLIAPOSTI = " & idRecordSelezionato
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            maxCoordOrizzontale = rec("MASSIMACOORDINATAORIZZONTALE")
            maxCoordVerticale = rec("MASSIMACOORDINATAVERTICALE")
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    txtMaxCoordOrizzontale = maxCoordOrizzontale
    txtMaxCoordVerticale = maxCoordVerticale
End Sub

Private Sub CaricaFormConfigurazionePiantaGrigliaPosti()
    Call frmConfigurazionePiantaGrigliaPosti.SetIdAreaSelezionata(idAreaSelezionata)
    Call frmConfigurazionePiantaGrigliaPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
    Call frmConfigurazionePiantaGrigliaPosti.SetNomePiantaSelezionata(nomePiantaSelezionata)
    Call frmConfigurazionePiantaGrigliaPosti.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazionePiantaGrigliaPosti.SetTipoGriglia(tipoGriglia)
    Call frmConfigurazionePiantaGrigliaPosti.SetMaxCoordOrizzontale(maxCoordOrizzontale)
    Call frmConfigurazionePiantaGrigliaPosti.SetMaxCoordVerticale(maxCoordVerticale)
    Call frmConfigurazionePiantaGrigliaPosti.SetDimOrizzontalePosto(dimOrizzontalePosto)
    Call frmConfigurazionePiantaGrigliaPosti.SetDimVerticalePosto(dimVerticalePosto)
    Call frmConfigurazionePiantaGrigliaPosti.Init
End Sub

Private Sub AggiornaAbilitazioneControlli()

    lblInfo1.Caption = IIf(tipoGriglia = TG_GRANDI_IMPIANTI, "Area", "Pianta")
    txtInfo1.Text = IIf(tipoGriglia = TG_GRANDI_IMPIANTI, nomeAreaSelezionata, nomePiantaSelezionata)
    txtInfo1.Enabled = False
                                
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        lblMaxCoordOrizzontale.Enabled = False
        txtMaxCoordOrizzontale.Text = ""
        lblMaxCoordVerticale.Enabled = False
        txtMaxCoordVerticale.Text = ""
        lblDimOrizzontalePosto.Enabled = False
        lblDimVerticalePosto.Enabled = False
        txtDimOrizzontalePosto.Text = ""
        txtDimVerticalePosto.Text = ""
        txtMaxCoordOrizzontale.Enabled = False
        txtMaxCoordVerticale.Enabled = False
        txtDimOrizzontalePosto.Enabled = False
        txtDimVerticalePosto.Enabled = False
        cmdInserisciNuovo.Enabled = Not esisteGrigliaPostiAssociata
        cmdModifica.Enabled = esisteGrigliaPostiAssociata
        cmdElimina.Enabled = esisteGrigliaPostiAssociata
        cmdDisegnaGriglia.Enabled = False
        cmdmodificaDimensioni.Enabled = False
        cmdConfiguraFasceSequenze.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        lblMaxCoordOrizzontale.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        lblMaxCoordVerticale.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        lblDimOrizzontalePosto.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        lblDimVerticalePosto.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        txtMaxCoordOrizzontale.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        txtMaxCoordVerticale.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        If tipoGriglia = TG_GRANDI_IMPIANTI Then
            txtDimOrizzontalePosto.Text = dimOrizzontalePosto
            txtDimVerticalePosto.Text = dimVerticalePosto
            txtDimOrizzontalePosto.Enabled = False
            txtDimVerticalePosto.Enabled = False
        Else
            txtDimOrizzontalePosto.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
            txtDimVerticalePosto.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO)
        End If
        cmdInserisciNuovo.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdDisegnaGriglia.Enabled = (gestioneRecordGriglia = ASG_MODIFICA And _
            esisteGrigliaPostiAssociata)
        cmdmodificaDimensioni.Enabled = (gestioneRecordGriglia = ASG_MODIFICA And _
            esisteGrigliaPostiAssociata)
        cmdConfiguraFasceSequenze.Enabled = (gestioneRecordGriglia = ASG_MODIFICA And _
            esisteGrigliaPostiAssociata)
        cmdConferma.Enabled = (gestioneRecordGriglia = ASG_INSERISCI_NUOVO And _
                                txtMaxCoordOrizzontale <> "" And _
                                txtMaxCoordVerticale.Text <> "" And _
                                txtDimOrizzontalePosto.Text <> "" And _
                                txtDimVerticalePosto.Text <> "") Or _
                                (gestioneRecordGriglia = ASG_ELIMINA)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuova griglia posti"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo griglia posti"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica griglia posti selezionata"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione griglia posti selezionata"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        lblMaxCoordOrizzontale.Enabled = False
        txtMaxCoordOrizzontale.Text = ""
        lblMaxCoordVerticale.Enabled = False
        txtMaxCoordVerticale.Text = ""
        lblDimOrizzontalePosto.Enabled = False
        lblDimVerticalePosto.Enabled = False
        txtDimOrizzontalePosto.Text = ""
        txtDimVerticalePosto.Text = ""
        txtMaxCoordOrizzontale.Enabled = False
        txtMaxCoordVerticale.Enabled = False
        txtDimOrizzontalePosto.Enabled = False
        txtDimVerticalePosto.Enabled = False
        cmdInserisciNuovo.Enabled = Not esisteGrigliaPostiAssociata
        cmdModifica.Enabled = esisteGrigliaPostiAssociata
        cmdElimina.Enabled = esisteGrigliaPostiAssociata
        cmdDisegnaGriglia.Enabled = False
        cmdmodificaDimensioni.Enabled = False
        cmdConfiguraFasceSequenze.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Private Sub Controlli_Init()
    txtMaxCoordOrizzontale.Text = ""
    txtMaxCoordVerticale.Text = ""
    txtDimOrizzontalePosto.Text = ""
    txtDimVerticalePosto.Text = ""
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    
On Error Resume Next
    
    ValoriCampiOK = True
    Set listaNonConformitā = New Collection
    
    If IsCampoInteroCorretto(txtMaxCoordOrizzontale) Then
        maxCoordOrizzontale = CLng(Trim(txtMaxCoordOrizzontale.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Max coord. Orizz.")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Max coord. Orizz. deve essere numerico di tipo intero;")
    End If
    If IsCampoInteroCorretto(txtMaxCoordVerticale) Then
        maxCoordVerticale = CLng(Trim(txtMaxCoordVerticale.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Max coord. Vert.")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Max coord. Vert. deve essere numerico di tipo intero;")
    End If
    If IsCampoInteroCorretto(txtDimOrizzontalePosto) Then
        dimOrizzontalePosto = CLng(Trim(txtDimOrizzontalePosto.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Dim. orizz. posto")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Dim. orizz. posto deve essere numerico di tipo intero;")
    End If
    If IsCampoInteroCorretto(txtDimVerticalePosto) Then
        dimVerticalePosto = CLng(Trim(txtDimVerticalePosto.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIntero", "Dim. vert. posto")
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore immesso sul campo Dim. vert. posto deve essere numerico di tipo intero;")
    End If
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If
    
End Function

Private Sub txtMaxCoordOrizzontale_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtMaxCoordVerticale_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDimOrizzontalePosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDimVerticalePosto_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovaGrigliaPosti As Long
    Dim stringaErroreDuplicazioneDati As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    idNuovaGrigliaPosti = OttieniIdentificatoreDaSequenza("SQ_GRIGLIAPOSTI")
    sql = "INSERT INTO GRIGLIAPOSTI (IDGRIGLIAPOSTI, MASSIMACOORDINATAORIZZONTALE,"
    sql = sql & " MASSIMACOORDINATAVERTICALE, DIMENSIONEORIZZONTALEPOSTO,"
    sql = sql & " DIMENSIONEVERTICALEPOSTO, IDAREA, IDPIANTA)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaGrigliaPosti & ", "
    sql = sql & maxCoordOrizzontale & ", "
    sql = sql & maxCoordVerticale & ", "
    sql = sql & dimOrizzontalePosto & ", "
    sql = sql & dimVerticalePosto & ", "
    sql = sql & IIf(tipoGriglia = TG_GRANDI_IMPIANTI, idAreaSelezionata, " NULL") & ", "
    sql = sql & IIf(tipoGriglia = TG_PICCOLI_IMPIANTI, idPiantaSelezionata, " NULL") & ")"
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call SetIdRecordSelezionato(idNuovaGrigliaPosti)
    Call AggiornaAbilitazioneControlli
    
Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Public Function GetGestioneExitCode() As ExitCodeEnum
    GetGestioneExitCode = gestioneExitCode
End Function

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim sql1 As String
'    Dim rec1 As New ADODB.Recordset
    Dim rec1 As OraDynaset
    Dim idA As Long
    Dim idF As Long
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
'   CONTROLLARE CHE NON CI SIANO TITOLI EMESSI PER I POSTI
'   PRIMA FASE: VENGONO ELIMINATE LE FASCE, LE SEQUENZE E I POSTI
    Select Case tipoGriglia
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT IDAREA FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata
'            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            Set rec = ORADB.CreateDynaset(sql, 0&)
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idA = rec("IDAREA")
                    Call EliminaPosti(idA)
                    sql1 = "SELECT IDFASCIAPOSTI FROM FASCIAPOSTI WHERE IDAREA = " & idA
'                    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
                    Set rec1 = ORADB.CreateDynaset(sql1, 0&)
                    If Not (rec1.BOF And rec1.EOF) Then
                        rec1.MoveFirst
                        While Not rec1.EOF
                            idF = rec1("IDFASCIAPOSTI")
                            Call EliminaSequenze(idF)
                            Call EliminaFascia(idF)
                            rec1.MoveNext
                        Wend
                    End If
                    rec1.Close
                    rec.MoveNext
                Wend
            End If
            rec.Close
        Case TG_GRANDI_IMPIANTI
            Call EliminaPosti(idAreaSelezionata)
            sql = "SELECT IDFASCIAPOSTI FROM FASCIAPOSTI WHERE IDAREA = " & idAreaSelezionata
'            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            Set rec = ORADB.CreateDynaset(sql, 0&)
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idF = rec("IDFASCIAPOSTI")
                    Call EliminaSequenze(idF)
                    Call EliminaFascia(idF)
                    rec.MoveNext
                Wend
            End If
            rec.Close
    End Select
    
'   SECONDA FASE: IL RECORD VIENE ELIMINATO
    sql = "DELETE FROM GRIGLIAPOSTI WHERE IDGRIGLIAPOSTI = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA

End Sub

Private Sub EliminaPosti(idA As Long) 'gli passo idArea
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql = "DELETE FROM POSTO WHERE IDAREA = " & idA
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub EliminaSequenze(idF As Long) 'gli passo idFasciaPosti
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql = "DELETE FROM SEQUENZAPOSTI WHERE IDFASCIAPOSTI = " & idF
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub EliminaFasce(idA As Long) 'gli passo idArea
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql = "DELETE FROM FASCIAPOSTI WHERE IDAREA = " & idA
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub EliminaFascia(idF As Long) 'gli passo idArea
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql = "DELETE FROM FASCIAPOSTI WHERE IDFASCIAPOSTI = " & idF
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            sql = "SELECT IDGRIGLIAPOSTI,"
            sql = sql & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
            sql = sql & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO"
            sql = sql & " FROM GRIGLIAPOSTI WHERE"
            sql = sql & " GRIGLIAPOSTI.IDAREA = " & idAreaSelezionata
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT IDGRIGLIAPOSTI,"
            sql = sql & " MASSIMACOORDINATAORIZZONTALE, MASSIMACOORDINATAVERTICALE,"
            sql = sql & " DIMENSIONEORIZZONTALEPOSTO, DIMENSIONEVERTICALEPOSTO"
            sql = sql & " FROM GRIGLIAPOSTI WHERE"
            sql = sql & " GRIGLIAPOSTI.IDPIANTA = " & idPiantaSelezionata
        Case Else
    End Select
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        maxCoordOrizzontale = rec("MASSIMACOORDINATAORIZZONTALE")
        maxCoordVerticale = rec("MASSIMACOORDINATAVERTICALE")
        dimOrizzontalePosto = rec("DIMENSIONEORIZZONTALEPOSTO")
        dimVerticalePosto = rec("DIMENSIONEVERTICALEPOSTO")
        idRecordSelezionato = rec("IDGRIGLIAPOSTI")
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
        
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtMaxCoordOrizzontale.Text = IIf(maxCoordOrizzontale = 0, "", CStr(maxCoordOrizzontale))
    txtMaxCoordVerticale.Text = IIf(maxCoordVerticale = 0, "", CStr(maxCoordVerticale))
    txtDimOrizzontalePosto.Text = IIf(dimOrizzontalePosto = 0, "", CStr(dimOrizzontalePosto))
    txtDimVerticalePosto.Text = IIf(dimVerticalePosto = 0, "", CStr(dimVerticalePosto))
        
    internalEvent = internalEventOld

End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim dom As CCDominioAttivitāEnum

    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            dom = CCDA_AREA
            stringaNota = "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idRecordSelezionato
        Case TG_PICCOLI_IMPIANTI
            dom = CCDA_PIANTA
            stringaNota = "IDPIANTA = " & idPiantaSelezionata
    End Select
    Select Case gestioneRecordGriglia
        Case ASG_INSERISCI_NUOVO
            If ValoriCampiOK Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, dom, CCDA_GRIGLIA_POSTI, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                esisteGrigliaPostiAssociata = True
            End If
        Case ASG_MODIFICA
            If ValoriCampiOK Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, dom, CCDA_GRIGLIA_POSTI, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                esisteGrigliaPostiAssociata = True
            End If
        Case ASG_ELIMINA
            Call EliminaDallaBaseDati
            Call ScriviLog(CCTA_CANCELLAZIONE, dom, CCDA_GRIGLIA_POSTI, stringaNota)
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            esisteGrigliaPostiAssociata = False
    End Select
    Call frmConfigurazionePiantaArea.SetIsAttributiAreaModificati(True)
    Call frmInizialePianta.SetIsAttributiPiantaModificati(True)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    If isAttributiAreaModificati Then
        Call frmConfigurazionePiantaArea.SetIsAttributiAreaModificati(True)
        Call frmInizialePianta.SetIsAttributiPiantaModificati(True)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
On Error GoTo gestioneErrori
    
    sql = "UPDATE GRIGLIAPOSTI SET"
    sql = sql & " MASSIMACOORDINATAORIZZONTALE = " & maxCoordOrizzontale & ","
    sql = sql & " MASSIMACOORDINATAVERTICALE = " & maxCoordVerticale & ","
    sql = sql & " DIMENSIONEORIZZONTALEPOSTO = " & dimOrizzontalePosto & ","
    sql = sql & " DIMENSIONEVERTICALEPOSTO = " & dimVerticalePosto & ","
    sql = sql & " IDAREA = " & IIf(tipoGriglia = TG_GRANDI_IMPIANTI, idAreaSelezionata, "NULL") & ","
    sql = sql & " IDPIANTA = " & IIf(tipoGriglia = TG_PICCOLI_IMPIANTI, idPiantaSelezionata, "NULL")
    sql = sql & " WHERE IDGRIGLIAPOSTI = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
    n = ORADB.ExecuteSQL(sql)
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub

Public Sub SetNomePiantaSelezionata(nomeP As String)
    nomePiantaSelezionata = nomeP
End Sub

Public Sub SetIdPiantaSelezionata(idP As Long)
    idPiantaSelezionata = idP
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetEsisteGrigliaPostiAssociata(exGriglia As Boolean)
    esisteGrigliaPostiAssociata = exGriglia
End Sub

'Private Sub CreaListaAreeAssociateAPianta_old()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim i As Integer
'    Dim n As Integer
'    Dim areaCorrente As InfoArea
'
'    Call ApriConnessioneBD
'
'    sql = "SELECT COUNT(*) FROM AREA WHERE" & _
'        " IDPIANTA = " & idPiantaSelezionata & " AND IDAREA_PADRE NOT IN" & _
'        " (SELECT IDAREA FROM AREA WHERE IDAREA_PADRE IS NOT NULL)"
'    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
'    n = rec("COUNT(*)")
'    rec.Close
'
'    ReDim elencoAreeAssociateAPianta(1 To n)
'
'    sql = "SELECT IDAREA, NOME, DESCRIZIONE FROM AREA WHERE" & _
'        " IDPIANTA = " & idPiantaSelezionata & " AND IDAREA_PADRE NOT IN" & _
'        " (SELECT IDAREA FROM AREA WHERE IDAREA_PADRE IS NOT NULL)"
'    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        i = 0
'        rec.MoveFirst
'        While Not rec.EOF
'            i = i + 1
'            areaCorrente.idArea = rec("IDAREA")
'            areaCorrente.nomeArea = rec("NOME")
'            areaCorrente.descrizioneArea = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
'            elencoAreeAssociateAPianta(i) = areaCorrente
'            rec.MoveNext
'        Wend
'    End If
'
'    rec.Close
'
'    Call ChiudiConnessioneBD
'
'End Sub

Private Sub cmdConfiguraFasceSequenze_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call ConfiguraFasceSequenze
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraFasceSequenze()
    If ValoriCampiOK Then
        Call CaricaFormConfigurazionePiantaFasceSequenze
    End If
End Sub

Private Sub CaricaFormConfigurazionePiantaFasceSequenze()
    Call frmConfigurazionePiantaFasceSequenze.SetIdAreaSelezionata(idAreaSelezionata)
    Call frmConfigurazionePiantaFasceSequenze.SetNomeAreaSelezionata(nomeAreaSelezionata)
    Call frmConfigurazionePiantaFasceSequenze.SetNomePiantaSelezionata(nomePiantaSelezionata)
    Call frmConfigurazionePiantaFasceSequenze.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazionePiantaFasceSequenze.SetTipoGriglia(tipoGriglia)
    Call frmConfigurazionePiantaFasceSequenze.SetMaxCoordOrizzontale(maxCoordOrizzontale)
    Call frmConfigurazionePiantaFasceSequenze.SetMaxCoordVerticale(maxCoordVerticale)
    Call frmConfigurazionePiantaFasceSequenze.SetDimOrizzontalePosto(dimOrizzontalePosto)
    Call frmConfigurazionePiantaFasceSequenze.SetDimVerticalePosto(dimVerticalePosto)
    Call frmConfigurazionePiantaFasceSequenze.Init
End Sub

Private Sub CreaListaAreeAssociateAPianta()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim i As Integer
    Dim n As Integer
'    Dim areaCorrente As InfoArea
    Dim areaCorrente As classeArea
    Dim chiaveArea As String

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans

'    sql = "SELECT COUNT(IDAREA) CONT FROM AREA WHERE" & _
'        " IDPIANTA = " & idPiantaSelezionata & " AND " & _
'        " IDTIPOAREA = " & TA_AREA_NUMERATA
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    n = rec("CONT")
'    rec.Close
    
'    ReDim elencoAreeAssociateAPianta(1 To n)
    Set listaAreeAssociateAPianta = New Collection
    
    sql = "SELECT IDAREA, CODICE || ' - ' || NOME AS NOME, DESCRIZIONE FROM AREA WHERE"
    sql = sql & " IDPIANTA = " & idPiantaSelezionata & " AND "
    sql = sql & " IDTIPOAREA = " & TA_AREA_NUMERATA
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New classeArea
            areaCorrente.idArea = rec("IDAREA").Value
            areaCorrente.nomeArea = rec("NOME")
            areaCorrente.descrizioneArea = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
            chiaveArea = ChiaveId(areaCorrente.idArea)
            Call listaAreeAssociateAPianta.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close

    Call ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA

End Sub


