VERSION 5.00
Begin VB.Form frmInizialeOrganizzazione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   9810
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9810
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkObbligoCodiceSicurezza 
      Alignment       =   1  'Right Justify
      Caption         =   "Obbligo codice sicurezza per titoli digitali per vendite internet"
      Height          =   255
      Left            =   1920
      TabIndex        =   54
      Top             =   3600
      Width           =   4755
   End
   Begin VB.CheckBox chkGestioneTariffeOmaggio 
      Alignment       =   1  'Right Justify
      Caption         =   "Gest tariffe omaggio"
      Height          =   255
      Left            =   7680
      TabIndex        =   48
      Top             =   3600
      Width           =   1755
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   33
      Top             =   8640
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   17
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   18
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   6495
      Left            =   10140
      TabIndex        =   27
      Top             =   540
      Width           =   1695
      Begin VB.CommandButton cmdManager 
         Caption         =   "Manager"
         Height          =   435
         Left            =   120
         TabIndex        =   51
         Top             =   5880
         Width           =   1395
      End
      Begin VB.CommandButton cmdTemplate 
         Caption         =   "Associazione layout HT"
         Height          =   435
         Left            =   120
         TabIndex        =   50
         Top             =   1320
         Width           =   1395
      End
      Begin VB.CommandButton cmdTipiSupportoDigitali 
         Caption         =   "   Associazione Tipi supp. digitali"
         Height          =   435
         Left            =   120
         TabIndex        =   49
         Top             =   780
         Width           =   1395
      End
      Begin VB.CommandButton cmdPerfezionatori 
         Caption         =   "Perfezionatori"
         Height          =   435
         Left            =   120
         TabIndex        =   46
         Top             =   5340
         Width           =   1395
      End
      Begin VB.CommandButton cmdFestività 
         Caption         =   "Festività"
         Height          =   435
         Left            =   120
         TabIndex        =   25
         Top             =   4800
         Width           =   1395
      End
      Begin VB.CommandButton cmdVenditoriEsterni 
         Caption         =   "Venditori esterni"
         Height          =   435
         Left            =   120
         TabIndex        =   24
         Top             =   4260
         Width           =   1395
      End
      Begin VB.CommandButton cmdPuntiVendita 
         Caption         =   "Classi di utenza"
         Height          =   435
         Left            =   120
         TabIndex        =   23
         Top             =   3720
         Width           =   1395
      End
      Begin VB.CommandButton cmdTipiSupporto 
         Caption         =   "   Associazione    Tipi supp./Layout"
         Height          =   435
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   1395
      End
      Begin VB.CommandButton cmdCausaliDiProtezione 
         Caption         =   "Causali di protezione"
         Height          =   435
         Left            =   120
         TabIndex        =   21
         Top             =   2640
         Width           =   1395
      End
      Begin VB.CommandButton cmdModalitaDiPagamento 
         Caption         =   "Modalità di pagamento"
         Height          =   435
         Left            =   120
         TabIndex        =   20
         Top             =   2100
         Width           =   1395
      End
      Begin VB.CommandButton cmdTariffe 
         Caption         =   "Tariffe"
         Height          =   435
         Left            =   120
         TabIndex        =   22
         Top             =   3180
         Width           =   1395
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Caratteristiche"
      Height          =   7935
      Left            =   120
      TabIndex        =   26
      Top             =   600
      Width           =   9855
      Begin VB.TextBox txtPuntoVenditaPerfezionamento 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   57
         Top             =   7440
         Width           =   3315
      End
      Begin VB.ComboBox cmbPuntiVendita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6960
         TabIndex        =   56
         Top             =   7440
         Width           =   2655
      End
      Begin VB.CommandButton cmdCercaPuntiVendita 
         Caption         =   "Cerca punti vendita"
         Height          =   375
         Left            =   5160
         TabIndex        =   55
         Top             =   7440
         Width           =   1575
      End
      Begin VB.TextBox txtPartitaIVA 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7500
         MaxLength       =   16
         TabIndex        =   52
         Top             =   2520
         Width           =   1815
      End
      Begin VB.TextBox txtNumGiorniConservazioneProdotti 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8760
         MaxLength       =   4
         TabIndex        =   4
         Top             =   2100
         Width           =   555
      End
      Begin VB.TextBox txtDescrizionePOS 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   21
         TabIndex        =   3
         Top             =   2100
         Width           =   2355
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   1440
         Width           =   7635
      End
      Begin VB.TextBox txtCodiceFiscale 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4500
         MaxLength       =   16
         TabIndex        =   6
         Top             =   2520
         Width           =   1815
      End
      Begin VB.TextBox txtEMail 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   32
         TabIndex        =   14
         Top             =   6060
         Width           =   3495
      End
      Begin VB.TextBox txtResponsabile 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   15
         Top             =   6720
         Width           =   3255
      End
      Begin VB.TextBox txtFax 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4980
         MaxLength       =   16
         TabIndex        =   13
         Top             =   5640
         Width           =   1815
      End
      Begin VB.TextBox txtTelefono 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   16
         TabIndex        =   12
         Top             =   5640
         Width           =   1815
      End
      Begin VB.TextBox txtCap 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4320
         MaxLength       =   5
         TabIndex        =   9
         Top             =   4380
         Width           =   675
      End
      Begin VB.TextBox txtComune 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   11
         Top             =   5220
         Width           =   3255
      End
      Begin VB.TextBox txtLocalita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   10
         Top             =   4800
         Width           =   3255
      End
      Begin VB.TextBox txtCivico 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   8
         TabIndex        =   8
         Top             =   4380
         Width           =   975
      End
      Begin VB.ComboBox cmbTipoOrganizzazione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6960
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   6720
         Width           =   2415
      End
      Begin VB.TextBox txtIndirizzo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   7
         Top             =   3540
         Width           =   7635
      End
      Begin VB.TextBox txtCodiceTL 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   4
         TabIndex        =   5
         Top             =   2520
         Width           =   555
      End
      Begin VB.TextBox txtRagioneSociale 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   780
         Width           =   7635
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   0
         Top             =   360
         Width           =   3255
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "PV perfezionamento"
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   7440
         Width           =   1455
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "PIVA"
         Height          =   255
         Left            =   6720
         TabIndex        =   53
         Top             =   2580
         Width           =   615
      End
      Begin VB.Label lblNumGiorniConservazioneProdotti 
         Alignment       =   1  'Right Justify
         Caption         =   "numero giorni conservazione prodotti"
         Height          =   255
         Left            =   4920
         TabIndex        =   47
         Top             =   2160
         Width           =   3675
      End
      Begin VB.Label lblDescrizionePOS 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione POS"
         Height          =   255
         Left            =   300
         TabIndex        =   45
         Top             =   2160
         Width           =   1275
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   480
         TabIndex        =   44
         Top             =   1500
         Width           =   1095
      End
      Begin VB.Label lblEMail 
         Alignment       =   1  'Right Justify
         Caption         =   "E-mail"
         Height          =   255
         Left            =   540
         TabIndex        =   43
         Top             =   6120
         Width           =   1035
      End
      Begin VB.Label lblResponsabile 
         Alignment       =   1  'Right Justify
         Caption         =   "responsabile"
         Height          =   255
         Left            =   540
         TabIndex        =   42
         Top             =   6780
         Width           =   1035
      End
      Begin VB.Label lblFax 
         Alignment       =   1  'Right Justify
         Caption         =   "fax"
         Height          =   255
         Left            =   3780
         TabIndex        =   41
         Top             =   5700
         Width           =   1035
      End
      Begin VB.Label lblTelefono 
         Alignment       =   1  'Right Justify
         Caption         =   "telefono"
         Height          =   255
         Left            =   540
         TabIndex        =   40
         Top             =   5700
         Width           =   1035
      End
      Begin VB.Label lblCap 
         Alignment       =   1  'Right Justify
         Caption         =   "CAP"
         Height          =   255
         Left            =   3240
         TabIndex        =   39
         Top             =   4440
         Width           =   915
      End
      Begin VB.Label lblComune 
         Alignment       =   1  'Right Justify
         Caption         =   "comune"
         Height          =   255
         Left            =   540
         TabIndex        =   38
         Top             =   5280
         Width           =   1035
      End
      Begin VB.Label lblLocalita 
         Alignment       =   1  'Right Justify
         Caption         =   "località"
         Height          =   255
         Left            =   540
         TabIndex        =   37
         Top             =   4860
         Width           =   1035
      End
      Begin VB.Label lblCivico 
         Alignment       =   1  'Right Justify
         Caption         =   "civico"
         Height          =   255
         Left            =   540
         TabIndex        =   36
         Top             =   4440
         Width           =   1035
      End
      Begin VB.Label lblCodicaFiscalePartitaIVA 
         Alignment       =   1  'Right Justify
         Caption         =   "CF"
         Height          =   255
         Left            =   3120
         TabIndex        =   35
         Top             =   2580
         Width           =   1215
      End
      Begin VB.Label lblTipoOrganizzazione 
         Alignment       =   1  'Right Justify
         Caption         =   "tipo Organizzazione"
         Height          =   255
         Left            =   5340
         TabIndex        =   34
         Top             =   6780
         Width           =   1455
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "codice TL"
         Height          =   255
         Left            =   660
         TabIndex        =   32
         Top             =   2580
         Width           =   915
      End
      Begin VB.Label lblRagioneSociale 
         Alignment       =   1  'Right Justify
         Caption         =   "ragione sociale"
         Height          =   255
         Left            =   480
         TabIndex        =   31
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "indirizzo"
         Height          =   255
         Left            =   540
         TabIndex        =   30
         Top             =   3540
         Width           =   1035
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   540
         TabIndex        =   29
         Top             =   420
         Width           =   1035
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      TabIndex        =   28
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialeOrganizzazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazione As String
Private ragioneSocialeOrganizzazione As String
Private indirizzoOrganizzazione As String
Private descrizioneOrganizzazione As String
Private descrizionePOS As String
Private numGiorniConservazioneProdotti As Long
Private codiceTLOrganizzazione As String
Private civico As String
Private localita As String
Private cap As String
Private comune As String
Private telefono As String
Private fax As String
Private eMail As String
Private responsabile As String
Private codiceFiscale As String
Private partitaIVA As String
Private codiceFiscaleSocieta As String
Private idTipoOrganizzazioneSelezionato As Long
Private nomeTipoOrganizzazioneSelezionato As String
'Private listaCampiValoriUnici As Collection
Private listaTipiDocumenti As Collection
Private gestEccedenzeOmaggioEvoluta As ValoreBooleanoEnum
Private obbligoCodiceSicurezza As ValoreBooleanoEnum
Private puntoVenditaPerfezionatore As String
Private idPuntoVenditaPerfezionatore As Long
Private internalEvent As Boolean

Private isRecordEditabile As Boolean
Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalitàFormCorrente As AzioneEnum
Private Const ENUMERATO_TIPO_DOCUMENTO As Integer = 5

Public Sub Init()
    Dim sql As String
    Dim i As Long

On Error GoTo errore_inizialeorganizzazione_init
    Call Variabili_Init
    Call AggiornaAbilitazioneControlli

    sql = "SELECT IDTIPOORGANIZZAZIONE AS ID, NOME" & _
        " FROM TIPOORGANIZZAZIONE WHERE" & _
        " IDTIPOORGANIZZAZIONE > 0" & _
        " ORDER BY NOME"
    
    Select Case modalitàFormCorrente
        Case A_NUOVO
            isRecordEditabile = True
            tipoStatoOrganizzazione = TSO_IN_CONFIGURAZIONE
            Call CaricaValoriCombo(cmbTipoOrganizzazione, sql, "NOME")
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call CaricaValoriCombo(cmbTipoOrganizzazione, sql, "NOME")
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call frmInizialeOrganizzazione.Show(vbModal)
    Call AggiornaAbilitazioneControlli
    Exit Sub

errore_inizialeorganizzazione_init:
    MsgBox ("i: " & i)

End Sub

Private Sub Variabili_Init()
    nomeOrganizzazione = ""
    indirizzoOrganizzazione = ""
    descrizioneOrganizzazione = ""
    descrizionePOS = ""
    numGiorniConservazioneProdotti = 180
    codiceTLOrganizzazione = ""
    codiceFiscale = ""
    partitaIVA = ""
    codiceFiscaleSocieta = ""
    ragioneSocialeOrganizzazione = ""
    civico = ""
    localita = ""
    cap = ""
    comune = ""
    telefono = ""
    fax = ""
    eMail = ""
    responsabile = ""
    idTipoOrganizzazioneSelezionato = idNessunElementoSelezionato
    nomeTipoOrganizzazioneSelezionato = ""
    gestEccedenzeOmaggioEvoluta = VB_FALSO
    obbligoCodiceSicurezza = VB_FALSO
    idPuntoVenditaPerfezionatore = idNessunElementoSelezionato
    puntoVenditaPerfezionatore = ""
End Sub

Public Sub SetModalitàForm(mf As AzioneEnum)
    modalitàFormCorrente = mf
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalitàFormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuova Organizzazione"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Organizzazione configurata"
    End Select
    fraAzioni.Visible = (modalitàFormCorrente = A_MODIFICA)
    cmdConferma.Enabled = Trim(txtNome.Text) <> "" And _
        Len(Trim(txtCodiceTL.Text)) = 4 And _
        (Len(Trim(txtCodiceFiscale.Text)) = 11 Or Len(Trim(txtCodiceFiscale.Text)) = 16) And _
        (Len(Trim(txtPartitaIVA.Text)) = 11 Or Len(Trim(txtPartitaIVA.Text)) = 16) And _
        Trim(txtNumGiorniConservazioneProdotti.Text) <> "" And _
        IsNumeric(Trim(txtNumGiorniConservazioneProdotti.Text))
        
    txtPuntoVenditaPerfezionamento = puntoVenditaPerfezionatore

End Sub

Private Sub cmbPuntiVendita_Click()
    If Not internalEvent Then
        idPuntoVenditaPerfezionatore = cmbPuntiVendita.ItemData(cmbPuntiVendita.ListIndex)
        puntoVenditaPerfezionatore = cmbPuntiVendita
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaOrganizzazione.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    isRecordEditabile = True
    sql = "SELECT O.IDORGANIZZAZIONE, O.NOME, O.DESCRIZIONE, O.NUMGIORNICONSERVAZIONEPRODOTTI, O.CODICETERMINALELOTTO," & _
        " O.INDIRIZZO, O.CIVICO, O.LOCALITA, O.CAP, O.COMUNE, O.TELEFONO, O.FAX, O.EMAIL," & _
        " O.RESPONSABILE, O.CODICEFISCALE, O.PARTITAIVA, O.CODICEFISCALESOCIETA, O.IDTIPOORGANIZZAZIONE, O.RAGIONESOCIALE," & _
        " O.IDTIPOSTATOORGANIZZAZIONE, TORG.NOME AS NOMET, O.DESCRIZIONEPOS, O.GESTECCEDENZEOMAGGIOEVOLUTA, O.OBBLIGOCODSICVENDITAINTERNETSD," & _
        " PV.IDPUNTOVENDITA IDPVPERF, PV.NOME PVPERF" & _
        " FROM ORGANIZZAZIONE O, TIPOORGANIZZAZIONE TORG, PUNTOVENDITA PV" & _
        " WHERE O.IDTIPOORGANIZZAZIONE = TORG.IDTIPOORGANIZZAZIONE(+)" & _
        " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND O.IDPUNTOVENDITAPERFEZIONATORE = PV.IDPUNTOVENDITA(+)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeOrganizzazione = rec("NOME")
        ragioneSocialeOrganizzazione = IIf(IsNull(rec("RAGIONESOCIALE")), "", rec("RAGIONESOCIALE"))
        descrizioneOrganizzazione = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        descrizionePOS = IIf(IsNull(rec("DESCRIZIONEPOS")), "", rec("DESCRIZIONEPOS"))
        indirizzoOrganizzazione = IIf(IsNull(rec("INDIRIZZO")), "", rec("INDIRIZZO"))
        numGiorniConservazioneProdotti = IIf(IsNull(rec("NUMGIORNICONSERVAZIONEPRODOTTI")), 0, rec("NUMGIORNICONSERVAZIONEPRODOTTI"))
        codiceTLOrganizzazione = rec("CODICETERMINALELOTTO")
        codiceFiscale = IIf(IsNull(rec("CODICEFISCALE")), "", rec("CODICEFISCALE"))
        partitaIVA = IIf(IsNull(rec("PARTITAIVA")), "", rec("PARTITAIVA"))
        codiceFiscaleSocieta = IIf(IsNull(rec("CODICEFISCALESOCIETA")), "", rec("CODICEFISCALESOCIETA"))
        civico = IIf(IsNull(rec("CIVICO")), "", rec("CIVICO"))
        localita = IIf(IsNull(rec("LOCALITA")), "", rec("LOCALITA"))
        cap = IIf(IsNull(rec("CAP")), "", rec("CAP"))
        comune = IIf(IsNull(rec("COMUNE")), "", rec("COMUNE"))
        telefono = IIf(IsNull(rec("TELEFONO")), "", rec("TELEFONO"))
        fax = IIf(IsNull(rec("FAX")), "", rec("FAX"))
        eMail = IIf(IsNull(rec("EMAIL")), "", rec("EMAIL"))
        responsabile = IIf(IsNull(rec("RESPONSABILE")), "", rec("RESPONSABILE"))
        idTipoOrganizzazioneSelezionato = IIf(IsNull(rec("IDTIPOORGANIZZAZIONE")), idNessunElementoSelezionato, rec("IDTIPOORGANIZZAZIONE"))
        nomeTipoOrganizzazioneSelezionato = IIf(IsNull(rec("NOMET")), "", rec("NOMET"))
        gestEccedenzeOmaggioEvoluta = rec("GESTECCEDENZEOMAGGIOEVOLUTA")
        obbligoCodiceSicurezza = rec("OBBLIGOCODSICVENDITAINTERNETSD")
        idPuntoVenditaPerfezionatore = IIf(IsNull(rec("IDPVPERF")), idNessunElementoSelezionato, rec("IDPVPERF"))
        puntoVenditaPerfezionatore = IIf(IsNull(rec("PVPERF")), "", rec("PVPERF"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtRagioneSociale.Text = ""
    txtIndirizzo.Text = ""
    txtDescrizione.Text = ""
    txtDescrizionePOS.Text = ""
    txtNumGiorniConservazioneProdotti.Text = ""
    txtCodiceTL.Text = ""
    txtCivico.Text = ""
    txtLocalita.Text = ""
    txtCap.Text = ""
    txtComune.Text = ""
    txtTelefono.Text = ""
    txtFax.Text = ""
    txtEMail.Text = ""
    txtResponsabile.Text = ""
    txtPuntoVenditaPerfezionamento.Text = ""
    
    txtNome.Text = nomeOrganizzazione
    txtRagioneSociale.Text = ragioneSocialeOrganizzazione
    txtIndirizzo.Text = indirizzoOrganizzazione
    txtDescrizione.Text = descrizioneOrganizzazione
    txtDescrizionePOS.Text = descrizionePOS
    txtNumGiorniConservazioneProdotti.Text = numGiorniConservazioneProdotti
    txtCodiceTL.Text = codiceTLOrganizzazione
    txtCodiceFiscale.Text = codiceFiscale
    txtPartitaIVA.Text = partitaIVA
    txtCivico.Text = civico
    txtLocalita.Text = localita
    txtCap.Text = cap
    txtComune.Text = comune
    txtTelefono.Text = telefono
    txtFax.Text = fax
    txtEMail.Text = eMail
    txtResponsabile.Text = responsabile
    txtPuntoVenditaPerfezionamento.Text = puntoVenditaPerfezionatore
    Call SelezionaElementoSuCombo(cmbTipoOrganizzazione, idTipoOrganizzazioneSelezionato)
    chkGestioneTariffeOmaggio.Value = gestEccedenzeOmaggioEvoluta
    chkObbligoCodiceSicurezza.Value = obbligoCodiceSicurezza
    internalEvent = internalEventOld
    
End Sub

Private Sub InserisciNellaBaseDati(inserisciInformazioniSistemistiche As Boolean)
    Dim sql As String
    Dim idNuovaOrganizzazione As Long
    Dim idNuovoValoreEnumerato As Long
    Dim nomeSequenza As String
    Dim n As Long
    Dim i As Integer
    Dim idNuovaModalitàDiPagamento As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    idNuovaOrganizzazione = OttieniIdentificatoreDaSequenza("SQ_ORGANIZZAZIONE")
    sql = " INSERT INTO ORGANIZZAZIONE (IDORGANIZZAZIONE, NOME, DESCRIZIONE, DESCRIZIONEPOS,"
    sql = sql & " RAGIONESOCIALE, NUMGIORNICONSERVAZIONEPRODOTTI, CODICETERMINALELOTTO, INDIRIZZO, CIVICO,"
    sql = sql & " LOCALITA, CAP, COMUNE, TELEFONO, FAX, EMAIL, RESPONSABILE,"
    sql = sql & " CODICEFISCALE, PARTITAIVA, CODICEFISCALESOCIETA, IDTIPOSTATOORGANIZZAZIONE, IDTIPOORGANIZZAZIONE, GESTECCEDENZEOMAGGIOEVOLUTA, OBBLIGOCODSICVENDITAINTERNETSD, IDPUNTOVENDITAPERFEZIONATORE)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaOrganizzazione & ", "
    sql = sql & SqlStringValue(nomeOrganizzazione) & ", "
    sql = sql & SqlStringValue(descrizioneOrganizzazione) & ", "
    sql = sql & SqlStringValue(descrizionePOS) & ", "
    sql = sql & SqlStringValue(ragioneSocialeOrganizzazione) & ", "
    sql = sql & numGiorniConservazioneProdotti & ", "
    sql = sql & SqlStringValue(codiceTLOrganizzazione) & ", "
    sql = sql & SqlStringValue(indirizzoOrganizzazione) & ", "
    sql = sql & SqlStringValue(civico) & ", "
    sql = sql & SqlStringValue(localita) & ", "
    sql = sql & SqlStringValue(cap) & ", "
    sql = sql & SqlStringValue(comune) & ", "
    sql = sql & SqlStringValue(telefono) & ", "
    sql = sql & SqlStringValue(fax) & ", "
    sql = sql & SqlStringValue(eMail) & ", "
    sql = sql & SqlStringValue(responsabile) & ", "
    sql = sql & SqlStringValue(codiceFiscale) & ", "
    sql = sql & SqlStringValue(partitaIVA) & ", "
    sql = sql & SqlStringValue(codiceFiscaleSocieta) & ", "
    sql = sql & TSO_IN_CONFIGURAZIONE & ", "
    sql = sql & IIf(idTipoOrganizzazioneSelezionato = idNessunElementoSelezionato, "NULL", idTipoOrganizzazioneSelezionato) & ", "
    sql = sql & gestEccedenzeOmaggioEvoluta & ", "
    sql = sql & obbligoCodiceSicurezza & ", "
    If idPuntoVenditaPerfezionatore = idNessunElementoSelezionato Then
        sql = sql & "NULL"
    Else
        sql = sql & idPuntoVenditaPerfezionatore
    End If
    sql = sql & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    Call CreaListaTipiDocumenti
    For i = 1 To listaTipiDocumenti.count
        idNuovoValoreEnumerato = OttieniIdentificatoreDaSequenza("SQ_VALOREENUMERATO")
        sql = "INSERT INTO VALOREENUMERATO (IDVALOREENUMERATO, IDTIPOENUMERATO,"
        sql = sql & " NOME, DESCRIZIONE, IDORGANIZZAZIONE)"
        sql = sql & " VALUES (" & idNuovoValoreEnumerato & ", "
        sql = sql & ENUMERATO_TIPO_DOCUMENTO & ", "
        sql = sql & SqlStringValue(CStr(listaTipiDocumenti.Item(i))) & ", "
        sql = sql & SqlStringValue(CStr(listaTipiDocumenti.Item(i))) & ", "
        sql = sql & idNuovaOrganizzazione & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next i
    
    'INSERIMENTO TIPO MODALITA DI PAGAMENTO DI DEFAULT = 'NON DEFINITA' PER IL TIPO TERM. POS
    idNuovaModalitàDiPagamento = OttieniIdentificatoreDaSequenza("SQ_MODALITADIPAGAMENTO")
    sql = " INSERT INTO MODALITADIPAGAMENTO ("
    sql = sql & " IDMODALITADIPAGAMENTO, NOME, DESCRIZIONE, "
    sql = sql & " IDORGANIZZAZIONE, IVAPREASSOLTA, IDTIPOMODALITADIPAGAMENTO) "
    sql = sql & " VALUES ("
    sql = sql & idNuovaModalitàDiPagamento & ", "
    sql = sql & "'NON DEFINITA', "
    sql = sql & "NULL, "
    sql = sql & idNuovaOrganizzazione & ", "
    sql = sql & VB_FALSO & ", "
    sql = sql & "0)"
    SETAConnection.Execute sql, n, adCmdText
    sql = " INSERT INTO ORGANIZZAZIONE_MODPAG_TIPOTERM ("
    sql = sql & " IDORGANIZZAZIONE, IDMODALITADIPAGAMENTO, IDTIPOTERMINALE) "
    sql = sql & " VALUES ("
    sql = sql & idNuovaOrganizzazione & ", "
    sql = sql & idNuovaModalitàDiPagamento & ", "
    sql = sql & TT_TERMINALE_POS & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    If inserisciInformazioniSistemistiche Then
        Call InserisciInformazioniSistemistichePerNuovaOrganizzazione(idNuovaOrganizzazione)
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call frmSceltaOrganizzazione.SetIdRecordSelezionato(idNuovaOrganizzazione)
    Call SetIdOrganizzazioneSelezionata(idNuovaOrganizzazione)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CreaListaTipiDocumenti()

    Set listaTipiDocumenti = New Collection
    Call listaTipiDocumenti.Add("PASS")
    Call listaTipiDocumenti.Add("CI")
    Call listaTipiDocumenti.Add("PAT")
End Sub

Private Sub cmdCausaliDiProtezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraCausaliDiProtezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraCausaliDiProtezione()
    Call CaricaFormConfigurazioneOrganizzazioneCausaliDiProtezione
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneCausaliDiProtezione()
    Call frmConfigurazioneOrganizzazioneCausaliDiProtezione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneCausaliDiProtezione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneCausaliDiProtezione.Init
End Sub

Private Sub cmdCercaPuntiVendita_Click()
    CaricaValoriComboPuntiVendita
End Sub

Private Sub CaricaValoriComboPuntiVendita()
    Dim sql As String
    
    If txtPuntoVenditaPerfezionamento.Text = "" Then
        cmbPuntiVendita.Clear
        idPuntoVenditaPerfezionatore = idNessunElementoSelezionato
    Else
        sql = "SELECT IDPUNTOVENDITA ID, NOME" & _
            " FROM PUNTOVENDITA" & _
            " WHERE NOME LIKE '" & txtPuntoVenditaPerfezionamento & "%'" & _
            " ORDER BY NOME"
        Call CaricaValoriCombo3(cmbPuntiVendita, sql, "NOME", 100)
    End If
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim causaNonEditabilita As String
    Dim isConfigurabile As Boolean
    Dim inserisciInformazioniSistemistiche As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    
    If ValoriCampiOK = True Then
        If isRecordEditabile Then
            Select Case modalitàFormCorrente
                Case A_NUOVO
                    inserisciInformazioniSistemistiche = False
                    Call frmMessaggio.Visualizza("ConfermaInserimentoInformazioniSistemistiche")
                    If frmMessaggio.exitCode = EC_CONFERMA Then
                        inserisciInformazioniSistemistiche = True
                    End If
                    Call InserisciNellaBaseDati(inserisciInformazioniSistemistiche)
                    Call BloccaDominioPerUtente(CCDA_ORGANIZZAZIONE, idOrganizzazioneSelezionata, isOrganizzazioneBloccataDaUtente)
                    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_ORGANIZZAZIONE, stringaNota, idOrganizzazioneSelezionata)
                    Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                    Call SetModalitàForm(A_MODIFICA)
                    Call AggiornaAbilitazioneControlli
                Case A_MODIFICA
                    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
                        isConfigurabile = True
                        If tipoStatoOrganizzazione = TSO_ATTIVA Then
                            Call frmMessaggio.Visualizza("ConfermaEditabilitàOrganizzazioneAttiva")
                            If frmMessaggio.exitCode <> EC_CONFERMA Then
                                isConfigurabile = False
                            End If
                        End If
                        If isConfigurabile Then
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_ORGANIZZAZIONE, CCDA_ORGANIZZAZIONE, stringaNota, idOrganizzazioneSelezionata)
                            Call frmMessaggio.Visualizza("NotificaModificaDati")
                            Call Esci
                        End If
                    Else
                        Call frmMessaggio.Visualizza("NotificaNonEditabilitàCampi", causaNonEditabilita)
                    End If
            End Select
        End If
    End If
    Call frmSceltaOrganizzazione.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub cmdFestività_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraFestività
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraFestività()
    Call CaricaFormConfigurazioneOrganizzazioneFestività
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneFestività()
    Call frmConfigurazioneOrganizzazioneFestività.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneFestività.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneFestività.Init
End Sub

Private Sub cmdModalitaDiPagamento_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraModalitàDiPagamento
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraModalitàDiPagamento()
    Call CaricaFormConfigurazioneOrganizzazioneModalitaDiPagamento
End Sub

Private Sub cmdPerfezionatori_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraPerfezionatori
    
    MousePointer = mousePointerOld

End Sub
Private Sub ConfiguraPerfezionatori()
    Call CaricaFormConfigurazioneOrganizzazionePerfezionatori
End Sub

Private Sub cmdManager_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraManager
    
    MousePointer = mousePointerOld

End Sub
Private Sub ConfiguraManager()
    Call CaricaFormConfigurazioneOrganizzazioneManager
End Sub

Private Sub cmdTariffe_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTariffe
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraTariffe()
    Call CaricaFormConfigurazioneOrganizzazioneTariffe
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneTariffe()
    Call frmConfigurazioneOrganizzazioneTariffe.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneTariffe.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneTariffe.Init
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneModalitaDiPagamento()
    Call frmConfigurazioneOrganizzazioneModalitaDiPagamento.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneModalitaDiPagamento.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneModalitaDiPagamento.Init
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneTipiSupporto()
    Call frmConfigurazioneOrganizzazioneTipiSupporto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneTipiSupporto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneTipiSupporto.Init
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneTipiSupportoDigitali()
    Call frmConfigurazioneOrganizzazioneTipiSupportoDigitali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneTipiSupportoDigitali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneTipiSupportoDigitali.Init
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneTemplate()
    Call frmConfigurazioneOrganizzazioneTemplate.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneTemplate.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneTemplate.Init
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazionePerfezionatori()
    Call frmConfigurazioneOrganizzazionePerfezionatori.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazionePerfezionatori.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazionePerfezionatori.Init
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneManager()
    Call frmConfigurazioneOrganizzazioneManager.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneManager.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneManager.Init
End Sub

Private Sub cmdTipiSupporto_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTipiSupporto
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdTipiSupportoDigitali_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTipiSupportoDigitali
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdTemplate_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTemplate
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraTipiSupporto()
    Call CaricaFormConfigurazioneOrganizzazioneTipiSupporto
End Sub

Private Sub ConfiguraTipiSupportoDigitali()
    Call CaricaFormConfigurazioneOrganizzazioneTipiSupportoDigitali
End Sub

Private Sub ConfiguraTemplate()
    Call CaricaFormConfigurazioneOrganizzazioneTemplate
End Sub

Private Sub cmdVenditoriEsterni_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraVenditoreEsterno
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraVenditoreEsterno()
    Call CaricaFormConfigurazioneOrganizzazioneVenditoreEsterno
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazioneVenditoreEsterno()
    Call frmConfigurazioneOrganizzazioneVenditoreEsterno.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazioneVenditoreEsterno.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazioneVenditoreEsterno.Init
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtIndirizzo_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtDescrizione_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCodiceTL_Change()
    Call AggiornaAbilitazioneControlli
End Sub
Private Sub txtNumGiorniConservazioneProdotti_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtComune_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtLocalita_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCap_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtFax_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtTelefono_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtResponsabile_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txteMail_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCodiceFiscale_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtPartitaIVA_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtCodiceFiscaleSocieta_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    sql = "UPDATE ORGANIZZAZIONE SET" & _
        " NOME = " & SqlStringValue(nomeOrganizzazione) & "," & _
        " RAGIONESOCIALE = " & SqlStringValue(ragioneSocialeOrganizzazione) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneOrganizzazione) & "," & _
        " DESCRIZIONEPOS = " & SqlStringValue(descrizionePOS) & "," & _
        " INDIRIZZO = " & SqlStringValue(indirizzoOrganizzazione) & "," & _
        " NUMGIORNICONSERVAZIONEPRODOTTI = " & numGiorniConservazioneProdotti & "," & _
        " CODICETERMINALELOTTO = " & SqlStringValue(codiceTLOrganizzazione) & "," & _
        " CODICEFISCALE = " & SqlStringValue(codiceFiscale) & "," & _
        " PARTITAIVA = " & SqlStringValue(partitaIVA) & "," & _
        " CODICEFISCALESOCIETA = " & SqlStringValue(codiceFiscaleSocieta) & "," & _
        " CIVICO = " & SqlStringValue(civico) & "," & _
        " LOCALITA = " & SqlStringValue(localita) & "," & _
        " CAP = " & SqlStringValue(cap) & "," & _
        " COMUNE = " & SqlStringValue(comune) & "," & _
        " TELEFONO = " & SqlStringValue(telefono) & "," & _
        " FAX = " & SqlStringValue(fax) & "," & _
        " EMAIL = " & SqlStringValue(eMail) & "," & _
        " RESPONSABILE = " & SqlStringValue(responsabile) & "," & _
        " IDTIPOORGANIZZAZIONE = " & IIf(idTipoOrganizzazioneSelezionato = idNessunElementoSelezionato, "NULL", idTipoOrganizzazioneSelezionato) & "," & _
        " GESTECCEDENZEOMAGGIOEVOLUTA = " & gestEccedenzeOmaggioEvoluta & "," & _
        " OBBLIGOCODSICVENDITAINTERNETSD = " & obbligoCodiceSicurezza & ","
        If idPuntoVenditaPerfezionatore = idNessunElementoSelezionato Then
            sql = sql & " IDPUNTOVENDITAPERFEZIONATORE = NULL"
        Else
            sql = sql & " IDPUNTOVENDITAPERFEZIONATORE = " & idPuntoVenditaPerfezionatore
        End If
        sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    SETAConnection.Execute sql, n, adCmdText
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformità As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    Set listaNonConformità = New Collection
    condizioneSql = ""
    If modalitàFormCorrente = A_MODIFICA Or modalitàFormCorrente = A_ELIMINA Then
        condizioneSql = "IDORGANIZZAZIONE <> " & idOrganizzazioneSelezionata
    End If
    
    nomeOrganizzazione = Trim(txtNome.Text)
    If ViolataUnicità("ORGANIZZAZIONE", "NOME = " & SqlStringValue(nomeOrganizzazione), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformità.Add("- il valore nome = " & SqlStringValue(nomeOrganizzazione) & _
            " è già presente in DB;")
    End If
    ragioneSocialeOrganizzazione = Trim(txtRagioneSociale.Text)
    descrizioneOrganizzazione = Trim(txtDescrizione.Text)
    descrizionePOS = Trim(txtDescrizionePOS.Text)
    indirizzoOrganizzazione = Trim(txtIndirizzo.Text)
    numGiorniConservazioneProdotti = Trim(txtNumGiorniConservazioneProdotti.Text)
    codiceTLOrganizzazione = Trim(txtCodiceTL.Text)
    If ViolataUnicità("ORGANIZZAZIONE", "CODICETERMINALELOTTO = " & SqlStringValue(codiceTLOrganizzazione), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformità.Add("- il valore codice TL = " & SqlStringValue(codiceTLOrganizzazione) & _
            " è già presente in DB;")
    End If
    codiceFiscale = Trim(txtCodiceFiscale.Text)
    If ViolataUnicità("ORGANIZZAZIONE", "CODICEFISCALE = " & SqlStringValue(codiceFiscale), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformità.Add("- il valore codice fiscale = " & SqlStringValue(codiceFiscale) & _
            " è già presente in DB;")
    End If
    partitaIVA = Trim(txtPartitaIVA.Text)
    If ViolataUnicità("ORGANIZZAZIONE", "PARTITAIVA = " & SqlStringValue(partitaIVA), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformità.Add("- il valore partita IVA = " & SqlStringValue(partitaIVA) & _
            " è già presente in DB;")
    End If
    civico = Trim(txtCivico.Text)
    localita = Trim(txtLocalita.Text)
    cap = Trim(txtCap.Text)
    comune = Trim(txtComune.Text)
    telefono = Trim(txtTelefono.Text)
    fax = Trim(txtFax.Text)
    eMail = Trim(txtEMail.Text)
    responsabile = Trim(txtResponsabile.Text)
   
    gestEccedenzeOmaggioEvoluta = chkGestioneTariffeOmaggio.Value
    obbligoCodiceSicurezza = chkObbligoCodiceSicurezza.Value
    
    If listaNonConformità.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitàCampi", ArgomentoMessaggio(listaNonConformità))
    End If

End Function

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub cmbTipoOrganizzazione_Click()
    idTipoOrganizzazioneSelezionato = cmbTipoOrganizzazione.ItemData(cmbTipoOrganizzazione.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeOrganizzazione))
'    Call listaCampiValoriUnici.Add("CODICETERMINALELOTTO = " & SqlStringValue(codiceTLOrganizzazione))
'    Call listaCampiValoriUnici.Add("CODICEFISCALE = " & SqlStringValue(codiceFiscalePartitaIVA))
'End Sub

Private Sub cmdPuntiVendita_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraPuntiVendita
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraPuntiVendita()
    Call CaricaFormConfigurazioneOrganizzazionePuntiVendita
End Sub

Private Sub CaricaFormConfigurazioneOrganizzazionePuntiVendita()
    Call frmConfigurazioneOrganizzazionePuntiVendita.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneOrganizzazionePuntiVendita.SetNomeOrganizzazioneSelezionata(nomeOrganizzazione)
    Call frmConfigurazioneOrganizzazionePuntiVendita.Init
End Sub

Private Sub Esci()
    Unload Me
End Sub

