VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSceltaPuntoVendita 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Punto Vendita"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   1
      Top             =   7620
      Width           =   5355
      Begin VB.CommandButton cmdClona 
         Caption         =   "Clona"
         Height          =   435
         Left            =   2700
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   0
      Top             =   8100
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcPuntoVendita 
      Height          =   375
      Left            =   8220
      Top             =   60
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrPuntiVenditaConfigurati 
      Height          =   6795
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   11986
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione del Punto Vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   7635
   End
End
Attribute VB_Name = "frmSceltaPuntoVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long

Private internalEvent As Boolean
Private exitCodeFormIniziale As ExitCodeEnum

Private Sub cmdClona_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CaricaFormClonazionePuntoVendita
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaPuntoVendita
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ModificaPuntoVendita
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call NuovoPuntoVendita
    
    MousePointer = mousePointerOld
End Sub

Private Sub NuovoPuntoVendita()
    Call frmInizialePuntoVendita.SetModalitāForm(A_NUOVO)
    Call frmInizialePuntoVendita.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaPuntoVendita()
    Call frmInizialePuntoVendita.SetModalitāForm(A_MODIFICA)
    Call frmInizialePuntoVendita.SetIdPuntoVenditaSelezionato(idRecordSelezionato)
    Call frmInizialePuntoVendita.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaPuntoVendita()
    Dim PuntoVenditaEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    
    Set listaTabelleCorrelate = New Collection
    
    Call frmInizialePuntoVendita.SetModalitāForm(A_ELIMINA)
    
    PuntoVenditaEliminabile = True
    If Not IsRecordEliminabile("IDPUNTOVENDITA", "ORGANIZZAZIONE_PUNTOVENDITA", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("ORGANIZZAZIONE_PUNTOVENDITA")
        PuntoVenditaEliminabile = False
    End If
    If Not frmInizialePuntoVendita.NonAssociatoConOperazioni Then
        Call listaTabelleCorrelate.Add("TERMINALE")
        PuntoVenditaEliminabile = False
    End If

    If PuntoVenditaEliminabile Then
        Call frmInizialePuntoVendita.SetIdPuntoVenditaSelezionato(idRecordSelezionato)
        Call frmInizialePuntoVendita.Init
        If exitCodeFormIniziale = EC_CONFERMA Then
            Call CaricaValoriGriglia
            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il punto vendita selezionato", tabelleCorrelate)
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub AggiornaAbilitazioneControlli()

    dgrPuntiVenditaConfigurati.Columns(0).Visible = False
    dgrPuntiVenditaConfigurati.Caption = "PUNTI VENDITA CONFIGURATI"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdClona.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    
End Sub

Private Sub dgrPuntiVenditaConfigurati_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrPuntiVenditaConfigurati, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcPuntoVendita.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init()

    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcPuntoVendita_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call dgrPuntiVenditaConfigurati_Init
End Sub

Private Sub adcPuntoVendita_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcPuntoVendita
    
    sql = "SELECT PV.IDPUNTOVENDITA ID," & _
        " PV.NOME ""Nome"", TPV.NOME ""Tipo""," & _
        " CITTA AS ""Cittā"", PROVINCIA AS ""Prov.""," & _
        " TELEFONO AS ""Telefono"", CODICESAP AS ""Codice SAP""" & _
        " FROM PUNTOVENDITA PV, TIPOPUNTOVENDITA TPV" & _
        " WHERE PV.IDTIPOPUNTOVENDITA = TPV.IDTIPOPUNTOVENDITA" & _
        " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrPuntiVenditaConfigurati.dataSource = d
    
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrPuntiVenditaConfigurati_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrPuntiVenditaConfigurati
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 6
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcPuntoVendita.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Public Sub SetExitCodeFormIniziale(exC As ExitCodeEnum)
    exitCodeFormIniziale = exC
End Sub

Private Sub CaricaFormClonazionePuntoVendita()
    Call frmClonazionePuntoVendita.SetIdPuntoVenditaSelezionato(idRecordSelezionato)
    Call frmClonazionePuntoVendita.Init
    Call CaricaValoriGriglia
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

