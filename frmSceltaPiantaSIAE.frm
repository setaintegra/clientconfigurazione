VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmSceltaPiantaSIAE 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta SIAE"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   2
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   2700
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   1
      Top             =   8100
      Width           =   1155
   End
   Begin VB.ComboBox cmbPianta 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   900
      Width           =   4095
   End
   Begin MSAdodcLib.Adodc adcPiantaSIAE 
      Height          =   375
      Left            =   300
      Top             =   6840
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrPianteSIAEConfigurate 
      Height          =   6075
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10716
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione della Pianta SIAE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   7635
   End
   Begin VB.Label lblPianta 
      Caption         =   "Pianta"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   660
      Width           =   1695
   End
End
Attribute VB_Name = "frmSceltaPiantaSIAE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idPiantaSelezionata As Long

Private internalEvent As Boolean
Private exitCodeFormIniziale As ExitCodeEnum

Private Sub cmbPianta_Click()
    Call cmbPianta_Update
End Sub

Private Sub cmbPianta_Update()
    idPiantaSelezionata = cmbPianta.ItemData(cmbPianta.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaPiantaSIAE
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ModificaPiantaSIAE
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call NuovaPiantaSIAE
    
    MousePointer = mousePointerOld
End Sub

Private Sub NuovaPiantaSIAE()
    Call frmInizialePiantaSIAE.SetModalitāForm(A_NUOVO)
    Call frmInizialePiantaSIAE.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaPiantaSIAE()
    Call frmInizialePiantaSIAE.SetModalitāForm(A_MODIFICA)
    Call frmInizialePiantaSIAE.SetIdPiantaSIAESelezionata(idRecordSelezionato)
    Call frmInizialePiantaSIAE.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmInizialePiantaSIAE.Init
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaPiantaSIAE()
    Dim piantaSIAEEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    
    Set listaTabelleCorrelate = New Collection
    
    Call frmInizialePiantaSIAE.SetModalitāForm(A_ELIMINA)
    
    piantaSIAEEliminabile = True
    If Not IsRecordEliminabile("IDPIANTASIAE", "PRODOTTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PRODOTTO")
        piantaSIAEEliminabile = False
    End If

    If piantaSIAEEliminabile Then
        Call frmInizialePiantaSIAE.SetIdPiantaSIAESelezionata(idRecordSelezionato)
        Call frmInizialePiantaSIAE.SetIdPiantaSelezionata(idPiantaSelezionata)
        Call frmInizialePiantaSIAE.Init
        If exitCodeFormIniziale = EC_CONFERMA Then
            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
            Call CaricaValoriGriglia
        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La pianta SIAE selezionata", tabelleCorrelate)
    End If
    
'    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Unload Me
End Sub
Private Sub AggiornaAbilitazioneControlli()

    dgrPianteSIAEConfigurate.Columns(0).Visible = False
    dgrPianteSIAEConfigurate.Caption = "PIANTE SIAE CONFIGURATE"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    
End Sub

Private Sub dgrPianteSIAEConfigurate_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrPianteSIAEConfigurate, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcPiantaSIAE.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init()
    Dim sql As String
    
    sql = "SELECT IDPIANTA ID, NOME FROM PIANTA ORDER BY NOME"
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriCombo(cmbPianta, sql, "NOME")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub CaricaValoriGriglia()
    Call adcPiantaSIAE_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call dgrPianteSIAEConfigurate_Init
End Sub

Private Sub adcPiantaSIAE_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Set d = adcPiantaSIAE
    
    Select Case idPiantaSelezionata
        Case idTuttiGliElementiSelezionati
            sql = "SELECT PS.IDPIANTASIAE AS ""ID"", PS.NOME AS ""Nome""," & _
                " PS.DESCRIZIONE AS ""Descrizione"", P.NOME AS ""Pianta""" & _
                " FROM PIANTASIAE PS, PIANTA P" & _
                " WHERE PS.IDPIANTA = P.IDPIANTA" & _
                " ORDER BY ""Nome"""
        Case Else
            sql = "SELECT PS.IDPIANTASIAE AS ""ID"", PS.NOME AS ""Nome""," & _
                " PS.DESCRIZIONE AS ""Descrizione"", P.NOME AS ""Pianta""" & _
                " FROM PIANTASIAE PS, PIANTA P" & _
                " WHERE PS.IDPIANTA = P.IDPIANTA" & _
                " AND PS.IDPIANTA = " & idPiantaSelezionata & _
                " ORDER BY ""Nome"""
    End Select
            
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrPianteSIAEConfigurate.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrPianteSIAEConfigurate_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrPianteSIAEConfigurate
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcPiantaSIAE.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
'    cmb.AddItem "<nessuno>"
'    cmb.ItemData(i) = idNessunElementoSelezionato
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Public Sub SetExitCodeFormIniziale(exC As ExitCodeEnum)
    exitCodeFormIniziale = exC
End Sub





