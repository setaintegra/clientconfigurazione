VERSION 5.00
Begin VB.Form frmDettagliRappresentazioniScadute 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Rappresentazioni scadute"
   ClientHeight    =   5505
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5925
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5505
   ScaleWidth      =   5925
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   1080
      TabIndex        =   6
      Top             =   5040
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   3900
      TabIndex        =   5
      Top             =   5040
      Width           =   1035
   End
   Begin VB.CommandButton cmdDeselezionaTutte 
      Caption         =   "deseleziona tutte"
      Height          =   315
      Left            =   2940
      TabIndex        =   2
      Top             =   4560
      Width           =   2895
   End
   Begin VB.CommandButton cmdSelezionaTutte 
      Caption         =   "seleziona tutte"
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   4560
      Width           =   2895
   End
   Begin VB.ListBox lstRappresentazioniScadute 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3660
      Left            =   60
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   900
      Width           =   5775
   End
   Begin VB.Label lblNota 
      Caption         =   "NOTA: selezionare le rappresentazioni delle quali si vogliono eliminare gli utilizzatori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   435
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   5715
   End
   Begin VB.Label lblRappresentazioniScadute 
      Caption         =   "Rappresentazioni scadute"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   660
      Width           =   2955
   End
End
Attribute VB_Name = "frmDettagliRappresentazioniScadute"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private idOrganizzazioneSelezionata As Long
'Private dirittiOperatore As clsDiritOperOrg

Private internalEvent As Boolean
Private exitCode As ExitCodeEnum
'
'Public Sub Init()
'    Dim sql As String
'
'    Set dirittiOperatore = New clsDiritOperOrg
'    dirittiOperatore.idOrganizzazione = idOrganizzazioneSelezionata
'    Call dirittiOperatore.Init
'    sql = " SELECT DISTINCT O.IDOPERATORE ID, USERNAME"
'    sql = sql & " FROM OPERATORE O, PUNTOVENDITA P, OPERATORE_ORGANIZZAZIONE OO"
'    sql = sql & " WHERE O.IDOPERATORE = OO.IDOPERATORE"
'    sql = sql & " AND O.ABILITATO = " & VB_VERO
'    sql = sql & " AND OO.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'    sql = sql & " AND ((O.IDPUNTOVENDITA = P.IDPUNTOVENDITA"
'    sql = sql & " AND P.IDTIPOPUNTOVENDITA = " & TPV_SPECIALE & ")"
'    sql = sql & " OR O.IDPUNTOVENDITA IS NULL)"
'    sql = sql & " ORDER BY USERNAME"
'    Call CaricaValoriLstProdotti
'    Call CaricaValoriCombo(cmbOperatoriMaster, sql, "USERNAME")
'    Call AggiornaAbilitazioneControlli
'    Me.Show (vbModal)
'End Sub
'
'Private Sub AggiornaAbilitazioneControlli()
''    cmdConferma.Enabled = dirittiOperatore.listaProdottiSelezionati.count > 0 And ''        (dirittiOperatore.idOperatoreSelezionato <> idNessunElementoSelezionato Or ''        dirittiOperatore.importaDirittiSuTutteLeTariffe = VB_VERO Or ''        dirittiOperatore.importaDirittiSuTutteLeProtezioni = VB_VERO)
'    If dirittiOperatore.selezionaDirittiDaOperatoreMaster Then
'        cmdConferma.Enabled = dirittiOperatore.listaProdottiSelezionati.count > 0 And '            dirittiOperatore.idOperatoreSelezionato <> idNessunElementoSelezionato
'    ElseIf dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto Then
'        cmdConferma.Enabled = dirittiOperatore.listaProdottiSelezionati.count > 0 And '            (dirittiOperatore.importaDirittiSuTutteLeTariffe = VB_VERO Or '            dirittiOperatore.importaDirittiSuTutteLeProtezioni = VB_VERO) And '            (dirittiOperatore.importaDirittiSuTutteLeOperazioni Or '            dirittiOperatore.importaDirittiSuSetMinimoOperazioni)
'    Else
'        cmdConferma.Enabled = False
'    End If
'    fraSelezioneDiritti.Enabled = (dirittiOperatore.listaProdottiSelezionati.count > 0)
'    cmbOperatoriMaster.Enabled = dirittiOperatore.selezionaDirittiDaOperatoreMaster
'    chkTariffe.Enabled = dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto
'    chkProtezioni.Enabled = dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto
'    fraOperazioni.Enabled = dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto
'    optTutteLeOperazioni.Enabled = dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto
'    optSoloSetMinimoOperazioni.Enabled = dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto
'End Sub
'
'Public Sub SetIdOrganizzazioneCorrente(idOrganizzazione As Long)
'    idOrganizzazioneSelezionata = idOrganizzazione
'End Sub
'
'Private Sub CaricaValoriLstProdotti()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim idProdotto As Long
'    Dim descr As String
'    Dim i As Integer
'
'    i = 1
'    Call ApriConnessioneBD
'    Call lstProdotti.Clear
'
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = " SELECT DISTINCT P.IDPRODOTTO ID, P.NOME, R.DATAORAFINE"
'        sql = sql & " FROM PRODOTTO P,"
'        sql = sql & " ("
'        sql = sql & " SELECT PR.IDPRODOTTO, "
'        sql = sql & " MAX(R.DATAORAINIZIO + (R.DURATAINMINUTI/1440)) DATAORAFINE"
'        sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, PRODOTTO P"
'        sql = sql & " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
'        sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO "
'        sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql & " GROUP BY PR.IDPRODOTTO"
'        sql = sql & " ) R"
'        sql = sql & " WHERE P.IDPRODOTTO = R.IDPRODOTTO(+)"
'        sql = sql & " AND R.DATAORAFINE > SYSDATE"
'        sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql & " AND (P.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'        sql = sql & " OR P.IDTIPOSTATORECORD IS NULL)"
'        sql = sql & " ORDER BY NOME"
'    Else
'        sql = " SELECT DISTINCT P.IDPRODOTTO ID, P.NOME, R.DATAORAFINE"
'        sql = sql & " FROM PRODOTTO P,"
'        sql = sql & " ("
'        sql = sql & " SELECT PR.IDPRODOTTO, "
'        sql = sql & " MAX(R.DATAORAINIZIO + (R.DURATAINMINUTI/1440)) DATAORAFINE"
'        sql = sql & " FROM PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R, PRODOTTO P"
'        sql = sql & " WHERE PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE"
'        sql = sql & " AND P.IDPRODOTTO = PR.IDPRODOTTO "
'        sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql & " GROUP BY PR.IDPRODOTTO"
'        sql = sql & " ) R"
'        sql = sql & " WHERE P.IDPRODOTTO = R.IDPRODOTTO(+)"
'        sql = sql & " AND R.DATAORAFINE > SYSDATE"
'        sql = sql & " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'        sql = sql & " ORDER BY NOME"
'    End If
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            idProdotto = rec("ID").Value
'            descr = rec("NOME")
'            Call lstProdotti.AddItem(descr)
'            lstProdotti.ItemData(i - 1) = idProdotto
'            i = i + 1
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'    Call ChiudiConnessioneBD
'End Sub
'
'Private Sub chkTariffe_Click()
'    If Not internalEvent Then
'        Call chkTariffe_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub chkTariffe_Update()
'    If chkTariffe.Value = vbChecked Then
'        dirittiOperatore.importaDirittiSuTutteLeTariffe = VB_VERO
'    Else
'        dirittiOperatore.importaDirittiSuTutteLeTariffe = VB_FALSO
'    End If
'End Sub
'
'Private Sub chkProtezioni_Click()
'    If Not internalEvent Then
'        Call chkProtezioni_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub chkProtezioni_Update()
'    If chkProtezioni.Value = vbChecked Then
'        dirittiOperatore.importaDirittiSuTutteLeProtezioni = VB_VERO
'    Else
'        dirittiOperatore.importaDirittiSuTutteLeProtezioni = VB_FALSO
'    End If
'End Sub
'
'Private Sub cmbOperatoriMaster_Click()
'    If Not internalEvent Then
'        Call cmbOperatoriMaster_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub cmbOperatoriMaster_Update()
'    dirittiOperatore.idOperatoreSelezionato = cmbOperatoriMaster.ItemData(cmbOperatoriMaster.ListIndex)
'End Sub
'
'Private Sub cmdAnnulla_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call Annulla
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub Annulla()
'    exitCode = EC_ANNULLA
'    Unload Me
'End Sub
'
'Private Sub cmdConferma_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call Conferma
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub Conferma()
'    'aggiornare i dati
'    exitCode = EC_CONFERMA
'    Unload Me
'End Sub
'
'Private Sub lstProdotti_Click()
'    Call VisualizzaListBoxToolTip(lstProdotti, "ID = " & lstProdotti.ItemData(lstProdotti.ListIndex) & "; " & lstProdotti.Text)
'End Sub
'
'Private Sub lstProdotti_ItemCheck(Item As Integer)
'    If Not internalEvent Then
'        Call SelezionaDeselezionaProdotto
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub SelezionaDeselezionaProdotto()
'    Dim idProdotto As Long
'    Dim prodotto As clsElementoLista
'
'On Error Resume Next
'
'    idProdotto = lstProdotti.ItemData(lstProdotti.ListIndex)
'    If lstProdotti.Selected(lstProdotti.ListIndex) Then
'        Set prodotto = New clsElementoLista
'        prodotto.idElementoLista = idProdotto
'        prodotto.descrizioneElementoLista = lstProdotti.Text
'        Call dirittiOperatore.listaProdottiSelezionati.Add(prodotto, ChiaveId(idProdotto))
'    Else
'        Call dirittiOperatore.listaProdottiSelezionati.Remove(ChiaveId(idProdotto))
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub cmdSelezionaTutti_Click()
'    Call SelezionaTuttiProdotti
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub SelezionaTuttiProdotti()
'    Dim i As Integer
'    Dim internalEventOld As Boolean
'    Dim idProdotto As Long
'    Dim prodotto As clsElementoLista
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    Set dirittiOperatore.listaProdottiSelezionati = Nothing
'    Set dirittiOperatore.listaProdottiSelezionati = New Collection
'    For i = 1 To lstProdotti.ListCount
'        Set prodotto = New clsElementoLista
'        idProdotto = lstProdotti.ItemData(i - 1)
'        prodotto.idElementoLista = idProdotto
'        prodotto.descrizioneElementoLista = lstProdotti.Text
'        Call dirittiOperatore.listaProdottiSelezionati.Add(prodotto, ChiaveId(idProdotto))
'        lstProdotti.Selected(i - 1) = True
'    Next i
'
'    internalEvent = internalEventOld
'End Sub
'
'Private Sub cmdDeselezionaTutti_Click()
'    Call DeselezionaTuttiProdotti
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub DeselezionaTuttiProdotti()
'    Dim i As Integer
'    Dim internalEventOld As Boolean
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    For i = 1 To lstProdotti.ListCount
'        lstProdotti.Selected(i - 1) = False
'    Next i
'    Set dirittiOperatore.listaProdottiSelezionati = Nothing
'    Set dirittiOperatore.listaProdottiSelezionati = New Collection
'
'    internalEvent = internalEventOld
'End Sub
'
'Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, nomeCampo As String)
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim i As Integer
'
'    Call ApriConnessioneBD
'
'    sql = strSQL
'
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        i = 1
'        rec.MoveFirst
'        While Not rec.EOF
'            cmb.AddItem rec(nomeCampo)
'            cmb.ItemData(i - 1) = rec("ID").Value
'            i = i + 1
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'
'    Call ChiudiConnessioneBD
'
'End Sub
'
'Public Function GetExitCode() As ExitCodeEnum
'    GetExitCode = exitCode
'End Function
'
'Public Function GetDirittiOperatore() As clsDiritOperOrg
'    Set GetDirittiOperatore = dirittiOperatore
'End Function
'
'Private Sub optOperatoreMaster_Click()
'    If Not internalEvent Then
'        Call ModalitāInserimentoDiritti_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
''
''Private Sub optOperatoreMaster_Update()
''    dirittiOperatore.selezionaDirittiDaOperatoreMaster = optOperatoreMaster.Value
''    dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto = optCaratteristicheProdotto.Value
''    Call dirittiOperatore.Update
''End Sub
'
'Private Sub optCaratteristicheProdotto_Click()
'    If Not internalEvent Then
'        Call ModalitāInserimentoDiritti_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub ModalitāInserimentoDiritti_Update()
'    dirittiOperatore.selezionaDirittiDaOperatoreMaster = optOperatoreMaster.Value
'    dirittiOperatore.selezionaDirittiSuCaratteristicheProdotto = optCaratteristicheProdotto.Value
'    Call dirittiOperatore.Update
'End Sub
'
'Private Sub optSoloSetMinimoOperazioni_Click()
'    If Not internalEvent Then
'        Call DirittiSuOperazioni_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub optTutteLeOperazioni_Click()
'    If Not internalEvent Then
'        Call DirittiSuOperazioni_Update
'    End If
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub DirittiSuOperazioni_Update()
'    dirittiOperatore.importaDirittiSuTutteLeOperazioni = optTutteLeOperazioni.Value
'    dirittiOperatore.importaDirittiSuSetMinimoOperazioni = optSoloSetMinimoOperazioni.Value
'    Call dirittiOperatore.Update
'End Sub
'
'
'
