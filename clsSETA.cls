VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSETA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' local variable(s) to hold property value(s)
Private OraDatabase As OraDatabase 'local copy
Public Property Let database(ByVal vData As OraDatabase)
   Set OraDatabase = vData
End Property

Public Sub APPLICAPROTEZIONESUPOSTI(IDOPERATORE_ As Variant, IDCAUSALEPROTEZIONE_ As Variant, IDPRODOTTO_ As Variant, LISTAIDPOSTO() As Variant, ByRef CODICEUSCITA As Variant)
  OraDatabase.Parameters.Add "IDOPERATORE_", IDOPERATORE_, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDCAUSALEPROTEZIONE_", IDCAUSALEPROTEZIONE_, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDPRODOTTO_", IDPRODOTTO_, ORAPARM_INPUT, 2
  Dim LISTAIDPOSTO_OraParamArray As OraParamArray
  OraDatabase.Parameters.AddTable "LISTAIDPOSTO", ORAPARM_INPUT, 2, UBound(LISTAIDPOSTO) - LBound(LISTAIDPOSTO), 22
  Set LISTAIDPOSTO_OraParamArray = OraDatabase.Parameters("LISTAIDPOSTO")
  Dim LISTAIDPOSTO_OraParamArray_count As Integer
  For LISTAIDPOSTO_OraParamArray_count = 0 To UBound(LISTAIDPOSTO) - LBound(LISTAIDPOSTO) - 1
    LISTAIDPOSTO_OraParamArray(LISTAIDPOSTO_OraParamArray_count) = LISTAIDPOSTO(LISTAIDPOSTO_OraParamArray_count + LBound(LISTAIDPOSTO))
  Next LISTAIDPOSTO_OraParamArray_count
  OraDatabase.Parameters.Add "CODICEUSCITA", CODICEUSCITA, ORAPARM_OUTPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.APPLICAPROTEZIONESUPOSTI(:IDOPERATORE_, :IDCAUSALEPROTEZIONE_, :IDPRODOTTO_, :LISTAIDPOSTO, :CODICEUSCITA); end;")

  CODICEUSCITA = OraDatabase.Parameters("CODICEUSCITA").Value

  OraDatabase.Parameters.Remove "IDOPERATORE_"
  OraDatabase.Parameters.Remove "IDCAUSALEPROTEZIONE_"
  OraDatabase.Parameters.Remove "IDPRODOTTO_"
  OraDatabase.Parameters.Remove "LISTAIDPOSTO"
  OraDatabase.Parameters.Remove "CODICEUSCITA"
End Sub

Public Sub CALCOLADATAORATERMINEANNULLO(idOrganizzazione As Variant, DATAORARIFERIMENTO As Date, ByRef DataOraTermineAnnullo As Date)
  OraDatabase.Parameters.Add "IDORGANIZZAZIONE", idOrganizzazione, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "DATAORARIFERIMENTO", DATAORARIFERIMENTO, ORAPARM_INPUT, 12
  OraDatabase.Parameters.Add "DATAORATERMINEANNULLO", DataOraTermineAnnullo, ORAPARM_OUTPUT, 12

  OraDatabase.ExecuteSQL ("Begin SETA.CALCOLADATAORATERMINEANNULLO(:IDORGANIZZAZIONE, :DATAORARIFERIMENTO, :DATAORATERMINEANNULLO); end;")

  DataOraTermineAnnullo = OraDatabase.Parameters("DATAORATERMINEANNULLO").Value

  OraDatabase.Parameters.Remove "IDORGANIZZAZIONE"
  OraDatabase.Parameters.Remove "DATAORARIFERIMENTO"
  OraDatabase.Parameters.Remove "DATAORATERMINEANNULLO"
End Sub

Public Sub CARICOPEROPERATORE(idOperatore As Variant, ByRef RS As OraDynaset, ByRef NUMERORECORD As Variant)
  OraDatabase.Parameters.Add "IDOPERATORE", idOperatore, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "RS", Null, ORAPARM_OUTPUT, 102
  OraDatabase.Parameters.Add "NUMERORECORD", NUMERORECORD, ORAPARM_OUTPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.CARICOPEROPERATORE(:IDOPERATORE, :RS, :NUMERORECORD); end;")

  Set RS = OraDatabase.Parameters("RS").Value
  NUMERORECORD = OraDatabase.Parameters("NUMERORECORD").Value

  OraDatabase.Parameters.Remove "IDOPERATORE"
  OraDatabase.Parameters.Remove "RS"
  OraDatabase.Parameters.Remove "NUMERORECORD"
End Sub

Public Sub CARICOPEROPERATORETIPOSUPPORTO(idOperatore As Variant, IDTIPOSUPPORTOCORRENTE As Variant, ByRef RS As OraDynaset)
  OraDatabase.Parameters.Add "IDOPERATORE", idOperatore, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDTIPOSUPPORTOCORRENTE", IDTIPOSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "RS", Null, ORAPARM_OUTPUT, 102

  OraDatabase.ExecuteSQL ("Begin SETA.CARICOPEROPERATORETIPOSUPPORTO(:IDOPERATORE, :IDTIPOSUPPORTOCORRENTE, :RS); end;")

  Set RS = OraDatabase.Parameters("RS").Value

  OraDatabase.Parameters.Remove "IDOPERATORE"
  OraDatabase.Parameters.Remove "IDTIPOSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "RS"
End Sub

Public Sub CARICOPERTIPOASSEGNAZSUPPORTO(IDTIPOASSEGNAZSUPPORTOCORRENTE As Variant, IDTIPOSUPPORTOCORRENTE As Variant, ByRef RS As OraDynaset)
  OraDatabase.Parameters.Add "IDTIPOASSEGNAZSUPPORTOCORRENTE", IDTIPOASSEGNAZSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDTIPOSUPPORTOCORRENTE", IDTIPOSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "RS", Null, ORAPARM_OUTPUT, 102

  OraDatabase.ExecuteSQL ("Begin SETA.CARICOPERTIPOASSEGNAZSUPPORTO(:IDTIPOASSEGNAZSUPPORTOCORRENTE, :IDTIPOSUPPORTOCORRENTE, :RS); end;")

  Set RS = OraDatabase.Parameters("RS").Value

  OraDatabase.Parameters.Remove "IDTIPOASSEGNAZSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "IDTIPOSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "RS"
End Sub

Public Sub CARICOPERTIPOSUPPORTO(IDTIPOSUPPORTOCORRENTE As Variant, ByRef RS As OraDynaset)
  OraDatabase.Parameters.Add "IDTIPOSUPPORTOCORRENTE", IDTIPOSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "RS", Null, ORAPARM_OUTPUT, 102

  OraDatabase.ExecuteSQL ("Begin SETA.CARICOPERTIPOSUPPORTO(:IDTIPOSUPPORTOCORRENTE, :RS); end;")

  Set RS = OraDatabase.Parameters("RS").Value

  OraDatabase.Parameters.Remove "IDTIPOSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "RS"
End Sub

Public Sub ELIMINARISERVAZSCADUTEPERORG(IDORGANIZZAZIONECORRENTE As Variant, ByRef RISERVAZIONIANNULLATE As Variant)
  OraDatabase.Parameters.Add "IDORGANIZZAZIONECORRENTE", IDORGANIZZAZIONECORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "RISERVAZIONIANNULLATE", RISERVAZIONIANNULLATE, ORAPARM_OUTPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.ELIMINARISERVAZSCADUTEPERORG(:IDORGANIZZAZIONECORRENTE, :RISERVAZIONIANNULLATE); end;")

  RISERVAZIONIANNULLATE = OraDatabase.Parameters("RISERVAZIONIANNULLATE").Value

  OraDatabase.Parameters.Remove "IDORGANIZZAZIONECORRENTE"
  OraDatabase.Parameters.Remove "RISERVAZIONIANNULLATE"
End Sub

Public Sub ESEGUIINSERIMENTOSUPPORTI(IDINSERIMENTOSUPPORTI As Variant)
  OraDatabase.Parameters.Add "IDINSERIMENTOSUPPORTI", IDINSERIMENTOSUPPORTI, ORAPARM_INPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.ESEGUIINSERIMENTOSUPPORTI(:IDINSERIMENTOSUPPORTI); end;")


  OraDatabase.Parameters.Remove "IDINSERIMENTOSUPPORTI"
End Sub

Public Sub INSERIMENTOLOTTOSUPPORTI(IDTIPOSUPPORTOCORRENTE As Variant, NUMEROTIPOGRAFICOINIZIALE As Variant, NUMEROTIPOGRAFICOFINALE As Variant)
  OraDatabase.Parameters.Add "IDTIPOSUPPORTOCORRENTE", IDTIPOSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "NUMEROTIPOGRAFICOINIZIALE", NUMEROTIPOGRAFICOINIZIALE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "NUMEROTIPOGRAFICOFINALE", NUMEROTIPOGRAFICOFINALE, ORAPARM_INPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.INSERIMENTOLOTTOSUPPORTI(:IDTIPOSUPPORTOCORRENTE, :NUMEROTIPOGRAFICOINIZIALE, :NUMEROTIPOGRAFICOFINALE); end;")


  OraDatabase.Parameters.Remove "IDTIPOSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "NUMEROTIPOGRAFICOINIZIALE"
  OraDatabase.Parameters.Remove "NUMEROTIPOGRAFICOFINALE"
End Sub

Public Sub RECUPEROREGRESSOLOTTOSUPPORTI(IDTIPOSUPPORTOCORRENTE As Variant, NUMEROTIPOGRAFICOINIZIALE As Variant, NUMEROTIPOGRAFICOFINALE As Variant, idOperatore As Variant)
  OraDatabase.Parameters.Add "IDTIPOSUPPORTOCORRENTE", IDTIPOSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "NUMEROTIPOGRAFICOINIZIALE", NUMEROTIPOGRAFICOINIZIALE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "NUMEROTIPOGRAFICOFINALE", NUMEROTIPOGRAFICOFINALE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDOPERATORE", idOperatore, ORAPARM_INPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.RECUPEROREGRESSOLOTTOSUPPORTI(:IDTIPOSUPPORTOCORRENTE, :NUMEROTIPOGRAFICOINIZIALE, :NUMEROTIPOGRAFICOFINALE, :IDOPERATORE); end;")


  OraDatabase.Parameters.Remove "IDTIPOSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "NUMEROTIPOGRAFICOINIZIALE"
  OraDatabase.Parameters.Remove "NUMEROTIPOGRAFICOFINALE"
  OraDatabase.Parameters.Remove "IDOPERATORE"
End Sub

Public Sub RIMUOVIPROTEZIONEDAPOSTI(IDOPERATORE_ As Variant, IDPRODOTTO_ As Variant, LISTAIDPOSTO() As Variant, ByRef CODICEUSCITA As Variant)
  OraDatabase.Parameters.Add "IDOPERATORE_", IDOPERATORE_, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDPRODOTTO_", IDPRODOTTO_, ORAPARM_INPUT, 2
  Dim LISTAIDPOSTO_OraParamArray As OraParamArray
  OraDatabase.Parameters.AddTable "LISTAIDPOSTO", ORAPARM_INPUT, 2, UBound(LISTAIDPOSTO) - LBound(LISTAIDPOSTO), 22
  Set LISTAIDPOSTO_OraParamArray = OraDatabase.Parameters("LISTAIDPOSTO")
  Dim LISTAIDPOSTO_OraParamArray_count As Integer
  For LISTAIDPOSTO_OraParamArray_count = 0 To UBound(LISTAIDPOSTO) - LBound(LISTAIDPOSTO) - 1
    LISTAIDPOSTO_OraParamArray(LISTAIDPOSTO_OraParamArray_count) = LISTAIDPOSTO(LISTAIDPOSTO_OraParamArray_count + LBound(LISTAIDPOSTO))
  Next LISTAIDPOSTO_OraParamArray_count
  OraDatabase.Parameters.Add "CODICEUSCITA", CODICEUSCITA, ORAPARM_OUTPUT, 2

  OraDatabase.ExecuteSQL ("Begin SETA.RIMUOVIPROTEZIONEDAPOSTI(:IDOPERATORE_, :IDPRODOTTO_, :LISTAIDPOSTO, :CODICEUSCITA); end;")

  CODICEUSCITA = OraDatabase.Parameters("CODICEUSCITA").Value

  OraDatabase.Parameters.Remove "IDOPERATORE_"
  OraDatabase.Parameters.Remove "IDPRODOTTO_"
  OraDatabase.Parameters.Remove "LISTAIDPOSTO"
  OraDatabase.Parameters.Remove "CODICEUSCITA"
End Sub

Public Sub RITIRANDIPERTIPOSUPPORTO(IDTIPOSUPPORTOCORRENTE As Variant, IDOPERATOREDESTINAZIONE As Variant, ByRef RS As OraDynaset)
  OraDatabase.Parameters.Add "IDTIPOSUPPORTOCORRENTE", IDTIPOSUPPORTOCORRENTE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "IDOPERATOREDESTINAZIONE", IDOPERATOREDESTINAZIONE, ORAPARM_INPUT, 2
  OraDatabase.Parameters.Add "RS", Null, ORAPARM_OUTPUT, 102

  OraDatabase.ExecuteSQL ("Begin SETA.RITIRANDIPERTIPOSUPPORTO(:IDTIPOSUPPORTOCORRENTE, :IDOPERATOREDESTINAZIONE, :RS); end;")

  Set RS = OraDatabase.Parameters("RS").Value

  OraDatabase.Parameters.Remove "IDTIPOSUPPORTOCORRENTE"
  OraDatabase.Parameters.Remove "IDOPERATOREDESTINAZIONE"
  OraDatabase.Parameters.Remove "RS"
End Sub

