VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmInterrogazione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Interrogazione"
   ClientHeight    =   8040
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14700
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8040
   ScaleWidth      =   14700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSAdodcLib.Adodc adcRiassunto 
      Height          =   330
      Left            =   6780
      Top             =   2640
      Visible         =   0   'False
      Width           =   2595
      _ExtentX        =   4577
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adcRiassunto"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrRiassunto 
      Bindings        =   "frmInterrogazione.frx":0000
      Height          =   1335
      Left            =   120
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1800
      Width           =   14415
      _ExtentX        =   25426
      _ExtentY        =   2355
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   375
      Left            =   12960
      TabIndex        =   3
      Top             =   7560
      Width           =   1575
   End
   Begin VB.ComboBox cmbTipoSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1020
      Width           =   4875
   End
   Begin VB.ComboBox cmbOperatore 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   180
      Width           =   4875
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   600
      Width           =   4875
   End
   Begin MSAdodcLib.Adodc adcInterrogazione 
      Height          =   330
      Left            =   6780
      Top             =   7020
      Visible         =   0   'False
      Width           =   2595
      _ExtentX        =   4577
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adcInterrogazione"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrInterrogazione 
      Bindings        =   "frmInterrogazione.frx":001B
      Height          =   3915
      Left            =   120
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   3540
      Width           =   14415
      _ExtentX        =   25426
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Line Line4 
      X1              =   300
      X2              =   300
      Y1              =   3360
      Y2              =   3540
   End
   Begin VB.Line Line3 
      X1              =   660
      X2              =   300
      Y1              =   3360
      Y2              =   3360
   End
   Begin VB.Label lblDettaglio 
      Caption         =   "Dettaglio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   720
      TabIndex        =   10
      Top             =   3300
      Width           =   2355
   End
   Begin VB.Line Line2 
      X1              =   300
      X2              =   300
      Y1              =   1620
      Y2              =   1800
   End
   Begin VB.Line Line1 
      X1              =   660
      X2              =   300
      Y1              =   1620
      Y2              =   1620
   End
   Begin VB.Label lblRiassunto 
      Caption         =   "Riassunto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   720
      TabIndex        =   9
      Top             =   1560
      Width           =   2355
   End
   Begin VB.Label lblTipoSupporto 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo supporto"
      Height          =   195
      Left            =   180
      TabIndex        =   8
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label lblOperatore 
      Alignment       =   1  'Right Justify
      Caption         =   "Operatore"
      Height          =   195
      Left            =   180
      TabIndex        =   7
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label lblOrganizzazione 
      Alignment       =   1  'Right Justify
      Caption         =   "Organizzazione"
      Height          =   195
      Left            =   180
      TabIndex        =   6
      Top             =   660
      Width           =   1575
   End
End
Attribute VB_Name = "frmInterrogazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrganizzazione As Long
Private idOperatore As Long
Private idTipoSupporto As Long
Private R As ADODB.Recordset
Private internalEvent As Boolean
Private operatoreAbilitato As Boolean
Private tipoInterrogazione As EnumeratoTipoInterrogazione

Public Sub Init(tipoInt As Long)
    Dim sql As String
    
    tipoInterrogazione = tipoInt
'    sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE" & _
'        " ORDER BY NOME"
'    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME", , , False)
    Select Case tipoInterrogazione
        Case TI_MAGAZZINO_LIS
            Call CaricaListaOrganizzazioni
        Case TI_PER_OPERATORE
            Call CaricaListaOperatori
    End Select
    Call Variabili_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    Select Case tipoInterrogazione
        Case TI_MAGAZZINO_LIS
            frmInterrogazione.Caption = "Interrogazione Magazzino LIS"
        Case TI_PER_OPERATORE
            frmInterrogazione.Caption = "Interrogazione per Operatore"
    End Select
'    cmbOperatore.Enabled = (idOrganizzazione <> idNessunElementoSelezionato)
'    lblOperatore.Enabled = (idOrganizzazione <> idNessunElementoSelezionato)
'    cmbOperatore.Visible = (tipoInterrogazione = TI_PER_OPERATORE)
'    lblOperatore.Visible = (tipoInterrogazione = TI_PER_OPERATORE)
'    cmbTipoSupporto.Enabled = (idOrganizzazione <> idNessunElementoSelezionato)
'    lblTipoSupporto.Enabled = (idOrganizzazione <> idNessunElementoSelezionato)
    cmbOperatore.Visible = (tipoInterrogazione = TI_PER_OPERATORE)
    lblOperatore.Visible = (tipoInterrogazione = TI_PER_OPERATORE)
    cmbOrganizzazione.Enabled = (idOperatore <> idNessunElementoSelezionato) Or (tipoInterrogazione = TI_MAGAZZINO_LIS)
    lblOrganizzazione.Enabled = (idOperatore <> idNessunElementoSelezionato) Or (tipoInterrogazione = TI_MAGAZZINO_LIS)
    cmbTipoSupporto.Enabled = (idOrganizzazione <> idNessunElementoSelezionato)
    lblTipoSupporto.Enabled = (idOrganizzazione <> idNessunElementoSelezionato)
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmbOrganizzazione_Click()
    idOrganizzazione = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    idTipoSupporto = idNessunElementoSelezionato
    Call CaricaListaTipiSupporto
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbOperatore_Click()
    idOperatore = cmbOperatore.ItemData(cmbOperatore.ListIndex)
    Call CaricaListaOrganizzazioni
    AggiornaAbilitazioneControlli
'    Call adcRiassunto_Init
'    Call dgrRiassunto_Init
'    Call adcInterrogazione_Init
'    Call dgrInterrogazione_Init
End Sub

Private Sub cmbTipoSupporto_Click()
    idTipoSupporto = cmbTipoSupporto.ItemData(cmbTipoSupporto.ListIndex)
    Call AggiornaAbilitazioneControlli
'    If tipoInterrogazione = TI_MAGAZZINO_LIS Then
'        Call adcRiassunto_Init
'        Call dgrRiassunto_Init
'        Call adcInterrogazione_Init
'        Call dgrInterrogazione_Init
'    End If
    Call adcRiassunto_Init
    Call dgrRiassunto_Init
    Call adcInterrogazione_Init
    Call dgrInterrogazione_Init
End Sub

Private Sub cmdEsci_Click()
    Call Esci
End Sub

Private Sub Variabili_Init()
    idOrganizzazione = idNessunElementoSelezionato
    idOperatore = idNessunElementoSelezionato
    idTipoSupporto = idNessunElementoSelezionato
End Sub

Private Sub CaricaListaOperatori()
    Dim sql As String
    
    Call cmbOperatore.Clear
    sql = "SELECT O.IDOPERATORE ID, O.USERNAME || '(' || DECODE(ABILITATO, 0 , 'Non abilitato)', 1, 'Abilitato)') DESCR" & _
        " FROM OPERATORE O, OPERATORE_ORGANIZZAZIONE OO" & _
        " WHERE O.IDOPERATORE = OO.IDOPERATORE" & _
        " AND OO.IDORGANIZZAZIONE = " & idOrganizzazione & _
        " ORDER BY DESCR"
    sql = "SELECT O.IDOPERATORE ID, O.USERNAME || '(' || DECODE(ABILITATO, 0 , 'Non abilitato)', 1, 'Abilitato)') DESCR" & _
        " FROM OPERATORE O" & _
        " ORDER BY DESCR"
    Call CaricaValoriCombo(cmbOperatore, sql, "DESCR", , , False)
End Sub

Private Sub CaricaListaOrganizzazioni()
    Dim sql As String
    
    Call cmbOrganizzazione.Clear
    Select Case tipoInterrogazione
        Case TI_MAGAZZINO_LIS
            sql = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE" & _
                " ORDER BY NOME"
        Case TI_PER_OPERATORE
            sql = "SELECT O.IDORGANIZZAZIONE ID, NOME " & _
                " FROM ORGANIZZAZIONE O, OPERATORE_ORGANIZZAZIONE OO" & _
                " WHERE O.IDORGANIZZAZIONE = OO.IDORGANIZZAZIONE" & _
                " AND OO.IDOPERATORE = " & idOperatore & _
                " ORDER BY NOME"
    End Select
    Call CaricaValoriCombo(cmbOrganizzazione, sql, "NOME", , , True)
End Sub

Private Sub CaricaListaTipiSupporto()
    Dim sql As String
    
    Call cmbTipoSupporto.Clear
    If idOrganizzazione = idTuttiGliElementiSelezionati Then
        sql = "SELECT DISTINCT T.IDTIPOSUPPORTO ID, T.NOME ||' - '|| T.CODICE LABEL" & _
            " FROM TIPOSUPPORTO T, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE T.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
            " ORDER BY LABEL"
    Else
        sql = "SELECT DISTINCT T.IDTIPOSUPPORTO ID, T.NOME ||' - '|| T.CODICE LABEL" & _
            " FROM TIPOSUPPORTO T, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE T.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
            " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazione & _
            " ORDER BY LABEL"
    End If
    Call CaricaValoriCombo(cmbTipoSupporto, sql, "LABEL", , , True)
End Sub

Private Sub adcRiassunto_Init()
    Dim internalEventOld As Boolean
    Dim f As Fields
    internalEventOld = internalEvent
    internalEvent = True
            
        Set R = New ADODB.Recordset
        Set f = R.Fields

        Call f.Append("IdTipoSupporto", adInteger, 8)
        Call f.Append("Tipo_supporto", adVarChar, 30)
        Call f.Append("Totale_assegnati", adInteger, 8)
        Call f.Append("Totale_disp.", adInteger, 8)
        Call f.Append("Percentuale_Consumo", adDouble, 4)
        Call f.Append("Disp_abilitati", adInteger, 8)
        Call f.Append("Disp_da_abilitare", adInteger, 8)
        Call f.Append("Emessi", adInteger, 8)
        Call f.Append("Disp_disabilitati", adInteger, 8)
        Call f.Append("Annullati", adInteger, 8)
        Call f.Append("Distrutti", adInteger, 8)
        Call f.Append("Smarriti", adInteger, 8)
        Call f.Append("Danneggiati", adInteger, 8)
'        Call f.Append("Abilitato", adVarChar, 2)
        Call R.Open
        Set dgrRiassunto.DataSource = R
        Call dgrRiassunto_Init
        Call AggiungiRecordsetTipiSupporto
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrInterrogazione_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    
    Set g = dgrInterrogazione
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    g.Columns(0).Width = (dimensioneGrid / 5)
    g.Columns(1).Width = (dimensioneGrid / 5)
    g.Columns(2).Width = (dimensioneGrid / 5)
    g.Columns(3).Width = (dimensioneGrid / 5)
    g.Columns(4).Width = (dimensioneGrid / 5)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub dgrRiassunto_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    
    Set g = dgrRiassunto
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 200
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / 13) * 2
    g.Columns(2).Width = (dimensioneGrid / 13)
    g.Columns(3).Width = (dimensioneGrid / 13)
    g.Columns(4).Width = (dimensioneGrid / 13)
    g.Columns(5).Width = (dimensioneGrid / 13)
    g.Columns(6).Width = (dimensioneGrid / 13)
    g.Columns(7).Width = (dimensioneGrid / 13)
    g.Columns(8).Width = (dimensioneGrid / 13)
    g.Columns(9).Width = (dimensioneGrid / 13)
    g.Columns(10).Width = (dimensioneGrid / 13)
    g.Columns(11).Width = (dimensioneGrid / 13)
    g.Columns(12).Width = (dimensioneGrid / 13)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub adcInterrogazione_Init()
    Dim cmd As New ADODB.Command
    Dim rec As ADODB.Recordset
    Dim param_IdOperatore As ADODB.Parameter
    Dim param_IdTipoSupporto As ADODB.Parameter
    Dim param_IdOrganizzazione As ADODB.Parameter
    
    Dim recOut As ADODB.Recordset
    
    cmd.ActiveConnection = setaConnection
'    cmd.CommandText = "INTERROGAZIONEOPERATORE"
'    cmd.CommandText = "PROVA"

    Select Case tipoInterrogazione
        Case TI_PER_OPERATORE
            If idTipoSupporto = idTuttiGliElementiSelezionati Then
                cmd.CommandText = "STATOCARICHI.PER_OPERATORE"
                cmd.CommandType = adCmdStoredProc
                
                setaConnection.CursorLocation = adUseClient
                    
                Set param_IdOperatore = cmd.CreateParameter(, adInteger, adParamInput, 4, idOperatore)
                cmd.Parameters.Append param_IdOperatore
            Else
                cmd.CommandText = "STATOCARICHI.PER_OPERATORE_TIPOSUPPORTO"
                cmd.CommandType = adCmdStoredProc
                
                setaConnection.CursorLocation = adUseClient
                    
                Set param_IdOperatore = cmd.CreateParameter(, adInteger, adParamInput, 4, idOperatore)
                cmd.Parameters.Append param_IdOperatore
                Set param_IdTipoSupporto = cmd.CreateParameter(, adInteger, adParamInput, 4, idTipoSupporto)
                cmd.Parameters.Append param_IdTipoSupporto
            End If
        Case TI_MAGAZZINO_LIS
            If idTipoSupporto = idTuttiGliElementiSelezionati Then
                cmd.CommandText = "STATOCARICHI.PER_MAGAZZINO"
                cmd.CommandType = adCmdStoredProc
                
                setaConnection.CursorLocation = adUseClient
            Else
                cmd.CommandText = "STATOCARICHI.PER_MAGAZZINO_TIPOSUPPORTO"
                cmd.CommandType = adCmdStoredProc
                
                setaConnection.CursorLocation = adUseClient
                    
                Set param_IdTipoSupporto = cmd.CreateParameter(, adInteger, adParamInput, 4, idTipoSupporto)
                cmd.Parameters.Append param_IdTipoSupporto
            End If
    End Select
    
    Set param_IdOrganizzazione = cmd.CreateParameter(, adInteger, adParamInput, 4, idOrganizzazione)
    cmd.Parameters.Append param_IdOrganizzazione
    
    Set rec = cmd.Execute
    Set adcInterrogazione.Recordset = rec
End Sub

Private Sub AggiungiRecordsetTipiSupporto()
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim totaleAssegnati As Long
    Dim totaleDisponibili As Long
    Dim disponibiliAbilitati As Long
    Dim disponibiliDaAbilitare As Long
    Dim disponibiliDisabilitati As Long
    Dim emessi As Long
    Dim annullati As Long
    Dim distrutti As Long
    Dim smarriti As Long
    Dim danneggiati As Long
    Dim idTipoStatoSupporto As Long
    Dim idOperatoreCorrente As Long
    Dim idTipoSupportoCorrente As Long
    Dim percentualeConsumo As Double
'    Dim abilitato As Boolean
    Dim condizioneSQL As String
    
    If idTipoSupporto <> idTuttiGliElementiSelezionati Then
        condizioneSQL = " AND S.IDTIPOSUPPORTO = " & idTipoSupporto
    End If
    Select Case tipoInterrogazione
        Case TI_PER_OPERATORE
            sql = " SELECT TS.IDTIPOSUPPORTO IDTIPO, TS.NOME NOMETIPOSUPPORTO," & _
                " TSS.IDTIPOSTATOSUPPORTO IDTIPOSTATO, TSS.NOME STATOSUPPORTO," & _
                " COUNT(DISTINCT IDSUPPORTO) COUNTT1" & _
                " FROM SUPPORTO S, TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL," & _
                " TIPOSTATOSUPPORTO TSS" & _
                " WHERE OTL.IDTIPOSUPPORTO = S.IDTIPOSUPPORTO" & _
                " AND S.IDTIPOSTATOSUPPORTO = TSS.IDTIPOSTATOSUPPORTO" & _
                " AND S.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
                " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazione & _
                condizioneSQL & _
                " AND S.IDOPERATORECORRENTE = " & idOperatore & _
                " AND IDTIPOSTATOASSEGNAZSUPPORTO = " & TSAS_ESEGUITA & _
                " GROUP BY TS.NOME, TSS.IDTIPOSTATOSUPPORTO, TSS.NOME, TS.IDTIPOSUPPORTO" & _
                " ORDER BY TS.NOME"
        Case TI_MAGAZZINO_LIS
            sql = " SELECT TS.IDTIPOSUPPORTO IDTIPO, TS.NOME NOMETIPOSUPPORTO," & _
                " TSS.IDTIPOSTATOSUPPORTO IDTIPOSTATO, TSS.NOME STATOSUPPORTO," & _
                " COUNT(DISTINCT IDSUPPORTO) COUNTT1" & _
                " FROM SUPPORTO S, TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL," & _
                " TIPOSTATOSUPPORTO TSS" & _
                " WHERE OTL.IDTIPOSUPPORTO = S.IDTIPOSUPPORTO" & _
                " AND S.IDTIPOSTATOSUPPORTO = TSS.IDTIPOSTATOSUPPORTO" & _
                " AND S.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
                " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazione & _
                condizioneSQL & _
                " AND S.IDOPERATORECORRENTE IS NULL " & _
                " GROUP BY TS.NOME, TSS.IDTIPOSTATOSUPPORTO, TSS.NOME, TS.IDTIPOSUPPORTO" & _
                " ORDER BY TS.NOME"
    End Select
    rec.Open sql, setaConnection, adOpenDynamic, adLockOptimistic
    idTipoSupportoCorrente = idNessunElementoSelezionato
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            If rec("IDTIPO") <> idTipoSupportoCorrente Then
                totaleAssegnati = 0
                totaleDisponibili = 0
                disponibiliAbilitati = 0
                disponibiliDaAbilitare = 0
                disponibiliDisabilitati = 0
                emessi = 0
                annullati = 0
                distrutti = 0
                percentualeConsumo = 0
                smarriti = 0
                danneggiati = 0
                R.AddNew
'                Select Case tipoInterrogazione
'                    Case TI_PER_OPERATORE
                        R("IdTipoSupporto") = rec("IDTIPO")
                        R("Tipo_supporto") = rec("NOMETIPOSUPPORTO")
'                    Case TI_MAGAZZINO_LIS
'                        R("idOperatore") = idNessunElementoSelezionato
'                        R("UserName") = "Magazzino LIS"
'                End Select
'                R("Abilitato") = IIf(rec("ABILITATO") = 1, "SI", "NO")
                R("Disp_abilitati") = 0
                R("Disp_da_abilitare") = 0
                R("Disp_disabilitati") = disponibiliDisabilitati
                R("Emessi") = 0
                R("Annullati") = 0
                R("Smarriti") = 0
                R("Danneggiati") = 0
                R("Distrutti") = 0
                R.Update
                idTipoSupportoCorrente = rec("IDTIPO")
            End If
            idTipoStatoSupporto = rec("IDTIPOSTATO")
            Select Case idTipoStatoSupporto
                Case TSS_DISPONIBILE_DA_ABILITARE
                    disponibiliDaAbilitare = rec("COUNTT1").Value
                    R("Disp_da_abilitare") = disponibiliDaAbilitare
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_DISPONIBILE_ABILITATO
                    disponibiliAbilitati = rec("COUNTT1").Value
                    R("Disp_abilitati") = disponibiliAbilitati
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_DISPONIBILE_DISABILITATO
                    disponibiliDisabilitati = rec("COUNTT1").Value
                    R("Disp_disabilitati") = disponibiliDisabilitati
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_EMESSO
                    emessi = rec("COUNTT1").Value
                    R("Emessi") = emessi
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_ANNULLATO
                    annullati = rec("COUNTT1").Value
                    R("Annullati") = annullati
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_SMARRITO
                    smarriti = rec("COUNTT1").Value
                    R("Smarriti") = smarriti
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_DANNEGGIATO
                    danneggiati = rec("COUNTT1").Value
                    R("Danneggiati") = danneggiati
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case TSS_DISTRUTTO
                    distrutti = rec("COUNTT1").Value
                    R("Distrutti") = distrutti
                    totaleAssegnati = disponibiliAbilitati + disponibiliDaAbilitare + emessi + annullati + distrutti + disponibiliDisabilitati + smarriti + danneggiati
                    totaleDisponibili = disponibiliAbilitati + disponibiliDaAbilitare
                    R("Totale_assegnati") = totaleAssegnati
                    R("Totale_disp.") = totaleDisponibili
                    If totaleAssegnati > 0 Then
                        percentualeConsumo = (totaleAssegnati - totaleDisponibili) / totaleAssegnati * 100
                        R("Percentuale_Consumo") = Round(percentualeConsumo, 2)
                    Else
                        percentualeConsumo = 0
                        R("Percentuale_Consumo") = 0
                    End If
                    R.Update
                Case Else
            End Select
            rec.MoveNext
        Wend
    End If
    rec.Close
End Sub

Private Function IsOperatoreAbilitato()
    Dim rec As New ADODB.Recordset
    Dim sql As String
    Dim counter As Long
    
    counter = 0
    sql = "SELECT COUNT(IDOPERATORE) CONTATORE FROM OPERATORE" & _
        " WHERE IDOPERATORE = " & idOperatore & _
        " AND ABILITATO = 1" '1 = TRUE
    rec.Open sql, setaConnection, adOpenDynamic, adLockOptimistic
        counter = rec("CONTATORE")
    rec.Close
    IsOperatoreAbilitato = (counter = 1)
End Function

