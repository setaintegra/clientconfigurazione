VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmDettagliIntervalloOperazioni 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Intervallo operazioni"
   ClientHeight    =   1890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1890
   ScaleWidth      =   6060
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtMinutiFine 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3780
      MaxLength       =   2
      TabIndex        =   6
      Top             =   600
      Width           =   435
   End
   Begin VB.TextBox txtOraFine 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3180
      MaxLength       =   2
      TabIndex        =   5
      Top             =   600
      Width           =   435
   End
   Begin VB.TextBox txtMinutiInizio 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3780
      MaxLength       =   2
      TabIndex        =   3
      Top             =   120
      Width           =   435
   End
   Begin VB.TextBox txtOraInizio 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3180
      MaxLength       =   2
      TabIndex        =   2
      Top             =   120
      Width           =   435
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   3240
      TabIndex        =   7
      Top             =   1440
      Width           =   1035
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   1560
      TabIndex        =   0
      Top             =   1440
      Width           =   1035
   End
   Begin MSComCtl2.DTPicker dtpDataInizio 
      Height          =   315
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   68681729
      CurrentDate     =   37607
   End
   Begin MSComCtl2.DTPicker dtpDataFine 
      Height          =   315
      Left            =   1080
      TabIndex        =   4
      Top             =   600
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   68681729
      CurrentDate     =   37607
   End
   Begin VB.Label lblSeparatoreOreMinutiFine 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3660
      TabIndex        =   12
      Top             =   600
      Width           =   75
   End
   Begin VB.Label lblSeparatoreOreMinutiInizio 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3660
      TabIndex        =   11
      Top             =   120
      Width           =   75
   End
   Begin VB.Label lblNota 
      Caption         =   "NOTA: non valorizzando i controlli si sceglierą una durata illimitata"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   1080
      Width           =   5775
   End
   Begin VB.Label lblInizio 
      Alignment       =   1  'Right Justify
      Caption         =   "Inizio:"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   180
      Width           =   735
   End
   Begin VB.Label lblFine 
      Alignment       =   1  'Right Justify
      Caption         =   "Fine:"
      Height          =   255
      Left            =   300
      TabIndex        =   8
      Top             =   660
      Width           =   675
   End
End
Attribute VB_Name = "frmDettagliIntervalloOperazioni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private dataOraInizio As Date
Private dataOraFine As Date

Private internalEvent As Boolean
Private exitCode As ExitCodeEnum

Public Sub Init()
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    txtOraInizio.Enabled = Not IsNull(dtpDataInizio.Value)
    txtMinutiInizio.Enabled = Not IsNull(dtpDataInizio.Value)
    lblSeparatoreOreMinutiInizio.Enabled = Not IsNull(dtpDataInizio.Value)
    txtOraFine.Enabled = Not IsNull(dtpDataFine.Value)
    txtMinutiFine.Enabled = Not IsNull(dtpDataFine.Value)
    lblSeparatoreOreMinutiFine.Enabled = Not IsNull(dtpDataFine.Value)
'    cmdConferma.Enabled = (dataOraInizio <> dataNulla And dataOraFine <> dataNulla) Or _
'        (dataOraInizio = dataNulla And dataOraFine = dataNulla)
    cmdConferma.Enabled = _
        (IsNull(dtpDataInizio.Value) And IsNull(dtpDataFine.Value)) Or _
        (Not IsNull(dtpDataInizio.Value) And Not IsNull(dtpDataFine.Value))
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim dataInizio As Date
    Dim oraInizio As Integer
    Dim minutiInizio As Integer
    Dim dataFine As Date
    Dim oraFine As Integer
    Dim minutiFine As Integer
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    
    Set listaNonConformitą = New Collection
    
    If IsNull(dtpDataInizio.Value) Then
        dataOraInizio = dataNulla
    Else
        dataInizio = FormatDateTime(dtpDataInizio.Value, vbShortDate)
        If txtOraInizio.Text <> "" Then
            If IsCampoOraCorretto(txtOraInizio) Then
                oraInizio = CInt(Trim(txtOraInizio.Text))
            Else
'                Call frmMessaggio.Visualizza("ErroreFormatoOra", "Ora inizio")
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Ora inizio deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiInizio.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiInizio) Then
                minutiInizio = CInt(Trim(txtMinutiInizio.Text))
            Else
'                Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Minuti inizio")
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Minuti inizio deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraInizio = FormatDateTime(dataInizio & " " & oraInizio & ":" & minutiInizio, vbGeneralDate)
    End If
    
    If IsNull(dtpDataFine.Value) Then
        dataOraFine = dataNulla
    Else
        dataFine = FormatDateTime(dtpDataFine.Value, vbShortDate)
        If txtOraFine.Text <> "" Then
            If IsCampoOraCorretto(txtOraFine) Then
                oraFine = CInt(Trim(txtOraFine.Text))
            Else
'                Call frmMessaggio.Visualizza("ErroreFormatoOra", "Ora fine")
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Ora fine deve essere numerico di tipo intero e compreso tra 0 e 23;")
            End If
        End If
        If txtMinutiFine.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiFine) Then
                minutiFine = CInt(Trim(txtMinutiFine.Text))
            Else
'                Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Minuti fine")
                ValoriCampiOK = False
                Call listaNonConformitą.Add("- il valore immesso sul campo Minuti fine deve essere numerico di tipo intero e compreso tra 0 e 59;")
            End If
        End If
        dataOraFine = FormatDateTime(dataFine & " " & oraFine & ":" & minutiFine, vbGeneralDate)
    End If
    
    If (dataOraInizio <> dataNulla) And (dataOraFine <> dataNulla) And (dataOraFine <= dataOraInizio) Then
'        Call frmMessaggio.Visualizza("ErroreInversioneDate", "Inizio", "Fine")
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- la data Inizio deve essere precedente alla data Fine;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
'    Call frmInizialePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_ANNULLA)
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    If ValoriCampiOK Then
        exitCode = EC_CONFERMA
'        Select Case operazioneSuPianta
'            Case A_NUOVO
'                Call frmInizialePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_CONFERMA)
'                Call frmInizialePianta.SetCodicePiantaOrganizzazioneSelezionata(codicePianta)
'            Case A_CLONA
'                Call frmClonazionePianta.SetExitCodeFormDettagliPiantaOrganizzazione(EC_DP_CONFERMA)
'                Call frmClonazionePianta.SetCodicePiantaOrganizzazioneSelezionata(codicePianta)
'            Case Else
'        End Select
        
        Unload Me
    End If
    
End Sub

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Function GetDataOraInizio() As Date
    GetDataOraInizio = dataOraInizio
End Function

Public Function GetDataOraFine() As Date
    GetDataOraFine = dataOraFine
End Function

'Public Sub SetIdPiantaSelezionata(idP As Long)
'    idPiantaSelezionata = idP
'End Sub
'
'Public Sub SetNomeOrganizzazioneSelezionata(nomeO As String)
'    nomeOrganizzazioneSelezionata = nomeO
'End Sub
'
'Public Sub SetNomePianta(nomeP As String)
'    nomePianta = nomeP
'End Sub
'
'Public Sub SetOperazioneSuPianta(operaz As AzioneEnum)
'    operazioneSuPianta = operaz
'End Sub

Private Sub txtOraInizio_Change()
    If Not internalEvent Then
        If Len(txtOraInizio.Text) = 2 Then
            txtMinutiInizio.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiInizio_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtOraFine_Change()
    If Not internalEvent Then
        If Len(txtOraFine.Text) = 2 Then
            txtMinutiFine.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiFine_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub dtpDataInizio_Init()
    dtpDataInizio.Value = Null
    dtpDataInizio.MinDate = dataNulla
End Sub

Private Sub dtpDataInizio_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dtpDataFine_Change()
    Call AggiornaAbilitazioneControlli
End Sub


