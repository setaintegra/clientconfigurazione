VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstSuperaree 
      Height          =   5685
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   15
      Top             =   1680
      Width           =   5415
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   9
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   8
      Top             =   240
      Width           =   1635
   End
   Begin VB.PictureBox adcConfigurazioneProdottoStampeAgiuntive 
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   6480
      ScaleHeight     =   270
      ScaleWidth      =   1575
      TabIndex        =   12
      Top             =   180
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   6
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   5
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblLista 
      Caption         =   "Selezionare le superaree per le quali NON č consentito il cambio utilizzatore:"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1320
      Width           =   6855
   End
   Begin VB.Label Label1 
      Caption         =   "Configurazione consentita solo per prodotti che rientrano nel decreto sicurezza"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   720
      Width           =   6855
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   11
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   10
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione cambio utilizzatore su superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoCambioUtilizzatoreSuSuperaree"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private isRecordEditabile As Boolean
Private valoreRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private coordinataXRecordSelezionato As Integer
Private coordinataYRecordSelezionato As Integer
Private progressivoRecordSelezionato As Integer
Private nomeOrganizzazioneSelezionata As String
Private rateo As Long
Private isProdottoAttivoSuTL As Boolean
Private idClasseProdottoSelezionata As ClasseProdottoEnum

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    If (rientraInDecretoSicurezza) Then
        lblLista.Enabled = True
        lstSuperaree.Enabled = True
        cmdConferma.Enabled = True
        cmdAnnulla.Enabled = True
    Else
        lblLista.Enabled = False
        lstSuperaree.Enabled = False
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneProtezioni
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call AggiornaAbilitazioneControlli
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    Dim risposta As VbMsgBoxResult
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If (superareeServizioNonSelezionate()) Then
        risposta = MsgBox("Le superaree di servizio non sono selezionate: si vuole comunque procedere?", vbYesNo)
            If risposta = vbYes Then
                Call Conferma
            End If
    Else
        Call Conferma
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
    
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            AggiornaBaseDati
        End If
        
        Unload Me
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call lstSuperaree_Init
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub lstSuperaree_Init()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    Call ApriConnessioneBD

    sql = "SELECT A.IDAREA, A.CODICE || ' - ' || A.NOME N, DECODE (S.IDSUPERAREA, NULL, 0, 1) SEL, A.IDTIPOAREA IDTIPOAREA" & _
            " FROM PRODOTTO P, AREA A, SUPERAREANOCAMBIOUTILIZZATORE S" & _
            " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND P.IDPIANTA = A.IDPIANTA" & _
            " AND A.IDTIPOAREA IN (" & TA_SUPERAREA_NUMERATA & ", " & TA_SUPERAREA_SERVIZIO & ")" & _
            " AND A.IDAREA = S.IDSUPERAREA(+)" & _
            " AND S.IDPRODOTTO(+) = " & idProdottoSelezionato & _
            " ORDER BY A.CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            Call lstSuperaree.AddItem(rec("N"))
            lstSuperaree.ItemData(i - 1) = rec("IDAREA")
            If rec("SEL") = 1 Then
                lstSuperaree.Selected(i - 1) = True
            End If
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD

End Sub

Private Sub AggiornaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    
    sql = "DELETE FROM SUPERAREANOCAMBIOUTILIZZATORE" & _
        " WHERE IDPRODOTTO =" & idProdottoSelezionato & _
        " AND IDSUPERAREA IN (" & _
        " SELECT IDAREA" & _
        " FROM AREA A, PRODOTTO P" & _
        " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND P.IDPIANTA = A.IDPIANTA" & _
        ")"
    SETAConnection.Execute sql, n, adCmdText
    
    For i = 1 To lstSuperaree.ListCount
        If lstSuperaree.Selected(i - 1) Then
            sql = "INSERT INTO SUPERAREANOCAMBIOUTILIZZATORE (IDPRODOTTO, IDSUPERAREA)" & _
                " VALUES (" & idProdottoSelezionato & ", " & lstSuperaree.ItemData(i - 1) & ")"
            SETAConnection.Execute sql, n, adCmdText
        End If
    Next i
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
 
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
'    sql = "UPDATE STAMPAAGGIUNTIVA SET" & _
'        " VALORE = " & SqlStringValue(valoreRecordSelezionato) & "," & _
'        " COORDINATAX = " & coordinataXRecordSelezionato & "," & _
'        " COORDINATAY = " & coordinataYRecordSelezionato & "," & _
'        " IDFORMATOSTAMPAAGGIUNTIVA = " & idFormatoStampaAggiuntiva & "," & _
'        " PROGRESSIVO = " & progressivoRecordSelezionato & _
'        " WHERE IDSTAMPAAGGIUNTIVA = " & idRecordSelezionato
'    SETAConnection.Execute sql, n, adCmdText
''    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
''            Call AggiornaParametriSessioneSuRecord("STAMPAAGGIUNTIVA", "IDSTAMPAAGGIUNTIVA", idRecordSelezionato, TSR_MODIFICATO)
''        End If
''    End If
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoChiaveProdotto.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoChiaveProdotto.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdStagioneSelezionata(idS As Long)
    idStagioneSelezionata = idS
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Private Sub CaricaFormConfigurazioneProtezioni()
    Call frmConfigurazioneProdottoProtezioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoProtezioni.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoProtezioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoProtezioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoProtezioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoProtezioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoProtezioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoProtezioni.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoProtezioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoProtezioni.Init
End Sub

Private Function superareeServizioNonSelezionate() As Boolean
    Dim i As Long
    
    superareeServizioNonSelezionate = False
    For i = 1 To lstSuperaree.ListCount
        lstSuperaree.ListIndex = i - 1
        If Left(lstSuperaree.Text, 3) = "SER" Or Left(lstSuperaree.Text, 2) = "90" Then
            If lstSuperaree.Selected(i - 1) = False Then
                superareeServizioNonSelezionate = True
            End If
        End If
    Next i
End Function

