VERSION 5.00
Begin VB.Form frmDettagliOrdineDiPostoSIAE 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ordine di posto SIAE"
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   900
      TabIndex        =   2
      Top             =   840
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2580
      TabIndex        =   1
      Top             =   840
      Width           =   1035
   End
   Begin VB.ComboBox cmbOrdineDiPostoSIAE 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   300
      Width           =   4275
   End
End
Attribute VB_Name = "frmDettagliOrdineDiPostoSIAE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrdinediPostoSIAESelezionato As Long
Private nomeOrdineDiPostoSiaeSelezionato As String
Private codiceOrdinediPostoSIAESelezionato As String
Private labelOrdineDiPostoSiaeSelezionato As String

Private exitCode As ExitCodeEnum

Public Sub Init()
    Dim sql As String

    Call cmbOrdineDiPostoSIAE.Clear
    idOrdinediPostoSIAESelezionato = idNessunElementoSelezionato
    sql = "SELECT IDORDINEDIPOSTOSIAE ID, CODICE || ' - ' || NOME LABEL" & _
        " FROM ORDINEDIPOSTOSIAE WHERE IDORDINEDIPOSTOSIAE > 0" & _
        " ORDER BY LABEL"
    Call CaricaValoriCombo(cmbOrdineDiPostoSIAE, sql, "LABEL")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = cmbOrdineDiPostoSIAE.ListIndex <> idNessunElementoSelezionato
End Sub

Private Sub cmbOrdineDiPostoSIAE_Click()
    Call cmbOrdineDiPostoSIAE_Update
End Sub

Private Sub cmbOrdineDiPostoSIAE_Update()
    idOrdinediPostoSIAESelezionato = cmbOrdineDiPostoSIAE.ItemData(cmbOrdineDiPostoSIAE.ListIndex)
    labelOrdineDiPostoSiaeSelezionato = cmbOrdineDiPostoSIAE.Text
    codiceOrdinediPostoSIAESelezionato = Left(labelOrdineDiPostoSiaeSelezionato, 2)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo1 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            descrizione = rec(NomeCampo1)
            cmb.AddItem descrizione
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Function GetIdOrdineDiPostoSiaeSelezionato() As Long
    GetIdOrdineDiPostoSiaeSelezionato = idOrdinediPostoSIAESelezionato
End Function

Public Function GetLabelOrdineDiPostoSiaeSelezionato() As String
    GetLabelOrdineDiPostoSiaeSelezionato = labelOrdineDiPostoSiaeSelezionato
End Function

Public Function GetCodiceOrdineDiPostoSiaeSelezionato() As String
    GetCodiceOrdineDiPostoSiaeSelezionato = codiceOrdinediPostoSIAESelezionato
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function



