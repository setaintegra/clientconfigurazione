VERSION 5.00
Begin VB.Form frmConfigurazioneOrganizzazioneManager 
   Caption         =   "Manager"
   ClientHeight    =   5970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9840
   LinkTopic       =   "Form1"
   ScaleHeight     =   5970
   ScaleWidth      =   9840
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstOperatoriManager 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   4200
      MultiSelect     =   2  'Extended
      TabIndex        =   11
      Top             =   1200
      Width           =   3555
   End
   Begin VB.CommandButton cmdOperatoreSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   3720
      TabIndex        =   10
      Top             =   1200
      Width           =   435
   End
   Begin VB.CommandButton cmdOperatoreDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   3720
      TabIndex        =   9
      Top             =   2520
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaOperatoriManager 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   3720
      TabIndex        =   8
      Top             =   3840
      Width           =   435
   End
   Begin VB.ListBox lstOperatoriDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   7
      Top             =   1200
      Width           =   3555
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6420
      TabIndex        =   4
      Top             =   240
      Width           =   3255
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   1
      Top             =   4920
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   8520
      TabIndex        =   0
      Top             =   5400
      Width           =   1155
   End
   Begin VB.Label lblOperatoriManager 
      Alignment       =   2  'Center
      Caption         =   "Manager"
      Height          =   195
      Left            =   4200
      TabIndex        =   13
      Top             =   960
      Width           =   3555
   End
   Begin VB.Label lblOperatoriDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   120
      TabIndex        =   12
      Top             =   960
      Width           =   3555
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Operatori manager"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   6420
      TabIndex        =   5
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String
Private internalEvent As Boolean
Private listaOperatoriDisponibili As Collection
Private listaOperatoriManager As Collection
Private isRecordEditabile As Boolean

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum

Public Sub SetIsOperatoreEditabile(isEditabile As Boolean)
    isRecordEditabile = isEditabile
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
End Sub

Public Sub Init()
    Call CaricaValoriLstOperatoriDisponibili
    Call CaricaValoriLstOperatoriManager
    Call AggiornaAbilitazioneControlli
    
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call InserisciNellaBaseDati
'    Call ScriviLog(CCTA_INSERIMENTO, CCDA_OPERATORE, CCDA_OPERATORE, stringaNota, idOperatoreSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub CaricaValoriLstOperatoriDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista
            
    Call ApriConnessioneBD
    
    Set listaOperatoriDisponibili = New Collection
    
    sql = " SELECT DISTINCT OP.IDOPERATORE ID, OP.USERNAME"
    sql = sql & " FROM"
    sql = sql & " ("
    sql = sql & " SELECT DISTINCT IDOPERATORE"
    sql = sql & " FROM OPERATOREMANAGER"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ) OM, OPERATORE OP, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, CLASSEPUNTOVENDITA CPV"
    sql = sql & " WHERE OP.IDOPERATORE = OM.IDOPERATORE(+)"
    sql = sql & " AND OM.IDOPERATORE IS NULL"
    sql = sql & " AND OP.IDPERFEZIONATORE IS NULL"
    sql = sql & " AND OP.IDPUNTOVENDITAELETTIVO = OCPVPV.IDPUNTOVENDITA"
    sql = sql & " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND OCPVPV.IDCLASSEPUNTOVENDITA = CPV.IDCLASSEPUNTOVENDITA"
    sql = sql & " AND CPV.DIRITTIOPERATOREGESTIBILI = 1"
    sql = sql & " ORDER BY USERNAME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.idElementoLista = rec("ID").Value
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriDisponibili.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOperatoriDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstOperatoriManager()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveOperatore As String
    Dim operatoreCorrente As clsElementoLista

    Call ApriConnessioneBD

    Set listaOperatoriManager = New Collection
    
    sql = " SELECT DISTINCT O.IDOPERATORE ID, O.USERNAME"
    sql = sql & " FROM OPERATORE O, OPERATOREMANAGER OM"
    sql = sql & " WHERE O.IDOPERATORE = OM.IDOPERATORE"
    sql = sql & " AND OM.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY USERNAME"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set operatoreCorrente = New clsElementoLista
            operatoreCorrente.idElementoLista = rec("ID").Value
            operatoreCorrente.nomeElementoLista = rec("USERNAME")
            operatoreCorrente.descrizioneElementoLista = rec("USERNAME")
            chiaveOperatore = ChiaveId(operatoreCorrente.idElementoLista)
            Call listaOperatoriManager.Add(operatoreCorrente, chiaveOperatore)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstOperatoriManager_Init
    
End Sub

Private Sub lstOperatoriDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriDisponibili.Clear

    If Not (listaOperatoriDisponibili Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriDisponibili
            lstOperatoriDisponibili.AddItem operatore.descrizioneElementoLista
            lstOperatoriDisponibili.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstOperatoriManager_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim operatore As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstOperatoriManager.Clear

    If Not (listaOperatoriManager Is Nothing) Then
        i = 1
        For Each operatore In listaOperatoriManager
            lstOperatoriManager.AddItem operatore.descrizioneElementoLista
            lstOperatoriManager.ItemData(i - 1) = operatore.idElementoLista
            i = i + 1
        Next operatore
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaInLstOperatoriDisponibili()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriManager.ListCount
        If lstOperatoriManager.Selected(i - 1) Then
            idOperatore = lstOperatoriManager.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriManager.Item(chiaveOperatore)
            Call listaOperatoriDisponibili.Add(operatore, chiaveOperatore)
            Call listaOperatoriManager.Remove(chiaveOperatore)
        End If
    Next i
    Call lstOperatoriDisponibili_Init
    Call lstOperatoriManager_Init
End Sub

Private Sub SpostaInLstOperatoriManager()
    Dim i As Integer
    Dim idOperatore As Long
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For i = 1 To lstOperatoriDisponibili.ListCount
        If lstOperatoriDisponibili.Selected(i - 1) Then
            idOperatore = lstOperatoriDisponibili.ItemData(i - 1)
            chiaveOperatore = ChiaveId(idOperatore)
            Set operatore = listaOperatoriDisponibili.Item(chiaveOperatore)
            Call listaOperatoriManager.Add(operatore, chiaveOperatore)
            Call listaOperatoriDisponibili.Remove(chiaveOperatore)
        End If
    Next i
    Call lstOperatoriDisponibili_Init
    Call lstOperatoriManager_Init
End Sub

Private Sub SvuotaOperatorimanager()
    Dim operatore As clsElementoLista
    Dim chiaveOperatore As String
    
    For Each operatore In listaOperatoriManager
        chiaveOperatore = ChiaveId(operatore.idElementoLista)
        Call listaOperatoriDisponibili.Add(operatore, chiaveOperatore)
    Next operatore
    Set listaOperatoriManager = Nothing
    Set listaOperatoriManager = New Collection
    
    Call lstOperatoriDisponibili_Init
    Call lstOperatoriManager_Init
End Sub

Private Sub cmdOperatoreDisponibile_Click()
    Call SpostaInLstOperatoriDisponibili
End Sub

Private Sub cmdOperatoreSelezionato_Click()
    Call SpostaInLstOperatoriManager
End Sub

Private Sub cmdSvuotaOperatoriManager_Click()
    Call SvuotaOperatorimanager
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim posizione As Integer
    Dim operatore As clsElementoLista
    Dim n As Long

On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
    
    sql = "DELETE FROM OPERATOREMANAGER WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    SETAConnection.Execute sql, n, adCmdText
    
    If listaOperatoriManager.count > 0 Then
        sql = "INSERT INTO OPERATOREMANAGER (IDOPERATORE, IDORGANIZZAZIONE)"
        sql = sql & " SELECT IDOPERATORE, " & idOrganizzazioneSelezionata
        sql = sql & " FROM OPERATORE"
        sql = sql & " WHERE IDOPERATORE IN (0"
        For posizione = 1 To listaOperatoriManager.count
            Set operatore = listaOperatoriManager.Item(posizione)
            sql = sql & ","
            sql = sql & operatore.idElementoLista
        Next posizione
        sql = sql & ")"
        SETAConnection.Execute sql, n, adCmdText
    End If
    
    SETAConnection.CommitTrans
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub
