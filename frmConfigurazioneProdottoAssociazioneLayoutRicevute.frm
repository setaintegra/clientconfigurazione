VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoAssociazioneLayoutRicevute 
   Caption         =   "Configurazione associazione layout ricevute"
   ClientHeight    =   6075
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11940
   LinkTopic       =   "Form1"
   ScaleHeight     =   6075
   ScaleWidth      =   11940
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEliminaAssociazione 
      Caption         =   "Elimina associazione"
      Height          =   435
      Left            =   120
      TabIndex        =   14
      Top             =   4440
      Width           =   1635
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   435
      Left            =   6240
      TabIndex        =   13
      Top             =   4440
      Width           =   1635
   End
   Begin VB.ListBox lstLayoutSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   6240
      TabIndex        =   11
      Top             =   960
      Width           =   5535
   End
   Begin VB.ListBox lstTipiSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   5835
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   2
      Top             =   5040
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   0
      Top             =   240
      Width           =   1635
   End
   Begin VB.Label Label1 
      Caption         =   "Layout supporto"
      Height          =   195
      Left            =   6240
      TabIndex        =   12
      Top             =   720
      Width           =   3255
   End
   Begin VB.Label lblTipiSupportoLista 
      Caption         =   "Tipi supporto / Layout supporto configurati"
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   720
      Width           =   3255
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione a layout supporto per ricevute"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   7395
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   8
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   7
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoAssociazioneLayoutRicevute"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idTipoSupportoSelezionato As Long
Private idLayoutSupportoSelezionato As Long
Private idTariffaSelezionata As Long
Private nomeLayoutSupportoSelezionato As String
Private nomeTariffaSelezionata As String
Private nomeTipoSupportoSelezionato As String
Private nessunElementoSelezionato As Boolean
Private isProdottoAttivoSuTL As Boolean
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long
Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private r As ADODB.Recordset
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Declare Function Beep Lib "kernel32" (ByVal dwFreq As Long, ByVal dwDuration As Long) As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False

    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        lstTipiSupporto.Enabled = False
        lblTipiSupportoLista.Enabled = False

    Else
        'Do Nothing
    End If

    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = True
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneChiaviProdotto
End Sub

Private Sub CaricaFormConfigurazioneChiaviProdotto()
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetRateo(rateo)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoChiaveProdotto.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoChiaveProdotto.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoChiaveProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoChiaveProdotto.Init
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    Dim sql As String
    Dim n As Long
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    
On Error GoTo errori_LayoutRicevute_Conferma
    
    If idLayoutSupportoSelezionato <> idNessunElementoSelezionato Then
    
        If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
            isConfigurabile = True
            If tipoStatoProdotto = TSP_ATTIVO Then
                Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            End If
            If isConfigurabile Then
                
                Call ApriConnessioneBD_ORA
                ORADB.BeginTrans
              
                sql = "DELETE FROM UTILIZZOLAYOUTRICEVUTA" & _
                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    " AND IDTIPOSUPPORTO = " & idTipoSupportoSelezionato
                n = ORADB.ExecuteSQL(sql)
            
                sql = "INSERT INTO UTILIZZOLAYOUTRICEVUTA (IDPRODOTTO, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & idTipoSupportoSelezionato & ", " & idLayoutSupportoSelezionato & ")"
                n = ORADB.ExecuteSQL(sql)
            
                ORADB.CommitTrans
            
                Call ChiudiConnessioneBD_ORA
                
                InizializzaListaTipiSupporto
                
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI_RICEVUTE, stringaNota, idProdottoSelezionato)
            End If
        Else
            Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
        End If
    
    End If
 
    Exit Sub

errori_LayoutRicevute_Conferma:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdEliminaAssociazione_Click()
    Call Elimina
End Sub

Private Sub Elimina()
    Dim sql As String
    Dim n As Long
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    
On Error GoTo errori_LayoutRicevute_Elimina
    
    If idTipoSupportoSelezionato <> idNessunElementoSelezionato Then
    
        If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
            isConfigurabile = True
            If tipoStatoProdotto = TSP_ATTIVO Then
                Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            End If
            If isConfigurabile Then
                
                Call ApriConnessioneBD_ORA
                ORADB.BeginTrans
              
                sql = "DELETE FROM UTILIZZOLAYOUTRICEVUTA" & _
                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                    " AND IDTIPOSUPPORTO = " & idTipoSupportoSelezionato
                n = ORADB.ExecuteSQL(sql)
            
                ORADB.CommitTrans
            
                Call ChiudiConnessioneBD_ORA
                
                InizializzaListaTipiSupporto
                
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI_RICEVUTE, stringaNota, idProdottoSelezionato)
            End If
        Else
            Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
        End If
    End If
 
    Exit Sub

errori_LayoutRicevute_Elimina:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()

    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
'    gestioneRecordGriglia = asg
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Public Sub Init()
    Dim sql As String

    idLayoutSupportoSelezionato = idNessunElementoSelezionato
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call InizializzaListaTipiSupporto
    Call AggiornaAbilitazioneControlli
    nessunElementoSelezionato = False

    Call Me.Show(vbModal)

End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub InizializzaListaTipiSupporto()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
        
    lstTipiSupporto.Clear
    lstLayoutSupporto.Clear
    
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO ID, DECODE (LS.NOME, NULL, TS.NOME, TS.NOME || ' ******* LAYOUT: ' || LS.NOME) NOME" & _
        " FROM ORGANIZ_TIPOSUP_LAYOUTSUP OTL, SETA_INTEGRA.UTILIZZOLAYOUTRICEVUTA ULR, LAYOUTSUPPORTO LS, TIPOSUPPORTO TS" & _
        " WHERE OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OTL.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO" & _
        " AND TS.ATTIVO = 1" & _
        " AND OTL.IDTIPOSUPPORTO = ULR.IDTIPOSUPPORTO(+)" & _
        " AND ULR.IDPRODOTTO(+) = " & idProdottoSelezionato & _
        " AND ULR.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO(+)" & _
        " AND LS.VALIDOPERTITOLODIACCESSO(+) = 0" & _
        " AND LS.ATTIVO(+) = 1" & _
        " ORDER BY NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstTipiSupporto.AddItem rec("NOME")
            lstTipiSupporto.ItemData(i) = rec("ID")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
End Sub

Private Sub lstLayoutSupporto_Click()
    idLayoutSupportoSelezionato = lstLayoutSupporto.ItemData(lstLayoutSupporto.ListIndex)
End Sub

Private Sub lstTipiSupporto_Click()
    If Not internalEvent Then
        Call SelezionaTipoSupporto
    End If
End Sub

Private Sub SelezionaTipoSupporto()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
        
    idTipoSupportoSelezionato = lstTipiSupporto.ItemData(lstTipiSupporto.ListIndex)
    
    lstLayoutSupporto.Clear
    
    sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO ID, LS.NOME || ' - ' || DESCRIZIONE NOME" & _
        " FROM ORGANIZ_TIPOSUP_LAYOUTSUP OTL, LAYOUTSUPPORTO LS" & _
        " WHERE OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OTL.IDTIPOSUPPORTO = " & idTipoSupportoSelezionato & _
        " AND OTL.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO" & _
        " AND LS.VALIDOPERTITOLODIACCESSO = 0" & _
        " AND LS.ATTIVO = 1" & _
        " ORDER BY NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstLayoutSupporto.AddItem rec("NOME")
            lstLayoutSupporto.ItemData(i) = rec("ID")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

    Call AggiornaAbilitazioneControlli
End Sub
