VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoModalitaFornitura 
   Caption         =   "Prodotto"
   ClientHeight    =   11760
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13485
   LinkTopic       =   "Form1"
   ScaleHeight     =   11760
   ScaleWidth      =   13485
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame6 
      Height          =   1575
      Left            =   1680
      TabIndex        =   48
      Top             =   9120
      Width           =   11655
      Begin VB.CheckBox chkPassbookTutteLeCPV 
         Caption         =   "Tutte le Classi Punto Vendita"
         Height          =   255
         Left            =   8160
         TabIndex        =   53
         Top             =   120
         Width           =   3375
      End
      Begin VB.ListBox lstPassbookClassiPV 
         Height          =   1185
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   52
         Top             =   360
         Width           =   3375
      End
      Begin VB.OptionButton optPassbookSi 
         Caption         =   "Si"
         Height          =   195
         Left            =   120
         TabIndex        =   50
         Top             =   360
         Width           =   615
      End
      Begin VB.OptionButton optPassbookNo 
         Caption         =   "No"
         Height          =   195
         Left            =   840
         TabIndex        =   49
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   9360
      TabIndex        =   36
      Top             =   10680
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   39
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   38
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   37
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   11700
      TabIndex        =   35
      Top             =   360
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9960
      TabIndex        =   34
      Top             =   360
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   240
      TabIndex        =   31
      Top             =   10680
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   33
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   32
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1695
      Left            =   1680
      TabIndex        =   26
      Top             =   5760
      Width           =   11655
      Begin VB.OptionButton optTitoliDigitaliSi 
         Caption         =   "Si"
         Height          =   195
         Left            =   120
         TabIndex        =   30
         Top             =   360
         Width           =   615
      End
      Begin VB.OptionButton optTitoliDigitaliNo 
         Caption         =   "No"
         Height          =   195
         Left            =   840
         TabIndex        =   29
         Top             =   360
         Width           =   735
      End
      Begin VB.ListBox lstTitoliDigitaliClassiPV 
         Height          =   1185
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   28
         Top             =   360
         Width           =   3375
      End
      Begin VB.CheckBox chkTitoliDigitaliTutteLeCPV 
         Caption         =   "Tutte le Classi Punto Vendita"
         Height          =   255
         Left            =   8160
         TabIndex        =   27
         Top             =   120
         Width           =   3375
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1695
      Left            =   1680
      TabIndex        =   19
      Top             =   4080
      Width           =   11655
      Begin VB.OptionButton optSpedizioneNo 
         Caption         =   "No"
         Height          =   200
         Left            =   840
         TabIndex        =   24
         Top             =   360
         Width           =   735
      End
      Begin VB.OptionButton optSpedizioneSi 
         Caption         =   "Si"
         Height          =   200
         Left            =   120
         TabIndex        =   23
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox txtNumGiorniNecessariPerSpedizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         MaxLength       =   4
         TabIndex        =   22
         Top             =   780
         Width           =   555
      End
      Begin VB.ListBox lstSpedizioneClassiPV 
         Height          =   1185
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   21
         Top             =   360
         Width           =   3375
      End
      Begin VB.CheckBox chkSpedizioneTutteLeCPV 
         Caption         =   "Tutte le Classi Punto Vendita"
         Height          =   255
         Left            =   8160
         TabIndex        =   20
         Top             =   120
         Width           =   3375
      End
      Begin VB.Label lblNumGiorniNecessariPerSpedizione 
         Caption         =   "Numero giorni necessari"
         Height          =   195
         Left            =   120
         TabIndex        =   25
         Top             =   840
         Width           =   1935
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1695
      Left            =   1680
      TabIndex        =   14
      Top             =   7440
      Width           =   11655
      Begin VB.OptionButton optHomeTicketingNo 
         Caption         =   "No"
         Height          =   195
         Left            =   840
         TabIndex        =   18
         Top             =   360
         Width           =   735
      End
      Begin VB.OptionButton optHomeTicketingSi 
         Caption         =   "Si"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   615
      End
      Begin VB.ListBox lstHomeTicketingClassiPV 
         Height          =   1185
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   16
         Top             =   360
         Width           =   3375
      End
      Begin VB.CheckBox chkHomeTicketingTutteLeCPV 
         Caption         =   "Tutte le Classi Punto Vendita"
         Height          =   255
         Left            =   8160
         TabIndex        =   15
         Top             =   120
         Width           =   3375
      End
   End
   Begin VB.Frame Frame4 
      Height          =   1695
      Left            =   1680
      TabIndex        =   5
      Top             =   2400
      Width           =   11655
      Begin VB.TextBox txtConsegnaIta 
         Height          =   315
         Left            =   1920
         TabIndex        =   11
         Top             =   840
         Width           =   6135
      End
      Begin VB.TextBox txtConsegnaIng 
         Height          =   315
         Left            =   1920
         TabIndex        =   10
         Top             =   1200
         Width           =   6135
      End
      Begin VB.ListBox lstRitiroClassiPV 
         Height          =   1185
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   9
         Top             =   360
         Width           =   3375
      End
      Begin VB.OptionButton optRitiroSi 
         Caption         =   "Si"
         Height          =   200
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   615
      End
      Begin VB.OptionButton optRitiroNo 
         Caption         =   "No"
         Height          =   200
         Left            =   840
         TabIndex        =   7
         Top             =   360
         Width           =   735
      End
      Begin VB.CheckBox chkRitiroTutteLeCPV 
         Caption         =   "Tutte le Classi Punto Vendita"
         Height          =   255
         Left            =   8160
         TabIndex        =   6
         Top             =   120
         Width           =   3375
      End
      Begin VB.Label lblConsegnaIta 
         Caption         =   "Modalit� consegna - ITA"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label lblConsegnaIng 
         Caption         =   "Modalit� consegna - ING"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1200
         Width           =   1815
      End
   End
   Begin VB.Frame Frame5 
      Height          =   1695
      Left            =   1680
      TabIndex        =   0
      Top             =   720
      Width           =   11655
      Begin VB.OptionButton optStandardNo 
         Caption         =   "No"
         Height          =   200
         Left            =   840
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.OptionButton optStandardSi 
         Caption         =   "Si"
         Height          =   200
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   615
      End
      Begin VB.ListBox lstStandardClassiPV 
         Height          =   1185
         Left            =   8160
         Style           =   1  'Checkbox
         TabIndex        =   2
         Top             =   360
         Width           =   3375
      End
      Begin VB.CheckBox chkStandardTutteLeCPV 
         Caption         =   "Tutte le Classi Punto Vendita"
         Height          =   255
         Left            =   8160
         TabIndex        =   1
         Top             =   120
         Width           =   3375
      End
   End
   Begin VB.Label lblPassbook 
      Caption         =   "Passbook"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   51
      Top             =   9240
      Width           =   1215
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   11700
      TabIndex        =   47
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   9960
      TabIndex        =   46
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Definizione modalit� di fornitura titoli"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   45
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblSpedizione 
      Caption         =   "Spedizione a domicilio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   44
      Top             =   4200
      Width           =   1215
   End
   Begin VB.Label lblTitoliDigitali 
      Caption         =   "Titoli digitali"
      DataField       =   "Spedizione a domicilio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   43
      Top             =   5880
      Width           =   1215
   End
   Begin VB.Label lblHomeTicketing 
      Caption         =   "Home ticketing"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   42
      Top             =   7560
      Width           =   1215
   End
   Begin VB.Label lblRitiro 
      Caption         =   "Ritiro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   41
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label lblStandard 
      Caption         =   "Standard"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   40
      Top             =   840
      Width           =   1215
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoModalitaFornitura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long

Private internalEvent As Boolean

Private prodottoRientraDecretoSicurezza As ValoreBooleanoEnum
Private numeroMassimoTitoliPerAcqProdotto As Long

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Caption = "Fine"
            cmdSuccessivo.Enabled = True
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    Call ApriConnessioneBD_ORA
    
    sql = "SELECT DISTINCT NUMGIORNINECESSARIPERSPEDIZ, MODALITACONSTITSTAMPSUCC_ITA, MODALITACONSTITSTAMPSUCC_ING, " & _
        " PTST.IDTIPOMODALITAFORNITURATITOLI STANDARD, PTR.IDTIPOMODALITAFORNITURATITOLI RITIRO, PTS.IDTIPOMODALITAFORNITURATITOLI SPEDIZIONE, PTTT.IDTIPOMODALITAFORNITURATITOLI TT, PTHT.IDTIPOMODALITAFORNITURATITOLI HT, PTP.IDTIPOMODALITAFORNITURATITOLI P" & _
        " FROM PRODOTTO P, PRODOTTO_TIPOMODALITAFORNITURA PTST, PRODOTTO_TIPOMODALITAFORNITURA PTR, PRODOTTO_TIPOMODALITAFORNITURA PTS, PRODOTTO_TIPOMODALITAFORNITURA PTTT, PRODOTTO_TIPOMODALITAFORNITURA PTHT, PRODOTTO_TIPOMODALITAFORNITURA PTP" & _
        " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND P.IDPRODOTTO = PTST.IDPRODOTTO(+)" & _
        " AND PTST.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_STANDARD & _
        " AND P.IDPRODOTTO = PTR.IDPRODOTTO(+)" & _
        " AND PTR.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_RITIRO & _
        " AND P.IDPRODOTTO = PTS.IDPRODOTTO(+)" & _
        " AND PTS.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_SPEDIZIONE & _
        " AND P.IDPRODOTTO = PTTT.IDPRODOTTO(+)" & _
        " AND PTTT.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_TITOLI_DIGITALI & _
        " AND P.IDPRODOTTO = PTHT.IDPRODOTTO(+)" & _
        " AND PTHT.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_HOME_TICKETING & _
        " AND P.IDPRODOTTO = PTP.IDPRODOTTO(+)" & _
        " AND PTP.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_PASSBOOK
    
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If Not IsNull(rec("NUMGIORNINECESSARIPERSPEDIZ")) Then
            txtNumGiorniNecessariPerSpedizione.Text = rec("NUMGIORNINECESSARIPERSPEDIZ")
        End If
        If Not IsNull(rec("MODALITACONSTITSTAMPSUCC_ITA")) Then
            txtConsegnaIta.Text = rec("MODALITACONSTITSTAMPSUCC_ITA")
        End If
        If Not IsNull(rec("MODALITACONSTITSTAMPSUCC_ING")) Then
            txtConsegnaIng.Text = rec("MODALITACONSTITSTAMPSUCC_ING")
        End If
        
        i = 0
        While Not rec.EOF
            txtConsegnaIta.Enabled = True
            txtConsegnaIng.Enabled = True
            
            If IsNull(rec("STANDARD")) Then
                optStandardNo = True
                chkStandardTutteLeCPV.Enabled = False
                lstStandardClassiPV.Enabled = False
            Else
                optStandardSi = True
                Call InizializzaCPV(TMFT_STANDARD, chkStandardTutteLeCPV, lstStandardClassiPV)
            End If
            
            If IsNull(rec("RITIRO")) Then
                optRitiroNo = True
                chkRitiroTutteLeCPV.Enabled = False
                lstRitiroClassiPV.Enabled = False
            Else
                optRitiroSi = True
                Call InizializzaCPV(TMFT_RITIRO, chkRitiroTutteLeCPV, lstRitiroClassiPV)
            End If
            
            If IsNull(rec("SPEDIZIONE")) Then
                optSpedizioneNo = True
                chkSpedizioneTutteLeCPV.Enabled = False
                lstSpedizioneClassiPV.Enabled = False
            Else
                optSpedizioneSi = True
                Call InizializzaCPV(TMFT_SPEDIZIONE, chkSpedizioneTutteLeCPV, lstSpedizioneClassiPV)
            End If
            
            If IsNull(rec("TT")) Then
                optTitoliDigitaliNo = True
                chkTitoliDigitaliTutteLeCPV.Enabled = False
                lstTitoliDigitaliClassiPV.Enabled = False
            Else
                optTitoliDigitaliSi = True
                Call InizializzaCPV(TMFT_TITOLI_DIGITALI, chkTitoliDigitaliTutteLeCPV, lstTitoliDigitaliClassiPV)
            End If
            
            If IsNull(rec("HT")) Then
                optHomeTicketingNo = True
                chkHomeTicketingTutteLeCPV.Enabled = False
                lstHomeTicketingClassiPV.Enabled = False
            Else
                optHomeTicketingSi = True
                Call InizializzaCPV(TMFT_HOME_TICKETING, chkHomeTicketingTutteLeCPV, lstHomeTicketingClassiPV)
            End If
            
            If IsNull(rec("P")) Then
                optPassbookNo = True
            Else
                optPassbookSi = True
            End If
            
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD_ORA
    
    internalEvent = internalEventOld
    
End Sub
                
Private Sub InizializzaCPV(tmft As TipoModalitaFornituraTitoliEnum, chk As CheckBox, lst As listBox)
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    chk.Enabled = True
    chk.Value = VB_VERO
    lst.Enabled = False
    lst.Clear
    sql = "SELECT CPV.IDCLASSEPUNTOVENDITA ID, CPV.NOME NOME, PT.IDTIPOMODALITAFORNITURATITOLI" & _
        " FROM CLASSEPUNTOVENDITA CPV, PRODOTTO_TIPOMODALITAFORNITURA PT" & _
        " WHERE CPV.IDCLASSEPUNTOVENDITA = PT.IDCLASSEPUNTOVENDITA(+)" & _
        " AND PT.IDPRODOTTO(+) = " & idProdottoSelezionato & _
        " AND PT.IDTIPOMODALITAFORNITURATITOLI(+) = " & tmft & _
        " ORDER BY CPV.IDCLASSEPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lst.AddItem (rec("NOME"))
            lst.ItemData(i) = (rec("ID"))
            If Not (IsNull(rec("IDTIPOMODALITAFORNITURATITOLI"))) Then
                lst.Selected(i) = True
                chk.Value = VB_FALSO
                lst.Enabled = True
            End If
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close

End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    prodottoRientraDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMassimoTitoliPerAcqProdotto = n
End Sub

Private Sub chkStandardTutteLeCPV_Click()
    Call GestisciCheckSiTutteLeCPV(TMFT_STANDARD, chkStandardTutteLeCPV, lstStandardClassiPV)
End Sub

Private Sub chkRitiroTutteLeCPV_Click()
    Call GestisciCheckSiTutteLeCPV(TMFT_RITIRO, chkRitiroTutteLeCPV, lstRitiroClassiPV)
End Sub

Private Sub chkSpedizioneTutteLeCPV_Click()
    Call GestisciCheckSiTutteLeCPV(TMFT_SPEDIZIONE, chkSpedizioneTutteLeCPV, lstSpedizioneClassiPV)
End Sub

Private Sub chkTitoliDigitaliTutteLeCPV_Click()
    Call GestisciCheckSiTutteLeCPV(TMFT_TITOLI_DIGITALI, chkTitoliDigitaliTutteLeCPV, lstTitoliDigitaliClassiPV)
End Sub

Private Sub chkHomeTicketingTutteLeCPV_Click()
    Call GestisciCheckSiTutteLeCPV(TMFT_HOME_TICKETING, chkHomeTicketingTutteLeCPV, lstHomeTicketingClassiPV)
End Sub

Private Sub chkPassbookTutteLeCPV_Click()
    Call GestisciCheckSiTutteLeCPV(TMFT_PASSBOOK, chkPassbookTutteLeCPV, lstPassbookClassiPV)
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConfiguraHomeTicketing_Click()
    Call CaricaFormHomeTicketing
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalit�FormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call InserisciNellaBaseDati
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub Successivo()
    Call AzionePercorsoGuidato(TNCP_FINE)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim sqlSelect As String
    Dim i As Long
    Dim n As Long
    Dim rec As OraDynaset
    Dim ValoriCampiOK As Boolean
    Dim messaggio As String
    Dim consegnaIta As String
    Dim consegnaIng As String
    
On Error GoTo errori_ModalitaFornitura_InserisciNellaBaseDati

    ValoriCampiOK = True

    If optSpedizioneSi.Value = True Then
        If txtNumGiorniNecessariPerSpedizione.Text = "" Then
            messaggio = "Valorizzare il numero di giorni necessari per spedizione"
            ValoriCampiOK = False
        Else
            If Not IsCampoNumericoCorretto(txtNumGiorniNecessariPerSpedizione) Then
                messaggio = "Il numero di giorni necessari per spedizione deve essere un numero"
                ValoriCampiOK = False
            End If
            If Not IsCampoInteroCorretto(txtNumGiorniNecessariPerSpedizione) Then
                messaggio = "Il numero di giorni necessari per spedizione deve essere un intero"
                ValoriCampiOK = False
            End If
        End If
    End If
    
    If ValoriCampiOK Then
        Call ApriConnessioneBD_ORA
        ORADB.BeginTrans
    
        sql = "UPDATE PRODOTTO SET NUMGIORNINECESSARIPERSPEDIZ = "
        If txtNumGiorniNecessariPerSpedizione.Text = "" Then
            sql = sql & "NULL"
        Else
            sql = sql & txtNumGiorniNecessariPerSpedizione.Text
        End If
        sql = sql & " WHERE IDPRODOTTO = " & idProdottoSelezionato
        n = ORADB.ExecuteSQL(sql)

        consegnaIta = Replace(txtConsegnaIta.Text, "'", "`")
        consegnaIng = Replace(txtConsegnaIng.Text, "'", "`")
        sql = "UPDATE PRODOTTO SET MODALITACONSTITSTAMPSUCC_ITA = '" & consegnaIta & "'," & _
            " MODALITACONSTITSTAMPSUCC_ING = '" & consegnaIng & "'" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
        n = ORADB.ExecuteSQL(sql)
    
' La modalit� standard si intende sempre presente
' No questa cosa non e' piu' vera!

'''''''        sqlSelect = "SELECT PT.IDTIPOMODALITAFORNITURATITOLI TMFT" & _
'''''''            " FROM PRODOTTO P, PRODOTTO_TIPOMODALITAFORNITURA PT" & _
'''''''            " WHERE P.IDPRODOTTO = " & idProdottoSelezionato & _
'''''''            " AND P.IDPRODOTTO = PT.IDPRODOTTO(+)" & _
'''''''            " AND PT.IDTIPOMODALITAFORNITURATITOLI(+) = " & TMFT_STANDARD
'''''''        Set rec = ORADB.CreateDynaset(sqlSelect, 0&)
'''''''        If Not (rec.BOF And rec.EOF) Then
'''''''            rec.MoveFirst
'''''''            If IsNull(rec("TMFT")) Then
'''''''                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI)" & _
'''''''                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_STANDARD & ")"
'''''''                n = ORADB.ExecuteSQL(sql)
'''''''            End If
'''''''        End If
'''''''        rec.Close
'''''''

        sql = "DELETE FROM PRODOTTO_TIPOMODALITAFORNITURA" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
        n = ORADB.ExecuteSQL(sql)
        
        If optStandardSi.Value = True Then
            If chkStandardTutteLeCPV.Value = VB_VERO Then
                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_STANDARD & ", NULL)"
                n = ORADB.ExecuteSQL(sql)
            Else
                For i = 1 To lstStandardClassiPV.ListCount
                    If lstStandardClassiPV.Selected(i - 1) = True Then
                        sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & TMFT_STANDARD & ", " & lstStandardClassiPV.ItemData(i - 1) & ")"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            End If
        End If

        If optRitiroSi.Value = True Then
            If chkRitiroTutteLeCPV.Value = VB_VERO Then
                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_RITIRO & ", NULL)"
                n = ORADB.ExecuteSQL(sql)
            Else
                For i = 1 To lstRitiroClassiPV.ListCount
                    If lstRitiroClassiPV.Selected(i - 1) = True Then
                        sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & TMFT_RITIRO & ", " & lstRitiroClassiPV.ItemData(i - 1) & ")"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            End If
        End If

        If optSpedizioneSi.Value = True Then
            If chkSpedizioneTutteLeCPV.Value = VB_VERO Then
                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_SPEDIZIONE & ", NULL)"
                n = ORADB.ExecuteSQL(sql)
            Else
                For i = 1 To lstSpedizioneClassiPV.ListCount
                    If lstSpedizioneClassiPV.Selected(i - 1) = True Then
                        sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & TMFT_SPEDIZIONE & ", " & lstSpedizioneClassiPV.ItemData(i - 1) & ")"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            End If
        End If

        If optTitoliDigitaliSi.Value = True Then
            If chkTitoliDigitaliTutteLeCPV.Value = VB_VERO Then
                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_TITOLI_DIGITALI & ", NULL)"
                n = ORADB.ExecuteSQL(sql)
            Else
                For i = 1 To lstTitoliDigitaliClassiPV.ListCount
                    If lstTitoliDigitaliClassiPV.Selected(i - 1) = True Then
                        sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & TMFT_TITOLI_DIGITALI & ", " & lstTitoliDigitaliClassiPV.ItemData(i - 1) & ")"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            End If
        End If

        If optHomeTicketingSi.Value = True Then
            If chkHomeTicketingTutteLeCPV.Value = VB_VERO Then
                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_HOME_TICKETING & ", NULL)"
                n = ORADB.ExecuteSQL(sql)
            Else
                For i = 1 To lstHomeTicketingClassiPV.ListCount
                    If lstHomeTicketingClassiPV.Selected(i - 1) = True Then
                        sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & TMFT_HOME_TICKETING & ", " & lstHomeTicketingClassiPV.ItemData(i - 1) & ")"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            End If
        End If

'        If optPassbookSi.Value = True Then
'            sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
'                " VALUES (" & idProdottoSelezionato & ", " & TMFT_PASSBOOK & ", " & TCPV_INTERNET & ")"
'            n = ORADB.ExecuteSQL(sql)
'        End If
        If optPassbookSi.Value = True Then
            If chkPassbookTutteLeCPV.Value = VB_VERO Then
                sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                    " VALUES (" & idProdottoSelezionato & ", " & TMFT_PASSBOOK & ", NULL)"
                n = ORADB.ExecuteSQL(sql)
            Else
                For i = 1 To lstPassbookClassiPV.ListCount
                    If lstPassbookClassiPV.Selected(i - 1) = True Then
                        sql = "INSERT INTO PRODOTTO_TIPOMODALITAFORNITURA (IDPRODOTTO, IDTIPOMODALITAFORNITURATITOLI, IDCLASSEPUNTOVENDITA)" & _
                            " VALUES (" & idProdottoSelezionato & ", " & TMFT_PASSBOOK & ", " & lstPassbookClassiPV.ItemData(i - 1) & ")"
                        n = ORADB.ExecuteSQL(sql)
                    End If
                Next i
            End If
        End If

        ORADB.CommitTrans

        Call ChiudiConnessioneBD_ORA

        Call AggiornaAbilitazioneControlli
    Else
        MsgBox messaggio
    End If

    Exit Sub

errori_ModalitaFornitura_InserisciNellaBaseDati:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub CaricaFormHomeTicketing()
    Call frmConfigurazioneProdottoTemplate.Init(idPiantaSelezionata, idProdottoSelezionato, nomeProdottoSelezionato, idOrganizzazioneSelezionata, nomeOrganizzazioneSelezionata)
End Sub

Private Sub AbilitaSpedizione()
    lblNumGiorniNecessariPerSpedizione.Enabled = True
    txtNumGiorniNecessariPerSpedizione.Enabled = True
End Sub

Private Sub DisabilitaSpedizione()
    lblNumGiorniNecessariPerSpedizione.Enabled = False
    txtNumGiorniNecessariPerSpedizione.Enabled = False
End Sub

Private Sub optStandardSi_Click()
    chkStandardTutteLeCPV.Enabled = True
    lstStandardClassiPV.Enabled = True
    Call GestisciCheckSiTutteLeCPV(TMFT_STANDARD, chkStandardTutteLeCPV, lstStandardClassiPV)
End Sub

Private Sub optStandardNo_Click()
    chkStandardTutteLeCPV.Enabled = False
    lstStandardClassiPV.Enabled = False
End Sub

Private Sub optRitiroSi_Click()
    chkRitiroTutteLeCPV.Enabled = True
    lstRitiroClassiPV.Enabled = True
    Call GestisciCheckSiTutteLeCPV(TMFT_RITIRO, chkRitiroTutteLeCPV, lstRitiroClassiPV)
End Sub

Private Sub optRitiroNo_Click()
    chkRitiroTutteLeCPV.Enabled = False
    lstRitiroClassiPV.Enabled = False
End Sub

Private Sub optSpedizioneSi_Click()
    Call AbilitaSpedizione
    chkSpedizioneTutteLeCPV.Enabled = True
    lstSpedizioneClassiPV.Enabled = True
    Call GestisciCheckSiTutteLeCPV(TMFT_SPEDIZIONE, chkSpedizioneTutteLeCPV, lstSpedizioneClassiPV)
End Sub

Private Sub optSpedizioneNo_Click()
    Call DisabilitaSpedizione
    chkSpedizioneTutteLeCPV.Enabled = False
    lstSpedizioneClassiPV.Enabled = False
End Sub

Private Sub optTitoliDigitaliSi_Click()
    chkTitoliDigitaliTutteLeCPV.Enabled = True
    lstTitoliDigitaliClassiPV.Enabled = True
    Call GestisciCheckSiTutteLeCPV(TMFT_TITOLI_DIGITALI, chkTitoliDigitaliTutteLeCPV, lstTitoliDigitaliClassiPV)
End Sub

Private Sub optTitoliDigitaliNo_Click()
    chkTitoliDigitaliTutteLeCPV.Enabled = False
    lstTitoliDigitaliClassiPV.Enabled = False
End Sub

Private Sub optHomeTicketingSi_Click()
    chkHomeTicketingTutteLeCPV.Enabled = True
    lstHomeTicketingClassiPV.Enabled = True
    Call GestisciCheckSiTutteLeCPV(TMFT_HOME_TICKETING, chkHomeTicketingTutteLeCPV, lstHomeTicketingClassiPV)
End Sub

Private Sub optHomeTicketingNo_Click()
    chkHomeTicketingTutteLeCPV.Enabled = False
    lstHomeTicketingClassiPV.Enabled = False
End Sub

Private Sub optPassbookSi_Click()
    chkPassbookTutteLeCPV.Enabled = True
    lstPassbookClassiPV.Enabled = True
    Call GestisciCheckSiTutteLeCPV(TMFT_PASSBOOK, chkPassbookTutteLeCPV, lstPassbookClassiPV)
End Sub

Private Sub optPassbookNo_Click()
    chkPassbookTutteLeCPV.Enabled = False
    lstPassbookClassiPV.Enabled = False
End Sub

Private Sub GestisciCheckSiTutteLeCPV(tmft As TipoModalitaFornituraTitoliEnum, chk As CheckBox, lst As listBox)
    lst.Enabled = True
    If lst.ListCount = 0 Then
        Call InizializzaCPV(tmft, chk, lst)
    End If
End Sub

