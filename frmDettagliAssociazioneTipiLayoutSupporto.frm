VERSION 5.00
Begin VB.Form frmDettagliAssociazioneTipiLayoutSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli associazione tipi supporto / layout supporto"
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8985
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   8985
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   435
      Left            =   3120
      TabIndex        =   11
      Top             =   6180
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   435
      Left            =   4920
      TabIndex        =   10
      Top             =   6180
      Width           =   1035
   End
   Begin VB.OptionButton optPianta 
      Caption         =   "intera pianta"
      Height          =   195
      Left            =   2340
      TabIndex        =   9
      Top             =   240
      Width           =   1455
   End
   Begin VB.OptionButton optSuperaree 
      Caption         =   "superaree"
      Height          =   195
      Left            =   1200
      TabIndex        =   8
      Top             =   240
      Width           =   1095
   End
   Begin VB.OptionButton optAree 
      Caption         =   "aree"
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   240
      Width           =   1035
   End
   Begin VB.ListBox lstAreeSuperaree 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5580
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   6
      Top             =   480
      Width           =   4275
   End
   Begin VB.ListBox lstLayoutSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   4560
      Style           =   1  'Checkbox
      TabIndex        =   4
      Top             =   4560
      Width           =   4275
   End
   Begin VB.ListBox lstTipiSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   4560
      Style           =   1  'Checkbox
      TabIndex        =   1
      Top             =   2520
      Width           =   4275
   End
   Begin VB.ListBox lstTariffe 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   4560
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   480
      Width           =   4275
   End
   Begin VB.Label lblLayoutSupporto 
      Caption         =   "Layout supporto"
      Height          =   195
      Left            =   4560
      TabIndex        =   5
      Top             =   4320
      Width           =   3255
   End
   Begin VB.Label lblTipiSupporto 
      Caption         =   "Tipi supporto"
      Height          =   195
      Left            =   4560
      TabIndex        =   3
      Top             =   2280
      Width           =   3255
   End
   Begin VB.Label lblTariffe 
      Caption         =   "Tariffe del prodotto (terminali associati)"
      Height          =   195
      Left            =   4560
      TabIndex        =   2
      Top             =   240
      Width           =   2955
   End
End
Attribute VB_Name = "frmDettagliAssociazioneTipiLayoutSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idProdottoMaster As Long
Private listaAreeSuperaree As Collection
Private listaTariffe As Collection
Private listaTipiTerminale As Collection
Private listaTipiSupporto As Collection
Private listaLayoutSupporto As Collection
Private listaIdAreeSuperareeSelezionate As Collection
Private listaIdTariffeSelezionate As Collection
Private listaIdTipiTerminaleSelezionati As Collection
Private listaIdTipiSupportoSelezionati As Collection
Private listaIdLayoutSupportoSelezionati As Collection
Private selezionaInteraPianta As Boolean
Private selezionaSuperaree As Boolean
Private selezionaAree As Boolean
Private idPiantaMaster As Long
'Private numeroElementiSelezionati As Integer

Private exitCode As ExitCodeEnum
Private internalEvent As Boolean

Public Sub Init()
    Dim internalEventOld As Boolean
    
'    internalEventOld = internalEvent
'    internalEvent = True
    
    Call Variabili_Init
    Call AggiornaAbilitazioneControlli
    Me.Show (vbModal)
    
'    internalEvent = internalEventOld
End Sub

Private Sub Variabili_Init()
    selezionaInteraPianta = False
    selezionaSuperaree = False
    selezionaAree = False
    Set listaIdAreeSuperareeSelezionate = New Collection
    Set listaIdTariffeSelezionate = New Collection
    Set listaIdTipiTerminaleSelezionati = New Collection
    Set listaIdTipiSupportoSelezionati = New Collection
    Set listaIdLayoutSupportoSelezionati = New Collection
    idPiantaMaster = IdPiantaAssociataAProdotto(idProdottoMaster)
'    numeroElementiSelezionati = 0
End Sub

Private Sub AggiornaAbilitazioneControlli()
    lstAreeSuperaree.Enabled = (selezionaAree Or selezionaSuperaree)
    If Not (listaIdAreeSuperareeSelezionate Is Nothing) Or selezionaInteraPianta Then
        lstTariffe.Enabled = (listaIdAreeSuperareeSelezionate.count > 0 Or selezionaInteraPianta)
    Else
        lstTariffe.Enabled = selezionaInteraPianta
    End If
    If Not (listaIdTipiTerminaleSelezionati Is Nothing) Then
        lstTipiSupporto.Enabled = (listaIdTariffeSelezionate.count > 0)
    Else
        lstTipiSupporto.Enabled = False
    End If
    If Not (listaIdTipiSupportoSelezionati Is Nothing) Then
        lstLayoutSupporto.Enabled = (listaIdTipiSupportoSelezionati.count > 0)
    Else
        lstLayoutSupporto.Enabled = False
    End If
    If Not (listaIdAreeSuperareeSelezionate Is Nothing) Then
'        If Not (listaIdLayoutSupportoSelezionati Is Nothing) Then
'            cmdConferma.Enabled = (listaIdLayoutSupportoSelezionati.count > 0)
'        Else
            cmdConferma.Enabled = (listaIdAreeSuperareeSelezionate.count > 0 Or selezionaInteraPianta)
'        End If
    Else
        cmdConferma.Enabled = selezionaInteraPianta
    End If
End Sub

Public Sub SetIdProdottoMaster(idPM As Long)
    idProdottoMaster = idPM
End Sub

Private Sub optAree_Click()
    If Not internalEvent Then
        Call optAree_Update
    End If
End Sub

Private Sub optAree_Update()
    selezionaInteraPianta = False
    selezionaSuperaree = False
    selezionaAree = True
    Call CaricaListaAreeSuperaree
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optPianta_Click()
    If Not internalEvent Then
        Call optPianta_Update
    End If
End Sub

Private Sub optPianta_Update()
    selezionaInteraPianta = True
    selezionaSuperaree = False
    selezionaAree = False
    Call CaricaListaTariffe
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optSuperaree_Click()
    If Not internalEvent Then
        Call optSuperaree_Update
    End If
End Sub

Private Sub optSuperaree_Update()
    selezionaInteraPianta = False
    selezionaSuperaree = True
    selezionaAree = False
    Call CaricaListaAreeSuperaree
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaAreeSuperaree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim recordCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Call lstAreeSuperaree.Clear
    Set listaAreeSuperaree = New Collection
    Set listaIdAreeSuperareeSelezionate = New Collection
    If selezionaSuperaree Then
        sql = "SELECT IDAREA, A.CODICE, A.NOME" & _
            " FROM AREA A, PRODOTTO P" & _
            " WHERE P.IDPIANTA = A.IDPIANTA" & _
            " AND P.IDPRODOTTO = " & idProdottoMaster & _
            " AND A.IDTIPOAREA = " & TA_SUPERAREA_NUMERATA & _
            " ORDER BY CODICE, NOME"
    ElseIf selezionaAree Then
        sql = "SELECT IDAREA, A.CODICE, A.NOME, A.IDAREA_PADRE" & _
            " FROM AREA A, PRODOTTO P" & _
            " WHERE P.IDPIANTA = A.IDPIANTA" & _
            " AND P.IDPRODOTTO = " & idProdottoMaster & _
            " AND A.IDTIPOAREA = " & TA_AREA_NUMERATA & _
            " ORDER BY CODICE, NOME"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordCorrente = New clsElementoLista
            recordCorrente.idElementoLista = rec("IDAREA")
            recordCorrente.codiceElementoLista = rec("CODICE")
            recordCorrente.nomeElementoLista = rec("NOME")
            If selezionaAree Then
                recordCorrente.idAttributoElementoLista = rec("IDAREA_PADRE")
            Else
                recordCorrente.idAttributoElementoLista = idNessunElementoSelezionato
            End If
            recordCorrente.descrizioneElementoLista = recordCorrente.codiceElementoLista & _
                " - " & recordCorrente.nomeElementoLista
            Call listaAreeSuperaree.Add(recordCorrente, ChiaveId(recordCorrente.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call listBox_Init(lstAreeSuperaree, listaAreeSuperaree)
    
End Sub

Private Sub listBox_Init(lst As ListBox, lista As Collection)
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lst.Clear

    If Not (lista Is Nothing) Then
        i = 1
        For Each tip In lista
            lst.AddItem tip.descrizioneElementoLista
            lst.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
'    Call ListaIdTariffeSelezionate_Update
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub CaricaListaTariffe()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim recordCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Call lstTariffe.Clear
    Set listaTariffe = New Collection
    Set listaIdTariffeSelezionate = New Collection
'    If selezionaSuperaree Then
'        sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
'        sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTO ULS"
'        sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
'        sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
'        sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'        sql = sql & " ORDER BY T.CODICE"
'    ElseIf selezionaAree Then
'        sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
'        sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'        sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
'        sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
'        sql = sql & " AND ULS.IDAREA = A.IDAREA_PADRE"
'        sql = sql & " AND A.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'        sql = sql & " ORDER BY T.CODICE"
'    ElseIf selezionaInteraPianta Then
'        sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
'        sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'        sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
'        sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
'        sql = sql & " AND ULS.IDAREA = A.IDAREA"
'        sql = sql & " AND A.IDPIANTA = " & IdPiantaMaster
'        sql = sql & " ORDER BY T.CODICE"
'    End If
    If selezionaInteraPianta Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
'            sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'            sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
'            sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
'            sql = sql & " AND ULS.IDAREA = A.IDAREA"
'            sql = sql & " AND A.IDPIANTA = " & idPiantaMaster
'            sql = sql & " AND (ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'            sql = sql & " OR ULS.IDTIPOSTATORECORD IS NULL )"
'            sql = sql & " ORDER BY T.CODICE"
'        Else
            sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
            sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTOCPV ULS, AREA A"
            sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
            sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
            sql = sql & " AND ULS.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPiantaMaster
            sql = sql & " ORDER BY T.CODICE"
'        End If
    Else
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
'            sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTO ULS"
'            sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
'            sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
'            sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'            sql = sql & " AND (ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'            sql = sql & " OR ULS.IDTIPOSTATORECORD IS NULL )"
'            sql = sql & " ORDER BY T.CODICE"
'        Else
            sql = "SELECT DISTINCT T.IDTARIFFA, T.CODICE, T.NOME"
            sql = sql & " FROM TARIFFA T, UTILIZZOLAYOUTSUPPORTOCPV ULS"
            sql = sql & " WHERE T.IDPRODOTTO = " & idProdottoMaster
            sql = sql & " AND ULS.IDTARIFFA = T.IDTARIFFA"
            sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
            sql = sql & " ORDER BY T.CODICE"
'        End If
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordCorrente = New clsElementoLista
            recordCorrente.idElementoLista = rec("IDTARIFFA")
            recordCorrente.codiceElementoLista = rec("CODICE")
            recordCorrente.nomeElementoLista = rec("NOME")
            recordCorrente.descrizioneElementoLista = recordCorrente.codiceElementoLista & _
                " - " & recordCorrente.nomeElementoLista
            Call listaTariffe.Add(recordCorrente, ChiaveId(recordCorrente.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call listBox_Init(lstTariffe, listaTariffe)
    
End Sub

Private Sub lstAreeSuperaree_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaElemento(lstAreeSuperaree, listaIdAreeSuperareeSelezionate)
    End If
End Sub

Private Sub lstTariffe_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaElemento(lstTariffe, listaIdTariffeSelezionate)
    End If
End Sub

Private Sub lstTipiSupporto_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaElemento(lstTipiSupporto, listaIdTipiSupportoSelezionati)
    End If
End Sub

Private Sub lstLayoutSupporto_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaElemento(lstLayoutSupporto, listaIdLayoutSupportoSelezionati)
    End If
End Sub

Private Sub SelezionaDeselezionaElemento(lst As ListBox, lista As Collection)
    Dim id As Long
    
    id = lst.ItemData(lst.ListIndex)
    If lst.Selected(lst.ListIndex) Then
        Call lista.Add(id, ChiaveId(id))
    Else
        Call lista.Remove(ChiaveId(id))
    End If
    Select Case lst
        Case lstAreeSuperaree
            If listaIdAreeSuperareeSelezionate.count > 0 Then
                Call CaricaListaTariffe
            End If
        Case lstTariffe
            If listaIdTariffeSelezionate.count > 0 Then
                Call CaricaListaTipiSupporto
            End If
        Case lstTipiSupporto
            If listaTipiSupporto.count > 0 Then
                Call CaricaListaLayoutSupporto
            End If
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaListaTipiSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim recordCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Call lstTipiSupporto.Clear
    Set listaTipiSupporto = New Collection
    Set listaIdTipiSupportoSelezionati = New Collection
'    If selezionaSuperaree Then
'        sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
'        sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTO ULS"
'        sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
'        sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'        sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'        sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
'        sql = sql & " ORDER BY TS.CODICE"
'    ElseIf selezionaAree Then
'        sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
'        sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'        sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
'        sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'        sql = sql & " AND ULS.IDAREA = A.IDAREA_PADRE"
'        sql = sql & " AND A.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'        sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
'        sql = sql & " ORDER BY TS.CODICE"
'    ElseIf selezionaInteraPianta Then
'        sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
'        sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'        sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
'        sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'        sql = sql & " AND ULS.IDAREA = A.IDAREA"
'        sql = sql & " AND A.IDPIANTA = " & IdPiantaMaster
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'        sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
'        sql = sql & " ORDER BY TS.CODICE"
'    End If
    If selezionaInteraPianta Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
'            sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'            sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
'            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'            sql = sql & " AND ULS.IDAREA = A.IDAREA"
'            sql = sql & " AND A.IDPIANTA = " & idPiantaMaster
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'            sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_POS & ", " & TT_TERMINALE_WEB & ", " & TT_TERMINALE_TOTEM & ")"
'            sql = sql & " AND (ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'            sql = sql & " OR ULS.IDTIPOSTATORECORD IS NULL )"
'            sql = sql & " ORDER BY TS.CODICE"
'        Else
            sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
            sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTOCPV ULS, AREA A"
            sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
            sql = sql & " AND ULS.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPiantaMaster
            sql = sql & " ORDER BY TS.CODICE"
'        End If
    Else
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
'            sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTO ULS"
'            sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
'            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'            sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'            sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_POS & ", " & TT_TERMINALE_WEB & ", " & TT_TERMINALE_TOTEM & ")"
'            sql = sql & " AND (ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'            sql = sql & " OR ULS.IDTIPOSTATORECORD IS NULL )"
'            sql = sql & " ORDER BY TS.CODICE"
'        Else
            sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE, TS.NOME"
            sql = sql & " FROM TIPOSUPPORTO TS, UTILIZZOLAYOUTSUPPORTOCPV ULS"
            sql = sql & " WHERE TS.IDTIPOSUPPORTO = ULS.IDTIPOSUPPORTO"
            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
            sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
            sql = sql & " ORDER BY TS.CODICE"
'        End If
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordCorrente = New clsElementoLista
            recordCorrente.idElementoLista = rec("IDTIPOSUPPORTO")
            recordCorrente.codiceElementoLista = rec("CODICE")
            recordCorrente.nomeElementoLista = rec("NOME")
            recordCorrente.descrizioneElementoLista = recordCorrente.codiceElementoLista & _
                " - " & recordCorrente.nomeElementoLista
            Call listaTipiSupporto.Add(recordCorrente, ChiaveId(recordCorrente.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call listBox_Init(lstTipiSupporto, listaTipiSupporto)
    
End Sub

Private Sub CaricaListaLayoutSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim recordCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Call lstLayoutSupporto.Clear
    Set listaLayoutSupporto = New Collection
    Set listaIdLayoutSupportoSelezionati = New Collection
'    If selezionaSuperaree Then
'        sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
'        sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTO ULS"
'        sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
'        sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'        sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'        sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
'        sql = sql & " ORDER BY LS.CODICE"
'    ElseIf selezionaAree Then
'        sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
'        sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'        sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
'        sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'        sql = sql & " AND ULS.IDAREA = A.IDAREA_PADRE"
'        sql = sql & " AND A.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'        sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
'        sql = sql & " ORDER BY LS.CODICE"
'    ElseIf selezionaInteraPianta Then
'        sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
'        sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'        sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
'        sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'        sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'        sql = sql & " AND ULS.IDAREA = A.IDAREA"
'        sql = sql & " AND A.IDPIANTA = " & IdPiantaMaster
'        sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'        sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_LOTTO & ", " & TT_TERMINALE_WEB & ")"
'        sql = sql & " ORDER BY LS.CODICE"
'    End If
    If selezionaInteraPianta Then
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
'            sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTO ULS, AREA A"
'            sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
'            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'            sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'            sql = sql & " AND ULS.IDAREA = A.IDAREA"
'            sql = sql & " AND A.IDPIANTA = " & idPiantaMaster
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'            sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_POS & ", " & TT_TERMINALE_WEB & ", " & TT_TERMINALE_TOTEM & ")"
'            sql = sql & " AND (ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'            sql = sql & " OR ULS.IDTIPOSTATORECORD IS NULL )"
'            sql = sql & " ORDER BY LS.CODICE"
'        Else
            sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
            sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTOCPV ULS, AREA A"
            sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
            sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
            sql = sql & " AND ULS.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPiantaMaster
            sql = sql & " ORDER BY LS.CODICE"
'        End If
    Else
'        If tipoModalitąConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
'            sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTO ULS"
'            sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
'            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
'            sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN (" & ElencoDaLista(listaIdTipiTerminaleSelezionati) & ")"
'            sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
'            sql = sql & " AND ULS.IDTIPOTERMINALE IN"
'            sql = sql & "(" & TT_TERMINALE_AVANZATO & ", " & TT_TERMINALE_POS & ", " & TT_TERMINALE_WEB & ", " & TT_TERMINALE_TOTEM & ")"
'            sql = sql & " AND (ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO
'            sql = sql & " OR ULS.IDTIPOSTATORECORD IS NULL )"
'            sql = sql & " ORDER BY LS.CODICE"
'        Else
            sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.CODICE, LS.NOME"
            sql = sql & " FROM LAYOUTSUPPORTO LS, UTILIZZOLAYOUTSUPPORTOCPV ULS"
            sql = sql & " WHERE LS.IDLAYOUTSUPPORTO = ULS.IDLAYOUTSUPPORTO"
            sql = sql & " AND ULS.IDTARIFFA IN (" & ElencoDaLista(listaIdTariffeSelezionate) & ")"
            sql = sql & " AND ULS.IDTIPOSUPPORTO IN (" & ElencoDaLista(listaIdTipiSupportoSelezionati) & ")"
            sql = sql & " AND ULS.IDAREA IN (" & ElencoDaLista(listaIdAreeSuperareeSelezionate) & ")"
            sql = sql & " ORDER BY LS.CODICE"
'        End If
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordCorrente = New clsElementoLista
            recordCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO")
            recordCorrente.codiceElementoLista = rec("CODICE")
            recordCorrente.nomeElementoLista = rec("NOME")
            recordCorrente.descrizioneElementoLista = recordCorrente.codiceElementoLista & _
                " - " & recordCorrente.nomeElementoLista
            Call listaLayoutSupporto.Add(recordCorrente, ChiaveId(recordCorrente.idElementoLista))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call listBox_Init(lstLayoutSupporto, listaLayoutSupporto)
    
End Sub

Public Function GetListaIdAreeSuperareeSelezionate() As Collection
    If Not (listaIdAreeSuperareeSelezionate Is Nothing) Then
        Set GetListaIdAreeSuperareeSelezionate = listaIdAreeSuperareeSelezionate
    Else
        Set GetListaIdAreeSuperareeSelezionate = New Collection
    End If
End Function

Public Function GetListaIdTariffeSelezionate() As Collection
    If Not (listaIdTariffeSelezionate Is Nothing) Then
        Set GetListaIdTariffeSelezionate = listaIdTariffeSelezionate
    Else
'        Set GetListaIdTariffeSelezionate = New Collection
        Set GetListaIdTariffeSelezionate = New Collection
    End If
End Function

Public Function GetListaIdTipiTerminaleSelezionati() As Collection
    If Not (listaIdTipiTerminaleSelezionati Is Nothing) Then
        Set GetListaIdTipiTerminaleSelezionati = listaIdTipiTerminaleSelezionati
    Else
        Set GetListaIdTipiTerminaleSelezionati = New Collection
    End If
End Function

Public Function GetListaIdTipiSupportoSelezionati() As Collection
    If Not (listaIdTipiSupportoSelezionati Is Nothing) Then
        Set GetListaIdTipiSupportoSelezionati = listaIdTipiSupportoSelezionati
    Else
        Set GetListaIdTipiSupportoSelezionati = New Collection
    End If
End Function

Public Function GetListaIdLayoutSupportoSelezionati() As Collection
    If Not (listaIdLayoutSupportoSelezionati Is Nothing) Then
        Set GetListaIdLayoutSupportoSelezionati = listaIdLayoutSupportoSelezionati
    Else
        Set GetListaIdLayoutSupportoSelezionati = New Collection
    End If
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Function GetSelezionaInteraPianta() As Boolean
    GetSelezionaInteraPianta = selezionaInteraPianta
End Function
'
'Private Function IdPiantaMaster() As Long
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim id As Long
'
'    Call ApriConnessioneBD
'
'    IdPiantaMaster = idNessunElementoSelezionato
'    sql = "SELECT IDPIANTA FROM PRODOTTO WHERE IDPRODOTTO = " & idProdottoMaster
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.EOF And rec.BOF) Then
'        rec.MoveFirst
'        id = rec("IDPIANTA").Value
'    End If
'    rec.Close
'
'    Call ChiudiConnessioneBD
'
'    IdPiantaMaster = id
'End Function
