VERSION 5.00
Begin VB.Form frmDettagliFascia 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli Fascia"
   ClientHeight    =   1740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   3960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtArea 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   780
      MaxLength       =   30
      TabIndex        =   1
      Top             =   660
      Width           =   2655
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   540
      TabIndex        =   2
      Top             =   1200
      Width           =   1035
   End
   Begin VB.TextBox txtIndiceDiPreferibilita 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1980
      MaxLength       =   4
      TabIndex        =   0
      Top             =   180
      Width           =   675
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2220
      TabIndex        =   3
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblIndiceDiPreferibilita 
      Alignment       =   1  'Right Justify
      Caption         =   "Indice di preferibilitą"
      Height          =   195
      Left            =   360
      TabIndex        =   5
      Top             =   240
      Width           =   1515
   End
   Begin VB.Label lblArea 
      Alignment       =   1  'Right Justify
      Caption         =   "Area"
      Height          =   195
      Left            =   300
      TabIndex        =   4
      Top             =   720
      Width           =   375
   End
End
Attribute VB_Name = "frmDettagliFascia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private indiceDiPreferibilita As Long
Private idFascia As Long
Private idAreaSelezionata As Long
Private nomeAreaSelezionata As String
Private xPosto As Integer
Private yPosto As Integer
Private listaFasceConfigurate As Collection

Private modalita As ModalitaFormDettagliFasciaEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init()
    Select Case modalita
        Case MFDF_CREA_FASCIA
            indiceDiPreferibilita = 0
        Case Else
            'Do Nothing
    End Select
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()

    lblIndiceDiPreferibilita.Enabled = True
    txtIndiceDiPreferibilita.Enabled = True
    txtArea.Enabled = False
    Select Case modalita
        Case MFDF_CREA_FASCIA
            frmDettagliFascia.Caption = "Creazione Nuova Fascia"
            cmdConferma.Enabled = (Len(Trim(txtIndiceDiPreferibilita.Text)) > 0)
        Case MFDF_MODIFICA_ATTRIBUTI_FASCIA
            frmDettagliPosto.Caption = "Modifica Attributi Fascia"
            cmdConferma.Caption = "&Modifica"
            cmdAnnulla.Caption = "&Esci"
            cmdConferma.Enabled = (Len(Trim(txtIndiceDiPreferibilita.Text)) > 0)
        Case MFDF_ELIMINA_FASCIA
            frmDettagliPosto.Caption = "Elimina Fascia"
            lblIndiceDiPreferibilita.Enabled = False
            txtIndiceDiPreferibilita.Enabled = False
        Case Else
    End Select
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    Set listaNonConformitą = New Collection
    
    If IsCampoInteroCorretto(txtIndiceDiPreferibilita) Then
        If CInt(Trim(txtIndiceDiPreferibilita.Text)) > 0 Then
            indiceDiPreferibilita = CInt(Trim(txtIndiceDiPreferibilita.Text))
        Else
'            Call frmMessaggio.Visualizza("ErroreFormatoDatiIndiceDiPreferibilita")
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore immesso sul campo indice di preferibilitą deve essere numerico di tipo intero e maggiore di zero;")
        End If
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiIndiceDiPreferibilita")
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo indice di preferibilitą deve essere numerico di tipo intero e maggiore di zero;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If
    
End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call frmConfigurazionePiantaFasceSequenze.SetExitCodeDettagliFascia(EC_DP_ANNULLA)
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    Dim i As Integer
    Dim trovato As Boolean
    Dim indiceFasciaCorrente As Integer
    
    If modalita = MFDF_ELIMINA_FASCIA Then
        Call frmConfigurazionePiantaFasceSequenze.SetExitCodeDettagliFascia(EC_DP_CONFERMA)
        Call frmConfigurazionePiantaFasceSequenze.SetIndiceDiPreferibilitaFascia(indiceDiPreferibilita)
        Unload Me
    Else
        If ValoriCampiOK Then
            i = 1
            trovato = False
            While i <= listaFasceConfigurate.count And Not trovato
                If indiceDiPreferibilita = listaFasceConfigurate(i) Then
                    trovato = True
                End If
                i = i + 1
            Wend
            If trovato Then
                Call frmMessaggio.Visualizza("AvvertimentoIndicePreferibilitaGiaPresentePerArea", _
                                              indiceDiPreferibilita, _
                                              nomeAreaSelezionata)
            Else
                Call frmConfigurazionePiantaFasceSequenze.SetExitCodeDettagliFascia(EC_DP_CONFERMA)
                Call frmConfigurazionePiantaFasceSequenze.SetIndiceDiPreferibilitaFascia(indiceDiPreferibilita)
                Unload Me
            End If
        End If
    End If
    
End Sub

Private Sub txtIndiceDiPreferibilita_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetModalitaForm(moda As ModalitaFormDettagliFasciaEnum)
    modalita = moda
End Sub

Public Sub SetIndiceDiPreferibilita(ind As Long)
    indiceDiPreferibilita = ind
End Sub

Public Sub SetIdFascia(idF As Long)
    idFascia = idF
End Sub

Private Sub AssegnaValoriCampi()

    txtIndiceDiPreferibilita.Text = IIf(indiceDiPreferibilita = 0, "", indiceDiPreferibilita)
    txtArea.Text = Trim(nomeAreaSelezionata)
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub

Public Sub SetListaFasceConfigurate(listaF As Collection)
    Dim fascia As classeFascia
    
    Set listaFasceConfigurate = New Collection
    For Each fascia In listaF
        Call listaFasceConfigurate.Add(fascia.indiceDiPreferibilita)
    Next fascia
End Sub

