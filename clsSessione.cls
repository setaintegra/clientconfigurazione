VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSessione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public id As Long
Public nome As String
Public descrizione As String       'Descrizione
Public idTipoSessione As TipoSessioneConfigurazioneEnum
Public tipoSessione As String
Public dataOraCreazione As Date
Public NomeMacchina As String

Public descrittore As String            'Contiene la descrizione


Public Sub Conferma(id As Long)
    Dim sql As String
    Dim sql1 As String
    
    sql = "UPDATE CC_SESSIONECONFIGURAZIONE " & _
        "  SET DATAORACONFERMA = sysdate " & _
        "  WHERE IDSESSIONECONFIGURAZIONE = " & idSessioneConfigurazioneCorrente
   
    ApriConnessioneBD
    
    SETAConnection.Execute sql, , adCmdText

    ChiudiConnessioneBD
End Sub


Public Sub SalvaNellaBaseDati()
    Dim n As Long
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
    sql = "SELECT SQ_SESSIONECONFIGURAZIONE.NEXTVAL AS NEWID FROM DUAL"
    rec.Open sql, SETAConnection, adOpenKeyset, adLockOptimistic
    id = rec("NEWID").Value
    rec.Close

    sql = " INSERT INTO CC_SESSIONECONFIGURAZIONE " & _
          " (IDSESSIONECONFIGURAZIONE, IDTIPOSESSIONECONFIGURAZIONE, NOMEMACCHINA, DATAORACREAZIONE, DESCRIZIONE, NOME)" & _
          " VALUES( " & id & " , " & idTipoSessione & " ," & SqlStringValue(getNomeMacchina) & " ,sysdate, " & SqlStringValue(descrizione) & ", " & SqlStringValue(nome) & " )"
    
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
End Sub


Public Sub CaricaDallaBaseDatiTramiteId(idSessione As Long)
    Dim rec As New ADODB.Recordset
    Dim sql As String
    
        sql = "SELECT Dataoracreazione, CC_SessioneConfigurazione.Descrizione  Descrizione," & _
            " Idsessioneconfigurazione, CC_SessioneConfigurazione.Idtiposessioneconfigurazione, " & _
            " CC_SessioneConfigurazione.Nome Sessione, CC_SessioneConfigurazione.Nomemacchina," & _
            " CC_TipoSessioneConfigurazione.Nome tipoSessione, CC_SessioneConfigurazione.idTipoSessioneConfigurazione idTipoSessione" & _
            " FROM CC_SessioneConfigurazione,CC_TipoSessioneConfigurazione " & _
            " WHERE CC_SessioneConfigurazione.idTipoSessioneConfigurazione = CC_TipoSessioneConfigurazione.idTipoSessioneConfigurazione" & _
            " AND CC_SessioneConfigurazione.IdSessioneConfigurazione = " & idSessione
          
    ApriConnessioneBD
    
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    
    If Not (rec.BOF And rec.EOF) Then
        id = rec("IdSessioneConfigurazione")
        nome = rec("Sessione")
        descrizione = ValoreCampo_string(rec, "Descrizione")
        dataOraCreazione = rec("dataOraCreazione")
        NomeMacchina = rec("nomeMacchina")
        idTipoSessione = rec("idTipoSessione")
        tipoSessione = rec("tipoSessione")
    End If
    rec.Close
    
    ChiudiConnessioneBD
    
End Sub

Public Function EsisteInBaseDati() As Boolean
    Dim rec As New ADODB.Recordset
    Dim sql As String
    
    sql = "SELECT COUNT(*) AS NumSessioni FROM CC_SESSIONECONFIGURAZIONE " & _
        "WHERE Nome = " & SqlStringValue(nome) & " AND NomeMacchina = " & SqlStringValue(getNomeMacchina)
    
    ApriConnessioneBD
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    
    EsisteInBaseDati = (rec("NumSessioni") > 0)
    
    rec.Close
    ChiudiConnessioneBD
End Function

