VERSION 5.00
Begin VB.Form frmConfigurazioneOrganizzazioneTipiSupportoDigitali 
   Caption         =   "Organizzazione"
   ClientHeight    =   7245
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12825
   LinkTopic       =   "Form1"
   ScaleHeight     =   7245
   ScaleWidth      =   12825
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkPassbook 
      Caption         =   "Utilizza Passbook"
      Height          =   375
      Left            =   8760
      TabIndex        =   13
      Top             =   1200
      Width           =   3855
   End
   Begin VB.CheckBox chkCarteFidelizzazione 
      Caption         =   "Utilizza tutte le carte di fidelizzazione"
      Height          =   375
      Left            =   4560
      TabIndex        =   12
      Top             =   1200
      Width           =   3855
   End
   Begin VB.CheckBox chkTessereTifoso 
      Caption         =   "Utilizza tutte le tessere del tifoso"
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   1200
      Width           =   3855
   End
   Begin VB.ListBox lstCarteFidelizzazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3900
      Left            =   4560
      Style           =   1  'Checkbox
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1800
      Width           =   3915
   End
   Begin VB.ListBox lstTessereTifoso 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3900
      Left            =   360
      Style           =   1  'Checkbox
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1800
      Width           =   3885
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   11520
      TabIndex        =   4
      Top             =   6600
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   360
      TabIndex        =   1
      Top             =   6120
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9420
      TabIndex        =   0
      Top             =   240
      Width           =   3255
   End
   Begin VB.Label lblPassbook 
      Alignment       =   2  'Center
      Caption         =   "Passbook"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   8880
      TabIndex        =   14
      Top             =   840
      Width           =   3735
   End
   Begin VB.Label lblCarteFidelizzazione 
      Alignment       =   2  'Center
      Caption         =   "Carte di fidelizzazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4680
      TabIndex        =   10
      Top             =   840
      Width           =   3735
   End
   Begin VB.Label lblTessereTifoso 
      Alignment       =   2  'Center
      Caption         =   "Tessere del tifoso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   360
      TabIndex        =   9
      Top             =   840
      Width           =   3855
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione dei Tipi Supporto Digitali all'Organizzazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   7155
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9420
      TabIndex        =   5
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneTipiSupportoDigitali"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nomeO As String)
    nomeOrganizzazioneSelezionata = nomeO
End Sub

Private Sub chkTessereTifoso_Click()
    Call SelezionaTipoSupportoSIAE(lstTessereTifoso, chkTessereTifoso.Value)
End Sub

Private Sub chkCarteFidelizzazione_Click()
    Call SelezionaTipoSupportoSIAE(lstCarteFidelizzazione, chkCarteFidelizzazione.Value)
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub SelezionaTipoSupportoSIAE(lst As listBox, check As Integer)
    Dim i As Long
    Dim numeroElementi As Long
    
    numeroElementi = lst.ListCount
    If check = vbChecked Then
        For i = 1 To numeroElementi
            lst.Selected(i - 1) = True
        Next i
        lst.Enabled = False
    Else
        For i = 1 To numeroElementi
            lst.Selected(i - 1) = False
        Next i
        lst.Enabled = True
    End If
End Sub

Private Sub InizializzaListaTipiSupporto(tipoSupporto As Integer, lst As listBox, check As Integer)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    lst.Clear
    
    Call ApriConnessioneBD
    
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.CODICE || ' - ' || TS.NOME AS DESCR, TSCO.IDORGANIZZAZIONE" & _
        " FROM TIPOSUPPORTO TS, TIPOSUPPORTOSIAE TSS, TSDIGCONSENTITOPERORG TSCO" & _
        " WHERE TS.IDTIPOSUPPORTOSIAE = TSS.IDTIPOSUPPORTOSIAE" & _
        " AND TSS.IDTIPOSUPPORTOSIAE = " & tipoSupporto & _
        " AND TS.IDTIPOSUPPORTO = TSCO.IDTIPOSUPPORTO(+)" & _
        " AND TSCO.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata & _
        " ORDER BY DESCR"
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 1
        While Not rec.EOF
            lst.AddItem rec("DESCR")
            lst.ItemData(i - 1) = rec("IDTIPOSUPPORTO")
            
            If check = vbChecked Then
                lst.Selected(i - 1) = True
            ElseIf Not (IsNull(rec("IDORGANIZZAZIONE"))) Then
                lst.Selected(i - 1) = True
            Else
                lst.Selected(i - 1) = False
            End If
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
    
    If check = vbChecked Then
        lst.Enabled = False
    Else
        lst.Enabled = True
    End If

End Sub

Public Sub InizializzaTessereTifoso()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(*) CONT FROM TSDIGCONSENTITOPERORG WHERE IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = 0 Then
            chkTessereTifoso.Value = vbUnchecked
        Else
            chkTessereTifoso.Value = vbChecked
        End If
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
    
    Call InizializzaListaTipiSupporto(IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO, lstTessereTifoso, chkTessereTifoso.Value)
End Sub

Public Sub InizializzaCarteFidelizzazione()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(*) CONT FROM TSDIGCONSENTITOPERORG WHERE IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = 0 Then
            chkCarteFidelizzazione.Value = vbUnchecked
        Else
            chkCarteFidelizzazione.Value = vbChecked
        End If
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
    
    Call InizializzaListaTipiSupporto(IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE, lstCarteFidelizzazione, chkCarteFidelizzazione.Value)
End Sub

Public Sub InizializzaPassbook()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    Call ApriConnessioneBD
    
    sql = "SELECT COUNT(*) CONT FROM TSDIGCONSENTITOPERORG WHERE IDTIPOSUPPORTOSIAE = " & IDTIPOSUPPORTOSIAE_PASSBOOK & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        If rec("CONT") = 0 Then
            chkPassbook.Value = vbUnchecked
        Else
            chkPassbook.Value = vbChecked
        End If
    End If
    rec.Close
        
    Call ChiudiConnessioneBD
End Sub

Public Sub InizializzaListe()
    Call InizializzaTessereTifoso
    Call InizializzaCarteFidelizzazione
    Call InizializzaPassbook
End Sub

Public Sub Init()
    Call InizializzaListe
    
    Call AggiornaAbilitazioneControlli
    
    Me.Show (vbModal)
End Sub

Private Sub Annulla()
    Call InizializzaListe
End Sub

Private Sub Conferma()
    Dim sql As String
    Dim i As Long
    Dim numeroRecord As Long
    Dim numeroElementi As Long
    Dim risposta As VbMsgBoxResult
    
    risposta = MsgBox("Attenzione: verranno cancellate anche informazioni non pi� valide in tutti i prodotti dell'organizzazione. Continuare?", vbYesNo)
    If risposta = vbYes Then
        
        Call ApriConnessioneBD
        
        SETAConnection.BeginTrans
        
        sql = "DELETE FROM TSDIGCONSENTITOPERORG WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
        SETAConnection.Execute sql, numeroRecord, adCmdText
        
        If chkTessereTifoso.Value = vbChecked Then
            sql = "INSERT INTO TSDIGCONSENTITOPERORG (IDORGANIZZAZIONE, IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO)" & _
                " VALUES (" & idOrganizzazioneSelezionata & ", " & IDTIPOSUPPORTOSIAE_TESSERA_TIFOSO & ", NULL)"
            SETAConnection.Execute sql, numeroRecord, adCmdText
        Else
            numeroElementi = lstTessereTifoso.ListCount
            For i = 1 To numeroElementi
                If lstTessereTifoso.Selected(i - 1) = True Then
                    sql = "INSERT INTO TSDIGCONSENTITOPERORG (IDORGANIZZAZIONE, IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO)" & _
                        " VALUES (" & idOrganizzazioneSelezionata & ", NULL, " & lstTessereTifoso.ItemData(i - 1) & ")"
                    SETAConnection.Execute sql, numeroRecord, adCmdText
                End If
            Next i
        End If
            
        If chkCarteFidelizzazione.Value = vbChecked Then
            sql = "INSERT INTO TSDIGCONSENTITOPERORG (IDORGANIZZAZIONE, IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO)" & _
                " VALUES (" & idOrganizzazioneSelezionata & ", " & IDTIPOSUPPORTOSIAE_CARTA_FIDELIZZAZIONE & ", NULL)"
            SETAConnection.Execute sql, numeroRecord, adCmdText
        Else
            numeroElementi = lstCarteFidelizzazione.ListCount
            For i = 1 To numeroElementi
                If lstCarteFidelizzazione.Selected(i - 1) = True Then
                    sql = "INSERT INTO TSDIGCONSENTITOPERORG (IDORGANIZZAZIONE, IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO)" & _
                        " VALUES (" & idOrganizzazioneSelezionata & ", NULL, " & lstCarteFidelizzazione.ItemData(i - 1) & ")"
                    SETAConnection.Execute sql, numeroRecord, adCmdText
                End If
            Next i
        End If
     
        If chkPassbook.Value = vbChecked Then
            sql = "INSERT INTO TSDIGCONSENTITOPERORG (IDORGANIZZAZIONE, IDTIPOSUPPORTOSIAE, IDTIPOSUPPORTO)" & _
                " VALUES (" & idOrganizzazioneSelezionata & ", " & IDTIPOSUPPORTOSIAE_PASSBOOK & ", NULL)"
            SETAConnection.Execute sql, numeroRecord, adCmdText
        End If
            
' cancellazione sui prodotti di tipi non pi� validi

        sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
            " WHERE IDTIPOSUPPORTOSIAE IN" & _
            " (" & _
            " SELECT DISTINCT TSC.IDTIPOSUPPORTOSIAE" & _
            " FROM PRODOTTO P, TSDIGCONSENTITOPERSUPERAREA TSC," & _
            " (" & _
            " SELECT IDTIPOSUPPORTOSIAE" & _
            " FROM TSDIGCONSENTITOPERORG" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " ) T" & _
            " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND P.IDPRODOTTO = TSC.IDPRODOTTO" & _
            " AND TSC.IDTIPOSUPPORTOSIAE = T.IDTIPOSUPPORTOSIAE(+)" & _
            " AND T.IDTIPOSUPPORTOSIAE IS NULL" & _
            " )" & _
            " AND IDPRODOTTO IN" & _
            " (" & _
            " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " )"
        SETAConnection.Execute sql, numeroRecord, adCmdText

        sql = "DELETE FROM TSDIGCONSENTITOPERSUPERAREA" & _
            " WHERE IDTIPOSUPPORTO IN" & _
            " (" & _
            " SELECT DISTINCT TSC.IDTIPOSUPPORTO" & _
            " FROM PRODOTTO P, TSDIGCONSENTITOPERSUPERAREA TSC," & _
            " (" & _
            " SELECT IDTIPOSUPPORTO" & _
            " FROM TSDIGCONSENTITOPERORG" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " UNION" & _
            " SELECT TS.IDTIPOSUPPORTO" & _
            " FROM TSDIGCONSENTITOPERORG TSDCPO, TIPOSUPPORTO TS" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND TSDCPO.IDTIPOSUPPORTOSIAE = TS.IDTIPOSUPPORTOSIAE" & _
            " ) T" & _
            " WHERE P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND P.IDPRODOTTO = TSC.IDPRODOTTO" & _
            " AND TSC.IDTIPOSUPPORTO = T.IDTIPOSUPPORTO(+)" & _
            " AND T.IDTIPOSUPPORTO IS NULL" & _
            " )" & _
            " AND IDPRODOTTO IN" & _
            " (" & _
            " SELECT IDPRODOTTO FROM PRODOTTO WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            ")"
        SETAConnection.Execute sql, numeroRecord, adCmdText

        SETAConnection.CommitTrans
        
        Call ChiudiConnessioneBD

        Call InizializzaListe
    End If
End Sub

