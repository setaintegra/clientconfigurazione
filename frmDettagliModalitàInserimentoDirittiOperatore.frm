VERSION 5.00
Begin VB.Form frmDettagliModalitàInserimentoDirittiOperatore 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Modalità inserimento diritti operatore"
   ClientHeight    =   1890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4215
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1890
   ScaleWidth      =   4215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraImporta 
      Caption         =   "Importa:"
      Height          =   975
      Left            =   180
      TabIndex        =   2
      Top             =   120
      Width           =   2715
      Begin VB.OptionButton optImportaConservandoPreesistenti 
         Caption         =   "conservando preesistenti"
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   240
         Width           =   2235
      End
      Begin VB.OptionButton optImportaEliminandoPreesistenti 
         Caption         =   "eliminando preesistenti"
         Height          =   195
         Left            =   180
         TabIndex        =   3
         Top             =   540
         Width           =   2235
      End
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2400
      TabIndex        =   1
      Top             =   1380
      Width           =   1035
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   720
      TabIndex        =   0
      Top             =   1380
      Width           =   1035
   End
End
Attribute VB_Name = "frmDettagliModalitàInserimentoDirittiOperatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private idLayoutSupportoSelezionato As Long
'Private nomeLayoutSupportoSelezionato As String
'Private codiceLayoutSupportoSelezionato As String
'Private labelLayoutSupportoSelezionato As String
'Private idOrganizzazioneSelezionata As Long

Private importaConservandoPreesistenti As Boolean
Private importaEliminandoPreesistenti As Boolean

Private exitCode As ExitCodeEnum
Private internalEvent As Boolean

Public Sub Init()
'    Dim listaId As Collection
'    Dim listaLabel As Collection
'    Dim sql As String
'
'    sql = "SELECT LS.IDLAYOUTSUPPORTO ID, LS.CODICE COD, LS.NOME NOME" & _
'        " FROM LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL WHERE" & _
'        " (LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO) AND" & _
'        " (OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") AND" & _
'        " (OTL.IDTIPOSUPPORTO = " & idTipoSupportoSelezionato & ")" & _
'        " ORDER BY COD"
'    Call CaricaValoriCombo(cmbLayoutSupporto, sql, "COD", "NOME")
    
    importaConservandoPreesistenti = False
    importaEliminandoPreesistenti = False
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = importaConservandoPreesistenti Or importaEliminandoPreesistenti
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Function GetImportaConservandoPreesistenti() As Boolean
    GetImportaConservandoPreesistenti = importaConservandoPreesistenti
End Function

Public Function GetImportaEliminandoPreesistenti() As Boolean
    GetImportaEliminandoPreesistenti = importaEliminandoPreesistenti
End Function

Private Sub optImportaConservandoPreesistenti_Click()
    If Not internalEvent Then
        importaConservandoPreesistenti = True
        importaEliminandoPreesistenti = False
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub optImportaEliminandoPreesistenti_Click()
    If Not internalEvent Then
        importaConservandoPreesistenti = False
        importaEliminandoPreesistenti = True
        Call AggiornaAbilitazioneControlli
    End If
End Sub

