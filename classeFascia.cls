VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "classeFascia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idFascia As Long
Public indiceDiPreferibilita As Long
Public collSequenze As New Collection
Public collPosti As New Collection
'
'Public Sub aggiungiPosto(posto As clsPosto) 'struttura dati temporanea; non va in DB
'    collPosti.Add posto
'End Sub
'
'Public Function ottieniSequenzaDaIndiceDiPreferibilita(ordine As Integer) As classeSequenza
'    Dim s As classeSequenza
'
'    For Each s In collSequenze
'        If s.ordineInFascia = ordine Then
'            Set ottieniSequenzaDaIndiceDiPreferibilita = s
'            ' come si esce dai foreach?
'        End If
'    Next s
'End Function

Public Sub creaSequenza(ordine As Long, listaPosti As Collection)
    Dim sequenza As classeSequenza
    Dim i As Integer
    
    Set sequenza = New classeSequenza
    sequenza.ordineInFascia = ordine
    Set sequenza.collPosti = listaPosti
    Call collSequenze.Add(sequenza, ChiaveId(sequenza.ordineInFascia))
    For i = 1 To sequenza.collPosti.count
        Call EliminaPostoDaLista(collPosti, sequenza.collPosti(i))
    Next i
End Sub

Private Sub EliminaElementoDaLista_OLD(lista As Collection, posto As clsPosto)
    Dim i As Integer
    Dim trovato As Boolean
    
    i = 1
    trovato = False
    While i <= lista.count And Not trovato
        If posto.idPosto = lista(i).idPosto Then
            trovato = True
            lista.Remove (i)
        End If
        i = i + 1
    Wend
End Sub

Private Sub EliminaPostoDaLista(lista As Collection, posto As clsPosto)

On Error Resume Next
    Call lista.Remove(ChiaveId(posto.idPosto))
End Sub

Public Function IsPostoAssociatoAFascia_old(idP As Long) As Boolean
    Dim sequenza As classeSequenza
    Dim trovato As Boolean
    Dim iSequenza As Integer
    Dim iFascia As Integer
    Dim iPosto As Integer
    
    iPosto = 1
    trovato = False
    While iPosto <= collPosti.count And Not trovato
        If idP = collPosti(iPosto).idPosto Then
            trovato = True
        End If
        iPosto = iPosto + 1
    Wend
        
'    IsPostoAssociatoAFascia = Not trovato
    IsPostoAssociatoAFascia_old = trovato
End Function

Public Sub EliminaSequenzaDaIndiceDiPreferibilita_old(indice As Long)
    Dim s As classeSequenza
    Dim i As Integer
    Dim index As Integer
    Dim j As Integer
    Dim chiave As String
    
    i = 1
    For Each s In collSequenze
        If s.ordineInFascia = indice Then
            index = i
            For j = 1 To s.collPosti.count
'                Call collPosti.Add(s.collPosti(j))
                chiave = ChiaveId(s.collPosti(j).idPosto)
                Call collPosti.Add(s.collPosti(j), chiave)
            Next j
        End If
        i = i + 1
    Next s
    collSequenze.Remove index
End Sub

Public Sub EliminaSequenzaDaIndiceDiPreferibilita(ordine As Long)
    Dim s As classeSequenza
    Dim p As clsPosto
    
On Error Resume Next
    Set s = collSequenze.Item(ChiaveId(ordine))
    For Each p In s.collPosti
        Call collPosti.Add(p, ChiaveId(p.idPosto))
    Next p
    Call collSequenze.Remove(ChiaveId(ordine))
End Sub

Public Function OrdineInFasciaSuccessivo() As Integer
    Dim sequenza As classeSequenza
    Dim maxOrd As Integer
    
    maxOrd = 0
    For Each sequenza In collSequenze
        If sequenza.ordineInFascia > maxOrd Then
            maxOrd = sequenza.ordineInFascia
        End If
    Next sequenza
    OrdineInFasciaSuccessivo = maxOrd + 1
End Function

Public Function IsPostoAssociatoAFascia(idP As Long) As Boolean
    
On Error GoTo gestioneErrori

    IsPostoAssociatoAFascia = True
    Call collPosti.Item(ChiaveId(idP))

    Exit Function
gestioneErrori:
    IsPostoAssociatoAFascia = False
End Function

