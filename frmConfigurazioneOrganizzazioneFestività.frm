VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneOrganizzazioneFestività 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   1800
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   6180
      Width           =   4635
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10680
      TabIndex        =   8
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   10
      Top             =   4860
      Width           =   5295
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   9
      Top             =   240
      Width           =   3255
   End
   Begin MSComCtl2.DTPicker dtpData 
      Height          =   315
      Left            =   120
      TabIndex        =   4
      Top             =   6180
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      DateIsNull      =   -1  'True
      Format          =   53411841
      CurrentDate     =   37606
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneFestività 
      Height          =   330
      Left            =   240
      Top             =   4080
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneFestività 
      Height          =   3915
      Left            =   120
      TabIndex        =   12
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   1800
      TabIndex        =   18
      Top             =   5940
      Width           =   1575
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Festività"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblData 
      Caption         =   "Data"
      Height          =   195
      Left            =   120
      TabIndex        =   16
      Top             =   5940
      Width           =   1395
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   14
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   13
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneFestività"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private dataRecordSelezionato As Date
Private nuovaDataRecordSelezionato As Date
Private descrizioneRecordSelezionato As String
Private nomeOrganizzazioneSelezionata As String

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()

'    lblInfo1.Caption = "Organizzazione"
'    txtInfo1.Text = nomeOrganizzazioneSelezionata
'    txtInfo1.Enabled = False

'    dgrConfigurazioneSpettacolo.Caption = "FESTIVITA' CONFIGURATE"

    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneFestività.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtDescrizione.Text = ""
        dtpData.Enabled = False
        txtDescrizione.Enabled = False
        lblData.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False

    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneFestività.Enabled = False
        dtpData.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblData.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = True
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneFestività.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtDescrizione.Text = ""
        dtpData.Enabled = False
        txtDescrizione.Enabled = False
        lblData.Enabled = False
        lblDescrizione.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    End If
    
End Sub

Private Sub Controlli_Init()

    lblInfo1.Caption = "Spettacolo"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    lblData.Caption = "Data (GG/MM/AAAA)"

    dgrConfigurazioneFestività.Caption = "FESTIVITA' CONFIGURATE"

    dgrConfigurazioneFestività.Enabled = True
    lblOperazioneInCorso.Caption = ""
    cmdInserisciNuovo.Enabled = True
    cmdInserisciDaSelezione.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdModifica.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato > idNessunElementoSelezionato)
    cmdConferma.Enabled = False
    cmdAnnulla.Enabled = False

End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata

    If ValoriCampiOK Then
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_FESTIVITA, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneFestività_Init
                Call SelezionaElementoSuGriglia(dataRecordSelezionato)
                Call dgrConfigurazioneFestività_Init
            Case ASG_INSERISCI_DA_SELEZIONE
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_FESTIVITA, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneFestività_Init
                Call SelezionaElementoSuGriglia(dataRecordSelezionato)
                Call dgrConfigurazioneFestività_Init
            Case ASG_MODIFICA
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_ORGANIZZAZIONE, CCDA_FESTIVITA, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneFestività_Init
                Call SelezionaElementoSuGriglia(dataRecordSelezionato)
                Call dgrConfigurazioneFestività_Init
            Case ASG_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_FESTIVITA, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneFestività_Init
                Call SetDataRecordSelezionato(dataNulla)
                Call SelezionaElementoSuGriglia(dataNulla)
                Call dgrConfigurazioneFestività_Init
        End Select
        
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetDataRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetDataRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call SetDataRecordSelezionato(dataNulla)
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub
'
'Private Sub SelezionaElementoSuGriglia(id As Long)
'    Dim rec As ADODB.Recordset
'    Dim internalEventOld As Boolean
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    Set rec = adcConfigurazioneSpettacolo.Recordset
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        Do While Not rec.EOF
'            If id = rec("ID") Then
'                Exit Do
'            End If
'            rec.MoveNext
'        Loop
'    End If
'
'    internalEvent = internalEventOld
'
'End Sub

Private Sub SelezionaElementoSuGriglia(dataFestività As Date)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneFestività.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If dataFestività = rec("DATA") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetDataRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneFestività_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetDataRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetDataRecordSelezionato(dataNulla)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneFestività_Init
    Call SelezionaElementoSuGriglia(dataNulla)
    Call dgrConfigurazioneFestività_Init
    Call dtpData_Init
    Call Me.Show(vbModal)

End Sub
'
'Private Sub GetIdRecordSelezionato()
'    Dim rec As ADODB.Recordset
'
'    Set rec = adcConfigurazioneSpettacolo.Recordset
'    If Not (rec.BOF) Then
'        If rec.EOF Then
'            rec.MoveFirst
'        End If
'        idRecordSelezionato = rec("ID").Value
'    Else
'        idRecordSelezionato = idNessunElementoSelezionato
'    End If
'
'    Call AggiornaAbilitazioneControlli
'End Sub

Private Sub GetDataRecordSelezionato()
    Dim rec As ADODB.Recordset

    Set rec = adcConfigurazioneFestività.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        dataRecordSelezionato = rec("DATA").Value
    Else
        dataRecordSelezionato = dataNulla
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub
'
'Public Sub SetIdRecordSelezionato(id As Long)
'    idRecordSelezionato = id
'End Sub

Public Sub SetDataRecordSelezionato(dataFestività As Date)
    dataRecordSelezionato = dataFestività
End Sub

Private Sub adcConfigurazioneFestività_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazioneFestività
    
    sql = "SELECT DATA ""Data"","
    sql = sql & " DESCRIZIONE ""Descrizione"""
    sql = sql & " FROM FESTIVITALOCALE"
    sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " ORDER BY ""Data"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneFestività.dataSource = d

    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT DESCRIZIONE"
    sql = sql & " FROM FESTIVITALOCALE"
    sql = sql & " WHERE DATA = " & SqlDateValue(dataRecordSelezionato)
    sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean

    internalEventOld = internalEvent
    internalEvent = True

    dtpData.Value = dataRecordSelezionato
    txtDescrizione.Text = Trim(descrizioneRecordSelezionato)

    internalEvent = internalEventOld

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim festivitàEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    festivitàEliminabile = True
    
'    If Not IsRecordEliminabile("IDRAPPRESENTAZIONE", "PRODOTTO_RAPPRESENTAZIONE", CStr(idRecordSelezionato)) Then
'        Call listaTabelleCorrelate.Add("PRODOTTO_RAPPRESENTAZIONE")
'        rappresentazioneEliminabile = False
'    End If
    
    If festivitàEliminabile Then
        sql = "DELETE FROM FESTIVITALOCALE"
        sql = sql & " WHERE DATA = " & SqlDateValue(dataRecordSelezionato)
        sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
        SETAConnection.Execute sql, , adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Festività selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub dgrConfigurazioneFestività_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneFestività
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 2
'    g.Columns(0).Visible = False
    g.Columns(0).Width = (dimensioneGrid / numeroCampi)
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformità As Collection

On Error Resume Next

    ValoriCampiOK = True
    Set listaNonConformità = New Collection
    nuovaDataRecordSelezionato = FormatDateTime(dtpData.Value, vbShortDate)
    If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
        If ViolataUnicità("FESTIVITALOCALE", "DATA = " & SqlDateValue(nuovaDataRecordSelezionato), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
            "", "", "") Then
            ValoriCampiOK = False
            Call listaNonConformità.Add("- il valore data = " & SqlDateValue(nuovaDataRecordSelezionato) & _
                " è già presente in DB per la stessa organizzazione;")
        End If
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
    If listaNonConformità.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitàCampi", ArgomentoMessaggio(listaNonConformità))
    End If

End Function

Private Sub dtpData_Init()
    dtpData.Value = Now
End Sub

Private Sub dtpData_Click()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = " INSERT INTO FESTIVITALOCALE ("
    sql = sql & " DATA, IDORGANIZZAZIONE, DESCRIZIONE) "
    sql = sql & " VALUES ("
    sql = sql & SqlDateValue(nuovaDataRecordSelezionato) & ", "
    sql = sql & idOrganizzazioneSelezionata & ", "
    sql = sql & SqlStringValue(descrizioneRecordSelezionato)
    sql = sql & " )"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetDataRecordSelezionato(dataRecordSelezionato)
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long
'    Dim dataOld As Date

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'    dataOld
    sql = "UPDATE FESTIVITALOCALE SET"
    sql = sql & " DATA = " & SqlDateTimeValue(nuovaDataRecordSelezionato) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato)
    sql = sql & " WHERE DATA = " & SqlDateValue(dataRecordSelezionato)
    sql = sql & " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    SETAConnection.Execute sql, n, adCmdText
        
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub









