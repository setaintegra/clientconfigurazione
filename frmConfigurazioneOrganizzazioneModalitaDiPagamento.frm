VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneOrganizzazioneModalitaDiPagamento 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbTipoModalitāDiPagamentoTracciabile 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   6600
      Width           =   3375
   End
   Begin VB.ListBox lstModalitāDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   4140
      MultiSelect     =   2  'Extended
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   6540
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaModalitāSelezionate 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   7500
      Width           =   435
   End
   Begin VB.CommandButton cmdModalitāDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   7200
      Width           =   435
   End
   Begin VB.CommandButton cmdModalitāSelezionata 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   6900
      Width           =   435
   End
   Begin VB.ListBox lstModalitāSelezionate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   8220
      MultiSelect     =   2  'Extended
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   6540
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaModalitāDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   6600
      Width           =   435
   End
   Begin VB.ListBox lstTipiTerminaleDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   4140
      MultiSelect     =   2  'Extended
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   5100
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaTipiTerminaleSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   6000
      Width           =   435
   End
   Begin VB.CommandButton cmdTipoTerminaleDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   5700
      Width           =   435
   End
   Begin VB.CommandButton cmdTipoTerminaleSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   5400
      Width           =   435
   End
   Begin VB.ListBox lstTipiTerminaleSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   8220
      MultiSelect     =   2  'Extended
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   5100
      Width           =   3555
   End
   Begin VB.CommandButton cmdSvuotaTipiTerminaleDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   5100
      Width           =   435
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   4
      Top             =   4800
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   5460
      Width           =   3675
   End
   Begin VB.CheckBox chkIVAPreassolta 
      Caption         =   "IVA preassolta"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   7080
      Width           =   2475
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   23
      Top             =   240
      Width           =   3255
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   20
      Top             =   3540
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   16
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   17
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneOrganizzazioneModalitaDiPagamento 
      Height          =   330
      Left            =   6360
      Top             =   180
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneOrganizzazioneModalitaDiPagamento 
      Height          =   2595
      Left            =   120
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   4577
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblTipoModalitāDiPagamento 
      Caption         =   "Tipo modalitā di pagamento tracciabile"
      Height          =   255
      Left            =   120
      TabIndex        =   38
      Top             =   6360
      Width           =   2775
   End
   Begin VB.Label lblModalitāSelezionate 
      Alignment       =   2  'Center
      Caption         =   "Modalitā standard selezionate"
      Height          =   195
      Left            =   8220
      TabIndex        =   37
      Top             =   6300
      Width           =   3555
   End
   Begin VB.Label lblModalitāDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Modalitā standard disponibili"
      Height          =   195
      Left            =   4140
      TabIndex        =   36
      Top             =   6300
      Width           =   3555
   End
   Begin VB.Label lblNota 
      Caption         =   "NOTA:ad ogni tipo terminale puō essere associata una sola modalitā di default"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   195
      Left            =   4140
      TabIndex        =   29
      Top             =   4560
      Width           =   6795
   End
   Begin VB.Label lblTipiTerminaleSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Modalitā di default per"
      Height          =   195
      Left            =   8220
      TabIndex        =   28
      Top             =   4860
      Width           =   3555
   End
   Begin VB.Label lblTipiTerminaleDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Tipi Terminale disponibili"
      Height          =   195
      Left            =   4140
      TabIndex        =   27
      Top             =   4860
      Width           =   3555
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   4560
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   5220
      Width           =   1575
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   24
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   22
      Top             =   3300
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   3300
      Width           =   1815
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Modalitā di Pagamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazioneModalitaDiPagamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idModalitaPagamentoTracciabileSelezionata As Long
Private idTipoTerminaleSelezionato As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private listaModalitāDisponibili As Collection
Private listaModalitāSelezionate As Collection
Private listaTipiTerminaleDisponibili As Collection
Private listaTipiTerminaleSelezionati As Collection
Private listaAppoggioTipoTerminale As Collection
'Private listaCampiValoriUnici As Collection
Private nomeTabellaTemporanea As String
Private isRecordEditabile As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private origineModalitā As OrigineCaratteristicaEnum
Private IVAPreassolta As ValoreBooleanoEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazioneOrganizzazioneModalitaDiPagamento.Caption = "MODALITA' DI PAGAMENTO CONFIGURATE"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            origineModalitā = OC_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneModalitaDiPagamento.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbTipoModalitāDiPagamentoTracciabile.Clear
        Call lstModalitāDisponibili.Clear
        Call lstModalitāSelezionate.Clear
        Call lstTipiTerminaleDisponibili.Clear
        Call lstTipiTerminaleSelezionati.Clear
        chkIVAPreassolta.Value = VB_FALSO
        cmbTipoModalitāDiPagamentoTracciabile.Enabled = False
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        chkIVAPreassolta.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblTipiTerminaleDisponibili.Enabled = False
        lblTipiTerminaleSelezionati.Enabled = False
        lblTipoModalitāDiPagamento.Enabled = False
        cmdModalitāSelezionata.Enabled = True
        cmdModalitāDisponibile.Enabled = True
        cmdSvuotaModalitāSelezionate.Enabled = True
        cmdSvuotaModalitāDisponibili.Enabled = True
        cmdTipoTerminaleSelezionato.Enabled = False
        cmdTipoTerminaleDisponibile.Enabled = False
        cmdSvuotaTipiTerminaleSelezionati.Enabled = False
        cmdSvuotaTipiTerminaleDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO And _
            origineModalitā = OC_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneModalitaDiPagamento.Enabled = False
        cmbTipoModalitāDiPagamentoTracciabile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblDescrizione.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkIVAPreassolta.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstTipiTerminaleDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lstTipiTerminaleSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipiTerminaleDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipiTerminaleSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblTipoModalitāDiPagamento.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        Call lstModalitāDisponibili.Clear
        Call lstModalitāSelezionate.Clear
        lstModalitāDisponibili.Enabled = False
        lstModalitāSelezionate.Enabled = False
        lblModalitāDisponibili.Enabled = False
        lblModalitāSelezionate.Enabled = False
        cmdModalitāSelezionata.Enabled = False
        cmdModalitāDisponibile.Enabled = False
        cmdSvuotaModalitāSelezionate.Enabled = False
        cmdSvuotaModalitāDisponibili.Enabled = False
        cmdTipoTerminaleSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdTipoTerminaleDisponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaTipiTerminaleSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdSvuotaTipiTerminaleDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtNome.Text) <> ""
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            origineModalitā <> OC_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneModalitaDiPagamento.Enabled = False
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbTipoModalitāDiPagamentoTracciabile.Clear
        chkIVAPreassolta.Value = VB_FALSO
        Call lstTipiTerminaleDisponibili.Clear
        Call lstTipiTerminaleSelezionati.Clear
        cmbTipoModalitāDiPagamentoTracciabile.Enabled = False
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        chkIVAPreassolta.Enabled = False
        lstTipiTerminaleDisponibili.Enabled = False
        lstTipiTerminaleSelezionati.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        lblTipiTerminaleDisponibili.Enabled = False
        lblTipiTerminaleSelezionati.Enabled = False
        lblTipoModalitāDiPagamento.Enabled = False
        cmdModalitāSelezionata.Enabled = True
        cmdModalitāDisponibile.Enabled = True
        cmdSvuotaModalitāSelezionate.Enabled = True
        cmdSvuotaModalitāDisponibili.Enabled = True
        cmdTipoTerminaleSelezionato.Enabled = False
        cmdTipoTerminaleDisponibile.Enabled = False
        cmdSvuotaTipiTerminaleSelezionati.Enabled = False
        cmdSvuotaTipiTerminaleDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (lstModalitāSelezionate.ListCount > 0)
        cmdAnnulla.Enabled = True
        Select Case origineModalitā
            Case OC_STANDARD
                lblOperazioneInCorso.Caption = "Operazione in corso:"
                lblOperazione.Caption = "inserimento nuovo record"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO And _
            origineModalitā = OC_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazioneModalitaDiPagamento.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        Call cmbTipoModalitāDiPagamentoTracciabile.Clear
        chkIVAPreassolta.Value = VB_FALSO
        Call lstTipiTerminaleDisponibili.Clear
        Call lstTipiTerminaleSelezionati.Clear
        Call lstModalitāSelezionate.Clear
        lstModalitāDisponibili.Enabled = True
        lstModalitāSelezionate.Enabled = True
        lblModalitāDisponibili.Enabled = True
        lblModalitāSelezionate.Enabled = True
        lstTipiTerminaleDisponibili.Enabled = False
        lstTipiTerminaleSelezionati.Enabled = False
        lblTipiTerminaleDisponibili.Enabled = False
        lblTipiTerminaleSelezionati.Enabled = False
        lblTipoModalitāDiPagamento.Enabled = False
        txtNome.Enabled = False
        txtDescrizione.Enabled = False
        cmbTipoModalitāDiPagamentoTracciabile.Enabled = False
        chkIVAPreassolta.Enabled = False
        lblNome.Enabled = False
        lblDescrizione.Enabled = False
        cmdModalitāSelezionata.Enabled = True
        cmdModalitāDisponibile.Enabled = True
        cmdSvuotaModalitāSelezionate.Enabled = True
        cmdSvuotaModalitāDisponibili.Enabled = True
        cmdTipoTerminaleSelezionato.Enabled = False
        cmdTipoTerminaleDisponibile.Enabled = False
        cmdSvuotaTipiTerminaleSelezionati.Enabled = False
        cmdSvuotaTipiTerminaleDisponibili.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub cmbTipoModalitāDiPagamentoTracciabile_Click()
'    If Not internalEvent Then
        idModalitaPagamentoTracciabileSelezionata = cmbTipoModalitāDiPagamentoTracciabile.ItemData(cmbTipoModalitāDiPagamentoTracciabile.ListIndex)
'        Call CaricaTipiTariffaSiae
'    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetOrigineModalitā(OC_NON_SPECIFICATO)
    Call CaricaValoriLstModalitāDisponibili
    Call AggiornaAbilitazioneControlli
    Set listaModalitāSelezionate = New Collection
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        "; IDMODALITAPAGAMENTO = " & idRecordSelezionato

    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoOrganizzazione = TSO_ATTIVA Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If ValoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_MODALITA_DI_PAGAMENTO, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetOrigineModalitā(OC_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                        Case ASG_INSERISCI_DA_SELEZIONE
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_MODALITA_DI_PAGAMENTO, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetOrigineModalitā(OC_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_ORGANIZZAZIONE, CCDA_MODALITA_DI_PAGAMENTO, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetOrigineModalitā(OC_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_MODALITA_DI_PAGAMENTO, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call SetOrigineModalitā(OC_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                    End Select
                    
                    Select Case origineModalitā
                        Case OC_STANDARD
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_MODALITA_DI_PAGAMENTO, stringaNota, idOrganizzazioneSelezionata)
                            Call SetOrigineModalitā(OC_NON_SPECIFICATO)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init
                    End Select
                End If
                
                Call CaricaValoriLstModalitāDisponibili
                Call AggiornaAbilitazioneControlli
                Set listaModalitāSelezionate = New Collection
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdModalitāDisponibile_Click()
    Call SpostaModalitāDaSelezionataADisponibile
End Sub

Private Sub SpostaModalitāDaSelezionataADisponibile()
    Call SpostaInLstModalitāDisponibili
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetOrigineModalitā(OC_STANDARD)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstModalitāDisponibili()
    Dim i As Integer
    Dim idModalitā As Long
    Dim modalitā As clsElementoLista
    Dim chiaveModalitā As String
    
    For i = 1 To lstModalitāSelezionate.ListCount
        If lstModalitāSelezionate.Selected(i - 1) Then
            idModalitā = lstModalitāSelezionate.ItemData(i - 1)
            chiaveModalitā = ChiaveId(idModalitā)
            Set modalitā = listaModalitāSelezionate.Item(chiaveModalitā)
            Call listaModalitāDisponibili.Add(modalitā, chiaveModalitā)
            Call listaModalitāSelezionate.Remove(chiaveModalitā)
        End If
    Next i
    Call lstModalitāDisponibili_Init
    Call lstModalitāSelezionate_Init
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDMODALITAPAGAMENTOTRACCIABILE ID, NOME"
    sql = sql & " FROM MODALITAPAGAMENTOTRACCIABILE"
    sql = sql & " WHERE IDMODALITAPAGAMENTOTRACCIABILE > 0"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetOrigineModalitā(OC_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoModalitāDiPagamentoTracciabile, sql, "NOME")
    Call CaricaValoriLstTipoTerminaleDisponibili
    Call CaricaValoriLstTipoTerminaleSelezionato
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Call EliminaTabellaAppoggioTipoTerminale
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    sql = "SELECT IDMODALITAPAGAMENTOTRACCIABILE ID, NOME"
    sql = sql & " FROM MODALITAPAGAMENTOTRACCIABILE"
    sql = sql & " WHERE IDMODALITAPAGAMENTOTRACCIABILE > 0"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetOrigineModalitā(OC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoModalitāDiPagamentoTracciabile, sql, "NOME")
    Call CaricaValoriLstTipoTerminaleDisponibili
    Call CaricaValoriLstTipoTerminaleSelezionato
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    isRecordEditabile = True
    sql = "SELECT IDMODALITAPAGAMENTOTRACCIABILE ID, NOME"
    sql = sql & " FROM MODALITAPAGAMENTOTRACCIABILE"
    sql = sql & " WHERE IDMODALITAPAGAMENTOTRACCIABILE > 0"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetOrigineModalitā(OC_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLstTipoTerminaleDisponibili
    Call CaricaValoriLstTipoTerminaleSelezionato
    Call AggiornaAbilitazioneControlli
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call CaricaValoriCombo(cmbTipoModalitāDiPagamentoTracciabile, sql, "NOME")
End Sub

Private Sub CaricaValoriLstModalitāDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveModalitā As String
    Dim modalitā As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaModalitāDisponibili = New Collection
    
    sql = "SELECT DISTINCT IDMODALITADIPAGAMENTO, NOME AS ""Nome"", DESCRIZIONE AS ""Descrizione""" & _
        " FROM MODALITADIPAGAMENTO WHERE" & _
        " IDORGANIZZAZIONE IS NULL" & _
        " ORDER BY ""Nome"""
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set modalitā = New clsElementoLista
            modalitā.idElementoLista = rec("IDMODALITADIPAGAMENTO").Value
            modalitā.nomeElementoLista = rec("NOME")
            modalitā.descrizioneElementoLista = rec("NOME")
            chiaveModalitā = ChiaveId(modalitā.idElementoLista)
            Call listaModalitāDisponibili.Add(modalitā, chiaveModalitā)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstModalitāDisponibili_Init
        
End Sub

Private Sub lstModalitāDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim modalitā As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstModalitāDisponibili.Clear

    If Not (listaModalitāDisponibili Is Nothing) Then
        i = 1
        For Each modalitā In listaModalitāDisponibili
            lstModalitāDisponibili.AddItem modalitā.descrizioneElementoLista
            lstModalitāDisponibili.ItemData(i - 1) = modalitā.idElementoLista
            i = i + 1
        Next modalitā
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstModalitāSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim modalitā As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstModalitāSelezionate.Clear

    If Not (listaModalitāSelezionate Is Nothing) Then
        i = 1
        For Each modalitā In listaModalitāSelezionate
            lstModalitāSelezionate.AddItem modalitā.descrizioneElementoLista
            lstModalitāSelezionate.ItemData(i - 1) = modalitā.idElementoLista
            i = i + 1
        Next modalitā
    End If
           
    internalEvent = internalEventOld

End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Public Sub SetOrigineModalitā(origine As OrigineCaratteristicaEnum)
    origineModalitā = origine
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneOrganizzazioneModalitaDiPagamento.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDMODALITAPAGAMENTOTRACCIABILE ID, NOME"
    sql = sql & " FROM MODALITAPAGAMENTOTRACCIABILE"
    sql = sql & " WHERE IDMODALITAPAGAMENTOTRACCIABILE > 0"
    sql = sql & " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetOrigineModalitā(OC_NON_SPECIFICATO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbTipoModalitāDiPagamentoTracciabile, sql, "NOME")
    Call CaricaValoriLstTipoTerminaleDisponibili
    Call CaricaValoriLstTipoTerminaleSelezionato
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModalitāSelezionata_Click()
    Call SpostaModalitāDaDisponibileASelezionata
End Sub

Private Sub SpostaModalitāDaDisponibileASelezionata()
    isRecordEditabile = True
    Call SpostaInLstModalitāSelezionate
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetOrigineModalitā(OC_STANDARD)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstModalitāSelezionate()
    Dim i As Integer
    Dim idModalitā As Long
    Dim modalitā As clsElementoLista
    Dim chiaveModalitā As String
    
    For i = 1 To lstModalitāDisponibili.ListCount
        If lstModalitāDisponibili.Selected(i - 1) Then
            idModalitā = lstModalitāDisponibili.ItemData(i - 1)
            chiaveModalitā = ChiaveId(idModalitā)
            Set modalitā = listaModalitāDisponibili.Item(chiaveModalitā)
            Call listaModalitāSelezionate.Add(modalitā, chiaveModalitā)
            Call listaModalitāDisponibili.Remove(chiaveModalitā)
        End If
    Next i
    Call lstModalitāDisponibili_Init
    Call lstModalitāSelezionate_Init
End Sub

Private Sub cmdSvuotaModalitāSelezionate_Click()
    Call SpostaTuttiInModalitāDisponibili
End Sub

Private Sub SpostaTuttiInModalitāDisponibili()
    Call SvuotaModalitāSelezionate
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetOrigineModalitā(OC_STANDARD)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
End Sub

Private Sub SvuotaModalitāSelezionate()
    Dim modalitā As clsElementoLista
    Dim chiaveModalitā As String
    
    For Each modalitā In listaModalitāSelezionate
        chiaveModalitā = ChiaveId(modalitā.idElementoLista)
        Call listaModalitāDisponibili.Add(modalitā, chiaveModalitā)
    Next modalitā
    Set listaModalitāSelezionate = Nothing
    Set listaModalitāSelezionate = New Collection
    
    Call lstModalitāDisponibili_Init
    Call lstModalitāSelezionate_Init
End Sub

Private Sub cmdSvuotaTipiTerminaleDisponibili_Click()
    Call SvuotaTipiTerminaleDisponibili
End Sub

Private Sub SvuotaTipiTerminaleDisponibili()
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    
    For Each tipo In listaTipiTerminaleDisponibili
        chiaveTipo = ChiaveId(tipo.idElementoLista)
        Call listaTipiTerminaleSelezionati.Add(tipo, chiaveTipo)
    Next tipo
    Set listaTipiTerminaleDisponibili = Nothing
    Set listaTipiTerminaleDisponibili = New Collection
    
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub cmdSvuotaTipiTerminaleSelezionati_Click()
    Call SvuotaTipiTerminaleSelezionati
End Sub

Private Sub SvuotaTipiTerminaleSelezionati()
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    
    For Each tipo In listaTipiTerminaleSelezionati
        chiaveTipo = ChiaveId(tipo.idElementoLista)
        Call listaTipiTerminaleDisponibili.Add(tipo, chiaveTipo)
    Next tipo
    Set listaTipiTerminaleSelezionati = Nothing
    Set listaTipiTerminaleSelezionati = New Collection
    
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub cmdTipoTerminaleDisponibile_Click()
    Call SpostaInLstTipiTerminaleDisponibili
End Sub

Private Sub SpostaInLstTipiTerminaleDisponibili()
    Dim i As Integer
    Dim idTipo As Long
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    
    For i = 1 To lstTipiTerminaleSelezionati.ListCount
        If lstTipiTerminaleSelezionati.Selected(i - 1) Then
            idTipo = lstTipiTerminaleSelezionati.ItemData(i - 1)
            chiaveTipo = ChiaveId(idTipo)
            Set tipo = listaTipiTerminaleSelezionati.Item(chiaveTipo)
            Call listaTipiTerminaleDisponibili.Add(tipo, chiaveTipo)
            Call listaTipiTerminaleSelezionati.Remove(chiaveTipo)
        End If
    Next i
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub cmdTipoTerminaleSelezionato_Click()
    Call SpostaInLstTipiTerminaleSelezionati
End Sub

Private Sub SpostaInLstTipiTerminaleSelezionati()
    Dim i As Integer
    Dim idTipo As Long
    Dim tipo As clsElementoLista
    Dim chiaveTipo As String
    
    For i = 1 To lstTipiTerminaleDisponibili.ListCount
        If lstTipiTerminaleDisponibili.Selected(i - 1) Then
            idTipo = lstTipiTerminaleDisponibili.ItemData(i - 1)
            chiaveTipo = ChiaveId(idTipo)
            Set tipo = listaTipiTerminaleDisponibili.Item(chiaveTipo)
            Call listaTipiTerminaleSelezionati.Add(tipo, chiaveTipo)
            Call listaTipiTerminaleDisponibili.Remove(chiaveTipo)
        End If
    Next i
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub dgrConfigurazioneOrganizzazioneModalitaDiPagamento_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetOrigineModalitā(OC_NON_SPECIFICATO)
    Call CreaTabellaAppoggioTipoTerminale
    Call adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init
    Call CaricaValoriLstModalitāDisponibili
    Set listaModalitāSelezionate = New Collection
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneOrganizzazioneModalitaDiPagamento.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneOrganizzazioneModalitaDiPagamento_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Call PopolaTabellaAppoggioTipoTerminale
    
    Set d = adcConfigurazioneOrganizzazioneModalitaDiPagamento
    
    sql = " SELECT M.IDMODALITADIPAGAMENTO ID, M.NOME ""Nome"", M.DESCRIZIONE ""Descrizione"",  "
    sql = sql & " DECODE(IVAPREASSOLTA, 0, 'NO', 1, 'SI') ""IVA preassolta"",  "
    sql = sql & " TMPT.NOME ""Tipo mod. tracc."", TMP.NOME ""Modalitā default per""  "
    sql = sql & " FROM MODALITADIPAGAMENTO M, " & nomeTabellaTemporanea & " TMP,"
    sql = sql & " MODALITAPAGAMENTOTRACCIABILE TMPT, "
    sql = sql & " ( "
    sql = sql & " SELECT OMT.IDMODALITADIPAGAMENTO  "
    sql = sql & " FROM ORGANIZZAZIONE_MODPAG_TIPOTERM OMT  "
    sql = sql & " WHERE OMT.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND OMT.IDTIPOTERMINALE = " & TT_TERMINALE_POS
    sql = sql & " ) OMT1 "
    sql = sql & " WHERE M.IDMODALITADIPAGAMENTO = TMP.IDMODALITA  "
    sql = sql & " AND M.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    sql = sql & " AND M.IDMODALITAPAGAMENTOTRACCIABILE = TMPT.IDMODALITAPAGAMENTOTRACCIABILE "
    sql = sql & " AND M.IDMODALITADIPAGAMENTO = OMT1.IDMODALITADIPAGAMENTO(+) "
    sql = sql & " AND OMT1.IDMODALITADIPAGAMENTO IS NULL "
    sql = sql & " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneOrganizzazioneModalitaDiPagamento.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim max As Long
    Dim i As Integer
    Dim idNuovaModalitā As Long
    Dim modalitā As clsElementoLista
    Dim tipoTerminale As clsElementoLista
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    Select Case origineModalitā
        Case OC_STANDARD
            For Each modalitā In listaModalitāSelezionate
                idNuovaModalitā = OttieniIdentificatoreDaSequenza("SQ_MODALITADIPAGAMENTO")
                sql = "INSERT INTO MODALITADIPAGAMENTO (IDMODALITADIPAGAMENTO, NOME,"
                sql = sql & " DESCRIZIONE, IDORGANIZZAZIONE, IVAPREASSOLTA,"
                sql = sql & " IDTIPOMODALITADIPAGAMENTO)"
                sql = sql & " (SELECT "
                sql = sql & idNuovaModalitā & ","
                sql = sql & " NOME, DESCRIZIONE,"
                sql = sql & idOrganizzazioneSelezionata & ","
                sql = sql & " IVAPREASSOLTA,"
                sql = sql & " IDTIPOMODALITADIPAGAMENTO)"
                sql = sql & " FROM MODALITADIPAGAMENTO WHERE"
                sql = sql & " IDMODALITADIPAGAMENTO = " & modalitā.idElementoLista & ")"
                SETAConnection.Execute sql, n, adCmdText
            Next modalitā
        Case Else
            idNuovaModalitā = OttieniIdentificatoreDaSequenza("SQ_MODALITADIPAGAMENTO")
            sql = "INSERT INTO MODALITADIPAGAMENTO (IDMODALITADIPAGAMENTO, NOME,"
            sql = sql & " DESCRIZIONE, IDORGANIZZAZIONE, IVAPREASSOLTA,"
            sql = sql & " IDMODALITAPAGAMENTOTRACCIABILE)"
            sql = sql & " VALUES ("
            sql = sql & idNuovaModalitā & ", "
            sql = sql & SqlStringValue(nomeRecordSelezionato) & ", "
            sql = sql & SqlStringValue(descrizioneRecordSelezionato) & ", "
            sql = sql & idOrganizzazioneSelezionata & ", "
            sql = sql & IVAPreassolta & ", "
            sql = sql & IIf(idModalitaPagamentoTracciabileSelezionata = idNessunElementoSelezionato, "NULL", idModalitaPagamentoTracciabileSelezionata) & ")"
            SETAConnection.Execute sql, n, adCmdText
    End Select
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("MODALITADIPAGAMENTO", "IDMODALITADIPAGAMENTO", idNuovaModalitā, TSR_NUOVO)
'    End If
    
'   INSERIMENTO IN TABELLA ORGANIZZAZIONE_MODPAG_TIPOTERM
    If Not (listaTipiTerminaleSelezionati Is Nothing) Then
        For Each tipoTerminale In listaTipiTerminaleSelezionati
            sql = "INSERT INTO ORGANIZZAZIONE_MODPAG_TIPOTERM"
            sql = sql & " (IDORGANIZZAZIONE, IDTIPOTERMINALE, IDMODALITADIPAGAMENTO)"
            sql = sql & " VALUES "
            sql = sql & "(" & idOrganizzazioneSelezionata & ", "
            sql = sql & tipoTerminale.idElementoLista & ", "
            sql = sql & idNuovaModalitā & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next tipoTerminale
    End If
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("MODALITADIPAGAMENTO", "IDMODALITADIPAGAMENTO", idNuovaModalitā, TSR_NUOVO)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovaModalitā)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
        sql = "SELECT NOME, DESCRIZIONE, IVAPREASSOLTA, IDMODALITAPAGAMENTOTRACCIABILE"
        sql = sql & " FROM MODALITADIPAGAMENTO"
        sql = sql & " WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        IVAPreassolta = rec("IVAPREASSOLTA")
        idModalitaPagamentoTracciabileSelezionata = IIf(IsNull(rec("IDMODALITAPAGAMENTOTRACCIABILE")), idNessunElementoSelezionato, rec("IDMODALITAPAGAMENTOTRACCIABILE"))
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    chkIVAPreassolta.Value = IVAPreassolta
    Call SelezionaElementoSuCombo(cmbTipoModalitāDiPagamentoTracciabile, idModalitaPagamentoTracciabileSelezionata)
    
    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati_OLD()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim tipoTerminale As clsElementoLista
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE MODALITADIPAGAMENTO SET" & _
        " NOME = " & SqlStringValue(nomeRecordSelezionato) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & "," & _
        " IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & "," & _
        " IVAPREASSOLTA = " & IVAPreassolta & _
        " WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("MODALITADIPAGAMENTO", "IDMODALITADIPAGAMENTO", idRecordSelezionato, TSR_MODIFICATO)
'        End If
'    End If
    
'   AGGIORNAMENTO TABELLA ORGANIZZAZIONE_MODPAG_TIPOTERM
    If Not (listaTipiTerminaleSelezionati Is Nothing) Then
        sql = "DELETE FROM ORGANIZZAZIONE_MODPAG_TIPOTERM" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND IDMODALITADIPAGAMENTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        For Each tipoTerminale In listaTipiTerminaleSelezionati
            sql = "INSERT INTO ORGANIZZAZIONE_MODPAG_TIPOTERM" & _
                " (IDORGANIZZAZIONE, IDTIPOTERMINALE, IDMODALITADIPAGAMENTO)" & _
                " VALUES " & _
                "(" & idOrganizzazioneSelezionata & ", " & _
                tipoTerminale.idElementoLista & ", " & _
                idRecordSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next tipoTerminale
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''            If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'                Call AggiornaParametriSessioneSuRecord("ORGANIZZAZIONE_MODPAG_TIPOTERM", "IDMODALITADIPAGAMENTO", idRecordSelezionato, TSR_NUOVO)
''            End If
'        End If
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub dgrConfigurazioneOrganizzazioneModalitaDiPagamento_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneOrganizzazioneModalitaDiPagamento
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub lstModalitāDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstModalitāDisponibili, lstModalitāDisponibili.Text)
End Sub

Private Sub lstModalitāSelezionate_Click()
    Call VisualizzaListBoxToolTip(lstModalitāSelezionate, lstModalitāSelezionate.Text)
End Sub

Private Sub lstTipiTerminaleDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstTipiTerminaleDisponibili, lstTipiTerminaleDisponibili.Text)
End Sub

Private Sub lstTipiTerminaleSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstTipiTerminaleSelezionati, lstTipiTerminaleSelezionati.Text)
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String
    Dim modalitā As clsElementoLista

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDMODALITADIPAGAMENTO <> " & idRecordSelezionato
    End If
    
    nomeRecordSelezionato = Trim(txtNome.Text)
    If origineModalitā = OC_STANDARD Then
        If Not (listaModalitāSelezionate Is Nothing) Then
            For Each modalitā In listaModalitāSelezionate
                If ViolataUnicitā("MODALITADIPAGAMENTO", "NOME = " & SqlStringValue(modalitā.descrizioneElementoLista), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
                    condizioneSql, "", "") Then
                    ValoriCampiOK = False
                    Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(modalitā.descrizioneElementoLista) & _
                        " č giā presente in DB per la stessa organizzazione;")
                End If
            Next modalitā
        End If
    Else
        If ViolataUnicitā("MODALITADIPAGAMENTO", "NOME = " & SqlStringValue(nomeRecordSelezionato), "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata, _
            condizioneSql, "", "") Then
            ValoriCampiOK = False
            Call listaNonConformitā.Add("- il valore nome = " & SqlStringValue(nomeRecordSelezionato) & _
                " č giā presente in DB per la stessa organizzazione;")
        End If
    End If
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    IVAPreassolta = chkIVAPreassolta.Value
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

'Private Sub CreaListaCampiValoriUnici()
'    Dim i As Integer
'    Dim modalitā As clsElementoLista
'
'    Set listaCampiValoriUnici = New Collection
'
'    If origineModalitā = OC_STANDARD Then
'        If Not (listaModalitāSelezionate Is Nothing) Then
'            For Each modalitā In listaModalitāSelezionate
'                listaCampiValoriUnici.Add ("NOME = " & SqlStringValue(modalitā.descrizioneElementoLista))
'            Next modalitā
'        End If
'    Else
'        Call listaCampiValoriUnici.Add("NOME = " & SqlStringValue(nomeRecordSelezionato))
'    End If
'End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim modalitāEliminabile As Boolean

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    
    modalitāEliminabile = True
    If Not IsRecordEliminabile("IDMODALITADIPAGAMENTO", "RICHIESTASIGILLO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("RICHIESTASIGILLO")
        modalitāEliminabile = False
    End If
    If Not IsRecordEliminabile("IDMODALITADIPAGAMENTO", "STATOTITOLO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("STATOTITOLO")
        modalitāEliminabile = False
    End If
    
    If modalitāEliminabile Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            If tipoStatoRecordSelezionato = TSR_NUOVO Then
'                sql = "DELETE FROM ORGANIZZAZIONE_MODPAG_TIPOTERM" & _
'                    " WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato & _
'                    " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
'                SETAConnection.Execute sql, n, adCmdText
'                sql = "DELETE FROM MODALITADIPAGAMENTO WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato
'                SETAConnection.Execute sql, n, adCmdText
'            Else
'                Call AggiornaParametriSessioneSuRecord("ORGANIZZAZIONE_MODPAG_TIPOTERM", "IDMODALITADIPAGAMENTO", idRecordSelezionato, TSR_ELIMINATO)
'                Call AggiornaParametriSessioneSuRecord("MODALITADIPAGAMENTO", "IDMODALITADIPAGAMENTO", idRecordSelezionato, TSR_ELIMINATO)
'            End If
'        Else
            sql = "DELETE FROM ORGANIZZAZIONE_MODPAG_TIPOTERM" & _
                " WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato & _
                " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
            SETAConnection.Execute sql, n, adCmdText
            sql = "DELETE FROM MODALITADIPAGAMENTO WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato
            SETAConnection.Execute sql, n, adCmdText
'        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Modalitā di pagamento selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Sub cmdSvuotaModalitāDisponibili_Click()
    Call SvuotaModalitāDisponibili
End Sub

Private Sub SvuotaModalitāDisponibili()
    Dim modalitā As clsElementoLista
    Dim chiaveModalitā As String
    
    isRecordEditabile = True
    For Each modalitā In listaModalitāDisponibili
        chiaveModalitā = ChiaveId(modalitā.idElementoLista)
        Call listaModalitāSelezionate.Add(modalitā, chiaveModalitā)
    Next modalitā
    Set listaModalitāDisponibili = Nothing
    Set listaModalitāDisponibili = New Collection
    
    Call lstModalitāDisponibili_Init
    Call lstModalitāSelezionate_Init
    'Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriLstTipoTerminaleDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoTerminale As String
    Dim tipoTerminaleCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTipiTerminaleDisponibili = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT T.IDTIPOTERMINALE, NOME"
'        sql = sql & " FROM TIPOTERMINALE T, ORGANIZZAZIONE_MODPAG_TIPOTERM M"
'        sql = sql & " WHERE T.IDTIPOTERMINALE = M.IDTIPOTERMINALE(+)"
'        sql = sql & " AND (M.IDTIPOTERMINALE IS NULL OR M.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")"
'        sql = sql & " AND M.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata
'        sql = sql & " AND T.IDTIPOTERMINALE NOT IN (" & TT_NON_SPECIFICATO & ", " & TT_TERMINALE_CONTROLLO_ACCESSI & ")"
'        sql = sql & " ORDER BY NOME"
'    Else
        sql = "SELECT T.IDTIPOTERMINALE, NOME"
        sql = sql & " FROM TIPOTERMINALE T, ORGANIZZAZIONE_MODPAG_TIPOTERM M"
        sql = sql & " WHERE T.IDTIPOTERMINALE = M.IDTIPOTERMINALE(+)"
        sql = sql & " AND M.IDTIPOTERMINALE IS NULL"
        sql = sql & " AND M.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata
        sql = sql & " AND T.IDTIPOTERMINALE NOT IN (" & TT_NON_SPECIFICATO & ", " & TT_TERMINALE_CONTROLLO_ACCESSI & ", " & TT_TERMINALE_POS & ")"
        sql = sql & " ORDER BY NOME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoTerminaleCorrente = New clsElementoLista
            tipoTerminaleCorrente.nomeElementoLista = rec("NOME")
            tipoTerminaleCorrente.descrizioneElementoLista = rec("NOME")
            tipoTerminaleCorrente.idElementoLista = rec("IDTIPOTERMINALE").Value
            chiaveTipoTerminale = ChiaveId(tipoTerminaleCorrente.idElementoLista)
            Call listaTipiTerminaleDisponibili.Add(tipoTerminaleCorrente, chiaveTipoTerminale)
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstTipiTerminaleDisponibili_Init
    
End Sub

Private Sub lstTipiTerminaleDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ter As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True

    lstTipiTerminaleDisponibili.Clear

    If Not (listaTipiTerminaleDisponibili Is Nothing) Then
        i = 1
        For Each ter In listaTipiTerminaleDisponibili
            lstTipiTerminaleDisponibili.AddItem ter.descrizioneElementoLista
            lstTipiTerminaleDisponibili.ItemData(i - 1) = ter.idElementoLista
            i = i + 1
        Next ter
    End If

    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriLstTipoTerminaleSelezionato()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoTerminale As String
    Dim tipoTerminaleCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTipiTerminaleSelezionati = New Collection
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT T.IDTIPOTERMINALE, NOME" & _
'            " FROM TIPOTERMINALE T, ORGANIZZAZIONE_MODPAG_TIPOTERM M" & _
'            " WHERE T.IDTIPOTERMINALE = M.IDTIPOTERMINALE" & _
'            " AND M.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND M.IDMODALITADIPAGAMENTO = " & idRecordSelezionato & _
'            " AND (M.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR M.IDTIPOSTATORECORD IS NULL)" & _
'            " AND T.IDTIPOTERMINALE NOT IN (" & TT_NON_SPECIFICATO & ", " & TT_TERMINALE_CONTROLLO_ACCESSI & ")" & _
'            " ORDER BY NOME"
'    Else
        sql = "SELECT T.IDTIPOTERMINALE, NOME" & _
            " FROM TIPOTERMINALE T, ORGANIZZAZIONE_MODPAG_TIPOTERM M" & _
            " WHERE T.IDTIPOTERMINALE = M.IDTIPOTERMINALE" & _
            " AND M.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND M.IDMODALITADIPAGAMENTO = " & idRecordSelezionato & _
            " AND T.IDTIPOTERMINALE NOT IN (" & TT_NON_SPECIFICATO & ", " & TT_TERMINALE_CONTROLLO_ACCESSI & ")" & _
            " ORDER BY NOME"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoTerminaleCorrente = New clsElementoLista
            tipoTerminaleCorrente.nomeElementoLista = rec("NOME")
            tipoTerminaleCorrente.descrizioneElementoLista = rec("NOME")
            tipoTerminaleCorrente.idElementoLista = rec("IDTIPOTERMINALE").Value
            chiaveTipoTerminale = ChiaveId(tipoTerminaleCorrente.idElementoLista)
            Call listaTipiTerminaleSelezionati.Add(tipoTerminaleCorrente, chiaveTipoTerminale)
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstTipiTerminaleSelezionati_Init
    
End Sub

Private Sub lstTipiTerminaleSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ter As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True

    lstTipiTerminaleSelezionati.Clear

    If Not (listaTipiTerminaleSelezionati Is Nothing) Then
        i = 1
        For Each ter In listaTipiTerminaleSelezionati
            lstTipiTerminaleSelezionati.AddItem ter.descrizioneElementoLista
            lstTipiTerminaleSelezionati.ItemData(i - 1) = ter.idElementoLista
            i = i + 1
        Next ter
    End If

    internalEvent = internalEventOld

End Sub

Private Sub PopolaTabellaAppoggioTipoTerminale()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim id As Integer
    Dim idModalitā As Long
    Dim elencoNomi As String
    Dim i As Integer
    Dim campoNome As String
    Dim n As Long
    Dim recordTemporaneo As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaAppoggioTipoTerminale = New Collection
    
    sql = "SELECT IDMODALITADIPAGAMENTO FROM MODALITADIPAGAMENTO" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set recordTemporaneo = New clsElementoLista
            recordTemporaneo.idElementoLista = rec("IDMODALITADIPAGAMENTO").Value
            Call listaAppoggioTipoTerminale.Add(recordTemporaneo)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    For Each recordTemporaneo In listaAppoggioTipoTerminale
        campoNome = ""
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql = "SELECT T.IDTIPOTERMINALE ID," & _
'                " T.NOME TIPOTER" & _
'                " FROM TIPOTERMINALE T, ORGANIZZAZIONE_MODPAG_TIPOTERM OMT" & _
'                " WHERE T.IDTIPOTERMINALE = OMT.IDTIPOTERMINALE(+)" & _
'                " AND OMT.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND OMT.IDMODALITADIPAGAMENTO = " & recordTemporaneo.idElementoLista & _
'                " AND (OMT.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'                " OR OMT.IDTIPOSTATORECORD IS NULL)"
'        Else
            sql = "SELECT T.IDTIPOTERMINALE ID," & _
                " T.NOME TIPOTER" & _
                " FROM TIPOTERMINALE T, ORGANIZZAZIONE_MODPAG_TIPOTERM OMT" & _
                " WHERE T.IDTIPOTERMINALE = OMT.IDTIPOTERMINALE(+)" & _
                " AND OMT.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND OMT.IDMODALITADIPAGAMENTO = " & recordTemporaneo.idElementoLista
'        End If
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                campoNome = IIf(campoNome = "", rec("TIPOTER"), campoNome & "; " & rec("TIPOTER"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        recordTemporaneo.nomeElementoLista = campoNome
    Next recordTemporaneo
    
'NOTA: qua sotto inizia il popolamento vero e proprio della tabella appoggio
'prima fase: la tabella viene pulita
    sql = "DELETE FROM " & nomeTabellaTemporanea
    SETAConnection.Execute sql, n, adCmdText
    
'seconda fase: vengono inseriti i record precedentemente tirati sų
    For Each recordTemporaneo In listaAppoggioTipoTerminale
        idModalitā = recordTemporaneo.idElementoLista
        elencoNomi = recordTemporaneo.nomeElementoLista
        sql = "INSERT INTO " & nomeTabellaTemporanea & _
            " VALUES (" & idModalitā & ", " & _
            SqlStringValue(elencoNomi) & ")"
        SETAConnection.Execute sql, n, adCmdText
    Next recordTemporaneo
    
    Call ChiudiConnessioneBD

End Sub

Private Sub CreaTabellaAppoggioTipoTerminale()
    Dim sql As String
    
    nomeTabellaTemporanea = SqlStringTableName("TMP_ORGMODTER_" & getNomeMacchina)
    sql = "CREATE TABLE " & nomeTabellaTemporanea & _
        " (IDMODALITA NUMBER(10), NOME VARCHAR2(1000))"
    
    Call EliminaTabellaAppoggioTipoTerminale
    ApriConnessioneBD
    SETAConnection.Execute (sql)
    ChiudiConnessioneBD
    
End Sub

Private Sub EliminaTabellaAppoggioTipoTerminale()
    Dim sql As String
    
On Error GoTo gestioneErrori
    
    sql = "DROP TABLE " & nomeTabellaTemporanea

    ApriConnessioneBD
    SETAConnection.Execute (sql)
    
gestioneErrori:
    ChiudiConnessioneBD

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim n As Long
    Dim tipoTerminale As clsElementoLista
    Dim condizioneSql As String
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "UPDATE MODALITADIPAGAMENTO SET"
    sql = sql & " NOME = " & SqlStringValue(nomeRecordSelezionato) & ","
    sql = sql & " DESCRIZIONE = " & SqlStringValue(descrizioneRecordSelezionato) & ","
    sql = sql & " IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ","
    sql = sql & " IDMODALITAPAGAMENTOTRACCIABILE = " & IIf(idModalitaPagamentoTracciabileSelezionata = idNessunElementoSelezionato, "NULL", idModalitaPagamentoTracciabileSelezionata) & ","
    sql = sql & " IVAPREASSOLTA = " & IVAPreassolta
    sql = sql & " WHERE IDMODALITADIPAGAMENTO = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
'   AGGIORNAMENTO TABELLA ORGANIZZAZIONE_MODPAG_TIPOTERM
    If Not (listaTipiTerminaleSelezionati Is Nothing) Then
        sql = "DELETE FROM ORGANIZZAZIONE_MODPAG_TIPOTERM"
        sql = sql & " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata
        sql = sql & " AND IDMODALITADIPAGAMENTO = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
        
        For Each tipoTerminale In listaTipiTerminaleSelezionati
            sql = "INSERT INTO ORGANIZZAZIONE_MODPAG_TIPOTERM"
            sql = sql & " (IDORGANIZZAZIONE, IDTIPOTERMINALE, IDMODALITADIPAGAMENTO)"
            sql = sql & " VALUES "
            sql = sql & "(" & idOrganizzazioneSelezionata & ", "
            sql = sql & tipoTerminale.idElementoLista & ", "
            sql = sql & idRecordSelezionato & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next tipoTerminale
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    If i = 0 Then i = 1
    cmb.AddItem "<nessuno>"
    cmb.ItemData(i - 1) = idNessunElementoSelezionato
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
'    If id = idNessunElementoSelezionato Then
'        cmb.ListIndex = idNessunElementoSelezionato
'    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
'    End If
    
End Sub







