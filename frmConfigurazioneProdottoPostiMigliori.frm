VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoPostiMigliori 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSvuotaAreeDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   5700
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   3660
      Width           =   435
   End
   Begin VB.ListBox lstAreeSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3630
      Left            =   6180
      MultiSelect     =   2  'Extended
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   3600
      Width           =   5535
   End
   Begin VB.CommandButton cmdAreaSelezionata 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   5700
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   4560
      Width           =   435
   End
   Begin VB.CommandButton cmdAreaDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   5700
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5460
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaAreeSelezionate 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   5700
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   6360
      Width           =   435
   End
   Begin VB.ListBox lstAreeDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3630
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   3600
      Width           =   5535
   End
   Begin VB.ListBox lstTipiTerminaleSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   6180
      MultiSelect     =   2  'Extended
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1440
      Width           =   5535
   End
   Begin VB.CommandButton cmdTipoTerminaleSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1500
      Width           =   435
   End
   Begin VB.CommandButton cmdTipoTerminaleDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   1920
      Width           =   435
   End
   Begin VB.CommandButton cmdSvuotaTipiTerminaleSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5700
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   2340
      Width           =   435
   End
   Begin VB.ListBox lstTipiTerminaleDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   1440
      Width           =   5535
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   5
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   4
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   3
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   0
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblConfigurazioneMaxPostiContigui 
      Alignment       =   2  'Center
      Caption         =   "CONFIGURAZIONE DEL NUMERO MASSIMO DI POSTI CONTIGUI PER AREA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   28
      Top             =   3120
      Width           =   11595
   End
   Begin VB.Label lblAssociazioneCausaliTipiTerminale 
      Alignment       =   2  'Center
      Caption         =   "ASSOCIAZIONE DELLE CAUSALI DI PROTEZIONE AI TIPI TERMINALE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   27
      Top             =   960
      Width           =   11595
   End
   Begin VB.Label lblAreeDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   120
      TabIndex        =   26
      Top             =   3360
      Width           =   5535
   End
   Begin VB.Label lblAreeSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionate"
      Height          =   195
      Left            =   6180
      TabIndex        =   25
      Top             =   3360
      Width           =   5535
   End
   Begin VB.Label lblTipiTerminaleDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Disponibili"
      Height          =   195
      Left            =   120
      TabIndex        =   18
      Top             =   1200
      Width           =   5535
   End
   Begin VB.Label lblTipiTerminaleSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Selezionati"
      Height          =   195
      Left            =   6180
      TabIndex        =   17
      Top             =   1200
      Width           =   5535
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   11
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   10
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Impostazione dei criteri per la selezione del miglior posto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   7335
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoPostiMigliori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idStagioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idClasseProdottoSelezionata As Long
Private idTipoTerminaleSelezionato As Long
Private idAreaSelezionata As Long
Private idCausaleProtezioneSelezionata As Long
Private rateo As Long
Private simboloTipoTerminaleSelezionato As String
Private numeroMaxPostiContigui As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idOrganizzazioneSelezionata As Long
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private listaAreeDisponibili As Collection
Private listaAreeSelezionate As Collection
Private listaTipiTerminaleDisponibili As Collection
Private listaTipiTerminaleSelezionati As Collection
Private isRecordEditabile As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdTipoTerminaleSelezionato(id As Long)
    idTipoTerminaleSelezionato = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    If gestioneExitCode <> EC_NON_SPECIFICATO Then
        Call lstTipiTerminaleDisponibili.Clear
        Call lstTipiTerminaleSelezionati.Clear
        Call lstAreeDisponibili.Clear
        Call lstAreeSelezionati.Clear
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = gestioneExitCode = EC_NON_SPECIFICATO
'            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Dim sql As String

    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriLstTipiTerminaleDisponibili
    Call CaricaValoriLstTipiTerminaleSelezionati
    Call CaricaValoriLstAreeDisponibili
    Call CaricaValoriLstAreeSelezionate
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub cmdAreaSelezionata_Click()
    Call AreaSelezionata
End Sub

Private Sub AreaSelezionata()
    Call CaricaFormDettagliMaxPostiContigui
    If frmDettagliMaxPostiContigui.GetExitCode = EC_CONFERMA Then
        numeroMaxPostiContigui = frmDettagliMaxPostiContigui.GetNumeroMaxPostiContigui
        Call SpostaInLstAreeSelezionate
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaInLstAreeSelezionate()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstAreeDisponibili.ListCount
        If lstAreeDisponibili.Selected(i - 1) Then
            idArea = lstAreeDisponibili.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaAreeDisponibili.Item(chiaveArea)
            area.nomeAttributoElementoLista = numeroMaxPostiContigui
            Call listaAreeSelezionate.Add(area, chiaveArea)
            Call listaAreeDisponibili.Remove(chiaveArea)
        End If
    Next i
    Call lstAreeDisponibili_Init
    Call lstAreeSelezionate_Init
End Sub

Private Sub CaricaFormDettagliMaxPostiContigui()
    Call frmDettagliMaxPostiContigui.Init
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoProtezioni.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
'qui bisogna fare il controllo di completezza del prodotto
            Unload Me
            Call frmConfigurazioneProdottoProtezioni.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            Call InserisciNellaBaseDati
            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_PARAMETRI_MIGLIOR_POSTO, stringaNota, idProdottoSelezionato)
            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
            Call CaricaValoriLstTipiTerminaleDisponibili
            Call CaricaValoriLstTipiTerminaleSelezionati
            Call CaricaValoriLstAreeDisponibili
            Call CaricaValoriLstAreeSelezionate
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneProdottoAbilitazioniSupportiDigitali
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idTipoTerminale As Long
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For i = 1 To lstTipiTerminaleDisponibili.ListCount
        If lstTipiTerminaleDisponibili.Selected(i - 1) Then
            idTipoTerminale = lstTipiTerminaleDisponibili.ItemData(i - 1)
            chiaveTipoTerminale = ChiaveId(idTipoTerminale)
            Set tipoTerminale = listaTipiTerminaleDisponibili.Item(chiaveTipoTerminale)
            Call listaTipiTerminaleSelezionati.Add(tipoTerminale, chiaveTipoTerminale)
            Call listaTipiTerminaleDisponibili.Remove(chiaveTipoTerminale)
        End If
    Next i
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim tipoTerminale As clsElementoLista
    Dim area As clsElementoLista
    Dim SqlConditions As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Call SETAConnection.BeginTrans
    SqlConditions = ""
    If Not (listaTipiTerminaleSelezionati Is Nothing) Then
'        sql = "DELETE FROM SELPOSTIMIGLIORI_CAUSPROTEZ" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato
        sql = "DELETE FROM SELPOSTIMIGL_CAUSPROTEZ_CPV" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each tipoTerminale In listaTipiTerminaleSelezionati
'            sql = "INSERT INTO SELPOSTIMIGLIORI_CAUSPROTEZ (IDCAUSALEPROTEZIONE," & _
'                " IDPRODOTTO, IDTIPOTERMINALE)" & _
'                " VALUES (" & tipoTerminale.idAttributoElementoLista & ", " & _
'                idProdottoSelezionato & ", " & _
'                tipoTerminale.idElementoLista & ")"
            sql = "INSERT INTO SELPOSTIMIGL_CAUSPROTEZ_CPV (IDCAUSALEPROTEZIONE, IDPRODOTTO, IDCLASSEPUNTOVENDITA)" & _
                " VALUES (" & tipoTerminale.idAttributoElementoLista & ", " & _
                idProdottoSelezionato & ", " & _
                tipoTerminale.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next tipoTerminale
    End If
    SqlConditions = ""
    If Not (listaAreeSelezionate Is Nothing) Then
        sql = "DELETE FROM SELPOSTIMIGLIORI_MAXCONTIGUI" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
        SETAConnection.Execute sql, n, adCmdText
        For Each area In listaAreeSelezionate
            sql = "INSERT INTO SELPOSTIMIGLIORI_MAXCONTIGUI (IDPRODOTTO," & _
                " IDAREA, NUMEROMASSIMOPOSTICONTIGUI)" & _
                " VALUES (" & idProdottoSelezionato & ", " & _
                area.idElementoLista & ", " & _
                CLng(area.nomeAttributoElementoLista) & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next area
    End If
    Call SETAConnection.CommitTrans
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub

Private Sub CaricaValoriLstAreeDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    Dim label As String

    Call ApriConnessioneBD

    Set listaAreeDisponibili = New Collection

'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT A.IDAREA ID, A.CODICE, A.NOME," & _
'            " DECODE (IDTIPOAREA, 1, 'Superarea', 2, 'Area numerata', 3, 'Area non numerata', 5, 'Supararea numerata', 6, 'Superarea non numerata', '') TIPO" & _
'            " FROM AREA A, PRODOTTO P," & _
'            " (" & _
'            " SELECT IDAREA, IDTIPOSTATORECORD" & _
'            " FROM SELPOSTIMIGLIORI_MAXCONTIGUI" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " ) S" & _
'            " WHERE P.IDPIANTA = A.IDPIANTA" & _
'            " AND S.IDAREA(+) = A.IDAREA" & _
'            " AND (S.IDAREA IS NULL OR S.IDTIPOSTATORECORD = " & TSR_ELIMINATO & ")" & _
'            " AND P.IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDTIPOAREA <> " & TA_SUPERAREA_SERVIZIO & _
'            " ORDER BY CODICE"
'    Else
        sql = "SELECT A.IDAREA ID, A.CODICE, A.NOME," & _
            " DECODE (IDTIPOAREA, 1, 'Superarea', 2, 'Area numerata', 3, 'Area non numerata', 5, 'Supararea numerata', 6, 'Superarea non numerata', '') TIPO" & _
            " FROM AREA A, PRODOTTO P," & _
            " (" & _
            " SELECT IDAREA FROM SELPOSTIMIGLIORI_MAXCONTIGUI" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " ) S" & _
            " WHERE P.IDPIANTA = A.IDPIANTA" & _
            " AND S.IDAREA(+) = A.IDAREA" & _
            " AND S.IDAREA IS NULL" & _
            " AND P.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDTIPOAREA <> " & TA_SUPERAREA_SERVIZIO & _
            " ORDER BY CODICE"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            label = rec("CODICE") & " - " & rec("NOME") & "(" & rec("TIPO") & ")"
            areaCorrente.nomeElementoLista = label
            areaCorrente.descrizioneElementoLista = label
            areaCorrente.idElementoLista = rec("ID").Value
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaAreeDisponibili.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD

    Call lstAreeDisponibili_Init

End Sub

Private Sub lstAreeDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstAreeDisponibili.Clear

    If Not (listaAreeDisponibili Is Nothing) Then
        i = 1
        For Each area In listaAreeDisponibili
            lstAreeDisponibili.AddItem area.descrizioneElementoLista
            lstAreeDisponibili.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriLstTipiTerminaleDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoTerminale As String
    Dim tipoTerminaleCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTipiTerminaleDisponibili = New Collection
    
'    sql = "SELECT T.IDTIPOTERMINALE ID, T.NOME" & _
'        " FROM TIPOTERMINALE T," & _
'        " (" & _
'        " SELECT S.IDTIPOTERMINALE FROM SELPOSTIMIGLIORI_CAUSPROTEZ S" & _
'        " WHERE S.IDPRODOTTO = " & idProdottoSelezionato & _
'        " ) S" & _
'        " WHERE T.IDTIPOTERMINALE NOT IN (" & TT_NON_SPECIFICATO & ", " & TT_TERMINALE_CONTROLLO_ACCESSI & ")" & _
'        " AND T.IDTIPOTERMINALE = S.IDTIPOTERMINALE (+)" & _
'        " AND S.IDTIPOTERMINALE IS NULL" & _
'        " ORDER BY NOME"
    sql = "SELECT CPV.IDCLASSEPUNTOVENDITA ID, CPV.NOME" & _
        " FROM CLASSEPUNTOVENDITA CPV," & _
        " (" & _
        " SELECT S.IDCLASSEPUNTOVENDITA" & _
        " FROM SELPOSTIMIGL_CAUSPROTEZ_CPV S" & _
        " WHERE S.IDPRODOTTO = " & idProdottoSelezionato & _
        " ) S" & _
        " WHERE CPV.DIRITTIOPERATOREGESTIBILI = 0" & _
        " AND CPV.IDCLASSEPUNTOVENDITA = S.IDCLASSEPUNTOVENDITA (+)" & _
        " AND S.IDCLASSEPUNTOVENDITA IS NULL" & _
        " ORDER BY CPV.IDCLASSEPUNTOVENDITA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoTerminaleCorrente = New clsElementoLista
            tipoTerminaleCorrente.nomeElementoLista = rec("NOME")
            tipoTerminaleCorrente.descrizioneElementoLista = rec("NOME")
            tipoTerminaleCorrente.idElementoLista = rec("ID").Value
            chiaveTipoTerminale = ChiaveId(tipoTerminaleCorrente.idElementoLista)
            Call listaTipiTerminaleDisponibili.Add(tipoTerminaleCorrente, chiaveTipoTerminale)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstTipiTerminaleDisponibili_Init
        
End Sub

Private Sub lstTipiTerminaleDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tipoTerminale As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTipiTerminaleDisponibili.Clear

    If Not (listaTipiTerminaleDisponibili Is Nothing) Then
        i = 1
        For Each tipoTerminale In listaTipiTerminaleDisponibili
            lstTipiTerminaleDisponibili.AddItem tipoTerminale.descrizioneElementoLista
            lstTipiTerminaleDisponibili.ItemData(i - 1) = tipoTerminale.idElementoLista
            i = i + 1
        Next tipoTerminale
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriLstTipiTerminaleSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim nomeOrganizzazione As String
    Dim codicePiantaOrganizzazione As Integer
    Dim idOrganizzazione As Long
    Dim label As String
    Dim chiaveTipoTerminale As String
    Dim tipoTerminaleCorrente As clsElementoLista
    
    Call ApriConnessioneBD

    Set listaTipiTerminaleSelezionati = New Collection
    
'    sql = "SELECT T.IDTIPOTERMINALE ID, T.NOME TERM, C.IDCAUSALEPROTEZIONE IDCAUS, C.SIMBOLOSUPIANTA SIMB" & _
'        " FROM TIPOTERMINALE T, SELPOSTIMIGLIORI_CAUSPROTEZ S, CAUSALEPROTEZIONE C" & _
'        " WHERE T.IDTIPOTERMINALE = S.IDTIPOTERMINALE" & _
'        " AND S.IDCAUSALEPROTEZIONE = C.IDCAUSALEPROTEZIONE" & _
'        " AND IDPRODOTTO = " & idProdottoSelezionato & _
'        " ORDER BY T.NOME"
    sql = "SELECT CPV.IDCLASSEPUNTOVENDITA ID, CPV.NOME CLASSE, C.IDCAUSALEPROTEZIONE IDCAUS, C.SIMBOLOSUPIANTA SIMB" & _
        " FROM CLASSEPUNTOVENDITA CPV, SELPOSTIMIGL_CAUSPROTEZ_CPV S, CAUSALEPROTEZIONE C" & _
        " WHERE CPV.IDCLASSEPUNTOVENDITA = S.IDCLASSEPUNTOVENDITA" & _
        " AND S.IDCAUSALEPROTEZIONE = C.IDCAUSALEPROTEZIONE" & _
        " AND IDPRODOTTO = " & idProdottoSelezionato & _
        " ORDER BY CPV.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoTerminaleCorrente = New clsElementoLista
            tipoTerminaleCorrente.codiceElementoLista = rec("SIMB")
            tipoTerminaleCorrente.nomeElementoLista = rec("CLASSE")
            tipoTerminaleCorrente.idAttributoElementoLista = rec("IDCAUS").Value
            label = tipoTerminaleCorrente.codiceElementoLista & _
                " - " & tipoTerminaleCorrente.nomeElementoLista
            tipoTerminaleCorrente.descrizioneElementoLista = label
            tipoTerminaleCorrente.idElementoLista = rec("ID").Value
            chiaveTipoTerminale = ChiaveId(tipoTerminaleCorrente.idElementoLista)
            Call listaTipiTerminaleSelezionati.Add(tipoTerminaleCorrente, chiaveTipoTerminale)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstTipiTerminaleSelezionati_Init
    
End Sub

Private Sub lstTipiTerminaleSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim label As String
    Dim tipoTerminale As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTipiTerminaleSelezionati.Clear

    If Not (listaTipiTerminaleSelezionati Is Nothing) Then
        i = 1
        For Each tipoTerminale In listaTipiTerminaleSelezionati
            label = tipoTerminale.codiceElementoLista & _
                " - " & tipoTerminale.nomeElementoLista
            tipoTerminale.descrizioneElementoLista = label
            lstTipiTerminaleSelezionati.AddItem label
            lstTipiTerminaleSelezionati.ItemData(i - 1) = tipoTerminale.idElementoLista
            i = i + 1
        Next tipoTerminale
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSvuotaTipiTerminaleSelezionati_Click()
    Call SvuotaTipiTerminaleSelezionati
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaAreeSelezionate_Click()
    Call SvuotaAreeSelezionate
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaTipiTerminaleSelezionati()
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For Each tipoTerminale In listaTipiTerminaleSelezionati
        tipoTerminale.descrizioneElementoLista = tipoTerminale.nomeElementoLista
        chiaveTipoTerminale = ChiaveId(tipoTerminale.idElementoLista)
        Call listaTipiTerminaleDisponibili.Add(tipoTerminale, chiaveTipoTerminale)
    Next tipoTerminale
    Set listaTipiTerminaleSelezionati = Nothing
    Set listaTipiTerminaleSelezionati = New Collection
    
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub SvuotaAreeSelezionate()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaAreeSelezionate
        area.descrizioneElementoLista = area.nomeElementoLista
        chiaveArea = ChiaveId(area.idElementoLista)
        Call listaAreeDisponibili.Add(area, chiaveArea)
    Next area
    Set listaAreeSelezionate = Nothing
    Set listaAreeSelezionate = New Collection
    
    Call lstAreeDisponibili_Init
    Call lstAreeSelezionate_Init
End Sub

Private Sub cmdTipoTerminaleDisponibile_Click()
    Call SpostaInLstTipiTerminaleDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAreaDisponibile_Click()
    Call SpostaInLstAreeDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdTipoTerminaleSelezionato_Click()
    Call TipoTerminaleSelezionato
End Sub

Private Sub TipoTerminaleSelezionato()
    If lstTipiTerminaleDisponibili.SelCount = 1 Then
        idTipoTerminaleSelezionato = lstTipiTerminaleDisponibili.ItemData(lstTipiTerminaleDisponibili.ListIndex)
        Call CaricaFormDettagliCausaleProtezione
        If frmDettagliCausaleProtezione.GetExitCode = EC_CONFERMA Then
            simboloTipoTerminaleSelezionato = frmDettagliCausaleProtezione.GetCodiceCausaleProtezioneSelezionata
            idCausaleProtezioneSelezionata = frmDettagliCausaleProtezione.GetIdCausaleProtezioneSelezionata
            Call SpostaInLstTipiTerminaleSelezionati
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaFormDettagliCausaleProtezione()
    Call frmDettagliCausaleProtezione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmDettagliCausaleProtezione.Init
End Sub

Private Sub SpostaInLstTipiTerminaleSelezionati()
    Dim i As Integer
    Dim idTipoTerminale As Long
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For i = 1 To lstTipiTerminaleDisponibili.ListCount
        If lstTipiTerminaleDisponibili.Selected(i - 1) Then
            idTipoTerminale = lstTipiTerminaleDisponibili.ItemData(i - 1)
            chiaveTipoTerminale = ChiaveId(idTipoTerminale)
            Set tipoTerminale = listaTipiTerminaleDisponibili.Item(chiaveTipoTerminale)
            tipoTerminale.codiceElementoLista = simboloTipoTerminaleSelezionato
            tipoTerminale.idAttributoElementoLista = idCausaleProtezioneSelezionata
            Call listaTipiTerminaleSelezionati.Add(tipoTerminale, chiaveTipoTerminale)
            Call listaTipiTerminaleDisponibili.Remove(chiaveTipoTerminale)
        End If
    Next i
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Public Sub SetIdOrganizzazioneSelezionata(idOrg As Long)
    idOrganizzazioneSelezionata = idOrg
End Sub

Private Sub SpostaInLstTipiTerminaleDisponibili()
    Dim i As Integer
    Dim idTipoTerminale As Long
    Dim tipoTerminale As clsElementoLista
    Dim chiaveTipoTerminale As String
    
    For i = 1 To lstTipiTerminaleSelezionati.ListCount
        If lstTipiTerminaleSelezionati.Selected(i - 1) Then
            idTipoTerminale = lstTipiTerminaleSelezionati.ItemData(i - 1)
            chiaveTipoTerminale = ChiaveId(idTipoTerminale)
            Set tipoTerminale = listaTipiTerminaleSelezionati.Item(chiaveTipoTerminale)
            tipoTerminale.descrizioneElementoLista = tipoTerminale.nomeElementoLista
            Call listaTipiTerminaleDisponibili.Add(tipoTerminale, chiaveTipoTerminale)
            Call listaTipiTerminaleSelezionati.Remove(chiaveTipoTerminale)
        End If
    Next i
    Call lstTipiTerminaleDisponibili_Init
    Call lstTipiTerminaleSelezionati_Init
End Sub

Private Sub SpostaInLstAreeDisponibili()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstAreeSelezionati.ListCount
        If lstAreeSelezionati.Selected(i - 1) Then
            idArea = lstAreeSelezionati.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaAreeSelezionate.Item(chiaveArea)
            area.descrizioneElementoLista = area.nomeElementoLista
            Call listaAreeDisponibili.Add(area, chiaveArea)
            Call listaAreeSelezionate.Remove(chiaveArea)
        End If
    Next i
    Call lstAreeDisponibili_Init
    Call lstAreeSelezionate_Init
End Sub

Private Sub CaricaValoriLstAreeSelezionate()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    Dim label As String

    Call ApriConnessioneBD

    Set listaAreeSelezionate = New Collection

'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT A.IDAREA ID, A.CODICE, A.NOME, NUMEROMASSIMOPOSTICONTIGUI," & _
'            " DECODE (IDTIPOAREA, 1, 'Superarea', 2, 'Area numerata', 3, 'Area non numerata', 5, 'Supararea numerata', 6, 'Superarea non numerata', '') TIPO" & _
'            " FROM AREA A, PRODOTTO P, SELPOSTIMIGLIORI_MAXCONTIGUI SM" & _
'            " WHERE P.IDPIANTA = A.IDPIANTA" & _
'            " AND SM.IDAREA = A.IDAREA" & _
'            " AND SM.IDPRODOTTO = P.IDPRODOTTO" & _
'            " AND P.IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDTIPOAREA <> " & TA_SUPERAREA_SERVIZIO & _
'            " AND (SM.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR SM.IDTIPOSTATORECORD IS NULL)" & _
'            " ORDER BY CODICE"
'    Else
        sql = "SELECT A.IDAREA ID, A.CODICE, A.NOME, NUMEROMASSIMOPOSTICONTIGUI," & _
            " DECODE (IDTIPOAREA, 1, 'Superarea', 2, 'Area numerata', 3, 'Area non numerata', 5, 'Supararea numerata', 6, 'Superarea non numerata', '') TIPO" & _
            " FROM AREA A, PRODOTTO P, SELPOSTIMIGLIORI_MAXCONTIGUI SM" & _
            " WHERE P.IDPIANTA = A.IDPIANTA" & _
            " AND SM.IDAREA = A.IDAREA" & _
            " AND SM.IDPRODOTTO = P.IDPRODOTTO" & _
            " AND P.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDTIPOAREA <> " & TA_SUPERAREA_SERVIZIO & _
            " ORDER BY CODICE"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            areaCorrente.nomeElementoLista = rec("CODICE") & " - " & rec("NOME") & "(" & rec("TIPO") & ")"
            areaCorrente.nomeAttributoElementoLista = rec("NUMEROMASSIMOPOSTICONTIGUI")
            label = areaCorrente.nomeElementoLista & _
                "; max posti: " & rec("NUMEROMASSIMOPOSTICONTIGUI")
            areaCorrente.descrizioneElementoLista = label
            areaCorrente.idElementoLista = rec("ID").Value
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaAreeSelezionate.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD

    Call lstAreeSelezionate_Init

End Sub

Private Sub lstAreeSelezionate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim label As String
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstAreeSelezionati.Clear

    If Not (listaAreeSelezionate Is Nothing) Then
        i = 1
        For Each area In listaAreeSelezionate
            area.descrizioneElementoLista = area.nomeElementoLista & _
                "; max posti: " & area.nomeAttributoElementoLista
            lstAreeSelezionati.AddItem area.descrizioneElementoLista
            lstAreeSelezionati.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub cmdSvuotaAreeDisponibili_Click()
    Call SvuotaAreeDisponibili
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SvuotaAreeDisponibili()
    Call CaricaFormDettagliMaxPostiContigui
    If frmDettagliMaxPostiContigui.GetExitCode = EC_CONFERMA Then
        numeroMaxPostiContigui = frmDettagliMaxPostiContigui.GetNumeroMaxPostiContigui
        Call SpostaTuttiInAreeSelezionate
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SpostaTuttiInAreeSelezionate()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaAreeDisponibili
        area.nomeAttributoElementoLista = numeroMaxPostiContigui
        chiaveArea = ChiaveId(area.idElementoLista)
        Call listaAreeSelezionate.Add(area, chiaveArea)
    Next area
    Set listaAreeDisponibili = Nothing
    Set listaAreeDisponibili = New Collection
    
    Call lstAreeDisponibili_Init
    Call lstAreeSelezionate_Init
End Sub

Private Sub CaricaFormConfigurazioneProdottoAbilitazioniSupportiDigitali()
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetRateo(rateo)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAbilitazioniSupportiDigitali.Init
End Sub

Private Sub lstAreeDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstAreeDisponibili, lstAreeDisponibili.Text)
End Sub

Private Sub lstAreeSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstAreeSelezionati, lstAreeSelezionati.Text)
End Sub

Private Sub lstTipiTerminaleDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstTipiTerminaleDisponibili, lstTipiTerminaleDisponibili.Text)
End Sub

Private Sub lstTipiTerminaleSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstTipiTerminaleSelezionati, lstTipiTerminaleSelezionati.Text)
End Sub
