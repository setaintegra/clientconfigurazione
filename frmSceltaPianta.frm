VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSceltaPianta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVerificaCompletezzaDettaglio 
      Caption         =   "Verifica compl (con dettagli)"
      Height          =   435
      Left            =   6960
      TabIndex        =   13
      Top             =   7920
      Width           =   1155
   End
   Begin VB.CommandButton cmdCambiaStato 
      Caption         =   "Rendi cancellabile"
      Height          =   435
      Left            =   8640
      TabIndex        =   12
      Top             =   7920
      Width           =   1155
   End
   Begin VB.CheckBox chkPianteAttive 
      Caption         =   "solo piante attive"
      Height          =   195
      Left            =   4440
      TabIndex        =   11
      Top             =   960
      Width           =   2655
   End
   Begin VB.CommandButton cmdVerificaCompletezza 
      Caption         =   "Verifica completezza"
      Height          =   435
      Left            =   5700
      TabIndex        =   5
      Top             =   7920
      Width           =   1155
   End
   Begin VB.ComboBox cmbVenue 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   900
      Width           =   4095
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   4
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      Height          =   915
      Left            =   120
      TabIndex        =   8
      Top             =   7620
      Width           =   5295
      Begin VB.CommandButton cmdClona 
         Caption         =   "Clona"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcPianta 
      Height          =   375
      Left            =   8220
      Top             =   60
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrPianteConfigurate 
      Height          =   6075
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10716
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblVenue 
      Caption         =   "Venue"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Selezione della Pianta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   7635
   End
End
Attribute VB_Name = "frmSceltaPianta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long
Private idTipoStatoPiantaSelezionata As TipoStatoPiantaEnum
Private idVenueSelezionato As Long
Private piantaACapienzaIllimitata As ValoreBooleanoEnum

Private internalEvent As Boolean
Private exitCodeFormIniziale As ExitCodeEnum

Private Sub chkPianteAttive_Click()
    CaricaValoriGriglia
    GetIdRecordSelezionato
End Sub

Private Sub cmbVenue_Click()
    Call cmbVenue_Update
End Sub

Private Sub cmbVenue_Update()
    idVenueSelezionato = cmbVenue.ItemData(cmbVenue.ListIndex)
    idRecordSelezionato = idNessunElementoSelezionato
    idTipoStatoPiantaSelezionata = TSPT_NON_SPECIFICATO
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaPianta
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ModificaPianta
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call NuovaPianta
    
    MousePointer = mousePointerOld
End Sub

Private Sub NuovaPianta()
    Call frmInizialePianta.SetModalitāForm(A_NUOVO)
    Call frmInizialePianta.Init(piantaACapienzaIllimitata)
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub ModificaPianta()
    Dim rec As ADODB.Recordset
    
    Set rec = adcPianta.Recordset
    Call frmInizialePianta.SetModalitāForm(A_MODIFICA)
    Call frmInizialePianta.SetIdPiantaSelezionata(idRecordSelezionato)
    If rec("ISPICCOLOIMPIANTO") = 1 Then
        Call BloccaDominioPerUtente(CCDA_PIANTA, idRecordSelezionato, isPiantaBloccataDaUtente)
        If Not isPiantaBloccataDaUtente Then
            Call frmMessaggio.Visualizza("NotificaRecordBloccato")
        End If
    End If
    Call frmInizialePianta.Init(piantaACapienzaIllimitata)
    If exitCodeFormIniziale = EC_CONFERMA Then
        Call CaricaValoriGriglia
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaPianta()
    Dim piantaEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim rec As ADODB.Recordset
    
    Set rec = adcPianta.Recordset
    If rec("ISPICCOLOIMPIANTO") = 1 Then
        Call BloccaDominioPerUtente(CCDA_PIANTA, idRecordSelezionato, isPiantaBloccataDaUtente)
        If Not isPiantaBloccataDaUtente Then
            Call frmMessaggio.Visualizza("NotificaRecordBloccato")
        End If
    End If
    Set listaTabelleCorrelate = New Collection
    Call frmInizialePianta.SetModalitāForm(A_ELIMINA)
    
    piantaEliminabile = True
    If Not IsRecordEliminabile("IDPIANTA", "PRODOTTO", CStr(idRecordSelezionato)) Then
        Call listaTabelleCorrelate.Add("PRODOTTO")
        piantaEliminabile = False
    End If
    If Not IsRecordEliminabile("IDPIANTA", "AREA", CStr(idRecordSelezionato), " AND IDTIPOAREA <> " & TA_SUPERAREA_SERVIZIO) Then
        Call listaTabelleCorrelate.Add("AREA")
        piantaEliminabile = False
    End If

    If piantaEliminabile Then
        Call frmInizialePianta.SetIdPiantaSelezionata(idRecordSelezionato)
        Call frmInizialePianta.Init(piantaACapienzaIllimitata)
        If exitCodeFormIniziale = EC_CONFERMA Then
            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
            Call CaricaValoriGriglia
        End If
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La pianta selezionata", tabelleCorrelate)
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub Esci()
    Unload Me
End Sub
Private Sub AggiornaAbilitazioneControlli()

    dgrPianteConfigurate.Columns(0).Visible = False
    dgrPianteConfigurate.Caption = "PIANTE CONFIGURATE"
    cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdClona.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdVerificaCompletezza.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdVerificaCompletezzaDettaglio.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
    cmdCambiaStato.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
End Sub

Private Sub cmdVerificaCompletezza_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call VerificaCompletezza(False)
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdVerificaCompletezzaDettaglio_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call VerificaCompletezza(True)
    
    MousePointer = mousePointerOld
End Sub

Private Sub VerificaCompletezza(conDettagli As Boolean)
    If IsPiantaCompleta(idRecordSelezionato, True, True, conDettagli) Then
        Call frmMessaggio.Visualizza("NotificaPiantaCompleta")
    End If
End Sub

Private Sub cmdCambiaStato_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CambiaStato
    
    MousePointer = mousePointerOld
End Sub

Private Sub CambiaStato()
    Dim sql As String
    Dim n As Long
    Dim risposta As VbMsgBoxResult
    
    Select Case (idTipoStatoPiantaSelezionata)
        Case TSPT_ATTIVA
            risposta = MsgBox("Sei sicuro di rendere la pianta cancellabile? L'operazione potrebbe essere irreversibile", vbYesNo)
            If risposta = vbYes Then
                Call ApriConnessioneBD
                sql = " UPDATE PIANTA SET IDTIPOSTATOPIANTA = " & TSPT_DA_CANCELLARE & "WHERE IDPIANTA = " & idRecordSelezionato
                SETAConnection.Execute sql, n, adCmdText
                If n <> 1 Then
                    MsgBox ("Stato non modificato")
                End If
            End If
        Case Else
            MsgBox ("Stato non modificabile")
    End Select
        
    CaricaValoriGriglia
    GetIdRecordSelezionato
    AggiornaAbilitazioneControlli
End Sub

Private Sub dgrPianteConfigurate_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrPianteConfigurate, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcPianta.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
        idTipoStatoPiantaSelezionata = rec("IDTIPOSTATOPIANTA").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
        idTipoStatoPiantaSelezionata = TSPT_NON_SPECIFICATO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init(capienzaIllimitata As ValoreBooleanoEnum)
    Dim sql As String
    
    idVenueSelezionato = idNessunElementoSelezionato
    idRecordSelezionato = idNessunElementoSelezionato
    piantaACapienzaIllimitata = capienzaIllimitata

    sql = "SELECT DISTINCT V.IDVENUE ID, V.NOME" & _
        " FROM VENUE V, VENUE_PIANTA VP, PIANTA P" & _
        " WHERE V.IDVENUE = VP.IDVENUE" & _
        " AND VP.IDPIANTA = P.IDPIANTA" & _
        " AND P.CAPIENZAILLIMITATA = " & capienzaIllimitata & _
        " ORDER BY NOME"
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    chkPianteAttive = VB_VERO
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcPianta_Init
    Call SelezionaElementoSuGriglia(idRecordSelezionato)
    Call dgrPianteConfigurate_Init
End Sub

Private Sub adcPianta_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    Dim sqlCondizioneStato As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcPianta
    
    If chkPianteAttive.Value = VB_VERO Then
        sqlCondizioneStato = " AND P.IDTIPOSTATOPIANTA = " & TSPT_ATTIVA
    Else
        sqlCondizioneStato = ""
    End If
    
    If piantaACapienzaIllimitata = VB_FALSO Then
        Select Case idVenueSelezionato
            Case idTuttiGliElementiSelezionati
                sql = "SELECT P.IDPIANTA ID, P.NOME AS ""Nome""," & _
                    " P.DESCRIZIONE AS ""Descrizione""," & _
                    " DECODE(GP.IDPIANTA, NULL, 0, 1) AS ""ISPICCOLOIMPIANTO""," & _
                    " DECODE(GP.IDPIANTA, NULL, 'Grande Impianto', 'Piccolo Impianto') AS ""Tipo Impianto""," & _
                    " TSP.IDTIPOSTATOPIANTA, TSP.NOME AS ""Stato""" & _
                    " FROM PIANTA P, GRIGLIAPOSTI GP, TIPOSTATOPIANTA TSP" & _
                    " WHERE GP.IDPIANTA(+) = P.IDPIANTA AND" & _
                    " P.IDTIPOSTATOPIANTA = TSP.IDTIPOSTATOPIANTA"
                sql = sql & sqlCondizioneStato
                sql = sql & " ORDER BY P.NOME"
            Case Else
                sql = "SELECT P.IDPIANTA AS ""ID"", P.NOME AS ""Nome""," & _
                    " P.DESCRIZIONE AS ""Descrizione""," & _
                    " DECODE(GP.IDPIANTA, NULL, 0, 1) AS ""ISPICCOLOIMPIANTO""," & _
                    " DECODE(GP.IDPIANTA, NULL, 'Grande Impianto', 'Piccolo Impianto') AS ""Tipo Impianto""," & _
                    " TSP.IDTIPOSTATOPIANTA, TSP.NOME AS ""Stato""" & _
                    " FROM PIANTA P, GRIGLIAPOSTI GP, VENUE_PIANTA VP, TIPOSTATOPIANTA TSP WHERE" & _
                    " P.IDPIANTA = VP.IDPIANTA AND" & _
                    " GP.IDPIANTA(+) = P.IDPIANTA AND" & _
                    " VP.IDVENUE = " & idVenueSelezionato & " AND " & _
                    " P.IDTIPOSTATOPIANTA = TSP.IDTIPOSTATOPIANTA"
                sql = sql & sqlCondizioneStato
                sql = sql & " ORDER BY P.NOME"
        End Select
    Else
        Select Case idVenueSelezionato
            Case idTuttiGliElementiSelezionati
                sql = "SELECT P.IDPIANTA ID, P.NOME AS ""Nome""," & _
                    " P.DESCRIZIONE AS ""Descrizione""," & _
                    " 1 AS ""ISPICCOLOIMPIANTO""," & _
                    " 'Pianta a capienza illimitata' AS ""Tipo Impianto""," & _
                    " TSP.IDTIPOSTATOPIANTA, TSP.NOME AS ""Stato""" & _
                    " FROM PIANTA P, TIPOSTATOPIANTA TSP" & _
                    " WHERE P.IDTIPOSTATOPIANTA = TSP.IDTIPOSTATOPIANTA" & _
                    " AND P.CAPIENZAILLIMITATA = " & VB_VERO
                sql = sql & sqlCondizioneStato
                sql = sql & " ORDER BY P.NOME"
            Case Else
                sql = "SELECT P.IDPIANTA AS ""ID"", P.NOME AS ""Nome""," & _
                    " P.DESCRIZIONE AS ""Descrizione""," & _
                    " 1 AS ""ISPICCOLOIMPIANTO""," & _
                    " 'Pianta a capienza illimitata' AS ""Tipo Impianto""," & _
                    " TSP.IDTIPOSTATOPIANTA, TSP.NOME AS ""Stato""" & _
                    " FROM PIANTA P, VENUE_PIANTA VP, TIPOSTATOPIANTA TSP WHERE" & _
                    " P.IDPIANTA = VP.IDPIANTA AND" & _
                    " VP.IDVENUE = " & idVenueSelezionato & " AND " & _
                    " P.IDTIPOSTATOPIANTA = TSP.IDTIPOSTATOPIANTA" & _
                    " AND P.CAPIENZAILLIMITATA = " & VB_VERO
                sql = sql & sqlCondizioneStato
                sql = sql & " ORDER BY P.NOME"
        End Select
    End If
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrPianteConfigurate.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrPianteConfigurate_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set dgrPianteConfigurate.dataSource = adcPianta
    
    Set g = dgrPianteConfigurate
    
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100 - 4000
    numeroCampi = 2
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Visible = False
    g.Columns(4).Width = 2000
    g.Columns(5).Visible = False
    g.Columns(6).Width = 2000
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcPianta.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub cmdClona_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CaricaFormClonazionePianta
    
    MousePointer = mousePointerOld
End Sub

Private Sub CaricaFormClonazionePianta()
    If esistePiantaInClonazione() Then
        MsgBox "Esiste gia' una clonazione di pianta in corso!"
    Else
        Call frmClonazionePianta.SetIdPiantaSelezionata(idRecordSelezionato)
        Call frmClonazionePianta.Init
        Call CaricaValoriGriglia
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
'    cmb.AddItem "<nessuno>"
'    cmb.ItemData(i) = idNessunElementoSelezionato
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Public Sub SetExitCodeFormIniziale(exC As ExitCodeEnum)
    exitCodeFormIniziale = exC
End Sub



