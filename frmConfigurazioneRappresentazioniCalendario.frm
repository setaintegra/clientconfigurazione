VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmConfigurazioneRappresentazioniCalendario 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Spettacolo"
   ClientHeight    =   9285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11700
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9285
   ScaleWidth      =   11700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDeselezionaTutti 
      Caption         =   "Deseleziona tutti"
      Height          =   255
      Left            =   5880
      TabIndex        =   41
      Top             =   8040
      Width           =   2055
   End
   Begin VB.CommandButton cmdSelezionaTutti 
      Caption         =   "Seleziona tutti"
      Height          =   255
      Left            =   3840
      TabIndex        =   40
      Top             =   8040
      Width           =   2055
   End
   Begin VB.Frame Frame2 
      Caption         =   "Parametri di definizione dell'insieme delle rappresentazioni"
      Height          =   1575
      Left            =   240
      TabIndex        =   29
      Top             =   720
      Width           =   11295
      Begin VB.Frame frmOption 
         Caption         =   "Opzioni"
         Height          =   975
         Left            =   4800
         TabIndex        =   30
         Top             =   480
         Width           =   6375
         Begin VB.OptionButton optTuttiIGiorni 
            Caption         =   "Tutti i giorni"
            Height          =   255
            Left            =   120
            TabIndex        =   35
            Top             =   240
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.OptionButton optEsclusiFineSettimana 
            Caption         =   "Escludi sabati e domeniche"
            Height          =   255
            Left            =   1560
            TabIndex        =   34
            Top             =   240
            Width           =   2295
         End
         Begin VB.OptionButton optEscluseDomeniche 
            Caption         =   "Escludi domeniche"
            Height          =   255
            Left            =   1560
            TabIndex        =   33
            Top             =   480
            Width           =   2295
         End
         Begin VB.OptionButton optSoloDomeniche 
            Caption         =   "Solo domeniche"
            Height          =   255
            Left            =   3960
            TabIndex        =   32
            Top             =   480
            Width           =   2295
         End
         Begin VB.OptionButton optSoloFineSettimana 
            Caption         =   "Solo sabati e domeniche"
            Height          =   255
            Left            =   3960
            TabIndex        =   31
            Top             =   240
            Width           =   2295
         End
      End
      Begin MSComCtl2.DTPicker dtpDataDal 
         Height          =   315
         Left            =   480
         TabIndex        =   36
         Top             =   600
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         _Version        =   393216
         Format          =   60424193
         CurrentDate     =   39820
      End
      Begin MSComCtl2.DTPicker dtpDataAl 
         Height          =   315
         Left            =   2880
         TabIndex        =   37
         Top             =   600
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         _Version        =   393216
         Format          =   60424193
         CurrentDate     =   39820
      End
      Begin VB.Label Label1 
         Caption         =   "Dal:"
         Height          =   195
         Left            =   120
         TabIndex        =   39
         Top             =   600
         Width           =   375
      End
      Begin VB.Label Label2 
         Caption         =   "Al:"
         Height          =   195
         Left            =   2520
         TabIndex        =   38
         Top             =   600
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Caratteristiche di ogni rappresentazione"
      Height          =   2655
      Left            =   240
      TabIndex        =   7
      Top             =   2520
      Width           =   11295
      Begin VB.ComboBox cmbVenue 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1260
         Width           =   3705
      End
      Begin VB.TextBox txtMinuti 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   840
         MaxLength       =   2
         TabIndex        =   18
         Top             =   600
         Width           =   435
      End
      Begin VB.TextBox txtOra 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   240
         MaxLength       =   2
         TabIndex        =   17
         Top             =   600
         Width           =   435
      End
      Begin VB.ListBox lstDisponibili 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1740
         Left            =   5100
         MultiSelect     =   2  'Extended
         TabIndex        =   16
         Top             =   780
         Width           =   2715
      End
      Begin VB.CommandButton cmdSvuotaSelezionati 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7920
         TabIndex        =   15
         Top             =   2040
         Width           =   435
      End
      Begin VB.CommandButton cmdDidsponibile 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7920
         TabIndex        =   14
         Top             =   1620
         Width           =   435
      End
      Begin VB.CommandButton cmdSelezionato 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7920
         TabIndex        =   13
         Top             =   1200
         Width           =   435
      End
      Begin VB.ListBox lstSelezionati 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1740
         Left            =   8460
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         Top             =   780
         Width           =   2715
      End
      Begin VB.TextBox txtOreDurata 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1380
         MaxLength       =   3
         TabIndex        =   11
         Top             =   600
         Width           =   555
      End
      Begin VB.TextBox txtMinutiDurata 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2100
         MaxLength       =   2
         TabIndex        =   10
         Top             =   600
         Width           =   435
      End
      Begin VB.CommandButton cmdSvuotaDisponibili 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7920
         TabIndex        =   9
         Top             =   780
         Width           =   435
      End
      Begin VB.TextBox txtQuantitaIngressiVendibili 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   240
         MaxLength       =   6
         TabIndex        =   8
         Top             =   1980
         Width           =   795
      End
      Begin VB.Label lblOra 
         Caption         =   "Ora (HH:MM)"
         Height          =   195
         Left            =   240
         TabIndex        =   28
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblVenue 
         Caption         =   "Venue"
         Height          =   195
         Left            =   240
         TabIndex        =   27
         Top             =   1020
         Width           =   1695
      End
      Begin VB.Label lblSeparatoreOreMinuti 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         TabIndex        =   26
         Top             =   600
         Width           =   75
      End
      Begin VB.Label lblSelezionati 
         Alignment       =   2  'Center
         Caption         =   "Selezionati"
         Height          =   195
         Left            =   8520
         TabIndex        =   25
         Top             =   480
         Width           =   2595
      End
      Begin VB.Label lblDisponibili 
         Alignment       =   2  'Center
         Caption         =   "Disponibili"
         Height          =   195
         Left            =   5160
         TabIndex        =   24
         Top             =   480
         Width           =   2595
      End
      Begin VB.Label lblTurni 
         Alignment       =   2  'Center
         Caption         =   "TURNI RAPPRESENTAZIONE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5100
         TabIndex        =   23
         Top             =   240
         Width           =   6075
      End
      Begin VB.Label lblSeparatoreOreMinutiDurata 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   22
         Top             =   600
         Width           =   75
      End
      Begin VB.Label lblDurata 
         Caption         =   "Durata (HH:MM)"
         Height          =   195
         Left            =   1380
         TabIndex        =   21
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblQuantitaIngressiVendibili 
         Caption         =   "Quantitą ingressi vendibili (prod. calendario)"
         Height          =   195
         Left            =   240
         TabIndex        =   20
         Top             =   1740
         Width           =   3075
      End
   End
   Begin VB.CommandButton cmdElimina 
      Caption         =   "Elimina"
      Height          =   375
      Left            =   5040
      TabIndex        =   6
      Top             =   8520
      Width           =   1695
   End
   Begin VB.ListBox lstCalendario 
      Height          =   2085
      Left            =   3840
      Style           =   1  'Checkbox
      TabIndex        =   5
      Top             =   5880
      Width           =   4095
   End
   Begin VB.CommandButton cmdAggiungiAElencoGiorni 
      Caption         =   "Aggiungi"
      Height          =   375
      Left            =   5040
      TabIndex        =   4
      Top             =   5280
      Width           =   1695
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8220
      TabIndex        =   2
      Top             =   240
      Width           =   3255
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10320
      TabIndex        =   0
      Top             =   8760
      Width           =   1155
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8220
      TabIndex        =   3
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Rappresentazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneRappresentazioniCalendario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idVenueRapprSelezionata As Long
Private idSpettacoloSelezionato As Long
Private SQLCaricamentoCombo As String
Private dataOraRecordSelezionato As Date
Private durata As Long
Private controlloAccessiAbilitato As ValoreBooleanoEnum
Private nomeSpettacoloSelezionato As String
Private progressivoTabellaTemporanea As Long
Private nomeTabellaTemporanea As String
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private listaAppoggio As Collection
Private utilizzatoriDaMantenere As ValoreBooleanoEnum
Private quantitaIngressiVendibili As Long
Private listaIdRappresentazioniSelezionate As Collection
Private dataOraDaInserire As Date
Private durataDaInserire As Long

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum

Private Sub AggiornaAbilitazioneControlli()

    lblInfo1.Caption = "Spettacolo"
    txtInfo1.Text = nomeSpettacoloSelezionato
    txtInfo1.Enabled = False

    cmdAggiungiAElencoGiorni.Enabled = (Len(txtOra.Text) = 2 And _
                           Len(txtMinuti.Text) = 2 And _
                           Len(txtOreDurata.Text) = 3 And _
                           Len(txtMinutiDurata.Text) = 2 And _
                           cmbVenue.ListIndex <> idNessunElementoSelezionato)

'    If (gestioneExitCode = EC_NON_SPECIFICATO And _
'            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
'        dgrConfigurazioneSpettacolo.Enabled = True
'        lblOperazioneInCorso.Caption = ""
'        lblOperazione.Caption = ""
'
'        txtOra.Text = ""
'        txtMinuti.Text = ""
'        txtOreDurata.Text = ""
'        txtMinutiDurata.Text = ""
'        txtQuantitaIngressiVendibili.Text = ""
'        Call cmbVenue.Clear
'        Call lstDisponibili.Clear
'        Call lstSelezionati.Clear
'
'        dtpData.Enabled = False
'        txtOra.Enabled = False
'        txtMinuti.Enabled = False
'        txtOreDurata.Enabled = False
'        txtMinutiDurata.Enabled = False
'        txtQuantitaIngressiVendibili.Enabled = False
'        chkUtilizzatoriDaMantenere.Enabled = False
'
'        lblData.Enabled = False
'        lblOra.Enabled = False
'        lblDurata.Enabled = False
'        lblVenue.Enabled = False
'        lblTurni.Enabled = False
'        lblDisponibili.Enabled = False
'        lblSelezionati.Enabled = False
'        lblSeparatoreOreMinuti.Enabled = False
'        lblSeparatoreOreMinutiDurata.Enabled = False
'        lblQuantitaIngressiVendibili.Enabled = False
'
'        cmbVenue.Enabled = False
'        cmdSelezionato.Enabled = False
'        cmdDidsponibile.Enabled = False
'        cmdSvuotaSelezionati.Enabled = False
'        cmdSvuotaDisponibili.Enabled = False
'        cmdInserisciNuovo.Enabled = True
'        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdConferma.Enabled = False
'        cmdAnnulla.Enabled = False
'
'    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
'            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
'        dgrConfigurazioneSpettacolo.Enabled = False
'        dtpData.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        txtOra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        txtMinuti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        txtOreDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        txtMinutiDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        txtQuantitaIngressiVendibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        chkUtilizzatoriDaMantenere.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmbVenue.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lstDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lstSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblData.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblOra.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblVenue.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblTurni.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblSeparatoreOreMinuti.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblSeparatoreOreMinutiDurata.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblQuantitaIngressiVendibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmdSelezionato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmdDidsponibile.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmdSvuotaSelezionati.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmdSvuotaDisponibili.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        cmdInserisciNuovo.Enabled = False
'        cmdInserisciDaSelezione.Enabled = False
'        cmdModifica.Enabled = False
'        cmdElimina.Enabled = False
'        cmdConferma.Enabled = (Len(txtOra.Text) = 2 And _
'                               Len(txtMinuti.Text) = 2 And _
'                               Len(txtOreDurata.Text) = 3 And _
'                               Len(txtMinutiDurata.Text) = 2 And _
'                               cmbVenue.ListIndex <> idNessunElementoSelezionato)
'        cmdAnnulla.Enabled = True
'        lblOperazioneInCorso.Caption = "Operazione in corso:"
'        Select Case gestioneRecordGriglia
'            Case ASG_INSERISCI_NUOVO
'                lblOperazione.Caption = "inserimento nuovo record"
'            Case ASG_INSERISCI_DA_SELEZIONE
'                lblOperazione.Caption = "inserimento nuovo record"
'            Case ASG_MODIFICA
'                lblOperazione.Caption = "modifica record selezionato"
'            Case ASG_ELIMINA
'                lblOperazione.Caption = "eliminazione record selezionato"
'        End Select
'
'    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
'            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
'        dgrConfigurazioneSpettacolo.Enabled = True
'        lblOperazioneInCorso.Caption = ""
'        lblOperazione.Caption = ""
'
'        txtOra.Text = ""
'        txtMinuti.Text = ""
'        txtOreDurata.Text = ""
'        txtMinutiDurata.Text = ""
'        txtQuantitaIngressiVendibili.Text = ""
'        Call cmbVenue.Clear
'        Call lstDisponibili.Clear
'        Call lstSelezionati.Clear
'
'        dtpData.Enabled = False
'        txtOra.Enabled = False
'        txtMinuti.Enabled = False
'        txtOreDurata.Enabled = False
'        txtMinutiDurata.Enabled = False
'        txtQuantitaIngressiVendibili.Enabled = False
'        chkUtilizzatoriDaMantenere.Enabled = False
'
'        lblData.Enabled = False
'        lblOra.Enabled = False
'        lblDurata.Enabled = False
'        lblVenue.Enabled = False
'        lblTurni.Enabled = False
'        lblDisponibili.Enabled = False
'        lblSelezionati.Enabled = False
'        lblSeparatoreOreMinuti.Enabled = False
'        lblSeparatoreOreMinutiDurata.Enabled = False
'        lblQuantitaIngressiVendibili.Enabled = False
'
'        cmbVenue.Enabled = False
'        cmdSelezionato.Enabled = False
'        cmdDidsponibile.Enabled = False
'        cmdSvuotaSelezionati.Enabled = False
'        cmdSvuotaDisponibili.Enabled = False
'        cmdInserisciNuovo.Enabled = True
'        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdConferma.Enabled = False
'        cmdAnnulla.Enabled = False
'
'    End If
    
End Sub

Private Sub Controlli_Init()

    lblInfo1.Caption = "Spettacolo"
    txtInfo1.Text = nomeSpettacoloSelezionato
    txtInfo1.Enabled = False
'    lblData.Caption = "Data (GG/MM/AAAA)"
    lblOra.Caption = "Ora (HH.MM)"
    lblVenue.Caption = "Venue"
    lblTurni.Caption = "Turni Rappresentazione"
    lblDisponibili.Caption = "Disponibili"
    lblSelezionati.Caption = "Selezionati"
    lblQuantitaIngressiVendibili.Caption = "Quantita ingressi vendibili (prod. calendario)"

    lblIntestazioneForm.Caption = "Configurazione delle Rappresentazioni"
'    dgrConfigurazioneSpettacolo.Caption = "RAPPRESENTAZIONI CONFIGURATE"
'
'    dgrConfigurazioneSpettacolo.Enabled = True
'    lblOperazioneInCorso.Caption = ""
    Call cmbVenue.Clear
    Call lstDisponibili.Clear
    Call lstSelezionati.Clear
    cmbVenue.Enabled = False
    cmdSelezionato.Enabled = False
    cmdDidsponibile.Enabled = False
    cmdSvuotaSelezionati.Enabled = False

End Sub

Public Sub SetIdSpettacoloSelezionato(id As Long)
    idSpettacoloSelezionato = id
End Sub

Public Sub SetNomeSpettacoloSelezionato(nome As String)
    nomeSpettacoloSelezionato = nome
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdDidsponibile_Click()
    Call SpostaInLstDisponibili
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EliminaRappresentazioniSelezionate
    
    MousePointer = mousePointerOld
End Sub

Private Function EliminaRappresentazioniSelezionate() As String
    Dim i As Integer
    
    For i = 1 To listaIdRappresentazioniSelezionate.count
        EliminaDallaBaseDati (listaIdRappresentazioniSelezionate.Item(i))
    Next i
    lstCalendario_Init
End Function

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmbVenue_Click()
    If Not internalEvent Then
        idVenueRapprSelezionata = cmbVenue.ItemData(cmbVenue.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub cmdSelezionaTutti_Click()
    Call SelezionaTutteLeRappresentazioni
End Sub

Private Sub SelezionaTutteLeRappresentazioni()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idRappresentazione As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set listaIdRappresentazioniSelezionate = Nothing
    Set listaIdRappresentazioniSelezionate = New Collection
    For i = 1 To lstCalendario.ListCount
        lstCalendario.Selected(i - 1) = True
        idRappresentazione = lstCalendario.ItemData(i - 1)
        Call listaIdRappresentazioniSelezionate.Add(idRappresentazione, ChiaveId(idRappresentazione))
    Next i
    
    internalEvent = internalEventOld
End Sub

Private Sub cmdDeselezionaTutti_Click()
    Call DeselezionaTutteLeRappresentazioni
End Sub

Private Sub DeselezionaTutteLeRappresentazioni()
    Dim i As Integer
    Dim internalEventOld As Boolean
    Dim idRappresentazione As Long
    
    internalEventOld = internalEvent
    internalEvent = True
    
    For i = 1 To lstCalendario.ListCount
        lstCalendario.Selected(i - 1) = False
    Next i
    Set listaIdRappresentazioniSelezionate = Nothing
    Set listaIdRappresentazioniSelezionate = New Collection
    
    internalEvent = internalEventOld
End Sub

Private Sub lstCalendario_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaRappresentazione
    End If
End Sub

Private Sub SelezionaDeselezionaRappresentazione()
    Dim idRappresentazione As Long
    Dim dataOraInizio As String
    
    idRappresentazione = lstCalendario.ItemData(lstCalendario.ListIndex)
    dataOraInizio = lstCalendario.Text
    If lstCalendario.Selected(lstCalendario.ListIndex) Then
        Call listaIdRappresentazioniSelezionate.Add(idRappresentazione, ChiaveId(idRappresentazione))
    Else
        Call listaIdRappresentazioniSelezionate.Remove(ChiaveId(idRappresentazione))
    End If
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
End Sub

Public Sub Init()
    Dim sql As String

    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call dtpDate_Init
    Call lstCalendario_Init
    
    sql = "SELECT IDVENUE AS ID, NOME FROM VENUE ORDER BY NOME"
    Call CaricaValoriCombo(cmbVenue, sql, "NOME")
    
    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
    
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub EliminaDallaBaseDati(idRappresentazione As Long)
    Dim sql As String
    Dim rappresentazioneEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String

    Call ApriConnessioneBD
    
    Set listaTabelleCorrelate = New Collection
    rappresentazioneEliminabile = True
    
    If Not IsRecordEliminabile("IDRAPPRESENTAZIONE", "PRODOTTO_RAPPRESENTAZIONE", CStr(idRappresentazione)) Then
        Call listaTabelleCorrelate.Add("PRODOTTO_RAPPRESENTAZIONE")
        rappresentazioneEliminabile = False
    End If
    
    If rappresentazioneEliminabile Then
        sql = "DELETE FROM RAPPRESENTAZIONE_TURNORAPPR" & _
            " WHERE IDRAPPRESENTAZIONE = " & idRappresentazione
        SETAConnection.Execute sql, , adCmdText
        sql = "DELETE FROM RAPPRESENTAZIONE" & _
            " WHERE IDRAPPRESENTAZIONE = " & idRappresentazione
        SETAConnection.Execute sql, , adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "La Rappresentazione selezionata", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Function ValoriCampiOK() As Boolean
    Dim ora As String
    Dim minuti As String
    Dim oreDurata As String
    Dim minutiDurata As String
    Dim secondi As String
    Dim dataInizio As Date
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    
    secondi = "00"
'    dataInizio = FormatDateTime(dtpData.Value, vbShortDate)
    Set listaNonConformitą = New Collection
    
    If IsCampoOraCorretto(txtOra) Then
        ora = CStr(Trim(txtOra.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Inizio ore deve essere numerico di tipo intero e compreso tra 0 e 23;")
    End If
    If IsCampoMinutiCorretto(txtMinuti) Then
        minuti = CStr(Trim(txtMinuti.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Inizio minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    
    If IsCampoInteroCorretto(txtOreDurata) Then
        oreDurata = CStr(Trim(txtOreDurata.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Durata ore deve essere numerico di tipo intero;")
    End If
    
    If IsCampoMinutiCorretto(txtMinutiDurata) Then
        minutiDurata = CStr(Trim(txtMinutiDurata.Text))
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Durata minuti deve essere numerico di tipo intero e compreso tra 0 e 59;")
    End If
    
    If IsCampoInteroCorretto(txtQuantitaIngressiVendibili) Then
        quantitaIngressiVendibili = CLng(txtQuantitaIngressiVendibili.Text)
    Else
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Quantitą ingressi vendibili deve essere numerico di tipo intero;")
    End If
    
    durata = (oreDurata * 60) + minutiDurata
'    dataOraRecordSelezionato = FormatDateTime(dataInizio & " " & ora & ":" & minuti & ":" & secondi, vbGeneralDate)
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub cmdAggiungiAElencoGiorni_Click()
    Call AggiungiRappresentazioni
End Sub

Private Sub AggiungiRappresentazioni()
    Dim dataDal As Date
    Dim dataAl As Date
    Dim data As Date
    
    durataDaInserire = (txtOreDurata.Text * 60) + txtMinutiDurata.Text
    If ValoriCampiOK Then
        dataDal = FormatDateTime(dtpDataDal.Value, vbShortDate)
        dataAl = FormatDateTime(dtpDataAl.Value, vbShortDate)
        
        If dataDal >= dataAl Then
            MsgBox "Intervallo date errato"
        Else
            If optTuttiIGiorni.Value = True Then
                For data = dataDal To dataAl
                    dataOraDaInserire = FormatDateTime(data & " " & txtOra.Text & ":" & txtMinuti.Text & ":" & 0, vbGeneralDate)
                    Call InserisciNellaBaseDati
                Next data
            ElseIf optEsclusiFineSettimana.Value = True Then
                For data = dataDal To dataAl
                    If (Weekday(data) <> vbSaturday And Weekday(data) <> vbSunday) Then
                        dataOraDaInserire = FormatDateTime(data & " " & txtOra.Text & ":" & txtMinuti.Text & ":" & 0, vbGeneralDate)
                        Call InserisciNellaBaseDati
                    End If
                Next data
            ElseIf optEscluseDomeniche.Value = True Then
                For data = dataDal To dataAl
                    If (Weekday(data) <> vbSunday) Then
                        dataOraDaInserire = FormatDateTime(data & " " & txtOra.Text & ":" & txtMinuti.Text & ":" & 0, vbGeneralDate)
                        Call InserisciNellaBaseDati
                    End If
                Next data
            ElseIf optSoloFineSettimana.Value = True Then
                For data = dataDal To dataAl
                    If (Weekday(data) = vbSaturday Or Weekday(data) = vbSunday) Then
                        dataOraDaInserire = FormatDateTime(data & " " & txtOra.Text & ":" & txtMinuti.Text & ":" & 0, vbGeneralDate)
                        Call InserisciNellaBaseDati
                    End If
                Next data
            ElseIf optSoloDomeniche.Value = True Then
                For data = dataDal To dataAl
                    If (Weekday(data) = vbSunday) Then
                        dataOraDaInserire = FormatDateTime(data & " " & txtOra.Text & ":" & txtMinuti.Text & ":" & 0, vbGeneralDate)
                        Call InserisciNellaBaseDati
                    End If
                Next data
            Else
                MsgBox "....."
            End If
        End If
        
        Call lstCalendario_Init

    End If
End Sub

Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub txtOra_Change()
    If Not internalEvent Then
        If Len(txtOra.Text) = 2 Then
            txtMinuti.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinuti_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtOreDurata_Change()
    If Not internalEvent Then
        If Len(txtOreDurata.Text) = 3 Then
            txtMinutiDurata.SetFocus
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtMinutiDurata_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub dtpDate_Init()
    dtpDataDal.Value = Now
    dtpDataAl.Value = Now
End Sub

Private Sub dtpData_Click()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SvuotaDisponibili
End Sub

Private Function IsRappresentazioneAggiornabile() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim aggiornabile As Boolean
    
    Call ApriConnessioneBD
    
    aggiornabile = False
'    a) tutti i prodotti correlati devono essere liberi, cioč non lockati da altro utente
'    b) tutti i prodotti correlati devono essere in stato "in configurazione"
    sql = " SELECT DISTINCT P.NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR" & _
        " WHERE P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE =  " & idRecordSelezionato & _
        " AND PR.IDPRODOTTO IN (SELECT IDPRODOTTO FROM CC_PRODOTTOUTILIZZATO)" & _
        " UNION" & _
        " SELECT DISTINCT P.NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR" & _
        " WHERE P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE =  " & idRecordSelezionato & _
        " AND P.IDTIPOSTATOPRODOTTO <> " & TSP_IN_CONFIGURAZIONE
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If (rec.BOF And rec.EOF) Then
        aggiornabile = True
    End If
    
    Call ChiudiConnessioneBD
    
    IsRappresentazioneAggiornabile = aggiornabile
End Function

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTurno As String
    Dim turnoRappresentazioneCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    sql = "SELECT IDTURNORAPPRESENTAZIONE IDT, NOME FROM TURNORAPPRESENTAZIONE ORDER BY NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set turnoRappresentazioneCorrente = New clsElementoLista
            turnoRappresentazioneCorrente.nomeElementoLista = rec("NOME")
            turnoRappresentazioneCorrente.descrizioneElementoLista = rec("NOME")
            turnoRappresentazioneCorrente.idElementoLista = rec("IDT").Value
            chiaveTurno = ChiaveId(turnoRappresentazioneCorrente.idElementoLista)
            Call listaDisponibili.Add(turnoRappresentazioneCorrente, chiaveTurno)
            rec.MoveNext
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim turno As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear
    
    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each turno In listaDisponibili
            lstDisponibili.AddItem turno.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = turno.idElementoLista
            i = i + 1
        Next turno
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTurno As String
    Dim turnoRappresentazioneCorrente As clsElementoLista
    
    Set listaSelezionati = New Collection
    Call lstSelezionati_Init
    
'    Call ApriConnessioneBD
'
'    If idRecordSelezionato >= 0 Then
'        sql = "SELECT RT.IDRAPPRESENTAZIONE, T.IDTURNORAPPRESENTAZIONE IDT, T.NOME FROM" & _
'            " TURNORAPPRESENTAZIONE T, RAPPRESENTAZIONE R, RAPPRESENTAZIONE_TURNORAPPR RT" & _
'            " WHERE R.IDRAPPRESENTAZIONE = RT.IDRAPPRESENTAZIONE" & _
'            " AND T.IDTURNORAPPRESENTAZIONE = RT.IDTURNORAPPRESENTAZIONE" & _
'            " AND RT.IDRAPPRESENTAZIONE = " & idRecordSelezionato & _
'            " ORDER BY NOME"
'        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'        If Not (rec.BOF And rec.EOF) Then
'            rec.MoveFirst
'            While Not rec.EOF
'                Set turnoRappresentazioneCorrente = New clsElementoLista
'                turnoRappresentazioneCorrente.nomeElementoLista = rec("NOME")
'                turnoRappresentazioneCorrente.descrizioneElementoLista = rec("NOME")
'                turnoRappresentazioneCorrente.idElementoLista = rec("IDT").Value
'                chiaveTurno = ChiaveId(turnoRappresentazioneCorrente.idElementoLista)
'                Call listaSelezionati.Add(turnoRappresentazioneCorrente, chiaveTurno)
'                rec.MoveNext
'            Wend
'        End If
'        rec.Close
'
'        Call ChiudiConnessioneBD
'
'        Call lstSelezionati_Init
'    Else
'        'do Nothing
'    End If
    
End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim turno As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear

    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each turno In listaSelezionati
            lstSelezionati.AddItem turno.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = turno.idElementoLista
            i = i + 1
        Next turno
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SvuotaDisponibili()
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For Each turno In listaDisponibili
        chiaveTurno = ChiaveId(turno.idElementoLista)
        Call listaSelezionati.Add(turno, chiaveTurno)
    Next turno
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SvuotaSelezionati()
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For Each turno In listaSelezionati
        chiaveTurno = ChiaveId(turno.idElementoLista)
        Call listaDisponibili.Add(turno, chiaveTurno)
    Next turno
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idTurno As Long
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idTurno = lstSelezionati.ItemData(i - 1)
            chiaveTurno = ChiaveId(idTurno)
            Set turno = listaSelezionati.Item(chiaveTurno)
            Call listaDisponibili.Add(turno, chiaveTurno)
            Call listaSelezionati.Remove(chiaveTurno)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idTurno As Long
    Dim turno As clsElementoLista
    Dim chiaveTurno As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idTurno = lstDisponibili.ItemData(i - 1)
            chiaveTurno = ChiaveId(idTurno)
            Set turno = listaDisponibili.Item(chiaveTurno)
            Call listaSelezionati.Add(turno, chiaveTurno)
            Call listaDisponibili.Remove(chiaveTurno)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim i As Integer
    Dim idNuovaRappresentazione As Long
    Dim n As Long
    Dim turno As clsElementoLista
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovaRappresentazione = OttieniIdentificatoreDaSequenza("SQ_RAPPRESENTAZIONE")
    
'   INSERIMENTO IN TABELLA RAPPRESENTAZIONE
    sql = "INSERT INTO RAPPRESENTAZIONE (IDRAPPRESENTAZIONE, DATAORAINIZIO, CONTROLLOACCESSIABILITATO,"
    sql = sql & " DURATAINMINUTI, IDVENUE, IDSPETTACOLO, UTILIZZATORIDAMANTENERE, QUANTITAINGRESSIVENDIBILI)"
    sql = sql & " VALUES ("
    sql = sql & idNuovaRappresentazione & ", "
    sql = sql & SqlDateTimeValue(dataOraDaInserire) & ", "
    sql = sql & VB_FALSO & ", "
    sql = sql & durataDaInserire & ", "
    sql = sql & idVenueRapprSelezionata & ", "
    sql = sql & idSpettacoloSelezionato & ", "
    sql = sql & quantitaIngressiVendibili & ", "
    sql = sql & "0)"
    SETAConnection.Execute sql, n, adCmdText
    
'   INSERIMENTO IN TABELLA RAPPRESENTAZIONE_TURNORAPPR
    If Not (listaSelezionati Is Nothing) Then
        For Each turno In listaSelezionati
            sql = "INSERT INTO RAPPRESENTAZIONE_TURNORAPPR (IDTURNORAPPRESENTAZIONE,"
            sql = sql & " IDRAPPRESENTAZIONE)"
            sql = sql & " VALUES (" & turno.idElementoLista & ", "
            sql = sql & idNuovaRappresentazione & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next turno
    End If
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub lstCalendario_Init()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    i = 1
    lstCalendario.Clear
    
    Call ApriConnessioneBD
    Set listaIdRappresentazioniSelezionate = New Collection
    sql = "SELECT IDRAPPRESENTAZIONE, DATAORAINIZIO" & _
        " FROM RAPPRESENTAZIONE" & _
        " WHERE IDSPETTACOLO = " & idSpettacoloSelezionato & _
        " ORDER BY DATAORAINIZIO"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Call lstCalendario.AddItem(rec("DATAORAINIZIO"))
            lstCalendario.ItemData(i - 1) = rec("IDRAPPRESENTAZIONE")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    Call ChiudiConnessioneBD
    
End Sub

