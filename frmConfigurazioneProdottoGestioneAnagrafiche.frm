VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoGestioneAnagrafiche 
   Caption         =   "Gestione anagrafiche"
   ClientHeight    =   10410
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11565
   LinkTopic       =   "Form1"
   ScaleHeight     =   10410
   ScaleWidth      =   11565
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstParametriAnagraficaAlternativi 
      Height          =   645
      Index           =   5
      Left            =   9360
      MultiSelect     =   2  'Extended
      TabIndex        =   38
      Top             =   8040
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CommandButton cmdAggiungiAlternativo 
      Caption         =   "Aggiungi"
      Height          =   435
      Index           =   5
      Left            =   9360
      TabIndex        =   37
      Top             =   8760
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdEliminaAlternativo 
      Caption         =   "Elimina"
      Height          =   435
      Index           =   5
      Left            =   10440
      TabIndex        =   36
      Top             =   8760
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.ListBox lstParametriAnagraficaAlternativi 
      Height          =   645
      Index           =   4
      Left            =   9360
      MultiSelect     =   2  'Extended
      TabIndex        =   35
      Top             =   6720
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CommandButton cmdAggiungiAlternativo 
      Caption         =   "Aggiungi"
      Height          =   435
      Index           =   4
      Left            =   9360
      TabIndex        =   34
      Top             =   7440
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdEliminaAlternativo 
      Caption         =   "Elimina"
      Height          =   435
      Index           =   4
      Left            =   10440
      TabIndex        =   33
      Top             =   7440
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.ListBox lstParametriAnagraficaAlternativi 
      Height          =   645
      Index           =   3
      Left            =   9360
      MultiSelect     =   2  'Extended
      TabIndex        =   32
      Top             =   5400
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CommandButton cmdAggiungiAlternativo 
      Caption         =   "Aggiungi"
      Height          =   435
      Index           =   3
      Left            =   9360
      TabIndex        =   31
      Top             =   6120
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdEliminaAlternativo 
      Caption         =   "Elimina"
      Height          =   435
      Index           =   3
      Left            =   10440
      TabIndex        =   30
      Top             =   6120
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.ListBox lstParametriAnagraficaAlternativi 
      Height          =   645
      Index           =   2
      Left            =   9360
      MultiSelect     =   2  'Extended
      TabIndex        =   29
      Top             =   4080
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CommandButton cmdAggiungiAlternativo 
      Caption         =   "Aggiungi"
      Height          =   435
      Index           =   2
      Left            =   9360
      TabIndex        =   28
      Top             =   4800
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdEliminaAlternativo 
      Caption         =   "Elimina"
      Height          =   435
      Index           =   2
      Left            =   10440
      TabIndex        =   27
      Top             =   4800
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdCancella 
      Caption         =   "Cancella tutto"
      Height          =   555
      Left            =   120
      TabIndex        =   26
      Top             =   4200
      Width           =   2115
   End
   Begin VB.CommandButton cmdEliminaAlternativo 
      Caption         =   "Elimina"
      Height          =   435
      Index           =   1
      Left            =   10440
      TabIndex        =   25
      Top             =   3480
      Width           =   915
   End
   Begin VB.CommandButton cmdAggiungiAlternativo 
      Caption         =   "Aggiungi"
      Height          =   435
      Index           =   1
      Left            =   9360
      TabIndex        =   24
      Top             =   3480
      Width           =   915
   End
   Begin VB.ListBox lstParametriAnagraficaAlternativi 
      Height          =   645
      Index           =   1
      Left            =   9360
      MultiSelect     =   2  'Extended
      TabIndex        =   23
      Top             =   2760
      Width           =   2055
   End
   Begin VB.CommandButton cmdConfermaRuolo 
      Caption         =   "Conferma ruolo"
      Height          =   555
      Left            =   3720
      TabIndex        =   22
      Top             =   4200
      Width           =   1875
   End
   Begin VB.CommandButton cmdEliminaAlternativo 
      Caption         =   "Elimina"
      Height          =   435
      Index           =   0
      Left            =   10440
      TabIndex        =   21
      Top             =   2160
      Width           =   915
   End
   Begin VB.CommandButton cmdAggiungiAlternativo 
      Caption         =   "Aggiungi"
      Height          =   435
      Index           =   0
      Left            =   9360
      TabIndex        =   20
      Top             =   2160
      Width           =   915
   End
   Begin VB.CommandButton cmdAggiungiSceltaAlternativa 
      Caption         =   "Aggiungi scelta (max 6)"
      Height          =   555
      Left            =   8040
      TabIndex        =   18
      Top             =   4200
      Width           =   1155
   End
   Begin VB.ListBox lstParametriAnagraficaDisponibili 
      Height          =   2310
      Left            =   3720
      Style           =   1  'Checkbox
      TabIndex        =   17
      Top             =   1440
      Width           =   1935
   End
   Begin VB.CommandButton cmdRuoloTitolareNonFisico 
      Caption         =   "Titolare ente/societ�"
      Height          =   435
      Left            =   120
      TabIndex        =   16
      Top             =   3240
      Width           =   2115
   End
   Begin VB.CommandButton cmdRuoloTitolareFisico 
      Caption         =   "Titolare fisico"
      Height          =   435
      Left            =   120
      TabIndex        =   15
      Top             =   2640
      Width           =   2115
   End
   Begin VB.ListBox lstParametriAnagraficaObbligatori 
      Height          =   1815
      Left            =   5880
      MultiSelect     =   2  'Extended
      TabIndex        =   12
      Top             =   1440
      Width           =   2055
   End
   Begin VB.CommandButton cmdRuoloAcquirenteNonFisico 
      Caption         =   "Acquirente ente/societ�"
      Height          =   435
      Left            =   120
      TabIndex        =   11
      Top             =   2040
      Width           =   2115
   End
   Begin VB.CommandButton cmdRuoloAcquirenteFisico 
      Caption         =   "Acquirente fisico"
      Height          =   435
      Left            =   120
      TabIndex        =   10
      Top             =   1440
      Width           =   2115
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8040
      TabIndex        =   5
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   9780
      TabIndex        =   4
      Top             =   240
      Width           =   1635
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10080
      TabIndex        =   3
      Top             =   9720
      Width           =   1275
   End
   Begin VB.ListBox lstParametriAnagraficaAlternativi 
      Height          =   645
      Index           =   0
      Left            =   9300
      MultiSelect     =   2  'Extended
      TabIndex        =   2
      Top             =   1440
      Width           =   2055
   End
   Begin VB.CommandButton cmdAggiungiObbligatorio 
      Caption         =   "Aggiungi"
      Height          =   435
      Left            =   5880
      TabIndex        =   1
      Top             =   3360
      Width           =   915
   End
   Begin VB.CommandButton cmdEliminaObbligatorio 
      Caption         =   "Elimina"
      Height          =   435
      Left            =   6960
      TabIndex        =   0
      Top             =   3360
      Width           =   915
   End
   Begin VB.Label Label2 
      Caption         =   "Parametri disponibili"
      Height          =   255
      Left            =   3720
      TabIndex        =   19
      Top             =   1080
      Width           =   2055
   End
   Begin VB.Label Label4 
      Caption         =   "Scelte alternative"
      Height          =   255
      Left            =   9360
      TabIndex        =   14
      Top             =   1080
      Width           =   1935
   End
   Begin VB.Label Label3 
      Caption         =   "Parametri obbligatori"
      Height          =   255
      Left            =   5880
      TabIndex        =   13
      Top             =   1080
      Width           =   2055
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Gestione tipi anagrafiche"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8040
      TabIndex        =   8
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9780
      TabIndex        =   7
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label Label1 
      Caption         =   "Ruoli"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   1935
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoGestioneAnagrafiche"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idLivelloGestioneAnagraficheSelezionato As Long
Private idRuoloAnagraficaCorrente As Long
Private idTipoAnagraficaCorrente As Long
Private qtaAlternativePresentiPerRuolo As Integer
Private qtaListePresentiPerRuolo As Integer
Private internalEvent As Boolean
Private operazioniEffettuate As Boolean

Private listaIdTipiAnagraficheDaAggiungere As Collection

Private prodottoRientraDecretoSicurezza As ValoreBooleanoEnum
Private numeroMassimoTitoliPerAcqProdotto As Long

Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
End Sub

Public Sub Init(idPianta As Long, idProdotto As Long, nomeProdotto As String, idOrganizzazione As Long, nomeOrganizzazione As String, idLivelloGestioneAnagrafiche As Long)

    idPiantaSelezionata = idPianta
    idProdottoSelezionato = idProdotto
    nomeProdottoSelezionato = nomeProdotto
    idOrganizzazioneSelezionata = idOrganizzazione
    nomeOrganizzazioneSelezionata = nomeOrganizzazione
    idLivelloGestioneAnagraficheSelezionato = idLivelloGestioneAnagrafiche
    
    idRuoloAnagraficaCorrente = idNessunElementoSelezionato
    idTipoAnagraficaCorrente = idNessunElementoSelezionato
    qtaAlternativePresentiPerRuolo = 0
    
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    
    Call AggiornaAbilitazioneControlli
    PopolaListaParametriAnagraficaDisponibili
    qtaAlternativePresentiPerRuolo = 0
    cmdConfermaRuolo.Enabled = False
    operazioniEffettuate = False
    If idLivelloGestioneAnagrafiche = LGA_LIVELLO_4 Then
        cmdRuoloAcquirenteNonFisico.Enabled = False
        cmdRuoloTitolareNonFisico.Enabled = False
    End If
    
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    prodottoRientraDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMassimoTitoliPerAcqProdotto = n
End Sub

Private Sub cmdAggiungiAlternativo_Click(Index As Integer)
    Call AggiungiAlternativo(Index)
End Sub

Private Sub cmdAggiungiObbligatorio_Click()
    Call aggiungiObbligatorio
End Sub

Private Sub cmdAggiungiSceltaAlternativa_Click()
    Call AggiungiSceltaAlternativa
End Sub

Private Sub cmdCancella_Click()
    Dim mousePointerOld As Integer
    Dim risposta As VbMsgBoxResult
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    risposta = MsgBox("Sei sicuro di cancellare tutte le configurazioni delle anagrafiche per questo prodotto?", vbYesNo)
    If risposta = vbYes Then
        Call CancellaTutto
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConfermaRuolo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfermaRuolo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEliminaAlternativo_Click(Index As Integer)
    Call EliminaAlternativo(Index)
End Sub

Private Sub cmdEliminaObbligatorio_Click()
    Call EliminaObbligatorio
End Sub

Private Sub cmdAggiungiTipiSelezionati_Click()
    Call InserisciTipiAnagraficaNellaBaseDati
End Sub

Private Sub cmdAggiungiTipoAnagrafica_Click()
    Call AggiungiTipiAnagrafica
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEliminaTipoAnagrafica_Click()
    Call EliminaTipiAnagrafica
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Dim risposta As VbMsgBoxResult
    
    If operazioniEffettuate Then
        risposta = MsgBox("Ci sono operazioni in sospeso non salvate: sei sicuro di voler uscire?", vbYesNo)
        If risposta = vbYes Then
            Unload Me
        End If
    Else
        Unload Me
    End If
End Sub

' Da completare
Private Function campiObbligatoriPresenti() As Boolean
    Dim i As Integer
    Dim trovato As Boolean
    Dim qtaAlternativeNonVuote As Integer
    
    campiObbligatoriPresenti = True
    
    If idRuoloAnagraficaCorrente = RA_ACQUIRENTE_FISICO Or idRuoloAnagraficaCorrente = RA_TITOLARE_FISICO Then
        trovato = False
        For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
            If lstParametriAnagraficaObbligatori.ItemData(i) = 1 Then ' COGNOME
                trovato = True
            End If
        Next i
        campiObbligatoriPresenti = campiObbligatoriPresenti And trovato
        
        trovato = False
        For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
            If lstParametriAnagraficaObbligatori.ItemData(i) = 2 Then ' NOME
                trovato = True
            End If
        Next i
        campiObbligatoriPresenti = campiObbligatoriPresenti And trovato
        
        If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_4) Then
            trovato = False
            For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
                If lstParametriAnagraficaObbligatori.ItemData(i) = 4 Then ' DATA_NASCITA
                    trovato = True
                End If
            Next i
            campiObbligatoriPresenti = campiObbligatoriPresenti And trovato
            
            trovato = False
            For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
                If lstParametriAnagraficaObbligatori.ItemData(i) = 5 Then ' LUOGO_NASCITA
                    trovato = True
                End If
            Next i
            campiObbligatoriPresenti = campiObbligatoriPresenti And trovato
            
            trovato = False
            For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
                If lstParametriAnagraficaObbligatori.ItemData(i) = 6 Then ' SESSO
                    trovato = True
                End If
            Next i
            campiObbligatoriPresenti = campiObbligatoriPresenti And trovato
            
        End If
        
    Else
        trovato = False
        For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
            If lstParametriAnagraficaObbligatori.ItemData(i) = 3 Then ' DENOMINAZIONE
                trovato = True
            End If
        Next i
        campiObbligatoriPresenti = campiObbligatoriPresenti And trovato
    End If
    
    ' verifica che ci siano almeno due alternative non vuote
    qtaAlternativeNonVuote = 0
    For i = 0 To qtaListePresentiPerRuolo - 1
        If lstParametriAnagraficaAlternativi(i).ListCount > 0 Then
            qtaAlternativeNonVuote = qtaAlternativeNonVuote + 1
        End If
    Next i
    If qtaAlternativeNonVuote = 1 Then
        campiObbligatoriPresenti = False
    End If
    
End Function

' cerca il tipo anagrafica da utilizzare, se non esiste lo crea
' indice = -1 indica che non ci sono alternative ma solo i campi obbligatori
Private Function cercaIdTipoAnagrafica(indiceScelteAlternative As Integer) As Long
    Dim sql As String
    Dim rec As OraDynaset
    Dim sIn As String
    Dim i As Integer
    Dim qtaParametri As Integer
    Dim qtaTipiAnagraficaTrovati As Integer
    Dim newId As Long
    Dim nome, descrizione As String
    Dim n As Integer
    
    sIn = "(-1"
    For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
        sIn = sIn & "," & lstParametriAnagraficaObbligatori.ItemData(i)
    Next i
    qtaParametri = lstParametriAnagraficaObbligatori.ListCount
    
    If indiceScelteAlternative > -1 Then   ' indice scelta alternativa
        For i = 0 To lstParametriAnagraficaAlternativi(indiceScelteAlternative).ListCount - 1
            sIn = sIn & "," & lstParametriAnagraficaAlternativi(indiceScelteAlternative).ItemData(i)
        Next i
        qtaParametri = qtaParametri + lstParametriAnagraficaAlternativi(indiceScelteAlternative).ListCount
    End If
    
    sIn = sIn & ")"

    qtaTipiAnagraficaTrovati = 0
    sql = "SELECT A.IDTIPOANAGRAFICA" & _
            " FROM (" & _
            " SELECT TA.IDTIPOANAGRAFICA, COUNT(*) QTA" & _
            " FROM TIPOANAGRAFICA TA" & _
            " INNER JOIN UTILIZZOPARAMETROANAGRAFICA UPA ON TA.IDTIPOANAGRAFICA = UPA.IDTIPOANAGRAFICA" & _
            " INNER JOIN (" & _
            "       SELECT IDPARAMETROANAGRAFICA" & _
            "       FROM PARAMETROANAGRAFICA" & _
            "       WHERE IDPARAMETROANAGRAFICA IN " & sIn & _
            " ) CT ON UPA.IDPARAMETROANAGRAFICA = CT.IDPARAMETROANAGRAFICA" & _
            " GROUP BY TA.IDTIPOANAGRAFICA" & _
            " HAVING COUNT(*) = " & qtaParametri & _
            ") A," & _
            " (" & _
            " SELECT IDTIPOANAGRAFICA, COUNT(*) QTA" & _
            " FROM UTILIZZOPARAMETROANAGRAFICA" & _
            " GROUP BY IDTIPOANAGRAFICA) B" & _
            " WHERE A.IDTIPOANAGRAFICA = B.IDTIPOANAGRAFICA" & _
            " AND A.QTA = B.QTA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            cercaIdTipoAnagrafica = rec("IDTIPOANAGRAFICA")
            qtaTipiAnagraficaTrovati = qtaTipiAnagraficaTrovati + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    If qtaTipiAnagraficaTrovati = 0 Then ' crea il tipo anagrafica
        ' la sequence non esiste, sezione critica per individuare il nuovo id
        sql = "SELECT MAX(IDTIPOANAGRAFICA) NEWID FROM TIPOANAGRAFICA"
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            newId = rec("NEWID") + 1
        End If
        rec.Close
        cercaIdTipoAnagrafica = newId
        
        nome = "Nuovo: " & Mid(sIn, 5, Len(sIn) - 5)
        descrizione = "Parametri: "
        sql = "SELECT CODICE FROM PARAMETROANAGRAFICA WHERE IDPARAMETROANAGRAFICA IN " & sIn
        Set rec = ORADB.CreateDynaset(sql, 0&)
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                descrizione = descrizione & rec("CODICE") & " "
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        sql = "INSERT INTO TIPOANAGRAFICA (IDTIPOANAGRAFICA, NOME, DESCRIZIONE)" & _
                " VALUES (" & newId & ", '" & nome & "', '" & descrizione & "')"
        n = ORADB.ExecuteSQL(sql)
                
        sql = "INSERT INTO UTILIZZOPARAMETROANAGRAFICA (IDTIPOANAGRAFICA, IDPARAMETROANAGRAFICA)" & _
                " SELECT " & newId & ", IDPARAMETROANAGRAFICA" & _
                " FROM PARAMETROANAGRAFICA" & _
                " WHERE IDPARAMETROANAGRAFICA IN " & sIn
        n = ORADB.ExecuteSQL(sql)
        
    ElseIf qtaTipiAnagraficaTrovati > 1 Then ' problema sul DB!!!
        MsgBox "Tipo anagrafica duplicato! Risolvere l'anomalia sul DB"
        cercaIdTipoAnagrafica = -1
    End If
    
End Function

Private Sub ConfermaRuolo()
    Dim sql As String
    Dim i As Integer
    Dim n As Integer
    Dim qtaAlternativeNonVuote As Long
    Dim idTipoAnagrafica As Long
    Dim continua As Boolean
    
On Error GoTo errori_ConfermaRuolo

    If campiObbligatoriPresenti Then
        Call ApriConnessioneBD_ORA
        ORADB.BeginTrans
    
        ' cancellaConfigurazioneCorrente
        sql = "DELETE FROM PRODOTTO_RUOLO_TIPOANAGRAFICA" & _
                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                " AND IDRUOLOANAGRAFICA = " & idRuoloAnagraficaCorrente
        n = ORADB.ExecuteSQL(sql)
                
        ' verifica che ci siano almeno due alternative non vuote, altrimenti � tutto obbligatorio
        qtaAlternativeNonVuote = 0
        For i = 0 To qtaListePresentiPerRuolo - 1
            If lstParametriAnagraficaAlternativi(i).ListCount > 0 Then
                qtaAlternativeNonVuote = qtaAlternativeNonVuote + 1
            End If
        Next i
        
        If qtaAlternativeNonVuote = 0 Then
            idTipoAnagrafica = cercaIdTipoAnagrafica(-1)
            If idTipoAnagrafica > -1 Then
                sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
                        " VALUES (" & idProdottoSelezionato & ", " & idRuoloAnagraficaCorrente & ", " & idTipoAnagrafica & ")"
                n = ORADB.ExecuteSQL(sql)
            Else
                ORADB.Rollback
            End If
        Else
            i = 0
            continua = True
            While i < qtaListePresentiPerRuolo And continua
                If lstParametriAnagraficaAlternativi(i).ListCount > 0 Then
                    idTipoAnagrafica = cercaIdTipoAnagrafica(i)
                    If idTipoAnagrafica > -1 Then
                        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
                                " SELECT " & idProdottoSelezionato & ", " & idRuoloAnagraficaCorrente & ", " & idTipoAnagrafica & _
                                " FROM DUAL" & _
                                " WHERE NOT EXISTS (" & _
                                " SELECT IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA" & _
                                " FROM PRODOTTO_RUOLO_TIPOANAGRAFICA" & _
                                " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
                                " AND IDRUOLOANAGRAFICA = " & idRuoloAnagraficaCorrente & _
                                " AND IDTIPOANAGRAFICA = " & idTipoAnagrafica & _
                                ")"
                        n = ORADB.ExecuteSQL(sql)
                    Else
                        ORADB.Rollback
                        continua = False
                    End If
                End If
                i = i + 1
            Wend
        End If
        
        ORADB.CommitTrans
        Call ChiudiConnessioneBD_ORA
        
        Call PulisciListe
        cmdRuoloAcquirenteFisico.Font.Bold = False
        cmdRuoloAcquirenteNonFisico.Font.Bold = False
        cmdRuoloTitolareFisico.Font.Bold = False
        cmdRuoloTitolareNonFisico.Font.Bold = False
        
        cmdConfermaRuolo.Enabled = False
        operazioniEffettuate = False
        
    Else
        MsgBox "Non tutti i campi richiesti (NOME e COGNOME oppure DENOMINAZIONE o CINQUINA per prodotti che rientrano nel Decreto Sicurezza) sono presenti o scelte alternative non congruenti"
    End If
    Exit Sub

errori_ConfermaRuolo:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CancellaTutto()
    Dim sql As String
    Dim n As Integer

    cmdRuoloAcquirenteFisico.Font.Bold = False
    cmdRuoloAcquirenteNonFisico.Font.Bold = False
    cmdRuoloTitolareFisico.Font.Bold = False
    cmdRuoloTitolareNonFisico.Font.Bold = False
    
    Call ApriConnessioneBD_ORA
'    ORADB.BeginTrans

    ' cancella le configurazioni anagrafiche del prodotto
    ' tranne quelle obbligatorie
    sql = "DELETE FROM PRODOTTO_RUOLO_TIPOANAGRAFICA" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
    n = ORADB.ExecuteSQL(sql)
    
    If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_1 Or idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_3) Then
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idProdottoSelezionato & ", " & RA_TITOLARE_FISICO & ", 3)"
        SETAConnection.Execute sql, n, adCmdText
    End If
    If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_2 Or idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_3) Then
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idProdottoSelezionato & ", " & RA_ACQUIRENTE_FISICO & ", 3)"
        SETAConnection.Execute sql, n, adCmdText
    End If
    If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_4) Then
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idProdottoSelezionato & ", " & RA_TITOLARE_FISICO & ", 1)"
        SETAConnection.Execute sql, n, adCmdText
        sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
            " VALUES(" & idProdottoSelezionato & ", " & RA_ACQUIRENTE_FISICO & ", 1)"
        SETAConnection.Execute sql, n, adCmdText
    End If
    
'    ORADB.CommitTrans
    Call ChiudiConnessioneBD_ORA

    Call PulisciListe
End Sub

Private Sub Conferma()
    
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub AggiungiTipiAnagrafica()
    Dim sql As String
    Dim n As Long
    Dim idSuperarea As Long
    Dim idTemplate As Long
    Dim i As Long
    Dim j As Long
    
On Error GoTo errori_Anagrafiche_Aggiungi

    lstParametriAnagraficaDisponibili.Clear
    lstParametriAnagraficaDisponibili.Visible = True
    
    PopolaListaTipiAnagraficaDisponibili
    
    Call AggiornaAbilitazioneControlli

    Exit Sub

errori_Anagrafiche_Aggiungi:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub InserisciTipiAnagraficaNellaBaseDati()
    Dim sql As String
    Dim i As Long
    Dim n As Long
    Dim idTipoAnagrafica As Long

On Error GoTo errori_InserisciTipiAnagrafica

'    Call ApriConnessioneBD_ORA
'    ORADB.BeginTrans
'
'    For i = 1 To lstTipiAnagraficaDisponibili.ListCount
'        If lstTipiAnagraficaDisponibili.Selected(i - 1) Then
'            idTipoAnagrafica = lstTipiAnagraficaDisponibili.ItemData(i - 1)
'            sql = "INSERT INTO PRODOTTO_RUOLO_TIPOANAGRAFICA (IDPRODOTTO, IDRUOLOANAGRAFICA, IDTIPOANAGRAFICA)" & _
'                    " SELECT " & idProdottoSelezionato & ", " & idRuoloAnagraficaCorrente & ", " & idTipoAnagrafica & _
'                    " FROM DUAL" & _
'                    " WHERE (" & _
'                    " SELECT COUNT(*)" & _
'                    " FROM PRODOTTO_RUOLO_TIPOANAGRAFICA" & _
'                    " WHERE IDPRODOTTO = " & idProdottoSelezionato & " AND IDRUOLOANAGRAFICA = " & idRuoloAnagraficaCorrente & " AND IDTIPOANAGRAFICA = " & idTipoAnagrafica & _
'                    " ) = 0"
'            n = ORADB.ExecuteSQL(sql)
'        End If
'    Next i
'
'    ORADB.CommitTrans
'    Call ChiudiConnessioneBD_ORA
'
'    Call PopolaListaTipiAnagrafica
'    lstTipiAnagraficaDisponibili.Visible = False
'    cmdAggiungiTipiSelezionati.Visible = False
'    cmdAggiungiTipoAnagrafica.Visible = True
'    cmdEliminaTipoAnagrafica.Visible = True
'
'    Call AggiornaAbilitazioneControlli

    Exit Sub

errori_InserisciTipiAnagrafica:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)


End Sub

Private Sub EliminaObbligatorio()
    Dim i As Long
    Dim idParametroSelezionato As Long

    idParametroSelezionato = idNessunElementoSelezionato

    i = 1
    While i <= lstParametriAnagraficaObbligatori.ListCount And idParametroSelezionato = idNessunElementoSelezionato
        If lstParametriAnagraficaObbligatori.Selected(i - 1) Then
            idParametroSelezionato = lstParametriAnagraficaObbligatori.ItemData(i - 1)
            If idRuoloAnagraficaCorrente = RA_ACQUIRENTE_FISICO And (idParametroSelezionato = IDPARAMETROANAGRAFICA_COGNOME Or idParametroSelezionato = IDPARAMETROANAGRAFICA_NOME) Or _
                        idRuoloAnagraficaCorrente = RA_ACQUIRENTE_NON_FISICO And idParametroSelezionato = IDPARAMETROANAGRAFICA_DENOMINAZIONE Or _
                        idRuoloAnagraficaCorrente = RA_TITOLARE_FISICO And (idParametroSelezionato = IDPARAMETROANAGRAFICA_COGNOME Or idParametroSelezionato = IDPARAMETROANAGRAFICA_NOME) Or _
                        idRuoloAnagraficaCorrente = RA_TITOLARE_NON_FISICO And idParametroSelezionato = IDPARAMETROANAGRAFICA_DENOMINAZIONE Then
                MsgBox "Parametro non eliminabile"
            Else
                lstParametriAnagraficaObbligatori.RemoveItem (i - 1)
                operazioniEffettuate = True
                cmdConfermaRuolo.Enabled = True
            End If
        End If
        i = i + 1
    Wend
    
End Sub

Private Sub EliminaParametroDaLista(lst As listBox, id As Long)
    Dim i As Long
    Dim idParametroCancellato As Long

    idParametroCancellato = idNessunElementoSelezionato

    i = 1
    While i <= lst.ListCount And idParametroCancellato = idNessunElementoSelezionato
        If lst.ItemData(i - 1) = id Then
            idParametroCancellato = lst.ItemData(i - 1)
            lst.RemoveItem (i - 1)
        End If
        i = i + 1
    Wend
    
End Sub
            
Private Function isParametroInListbox(lst As listBox, idParametro As Long)
    Dim i As Long

    isParametroInListbox = False
    For i = 0 To lst.ListCount - 1
        If lst.ItemData(i) = idParametro Then
            isParametroInListbox = True
        End If
    Next i
End Function

Private Sub spostaParametroInObbligatorio(idParametro As Long)
    Dim i, j, newIndex As Integer
    Dim spostato As Boolean
    Dim codice As String
    
    For i = 0 To qtaListePresentiPerRuolo - 1
        j = 0
        spostato = False
        While j < lstParametriAnagraficaAlternativi(i).ListCount And Not spostato
            If lstParametriAnagraficaAlternativi(i).ItemData(j) = idParametro Then
                lstParametriAnagraficaAlternativi(i).RemoveItem (j)
                spostato = True
            End If
            j = j + 1
        Wend
    Next i
    
    codice = ""
    For i = 0 To lstParametriAnagraficaDisponibili.ListCount - 1
        If lstParametriAnagraficaDisponibili.ItemData(i) = idParametro Then
            codice = lstParametriAnagraficaDisponibili.List(i)
        End If
    Next i
    
    newIndex = lstParametriAnagraficaObbligatori.ListCount
    lstParametriAnagraficaObbligatori.AddItem codice
    lstParametriAnagraficaObbligatori.ItemData(newIndex) = idParametro
    
End Sub

Private Sub AggiungiAlternativo(Index As Integer)
    Dim i As Long
    Dim j As Long
    Dim k As Long
    Dim idParametroSelezionato As Long
    Dim codiceParametroSelezionato As String
    Dim indiceAlternativaPiena As Integer
    Dim idParametro As Long
    Dim parametroDaSpostare As Boolean
    Dim listaIdParametroDaSpostareInObbligatori(10) As Long
    Dim ids As Integer
    Dim tutteAlternativePiene As Boolean
    
    For i = 0 To lstParametriAnagraficaDisponibili.ListCount - 1
        If lstParametriAnagraficaDisponibili.Selected(i) Then
            idParametroSelezionato = lstParametriAnagraficaDisponibili.ItemData(i)
            codiceParametroSelezionato = lstParametriAnagraficaDisponibili.List(i)
            
            If (Not (isParametroObbligatorio(idParametroSelezionato))) Then
                If (Not (isParametroAlternativo(Index, idParametroSelezionato))) Then
                    lstParametriAnagraficaAlternativi(Index).AddItem codiceParametroSelezionato
                    lstParametriAnagraficaAlternativi(Index).ItemData(lstParametriAnagraficaAlternativi(Index).ListCount - 1) = idParametroSelezionato
    
                    operazioniEffettuate = True
                    cmdConfermaRuolo.Enabled = True
                End If
            End If
        End If
    Next i
    
    ' qui bisogna verificare se i campi aggiunti sono presenti in tutte le alternative e quindi spostarli sugli obbligatori
    tutteAlternativePiene = True
    parametroDaSpostare = True
    While tutteAlternativePiene = True And parametroDaSpostare = True
        ids = 0
        tutteAlternativePiene = True
        For i = 0 To qtaListePresentiPerRuolo - 1
            If lstParametriAnagraficaAlternativi(i).ListCount = 0 Then
                tutteAlternativePiene = False
            End If
        Next i
        
        If tutteAlternativePiene = True Then
            For j = 0 To lstParametriAnagraficaAlternativi(0).ListCount - 1
                idParametro = lstParametriAnagraficaAlternativi(0).ItemData(j)
                parametroDaSpostare = True
                For k = 1 To qtaListePresentiPerRuolo - 1
                    If Not isParametroInListbox(lstParametriAnagraficaAlternativi(k), idParametro) Then
                        parametroDaSpostare = False
                    End If
                Next k
                If parametroDaSpostare Then
                    listaIdParametroDaSpostareInObbligatori(ids) = idParametro
                    ids = ids + 1
                End If
            Next j
        End If
    
        For ids = 0 To 9
            If (listaIdParametroDaSpostareInObbligatori(ids) > 0) Then
                Call spostaParametroInObbligatorio(listaIdParametroDaSpostareInObbligatori(ids))
            End If
        Next ids
        For ids = 0 To 9
            listaIdParametroDaSpostareInObbligatori(ids) = 0
        Next ids
    Wend
    
    Call deselezionaParametriDisponibili
End Sub

Private Sub EliminaAlternativo(Index As Integer)
    Dim i As Long
    Dim j As Long
    Dim idParametroCancellato As Long
    Dim codiceParametroSelezionato As String
    
    idParametroCancellato = idNessunElementoSelezionato
    For i = 1 To lstParametriAnagraficaAlternativi(Index).ListCount
        If idParametroCancellato = idNessunElementoSelezionato Then
            If lstParametriAnagraficaAlternativi(Index).Selected(i - 1) Then
                idParametroCancellato = lstParametriAnagraficaAlternativi(Index).ItemData(i - 1)
                lstParametriAnagraficaAlternativi(Index).RemoveItem (i - 1)
    
                operazioniEffettuate = True
                cmdConfermaRuolo.Enabled = True
            End If
        End If
    Next i
End Sub

Private Function aggiungiObbligatorioALista(idParametro As Long, codiceParametro As String) As Boolean
    Dim j As Long
    
    aggiungiObbligatorioALista = False
    
    If (Not (isParametroObbligatorio(idParametro))) Then
        lstParametriAnagraficaObbligatori.AddItem codiceParametro
        lstParametriAnagraficaObbligatori.ItemData(lstParametriAnagraficaObbligatori.ListCount - 1) = idParametro
        
        For j = 0 To qtaAlternativePresentiPerRuolo - 1
            Call EliminaParametroDaLista(lstParametriAnagraficaAlternativi(j), idParametro)
        Next j
        
        aggiungiObbligatorioALista = True
        
    End If
End Function

Private Sub aggiungiObbligatorio()
    Dim i As Long
    Dim j As Long
    Dim idParametroSelezionato As Long
    Dim codiceParametroSelezionato As String
    
    For i = 1 To lstParametriAnagraficaDisponibili.ListCount
        If lstParametriAnagraficaDisponibili.Selected(i - 1) Then
            idParametroSelezionato = lstParametriAnagraficaDisponibili.ItemData(i - 1)
            codiceParametroSelezionato = lstParametriAnagraficaDisponibili.List(i - 1)
            
'            If (Not (isParametroObbligatorio(idParametroSelezionato))) Then
'                lstParametriAnagraficaObbligatori.AddItem codiceParametroSelezionato
'                lstParametriAnagraficaObbligatori.ItemData(lstParametriAnagraficaObbligatori.ListCount - 1) = idParametroSelezionato
'
'                For j = 0 To qtaAlternativePresentiPerRuolo - 1
'                    Call EliminaParametroDaLista(lstParametriAnagraficaAlternativi(j), idParametroSelezionato)
'                Next j
'
'                operazioniEffettuate = True
'                cmdConfermaRuolo.Enabled = True
'
'            End If
            If aggiungiObbligatorioALista(idParametroSelezionato, codiceParametroSelezionato) = True Then
                operazioniEffettuate = True
                cmdConfermaRuolo.Enabled = True
            End If
        End If
    Next i
    
    Call deselezionaParametriDisponibili

End Sub

Private Sub AggiungiSceltaAlternativa()
    Dim i As Integer
    Dim esisteListaVuota As Boolean
    
    esisteListaVuota = False
    For i = 0 To qtaListePresentiPerRuolo - 1
        If lstParametriAnagraficaAlternativi(i).ListCount = 0 Then
            esisteListaVuota = True
        End If
    Next i
    
    If Not esisteListaVuota Then
'        Load lstParametriAnagraficaAlternativi(qtaListePresentiPerRuolo)
'        lstParametriAnagraficaAlternativi(qtaListePresentiPerRuolo).Left = 9300
'        lstParametriAnagraficaAlternativi(qtaListePresentiPerRuolo).tOp = 1440 + 1900 * qtaListePresentiPerRuolo
'        lstParametriAnagraficaAlternativi(qtaListePresentiPerRuolo).Visible = True
'        Load cmdAggiungiAlternativo(qtaListePresentiPerRuolo)
'        cmdAggiungiAlternativo(qtaListePresentiPerRuolo).Left = 9360
'        cmdAggiungiAlternativo(qtaListePresentiPerRuolo).tOp = 2760 + 1900 * qtaListePresentiPerRuolo
'        cmdAggiungiAlternativo(qtaListePresentiPerRuolo).Visible = True
'        Load cmdEliminaAlternativo(qtaListePresentiPerRuolo)
'        cmdEliminaAlternativo(qtaListePresentiPerRuolo).Left = 10440
'        cmdEliminaAlternativo(qtaListePresentiPerRuolo).tOp = 2760 + 1900 * qtaListePresentiPerRuolo
'        cmdEliminaAlternativo(qtaListePresentiPerRuolo).Visible = True
        lstParametriAnagraficaAlternativi(qtaListePresentiPerRuolo).Visible = True
        cmdAggiungiAlternativo(qtaListePresentiPerRuolo).Visible = True
        cmdEliminaAlternativo(qtaListePresentiPerRuolo).Visible = True
        
        
        qtaListePresentiPerRuolo = qtaListePresentiPerRuolo + 1
    End If
    
End Sub

Private Sub EliminaTipiAnagrafica()
    Dim sql As String
    Dim n As Long
    Dim i As Integer
    Dim idTipoAnagrafica As Long
        
On Error GoTo errori_EliminaTipiAnagrafica

'    Call ApriConnessioneBD_ORA
'    ORADB.BeginTrans
'
'    For i = 1 To lstTipiAnagrafica.ListCount
'        If lstTipiAnagrafica.Selected(i - 1) Then
'            idTipoAnagrafica = lstTipiAnagrafica.ItemData(i - 1)
'            sql = "DELETE FROM PRODOTTO_RUOLO_TIPOANAGRAFICA WHERE IDPRODOTTO = " & idProdottoSelezionato & " AND IDRUOLOANAGRAFICA= " & idRuoloAnagraficaCorrente & " AND IDTIPOANAGRAFICA= " & idTipoAnagraficaCorrente
'            n = ORADB.ExecuteSQL(sql)
'        End If
'    Next i
'
'    ORADB.CommitTrans
'    Call ChiudiConnessioneBD_ORA
'
'    Call PopolaListaTipiAnagrafica
'
'    Call AggiornaAbilitazioneControlli

    Exit Sub

errori_EliminaTipiAnagrafica:
    ORADB.Rollback
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub PopolaListaParametriAnagraficaDisponibili()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long

    lstParametriAnagraficaDisponibili.Clear
    idTipoAnagraficaCorrente = idNessunElementoSelezionato

    ' non viene considerato il luogo di residenza
    ' riaggiunto il 6 settembre 2019
'    sql = "SELECT PA.IDPARAMETROANAGRAFICA, PA.CODICE" & _
'            " FROM PARAMETROANAGRAFICA PA" & _
'            " WHERE IDPARAMETROANAGRAFICA <> 10" & _
'            " ORDER BY PA.CODICE"
    sql = "SELECT PA.IDPARAMETROANAGRAFICA, PA.CODICE" & _
            " FROM PARAMETROANAGRAFICA PA" & _
            " ORDER BY PA.CODICE"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstParametriAnagraficaDisponibili.AddItem rec("CODICE")
            lstParametriAnagraficaDisponibili.ItemData(i) = rec("IDPARAMETROANAGRAFICA")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close

End Sub

Private Sub contaAlternativePresentiPerRuolo()
    Dim sql As String
    Dim rec As OraDynaset
    Dim sql1 As String
    Dim rec1 As OraDynaset
    Dim i As Long
    Dim j As Long
    Dim idTipoAnagrafica As Long

    qtaAlternativePresentiPerRuolo = 0
    
    sql = "SELECT COUNT(DISTINCT IDTIPOANAGRAFICA) QTA" & _
            " FROM PRODOTTO_RUOLO_TIPOANAGRAFICA PRT" & _
            " WHERE PRT.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDRUOLOANAGRAFICA = " & idRuoloAnagraficaCorrente
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        qtaAlternativePresentiPerRuolo = rec("QTA")
    End If
    rec.Close
End Sub

Private Sub PulisciListe()
    Dim i As Long
    
    Call deselezionaParametriDisponibili
    lstParametriAnagraficaObbligatori.Clear
    lstParametriAnagraficaAlternativi(0).Clear
    lstParametriAnagraficaAlternativi(1).Clear
    For i = 2 To 6 - 1
        lstParametriAnagraficaAlternativi(i).Clear
        lstParametriAnagraficaAlternativi(i).Visible = False
        cmdAggiungiAlternativo(i).Visible = False
        cmdEliminaAlternativo(i).Visible = False
    Next i
End Sub

Private Sub PopolaListe()
    Dim sql As String
    Dim rec As OraDynaset
    Dim sql1 As String
    Dim rec1 As OraDynaset
    Dim i As Long
    Dim j As Long
    Dim idTipoAnagrafica As Long

    lstParametriAnagraficaObbligatori.Clear
    
    sql = "SELECT PA.IDPARAMETROANAGRAFICA, PA.CODICE, COUNT(*)" & _
            " FROM PRODOTTO_RUOLO_TIPOANAGRAFICA PRT" & _
            " INNER JOIN TIPOANAGRAFICA TA ON PRT.IDTIPOANAGRAFICA = TA.IDTIPOANAGRAFICA" & _
            " INNER JOIN UTILIZZOPARAMETROANAGRAFICA UPA ON TA.IDTIPOANAGRAFICA = UPA.IDTIPOANAGRAFICA" & _
            " INNER JOIN PARAMETROANAGRAFICA PA ON UPA.IDPARAMETROANAGRAFICA = PA.IDPARAMETROANAGRAFICA" & _
            " WHERE PRT.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDRUOLOANAGRAFICA = " & idRuoloAnagraficaCorrente & _
            " GROUP BY PA.IDPARAMETROANAGRAFICA, PA.CODICE" & _
            " HAVING COUNT(*) = " & qtaAlternativePresentiPerRuolo & _
            " ORDER BY PA.CODICE"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            lstParametriAnagraficaObbligatori.AddItem rec("CODICE")
            lstParametriAnagraficaObbligatori.ItemData(i) = rec("IDPARAMETROANAGRAFICA")
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
'    ' titolare obbligatorio
'    If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_1 Or idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_3) _
'                And (idRuoloAnagraficaCorrente = RA_TITOLARE_FISICO) Then
'        Call aggiungiObbligatorioALista(1, "COGNOME")
'        Call aggiungiObbligatorioALista(2, "NOME")
'    End If
'
'    ' acquirente obbligatorio
'    If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_2 Or idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_3) _
'                And (idRuoloAnagraficaCorrente = RA_ACQUIRENTE_FISICO) Then
'        Call aggiungiObbligatorioALista(1, "COGNOME")
'        Call aggiungiObbligatorioALista(2, "NOME")
'    End If
'
'    ' Decreto sicurezza
'    If (idLivelloGestioneAnagraficheSelezionato = LGA_LIVELLO_4) _
'                And (idRuoloAnagraficaCorrente = RA_ACQUIRENTE_FISICO Or idRuoloAnagraficaCorrente = RA_TITOLARE_FISICO) Then
'        Call aggiungiObbligatorioALista(1, "COGNOME")
'        Call aggiungiObbligatorioALista(2, "NOME")
'        Call aggiungiObbligatorioALista(4, "DATA_NASCITA")
'        Call aggiungiObbligatorioALista(5, "LUOGO_NASCITA")
'        Call aggiungiObbligatorioALista(6, "SESSO")
'    End If
    
    i = 0
    sql = "SELECT DISTINCT IDTIPOANAGRAFICA" & _
            " FROM PRODOTTO_RUOLO_TIPOANAGRAFICA PRT" & _
            " WHERE PRT.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDRUOLOANAGRAFICA = " & idRuoloAnagraficaCorrente
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            If i > 1 Then
                lstParametriAnagraficaAlternativi(i).Visible = True
                cmdAggiungiAlternativo(i).Visible = True
                cmdEliminaAlternativo(i).Visible = True
            End If
            
            idTipoAnagrafica = rec("IDTIPOANAGRAFICA")
            
            sql1 = "SELECT DISTINCT PA.IDPARAMETROANAGRAFICA, PA.CODICE" & _
                    " FROM UTILIZZOPARAMETROANAGRAFICA UPA" & _
                    " INNER JOIN PARAMETROANAGRAFICA PA ON UPA.IDPARAMETROANAGRAFICA = PA.IDPARAMETROANAGRAFICA" & _
                    " WHERE UPA.IDTIPOANAGRAFICA = " & idTipoAnagrafica & _
                    " ORDER BY PA.CODICE"
            Set rec1 = ORADB.CreateDynaset(sql1, 0&)
            If Not (rec1.BOF And rec1.EOF) Then
                rec1.MoveFirst
                j = 0
                While Not rec1.EOF
                    If (Not (isParametroObbligatorio(rec1("IDPARAMETROANAGRAFICA")))) Then
                        lstParametriAnagraficaAlternativi(i).AddItem rec1("CODICE")
                        lstParametriAnagraficaAlternativi(i).ItemData(j) = rec1("IDPARAMETROANAGRAFICA")
                        j = j + 1
                    End If
                    rec1.MoveNext
                Wend
            End If
            rec1.Close
            
            rec.MoveNext
            i = i + 1
        Wend
    End If
    rec.Close
    
    If i > 2 Then
        qtaListePresentiPerRuolo = i
    Else
        qtaListePresentiPerRuolo = 2
    End If

End Sub

Private Function isParametroAlternativo(Index As Integer, idParametro As Long)
    Dim i As Long
    
    isParametroAlternativo = False
    
    For i = 0 To lstParametriAnagraficaAlternativi(Index).ListCount - 1
        If lstParametriAnagraficaAlternativi(Index).ItemData(i) = idParametro Then
            isParametroAlternativo = True
        End If
        
    Next i
    
End Function

Private Function isParametroObbligatorio(idParametro As Long)
    Dim i As Long
    
    isParametroObbligatorio = False
    
    For i = 0 To lstParametriAnagraficaObbligatori.ListCount - 1
        If lstParametriAnagraficaObbligatori.ItemData(i) = idParametro Then
            isParametroObbligatorio = True
        End If
        
    Next i
    
End Function

Private Sub PopolaListaTipiAnagraficaDisponibili()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long

'    sql = "SELECT TA.IDTIPOANAGRAFICA, TA.NOME" & _
'            " FROM TIPOANAGRAFICA TA, PRODOTTO_RUOLO_TIPOANAGRAFICA PRT" & _
'            " WHERE TA.IDTIPOANAGRAFICA = PRT.IDTIPOANAGRAFICA(+)" & _
'            " AND PRT.IDPRODOTTO(+) = " & idProdottoSelezionato & _
'            " AND PRT.IDRUOLOANAGRAFICA(+) = " & idRuoloAnagraficaCorrente & _
'            " AND PRT.IDTIPOANAGRAFICA IS NULL" & _
'            " ORDER BY TA.IDTIPOANAGRAFICA"
'    Set rec = ORADB.CreateDynaset(sql, 0&)
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        i = 0
'        While Not rec.EOF
'            lstTipiAnagraficaDisponibili.AddItem rec("NOME")
'            lstTipiAnagraficaDisponibili.ItemData(i) = rec("IDTIPOANAGRAFICA")
'            rec.MoveNext
'            i = i + 1
'        Wend
'    End If
'    rec.Close

End Sub

Private Sub PopolaListaParametriAnagrafica()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long

'    lstCodiciParametriAnagrafica.Clear
'
'    sql = "SELECT CODICE" & _
'            " FROM UTILIZZOPARAMETROANAGRAFICA UPA, PARAMETROANAGRAFICA PA" & _
'            " WHERE UPA.IDTIPOANAGRAFICA = " & idTipoAnagraficaCorrente & _
'            " AND UPA.IDPARAMETROANAGRAFICA = PA.IDPARAMETROANAGRAFICA"
'    Set rec = ORADB.CreateDynaset(sql, 0&)
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        i = 0
'        While Not rec.EOF
'            lstCodiciParametriAnagrafica.AddItem rec("CODICE")
'            rec.MoveNext
'            i = i + 1
'        Wend
'    End If
'    rec.Close

End Sub

Private Sub cmdRuoloAcquirenteFisico_Click()
    idRuoloAnagraficaCorrente = RA_ACQUIRENTE_FISICO
    Call InizializzaRuolo
    cmdRuoloAcquirenteFisico.Font.Bold = True
End Sub

Private Sub cmdRuoloAcquirenteNonFisico_Click()
    idRuoloAnagraficaCorrente = RA_ACQUIRENTE_NON_FISICO
    Call InizializzaRuolo
    cmdRuoloAcquirenteNonFisico.Font.Bold = True
End Sub

Private Sub cmdRuoloTitolareFisico_Click()
    idRuoloAnagraficaCorrente = RA_TITOLARE_FISICO
    Call InizializzaRuolo
    cmdRuoloTitolareFisico.Font.Bold = True
End Sub

Private Sub cmdRuoloTitolareNonFisico_Click()
    idRuoloAnagraficaCorrente = RA_TITOLARE_NON_FISICO
    Call InizializzaRuolo
    cmdRuoloTitolareNonFisico.Font.Bold = True
End Sub

Private Sub deselezionaParametriDisponibili()
    Dim i As Integer
    
    For i = 0 To lstParametriAnagraficaDisponibili.ListCount - 1
        lstParametriAnagraficaDisponibili.Selected(i) = False
    Next i
End Sub

Private Sub InizializzaRuolo()
    Dim i As Integer
    
    cmdRuoloAcquirenteFisico.Font.Bold = False
    cmdRuoloAcquirenteNonFisico.Font.Bold = False
    cmdRuoloTitolareFisico.Font.Bold = False
    cmdRuoloTitolareNonFisico.Font.Bold = False
    
    cmdConfermaRuolo.Enabled = False
   
    Call deselezionaParametriDisponibili
    
    lstParametriAnagraficaAlternativi(0).Clear
    lstParametriAnagraficaAlternativi(1).Clear
    For i = 2 To 6 - 1
        lstParametriAnagraficaAlternativi(i).Clear
        lstParametriAnagraficaAlternativi(i).Visible = False
        cmdAggiungiAlternativo(i).Visible = False
        cmdEliminaAlternativo(i).Visible = False
    Next i
    Call contaAlternativePresentiPerRuolo
    PopolaListe
End Sub

Private Sub lstTipiAnagrafica_Click()
    Call SelezionaTipoAnagrafica
End Sub

Private Sub SelezionaTipoAnagrafica()
    Dim i As Integer
    Dim sql As String
    
'    For i = 1 To lstTipiAnagrafica.ListCount
'        If lstTipiAnagrafica.Selected(i - 1) Then
'            idTipoAnagraficaCorrente = lstTipiAnagrafica.ItemData(i - 1)
'        End If
'    Next i
'
'    PopolaListaParametriAnagrafica
End Sub

