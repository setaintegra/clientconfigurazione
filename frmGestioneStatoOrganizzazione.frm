VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmGestioneStatoOrganizzazione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gestione Stato Organizzazione"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConvalidaConfigura 
      Caption         =   "Convalida"
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   7560
      Width           =   1155
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   0
      Top             =   8160
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcOrganizzazioni 
      Height          =   375
      Left            =   7740
      Top             =   300
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrOrganizzazioniConfigurate 
      Height          =   6615
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   11668
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Gestione dello stato dell'Organizzazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   180
      Width           =   7335
   End
End
Attribute VB_Name = "frmGestioneStatoOrganizzazione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idRecordSelezionato As Long

Private internalEvent As Boolean
Private statoModificabile As ValoreBooleanoEnum

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub AggiornaAbilitazioneControlli()

    dgrOrganizzazioniConfigurate.Columns(0).Visible = False
    dgrOrganizzazioniConfigurate.Caption = "ORGANIZZAZIONI CONFIGURATE"
    'cmdConvalidaConfigura.Enabled = False
    If idRecordSelezionato <> idNessunElementoSelezionato Then
        Select Case tipoStatoOrganizzazione
            Case TSO_IN_CONFIGURAZIONE
                cmdConvalidaConfigura.Visible = True
                cmdConvalidaConfigura.Caption = "Convalida"
                cmdConvalidaConfigura.Enabled = (statoModificabile = VB_VERO)
            Case TSO_ATTIVABILE
                cmdConvalidaConfigura.Visible = True
                cmdConvalidaConfigura.Caption = "Configura"
                cmdConvalidaConfigura.Enabled = (statoModificabile = VB_VERO)
            Case TSO_ATTIVA
                cmdConvalidaConfigura.Visible = False
                cmdConvalidaConfigura.Enabled = False
            Case Else
        End Select
    Else
        cmdConvalidaConfigura.Enabled = False
    End If
End Sub

Private Sub AggiornaDataGrid()
    Call CaricaValoriGriglia
End Sub

Private Sub dgrOrganizzazioniConfigurate_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
        Call GetStatoOrganizzazioneRecordSelezionato
        Call GetStatoModificabileOrganizzazioneRecordSelezionato
        Call VisualizzaDataGridToolTip(dgrOrganizzazioniConfigurate, "ID = " & idRecordSelezionato)
    End If
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcOrganizzazioni.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub GetStatoOrganizzazioneRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcOrganizzazioni.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        tipoStatoOrganizzazione = rec("TIPOSTATOENUM")
    Else
        tipoStatoOrganizzazione = TSO_NON_SPECIFICATO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub GetStatoModificabileOrganizzazioneRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcOrganizzazioni.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        statoModificabile = IIf(rec("Stato modificabile") = "SI", VB_VERO, VB_FALSO)
    Else
        statoModificabile = VB_FALSO
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub Init()
    Dim sql As String
    
    idRecordSelezionato = idNessunElementoSelezionato
    Call CaricaValoriGriglia
    Call AggiornaAbilitazioneControlli
    idRecordSelezionato = idNessunElementoSelezionato
    Call Me.Show(vbModal)
    
End Sub

Private Sub CaricaValoriGriglia()
    Call adcOrganizzazioni_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrOrganizzazioniConfigurate_Init
End Sub

Private Sub adcOrganizzazioni_Init()
    Dim internalEventOld As Boolean
    internalEventOld = internalEvent
    internalEvent = True

    Dim d As Adodc
    Dim sql As String
    
    Set d = adcOrganizzazioni
    
'    sql = "SELECT O.IDORGANIZZAZIONE ID, O.NOME ""Nome""," & _
'        " O.DESCRIZIONE ""Descrizione""," & _
'        " TSO.NOME ""Stato""," & _
'        " O.IDTIPOSTATOORGANIZZAZIONE TIPOSTATOENUM," & _
'        " DECODE (O.IDTIPOSTATOORGANIZZAZIONE, 1, DECODE (O.IDSESSIONECONFIGURAZIONE, -1, 'SI', NULL, 'SI', 'NO')," & _
'        " 2, DECODE (O.IDSESSIONECONFIGURAZIONE, " & idSessioneConfigurazioneCorrente & _
'        " , 'SI', NULL, 'SI', 'NO'), 'NO'" & _
'        " ) ""Stato modificabile""" & _
'        " FROM TIPOSTATOORGANIZZAZIONE TSO, ORGANIZZAZIONE O WHERE" & _
'        " (O.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE) " & _
'        " ORDER BY TIPOSTATOENUM, ""Nome"""
    
    sql = "SELECT O.IDORGANIZZAZIONE AS ""ID"", O.NOME AS ""Nome""," & _
        " O.DESCRIZIONE AS ""Descrizione"", TSO.NOME AS ""Stato""," & _
        " O.IDTIPOSTATOORGANIZZAZIONE AS TIPOSTATOENUM," & _
        " DECODE (O.IDTIPOSTATOORGANIZZAZIONE, 1, DECODE (O.IDSESSIONECONFIGURAZIONE, -1, 'SI', NULL, 'SI', 'NO')," & _
        " 2, DECODE (O.IDSESSIONECONFIGURAZIONE, " & idSessioneConfigurazioneCorrente & _
        " , 'SI', NULL, 'SI', 'NO'), 'NO'" & _
        " ) ""Stato modificabile""" & _
        " FROM ORGANIZZAZIONE O, TIPOSTATOORGANIZZAZIONE TSO" & _
        " WHERE O.IDTIPOSTATOORGANIZZAZIONE = TSO.IDTIPOSTATOORGANIZZAZIONE" & _
        " ORDER BY TIPOSTATOENUM, ""Nome"""
    
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrOrganizzazioniConfigurate.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrOrganizzazioniConfigurate_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrOrganizzazioniConfigurate
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcOrganizzazioni.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub cmdConvalidaConfigura_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConvalidaConfigura
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConvalidaConfigura()
    Dim ccAttivit� As CCTipoAttivit�Enum
    
'    Call BloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
'    If Not isProdottoBloccatoDaUtente Then
'        Call frmMessaggio.Visualizza("NotificaRecordBloccato")
'    Else
        Call AggiornaStatoOrganizzazioneInBaseDati(ccAttivit�)
        Call ScriviLog(ccAttivit�, CCDA_ORGANIZZAZIONE, , "IDOrganizzazione = " & idRecordSelezionato)
        Call SelezionaElementoSuGriglia(idRecordSelezionato)
'        Call SbloccaDominioPerUtente(CCDA_PRODOTTO, idRecordSelezionato, isProdottoBloccatoDaUtente)
        Call AggiornaAbilitazioneControlli
'    End If
End Sub

Private Sub AggiornaStatoOrganizzazioneInBaseDati(ccAttivit� As CCTipoAttivit�Enum)
    Dim sql As String
    Dim n As Integer
    Dim m As Integer
    Dim internalEventOld As Boolean
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    If statoModificabile = VB_VERO Then
        Select Case tipoStatoOrganizzazione
            Case TSO_IN_CONFIGURAZIONE
                'If IsOrganizzazioneAttivabile(idRecordSelezionato, True) Then
                    n = 0
                    m = 0
                    sql = "UPDATE ORGANIZZAZIONE SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVABILE & _
                        " WHERE IDOrganizzazione = " & idRecordSelezionato & _
                        " AND IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE
                    SETAConnection.Execute sql, n, adCmdText
                    sql = "UPDATE " & nomeSchema & "ORGANIZZAZIONE SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVABILE & _
                        " WHERE IDORGANIZZAZIONE = " & idRecordSelezionato & _
                        " AND IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE
                    SETAConnection.Execute sql, m, adCmdText
                    If (n = 1) And (m = 1) Then
                        tipoStatoOrganizzazione = TSO_ATTIVABILE
                        ccAttivit� = CCTA_CONVALIDA
                    End If
                'End If
            Case TSO_ATTIVABILE
                    n = 0
                    m = 0
                sql = "UPDATE ORGANIZZAZIONE SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE & _
                    " WHERE IDORGANIZZAZIONE = " & idRecordSelezionato & _
                    " AND IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVABILE
                SETAConnection.Execute sql, n, adCmdText
                sql = "UPDATE " & nomeSchema & "ORGANIZZAZIONE SET IDTIPOSTATOORGANIZZAZIONE = " & TSO_IN_CONFIGURAZIONE & _
                    " WHERE IDORGANIZZAZIONE = " & idRecordSelezionato & _
                    " AND IDTIPOSTATOORGANIZZAZIONE = " & TSO_ATTIVABILE
                SETAConnection.Execute sql, m, adCmdText
                If (n = 1) And (m = 1) Then
                    tipoStatoOrganizzazione = TSO_IN_CONFIGURAZIONE
                    ccAttivit� = CCTA_CONFIGURAZIONE
                Else
                    tipoStatoOrganizzazione = TSO_ATTIVA
                End If
            Case Else
                'Do Nothing
        End Select
    End If
    SETAConnection.CommitTrans
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call adcOrganizzazioni_Init
    Call dgrOrganizzazioniConfigurate_Init
    
    internalEvent = internalEventOld
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
End Sub





