VERSION 5.00
Begin VB.Form frmConfigurazioneProdottoClassiSuperAree 
   Caption         =   "Classi di superaree"
   ClientHeight    =   8670
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   ScaleHeight     =   8670
   ScaleWidth      =   11955
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdImportaClassiSuperareeProdotti 
      Caption         =   "Importa da pianta le classi superarea mancanti"
      Height          =   435
      Left            =   120
      TabIndex        =   22
      Top             =   840
      Width           =   3435
   End
   Begin VB.ListBox lstClassiSuperareeProdotto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3000
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   2340
      Width           =   3500
   End
   Begin VB.ListBox lstDisponibili 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Left            =   4200
      MultiSelect     =   2  'Extended
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2340
      Width           =   3500
   End
   Begin VB.CommandButton cmdSvuotaSelezionati 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   7800
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   5820
      Width           =   435
   End
   Begin VB.CommandButton cmdDisponibile 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   7800
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   4680
      Width           =   435
   End
   Begin VB.CommandButton cmdSelezionato 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   7800
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   3540
      Width           =   435
   End
   Begin VB.ListBox lstSelezionati 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Left            =   8280
      MultiSelect     =   2  'Extended
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   2340
      Width           =   3500
   End
   Begin VB.CommandButton cmdSvuotaDisponibili 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   7800
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   2400
      Width           =   435
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   840
      TabIndex        =   6
      Top             =   5400
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   5
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   4
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   0
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Cllassi di superaree configurate per il prodotto"
      Height          =   195
      Left            =   120
      TabIndex        =   21
      Top             =   2040
      Width           =   3495
   End
   Begin VB.Label lblSelezionati 
      Alignment       =   2  'Center
      Caption         =   "Superaree selezionate (codice - nome)"
      Height          =   195
      Left            =   8280
      TabIndex        =   19
      Top             =   2100
      Width           =   3495
   End
   Begin VB.Label lblDisponibili 
      Alignment       =   2  'Center
      Caption         =   "Superareee disponibili (codice - nome)"
      Height          =   195
      Left            =   4260
      TabIndex        =   18
      Top             =   2100
      Width           =   3495
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle classi di superaree"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   7935
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   16
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   15
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoClassiSuperAree"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPiantaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private idClasseSuperareaProdottoSelezionata As Long
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isProdottoAttivoSuTL As Boolean
Private internalEvent As Boolean
Private rateo As Long
Private listaDisponibili As Collection
Private listaSelezionati As Collection
Private numeroMaxTitoliEmettibiliPerSuperarea As Long

Private gestioneExitCode As ExitCodeEnum
Private modalitāFormCorrente As AzioneEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = True
            cmdPrecedente.Enabled = True
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub Init()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    idClasseSuperareaProdottoSelezionata = idNessunElementoSelezionato
    Call CaricaValoriLstClassiSuperareeProdotto
'    Call CaricaValoriLstDisponibili
'    Call CaricaValoriLstSelezionati
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Private Sub CaricaValoriLstClassiSuperareeProdotto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    lstClassiSuperareeProdotto.Clear
    
    Call ApriConnessioneBD
    
    sql = "SELECT CSP.IDCLASSESUPERAREAPRODOTTO, CS.NOME, CS.CLASSEPRINCIPALE" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESUPERAREA CS" & _
        " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
        " AND CSP.IDCLASSESUPERAREA = CS.IDCLASSESUPERAREA"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            If rec("CLASSEPRINCIPALE") = 1 Then
                lstClassiSuperareeProdotto.AddItem rec("NOME") & " (PRINCIPALE)"
            Else
                lstClassiSuperareeProdotto.AddItem rec("NOME")
            End If
            lstClassiSuperareeProdotto.ItemData(i) = rec("IDCLASSESUPERAREAPRODOTTO")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
End Sub

Private Sub CaricaValoriLstDisponibili()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaDisponibili = New Collection
    
    sql = "SELECT SA.IDAREA ID, SA.CODICE, SA.NOME" & _
        " FROM AREA SA," & _
        " (" & _
        "SELECT CSP_SA.IDSUPERAREA" & _
        " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESUPERAREAPROD_SUPERAREA CSP_SA" & _
        " WHERE CSP.IDPRODOTTO = " & idProdottoSelezionato & _
        " AND CSP.IDCLASSESUPERAREAPRODOTTO = CSP_SA.IDCLASSESUPERAREAPRODOTTO" & _
        ") SA_P" & _
        " WHERE (SA.IDTIPOAREA = " & TA_SUPERAREA_NUMERATA & " OR SA.IDTIPOAREA =  " & TA_SUPERAREA_NON_NUMERATA & " )" & _
        " AND SA.IDPIANTA = " & idPiantaSelezionata & _
        " AND SA.IDAREA = SA_P.IDSUPERAREA(+)" & _
        " AND SA_P.IDSUPERAREA IS NULL" & _
        " ORDER BY SA.CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            areaCorrente.codiceElementoLista = rec("CODICE")
            areaCorrente.nomeElementoLista = rec("NOME")
            areaCorrente.descrizioneElementoLista = areaCorrente.codiceElementoLista & " - " & areaCorrente.nomeElementoLista
            areaCorrente.idElementoLista = rec("ID").Value
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaDisponibili.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstDisponibili_Init
        
End Sub

Private Sub CaricaValoriLstSelezionati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveArea As String
    Dim areaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSelezionati = New Collection
    
    sql = "SELECT A.IDAREA ID, CODICE, NOME" & _
        " FROM AREA A, CLASSESUPERAREAPROD_SUPERAREA CSP_SA" & _
        " WHERE A.IDAREA = CSP_SA.IDSUPERAREA" & _
        " AND CSP_SA.IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata & _
        " ORDER BY CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set areaCorrente = New clsElementoLista
            areaCorrente.codiceElementoLista = rec("CODICE")
            areaCorrente.nomeElementoLista = rec("NOME")
            areaCorrente.descrizioneElementoLista = areaCorrente.nomeAttributoElementoLista & _
                " / " & _
                areaCorrente.codiceElementoLista & _
                " - " & _
                areaCorrente.nomeElementoLista
            areaCorrente.idElementoLista = rec("ID").Value
            chiaveArea = ChiaveId(areaCorrente.idElementoLista)
            Call listaSelezionati.Add(areaCorrente, chiaveArea)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstSelezionati_Init
        
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdDisponibile_Click()
    Call SpostaInLstDisponibili
End Sub

Private Sub cmdImportaClassiSuperareeProdotti_Click()
    Call ImportaClassiSuperareaDaPianta
End Sub

Private Sub cmdSelezionato_Click()
    Call SpostaInLstSelezionati
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub
    
Private Sub ImportaClassiSuperareaDaPianta()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
    sql = "INSERT INTO CLASSESUPERAREAPRODOTTO (IDCLASSESUPERAREAPRODOTTO, IDCLASSESUPERAREA, IDPRODOTTO)" & _
        " SELECT SQ_CLASSESUPERAREAPRODOTTO.NEXTVAL, CS.IDCLASSESUPERAREA, " & idProdottoSelezionato & _
        " FROM CLASSESUPERAREA CS, CLASSESUPERAREAPRODOTTO CSP" & _
        " WHERE IDPIANTA = " & idPiantaSelezionata & _
        " AND CS.IDCLASSESUPERAREA = CSP.IDCLASSESUPERAREA(+)" & _
        " AND CSP.IDCLASSESUPERAREA IS NULL" & _
        " AND CSP.IDPRODOTTO(+) = " & idProdottoSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call CaricaValoriLstClassiSuperareeProdotto

End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmInizialeProdotto.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmInizialeProdotto.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call InserisciNellaBaseDati
            Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_CAPIENZA_SUPERAREA, stringaNota, idProdottoSelezionato)
            Call SetGestioneExitCode(EC_NON_SPECIFICATO)
            Call AggiornaAbilitazioneControlli
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
    
End Sub

Private Sub Successivo()
    Call CaricaFormConfigurazioneAbilitazionePuntiVendita
End Sub

Private Sub CaricaFormConfigurazioneAbilitazionePuntiVendita()
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetRateo(rateo)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAbilitazionePuntiVendita.Init
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub lstDisponibili_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim superarea As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstDisponibili.Clear
    
    If Not (listaDisponibili Is Nothing) Then
        i = 1
        For Each superarea In listaDisponibili
            lstDisponibili.AddItem superarea.descrizioneElementoLista
            lstDisponibili.ItemData(i - 1) = superarea.idElementoLista
            i = i + 1
        Next superarea
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub lstSelezionati_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim area As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSelezionati.Clear
    
    If Not (listaSelezionati Is Nothing) Then
        i = 1
        For Each area In listaSelezionati
            area.descrizioneElementoLista = area.codiceElementoLista & _
                " - " & _
                area.nomeElementoLista
            lstSelezionati.AddItem area.descrizioneElementoLista
            lstSelezionati.ItemData(i - 1) = area.idElementoLista
            i = i + 1
        Next area
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SpostaTuttiInAreeSelezionate()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaDisponibili
        chiaveArea = ChiaveId(area.idElementoLista)
'        area.descrizioneElementoLista = area.nomeAttributoElementoLista & _
'            " / " & _
'            area.codiceElementoLista & _
'            " - " & _
'            area.nomeElementoLista
        area.nomeAttributoElementoLista = numeroMaxTitoliEmettibiliPerSuperarea
        Call listaSelezionati.Add(area, chiaveArea)
    Next area
    Set listaDisponibili = Nothing
    Set listaDisponibili = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SvuotaSelezionati()
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For Each area In listaSelezionati
        area.descrizioneElementoLista = area.codiceElementoLista & _
            " - " & _
            area.nomeElementoLista
        chiaveArea = ChiaveId(area.idElementoLista)
        Call listaDisponibili.Add(area, chiaveArea)
    Next area
    Set listaSelezionati = Nothing
    Set listaSelezionati = New Collection
    
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstDisponibili()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstSelezionati.ListCount
        If lstSelezionati.Selected(i - 1) Then
            idArea = lstSelezionati.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaSelezionati.Item(chiaveArea)
            area.descrizioneElementoLista = area.codiceElementoLista & _
                " - " & _
                area.nomeElementoLista
            Call listaDisponibili.Add(area, chiaveArea)
            Call listaSelezionati.Remove(chiaveArea)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub SpostaInLstSelezionati()
    Dim i As Integer
    Dim idArea As Long
    Dim area As clsElementoLista
    Dim chiaveArea As String
    
    For i = 1 To lstDisponibili.ListCount
        If lstDisponibili.Selected(i - 1) Then
            idArea = lstDisponibili.ItemData(i - 1)
            chiaveArea = ChiaveId(idArea)
            Set area = listaDisponibili.Item(chiaveArea)
            area.nomeAttributoElementoLista = numeroMaxTitoliEmettibiliPerSuperarea
            Call listaSelezionati.Add(area, chiaveArea)
            Call listaDisponibili.Remove(chiaveArea)
        End If
    Next i
    Call lstDisponibili_Init
    Call lstSelezionati_Init
End Sub

Private Sub cmdSvuotaDisponibili_Click()
    Call SpostaTuttiInAreeSelezionate
End Sub

Private Sub cmdSvuotaSelezionati_Click()
    Call SvuotaSelezionati
End Sub

Private Sub lstClassiSuperareeProdotto_Click()
    Dim i As Long
    
    For i = 1 To lstClassiSuperareeProdotto.ListCount
        If lstClassiSuperareeProdotto.Selected(i - 1) Then
            idClasseSuperareaProdottoSelezionata = lstClassiSuperareeProdotto.ItemData(i - 1)
        End If
    Next i

    Call CaricaValoriLstDisponibili
    Call CaricaValoriLstSelezionati
End Sub


Private Sub lstDisponibili_Click()
    Call VisualizzaListBoxToolTip(lstDisponibili, lstDisponibili.Text)
End Sub

Private Sub lstSelezionati_Click()
    Call VisualizzaListBoxToolTip(lstSelezionati, lstSelezionati.Text)
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim area As clsElementoLista
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    If Not (listaSelezionati Is Nothing) Then
        sql = "DELETE FROM CLASSESUPERAREAPROD_SUPERAREA WHERE IDCLASSESUPERAREAPRODOTTO = " & idClasseSuperareaProdottoSelezionata
        SETAConnection.Execute sql, n, adCmdText
        
        For Each area In listaSelezionati
            sql = "INSERT INTO CLASSESUPERAREAPROD_SUPERAREA (IDCLASSESUPERAREAPRODOTTO, IDSUPERAREA)"
            sql = sql & " VALUES ("
            sql = sql & idClasseSuperareaProdottoSelezionata & ", "
            sql = sql & area.idElementoLista & ")"
            SETAConnection.Execute sql, n, adCmdText
        Next area
    End If
    
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub CaricaFormDettagliMaxTitoliEmettibili()
    Call frmDettagliMaxTitoliPerSuperarea.Init
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdStagioneSelezionata(idS As Long)
    idStagioneSelezionata = idS
End Sub

