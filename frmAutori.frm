VERSION 5.00
Begin VB.Form frmAutori 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Informazioni su"
   ClientHeight    =   6225
   ClientLeft      =   4020
   ClientTop       =   1725
   ClientWidth     =   5010
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   415
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   334
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pcbIntegra 
      Height          =   675
      Left            =   60
      ScaleHeight     =   615
      ScaleWidth      =   615
      TabIndex        =   15
      Top             =   4740
      Width           =   675
   End
   Begin VB.CommandButton cmdChiudi 
      Caption         =   "Chiudi"
      Height          =   375
      Left            =   3840
      TabIndex        =   11
      Top             =   5820
      Width           =   1035
   End
   Begin VB.Frame Frame1 
      Height          =   75
      Index           =   2
      Left            =   120
      TabIndex        =   10
      Top             =   1800
      Width           =   4935
   End
   Begin VB.Frame Frame1 
      Height          =   75
      Index           =   1
      Left            =   60
      TabIndex        =   7
      Top             =   3000
      Width           =   4935
   End
   Begin VB.Frame Frame1 
      Height          =   75
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   4560
      Width           =   4935
   End
   Begin VB.Label Label11 
      Caption         =   "www.ticketone.it"
      Height          =   255
      Left            =   1440
      TabIndex        =   14
      Top             =   3600
      Width           =   3075
   End
   Begin VB.Label Label10 
      Caption         =   "info@integrasistemi.it - www.integrasistemi.it"
      Height          =   255
      Left            =   840
      TabIndex        =   13
      Top             =   5160
      Width           =   4155
   End
   Begin VB.Label Label9 
      Caption         =   "Via Baldo degli Ubaldi, 190 - 00167 Roma"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   840
      TabIndex        =   12
      Top             =   4920
      Width           =   4155
   End
   Begin VB.Label Label8 
      Caption         =   "Versione 17.6.0  rilasciata il 09 dicembre 2019"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      TabIndex        =   9
      Top             =   1920
      Width           =   4455
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Versione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3480
      TabIndex        =   8
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Analisi, progettazione e sviluppo software"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   660
      TabIndex        =   6
      Top             =   4320
      Width           =   4275
   End
   Begin VB.Label Label5 
      Caption         =   "TicketOne spa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   5
      Top             =   3240
      Width           =   3075
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "ProprietÓ e diritti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2220
      TabIndex        =   4
      Top             =   2760
      Width           =   2715
   End
   Begin VB.Label Label3 
      Caption         =   "Sistema di Emissione Titoli di Accesso           Client di Configurazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   2
      Top             =   600
      Width           =   4395
   End
   Begin VB.Label Label1 
      Caption         =   "SETA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   555
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   3015
   End
   Begin VB.Label lblAzienda 
      Caption         =   "INTEGRA Sistemi S.r.l."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   840
      TabIndex        =   0
      Top             =   4680
      Width           =   4155
   End
End
Attribute VB_Name = "frmAutori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub Init()
    Call frmAutori.Show(vbModal)
End Sub

Private Sub cmdChiudi_Click()
    Unload Me
End Sub

