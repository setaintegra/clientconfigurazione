VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneOperatoreAnagrafica 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Operatore"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraAnagrafica 
      Height          =   6135
      Left            =   120
      TabIndex        =   6
      Top             =   780
      Width           =   11715
      Begin VB.TextBox txtNumeroDocumento 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   10
         TabIndex        =   25
         Top             =   1740
         Width           =   1155
      End
      Begin VB.ComboBox cmbTipoDocumento 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   1740
         Width           =   3255
      End
      Begin VB.TextBox txtEMail 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   32
         TabIndex        =   23
         Top             =   3660
         Width           =   3495
      End
      Begin VB.TextBox txtFax 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   16
         TabIndex        =   22
         Top             =   3660
         Width           =   1815
      End
      Begin VB.TextBox txtTelefonoFisso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   16
         TabIndex        =   21
         Top             =   3240
         Width           =   1815
      End
      Begin VB.TextBox txtCap 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9420
         MaxLength       =   5
         TabIndex        =   20
         Top             =   2280
         Width           =   675
      End
      Begin VB.TextBox txtComuneNascita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   30
         TabIndex        =   19
         Top             =   1320
         Width           =   3255
      End
      Begin VB.TextBox txtCivico 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   8
         TabIndex        =   18
         Top             =   2280
         Width           =   975
      End
      Begin VB.TextBox txtNome 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   17
         Top             =   480
         Width           =   3255
      End
      Begin VB.TextBox txtCognome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   30
         TabIndex        =   16
         Top             =   480
         Width           =   3255
      End
      Begin VB.TextBox txtIndirizzo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   15
         Top             =   2280
         Width           =   3255
      End
      Begin VB.TextBox txtTelefonoMobile 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   16
         TabIndex        =   14
         Top             =   3240
         Width           =   1815
      End
      Begin VB.TextBox txtCitta 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         MaxLength       =   30
         TabIndex        =   13
         Top             =   2700
         Width           =   3255
      End
      Begin VB.OptionButton optMaschio 
         Caption         =   "M"
         Height          =   195
         Left            =   1500
         TabIndex        =   12
         Top             =   960
         Width           =   495
      End
      Begin VB.OptionButton optFemmina 
         Caption         =   "F"
         Height          =   195
         Left            =   2100
         TabIndex        =   11
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox txtProvincia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6840
         MaxLength       =   2
         TabIndex        =   10
         Top             =   2700
         Width           =   435
      End
      Begin VB.Frame fraTrattamentoDatiConsentito 
         Caption         =   "Trattamento dati personali consentito (leggi 675/96 e 196/03):"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1515
         Left            =   1500
         TabIndex        =   7
         Top             =   4200
         Width           =   5715
         Begin VB.CheckBox chkTrattamentoDatiConsentito2 
            Caption         =   $"frmConfigurazioneOperatoreAnagrafica.frx":0000
            Height          =   615
            Left            =   360
            TabIndex        =   9
            Top             =   660
            Width           =   4875
         End
         Begin VB.CheckBox chkTrattamentoDatiConsentito1 
            Caption         =   "per attivit� pertinenti allo scopo della raccolta;"
            Height          =   195
            Left            =   360
            TabIndex        =   8
            Top             =   360
            Width           =   3675
         End
      End
      Begin MSMask.MaskEdBox mskCodiceFiscale 
         Height          =   315
         Left            =   6840
         TabIndex        =   26
         Top             =   900
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   16
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   ">??????99?99?999?"
         PromptChar      =   "_"
      End
      Begin MSComCtl2.DTPicker dtpDataNascita 
         Height          =   315
         Left            =   1500
         TabIndex        =   27
         Top             =   1320
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61734913
         CurrentDate     =   37607
      End
      Begin VB.Label lblNumeroDocumento 
         Alignment       =   1  'Right Justify
         Caption         =   "Numero"
         Height          =   195
         Left            =   6000
         TabIndex        =   44
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label lblTipoDocumento 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo documento"
         Height          =   195
         Left            =   240
         TabIndex        =   43
         Top             =   1800
         Width           =   1155
      End
      Begin VB.Label lblEMail 
         Alignment       =   1  'Right Justify
         Caption         =   "E-mail"
         Height          =   195
         Left            =   720
         TabIndex        =   42
         Top             =   3720
         Width           =   675
      End
      Begin VB.Label lblFax 
         Alignment       =   1  'Right Justify
         Caption         =   "Fax"
         Height          =   195
         Left            =   6180
         TabIndex        =   41
         Top             =   3720
         Width           =   555
      End
      Begin VB.Label lblTelefonoFisso 
         Alignment       =   1  'Right Justify
         Caption         =   "Telefono fisso"
         Height          =   195
         Left            =   360
         TabIndex        =   40
         Top             =   3300
         Width           =   1035
      End
      Begin VB.Label lblCap 
         Alignment       =   1  'Right Justify
         Caption         =   "CAP"
         Height          =   195
         Left            =   8640
         TabIndex        =   39
         Top             =   2340
         Width           =   675
      End
      Begin VB.Label lblComuneNascita 
         Alignment       =   1  'Right Justify
         Caption         =   "Comune di nascita"
         Height          =   195
         Left            =   5280
         TabIndex        =   38
         Top             =   1380
         Width           =   1455
      End
      Begin VB.Label lblCivico 
         Alignment       =   1  'Right Justify
         Caption         =   "Civico"
         Height          =   195
         Left            =   6000
         TabIndex        =   37
         Top             =   2340
         Width           =   735
      End
      Begin VB.Label lblCodicaFiscale 
         Alignment       =   1  'Right Justify
         Caption         =   "Codice fiscale"
         Height          =   195
         Left            =   5640
         TabIndex        =   36
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Indirizzo"
         Height          =   195
         Left            =   360
         TabIndex        =   35
         Top             =   2340
         Width           =   1035
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome"
         Height          =   195
         Left            =   660
         TabIndex        =   34
         Top             =   540
         Width           =   735
      End
      Begin VB.Label lblCognome 
         Alignment       =   1  'Right Justify
         Caption         =   "Cognome"
         Height          =   195
         Left            =   5700
         TabIndex        =   33
         Top             =   540
         Width           =   1035
      End
      Begin VB.Label lblTelefonoMobile 
         Alignment       =   1  'Right Justify
         Caption         =   "Telefono mobile"
         Height          =   195
         Left            =   5280
         TabIndex        =   32
         Top             =   3300
         Width           =   1455
      End
      Begin VB.Label lbldataNascita 
         Alignment       =   1  'Right Justify
         Caption         =   "Data di nascita"
         Height          =   195
         Left            =   240
         TabIndex        =   31
         Top             =   1380
         Width           =   1155
      End
      Begin VB.Label lblCitta 
         Alignment       =   1  'Right Justify
         Caption         =   "Citt�"
         Height          =   195
         Left            =   360
         TabIndex        =   30
         Top             =   2760
         Width           =   1035
      End
      Begin VB.Label lblSesso 
         Alignment       =   1  'Right Justify
         Caption         =   "Sesso"
         Height          =   195
         Left            =   360
         TabIndex        =   29
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label lblProvincia 
         Alignment       =   1  'Right Justify
         Caption         =   "Provincia"
         Height          =   195
         Left            =   6060
         TabIndex        =   28
         Top             =   2760
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   435
      Left            =   120
      TabIndex        =   5
      Top             =   8100
      Width           =   1155
   End
   Begin VB.CommandButton cmdPulisciControlli 
      Caption         =   "Pulisci campi"
      Height          =   435
      Left            =   1380
      TabIndex        =   4
      Top             =   8100
      Width           =   1155
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   1
      Top             =   240
      Width           =   3255
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10680
      TabIndex        =   0
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Anagrafica Operatore"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   2
      Top             =   0
      Width           =   1635
   End
End
Attribute VB_Name = "frmConfigurazioneOperatoreAnagrafica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idOperatoreSelezionato As Long
Private userNameOperatoreSelezionato As String
Private nome As String
Private cognome As String
Private codiceFiscale As String
Private sesso As SessoEnum
Private dataNascita As Date
Private comuneNascita As String
Private indirizzo As String
Private civico As String
Private cap As String
Private citt� As String
Private provincia As String
Private telefonoFisso As String
Private telefonoMobile As String
Private eMail As String
Private fax As String
Private idPersonaSelezionata As Long
Private idTipoDocumento As Long
Private numeroDocumento As String
Private trattamentoDatiConsentito1 As ValoreBooleanoEnum
Private trattamentoDatiConsentito2 As ValoreBooleanoEnum
Private isRecordEditabile As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private modalit�FormCorrente As AzioneEnum

Private Sub AggiornaAbilitazioneControlli()

    lblInfo1.Caption = "Operatore"
    txtInfo1.Text = userNameOperatoreSelezionato
    txtInfo1.Enabled = False
    
    cmdConferma.Enabled = (Len(Trim(txtCognome.Text)) > 0)
    chkTrattamentoDatiConsentito1.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    chkTrattamentoDatiConsentito2.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCognome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    mskCodiceFiscale.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtNumeroDocumento.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    dtpDataNascita.Enabled = (modalit�FormCorrente <> A_ELIMINA And _
        Len(Trim(txtCognome.Text)) > 0)
    txtComuneNascita.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtIndirizzo.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCivico.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCap.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtProvincia.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCitta.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtTelefonoFisso.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtFax.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtTelefonoMobile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtEMail.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    optMaschio.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    optFemmina.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    
End Sub

Public Sub SetIdOperatoreSelezionato(id As Long)
    idOperatoreSelezionato = id
End Sub

Public Sub SetUserNameOperatoreSelezionato(uName As String)
    userNameOperatoreSelezionato = uName
End Sub

Private Sub cmbTipoDocumento_Click()
    If Not internalEvent Then
        idTipoDocumento = cmbTipoDocumento.ItemData(cmbTipoDocumento.ListIndex)
    End If
End Sub

Private Sub cmdPulisciControlli_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call PulisciControlli
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDOPERATORE = " & idOperatoreSelezionato
    If ValoriCampiOK Then
        Select Case modalit�FormCorrente
            Case A_NUOVO
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_OPERATORE, CCDA_PERSONA, stringaNota, idOperatoreSelezionato)
            Case A_MODIFICA
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_OPERATORE, CCDA_PERSONA, stringaNota, idOperatoreSelezionato)
            Case A_ELIMINA
                Call EliminaDallaBaseDati
                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_OPERATORE, CCDA_PERSONA, stringaNota, idOperatoreSelezionato)
            Case Else
                'Do Nothing
        End Select
        
        Call AggiornaAbilitazioneControlli
        Call frmInizialeOperatore.SetIdPersonaOperatore(idPersonaSelezionata)
        Unload Me
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetModalit�FormCorrente(mfcorr As AzioneEnum)
    modalit�FormCorrente = mfcorr
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Public Sub Init()
    Dim sql As String

    sql = "SELECT IDVALOREENUMERATO ID, NOME FROM VALOREENUMERATO WHERE" & _
        " IDTIPOENUMERATO = 5 AND" & _
        " IDORGANIZZAZIONE IS NULL" & _
        " ORDER BY NOME"
    Call Variabili_Init
    Call cmbTipoDocumento.Clear
    Call CaricaValoriCombo(cmbTipoDocumento, sql, "NOME")
    Select Case modalit�FormCorrente
        Case A_NUOVO
            isRecordEditabile = True
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call SelezionaElementoSuCombo(cmbTipoDocumento, idTipoDocumento)
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call SelezionaElementoSuCombo(cmbTipoDocumento, idTipoDocumento)
        Case Else
    End Select
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)

End Sub

Private Sub dtpDataNascita_Init()
    dtpDataNascita.MinDate = dataNulla
End Sub

Private Sub Variabili_Init()
    nome = ""
    cognome = ""
    codiceFiscale = codiceFiscaleNullo
    idTipoDocumento = idNessunElementoSelezionato
    numeroDocumento = ""
    dataNascita = dataNulla
    comuneNascita = ""
    sesso = S_NON_SPECIFICATO
    indirizzo = ""
    civico = ""
    cap = ""
    provincia = ""
    citt� = ""
    telefonoFisso = ""
    fax = ""
    telefonoMobile = ""
    eMail = ""
    trattamentoDatiConsentito1 = VB_FALSO
    trattamentoDatiConsentito2 = VB_FALSO
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovaPersona As Long
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'   INSERIMENTO IN TABELLA PERSONA
    SETAConnection.BeginTrans
    idNuovaPersona = OttieniIdentificatoreDaSequenza("SQ_PERSONA")
    sql = "INSERT INTO PERSONA (IDPERSONA, COGNOME, NOME, CODICEFISCALE," & _
        " IDTIPODOCUMENTO, NUMERODOCUMENTORICONOSCIMENTO, DATANASCITA," & _
        " COMUNENASCITA, SESSO, INDIRIZZO, CIVICO, CAP, PROVINCIA," & _
        " CITTA, TELEFONOFISSO, FAX, TELEFONOMOBILE, EMAIL, TRATTAMENTODATICONSENTITO1, TRATTAMENTODATICONSENTITO2)" & _
        " VALUES (" & _
        idNuovaPersona & ", " & _
        SqlStringValue(cognome) & ", " & _
        SqlStringValue(nome) & ", " & _
        SqlStringValue(codiceFiscale) & ", " & _
        IIf(idTipoDocumento = idNessunElementoSelezionato, "NULL", idTipoDocumento) & ", " & _
        SqlStringValue(numeroDocumento) & ", " & _
        IIf(dataNascita = dataNulla, "NULL", SqlDateValue(dataNascita)) & ", " & _
        SqlStringValue(comuneNascita) & ", " & _
        IIf(sesso = S_NON_SPECIFICATO, "NULL", IIf(sesso = S_FEMMINA, "'F'", "'M'")) & ", " & _
        SqlStringValue(indirizzo) & ", " & _
        SqlStringValue(civico) & ", " & _
        SqlStringValue(cap) & ", " & _
        SqlStringValue(provincia) & ", " & _
        SqlStringValue(citt�) & ", " & _
        SqlStringValue(telefonoFisso) & ", " & _
        SqlStringValue(fax) & ", " & _
        SqlStringValue(telefonoMobile) & ", " & _
        SqlStringValue(eMail) & ", " & _
        IIf(trattamentoDatiConsentito1 = VB_VERO, VB_VERO, VB_FALSO) & ", " & _
        IIf(trattamentoDatiConsentito2 = VB_VERO, VB_VERO, VB_FALSO) & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        Call AggiornaParametriSessioneSuRecord("PERSONA", "IDPERSONA", idNuovaPersona, TSR_NUOVO)
'    End If
    
'   AGGIORNAMENTO IN TABELLA OPERATORE
    sql = "UPDATE OPERATORE SET IDPERSONA = " & idNuovaPersona & _
        " WHERE IDOPERATORE = " & idOperatoreSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    idPersonaSelezionata = idNuovaPersona
    Call AggiornaAbilitazioneControlli
        
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
        
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    isRecordEditabile = True
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT P.NOME, COGNOME, CODICEFISCALE, IDTIPODOCUMENTO, NUMERODOCUMENTORICONOSCIMENTO," & _
'            " DATANASCITA, COMUNENASCITA, SESSO, INDIRIZZO, CIVICO, CAP, PROVINCIA," & _
'            " CITTA, TELEFONOFISSO, FAX, TELEFONOMOBILE, EMAIL, TRATTAMENTODATICONSENTITO1, TRATTAMENTODATICONSENTITO2," & _
'            " P.IDTIPOSTATORECORD IDTIPOSTATORECORD, P.IDSESSIONECONFIGURAZIONE IDSESSIONECONFIGURAZIONE" & _
'            " FROM PERSONA P, OPERATORE O WHERE" & _
'            " (O.IDPERSONA = P.IDPERSONA) AND" & _
'            " (O.IDOPERATORE = " & idOperatoreSelezionato & ")"
'    Else
        sql = "SELECT P.NOME, COGNOME, CODICEFISCALE, IDTIPODOCUMENTO, NUMERODOCUMENTORICONOSCIMENTO," & _
            " DATANASCITA, COMUNENASCITA, SESSO, INDIRIZZO, CIVICO, CAP, PROVINCIA," & _
            " CITTA, TELEFONOFISSO, FAX, TELEFONOMOBILE, EMAIL, TRATTAMENTODATICONSENTITO1, TRATTAMENTODATICONSENTITO2" & _
            " FROM PERSONA P, OPERATORE O WHERE" & _
            " (O.IDPERSONA = P.IDPERSONA) AND" & _
            " (O.IDOPERATORE = " & idOperatoreSelezionato & ")"
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nome = IIf(IsNull(rec("NOME")), "", rec("NOME"))
        cognome = rec("COGNOME")
        codiceFiscale = IIf(IsNull(rec("CODICEFISCALE")), codiceFiscaleNullo, rec("CODICEFISCALE"))
        idTipoDocumento = IIf(IsNull(rec("IDTIPODOCUMENTO")), idNessunElementoSelezionato, rec("IDTIPODOCUMENTO"))
        numeroDocumento = IIf(IsNull(rec("NUMERODOCUMENTORICONOSCIMENTO")), "", rec("NUMERODOCUMENTORICONOSCIMENTO"))
        dataNascita = IIf(IsNull(rec("DATANASCITA")), dataNulla, rec("DATANASCITA"))
        comuneNascita = IIf(IsNull(rec("COMUNENASCITA")), "", rec("COMUNENASCITA"))
        If IsNull(rec("SESSO")) Then
            sesso = S_NON_SPECIFICATO
        ElseIf rec("SESSO") = "M" Then
            sesso = S_MASCHIO
        ElseIf rec("SESSO") = "F" Then
            sesso = S_FEMMINA
        End If
        indirizzo = IIf(IsNull(rec("INDIRIZZO")), "", rec("INDIRIZZO"))
        civico = IIf(IsNull(rec("CIVICO")), "", rec("CIVICO"))
        cap = IIf(IsNull(rec("CAP")), "", rec("CAP"))
        provincia = IIf(IsNull(rec("PROVINCIA")), "", rec("PROVINCIA"))
        citt� = IIf(IsNull(rec("CITTA")), "", rec("CITTA"))
        telefonoFisso = IIf(IsNull(rec("TELEFONOFISSO")), "", rec("TELEFONOFISSO"))
        fax = IIf(IsNull(rec("FAX")), "", rec("FAX"))
        telefonoMobile = IIf(IsNull(rec("TELEFONOMOBILE")), "", rec("TELEFONOMOBILE"))
        eMail = IIf(IsNull(rec("EMAIL")), "", rec("EMAIL"))
        trattamentoDatiConsentito1 = IIf(IsNull(rec("TRATTAMENTODATICONSENTITO1")), VB_FALSO, rec("TRATTAMENTODATICONSENTITO1"))
        trattamentoDatiConsentito2 = IIf(IsNull(rec("TRATTAMENTODATICONSENTITO2")), VB_FALSO, rec("TRATTAMENTODATICONSENTITO2"))
'        If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    Call SelezionaElementoSuCombo(cmbTipoDocumento, idTipoDocumento)
    chkTrattamentoDatiConsentito1.Value = trattamentoDatiConsentito1
    chkTrattamentoDatiConsentito2.Value = trattamentoDatiConsentito2
    txtNome.Text = nome
    txtCognome.Text = cognome
    mskCodiceFiscale.Text = codiceFiscale
    txtNumeroDocumento.Text = numeroDocumento
    dtpDataNascita.Value = IIf(dataNascita = dataNulla, Null, dataNascita)
    txtComuneNascita.Text = comuneNascita
    txtIndirizzo.Text = indirizzo
    txtCivico.Text = civico
    txtCap.Text = cap
    txtProvincia.Text = provincia
    txtCitta.Text = citt�
    txtTelefonoFisso.Text = telefonoFisso
    txtFax.Text = fax
    txtTelefonoMobile.Text = telefonoMobile
    txtEMail.Text = eMail
    Select Case sesso
        Case S_MASCHIO
            optMaschio.Value = True
        Case S_FEMMINA
            optFemmina.Value = True
        Case Else
            optFemmina.Value = False
            optMaschio.Value = False
    End Select

    internalEvent = internalEventOld

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM PERSONA WHERE IDPERSONA = " & idPersonaSelezionata
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            Call AggiornaParametriSessioneSuRecord("PERSONA", "IDPERSONA", idPersonaSelezionata, TSR_ELIMINATO)
'        End If
'    Else
        sql = "DELETE FROM PERSONA WHERE IDPERSONA = " & idPersonaSelezionata
        SETAConnection.Execute sql, n, adCmdText
'    End If
    
    Call ChiudiConnessioneBD

End Sub

Private Function ValoriCampiOK() As Boolean

    ValoriCampiOK = True
    
    trattamentoDatiConsentito1 = chkTrattamentoDatiConsentito1.Value
    trattamentoDatiConsentito2 = chkTrattamentoDatiConsentito2.Value
    nome = Trim(txtNome.Text)
    cognome = Trim(txtCognome.Text)
    codiceFiscale = mskCodiceFiscale.Text
    numeroDocumento = Trim(txtNumeroDocumento.Text)
    If Not IsNull(dtpDataNascita.Value) Then
        dataNascita = FormatDateTime(dtpDataNascita.Value, vbShortDate)
    Else
        dataNascita = dataNulla
    End If
    comuneNascita = Trim(txtComuneNascita.Text)
    indirizzo = Trim(txtIndirizzo.Text)
    civico = Trim(txtCivico.Text)
    cap = Trim(txtCap.Text)
    provincia = Trim(UCase(txtProvincia.Text))
    citt� = Trim(txtCitta.Text)
    telefonoFisso = Trim(txtTelefonoFisso.Text)
    fax = Trim(txtFax.Text)
    telefonoMobile = Trim(txtTelefonoMobile.Text)
    eMail = Trim(txtEMail.Text)
    If optMaschio.Value = True Then
        sesso = S_MASCHIO
    ElseIf optFemmina.Value = True Then
        sesso = S_FEMMINA
    Else
        sesso = S_NON_SPECIFICATO
    End If

End Function

Private Sub PulisciControlli()
    Call Variabili_Init
    Call AssegnaValoriCampi
End Sub

Private Sub txtCognome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "SELECT IDPERSONA FROM OPERATORE WHERE" & _
        " IDOPERATORE = " & idOperatoreSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            idPersonaSelezionata = rec("IDPERSONA")
        End If
    rec.Close
    
    sql = "UPDATE PERSONA SET" & _
        " NOME = " & SqlStringValue(nome) & "," & _
        " COGNOME = " & SqlStringValue(cognome) & "," & _
        " CODICEFISCALE = " & IIf(codiceFiscale = codiceFiscaleNullo, "NULL", SqlStringValue(codiceFiscale)) & "," & _
        " IDTIPODOCUMENTO = " & IIf(idTipoDocumento = idNessunElementoSelezionato, "NULL", idTipoDocumento) & "," & _
        " NUMERODOCUMENTORICONOSCIMENTO = " & IIf(numeroDocumento = "", "NULL", SqlStringValue(numeroDocumento)) & "," & _
        " DATANASCITA = " & IIf(dataNascita = dataNulla, "NULL", SqlDateValue(dataNascita)) & "," & _
        " COMUNENASCITA = " & SqlStringValue(comuneNascita) & "," & _
        " SESSO = " & IIf(sesso = S_NON_SPECIFICATO, "NULL", IIf(sesso = S_MASCHIO, "'M'", "'F'")) & "," & _
        " INDIRIZZO = " & SqlStringValue(indirizzo) & "," & _
        " CIVICO = " & SqlStringValue(civico) & "," & _
        " CAP = " & SqlStringValue(cap) & "," & _
        " PROVINCIA = " & SqlStringValue(provincia) & "," & _
        " CITTA = " & SqlStringValue(citt�) & "," & _
        " TELEFONOFISSO = " & SqlStringValue(telefonoFisso) & "," & _
        " FAX = " & SqlStringValue(fax) & "," & _
        " TELEFONOMOBILE = " & SqlStringValue(telefonoMobile) & "," & _
        " EMAIL = " & SqlStringValue(eMail) & "," & _
        " TRATTAMENTODATICONSENTITO1 = " & IIf(trattamentoDatiConsentito1 = VB_VERO, VB_VERO, VB_FALSO) & "," & _
        " TRATTAMENTODATICONSENTITO2 = " & IIf(trattamentoDatiConsentito2 = VB_VERO, VB_VERO, VB_FALSO) & _
        " WHERE IDPERSONA = " & idPersonaSelezionata
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalit�Configurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NON_SPECIFICATO Then
'            Call AggiornaParametriSessioneSuRecord("PERSONA", "IDPERSONA", idPersonaSelezionata, TSR_MODIFICATO)
'        End If
'    End If
    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    Resume 0
End Sub







