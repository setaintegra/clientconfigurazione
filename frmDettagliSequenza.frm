VERSION 5.00
Begin VB.Form frmDettagliSequenza 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dettagli Sequenza"
   ClientHeight    =   1740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   3960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2220
      TabIndex        =   3
      Top             =   1200
      Width           =   1035
   End
   Begin VB.TextBox txtOrdineSequenzaInFascia 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2400
      MaxLength       =   4
      TabIndex        =   0
      Top             =   180
      Width           =   675
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   540
      TabIndex        =   2
      Top             =   1200
      Width           =   1035
   End
   Begin VB.TextBox txtArea 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   780
      MaxLength       =   30
      TabIndex        =   1
      Top             =   660
      Width           =   2655
   End
   Begin VB.Label lblArea 
      Alignment       =   1  'Right Justify
      Caption         =   "Area"
      Height          =   195
      Left            =   300
      TabIndex        =   5
      Top             =   720
      Width           =   375
   End
   Begin VB.Label lblOrdineSequenzaInFascia 
      Alignment       =   1  'Right Justify
      Caption         =   "Ordine sequenza in fascia"
      Height          =   195
      Left            =   300
      TabIndex        =   4
      Top             =   240
      Width           =   1995
   End
End
Attribute VB_Name = "frmDettagliSequenza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private ordineSequenzaInFascia As Long
Private idSequenza As Long
Private idAreaSelezionata As Long
Private nomeAreaSelezionata As String
Private xPosto As Integer
Private yPosto As Integer
Private listaSequenzeConfigurate As Collection

Private modalita As ModalitaFormDettagliSequenzaEnum
Private tipoGriglia As TipoGrigliaEnum

Public Sub Init()
    Select Case modalita
        Case MFDS_CREA_SEQUENZA
            ordineSequenzaInFascia = 0
        Case Else
            'Do Nothing
    End Select
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub AggiornaAbilitazioneControlli()

    lblOrdineSequenzaInFascia.Enabled = True
    txtOrdineSequenzaInFascia.Enabled = True
    txtArea.Enabled = False
    Select Case modalita
        Case MFDS_CREA_SEQUENZA
            frmDettagliFascia.Caption = "Creazione Nuova Sequenza"
            cmdConferma.Enabled = (Len(Trim(txtOrdineSequenzaInFascia.Text)) > 0)
        Case MFDS_MODIFICA_ATTRIBUTI_SEQUENZA
            frmDettagliPosto.Caption = "Modifica Attributi Sequenza"
            cmdConferma.Caption = "&Modifica"
            cmdAnnulla.Caption = "&Esci"
            cmdConferma.Enabled = (Len(Trim(txtOrdineSequenzaInFascia.Text)) > 0)
        Case MFDS_ELIMINA_SEQUENZA
            frmDettagliPosto.Caption = "Elimina Sequenza"
            lblOrdineSequenzaInFascia.Enabled = False
            txtOrdineSequenzaInFascia.Enabled = False
        Case Else
    End Select
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitą As Collection
    
On Error Resume Next
    
    ValoriCampiOK = True
    Set listaNonConformitą = New Collection
    
    If IsCampoInteroCorretto(txtOrdineSequenzaInFascia) Then
        If CInt(Trim(txtOrdineSequenzaInFascia.Text)) > 0 Then
            ordineSequenzaInFascia = CInt(Trim(txtOrdineSequenzaInFascia.Text))
        Else
'            Call frmMessaggio.Visualizza("ErroreFormatoDatiOrdine")
            ValoriCampiOK = False
            Call listaNonConformitą.Add("- il valore immesso sul campo Ordine sequenza in fascia deve essere numerico di tipo intero;")
        End If
    Else
'        Call frmMessaggio.Visualizza("ErroreFormatoDatiOrdine")
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Ordine sequenza in fascia deve essere numerico di tipo intero;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If
    
End Function

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    Call frmConfigurazionePiantaFasceSequenze.SetExitCodeDettagliSequenza(EC_DP_ANNULLA)
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    Dim i As Integer
    Dim trovato As Boolean

    If modalita = MFDS_ELIMINA_SEQUENZA Then
        Call frmConfigurazionePiantaFasceSequenze.SetExitCodeDettagliSequenza(EC_DP_CONFERMA)
        Call frmConfigurazionePiantaFasceSequenze.SetOrdineSequenzaInFascia(ordineSequenzaInFascia)
        Unload Me
    Else
        If ValoriCampiOK Then
            i = 1
            trovato = False
            While i <= listaSequenzeConfigurate.count And Not trovato
                If ordineSequenzaInFascia = listaSequenzeConfigurate(i) Then
                    trovato = True
                End If
                i = i + 1
            Wend
            If trovato Then
                Call frmMessaggio.Visualizza("AvvertimentoIndicePreferibilitaGiaPresentePerFascia", _
                                              ordineSequenzaInFascia, _
                                              "corrente")
            Else
                Call frmConfigurazionePiantaFasceSequenze.SetExitCodeDettagliSequenza(EC_DP_CONFERMA)
                Call frmConfigurazionePiantaFasceSequenze.SetOrdineSequenzaInFascia(ordineSequenzaInFascia)
                Unload Me
            End If
        End If
    End If
    
End Sub

Private Sub txtOrdineSequenzaInFascia_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetModalitaForm(moda As ModalitaFormDettagliFasciaEnum)
    modalita = moda
End Sub

Public Sub SetOrdineSequenzaInFascia(ind As Long)
    ordineSequenzaInFascia = ind
End Sub

Public Sub SetIdSequenza(idS As Long)
    idSequenza = idS
End Sub

Private Sub AssegnaValoriCampi()

    txtOrdineSequenzaInFascia.Text = IIf(ordineSequenzaInFascia = 0, "", ordineSequenzaInFascia)
    txtArea.Text = Trim(nomeAreaSelezionata)
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub

Public Sub SetListaSequenzeConfigurate(listaS As Collection)
    Dim sequenza As classeSequenza
    
    Set listaSequenzeConfigurate = New Collection
    For Each sequenza In listaS
        Call listaSequenzeConfigurate.Add(sequenza.ordineInFascia)
    Next sequenza
End Sub





