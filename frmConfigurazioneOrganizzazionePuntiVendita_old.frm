VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmConfigurazioneOrganizzazionePuntiVendita 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   21
      Top             =   240
      Width           =   3255
   End
   Begin VB.TextBox txtCitta 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5160
      MaxLength       =   30
      TabIndex        =   5
      Top             =   7260
      Width           =   3255
   End
   Begin VB.TextBox txtProvincia 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      MaxLength       =   2
      TabIndex        =   6
      Top             =   7260
      Width           =   375
   End
   Begin VB.TextBox txtIndirizzo 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   5160
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   6120
      Width           =   4635
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   9
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   10
      Top             =   4860
      Width           =   2775
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   120
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   6780
      Width           =   4635
   End
   Begin VB.ComboBox cmbNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   6120
      Width           =   4095
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneOrganizzazionePuntiVendita 
      Height          =   330
      Left            =   5700
      Top             =   5040
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneOrganizzazionePuntiVendita 
      Height          =   3915
      Left            =   120
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   22
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblCitta 
      Caption         =   "cittā"
      Height          =   255
      Left            =   5160
      TabIndex        =   20
      Top             =   7020
      Width           =   1035
   End
   Begin VB.Label lblProvincia 
      Caption         =   "provincia"
      Height          =   255
      Left            =   8580
      TabIndex        =   19
      Top             =   7020
      Width           =   1035
   End
   Begin VB.Label lblIndirizzo 
      Caption         =   "Indirizzo"
      Height          =   255
      Left            =   5160
      TabIndex        =   18
      Top             =   5880
      Width           =   1575
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Punti Vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   5295
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   15
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   5880
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   6540
      Width           =   1575
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazionePuntiVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private SQLCaricamentoCombo As String
Private nomeRecordSelezionato As String
Private codiceRecordSelezionato As String
Private descrizioneRecordSelezionato As String
Private indirizzoRecordSelezionato As String
Private cittaRecordSelezionato As String
Private civicoRecordSelezionato As String
Private provinciaRecordSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private isRecordEditabile As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Organizzazione"
    txtInfo1.Text = nomeOrganizzazioneSelezionata
    txtInfo1.Enabled = False
    
    dgrConfigurazioneOrganizzazionePuntiVendita.Caption = "PUNTI VENDITA CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazionePuntiVendita.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbNome.Clear
        txtDescrizione.Text = ""
        txtIndirizzo.Text = ""
        txtCitta.Text = ""
        txtProvincia.Text = ""
        cmbNome.Enabled = False
        txtIndirizzo.Enabled = False
        txtDescrizione.Enabled = False
        txtCitta.Enabled = False
        txtProvincia.Enabled = False
        lblNome.Enabled = False
        lblIndirizzo.Enabled = False
        lblDescrizione.Enabled = False
        lblCitta.Enabled = False
        lblProvincia.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazionePuntiVendita.Enabled = False
        cmbNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        txtIndirizzo.Enabled = False
        txtDescrizione.Enabled = False
        txtCitta.Enabled = False
        txtProvincia.Enabled = False
        lblIndirizzo.Enabled = False
        lblDescrizione.Enabled = False
        lblCitta.Enabled = False
        lblProvincia.Enabled = False
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (cmbNome.ListIndex <> idNessunElementoSelezionato)
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneOrganizzazionePuntiVendita.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbNome.Clear
        txtDescrizione.Text = ""
        txtIndirizzo.Text = ""
        txtCitta.Text = ""
        txtProvincia.Text = ""
        cmbNome.Enabled = False
        txtIndirizzo.Enabled = False
        txtDescrizione.Enabled = False
        txtCitta.Enabled = False
        txtProvincia.Enabled = False
        lblNome.Enabled = False
        lblIndirizzo.Enabled = False
        lblDescrizione.Enabled = False
        lblCitta.Enabled = False
        lblProvincia.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
    gestioneFormCorrente = gs
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    Dim causaNonEditabilita As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        "; IDPUNTOVENDITA = " & idRecordSelezionato

    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoOrganizzazione = TSO_ATTIVA Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If ValoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_INSERISCI_NUOVO
                            Call InserisciNellaBaseDati
                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_PUNTO_VENDITA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazionePuntiVendita_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneOrganizzazionePuntiVendita_Init
                        Case ASG_ELIMINA
                            Call EliminaDallaBaseDati
                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_PUNTO_VENDITA, stringaNota, idOrganizzazioneSelezionata)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneOrganizzazionePuntiVendita_Init
                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
                            Call dgrConfigurazioneOrganizzazionePuntiVendita_Init
                        Case Else
                            'Do Nothing
                    End Select
                End If
                Call AggiornaAbilitazioneControlli
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDPUNTOVENDITA AS ID, NOME FROM PUNTOVENDITA" & _
        " WHERE IDPUNTOVENDITA = " & idRecordSelezionato
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbNome, sql, "NOME")
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    isRecordEditabile = True
    sql = "SELECT P.IDPUNTOVENDITA ID, P.NOME, P.INDIRIZZO, P.DESCRIZIONE" & _
        " FROM PUNTOVENDITA P, ORGANIZZAZIONE_PUNTOVENDITA OP" & _
        " WHERE P.IDPUNTOVENDITA = OP.IDPUNTOVENDITA(+)" & _
        " AND OP.IDPUNTOVENDITA IS NULL" & _
        " AND OP.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriCombo(cmbNome, sql, "NOME")
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmbNome_Click()
    Call CaricaValoriCampiTesto
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriCampiTesto()
    idRecordSelezionato = cmbNome.ItemData(cmbNome.ListIndex)
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
End Sub

Private Sub dgrConfigurazioneOrganizzazionePuntiVendita_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneOrganizzazionePuntiVendita_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneOrganizzazionePuntiVendita_Init
    Call Me.Show(vbModal)
End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneOrganizzazionePuntiVendita.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec(0).Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneOrganizzazionePuntiVendita_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneOrganizzazionePuntiVendita
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT P.IDPUNTOVENDITA ID," & _
'            " NOME ""Nome"", DESCRIZIONE ""Descrizione""," & _
'            " INDIRIZZO ""Indirizzo"", CITTA ""Citta"", PROVINCIA ""Prov.""" & _
'            " FROM PUNTOVENDITA P, ORGANIZZAZIONE_PUNTOVENDITA O" & _
'            " WHERE P.IDPUNTOVENDITA = O.IDPUNTOVENDITA" & _
'            " AND O.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND (O.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
'            " OR O.IDTIPOSTATORECORD IS NULL)" & _
'            " ORDER BY ""Nome"""
'    Else
        sql = "SELECT P.IDPUNTOVENDITA ID," & _
            " NOME ""Nome"", DESCRIZIONE ""Descrizione""," & _
            " INDIRIZZO ""Indirizzo"", CITTA ""Citta"", PROVINCIA ""Prov.""" & _
            " FROM PUNTOVENDITA P, ORGANIZZAZIONE_PUNTOVENDITA O" & _
            " WHERE P.IDPUNTOVENDITA = O.IDPUNTOVENDITA" & _
            " AND O.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " ORDER BY ""Nome"""
'    End If
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneOrganizzazionePuntiVendita.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    Dim condizioneSql As String
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
    sql = "INSERT INTO ORGANIZZAZIONE_PUNTOVENDITA (IDORGANIZZAZIONE, IDPUNTOVENDITA)" & _
    " VALUES (" & idOrganizzazioneSelezionata & ", " & idRecordSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        tipoStatoRecordSelezionato = TSR_NUOVO
'        condizioneSql = " AND IDPUNTOVENDITA = " & idRecordSelezionato
'        Call AggiornaParametriSessioneSuRecord("ORGANIZZAZIONE_PUNTOVENDITA", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_NUOVO, condizioneSql)
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT NOME, INDIRIZZO, DESCRIZIONE, CITTA, PROVINCIA, CIVICO," & _
'            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
'            " FROM PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idRecordSelezionato
'    Else
        sql = "SELECT NOME, INDIRIZZO, DESCRIZIONE, CITTA, PROVINCIA, CIVICO" & _
            " FROM PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        indirizzoRecordSelezionato = IIf(IsNull(rec("INDIRIZZO")), "", rec("INDIRIZZO"))
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        cittaRecordSelezionato = IIf(IsNull(rec("CITTA")), "", rec("CITTA"))
        provinciaRecordSelezionato = IIf(IsNull(rec("PROVINCIA")), "", rec("PROVINCIA"))
        civicoRecordSelezionato = IIf(IsNull(rec("CIVICO")), "", rec("CIVICO"))
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True
    
    Call SelezionaElementoSuCombo(cmbNome, idRecordSelezionato)
    txtDescrizione.Text = ""
    txtDescrizione.Text = descrizioneRecordSelezionato
    txtIndirizzo.Text = ""
    txtIndirizzo.Text = IIf(Trim(indirizzoRecordSelezionato) = "", "", indirizzoRecordSelezionato & ", " & civicoRecordSelezionato)
    txtCitta.Text = ""
    txtCitta.Text = cittaRecordSelezionato
    txtProvincia.Text = ""
    txtProvincia.Text = provinciaRecordSelezionato

    internalEvent = internalEventOld

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim SqlConstraints As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    SETAConnection.BeginTrans
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If tipoStatoRecordSelezionato = TSR_NUOVO Then
'            sql = "DELETE FROM ORGANIZZAZIONE_PUNTOVENDITA" & _
'                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'                " AND IDPUNTOVENDITA" & " = " & idRecordSelezionato
'            SETAConnection.Execute sql, n, adCmdText
'        Else
'            SqlConstraints = " AND IDPUNTOVENDITA = " & idRecordSelezionato
'            Call AggiornaParametriSessioneSuRecord("ORGANIZZAZIONE_PUNTOVENDITA", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_ELIMINATO, SqlConstraints)
'        End If
'    Else
        sql = "DELETE FROM ORGANIZZAZIONE_PUNTOVENDITA" & _
            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND IDPUNTOVENDITA" & " = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazioneOrganizzazionePuntiVendita_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneOrganizzazionePuntiVendita
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneOrganizzazionePuntiVendita.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Function ValoriCampiOK() As Boolean

    ValoriCampiOK = True
    indirizzoRecordSelezionato = Trim(txtIndirizzo.Text)
    nomeRecordSelezionato = Trim(cmbNome.Text)
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    
End Function


