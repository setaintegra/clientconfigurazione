VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazionePuntoVenditaTerminaleAvanzato 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Punto Vendita"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbSottorete 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2460
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   6300
      Width           =   5535
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   17
      Top             =   240
      Width           =   3255
   End
   Begin VB.TextBox txtCodice 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   20
      TabIndex        =   4
      Top             =   6300
      Width           =   2235
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   9
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   11
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   8
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   10
      Top             =   4860
      Width           =   5295
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   3960
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   2700
         TabIndex        =   2
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciDaSelezione 
         Caption         =   "Inserisci da selezione"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CheckBox chkAbilitato 
      Caption         =   "Abilitato"
      Height          =   375
      Left            =   8100
      TabIndex        =   6
      Top             =   6300
      Width           =   2775
   End
   Begin MSAdodcLib.Adodc adcConfigurazionePuntoVenditaTerminaliAvanzati 
      Height          =   330
      Left            =   5880
      Top             =   4740
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazionePuntoVenditaTerminaliAvanzati 
      Height          =   3915
      Left            =   120
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSottorete 
      Caption         =   "Nome - descrizione Sottorete"
      Height          =   255
      Left            =   2460
      TabIndex        =   19
      Top             =   6060
      Width           =   2475
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   18
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblCodice 
      Caption         =   "Codice"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   6060
      Width           =   1695
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Terminali Avanzati"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   5775
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   14
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   4620
      Width           =   1815
   End
End
Attribute VB_Name = "frmConfigurazionePuntoVenditaTerminaleAvanzato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idPuntoVenditaSelezionato As Long
Private idSottorete As Long
Private SQLCaricamentoCombo As String
Private codiceRecordSelezionato As String
Private descrizioneRecordSelezionato As String
Private nomePuntoVenditaSelezionato As String

Private abilitato As ValoreBooleanoEnum
Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Punto Vendita"
    txtInfo1.Text = nomePuntoVenditaSelezionato
    txtInfo1.Enabled = False
    
    dgrConfigurazionePuntoVenditaTerminaliAvanzati.Caption = "TERMINALI AVANZATI CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePuntoVenditaTerminaliAvanzati.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtCodice.Text = ""
        Call cmbSottorete.Clear
        chkAbilitato.Value = vbUnchecked
        txtCodice.Enabled = False
        cmbSottorete.Enabled = False
        chkAbilitato.Enabled = False
        lblCodice.Enabled = False
        lblSottorete.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePuntoVenditaTerminaliAvanzati.Enabled = False
        txtCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmbSottorete.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        chkAbilitato.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblCodice.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblSottorete.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdInserisciDaSelezione.Enabled = False
        cmdModifica.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = Trim(txtCodice.Text) <> "" And _
            idSottorete <> idNessunElementoSelezionato
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazionePuntoVenditaTerminaliAvanzati.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtCodice.Text = ""
        Call cmbSottorete.Clear
        chkAbilitato.Value = vbUnchecked
        txtCodice.Enabled = False
        cmbSottorete.Enabled = False
        chkAbilitato.Enabled = False
        lblCodice.Enabled = False
        lblSottorete.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdInserisciDaSelezione.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    End If
    
End Sub

Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
    gestioneFormCorrente = gs
End Sub

Public Sub SetIdPuntoVenditaSelezionato(id As Long)
    idPuntoVenditaSelezionato = id
End Sub

Public Sub SetNomePuntoVenditaSelezionato(nome As String)
    nomePuntoVenditaSelezionato = nome
End Sub

Private Sub cmbSottorete_Click()
    If Not internalEvent Then
        idSottorete = cmbSottorete.ItemData(cmbSottorete.ListIndex)
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
        
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String
    
    stringaNota = "IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
        "; IDTERMINALE = " & idRecordSelezionato
    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli

'    If ValoriCampiOK Then
'        Select Case gestioneRecordGriglia
'            Case ASG_INSERISCI_NUOVO
'                Call InserisciNellaBaseDati
'                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
'                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'                Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
'                Call SelezionaElementoSuGriglia(idRecordSelezionato)
'                Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
'            Case ASG_INSERISCI_DA_SELEZIONE
'                Call InserisciNellaBaseDati
'                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
'                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'                Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
'                Call SelezionaElementoSuGriglia(idRecordSelezionato)
'                Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
'            Case ASG_MODIFICA
'                Call AggiornaNellaBaseDati
'                Call ScriviLog(CCTA_MODIFICA, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
'                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'                Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
'                Call SelezionaElementoSuGriglia(idRecordSelezionato)
'                Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
'            Case ASG_ELIMINA
'                Call EliminaDallaBaseDati
'                Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
'                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'                Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
'                Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'                Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
'                Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
'        End Select
'
'        Call AggiornaAbilitazioneControlli
'    End If
    
    If gestioneRecordGriglia = ASG_ELIMINA Then
        Call EliminaDallaBaseDati
        Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
        Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
        Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
        Call SetIdRecordSelezionato(idNessunElementoSelezionato)
        Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
        Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
    Else
        If ValoriCampiOK Then
            If gestioneRecordGriglia = ASG_INSERISCI_NUOVO Or gestioneRecordGriglia = ASG_INSERISCI_DA_SELEZIONE Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
            ElseIf gestioneRecordGriglia = ASG_MODIFICA Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_PUNTO_VENDITA, CCDA_TERMINALE_AVANZATO, stringaNota)
            End If
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
            Call SelezionaElementoSuGriglia(idRecordSelezionato)
            Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDSOTTORETE ID, NOME || ' - ' || DESCRIZIONE LABEL" & _
        " FROM SOTTORETE ORDER BY LABEL"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbSottorete, sql, "LABEL")
    Call AssegnaValoriCampi
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciDaSelezione_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciDaSelezione
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciDaSelezione()
    Dim sql As String
    
    sql = "SELECT IDSOTTORETE ID, NOME || ' - ' || DESCRIZIONE LABEL" & _
        " FROM SOTTORETE ORDER BY LABEL"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
    sql = "SELECT IDSOTTORETE ID, NOME || ' - ' || DESCRIZIONE LABEL" & _
        " FROM SOTTORETE ORDER BY LABEL"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call CaricaValoriCombo(cmbSottorete, sql, "LABEL")
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub txtCodice_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Dim sql As String
    
    sql = "SELECT IDSOTTORETE ID, NOME || ' - ' || DESCRIZIONE LABEL" & _
        " FROM SOTTORETE ORDER BY LABEL"
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbSottorete, sql, "LABEL")
    Call AssegnaValoriCampi
End Sub

Private Sub dgrConfigurazionePuntoVenditaTerminaliAvanzati_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()
    idSottorete = idNessunElementoSelezionato
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazionePuntoVenditaTerminaliAvanzati_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazionePuntoVenditaTerminaliAvanzati.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec(0).Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazionePuntoVenditaTerminaliAvanzati_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcConfigurazionePuntoVenditaTerminaliAvanzati
    
    sql = "SELECT T.IDTERMINALE AS ""ID"", T.CODICE AS ""Codice""," & _
        " S.NOME || ' - ' || S.DESCRIZIONE ""Nome - descrizione Sottorete""," & _
        " DECODE (ABILITATO, 0, 'NO', 1, 'SI') AS ""Abilitato""" & _
        " FROM TERMINALE T, SOTTORETE S" & _
        " WHERE T.IDSOTTORETE = S.IDSOTTORETE" & _
        " AND T.IDPUNTOVENDITA= " & idPuntoVenditaSelezionato & _
        " ORDER BY ""Codice"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazionePuntoVenditaTerminaliAvanzati.dataSource = d

    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoTerminaleAvanzato As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    idNuovoTerminaleAvanzato = OttieniIdentificatoreDaSequenza("SQ_TERMINALE")
    sql = "INSERT INTO TERMINALE (IDTERMINALE, CODICE, ABILITATO," & _
        " IDPUNTOVENDITA, IDSOTTORETE, IDTIPOTERMINALE)" & _
        " VALUES (" & _
        idNuovoTerminaleAvanzato & ", " & _
        SqlStringValue(codiceRecordSelezionato) & ", " & _
        abilitato & ", " & _
        idPuntoVenditaSelezionato & ", " & _
        idSottorete & ", " & _
        TT_TERMINALE_AVANZATO & ")"
    SETAConnection.Execute sql, , adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idNuovoTerminaleAvanzato)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT CODICE, ABILITATO, IDSOTTORETE FROM TERMINALE" & _
        " WHERE IDTERMINALE = " & idRecordSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        codiceRecordSelezionato = rec("CODICE")
        abilitato = rec("ABILITATO")
        idSottorete = rec("IDSOTTORETE").Value
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtCodice.Text = ""
    txtCodice.Text = codiceRecordSelezionato
    chkAbilitato.Value = abilitato
    
    Call SelezionaElementoSuCombo(cmbSottorete, idSottorete)

    internalEvent = internalEventOld

End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD

On Error GoTo gestioneErrori

    sql = "UPDATE TERMINALE SET" & _
        " CODICE = " & SqlStringValue(codiceRecordSelezionato) & ", " & _
        " IDSOTTORETE = " & idSottorete & ", " & _
        " ABILITATO = " & abilitato & _
        " WHERE IDTERMINALE = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim terminaleEliminabile As Boolean
    Dim listaTabelleCorrelate As Collection
    Dim tabelleCorrelate As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    Set listaTabelleCorrelate = New Collection
    
    terminaleEliminabile = True
    If Not IsRecordEliminabile("IDTERMINALE", "OPERAZIONE", "'" & Trim(idRecordSelezionato) & "'") Then
        Call listaTabelleCorrelate.Add("OPERAZIONE")
        terminaleEliminabile = False
    End If

    If terminaleEliminabile Then
        sql = "DELETE FROM TERMINALE WHERE IDTERMINALE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
    Else
        tabelleCorrelate = ArgomentoMessaggio(listaTabelleCorrelate)
        Call frmMessaggio.Visualizza("NotificaRecordNonEliminabile", "Il Terminale selezionato", tabelleCorrelate)
    End If
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazionePuntoVenditaTerminaliAvanzati_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazionePuntoVenditaTerminaliAvanzati
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazionePuntoVenditaTerminaliAvanzati.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitā As Collection
    Dim condizioneSql As String

    ValoriCampiOK = True
    condizioneSql = ""
    Set listaNonConformitā = New Collection
    If gestioneRecordGriglia = ASG_MODIFICA Or gestioneRecordGriglia = ASG_ELIMINA Then
        condizioneSql = "IDTERMINALE <> " & idRecordSelezionato
    End If
    
    codiceRecordSelezionato = UCase(Trim(txtCodice.Text))
    If ViolataUnicitā("TERMINALE", "CODICE = " & SqlStringValue(codiceRecordSelezionato), "", _
        condizioneSql, "", "") Then
        ValoriCampiOK = False
        Call listaNonConformitā.Add("- il valore codice = " & SqlStringValue(codiceRecordSelezionato) & _
            " č giā presente in DB;")
    End If
    abilitato = chkAbilitato.Value
    
    If listaNonConformitā.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitāCampi", ArgomentoMessaggio(listaNonConformitā))
    End If

End Function
'
'Private Sub CreaListaCampiValoriUnici()
'    Set listaCampiValoriUnici = New Collection
'
'    Call listaCampiValoriUnici.Add("CODICE = " & SqlStringValue(codiceRecordSelezionato))
'End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
        
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub


