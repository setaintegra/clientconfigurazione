VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfigurazioneOrganizzazionePuntiVendita 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Organizzazione"
   ClientHeight    =   11490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13350
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11490
   ScaleWidth      =   13350
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmAggiornamento 
      Caption         =   "Configurazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7095
      Left            =   120
      TabIndex        =   7
      Top             =   3720
      Width           =   13095
      Begin VB.CommandButton cmdSelezionaFile 
         Caption         =   "Importa PV da ..."
         Height          =   375
         Left            =   8040
         TabIndex        =   25
         Top             =   3000
         Width           =   1395
      End
      Begin VB.CommandButton cmdDisabilita 
         Caption         =   "Disabilita"
         Height          =   375
         Left            =   8040
         TabIndex        =   20
         Top             =   6360
         Width           =   4935
      End
      Begin VB.CommandButton cmdAbilita 
         Caption         =   "Abilita"
         Height          =   375
         Left            =   9480
         TabIndex        =   19
         Top             =   3000
         Width           =   3495
      End
      Begin VB.ListBox lstRegioniSelezionate 
         Height          =   2400
         Left            =   2400
         TabIndex        =   18
         Top             =   3960
         Width           =   1695
      End
      Begin VB.ListBox lstProvinceSelezionate 
         Height          =   2400
         Left            =   4200
         TabIndex        =   17
         Top             =   3960
         Width           =   1695
      End
      Begin VB.ListBox lstComuniSelezionati 
         Height          =   2400
         Left            =   6000
         TabIndex        =   16
         Top             =   3960
         Width           =   1695
      End
      Begin VB.ListBox lstPuntiVenditaSelezionati 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2400
         Left            =   8040
         MultiSelect     =   2  'Extended
         TabIndex        =   15
         Top             =   3960
         Width           =   4935
      End
      Begin VB.ListBox lstPuntiVenditaDisponibili 
         Height          =   2400
         Left            =   8040
         MultiSelect     =   2  'Extended
         TabIndex        =   14
         Top             =   600
         Width           =   4935
      End
      Begin VB.ListBox lstComuniDisponibili 
         Height          =   2400
         Left            =   6000
         TabIndex        =   13
         Top             =   600
         Width           =   1695
      End
      Begin VB.ListBox lstProvinceDisponibili 
         Height          =   2400
         Left            =   4200
         TabIndex        =   12
         Top             =   600
         Width           =   1695
      End
      Begin VB.ListBox lstRegioniDisponibili 
         Height          =   2400
         Left            =   2400
         TabIndex        =   11
         Top             =   600
         Width           =   1695
      End
      Begin VB.ListBox lstClassiPVDaModificare 
         Height          =   2205
         Left            =   240
         TabIndex        =   10
         Top             =   2280
         Width           =   1695
      End
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1320
         TabIndex        =   9
         Top             =   6600
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   120
         TabIndex        =   8
         Top             =   6600
         Width           =   1155
      End
      Begin MSComDlg.CommonDialog cdlFileImport 
         Left            =   7560
         Top             =   3000
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblInsiemePuntiVenditaSelezionati 
         Caption         =   "Insieme punti vendita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8040
         TabIndex        =   24
         Top             =   3720
         Width           =   4875
      End
      Begin VB.Label lblInsiemePuntiVenditaDisponibili 
         Caption         =   "Insieme punti vendita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8040
         TabIndex        =   23
         Top             =   360
         Width           =   4875
      End
      Begin VB.Label Label2 
         Caption         =   "Punti vendita abilitati"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2400
         TabIndex        =   22
         Top             =   3600
         Width           =   3555
      End
      Begin VB.Label Label1 
         Caption         =   "Punti vendita non abilitati"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2400
         TabIndex        =   21
         Top             =   240
         Width           =   3435
      End
   End
   Begin VB.Frame frmPuntiVenditaConfigurati 
      Caption         =   "Situazione attuale della configurazione dei punti vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   13095
      Begin VB.ListBox lstPuntiVenditaConfigurati 
         Height          =   2400
         Left            =   4680
         TabIndex        =   6
         Top             =   360
         Width           =   8295
      End
      Begin VB.ListBox lstClassiPV 
         Height          =   2400
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   4095
      End
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9180
      TabIndex        =   2
      Top             =   240
      Width           =   3975
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   12000
      TabIndex        =   0
      Top             =   10920
      Width           =   1155
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   9180
      TabIndex        =   3
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Punti Vendita"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5295
   End
End
Attribute VB_Name = "frmConfigurazioneOrganizzazionePuntiVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private nomeFileImportazione As String
Private excImportazione As New Excel.Application
Private idOrganizzazioneSelezionata As Long
Private nomeOrganizzazioneSelezionata As String

Private idClassePVSelezionata As Long
Private idClassePVDaModificareSelezionata As Long
    
Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub Init()

    Call ApriConnessioneBD_ORA
    ORADB.BeginTrans
    
    lblInfo1.Caption = "Organizzazione"
    Call initListaClassiPV
    Call initListaClassiPVDaModificare
    Call Me.Show(vbModal)
End Sub

Private Sub initListaClassiPV()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
        
    sql = "SELECT IDCLASSEPUNTOVENDITA, DESCRIZIONE FROM CLASSEPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstClassiPV.AddItem(rec("DESCRIZIONE"), i)
            lstClassiPV.ItemData(i) = rec("IDCLASSEPUNTOVENDITA")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call lstClassiPV.AddItem("Tutti", i)
    lstClassiPV.ItemData(i) = idNessunElementoSelezionato

End Sub

Private Sub initListaClassiPVDaModificare()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    
    sql = "SELECT IDCLASSEPUNTOVENDITA, NOME FROM CLASSEPUNTOVENDITA"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstClassiPVDaModificare.AddItem(rec("NOME"), i)
            lstClassiPVDaModificare.ItemData(i) = rec("IDCLASSEPUNTOVENDITA")
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub ClearListe()
    lstRegioniDisponibili.Clear
    lstRegioniSelezionate.Clear
    lstProvinceDisponibili.Clear
    lstProvinceSelezionate.Clear
    lstComuniDisponibili.Clear
    lstComuniSelezionati.Clear
    lstPuntiVenditaDisponibili.Clear
    lstPuntiVenditaSelezionati.Clear
End Sub

Private Sub Annulla()
    ' se c'č qualcosa in sospeso viene annullato e si comincia una nuova transazione
    ORADB.Rollback
    ORADB.BeginTrans
    Call ClearListe
End Sub

Private Sub Conferma()
    ' se c'č qualcosa in sospeso viene confermato e si comincia una nuova transazione
    ORADB.CommitTrans
    ORADB.BeginTrans
    Call ClearListe
    PopolaListaPuntiVenditaConfigurati
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer

    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    Call Esci

    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    ' se c'č qualcosa in sospeso viene annullato
    ORADB.Rollback
    Call ChiudiConnessioneBD_ORA
    Unload Me
End Sub

Private Sub cmdSelezionaFile_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ImportaPuntiVendita
    
    MousePointer = mousePointerOld

End Sub
   
Private Sub ImportaPuntiVendita()
    SelezionaTuttiDaDisabilitare
    Call Disabilita
    Call ApriFileImportazionePuntiVendita
End Sub

Private Sub ApriFileImportazionePuntiVendita()
    Dim pippo As String
    
On Error GoTo gestioneErroreAperturaFileImportazionePuntiVendita

    DoEvents
    
    cdlFileImport.InitDir = App.Path
    cdlFileImport.Filter = "Excel (*.xls)|*.xls|File di testo (*.txt)|*.txt"
    'cdlFileImport.DefaultExt = "xls"
    cdlFileImport.Flags = cdlOFNFileMustExist Or cdlOFNExplorer
    cdlFileImport.FileName = ""
    cdlFileImport.CancelError = True
    cdlFileImport.ShowOpen
    
    nomeFileImportazione = cdlFileImport.FileName
    DoEvents
    If Mid(nomeFileImportazione, Len(nomeFileImportazione) - 2, 3) = "xls" Then
        Call LeggiPuntiVenditaDaFileExcel
    Else
        If Mid(nomeFileImportazione, Len(nomeFileImportazione) - 2, 3) = "txt" Then
            Call LeggiPuntiVenditaDaFileTXT
        Else
            MsgBox "Tipo di file non riconosciuto"
        End If
    End If
    
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
    
    Exit Sub
    
gestioneErroreAperturaFileImportazionePuntiVendita:
    'do nothing
End Sub

Private Sub LeggiPuntiVenditaDaFileExcel()
    Dim workbook As workbook
    Dim workSheet As workSheet
    Dim nomePuntoVendita As String
    Dim idPuntoVendita As Long
    Dim i As Long
    Dim numeroNulli As Long
    Dim numeroValori As Long
    Dim rangeTotale As range
    Dim quantitaPuntiVenditaInseriti As Long
    Dim quantitaDatiErrati As Long
    Dim quantitaDatiRipetuti As Long
    Dim listaErrori As String
    
    Set workbook = excImportazione.Workbooks.Open(nomeFileImportazione)
    Set workSheet = workbook.ActiveSheet
    
On Error Resume Next
    
    i = 1
    nomePuntoVendita = ""
    quantitaPuntiVenditaInseriti = 0
    quantitaDatiErrati = 0
    quantitaDatiRipetuti = 0
    listaErrori = ""
    
    Set rangeTotale = workSheet.range("A:A")
    numeroNulli = excImportazione.WorksheetFunction.CountBlank(rangeTotale)
    numeroValori = 65536 - numeroNulli
    
'    Call pgbAvanzamento_Init(numeroValori)
    
    While workSheet.Rows.Cells(i, 1) <> ""
        nomePuntoVendita = workSheet.Rows.Cells(i, 1)
        idPuntoVendita = getIdPuntoVendita(nomePuntoVendita)
        If idPuntoVendita = idNessunElementoSelezionato Then
            quantitaDatiErrati = quantitaDatiErrati + 1
            listaErrori = listaErrori & " " & nomePuntoVendita
        Else
            If AbilitaPuntoVendita(idPuntoVendita) = 1 Then
                quantitaPuntiVenditaInseriti = quantitaPuntiVenditaInseriti + 1
            Else
                quantitaDatiRipetuti = quantitaDatiRipetuti + 1
            End If
        End If
        i = i + 1
        DoEvents
    Wend
    MsgBox "Trovati " & quantitaPuntiVenditaInseriti & " punti vendita; trovati " & quantitaDatiErrati & " errori (" & listaErrori & "); trovati " & quantitaDatiRipetuti & " ripetuti"
    
    Call excImportazione.Quit
End Sub

Private Sub LeggiPuntiVenditaDaFileTXT()
    Dim nomePuntoVendita As String
    Dim idPuntoVendita As Long
    Dim numeroValori As Long
    Dim rangeTotale As range
    Dim quantitaPuntiVenditaInseriti As Long, quantitaDatiErrati, quantitaDatiRipetuti As Long
    Dim listaErrori As String
    
    Dim riga As String
    
On Error Resume Next
    nomePuntoVendita = ""
    quantitaPuntiVenditaInseriti = 0
    quantitaDatiErrati = 0
    quantitaDatiRipetuti = 0
    listaErrori = ""

    Open nomeFileImportazione For Input As #1
    While Not EOF(1)
        Line Input #1, nomePuntoVendita
        idPuntoVendita = getIdPuntoVendita(nomePuntoVendita)
        If idPuntoVendita = idNessunElementoSelezionato Then
            quantitaDatiErrati = quantitaDatiErrati + 1
            listaErrori = listaErrori & " " & nomePuntoVendita
        Else
            If AbilitaPuntoVendita(idPuntoVendita) = 1 Then
                quantitaPuntiVenditaInseriti = quantitaPuntiVenditaInseriti + 1
            Else
                quantitaDatiRipetuti = quantitaDatiRipetuti + 1
            End If
        End If
    Wend
    Close #1
    
End Sub

Private Sub lstClassiPV_Click()
    Dim numeroElementiSelezionati As Long
    
    idClassePVSelezionata = lstClassiPV.ItemData(lstClassiPV.ListIndex)
    Call PopolaListaPuntiVenditaConfigurati
    
End Sub

' Popola la lista dei punti vendita configurati
Private Sub PopolaListaPuntiVenditaConfigurati()
    Dim sql As String
    Dim rec As OraDynaset
    Dim i As Long
    Dim nome As String

    Call lstPuntiVenditaConfigurati.Clear
   
    If idClassePVSelezionata = idNessunElementoSelezionato Then
        sql = "SELECT PV.NOME || ' - ' || C.NOME || ' (' || P.SIGLA || ')' NOME" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, COMUNE C, PROVINCIA P" & _
            " WHERE OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " AND PV.IDCOMUNE = C.IDCOMUNE (+)" & _
            " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
            " ORDER BY PV.NOME"
    Else
        sql = "SELECT PV.NOME || ' - ' || C.NOME || ' (' || P.SIGLA || ')' NOME" & _
            " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPV, COMUNE C, PROVINCIA P" & _
            " WHERE OCPV.IDCLASSEPUNTOVENDITA = " & idClassePVSelezionata & _
            " AND OCPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCPV.IDPUNTOVENDITA = PV.IDPUNTOVENDITA" & _
            " AND PV.IDCOMUNE = C.IDCOMUNE (+)" & _
            " AND C.IDPROVINCIA = P.IDPROVINCIA(+)" & _
            " ORDER BY PV.NOME"
    End If

    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstPuntiVenditaConfigurati.AddItem(rec("NOME"), i)
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

End Sub

Private Sub lstClassiPVDaModificare_Click()
    Dim numeroElementiSelezionati As Long
    
    idClassePVDaModificareSelezionata = lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex)
    If idClassePVDaModificareSelezionata = TCPV_INTERNET Or idClassePVDaModificareSelezionata = TCPV_CALL_CENTER_LIS Or idClassePVDaModificareSelezionata = TCPV_ALTRI_CALL_CENTER Then
        lstRegioniDisponibili.Clear
        lstRegioniSelezionate.Clear
        lstProvinceDisponibili.Clear
        lstProvinceSelezionate.Clear
        lstComuniDisponibili.Clear
        lstComuniSelezionati.Clear
    Else
        Call PopolaListeRegioni
    End If
'    Call PopolaListePVDisponibiliConClassePV(lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
'    Call PopolaListePVSelezionatiConClassePV(lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub PopolaListeRegioni()
    Dim sql As String
    
    sql = "SELECT IDREGIONE ID, NOME FROM REGIONE ORDER BY NOME"
    Call PopolaLista(lstRegioniDisponibili, sql)
    Call PopolaLista(lstRegioniSelezionate, sql)
End Sub

Private Sub PopolaListePVDisponibiliConClassePV(idClassePV)
    Dim sql As String
    
    sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM ORGANIZ_CLASSEPV_PUNTOVENDITA WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili"
End Sub

Private Sub PopolaListePVSelezionatiConClassePV(idClassePV)
    Dim sql As String
    
    sql = "SELECT DISTINCT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita della classe selezionata"
End Sub

Private Sub PopolaListePVDisponibiliConRegione(idRegione)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, COMUNE C, PROVINCIA P," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM ORGANIZ_CLASSEPV_PUNTOVENDITA WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = P.IDPROVINCIA" & _
        " AND P.IDREGIONE = " & idRegione & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili nella regione selezionata"
    
End Sub

Private Sub PopolaListePVSelezionatiConRegione(idRegione)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, COMUNE C, PROVINCIA P" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = P.IDPROVINCIA" & _
        " AND P.IDREGIONE = " & idRegione & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita della regione selezionata"
    
End Sub

Private Sub PopolaListePVDisponibiliConProvincia(idProvincia)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, COMUNE C," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM ORGANIZ_CLASSEPV_PUNTOVENDITA WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = " & idProvincia & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili nella provincia selezionata"
    
End Sub

Private Sub PopolaListePVSelezionatiConProvincia(idProvincia)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV, COMUNE C" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND PV.IDCOMUNE = C.IDCOMUNE" & _
        " AND C.IDPROVINCIA = " & idProvincia & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita della provincia selezionata"
    
End Sub

Private Sub PopolaListePVDisponibiliConComune(idComune)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV," & _
        " (SELECT DISTINCT IDPUNTOVENDITA FROM ORGANIZ_CLASSEPV_PUNTOVENDITA WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & ") T" & _
        " WHERE PV.IDPUNTOVENDITA = T.IDPUNTOVENDITA(+)" & _
        " AND T.IDPUNTOVENDITA IS NULL" & _
        " AND IDCOMUNE = " & idComune & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaDisponibili, sql)
    lblInsiemePuntiVenditaDisponibili.Caption = "Punti vendita disponibili nel comune selezionato"
    
End Sub

Private Sub PopolaListePVSelezionatiConComune(idComune)
    Dim sql As String
    
    sql = "SELECT PV.IDPUNTOVENDITA ID, PV.NOME" & _
        " FROM PUNTOVENDITA PV, ORGANIZ_CLASSEPV_PUNTOVENDITA OCPVPV" & _
        " WHERE PV.IDPUNTOVENDITA = OCPVPV.IDPUNTOVENDITA" & _
        " AND OCPVPV.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND OCPVPV.IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
        " AND IDCOMUNE = " & idComune & _
        " ORDER BY NOME"
    Call PopolaLista(lstPuntiVenditaSelezionati, sql)
    lblInsiemePuntiVenditaSelezionati.Caption = "Punti vendita del comune selezionato"
    
End Sub

Private Sub lstRegioniDisponibili_Click()
    Dim sql As String
    
    sql = "SELECT IDPROVINCIA ID, NOME FROM PROVINCIA WHERE IDREGIONE = " & lstRegioniDisponibili.ItemData(lstRegioniDisponibili.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstProvinceDisponibili, sql)
    lstComuniDisponibili.Clear
    Call PopolaListePVDisponibiliConRegione(lstRegioniDisponibili.ItemData(lstRegioniDisponibili.ListIndex))
End Sub

Private Sub lstRegioniSelezionate_Click()
    Dim sql As String
    
    sql = "SELECT IDPROVINCIA ID, NOME FROM PROVINCIA WHERE IDREGIONE = " & lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstProvinceSelezionate, sql)
    lstComuniSelezionati.Clear
    Call PopolaListePVSelezionatiConRegione(lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex))
End Sub

Private Sub lstProvinceDisponibili_Click()
    Dim sql As String
    
    sql = "SELECT IDCOMUNE ID, NOME FROM COMUNE WHERE IDPROVINCIA = " & lstProvinceDisponibili.ItemData(lstProvinceDisponibili.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstComuniDisponibili, sql)
    Call PopolaListePVDisponibiliConProvincia(lstProvinceDisponibili.ItemData(lstProvinceDisponibili.ListIndex))
End Sub

Private Sub lstProvinceSelezionate_Click()
    Dim sql As String
    
    sql = "SELECT IDCOMUNE ID, NOME FROM COMUNE WHERE IDPROVINCIA = " & lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex) & " ORDER BY NOME"
    Call PopolaLista(lstComuniSelezionati, sql)
    Call PopolaListePVSelezionatiConProvincia(lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex))
End Sub

Private Sub lstComuniDisponibili_Click()
    Call PopolaListaPuntiVenditaDisponibili
End Sub

Private Sub PopolaListaPuntiVenditaDisponibili()
    
    If lstComuniDisponibili.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVDisponibiliConComune (lstComuniDisponibili.ItemData(lstComuniDisponibili.ListIndex))
    ElseIf lstProvinceDisponibili.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVDisponibiliConProvincia (lstProvinceDisponibili.ItemData(lstProvinceDisponibili.ListIndex))
    ElseIf lstRegioniDisponibili.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVDisponibiliConRegione (lstRegioniDisponibili.ItemData(lstRegioniDisponibili.ListIndex))
    Else
        PopolaListePVDisponibiliConClassePV (lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
    End If

End Sub

Private Sub lstComuniSelezionati_Click()
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub PopolaListaPuntiVenditaSelezionati()
    
    If lstComuniSelezionati.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVSelezionatiConComune (lstComuniSelezionati.ItemData(lstComuniSelezionati.ListIndex))
    ElseIf lstProvinceSelezionate.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVSelezionatiConProvincia (lstProvinceSelezionate.ItemData(lstProvinceSelezionate.ListIndex))
    ElseIf lstRegioniSelezionate.ListIndex <> idNessunElementoSelezionato Then
        PopolaListePVSelezionatiConRegione (lstRegioniSelezionate.ItemData(lstRegioniSelezionate.ListIndex))
    Else
        PopolaListePVSelezionatiConClassePV (lstClassiPVDaModificare.ItemData(lstClassiPVDaModificare.ListIndex))
    End If

End Sub

Private Sub cmdAbilita_Click()
    Call Abilita
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub cmdDisabilita_Click()
    Call Disabilita
    Call PopolaListaPuntiVenditaDisponibili
    Call PopolaListaPuntiVenditaSelezionati
End Sub

Private Sub Abilita()
    Dim sql As String
    Dim n As Long
    Dim idPuntoVendita As Long
    Dim i As Long
    
    For i = 0 To lstPuntiVenditaDisponibili.ListCount - 1
        If lstPuntiVenditaDisponibili.Selected(i) = True Then
            idPuntoVendita = lstPuntiVenditaDisponibili.ItemData(i)

            Call AbilitaPuntoVendita(idPuntoVendita)
        End If
    Next i

End Sub

Private Function AbilitaPuntoVendita(idPV As Long) As Long
    Dim sql As String
    
'    sql = "INSERT INTO ORGANIZ_CLASSEPV_PUNTOVENDITA (IDORGANIZZAZIONE, IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA)" & _
'        " VALUES (" & idOrganizzazioneSelezionata & ", " & idClassePVDaModificareSelezionata & ", " & idPV & ")"
    sql = "INSERT INTO ORGANIZ_CLASSEPV_PUNTOVENDITA (IDORGANIZZAZIONE, IDCLASSEPUNTOVENDITA, IDPUNTOVENDITA)" & _
        " SELECT " & idOrganizzazioneSelezionata & ", " & idClassePVDaModificareSelezionata & ", " & idPV & _
        " FROM DUAL" & _
        " WHERE (SELECT COUNT(*) FROM ORGANIZ_CLASSEPV_PUNTOVENDITA WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & " AND IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & " AND IDPUNTOVENDITA = " & idPV & ") = 0"
    AbilitaPuntoVendita = ORADB.ExecuteSQL(sql)
    
End Function

Private Sub Disabilita()
    Dim sql As String
    Dim n As Long
    Dim idPuntoVendita As Long
    Dim i As Long
    
    For i = 0 To lstPuntiVenditaSelezionati.ListCount - 1
        If lstPuntiVenditaSelezionati.Selected(i) = True Then
            idPuntoVendita = lstPuntiVenditaSelezionati.ItemData(i)

            ' disabilita prima in tutti i prodotti
            sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA" & _
                " WHERE IDPUNTOVENDITA = " & idPuntoVendita & _
                " AND IDCLASSESUPERAREAPRODOTTO IN" & _
                " (" & _
                " SELECT IDCLASSESUPERAREAPRODOTTO" & _
                " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO P" & _
                " WHERE CSP.IDPRODOTTO = P.IDPRODOTTO" & _
                " AND P.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " )"
            n = ORADB.ExecuteSQL(sql)
            
            sql = "DELETE FROM ORGANIZ_CLASSEPV_PUNTOVENDITA" & _
                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
                " AND IDCLASSEPUNTOVENDITA = " & idClassePVDaModificareSelezionata & _
                " AND IDPUNTOVENDITA = " & idPuntoVendita
            n = ORADB.ExecuteSQL(sql)
        End If
    Next i

End Sub

' VECCHIA VERSIONE ***********************************
'Option Explicit
'
'Private internalEvent As Boolean
'
'Private idRecordSelezionato As Long
'Private idOrganizzazioneSelezionata As Long
'Private SQLCaricamentoCombo As String
'Private nomeRecordSelezionato As String
'Private codiceRecordSelezionato As String
'Private descrizioneRecordSelezionato As String
'Private indirizzoRecordSelezionato As String
'Private cittaRecordSelezionato As String
'Private civicoRecordSelezionato As String
'Private provinciaRecordSelezionato As String
'Private nomeOrganizzazioneSelezionata As String
'Private isRecordEditabile As Boolean
'
'Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
'Private gestioneExitCode As ExitCodeEnum
'Private gestioneRecordGriglia As AzioneSuGrigliaEnum
'Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum
'
'Private Sub AggiornaAbilitazioneControlli()
'
'    lblInfo1.Caption = "Organizzazione"
'    txtInfo1.Text = nomeOrganizzazioneSelezionata
'    txtInfo1.Enabled = False
'
'    dgrConfigurazioneOrganizzazionePuntiVendita.Caption = "PUNTI VENDITA CONFIGURATI"
'
'    If (gestioneExitCode = EC_NON_SPECIFICATO And _
'            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
'        dgrConfigurazioneOrganizzazionePuntiVendita.Enabled = True
'        lblOperazioneInCorso.Caption = ""
'        lblOperazione.Caption = ""
'        Call cmbNome.Clear
'        txtDescrizione.Text = ""
'        txtIndirizzo.Text = ""
'        txtCitta.Text = ""
'        txtProvincia.Text = ""
'        cmbNome.Enabled = False
'        txtIndirizzo.Enabled = False
'        txtDescrizione.Enabled = False
'        txtCitta.Enabled = False
'        txtProvincia.Enabled = False
'        lblNome.Enabled = False
'        lblIndirizzo.Enabled = False
'        lblDescrizione.Enabled = False
'        lblCitta.Enabled = False
'        lblProvincia.Enabled = False
'        cmdInserisciNuovo.Enabled = True
'        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdConferma.Enabled = False
'        cmdAnnulla.Enabled = False
'
'    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
'            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
'        dgrConfigurazioneOrganizzazionePuntiVendita.Enabled = False
'        cmbNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
'        txtIndirizzo.Enabled = False
'        txtDescrizione.Enabled = False
'        txtCitta.Enabled = False
'        txtProvincia.Enabled = False
'        lblIndirizzo.Enabled = False
'        lblDescrizione.Enabled = False
'        lblCitta.Enabled = False
'        lblProvincia.Enabled = False
'        cmdInserisciNuovo.Enabled = False
'        cmdElimina.Enabled = False
'        cmdConferma.Enabled = (cmbNome.ListIndex <> idNessunElementoSelezionato)
'        cmdAnnulla.Enabled = True
'        lblOperazioneInCorso.Caption = "Operazione in corso:"
'        Select Case gestioneRecordGriglia
'            Case ASG_INSERISCI_NUOVO
'                lblOperazione.Caption = "inserimento nuovo record"
'            Case ASG_INSERISCI_DA_SELEZIONE
'                lblOperazione.Caption = "inserimento nuovo record"
'            Case ASG_MODIFICA
'                lblOperazione.Caption = "modifica record selezionato"
'            Case ASG_ELIMINA
'                lblOperazione.Caption = "eliminazione record selezionato"
'            Case Else
'                'Do Nothing
'        End Select
'
'    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
'            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
'        dgrConfigurazioneOrganizzazionePuntiVendita.Enabled = True
'        lblOperazioneInCorso.Caption = ""
'        lblOperazione.Caption = ""
'        Call cmbNome.Clear
'        txtDescrizione.Text = ""
'        txtIndirizzo.Text = ""
'        txtCitta.Text = ""
'        txtProvincia.Text = ""
'        cmbNome.Enabled = False
'        txtIndirizzo.Enabled = False
'        txtDescrizione.Enabled = False
'        txtCitta.Enabled = False
'        txtProvincia.Enabled = False
'        lblNome.Enabled = False
'        lblIndirizzo.Enabled = False
'        lblDescrizione.Enabled = False
'        lblCitta.Enabled = False
'        lblProvincia.Enabled = False
'        cmdInserisciNuovo.Enabled = True
'        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
'        cmdConferma.Enabled = False
'        cmdAnnulla.Enabled = False
'
'    Else
'        'Do Nothing
'    End If
'
'End Sub
'
'Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
'    gestioneFormCorrente = gs
'End Sub
'
'Public Sub SetIdOrganizzazioneSelezionata(id As Long)
'    idOrganizzazioneSelezionata = id
'End Sub
'
'Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
'    nomeOrganizzazioneSelezionata = nome
'End Sub
'
'Private Sub cmdAnnulla_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call Annulla
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub Annulla()
'    Call SetGestioneExitCode(EC_ANNULLA)
'    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub cmdConferma_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call Conferma
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub Conferma()
'    Dim stringaNota As String
'    Dim causaNonEditabilita As String
'    Dim isConfigurabile As Boolean
'
'    causaNonEditabilita = ""
'    stringaNota = "IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'        "; IDPUNTOVENDITA = " & idRecordSelezionato
'
'    If IsOrganizzazioneEditabile(idOrganizzazioneSelezionata, causaNonEditabilita) Then
'        isConfigurabile = True
'        If tipoStatoOrganizzazione = TSO_ATTIVA Then
'            Call frmMessaggio.Visualizza("ConfermaEditabilitāOrganizzazioneAttiva")
'            If frmMessaggio.exitCode <> EC_CONFERMA Then
'                isConfigurabile = False
'            End If
'        End If
'        If isConfigurabile Then
'            Call SetGestioneExitCode(EC_CONFERMA)
'            Call AggiornaAbilitazioneControlli
'
'            If ValoriCampiOK Then
'                If isRecordEditabile Then
'                    Select Case gestioneRecordGriglia
'                        Case ASG_INSERISCI_NUOVO
'                            Call InserisciNellaBaseDati
'                            Call ScriviLog(CCTA_INSERIMENTO, CCDA_ORGANIZZAZIONE, CCDA_PUNTO_VENDITA, stringaNota, idOrganizzazioneSelezionata)
'                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'                            Call adcConfigurazioneOrganizzazionePuntiVendita_Init
'                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
'                            Call dgrConfigurazioneOrganizzazionePuntiVendita_Init
'                        Case ASG_ELIMINA
'                            Call EliminaDallaBaseDati
'                            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_ORGANIZZAZIONE, CCDA_PUNTO_VENDITA, stringaNota, idOrganizzazioneSelezionata)
'                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'                            Call adcConfigurazioneOrganizzazionePuntiVendita_Init
'                            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'                            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
'                            Call dgrConfigurazioneOrganizzazionePuntiVendita_Init
'                        Case Else
'                            'Do Nothing
'                    End Select
'                End If
'                Call AggiornaAbilitazioneControlli
'            End If
'        End If
'    Else
'        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
'    End If
'End Sub
'
'Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
'    gestioneExitCode = ec
'End Sub
'
'Private Sub cmdElimina_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call Elimina
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub Elimina()
'    Dim sql As String
'
'    sql = "SELECT IDPUNTOVENDITA AS ID, NOME FROM PUNTOVENDITA" & _
'        " WHERE IDPUNTOVENDITA = " & idRecordSelezionato
'    Call SetGestioneRecordGriglia(ASG_ELIMINA)
'    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call AggiornaAbilitazioneControlli
'    Call GetIdRecordSelezionato
'    Call CaricaDallaBaseDati
'    Call CaricaValoriCombo(cmbNome, sql, "NOME")
'    Call AssegnaValoriCampi
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub cmdEsci_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call Esci
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub Esci()
'    Unload Me
'End Sub
'
'Private Sub cmdInserisciNuovo_Click()
'    Dim mousePointerOld As Integer
'
'    mousePointerOld = MousePointer
'    MousePointer = vbHourglass
'
'    Call InserisciNuovo
'
'    MousePointer = mousePointerOld
'End Sub
'
'Private Sub InserisciNuovo()
'    Dim sql As String
'
'    isRecordEditabile = True
'    sql = "SELECT P.IDPUNTOVENDITA ID, P.NOME, P.INDIRIZZO, P.DESCRIZIONE" & _
'        " FROM PUNTOVENDITA P, ORGANIZZAZIONE_PUNTOVENDITA OP" & _
'        " WHERE P.IDPUNTOVENDITA = OP.IDPUNTOVENDITA(+)" & _
'        " AND OP.IDPUNTOVENDITA IS NULL" & _
'        " AND OP.IDORGANIZZAZIONE(+) = " & idOrganizzazioneSelezionata & _
'        " ORDER BY NOME"
'    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
'    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call AggiornaAbilitazioneControlli
'    Call CaricaValoriCombo(cmbNome, sql, "NOME")
'End Sub
'
'Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim i As Integer
'
'    Call ApriConnessioneBD
'
'    sql = strSQL
'
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        i = 1
'        rec.MoveFirst
'        While Not rec.EOF
'            cmb.AddItem rec(NomeCampo)
'            cmb.ItemData(i - 1) = rec("ID").Value
'            i = i + 1
'            rec.MoveNext
'        Wend
'    End If
'
'    rec.Close
'    Call ChiudiConnessioneBD
'
'End Sub
'
'Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
'    gestioneRecordGriglia = asg
'End Sub
'
'Private Sub cmbNome_Click()
'    Call CaricaValoriCampiTesto
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub CaricaValoriCampiTesto()
'    idRecordSelezionato = cmbNome.ItemData(cmbNome.ListIndex)
'    Call CaricaDallaBaseDati
'    Call AssegnaValoriCampi
'End Sub
'
'Private Sub dgrConfigurazioneOrganizzazionePuntiVendita_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
'    If Not internalEvent Then
'        Call GetIdRecordSelezionato
'    End If
'End Sub
'
'Public Sub Init()
'    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
'    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
'    Call adcConfigurazioneOrganizzazionePuntiVendita_Init
'    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
'    Call dgrConfigurazioneOrganizzazionePuntiVendita_Init
'    Call Me.Show(vbModal)
'End Sub
'
'Private Sub GetIdRecordSelezionato()
'    Dim rec As ADODB.Recordset
'
'    Set rec = adcConfigurazioneOrganizzazionePuntiVendita.Recordset
'    If Not (rec.BOF) Then
'        If rec.EOF Then
'            rec.MoveFirst
'        End If
'        idRecordSelezionato = rec(0).Value
'    Else
'        idRecordSelezionato = idNessunElementoSelezionato
'    End If
'
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Public Sub SetIdRecordSelezionato(id As Long)
'    idRecordSelezionato = id
'End Sub
'
'Private Sub adcConfigurazioneOrganizzazionePuntiVendita_Init()
'    Dim internalEventOld As Boolean
'    Dim d As Adodc
'    Dim sql As String
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    Set d = adcConfigurazioneOrganizzazionePuntiVendita
'
''    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        sql = "SELECT P.IDPUNTOVENDITA ID," & _
''            " NOME ""Nome"", DESCRIZIONE ""Descrizione""," & _
''            " INDIRIZZO ""Indirizzo"", CITTA ""Citta"", PROVINCIA ""Prov.""" & _
''            " FROM PUNTOVENDITA P, ORGANIZZAZIONE_PUNTOVENDITA O" & _
''            " WHERE P.IDPUNTOVENDITA = O.IDPUNTOVENDITA" & _
''            " AND O.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
''            " AND (O.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & _
''            " OR O.IDTIPOSTATORECORD IS NULL)" & _
''            " ORDER BY ""Nome"""
''    Else
'        sql = "SELECT P.IDPUNTOVENDITA ID," & _
'            " NOME ""Nome"", DESCRIZIONE ""Descrizione""," & _
'            " INDIRIZZO ""Indirizzo"", CITTA ""Citta"", PROVINCIA ""Prov.""" & _
'            " FROM PUNTOVENDITA P, ORGANIZZAZIONE_PUNTOVENDITA O" & _
'            " WHERE P.IDPUNTOVENDITA = O.IDPUNTOVENDITA" & _
'            " AND O.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " ORDER BY ""Nome"""
''    End If
'    d.ConnectionString = StringaDiConnessione
'    d.RecordSource = sql
'    d.Refresh
'
'    Set dgrConfigurazioneOrganizzazionePuntiVendita.dataSource = d
'
'    internalEvent = internalEventOld
'
'    Call AggiornaAbilitazioneControlli
'End Sub
'
'Private Sub InserisciNellaBaseDati()
'    Dim sql As String
'    Dim n As Long
'    Dim condizioneSql As String
'
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
'    sql = "INSERT INTO ORGANIZZAZIONE_PUNTOVENDITA (IDORGANIZZAZIONE, IDPUNTOVENDITA)" & _
'    " VALUES (" & idOrganizzazioneSelezionata & ", " & idRecordSelezionato & ")"
'    SETAConnection.Execute sql, n, adCmdText
''    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        tipoStatoRecordSelezionato = TSR_NUOVO
''        condizioneSql = " AND IDPUNTOVENDITA = " & idRecordSelezionato
''        Call AggiornaParametriSessioneSuRecord("ORGANIZZAZIONE_PUNTOVENDITA", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_NUOVO, condizioneSql)
''    End If
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Call SetIdRecordSelezionato(idRecordSelezionato)
'    Call AggiornaAbilitazioneControlli
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
'
'End Sub
'
'Private Sub CaricaDallaBaseDati()
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim idSessione As Long
'
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    isRecordEditabile = True
''    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        sql = "SELECT NOME, INDIRIZZO, DESCRIZIONE, CITTA, PROVINCIA, CIVICO," & _
''            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE" & _
''            " FROM PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idRecordSelezionato
''    Else
'        sql = "SELECT NOME, INDIRIZZO, DESCRIZIONE, CITTA, PROVINCIA, CIVICO" & _
'            " FROM PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idRecordSelezionato
''    End If
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.BOF And rec.EOF) Then
'        nomeRecordSelezionato = rec("NOME")
'        indirizzoRecordSelezionato = IIf(IsNull(rec("INDIRIZZO")), "", rec("INDIRIZZO"))
'        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
'        cittaRecordSelezionato = IIf(IsNull(rec("CITTA")), "", rec("CITTA"))
'        provinciaRecordSelezionato = IIf(IsNull(rec("PROVINCIA")), "", rec("PROVINCIA"))
'        civicoRecordSelezionato = IIf(IsNull(rec("CIVICO")), "", rec("CIVICO"))
''        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
''            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
''            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
''                idSessione = idSessioneConfigurazioneCorrente)
''        End If
'    End If
'
'    rec.Close
'    Call ChiudiConnessioneBD
'
'    Exit Sub
'
'gestioneErrori:
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
'
'End Sub
'
'Private Sub AssegnaValoriCampi()
'    Dim internalEventOld As Boolean
'    Dim i As Integer
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    Call SelezionaElementoSuCombo(cmbNome, idRecordSelezionato)
'    txtDescrizione.Text = ""
'    txtDescrizione.Text = descrizioneRecordSelezionato
'    txtIndirizzo.Text = ""
'    txtIndirizzo.Text = IIf(Trim(indirizzoRecordSelezionato) = "", "", indirizzoRecordSelezionato & ", " & civicoRecordSelezionato)
'    txtCitta.Text = ""
'    txtCitta.Text = cittaRecordSelezionato
'    txtProvincia.Text = ""
'    txtProvincia.Text = provinciaRecordSelezionato
'
'    internalEvent = internalEventOld
'
'End Sub
'
'Private Sub EliminaDallaBaseDati()
'    Dim sql As String
'    Dim SqlConstraints As String
'    Dim n As Long
'
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
''    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''        If tipoStatoRecordSelezionato = TSR_NUOVO Then
''            sql = "DELETE FROM ORGANIZZAZIONE_PUNTOVENDITA" & _
''                " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
''                " AND IDPUNTOVENDITA" & " = " & idRecordSelezionato
''            SETAConnection.Execute sql, n, adCmdText
''        Else
''            SqlConstraints = " AND IDPUNTOVENDITA = " & idRecordSelezionato
''            Call AggiornaParametriSessioneSuRecord("ORGANIZZAZIONE_PUNTOVENDITA", "IDORGANIZZAZIONE", idOrganizzazioneSelezionata, TSR_ELIMINATO, SqlConstraints)
''        End If
''    Else
'        sql = "DELETE FROM ORGANIZZAZIONE_PUNTOVENDITA" & _
'            " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
'            " AND IDPUNTOVENDITA" & " = " & idRecordSelezionato
'        SETAConnection.Execute sql, n, adCmdText
''    End If
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
'
'End Sub
'
'Private Sub dgrConfigurazioneOrganizzazionePuntiVendita_Init()
'    Dim g As DataGrid
'    Dim dimensioneGrid As Long
'    Dim numeroCampi As Integer
'
'    Set g = dgrConfigurazioneOrganizzazionePuntiVendita
'    g.ScrollBars = dbgVertical
'    dimensioneGrid = g.Width - 100
'    numeroCampi = 5
'    g.Columns(0).Visible = False
'    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
'
'    g.MarqueeStyle = dbgHighlightRow
'End Sub
'
'Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
'    Dim i As Integer
'
'    If id = idNessunElementoSelezionato Then
'        cmb.ListIndex = idNessunElementoSelezionato
'    Else
'        For i = 1 To cmb.ListCount
'            If id = cmb.ItemData(i - 1) Then
'                cmb.ListIndex = i - 1
'            End If
'        Next i
'    End If
'
'End Sub
'
'Private Sub SelezionaElementoSuGriglia(id As Long)
'    Dim rec As ADODB.Recordset
'    Dim internalEventOld As Boolean
'
'
'    internalEventOld = internalEvent
'    internalEvent = True
'
'    Set rec = adcConfigurazioneOrganizzazionePuntiVendita.Recordset
'    If Not (rec.BOF And rec.EOF) Then
'        rec.MoveFirst
'        Do While Not rec.EOF
'            If id = rec("ID") Then
'                Exit Do
'            End If
'            rec.MoveNext
'        Loop
'    End If
'    internalEvent = internalEventOld
'
'End Sub
'
'Private Function ValoriCampiOK() As Boolean
'
'    ValoriCampiOK = True
'    indirizzoRecordSelezionato = Trim(txtIndirizzo.Text)
'    nomeRecordSelezionato = Trim(cmbNome.Text)
'    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
'
'End Function

Private Sub SelezionaTuttiDaDisabilitare()
    Dim i As Long
    
    For i = 0 To lstPuntiVenditaSelezionati.ListCount - 1
        lstPuntiVenditaSelezionati.Selected(i) = True
    Next i

End Sub

