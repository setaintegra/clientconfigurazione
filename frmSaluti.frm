VERSION 5.00
Begin VB.Form frmSaluti 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Informazioni su"
   ClientHeight    =   4830
   ClientLeft      =   4020
   ClientTop       =   1725
   ClientWidth     =   6360
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   Picture         =   "frmSaluti.frx":0000
   ScaleHeight     =   322
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   424
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pcbIntegra 
      Height          =   675
      Left            =   5640
      Picture         =   "frmSaluti.frx":6F1B
      ScaleHeight     =   615
      ScaleWidth      =   615
      TabIndex        =   3
      Top             =   3840
      Width           =   675
   End
   Begin VB.CommandButton cmdChiudi 
      Caption         =   "Chiudi"
      Height          =   495
      Left            =   2400
      TabIndex        =   2
      Top             =   4200
      Width           =   1515
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Auguri!"
      BeginProperty Font 
         Name            =   "Calisto MT"
         Size            =   72
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1995
      Left            =   120
      TabIndex        =   4
      Top             =   2040
      Width           =   6255
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Dario!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   555
      Left            =   240
      TabIndex        =   1
      Top             =   1800
      Width           =   3015
   End
   Begin VB.Label lblAzienda 
      BackStyle       =   0  'Transparent
      Caption         =   "Gli amici di IS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4200
      TabIndex        =   0
      Top             =   3840
      Width           =   1275
   End
End
Attribute VB_Name = "frmSaluti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub Init()
    Call frmSaluti.Show(vbModal)
End Sub

Private Sub cmdChiudi_Click()
    Unload Me
End Sub

