VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfigurazioneSpettacoloGenereSiae 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Spettacolo"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8580
      TabIndex        =   16
      Top             =   240
      Width           =   3255
   End
   Begin MSComCtl2.UpDown updIncidenza 
      Height          =   315
      Left            =   10095
      TabIndex        =   15
      Top             =   6180
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   556
      _Version        =   393216
      BuddyControl    =   "txtIncidenza"
      BuddyDispid     =   196616
      OrigLeft        =   6060
      OrigTop         =   6120
      OrigRight       =   6300
      OrigBottom      =   6495
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10680
      TabIndex        =   6
      Top             =   8100
      Width           =   1155
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   12
      Top             =   4860
      Width           =   2775
      Begin VB.CommandButton cmdElimina 
         Caption         =   "Elimina"
         Height          =   435
         Left            =   1440
         TabIndex        =   1
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdInserisciNuovo 
         Caption         =   "Inserisci Nuovo"
         Height          =   435
         Left            =   180
         TabIndex        =   0
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ComboBox cmbNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   6180
      Width           =   9375
   End
   Begin VB.TextBox txtIncidenza 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9600
      MaxLength       =   3
      TabIndex        =   3
      Top             =   6180
      Width           =   495
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   7
      Top             =   7680
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneSpettacoloGenereSiae 
      Height          =   330
      Left            =   240
      Top             =   4080
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneSpettacoloGenereSiae 
      Height          =   3915
      Left            =   120
      TabIndex        =   8
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   6906
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8580
      TabIndex        =   17
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   14
      Top             =   4620
      Width           =   2775
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome (Descrizione) - Codice"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   5940
      Width           =   3315
   End
   Begin VB.Label lblIncidenza 
      Caption         =   "Incidenza"
      Height          =   255
      Left            =   9600
      TabIndex        =   10
      Top             =   5940
      Width           =   915
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione dei Generi SIAE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   5835
   End
End
Attribute VB_Name = "frmConfigurazioneSpettacoloGenereSiae"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private nomeRecordSelezionato As String
Private codiceRecordSelezionato As String
Private idSpettacoloSelezionato As Long
Private SQLCaricamentoCombo As String
Private nomeSpettacoloSelezionato As String
Private incidenza As Long

Private gestioneExitCode As ExitCodeEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private gestioneFormCorrente As GestioneConfigurazioneOrganizzazioneEnum

Private Sub AggiornaAbilitazioneControlli()
     
    lblInfo1.Caption = "Spettacolo"
    txtInfo1.Text = nomeSpettacoloSelezionato
    txtInfo1.Enabled = False
    
'    txtIncidenza.Enabled = False
    
    dgrConfigurazioneSpettacoloGenereSiae.Caption = "GENERI SIAE CONFIGURATI"
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneSpettacoloGenereSiae.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbNome.Clear
        cmbNome.Enabled = False
        lblNome.Enabled = False
        txtIncidenza.Text = ""
        lblIncidenza.Enabled = False
        updIncidenza.Enabled = False
        txtIncidenza.Enabled = False
        lblIncidenza.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneSpettacoloGenereSiae.Enabled = False
        cmbNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblNome.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblIncidenza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        updIncidenza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        updIncidenza.max = MaxIncidenzaPossibile
        txtIncidenza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        lblIncidenza.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdInserisciNuovo.Enabled = False
        cmdElimina.Enabled = False
        cmdConferma.Enabled = (cmbNome.ListIndex <> idNessunElementoSelezionato And _
                                Trim(txtIncidenza.Text) <> "" And Trim(txtIncidenza.Text) <> "0")
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneSpettacoloGenereSiae.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        Call cmbNome.Clear
        cmbNome.Enabled = False
        lblNome.Enabled = False
        txtIncidenza.Text = ""
        updIncidenza.Enabled = False
        lblIncidenza.Enabled = False
        txtIncidenza.Enabled = False
        lblIncidenza.Enabled = False
        cmdInserisciNuovo.Enabled = True
        cmdElimina.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
End Sub

Public Sub SetGestioneFormCorrente(gs As GestioneConfigurazioneOrganizzazioneEnum)
    gestioneFormCorrente = gs
End Sub

Public Sub SetIdSpettacoloSelezionato(id As Long)
    idSpettacoloSelezionato = id
End Sub

Public Sub SetNomeSpettacoloSelezionato(nome As String)
    nomeSpettacoloSelezionato = nome
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    Call SetGestioneExitCode(EC_CONFERMA)
    Call AggiornaAbilitazioneControlli
    stringaNota = "IDSPETTACOLO = " & idSpettacoloSelezionato

    Select Case gestioneRecordGriglia
        Case ASG_INSERISCI_NUOVO
            If ValoriCampiOK Then
                Call InserisciNellaBaseDati
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_SPETTACOLO, CCDA_GENERE_SIAE, stringaNota)
                Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                Call adcConfigurazioneSpettacoloGenereSiae_Init
                Call SelezionaElementoSuGriglia(idRecordSelezionato)
                Call dgrConfigurazioneSpettacoloGenereSiae_Init
            End If
        Case ASG_ELIMINA
            Call EliminaDallaBaseDati
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_SPETTACOLO, CCDA_GENERE_SIAE, stringaNota)
            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
            Call adcConfigurazioneSpettacoloGenereSiae_Init
            Call SetIdRecordSelezionato(idNessunElementoSelezionato)
            Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
            Call dgrConfigurazioneSpettacoloGenereSiae_Init
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim sql As String
    
    sql = "SELECT IDGENERESIAE AS ID, NOME, DESCRIZIONE, CODICE FROM GENERESIAE" & _
        " WHERE IDGENERESIAE > 0"
    Call SetGestioneRecordGriglia(ASG_ELIMINA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call CaricaValoriCombo(cmbNome, sql)
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdInserisciNuovo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call InserisciNuovo
    
    MousePointer = mousePointerOld
End Sub

Private Sub InserisciNuovo()
    Dim sql As String
    
'    modificata per introduzione prodotti non fiscali: permesso anche GENERE SIAE = 0
'    sql = "SELECT G.IDGENERESIAE AS ID, NOME, CODICE, DESCRIZIONE" & _
'        " FROM GENERESIAE G, SPETTACOLO_GENERESIAE S" & _
'        " WHERE G.IDGENERESIAE > 0" & _
'        " AND G.IDGENERESIAE = S.IDGENERESIAE(+)" & _
'        " AND S.IDGENERESIAE IS NULL" & _
'        " AND S.IDSPETTACOLO(+) = " & idSpettacoloSelezionato & _
'        " ORDER BY NOME"
    sql = "SELECT G.IDGENERESIAE AS ID, NOME, CODICE, DESCRIZIONE" & _
        " FROM GENERESIAE G, SPETTACOLO_GENERESIAE S" & _
        " WHERE G.IDGENERESIAE = S.IDGENERESIAE(+)" & _
        " AND S.IDGENERESIAE IS NULL" & _
        " AND S.IDSPETTACOLO(+) = " & idSpettacoloSelezionato & _
        " ORDER BY NOME"
    Call SetGestioneRecordGriglia(ASG_INSERISCI_NUOVO)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriCombo(cmbNome, sql, "NOME")
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, Optional NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim datiRecord As String
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            datiRecord = rec("NOME") & " (" & rec("DESCRIZIONE") & ") - " & rec("CODICE")
            cmb.AddItem datiRecord
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
        
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmbNome_Click()
    idRecordSelezionato = cmbNome.ItemData(cmbNome.ListIndex)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrConfigurazioneSpettacoloGenereSiae_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneSpettacoloGenereSiae_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneSpettacoloGenereSiae_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneSpettacoloGenereSiae.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec(0).Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneSpettacoloGenereSiae_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set d = adcConfigurazioneSpettacoloGenereSiae
    
    sql = "SELECT GENERESIAE.IDGENERESIAE ID,"
    sql = sql & " GENERESIAE.NOME AS ""Nome"", GENERESIAE.CODICE AS ""Codice"","
    sql = sql & " GENERESIAE.DESCRIZIONE AS ""Descrizione"","
    sql = sql & " GENERESIAE.DESCRIZIONEPOS AS ""Descrizione POS"","
    sql = sql & " SPETTACOLO_GENERESIAE.INCIDENZA AS ""Incidenza (%)"", SOMMA"
    sql = sql & " FROM GENERESIAE, SPETTACOLO_GENERESIAE, (SELECT SUM(INCIDENZA) AS SOMMA"
    sql = sql & " FROM GENERESIAE, SPETTACOLO_GENERESIAE WHERE"
    sql = sql & " (GENERESIAE.IDGENERESIAE = SPETTACOLO_GENERESIAE.IDGENERESIAE) AND"
    sql = sql & " (SPETTACOLO_GENERESIAE.IDSPETTACOLO = " & idSpettacoloSelezionato & ")) WHERE"
    sql = sql & " (GENERESIAE.IDGENERESIAE = SPETTACOLO_GENERESIAE.IDGENERESIAE) AND"
    sql = sql & " (SPETTACOLO_GENERESIAE.IDSPETTACOLO = " & idSpettacoloSelezionato & ")"
    sql = sql & " ORDER BY NOME"
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneSpettacoloGenereSiae.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
    sql = "INSERT INTO SPETTACOLO_GENERESIAE (IDSPETTACOLO, IDGENERESIAE, INCIDENZA)" & _
        " VALUES (" & _
        idSpettacoloSelezionato & ", " & _
        idRecordSelezionato & ", " & _
        incidenza & ")"
        SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call SetIdRecordSelezionato(idRecordSelezionato)
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT GENERESIAE.IDGENERESIAE ID, GENERESIAE.NOME NOME," & _
        " GENERESIAE.CODICE CODICE, SPETTACOLO_GENERESIAE.INCIDENZA INCIDENZA" & _
        " FROM GENERESIAE, SPETTACOLO_GENERESIAE WHERE" & _
        " (GENERESIAE.IDGENERESIAE = SPETTACOLO_GENERESIAE.IDGENERESIAE) AND" & _
        " (GENERESIAE.IDGENERESIAE = " & idRecordSelezionato & ") AND" & _
        " SPETTACOLO_GENERESIAE.IDSPETTACOLO = " & idSpettacoloSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        codiceRecordSelezionato = rec("CODICE")
        incidenza = rec("INCIDENZA")
    End If

    rec.Close
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True
    
    Call SelezionaElementoSuCombo(cmbNome, idRecordSelezionato)
    txtIncidenza.Text = ""
    txtIncidenza.Text = incidenza

    internalEvent = internalEventOld

End Sub

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "DELETE FROM SPETTACOLO_GENERESIAE" & _
        " WHERE IDSPETTACOLO = " & idSpettacoloSelezionato & _
        " AND IDGENERESIAE" & " = " & idRecordSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub dgrConfigurazioneSpettacoloGenereSiae_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneSpettacoloGenereSiae
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Visible = False
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneSpettacoloGenereSiae.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Function ValoriCampiOK() As Boolean
    Dim listaNonConformitą As Collection

On Error Resume Next

    ValoriCampiOK = True
    Set listaNonConformitą = New Collection
    
    idRecordSelezionato = cmbNome.ItemData(cmbNome.ListIndex)
    If IsCampoIncidenzaCorretto(txtIncidenza) Then
        incidenza = CInt(Trim(txtIncidenza.Text))
    Else
'        Call frmMessaggio.Visualizza("ErroreRangeCampoIncidenza")
        ValoriCampiOK = False
        Call listaNonConformitą.Add("- il valore immesso sul campo Incidenza deve essere numerico di tipo intero compreso tra 0 e 100;")
    End If
    
    If listaNonConformitą.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformitąCampi", ArgomentoMessaggio(listaNonConformitą))
    End If

End Function

Private Sub txtIncidenza_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Function MaxIncidenzaPossibile() As Integer
    Dim rec As ADODB.Recordset
    Dim somma As Integer
    
    Call ApriConnessioneBD
    
    Set rec = adcConfigurazioneSpettacoloGenereSiae.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        somma = rec("SOMMA").Value
        MaxIncidenzaPossibile = 100 - somma
    Else
        MaxIncidenzaPossibile = 100
    End If
    
    Call ChiudiConnessioneBD
    
End Function
