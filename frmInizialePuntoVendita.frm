VERSION 5.00
Begin VB.Form frmInizialePuntoVendita 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Punto Vendita"
   ClientHeight    =   9585
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13785
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9585
   ScaleWidth      =   13785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstOperatoriAfferenti 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   6120
      MultiSelect     =   2  'Extended
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   5400
      Width           =   5415
   End
   Begin VB.Frame Frame1 
      Caption         =   "Caratteristiche"
      Height          =   7755
      Left            =   120
      TabIndex        =   19
      Top             =   600
      Width           =   11655
      Begin VB.CommandButton cmdTerminaleVirtuale 
         Caption         =   "aggiungi/modifica"
         Height          =   375
         Left            =   6960
         TabIndex        =   43
         Top             =   7080
         Width           =   1575
      End
      Begin VB.TextBox txtTerminaleVirtuale 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4800
         MaxLength       =   20
         TabIndex        =   42
         Top             =   7080
         Width           =   2175
      End
      Begin VB.CommandButton cmdElencaComuni 
         Caption         =   "elenca"
         Height          =   375
         Left            =   5040
         TabIndex        =   39
         Top             =   3900
         Width           =   855
      End
      Begin VB.ComboBox cmbComune 
         Height          =   315
         Left            =   6000
         TabIndex        =   38
         Top             =   3900
         Width           =   3375
      End
      Begin VB.TextBox txtComune 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   37
         Top             =   3900
         Width           =   3255
      End
      Begin VB.ListBox lstOrganizzazioni 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1950
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   4800
         Width           =   5535
      End
      Begin VB.ComboBox cmbTipoPuntoVendita 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7020
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   2355
      End
      Begin VB.TextBox txtCodiceSAP 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   6
         TabIndex        =   3
         Top             =   1500
         Width           =   855
      End
      Begin VB.TextBox txtProvincia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9000
         MaxLength       =   2
         TabIndex        =   8
         Top             =   2640
         Width           =   375
      End
      Begin VB.TextBox txtNome 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   0
         Top             =   360
         Width           =   3255
      End
      Begin VB.TextBox txtDescrizione 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   780
         Width           =   7635
      End
      Begin VB.TextBox txtIndirizzo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   1740
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   1920
         Width           =   7635
      End
      Begin VB.TextBox txtCivico 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   8
         TabIndex        =   5
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox txtCitta 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4920
         MaxLength       =   30
         TabIndex        =   7
         Top             =   2640
         Width           =   3255
      End
      Begin VB.TextBox txtCap 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3480
         MaxLength       =   5
         TabIndex        =   6
         Top             =   2640
         Width           =   675
      End
      Begin VB.TextBox txtTelefono 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   16
         TabIndex        =   9
         Top             =   3060
         Width           =   1815
      End
      Begin VB.TextBox txtFax 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4140
         MaxLength       =   16
         TabIndex        =   10
         Top             =   3060
         Width           =   1815
      End
      Begin VB.TextBox txtResponsabile 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         MaxLength       =   30
         TabIndex        =   11
         Top             =   3480
         Width           =   3255
      End
      Begin VB.TextBox txtEMail 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5940
         MaxLength       =   32
         TabIndex        =   12
         Top             =   3480
         Width           =   3495
      End
      Begin VB.Label lblTerminaleVirtuale 
         Alignment       =   1  'Right Justify
         Caption         =   "terminale virtuale"
         Height          =   255
         Left            =   3120
         TabIndex        =   44
         Top             =   7080
         Width           =   1515
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Organizzazioni collegate"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   41
         Top             =   4560
         Width           =   5535
      End
      Begin VB.Label lblComune 
         Alignment       =   1  'Right Justify
         Caption         =   "comune"
         Height          =   255
         Left            =   600
         TabIndex        =   36
         Top             =   3960
         Width           =   1035
      End
      Begin VB.Label lblOperatoriAfferenti 
         Alignment       =   2  'Center
         Caption         =   "Operatori afferenti"
         Height          =   195
         Left            =   6000
         TabIndex        =   35
         Top             =   4560
         Width           =   5415
      End
      Begin VB.Label lblTipoPuntoVendita 
         Alignment       =   1  'Right Justify
         Caption         =   "tipo punto vendita"
         Height          =   255
         Left            =   5580
         TabIndex        =   34
         Top             =   420
         Width           =   1275
      End
      Begin VB.Label lblSpiegazioneCodiceSAP 
         Caption         =   "(lasciare il campo vuoto per escludere il punto vendita dalla contabilizzazione)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2760
         TabIndex        =   33
         Top             =   1560
         Width           =   6855
      End
      Begin VB.Label lblCodiceSAP 
         Alignment       =   1  'Right Justify
         Caption         =   "codice SAP"
         Height          =   255
         Left            =   540
         TabIndex        =   32
         Top             =   1560
         Width           =   1035
      End
      Begin VB.Label lblProvincia 
         Alignment       =   1  'Right Justify
         Caption         =   "prov."
         Height          =   255
         Left            =   8460
         TabIndex        =   31
         Top             =   2700
         Width           =   375
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "nome"
         Height          =   255
         Left            =   540
         TabIndex        =   29
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblIndirizzo 
         Alignment       =   1  'Right Justify
         Caption         =   "indirizzo"
         Height          =   255
         Left            =   540
         TabIndex        =   28
         Top             =   1920
         Width           =   1035
      End
      Begin VB.Label lblDescrizione 
         Alignment       =   1  'Right Justify
         Caption         =   "descrizione"
         Height          =   255
         Left            =   480
         TabIndex        =   27
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblCivico 
         Alignment       =   1  'Right Justify
         Caption         =   "civico"
         Height          =   255
         Left            =   540
         TabIndex        =   26
         Top             =   2700
         Width           =   1035
      End
      Begin VB.Label lblCitta 
         Alignment       =   1  'Right Justify
         Caption         =   "citt�"
         Height          =   255
         Left            =   4260
         TabIndex        =   25
         Top             =   2700
         Width           =   495
      End
      Begin VB.Label lblCap 
         Alignment       =   1  'Right Justify
         Caption         =   "CAP"
         Height          =   255
         Left            =   2880
         TabIndex        =   24
         Top             =   2700
         Width           =   435
      End
      Begin VB.Label lblTelefono 
         Alignment       =   1  'Right Justify
         Caption         =   "telefono"
         Height          =   255
         Left            =   540
         TabIndex        =   23
         Top             =   3120
         Width           =   1035
      End
      Begin VB.Label lblFax 
         Alignment       =   1  'Right Justify
         Caption         =   "fax"
         Height          =   255
         Left            =   3660
         TabIndex        =   22
         Top             =   3120
         Width           =   315
      End
      Begin VB.Label lblResponsabile 
         Alignment       =   1  'Right Justify
         Caption         =   "responsabile"
         Height          =   255
         Left            =   540
         TabIndex        =   21
         Top             =   3540
         Width           =   1035
      End
      Begin VB.Label lblEMail 
         Alignment       =   1  'Right Justify
         Caption         =   "E-mail"
         Height          =   255
         Left            =   5280
         TabIndex        =   20
         Top             =   3540
         Width           =   495
      End
   End
   Begin VB.Frame fraAzioni 
      Height          =   795
      Left            =   11880
      TabIndex        =   18
      Top             =   600
      Width           =   1695
      Begin VB.CommandButton cmdTerminaliAvanzati 
         Caption         =   "Terminali"
         Height          =   435
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.Frame fraGestioneForm 
      Height          =   915
      Left            =   120
      TabIndex        =   17
      Top             =   8520
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   15
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "IntestazioneForm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      TabIndex        =   30
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmInizialePuntoVendita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idPuntoVenditaSelezionato As Long
Private nomePuntoVendita As String
Private descrizione As String
Private civico As String
Private localita As String
Private indirizzo As String
Private cap As String
Private citta As String
Private provincia As String
Private telefono As String
Private fax As String
Private eMail As String
Private codiceSAP As String
Private responsabile As String
Private idComuneSelezionato As Long
Private comune As String

Private internalEvent As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private modalit�FormCorrente As AzioneEnum
Private idTipoPuntoVendita As TipoPuntoVenditaEnum

Public Sub Init()
    Dim sql As String

    Call Variabili_Init
    Call AggiornaAbilitazioneControlli
    Call lstOrganizzazioni.Clear
    
    sql = "SELECT IDTIPOPUNTOVENDITA ID, NOME" & _
        " FROM TIPOPUNTOVENDITA WHERE IDTIPOPUNTOVENDITA > 0" & _
        " ORDER BY NOME"
    Select Case modalit�FormCorrente
        Case A_NUOVO
            Call CaricaValoriCombo(cmbTipoPuntoVendita, sql, "NOME")
        Case A_MODIFICA
            Call CaricaDallaBaseDati
            Call CaricaValoriCombo(cmbTipoPuntoVendita, sql, "NOME")
            Call AssegnaValoriCampi
        Case A_ELIMINA
            Call CaricaDallaBaseDati
            Call CaricaValoriCombo(cmbTipoPuntoVendita, sql, "NOME")
            Call AssegnaValoriCampi
        Case Else
            'Do Nothing
    End Select
    
    Call CaricaValoriLstOrganizzazioni
    Call CaricaValoriLstOperatori
    
    Call frmInizialePuntoVendita.Show(vbModal)
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub Variabili_Init()
    nomePuntoVendita = ""
    indirizzo = ""
    descrizione = ""
    civico = ""
    cap = ""
    citta = ""
    telefono = ""
    fax = ""
    eMail = ""
    responsabile = ""
    provincia = ""
    codiceSAP = ""
    idTipoPuntoVendita = TPV_NON_SPECIFICATO
    idComuneSelezionato = idNessunElementoSelezionato
    comune = ""
End Sub

Public Sub SetModalit�Form(mf As AzioneEnum)
    modalit�FormCorrente = mf
End Sub

Public Sub SetIdPuntoVenditaSelezionato(id As Long)
    idPuntoVenditaSelezionato = id
End Sub

Private Sub AggiornaAbilitazioneControlli()

    Select Case modalit�FormCorrente
        Case A_NUOVO
            lblIntestazioneForm.Caption = "Inserimento nuovo Punto Vendita"
        Case A_MODIFICA
            lblIntestazioneForm.Caption = "Modifica Punto Vendita configurato"
        Case A_ELIMINA
            lblIntestazioneForm.Caption = "Eliminazione Punto Vendita configurato"
    End Select
    fraAzioni.Visible = (modalit�FormCorrente = A_MODIFICA)
    cmdTerminaliAvanzati.Enabled = (modalit�FormCorrente = A_MODIFICA And (idTipoPuntoVendita = TPV_SPECIALE))
    txtNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtIndirizzo.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCivico.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCap.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCitta.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtProvincia.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtTelefono.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtFax.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtEMail.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtResponsabile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    txtCodiceSAP.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    cmbTipoPuntoVendita.Enabled = (modalit�FormCorrente = A_NUOVO)
    lblNome.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblDescrizione.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblIndirizzo.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblCivico.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblCap.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblCitta.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblProvincia.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblTelefono.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblFax.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblEMail.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblResponsabile.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblCodiceSAP.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblSpiegazioneCodiceSAP.Enabled = (modalit�FormCorrente <> A_ELIMINA)
    lblTipoPuntoVendita.Enabled = (modalit�FormCorrente = A_NUOVO)
    cmdConferma.Enabled = Trim(txtNome.Text) <> "" And (idTipoPuntoVendita = TPV_SPECIALE Or idTipoPuntoVendita = TPV_RICEVITORIA Or idTipoPuntoVendita = TPV_FILIALE)
    
    txtTerminaleVirtuale.Enabled = (modalit�FormCorrente = A_MODIFICA)
    cmdTerminaleVirtuale.Enabled = (modalit�FormCorrente = A_MODIFICA)
    
End Sub
Private Sub cmbComune_Click()
    If Not internalEvent Then
        idComuneSelezionato = cmbComune.ItemData(cmbComune.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call frmSceltaPuntoVendita.SetExitCodeFormIniziale(EC_ANNULLA)
    Call Esci
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    sql = "SELECT PV.NOME, PV.DESCRIZIONE, INDIRIZZO, CIVICO, CAP, PROVINCIA, CITTA," & _
        " TELEFONO, FAX, EMAIL, RESPONSABILE, CODICESAP, IDTIPOPUNTOVENDITA," & _
        " C.IDCOMUNE, C.NOME || ' (' || P.SIGLA || ')' COMUNE" & _
        " FROM PUNTOVENDITA PV, COMUNE C, PROVINCIA P" & _
        " WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
        " AND PV.IDCOMUNE = C.IDCOMUNE(+)" & _
        " AND C.IDPROVINCIA = P.IDPROVINCIA(+)"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomePuntoVendita = rec("NOME")
        descrizione = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        indirizzo = IIf(IsNull(rec("INDIRIZZO")), "", rec("INDIRIZZO"))
        civico = IIf(IsNull(rec("CIVICO")), "", rec("CIVICO"))
        cap = IIf(IsNull(rec("CAP")), "", rec("CAP"))
        citta = IIf(IsNull(rec("CITTA")), "", rec("CITTA"))
        provincia = IIf(IsNull(rec("PROVINCIA")), "", rec("PROVINCIA"))
        telefono = IIf(IsNull(rec("TELEFONO")), "", rec("TELEFONO"))
        fax = IIf(IsNull(rec("FAX")), "", rec("FAX"))
        eMail = IIf(IsNull(rec("EMAIL")), "", rec("EMAIL"))
        responsabile = IIf(IsNull(rec("RESPONSABILE")), "", rec("RESPONSABILE"))
        codiceSAP = IIf(IsNull(rec("CODICESAP")), "", rec("CODICESAP"))
        idTipoPuntoVendita = rec("IDTIPOPUNTOVENDITA")
        idComuneSelezionato = IIf(IsNull(rec("IDCOMUNE")), idNessunElementoSelezionato, rec("IDCOMUNE"))
        comune = rec("COMUNE")
    End If

    rec.Close
    
    txtTerminaleVirtuale = ""
    sql = "SELECT TERM.CODICE" & _
        " FROM TERMINALE TERM" & _
        " WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
        " AND VIRTUALE = 1"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        txtTerminaleVirtuale = rec("CODICE")
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    txtNome.Text = ""
    txtIndirizzo.Text = ""
    txtDescrizione.Text = ""
    txtCivico.Text = ""
    txtCap.Text = ""
    txtCitta.Text = ""
    txtTelefono.Text = ""
    txtProvincia.Text = ""
    txtFax.Text = ""
    txtEMail.Text = ""
    txtResponsabile.Text = ""
    txtCodiceSAP.Text = ""
    
    txtNome.Text = nomePuntoVendita
    txtIndirizzo.Text = indirizzo
    txtDescrizione.Text = descrizione
    txtCivico.Text = civico
    txtProvincia.Text = provincia
    txtCap.Text = cap
    txtCitta.Text = citta
    txtTelefono.Text = telefono
    txtFax.Text = fax
    txtEMail.Text = eMail
    txtResponsabile.Text = responsabile
    txtCodiceSAP.Text = codiceSAP
    txtComune.Text = comune
    
    Call SelezionaElementoSuCombo(cmbTipoPuntoVendita, idTipoPuntoVendita)
    
    internalEvent = internalEventOld
    
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql As String
    Dim idNuovoPuntoVendita As Long
    Dim nomeSequenza As String
    Dim n As Long
    Dim organizzazione As clsElementoLista
    
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    idNuovoPuntoVendita = OttieniIdentificatoreDaSequenza("SQ_PUNTOVENDITA")
    
    sql = "INSERT INTO PUNTOVENDITA (IDPUNTOVENDITA, NOME, DESCRIZIONE, INDIRIZZO," & _
        " CIVICO, PROVINCIA, CAP, CITTA, TELEFONO, FAX, EMAIL, CODICESAP, RESPONSABILE," & _
        " IDTIPOPUNTOVENDITA, IDCOMUNE)" & _
        " VALUES (" & _
        idNuovoPuntoVendita & ", " & _
        SqlStringValue(nomePuntoVendita) & ", " & _
        SqlStringValue(descrizione) & ", " & _
        SqlStringValue(indirizzo) & ", " & _
        SqlStringValue(civico) & ", " & _
        SqlStringValue(provincia) & ", " & _
        SqlStringValue(cap) & ", " & _
        SqlStringValue(citta) & ", " & _
        SqlStringValue(telefono) & ", " & _
        SqlStringValue(fax) & ", " & _
        SqlStringValue(eMail) & ", " & _
        SqlStringValue(codiceSAP) & ", " & _
        SqlStringValue(responsabile) & ", " & _
        idTipoPuntoVendita & ", "
    If idComuneSelezionato = idNessunElementoSelezionato Then
        sql = sql & "NULL)"
    Else
        sql = sql & idComuneSelezionato & ")"
    End If
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Call frmSceltaPuntoVendita.SetIdRecordSelezionato(idNuovoPuntoVendita)
    Call SetIdPuntoVenditaSelezionato(idNuovoPuntoVendita)
    If idTipoPuntoVendita = TPV_RICEVITORIA Then
        Call CreaOperatoreRicevitoria(StringaConApiciRaddoppiati(nomePuntoVendita))
    End If
    If idTipoPuntoVendita = TPV_FILIALE Then
        Call CreaOperatoreFiliale(StringaConApiciRaddoppiati(nomePuntoVendita))
    End If
    Call AggiornaAbilitazioneControlli
    
    Exit Sub

gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Sub cmdElencaComuni_Click()
    Call PopolaComboComuni
End Sub

Private Sub PopolaComboComuni()
    Dim strSQL As String
    
    strSQL = "SELECT C.IDCOMUNE ID, C.NOME || ' (' || P.SIGLA || ')' NOME FROM COMUNE C, PROVINCIA P WHERE C.NOME LIKE '" & UCase(txtComune.Text) & "%' AND C.IDPROVINCIA = P.IDPROVINCIA"
    Call CaricaValoriCombo3(cmbComune, strSQL, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmdTerminaleVirtuale_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTerminaleVirtuale
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraTerminaleVirtuale()
    Dim sql As String
    Dim n As Long
    
    Call ApriConnessioneBD
    
    If txtTerminaleVirtuale = "" Then
        sql = "DELETE FROM TERMINALE" & _
            " WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
            " AND VIRTUALE = 1"
        SETAConnection.Execute sql, n, adCmdText
    Else
        sql = "UPDATE TERMINALE SET CODICE = '" & txtTerminaleVirtuale.Text & "', DESCRIZIONE = '" & txtTerminaleVirtuale.Text & "'" & _
            " WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
            " AND VIRTUALE = 1"
        SETAConnection.Execute sql, n, adCmdText
        
        If n = 0 Then
            sql = "INSERT INTO TERMINALE (IDTERMINALE, CODICE, ABILITATO, IDPUNTOVENDITA, IDTIPOTERMINALE, IDSOTTORETE, DESCRIZIONE, VIRTUALE, NUMEROMASSIMOSESSIONIPERMESSE)" & _
                " SELECT SQ_TERMINALE.NEXTVAL, '" & txtTerminaleVirtuale.Text & "', 1, " & idPuntoVenditaSelezionato & ", 1, IDSOTTORETE, '" & txtTerminaleVirtuale.Text & "', 1, NULL" & _
                " FROM SOTTORETE" & _
                " WHERE NOME = 'DMZ'"
            SETAConnection.Execute sql, n, adCmdText
            If n = 0 Then
                MsgBox "errore nell'inserimento del terminale virtuale"
            End If
        End If
    End If
    
    Call ChiudiConnessioneBD
End Sub

Private Sub cmdTerminaliAvanzati_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfiguraTerminaliAvanzatiOTotem
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfiguraTerminaliAvanzatiOTotem()
    Call CaricaFormConfigurazionePuntoVenditaTerminaliAvanzatiOTotem
End Sub

Private Sub CaricaFormConfigurazionePuntoVenditaTerminaliAvanzatiOTotem()
    Call frmConfigurazionePuntoVenditaTerminaleAvanzato.SetIdPuntoVenditaSelezionato(idPuntoVenditaSelezionato)
    Call frmConfigurazionePuntoVenditaTerminaleAvanzato.SetNomePuntoVenditaSelezionato(nomePuntoVendita)
    Call frmConfigurazionePuntoVenditaTerminaleAvanzato.Init
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim stringaNota As String

    stringaNota = "IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    
    Select Case modalit�FormCorrente
        Case A_NUOVO
            If valoriCampiOK Then
                Call InserisciNellaBaseDati
                stringaNota = "IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
                Call ScriviLog(CCTA_INSERIMENTO, CCDA_PUNTO_VENDITA, , stringaNota)
                Call frmMessaggio.Visualizza("NotificaNuovoInserimento")
                If (idTipoPuntoVendita = TPV_SPECIALE Or idTipoPuntoVendita = TPV_FILIALE) Then
                    Call SetModalit�Form(A_MODIFICA)
                    Call AggiornaAbilitazioneControlli
                Else
                    Call Esci
                End If
            End If
        Case A_MODIFICA
            If valoriCampiOK Then
                Call AggiornaNellaBaseDati
                Call ScriviLog(CCTA_MODIFICA, CCDA_PUNTO_VENDITA, , stringaNota)
                Call frmMessaggio.Visualizza("NotificaModificaDati")
                Call Esci
            End If
        Case A_ELIMINA
            Call EliminaDallaBaseDati
            Call ScriviLog(CCTA_CANCELLAZIONE, CCDA_PUNTO_VENDITA, , stringaNota)
            Call frmMessaggio.Visualizza("NotificaEliminazioneDati")
            Call Esci
    End Select
    Call frmSceltaPuntoVendita.SetExitCodeFormIniziale(EC_CONFERMA)
End Sub

Private Sub txtNome_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiornaNellaBaseDati()
    Dim sql As String
    Dim sql0 As String
    Dim sql1 As String
    Dim n As Long
    Dim i As Integer
    Dim organizzazione As clsElementoLista
    Dim condizioneSql As String

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

'   AGGIORNAMENTO TABELLA PUNTOVENDITA
    sql = "UPDATE PUNTOVENDITA SET" & _
        " NOME = " & SqlStringValue(nomePuntoVendita) & "," & _
        " DESCRIZIONE = " & SqlStringValue(descrizione) & "," & _
        " INDIRIZZO = " & SqlStringValue(indirizzo) & "," & _
        " CIVICO = " & SqlStringValue(civico) & "," & _
        " PROVINCIA = " & SqlStringValue(provincia) & "," & _
        " CAP = " & SqlStringValue(cap) & "," & _
        " CITTA = " & SqlStringValue(citta) & "," & _
        " TELEFONO = " & SqlStringValue(telefono) & "," & _
        " FAX = " & SqlStringValue(fax) & "," & _
        " EMAIL = " & SqlStringValue(eMail) & "," & _
        " RESPONSABILE = " & SqlStringValue(responsabile) & "," & _
        " CODICESAP = " & SqlStringValue(codiceSAP) & "," & _
        " IDTIPOPUNTOVENDITA = " & idTipoPuntoVendita & ","
    If idComuneSelezionato = idNessunElementoSelezionato Then
        sql = sql & " IDCOMUNE = NULL"
    Else
        sql = sql & " IDCOMUNE = " & idComuneSelezionato
    End If
    sql = sql & " WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub

Private Function valoriCampiOK() As Boolean
    Dim listaNonConformit� As Collection
    Dim condizioneSql As String

    valoriCampiOK = True
    Set listaNonConformit� = New Collection
    condizioneSql = ""
    If modalit�FormCorrente = A_MODIFICA Or modalit�FormCorrente = A_ELIMINA Then
        condizioneSql = "IDPUNTOVENDITA <> " & idPuntoVenditaSelezionato
    End If
    
    nomePuntoVendita = Trim(txtNome.Text)
    If ViolataUnicit�("PUNTOVENDITA", "NOME = " & SqlStringValue(nomePuntoVendita), "", _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformit�.Add("- il valore nome = " & SqlStringValue(nomePuntoVendita) & _
            " � gi� presente in DB;")
    End If
    descrizione = Trim(txtDescrizione.Text)
    indirizzo = Trim(txtIndirizzo.Text)
    civico = Trim(txtCivico.Text)
    provincia = Trim(txtProvincia.Text)
    cap = Trim(txtCap.Text)
    citta = Trim(txtCitta.Text)
    telefono = Trim(txtTelefono.Text)
    fax = Trim(txtFax.Text)
    eMail = Trim(txtEMail.Text)
    responsabile = Trim(txtResponsabile.Text)
    codiceSAP = Trim(txtCodiceSAP.Text)
    If ViolataUnicit�("PUNTOVENDITA", "CODICESAP = " & SqlStringValue(codiceSAP), "", _
        condizioneSql, "", "") Then
        valoriCampiOK = False
        Call listaNonConformit�.Add("- il valore codice SAP = " & SqlStringValue(codiceSAP) & _
            " � gi� presente in DB;")
    End If
    
    If listaNonConformit�.count > 0 Then
        Call frmMessaggio.Visualizza("ErroreNonConformit�Campi", ArgomentoMessaggio(listaNonConformit�))
    End If

End Function

Public Function NonAssociatoConOperazioni() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset

    Call ApriConnessioneBD
    
    sql = "SELECT IDOPERAZIONE FROM " & _
        " TERMINALE T, OPERAZIONE O WHERE" & _
        " T.IDTERMINALE = O.IDTERMINALE AND" & _
        " T.IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        NonAssociatoConOperazioni = False
    Else
        NonAssociatoConOperazioni = True
    End If
    
    Call ChiudiConnessioneBD
    
End Function

Private Sub EliminaDallaBaseDati()
    Dim sql As String
    Dim codice As String
    Dim idTerminale As Long
    Dim n As Long

    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori
    
'   ELIMINAZIONE RECORD ORGANIZZAZIONE_PUNTOVENDITA
    sql = "DELETE FROM ORGANIZZAZIONE_PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
'   ELIMINAZIONE RECORD TERMINALEAVANZATO
    sql = "DELETE FROM TERMINALE WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
'   ELIMINAZIONE RECORD OPERATORE
    If idTipoPuntoVendita = TPV_RICEVITORIA Then
        sql = "DELETE FROM OPERATORE WHERE USERNAME = " & SqlStringValue(nomePuntoVendita)
        SETAConnection.Execute sql, n, adCmdText
    End If
    
    sql = "DELETE FROM OPERATORE WHERE IDPUNTOVENDITAELETTIVO = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM ORGANIZ_CLASSEPV_PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    sql = "DELETE FROM CLASSESAPROD_PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText

'   ELIMINAZIONE RECORD PUNTOVENDITA
    sql = "DELETE FROM PUNTOVENDITA WHERE IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)

End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
    '        Else
    '            cmb.ListIndex = id
            End If
        Next i
    End If
    
End Sub

Private Sub cmbTipoPuntoVendita_Click()
    If Not internalEvent Then
        idTipoPuntoVendita = cmbTipoPuntoVendita.ItemData(cmbTipoPuntoVendita.ListIndex)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CreaOperatoreRicevitoria(userName As String)
    Dim sql As String
    Dim idNuovoOperatore As Long
    Const STATO_CASSA_CHIUSA As Integer = 2
    Dim n As Long
    
    Call ApriConnessioneBD

    idNuovoOperatore = OttieniIdentificatoreDaSequenza("SQ_OPERATORE")
    sql = "INSERT INTO OPERATORE (IDOPERATORE, USERNAME, PASSWORD, ABILITATO," & _
        " DATAORAULTIMAAPERTURACASSA, DATAORAULTIMACHIUSURACASSA," & _
        " IMPORTOAPERTURACASSA, IDTIPOSTATOCASSA, IDPERSONA, IDPUNTOVENDITA)" & _
        " VALUES (" & _
        idNuovoOperatore & ", " & _
        SqlStringValue(userName) & ", " & _
        SqlStringValue(userName) & ", " & _
        VB_VERO & ", " & _
        "NULL, " & _
        "NULL, " & _
        "NULL, " & _
        STATO_CASSA_CHIUSA & ", " & _
        "NULL, " & _
        idPuntoVenditaSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CreaOperatoreFiliale(userName As String)
    Dim sql As String
    Dim idNuovoOperatore As Long
    Dim idNuovoTerminale As Long
    Const STATO_CASSA_CHIUSA As Integer = 2
    Dim n As Long
    Dim organizzazione As clsElementoLista
    
    Call ApriConnessioneBD

    idNuovoOperatore = OttieniIdentificatoreDaSequenza("SQ_OPERATORE")
    sql = "INSERT INTO OPERATORE (IDOPERATORE, USERNAME, CRYPTO_HASH, ABILITATO," & _
        " DATAORAULTIMAAPERTURACASSA, DATAORAULTIMACHIUSURACASSA," & _
        " IMPORTOAPERTURACASSA, IDTIPOSTATOCASSA, IDPERSONA, IDPUNTOVENDITA)" & _
        " VALUES (" & _
        idNuovoOperatore & ", " & _
        SqlStringValue(userName) & ", " & _
        "SETA.GET_HASH(" & SqlStringValue(userName) & "), " & _
        VB_VERO & ", " & _
        "NULL, " & _
        "NULL, " & _
        "NULL, " & _
        "NULL, " & _
        "NULL, " & _
        idPuntoVenditaSelezionato & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    ' IDSOTTORETE: si lascia il default
    idNuovoTerminale = OttieniIdentificatoreDaSequenza("SQ_TERMINALE")
    sql = "INSERT INTO TERMINALE (IDTERMINALE, CODICE, ABILITATO, IDPUNTOVENDITA, " & _
        " IDTIPOTERMINALE)" & _
        " VALUES (" & _
        idNuovoTerminale & ", " & _
        SqlStringValue(userName) & ", " & _
        VB_VERO & ", " & _
        idPuntoVenditaSelezionato & ", " & _
        TT_TERMINALE_TOTEM & ")"
    SETAConnection.Execute sql, n, adCmdText
    
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub CaricaValoriLstOrganizzazioni()
    Dim sql As String
    Dim rec As New ADODB.Recordset
        
    Call ApriConnessioneBD
    sql = "SELECT O.NOME || ', ' || CPV.NOME ORG" & _
        " FROM ORGANIZZAZIONE O, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, CLASSEPUNTOVENDITA CPV" & _
        " WHERE OCP.IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
        " AND OCP.IDCLASSEPUNTOVENDITA = CPV.IDCLASSEPUNTOVENDITA" & _
        " AND OCP.IDORGANIZZAZIONE = O.IDORGANIZZAZIONE" & _
        " ORDER BY O.NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            lstOrganizzazioni.AddItem (rec("ORG"))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub CaricaValoriLstOperatori()
    Dim sql As String
    Dim rec As New ADODB.Recordset
        
    Call ApriConnessioneBD
    sql = "SELECT DECODE(ABILITATO, 1, USERNAME || ', ABILITATO', USERNAME || ', NON ABILITATO') OP" & _
        " FROM OPERATORE" & _
        " WHERE IDPUNTOVENDITAELETTIVO = " & idPuntoVenditaSelezionato & _
        " ORDER BY USERNAME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            lstOperatoriAfferenti.AddItem (rec("OP"))
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub


