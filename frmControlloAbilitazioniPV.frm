VERSION 5.00
Begin VB.Form frmControlloAbilitazioniPV 
   Caption         =   "Controllo abilitazioni punto vendita"
   ClientHeight    =   10050
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12405
   LinkTopic       =   "Form1"
   ScaleHeight     =   10050
   ScaleWidth      =   12405
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstOperazioniAbilitate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4470
      Left            =   120
      TabIndex        =   15
      Top             =   4800
      Width           =   12135
   End
   Begin VB.ListBox lstAbilitazioniAlProdotto 
      Height          =   1185
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   13
      Top             =   3120
      Width           =   12135
   End
   Begin VB.TextBox txtClassePuntoVendita 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   120
      TabIndex        =   10
      Top             =   2400
      Width           =   7755
   End
   Begin VB.TextBox txtPuntoVenditaElettivo 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1920
      MaxLength       =   30
      TabIndex        =   0
      Top             =   120
      Width           =   3315
   End
   Begin VB.ComboBox cmbPuntiVendita 
      Height          =   315
      Left            =   8040
      TabIndex        =   2
      Top             =   120
      Width           =   3015
   End
   Begin VB.CommandButton cmdCercaPuntiVendita 
      Caption         =   "Cerca punti vendita"
      Height          =   375
      Left            =   5280
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
   Begin VB.ComboBox cmbProdotto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   960
      Width           =   6225
   End
   Begin VB.ComboBox cmbOrganizzazione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   600
      Width           =   6225
   End
   Begin VB.CommandButton cmdChiudi 
      Caption         =   "Chiudi"
      Height          =   495
      Left            =   5160
      TabIndex        =   9
      Top             =   9480
      Width           =   2055
   End
   Begin VB.Label lblIntestazione 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   1680
      Width           =   12015
   End
   Begin VB.Label Label5 
      Caption         =   "Operazioni abilitate"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   4560
      Width           =   3015
   End
   Begin VB.Label Label4 
      Caption         =   "Abilitazioni al prodotto"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   2880
      Width           =   3015
   End
   Begin VB.Label Label1 
      Caption         =   "Classe punto vendita di appartenenza"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2160
      Width           =   3015
   End
   Begin VB.Label Label3 
      Caption         =   "Punto vendita"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "seleziona"
      Height          =   255
      Left            =   7080
      TabIndex        =   7
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lblProdotto 
      Caption         =   "Prodotti attivi e attivabili"
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   1695
   End
   Begin VB.Label lblOrganizzazione 
      Caption         =   "Organizzazioni attive"
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   1695
   End
End
Attribute VB_Name = "frmControlloAbilitazioniPV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private idOrganizzazioneSelezionata As Long
Private idProdottoSelezionato As Long
Private idPuntoVenditaSelezionato As Long
Private nomeProdottoSelezionato As String
Private nomePuntoVenditaSelezionato As String

Private internalEvent As Boolean

Public Sub Init()
    Call CaricaComboOrganizzazione
    Call Me.Show
End Sub

Private Sub CaricaComboOrganizzazione()
    Dim sqlOrg As String
    
    Call cmbOrganizzazione.Clear
    sqlOrg = "SELECT IDORGANIZZAZIONE ID, NOME FROM ORGANIZZAZIONE WHERE IDTIPOSTATOORGANIZZAZIONE = 3 ORDER BY NOME"
    Call CaricaValoriCombo3(cmbOrganizzazione, sqlOrg, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbOrganizzazione_Click()
    idOrganizzazioneSelezionata = cmbOrganizzazione.ItemData(cmbOrganizzazione.ListIndex)
    Call CaricaComboProdotto
End Sub

Private Sub CaricaComboProdotto()
    Dim sqlProd As String
    Dim idDaRilevare As Variant
    
    Call cmbProdotto.Clear
    sqlProd = "SELECT P.IDPRODOTTO ID, NOME || ' - ' || TO_CHAR(MIN(DATAORAINIZIO), 'DD-MM-YYYY HH24:MI:SS') AS NOME" & _
        " FROM PRODOTTO P, PRODOTTO_RAPPRESENTAZIONE PR, RAPPRESENTAZIONE R" & _
        " WHERE IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " AND IDTIPOSTATOPRODOTTO IN (" & TSP_ATTIVO & ", " & TSP_ATTIVABILE & ")" & _
        " AND P.IDPRODOTTO = PR.IDPRODOTTO" & _
        " AND PR.IDRAPPRESENTAZIONE = R.IDRAPPRESENTAZIONE" & _
        " GROUP BY P.IDPRODOTTO, NOME, IDPIANTA" & _
        " ORDER BY MIN(DATAORAINIZIO) DESC"
        
        Call CaricaValoriCombo3(cmbProdotto, sqlProd, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbProdotto_Click()
    idProdottoSelezionato = cmbProdotto.ItemData(cmbProdotto.ListIndex)
    nomeProdottoSelezionato = cmbProdotto
    Call VisualizzaDatiProdotto
End Sub

Private Sub VisualizzaDatiProdotto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Long
    
    If idProdottoSelezionato <> idNessunElementoSelezionato And idPuntoVenditaSelezionato <> idNessunElementoSelezionato Then
        lblIntestazione = "Punto vendita: " & nomePuntoVenditaSelezionato & " - Prodotto: " & nomeProdottoSelezionato
        Call ApriConnessioneBD
        
        sql = "SELECT CPV.DESCRIZIONE" & _
            " FROM ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, CLASSEPUNTOVENDITA CPV" & _
            " WHERE OCP.IDCLASSEPUNTOVENDITA = CPV.IDCLASSEPUNTOVENDITA" & _
            " AND IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND IDPUNTOVENDITA = " & idPuntoVenditaSelezionato
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            txtClassePuntoVendita = rec("DESCRIZIONE")
        End If
        rec.Close
        
        lstAbilitazioniAlProdotto.Clear
        i = 0
        sql = "SELECT CS.NOME, CP.IDPUNTOVENDITA" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP, CLASSESUPERAREA CS, CLASSESAPROD_PUNTOVENDITA CP" & _
            " WHERE CSP.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND CSP.IDCLASSESUPERAREA = CS.IDCLASSESUPERAREA" & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = CP.IDCLASSESUPERAREAPRODOTTO(+)" & _
            " AND CP.IDPUNTOVENDITA(+) = " & idPuntoVenditaSelezionato & _
            " ORDER BY CS.CLASSEPRINCIPALE DESC, CS.NOME"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                lstAbilitazioniAlProdotto.AddItem (rec("NOME"))
                If IsNull(rec("IDPUNTOVENDITA")) Then
                    lstAbilitazioniAlProdotto.Selected(i) = False
                Else
                    lstAbilitazioniAlProdotto.Selected(i) = True
                End If
                i = i + 1
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        lstOperazioniAbilitate.Clear
        sql = "SELECT RPAD(CS.NOME, 30, ' ') || '; ' ||" & _
            " RPAD(T.NOME, 30, ' ') || '; ' ||" & _
            " RPAD(DECODE(DATAORAINIZIOVALIDITA, TO_DATE('01/01/1970 0.00.01', 'DD/MM/YYYY HH24.MI.SS'), 'ILLIMITATO', TO_CHAR(DATAORAINIZIOVALIDITA, 'DD/MM/YYYY HH24.MI.SS')), 21, ' ') || '; ' ||" & _
            " RPAD(DECODE(DATAORAFINEVALIDITA, TO_DATE('31/12/2050 23.59.59', 'DD/MM/YYYY HH24.MI.SS'), 'ILLIMITATO', TO_CHAR(DATAORAFINEVALIDITA, 'DD/MM/YYYY HH24.MI.SS')), 21, ' ') S" & _
            " FROM CLASSESUPERAREAPRODOTTO CSP, PRODOTTO_CLASSEPV_TIPOOPERAZ PCT, ORGANIZ_CLASSEPV_PUNTOVENDITA OCP, CLASSESAPROD_PUNTOVENDITA CP, CLASSESUPERAREA CS, TIPOOPERAZIONE T" & _
            " WHERE CSP.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = PCT.IDCLASSESUPERAREAPRODOTTO" & _
            " AND PCT.IDCLASSEPUNTOVENDITA = OCP.IDCLASSEPUNTOVENDITA" & _
            " AND OCP.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OCP.IDPUNTOVENDITA = " & idPuntoVenditaSelezionato & _
            " AND CSP.IDCLASSESUPERAREAPRODOTTO = CP.IDCLASSESUPERAREAPRODOTTO" & _
            " AND OCP.IDPUNTOVENDITA = CP.IDPUNTOVENDITA" & _
            " AND CSP.IDCLASSESUPERAREA = CS.IDCLASSESUPERAREA" & _
            " AND PCT.IDTIPOOPERAZIONE = T.IDTIPOOPERAZIONE" & _
            " ORDER BY CS.CLASSEPRINCIPALE DESC, CS.NOME, T.NOME"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                lstOperazioniAbilitate.AddItem (rec("S"))
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ChiudiConnessioneBD
    End If
End Sub

Private Sub cmdCercaPuntiVendita_Click()
    Call CaricaValoriComboPuntiVendita
End Sub

Private Sub cmdChiudi_Click()
    Unload Me
End Sub

Private Sub CaricaValoriComboPuntiVendita()
    Dim sql As String
    
    sql = "SELECT IDPUNTOVENDITA ID, NOME" & _
        " FROM PUNTOVENDITA" & _
        " WHERE NOME LIKE '" & txtPuntoVenditaElettivo & "%'" & _
        " ORDER BY NOME"
    Call CaricaValoriCombo3(cmbPuntiVendita, sql, "NOME", numeroMassimoElementiInCombo)
End Sub

Private Sub cmbPuntiVendita_Click()
    If Not internalEvent Then
        idPuntoVenditaSelezionato = cmbPuntiVendita.ItemData(cmbPuntiVendita.ListIndex)
        nomePuntoVenditaSelezionato = cmbPuntiVendita
        VisualizzaDatiProdotto
    End If
End Sub

