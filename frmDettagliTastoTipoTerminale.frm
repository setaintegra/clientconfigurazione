VERSION 5.00
Begin VB.Form frmDettagliTastoTipoTerminale 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tasto Tipo Terminale"
   ClientHeight    =   1500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1500
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbTastoTipoTerminale 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   420
      Width           =   4275
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   2580
      TabIndex        =   1
      Top             =   960
      Width           =   1035
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "&Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   900
      TabIndex        =   0
      Top             =   960
      Width           =   1035
   End
   Begin VB.Label lblAssociazioneConTipoTerminale02 
      Caption         =   "xxx"
      Height          =   255
      Left            =   2580
      TabIndex        =   4
      Top             =   180
      Width           =   1815
   End
   Begin VB.Label lblAssociazioneConTipoTerminale01 
      Caption         =   "Associazione con TipoTerminale:"
      Height          =   255
      Left            =   180
      TabIndex        =   3
      Top             =   180
      Width           =   2355
   End
End
Attribute VB_Name = "frmDettagliTastoTipoTerminale"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idTipoTerminaleSelezionato As TipoTerminaleEnum
Private nomeTipoTerminaleSelezionato As String
Private idTariffaSelezionata As Long
Private idTastoTipoTerminaleSelezionato As Long
Private idProdottoSelezionato As Long
Private codiceTastoTipoTerminaleSelezionato As String
Private rateoProdottoSelezionato As Long

Private exitCode As ExitCodeEnum

Public Sub Init()
    Dim sql As String

    Call cmbTastoTipoTerminale.Clear
    idTastoTipoTerminaleSelezionato = idNessunElementoSelezionato
    If idTipoTerminaleSelezionato = TT_TERMINALE_LOTTO And rateoProdottoSelezionato = 1 Then 'prendere solo i primi 5 codici
        sql = "SELECT IDTASTOTIPOTERMINALE ID, CODICE COD" & _
            " FROM TASTOTIPOTERMINALE" & _
            " WHERE NOT EXISTS" & _
            " (SELECT IDTIPOTERMINALE" & _
            " FROM TARIFFA_TIPOTERMINALE T_T, TARIFFA T" & _
            " WHERE TASTOTIPOTERMINALE.IDTASTOTIPOTERMINALE = T_T.IDTASTOTIPOTERMINALE" & _
            " AND T.IDTARIFFA = T_T.IDTARIFFA" & _
            " AND T.IDPRODOTTO = " & idProdottoSelezionato & ")" & _
            " AND IDTASTOTIPOTERMINALE IN (1, 2, 3, 4, 5)" & _
            " AND IDTIPOTERMINALE = " & TT_TERMINALE_LOTTO & _
            " ORDER BY COD"
    Else
        sql = "SELECT IDTASTOTIPOTERMINALE ID, CODICE COD" & _
            " FROM TASTOTIPOTERMINALE" & _
            " WHERE NOT EXISTS" & _
            " (SELECT IDTIPOTERMINALE" & _
            " FROM TARIFFA_TIPOTERMINALE T_T, TARIFFA T" & _
            " WHERE TASTOTIPOTERMINALE.IDTASTOTIPOTERMINALE = T_T.IDTASTOTIPOTERMINALE" & _
            " AND T.IDTARIFFA = T_T.IDTARIFFA" & _
            " AND T.IDPRODOTTO = " & idProdottoSelezionato & ")" & _
            " AND IDTIPOTERMINALE = " & idTipoTerminaleSelezionato & _
            " ORDER BY COD"
    End If
    Call CaricaValoriCombo(cmbTastoTipoTerminale, sql, "COD")
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
    
End Sub

Private Sub AggiornaAbilitazioneControlli()
    lblAssociazioneConTipoTerminale02.Caption = Trim(nomeTipoTerminaleSelezionato)
    cmdConferma.Enabled = cmbTastoTipoTerminale.ListIndex <> idNessunElementoSelezionato
End Sub

Private Sub cmbTastoTipoTerminale_Click()
    Call cmbTastoTipoTerminale_Update
End Sub

Private Sub cmbTastoTipoTerminale_Update()
    idTastoTipoTerminaleSelezionato = cmbTastoTipoTerminale.ItemData(cmbTastoTipoTerminale.ListIndex)
    codiceTastoTipoTerminaleSelezionato = cmbTastoTipoTerminale.Text
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Call Annulla
End Sub

Private Sub Annulla()
    exitCode = EC_ANNULLA
    Unload Me
End Sub

Private Sub cmdConferma_Click()
    Call Conferma
End Sub

Private Sub Conferma()
    exitCode = EC_CONFERMA
    Unload Me
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo1 As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim descrizione As String
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            descrizione = rec(NomeCampo1)
            cmb.AddItem descrizione
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
        
End Sub

Public Function GetIdTastoTipoTerminaleSelezionato() As Long
    GetIdTastoTipoTerminaleSelezionato = idTastoTipoTerminaleSelezionato
End Function

Public Function GetCodiceTastoTipoTerminaleSelezionato() As String
    GetCodiceTastoTipoTerminaleSelezionato = codiceTastoTipoTerminaleSelezionato
End Function

Public Function GetExitCode() As ExitCodeEnum
    GetExitCode = exitCode
End Function

Public Sub SetIdTipoTerminaleSelezionato(idTT As TipoTerminaleEnum)
    idTipoTerminaleSelezionato = idTT
End Sub

Public Sub SetNomeTipoTerminaleSelezionato(nomeTT As String)
    nomeTipoTerminaleSelezionato = nomeTT
End Sub

Public Sub SetIdTariffaSelezionata(idT As Long)
    idTariffaSelezionata = idT
End Sub

Public Sub SetIdProdottoSelezionato(idP As Long)
    idProdottoSelezionato = idP
End Sub

Public Sub SetRateoProdottoSelezionato(rat As Long)
    rateoProdottoSelezionato = rat
End Sub




