VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdElimina 
      Caption         =   "    Elimina associazioni"
      Height          =   435
      Left            =   10680
      TabIndex        =   31
      Top             =   4080
      Width           =   1155
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   16
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   15
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   11
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstTariffe 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   4800
      Style           =   1  'Checkbox
      TabIndex        =   10
      Top             =   1020
      Width           =   4275
   End
   Begin VB.ListBox lstTipiSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   4800
      Style           =   1  'Checkbox
      TabIndex        =   9
      Top             =   2580
      Width           =   7035
   End
   Begin VB.CommandButton cmdAggiungi 
      Caption         =   "Aggiungi"
      Height          =   435
      Left            =   6900
      TabIndex        =   8
      Top             =   4080
      Width           =   1155
   End
   Begin VB.CommandButton cmdDeselezionaArea 
      Caption         =   " Deseleziona          area"
      Height          =   435
      Left            =   4800
      TabIndex        =   7
      Top             =   4080
      Width           =   1155
   End
   Begin VB.ListBox lstTipoTerminale 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   9240
      Style           =   1  'Checkbox
      TabIndex        =   6
      Top             =   1020
      Width           =   2595
   End
   Begin VB.ComboBox cmbTariffe 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":0000
      Left            =   8160
      List            =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   4980
      Width           =   3705
   End
   Begin VB.ComboBox cmbTipiTerminale 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":0004
      Left            =   8160
      List            =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   5580
      Width           =   3705
   End
   Begin VB.ComboBox cmbTipiSupporto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":0008
      Left            =   8160
      List            =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   6180
      Width           =   3705
   End
   Begin VB.ComboBox cmbLayoutSupporto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":000C
      Left            =   8160
      List            =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":000E
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   6780
      Width           =   3705
   End
   Begin VB.CommandButton cmdConfermaModifica 
      Caption         =   "Conferma"
      Height          =   315
      Left            =   8700
      TabIndex        =   1
      Top             =   7200
      Width           =   1155
   End
   Begin VB.CommandButton cmdAnnullaModifica 
      Caption         =   "Annulla"
      Height          =   315
      Left            =   10260
      TabIndex        =   0
      Top             =   7200
      Width           =   1155
   End
   Begin MSComctlLib.ImageList imlCompletezza 
      Left            =   3900
      Top             =   3840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":0010
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":016A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigurazioneProdottoAssociazioneTipiLayout2.frx":02C4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwAreePianta 
      Height          =   3915
      Left            =   120
      TabIndex        =   17
      Top             =   600
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   6906
      _Version        =   393217
      Indentation     =   706
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin MSDataGridLib.DataGrid dgrRiepilogo 
      Height          =   2535
      Left            =   120
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   4980
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   4471
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   30
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   29
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione a Tipi supporto e Layout supporto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   7395
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso: associazione a tutte le aree di"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   27
      Top             =   4680
      Width           =   4575
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   4800
      TabIndex        =   26
      Top             =   4680
      Width           =   2535
   End
   Begin VB.Label lblTariffeLista 
      Caption         =   "Tariffe del prodotto (terminali associati)"
      Height          =   195
      Left            =   4800
      TabIndex        =   25
      Top             =   780
      Width           =   2955
   End
   Begin VB.Label lblTipiSupportoLista 
      Caption         =   "Tipi supporto / Layout supporto"
      Height          =   195
      Left            =   4800
      TabIndex        =   24
      Top             =   2340
      Width           =   3255
   End
   Begin VB.Label lblTipoTerminaleLista 
      Caption         =   "Tipo terminale"
      Height          =   195
      Left            =   9240
      TabIndex        =   23
      Top             =   780
      Width           =   1875
   End
   Begin VB.Label lblTariffeCombo 
      Caption         =   "Tariffe dell'associazione prodotto/superarea"
      Height          =   195
      Left            =   8160
      TabIndex        =   22
      Top             =   4740
      Width           =   3135
   End
   Begin VB.Label lblTipoTerminaleCombo 
      Caption         =   "Tipo terminale"
      Height          =   195
      Left            =   8160
      TabIndex        =   21
      Top             =   5340
      Width           =   1695
   End
   Begin VB.Label lblTipiSupportoCombo 
      Caption         =   "Tipi supporto"
      Height          =   195
      Left            =   8160
      TabIndex        =   20
      Top             =   5940
      Width           =   1695
   End
   Begin VB.Label lblLayoutSupportoCombo 
      Caption         =   "Layout supporto"
      Height          =   195
      Left            =   8160
      TabIndex        =   19
      Top             =   6540
      Width           =   1695
   End
   Begin VB.Menu mnuAzioneSuRecord 
      Caption         =   "Azione su Record"
      Visible         =   0   'False
      Begin VB.Menu mnuModifica 
         Caption         =   "&Modifica"
      End
      Begin VB.Menu mnuElimina 
         Caption         =   "&Elimina"
      End
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idTipoSupportoSelezionato As Long
Private idTipoTerminaleSelezionato As Long
Private idLayoutSupportoSelezionato As Long
Private idTariffaSelezionata As Long
Private nomeTipoTerminaleSelezionato As String
Private nomeLayoutSupportoSelezionato As String
Private nomeTariffaSelezionata As String
Private nomeTipoSupportoSelezionato As String
Private nessunElementoSelezionato As Boolean
Private isProdottoAttivoSuTL As Boolean
Private utilizzoLayoutSupporto As clsUsoLayout
Private areaCommit As clsAssocArea
Private r As ADODB.Recordset
Private isCaratteristicaEreditata As ValoreBooleanoEnum
Private listaIdAreeGenitori As Collection
Private isElementoSuGrigliaSelezionato As Boolean
Private tipoTariffaLotto As String
Private idSuperareaServizio As Long
Private listaTariffe As Collection
Private listaTipiTerminale As Collection
Private listaTipiSupporto As Collection
Private listaLayoutSupporto As Collection
'Private tipoStatoRecord As TipoStatoRecordEnum

Private gestioneExitCode As ExitCodeEnum
Private tipoTerminale As TipoTerminaleEnum
Private tipoImmagine As ImmagineAssociazioneTreeViewEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    dgrRiepilogo.Caption = "RIEPILOGO DETTAGLIATO ASSOCIAZIONI"
        
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        tvwAreePianta.Enabled = True
        dgrRiepilogo.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        cmdAggiungi.Enabled = False
        cmdDeselezionaArea.Enabled = False
        cmdElimina.Enabled = False
        cmdConfermaModifica.Enabled = False
        cmdAnnullaModifica.Enabled = False
        lstTariffe.Enabled = False
        lstTipiSupporto.Enabled = False
        lstTipoTerminale.Enabled = False
        cmbTariffe.Enabled = False
        cmbTipiTerminale.Enabled = False
        cmbTipiSupporto.Enabled = False
        cmbLayoutSupporto.Enabled = False
        lblTariffeCombo.Enabled = False
        lblTipoTerminaleCombo.Enabled = False
        lblTipiSupportoCombo.Enabled = False
        lblLayoutSupportoCombo.Enabled = False
        lblTariffeLista.Enabled = False
        lblTipiSupportoLista.Enabled = False
        lblTipoTerminaleLista.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        tvwAreePianta.Enabled = False
        dgrRiepilogo.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        If areaCommit Is Nothing Then
            cmdAggiungi.Enabled = False
        Else
            cmdAggiungi.Enabled = (areaCommit.collTariffe.count > 0 And _
                areaCommit.collTipoTerm.count > 0 And areaCommit.collTipiSupporto.count > 0)
        End If
        cmdDeselezionaArea.Enabled = True
        cmdElimina.Enabled = True
        cmdConfermaModifica.Enabled = isElementoSuGrigliaSelezionato
        cmdAnnullaModifica.Enabled = isElementoSuGrigliaSelezionato
        lstTariffe.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        lstTipiSupporto.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        lstTipoTerminale.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmbTariffe.Enabled = isElementoSuGrigliaSelezionato
        cmbTipiTerminale.Enabled = isElementoSuGrigliaSelezionato
        cmbTipiSupporto.Enabled = isElementoSuGrigliaSelezionato
        cmbLayoutSupporto.Enabled = isElementoSuGrigliaSelezionato
        lblTariffeCombo.Enabled = isElementoSuGrigliaSelezionato
        lblTipoTerminaleCombo.Enabled = isElementoSuGrigliaSelezionato
        lblTipiSupportoCombo.Enabled = isElementoSuGrigliaSelezionato
        lblLayoutSupportoCombo.Enabled = isElementoSuGrigliaSelezionato
        lblTariffeLista.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        lblTipiSupportoLista.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        lblTipoTerminaleLista.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        If idRecordSelezionato = idTuttiGliElementiSelezionati Then
            lblOperazioneInCorso.Caption = "Operazione in corso: " & _
                "associazione a tutte le Aree di "
            lblOperazione.Caption = tvwAreePianta.Nodes(ChiaveId(idRecordSelezionato)).Text
        Else
            lblOperazioneInCorso.Caption = "Operazione in corso: " & _
                "associazione ad Area "
            lblOperazione.Caption = tvwAreePianta.Nodes(ChiaveId(idRecordSelezionato)).Text
        End If
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        tvwAreePianta.Enabled = True
        dgrRiepilogo.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        cmdAggiungi.Enabled = False
        cmdDeselezionaArea.Enabled = False
        cmdElimina.Enabled = False
        cmdConfermaModifica.Enabled = False
        cmdAnnullaModifica.Enabled = False
        lstTariffe.Enabled = False
        lstTipiSupporto.Enabled = False
        lstTipoTerminale.Enabled = False
        cmbTariffe.Enabled = False
        cmbTipiTerminale.Enabled = False
        cmbTipiSupporto.Enabled = False
        cmbLayoutSupporto.Enabled = False
        lblTariffeCombo.Enabled = False
        lblTipoTerminaleCombo.Enabled = False
        lblTipiSupportoCombo.Enabled = False
        lblLayoutSupportoCombo.Enabled = False
        lblTariffeLista.Enabled = False
        lblTipiSupportoLista.Enabled = False
        lblTipoTerminaleLista.Enabled = False
        
    Else
        'Do Nothing
    End If
        
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)
            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
    Call InserisciNellaBaseDati
    Call CaricaFormConfigurazioneChiaviProdotto
End Sub

Private Sub CaricaFormConfigurazioneChiaviProdotto()
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoChiaveProdotto.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoChiaveProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoChiaveProdotto.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoChiaveProdotto.Init
End Sub

Private Sub cmbTariffe_Click()
    If Not internalEvent Then
        idTariffaSelezionata = cmbTariffe.ItemData(cmbTariffe.ListIndex)
        nomeTariffaSelezionata = cmbTariffe.List(cmbTariffe.ListIndex)
    End If
End Sub

Private Sub cmbTipiTerminale_Click()
    If Not internalEvent Then
        idTipoTerminaleSelezionato = cmbTipiTerminale.ItemData(cmbTipiTerminale.ListIndex)
        nomeTipoTerminaleSelezionato = cmbTipiTerminale.List(cmbTipiTerminale.ListIndex)
    End If
End Sub

Private Sub cmbTipiSupporto_Click()
    If Not internalEvent Then
        idTipoSupportoSelezionato = cmbTipiSupporto.ItemData(cmbTipiSupporto.ListIndex)
        nomeTipoSupportoSelezionato = cmbTipiSupporto.List(cmbTipiSupporto.ListIndex)
    End If
End Sub

Private Sub cmbLayoutSupporto_Click()
    If Not internalEvent Then
        idLayoutSupportoSelezionato = cmbLayoutSupporto.ItemData(cmbLayoutSupporto.ListIndex)
        nomeLayoutSupportoSelezionato = cmbLayoutSupporto.List(cmbLayoutSupporto.ListIndex)
    End If
End Sub

Private Sub cmdAggiungi_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Aggiungi
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdDeselezionaArea_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call DeselezionaArea
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub DeselezionaArea()
    Dim causaNonEditabilita As String
    Dim nomeArea As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    nomeArea = tvwAreePianta.Nodes(ChiaveId(idRecordSelezionato)).Text
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call frmMessaggio.Visualizza("ConfermaSalvataggioAssociazioniArea", nomeArea)
    If frmMessaggio.exitCode = EC_CONFERMA Then
        DoEvents
        Set utilizzoLayoutSupporto = New clsUsoLayout
        If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
            isConfigurabile = True
            If tipoStatoProdotto = TSP_ATTIVO Then
                Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            End If
            If isConfigurabile Then
                Call InserisciNellaBaseDati
            End If
        End If
        Call utilizzoLayoutSupporto.inizializza(idProdottoSelezionato)
        Call AssociaIconaANodi
    End If
    Call EliminaStrutturaDatiCache
    Call PulisciControlliAssociazioni
    idRecordSelezionato = idNessunElementoSelezionato
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnullaModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call AnnullaModifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub AnnullaModifica()
    Dim internalEventOld As Boolean

    isElementoSuGrigliaSelezionato = False
    idTariffaSelezionata = idNessunElementoSelezionato
    idTipoTerminaleSelezionato = idNessunElementoSelezionato
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    idLayoutSupportoSelezionato = idNessunElementoSelezionato
    internalEventOld = internalEvent
    internalEvent = True
    Call SelezionaElementoSuCombo(cmbTariffe, idTariffaSelezionata)
    Call SelezionaElementoSuCombo(cmbTipiTerminale, idTipoTerminaleSelezionato)
    Call SelezionaElementoSuCombo(cmbTipiSupporto, idTipoSupportoSelezionato)
    Call SelezionaElementoSuCombo(cmbLayoutSupporto, idLayoutSupportoSelezionato)
    internalEvent = internalEventOld
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdConfermaModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call ConfermaModificaRecord
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfermaModificaRecord()

    Call SetGestioneExitCode(EC_CONFERMA)
    r.Update
    r("IDTARIFFA") = idTariffaSelezionata
    r("TARIFFA") = nomeTariffaSelezionata
    r("IDTIPOTERMINALE") = idTipoTerminaleSelezionato
    r("TIPOTERMINALE") = nomeTipoTerminaleSelezionato
    r("IDTIPOSUPPORTO") = idTipoSupportoSelezionato
    r("TIPOSUPPORTO") = nomeTipoSupportoSelezionato
    r("IDLAYOUTSUPPORTO") = idLayoutSupportoSelezionato
    r("LAYOUTSUPPORTO") = nomeLayoutSupportoSelezionato
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        If r("IDTIPOSTATORECORD") = idNessunElementoSelezionato Then
'            r("IDTIPOSTATORECORD") = TSR_MODIFICATO
'        End If
'    End If
    Call PulisciComboBox
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub Elimina()
    Dim causaNonEditabilita As String
    Dim nomeArea As String
    Dim isConfigurabile As Boolean
    
    nomeArea = tvwAreePianta.Nodes(ChiaveId(idRecordSelezionato)).Text
    causaNonEditabilita = ""
    Call frmMessaggio.Visualizza("ConfermaEliminazioneAssociazioni", nomeArea)
    If frmMessaggio.exitCode = EC_CONFERMA Then
        DoEvents
        If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
            isConfigurabile = True
            If tipoStatoProdotto = TSP_ATTIVO Then
                Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            End If
            If isConfigurabile Then
                Call EliminaAssociazioniInBD(idRecordSelezionato)
    '            Set areaCommit = New clsAssocArea
                Call adcRiepilogo_Init
                Call dgrRiepilogo_Init
            End If
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
                isConfigurabile = True
                If tipoStatoProdotto = TSP_ATTIVO Then
                    Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                End If
                If isConfigurabile Then
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI, stringaNota, idProdottoSelezionato)
                End If
            Else
                Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
            End If
'            Call Esci
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Public Sub Init()
    Dim sql As String

    Set utilizzoLayoutSupporto = New clsUsoLayout
    isElementoSuGrigliaSelezionato = False
    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call utilizzoLayoutSupporto.inizializza(idProdottoSelezionato)
    Call tvwAreePianta_Init
    Call adcRiepilogo_Init
    Call dgrRiepilogo_Init
    Call CaricaGerarchiaAree
    Call AssociaIconaANodi
    Call AggiornaAbilitazioneControlli
    nessunElementoSelezionato = False
    Call Me.Show(vbModal)

End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPrezzi.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoPrezzi.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("CODICE")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If idRecordSelezionato <> idTuttiGliElementiSelezionati Then
        If i <= 0 Then
            i = 1
        Else
            'Do Nothing
        End If
        cmb.AddItem "<nessuno>"
        cmb.ItemData(i - 1) = idNessunElementoSelezionato
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Private Sub CaricaGerarchiaAree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim level As Integer
    Dim idArea As Long
    Dim idPadre As Long
    Dim chiaveArea As String
    Dim chiavePadre As String
    Dim chiavePianta As String
    Dim nomeArea As String
    Dim t As TreeView
    Dim l As ImageList
    Dim nodo As Node
    Dim tipoSA As TipoAreaEnum
    
    Set t = tvwAreePianta
    Set l = imlCompletezza
    
    chiavePianta = ChiaveId(idTuttiGliElementiSelezionati)
    Set nodo = t.Nodes.Add(, , chiavePianta, "Pianta")
    t.Nodes(chiavePianta).Image = IAT_PIANTA
    Call ApriConnessioneBD
    
    sql = "SELECT LEVEL, IDAREA, NOME, CODICE, IDAREA_PADRE, IDTIPOAREA" & _
        " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & _
        " START WITH IDAREA_PADRE IS NULL" & _
        " CONNECT BY PRIOR IDAREA = IDAREA_PADRE" & _
        " ORDER BY LEVEL"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    DoEvents
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not (rec.EOF)
            level = rec("LEVEL").Value
            idArea = rec("IDAREA")
            nomeArea = rec("CODICE") & " - " & rec("NOME")
            tipoSA = rec("IDTIPOAREA")
            If tipoSA = TA_SUPERAREA_SERVIZIO Then
                idSuperareaServizio = idArea
            End If
            idPadre = IIf(IsNull(rec("IDAREA_PADRE")), idTuttiGliElementiSelezionati, rec("IDAREA_PADRE"))
            chiaveArea = ChiaveId(idArea)
            chiavePadre = ChiaveId(idPadre)
            If (idPadre = idTuttiGliElementiSelezionati) Then
                Set nodo = t.Nodes.Add(chiavePianta, tvwChild, chiaveArea, nomeArea)
            Else
                Set nodo = t.Nodes.Add(chiavePadre, tvwChild, chiaveArea, nomeArea)
            End If
            t.Nodes(chiaveArea).Image = IAT_MANCANTE
            Call nodo.EnsureVisible
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub tvwAreePianta_Init()
    Dim t As TreeView
    
    Set t = tvwAreePianta
    t.Style = tvwTreelinesPlusMinusPictureText
    t.ImageList = imlCompletezza
End Sub

Private Sub dgrRiepilogo_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call ImpostaAzioneSuDataGrid(Button, Shift, x, Y)
End Sub

Private Sub ImpostaAzioneSuDataGrid(Button As Integer, Shift As Integer, x As Single, Y As Single)
    If Button = vbRightButton Then
        If Not (r.EOF And r.BOF) Then
            If r("ISEREDITATA").Value = VB_FALSO Then
                Call PopupMenu(mnuAzioneSuRecord)
            End If
        End If
    End If
End Sub

Private Sub dgrRiepilogo_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call PulisciComboBox
    End If
End Sub

Private Sub mnuElimina_Click()
    Call EliminaRecord
End Sub

Private Sub EliminaRecord()
    Call r.Delete(adAffectCurrent)
End Sub

Private Sub mnuModifica_Click()
    Call ModificaRecord
End Sub

Private Sub ModificaRecord()

    GetListaIdRecordSelezionato
    isElementoSuGrigliaSelezionato = True
    Call AggiornaAbilitazioneControlli
    Call CaricaValoriComboDaLista(cmbTariffe, listaTariffe)
    Call CaricaValoriComboDaLista(cmbTipiTerminale, listaTipiTerminale)
    Call CaricaValoriComboDaLista(cmbTipiSupporto, listaTipiSupporto)
    Call CaricaValoriComboDaLista(cmbLayoutSupporto, listaLayoutSupporto)
    Call SelezionaElementoSuCombo(cmbTariffe, idTariffaSelezionata)
    Call SelezionaElementoSuCombo(cmbTipiTerminale, idTipoTerminaleSelezionato)
    Call SelezionaElementoSuCombo(cmbTipiSupporto, idTipoSupportoSelezionato)
    Call SelezionaElementoSuCombo(cmbLayoutSupporto, idLayoutSupportoSelezionato)
End Sub

Private Sub GetListaIdRecordSelezionato()
    If Not (r.BOF) Then
        If r.EOF Then
            r.MoveFirst
        End If
        isCaratteristicaEreditata = r("ISEREDITATA").Value
        idTariffaSelezionata = r("IDTARIFFA").Value
        idTipoTerminaleSelezionato = r("IDTIPOTERMINALE").Value
        idTipoSupportoSelezionato = r("IDTIPOSUPPORTO").Value
        idLayoutSupportoSelezionato = r("IDLAYOUTSUPPORTO").Value
    Else
        isCaratteristicaEreditata = VB_VERO
        idTariffaSelezionata = idNessunElementoSelezionato
        idTipoTerminaleSelezionato = idNessunElementoSelezionato
        idTipoSupportoSelezionato = idNessunElementoSelezionato
        idLayoutSupportoSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstTipiSupporto_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaTipoSupporto
    End If
End Sub


Private Sub lstTariffe_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaTariffa
    End If
End Sub

Private Sub lstTipoTerminale_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaTipoTerminale
    End If
End Sub

Private Function RilevaLayoutAssociatoATipo() As Boolean
    Call frmDettagliLayoutSupporto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmDettagliLayoutSupporto.Init(idTipoSupportoSelezionato)
    If frmDettagliLayoutSupporto.GetExitCode = EC_CONFERMA Then
        RilevaLayoutAssociatoATipo = True
    Else
        RilevaLayoutAssociatoATipo = False
    End If
End Function

Private Sub tvwAreePianta_Click()
    If Not internalEvent Then
        If Not nessunElementoSelezionato Then
            Call ElementoSelezionatoSuTreeView
        End If
    End If
    nessunElementoSelezionato = False
End Sub

Private Sub PulisciControlliAssociazioni()
    Call adcRiepilogo_Init
    Call dgrRiepilogo_Init
    Call lstTariffe_Init
    Call lstTipoTerminale_Init
    Call lstTipiSupporto_Init
End Sub

Private Sub PulisciListeAssociazioni()
    Call lstTariffe_Init
    Call lstTipoTerminale_Init
    Call lstTipiSupporto_Init
End Sub

Private Sub GetIdRecordSelezionato()
    idRecordSelezionato = IdChiave(tvwAreePianta.selectedItem.Key)
End Sub

Private Sub AggiornaTreeView()
    Dim t As TreeView
    
    Set t = tvwAreePianta
    t.Refresh
End Sub

Private Sub PulisciTreeView()
    Dim t As TreeView
    
    Set t = tvwAreePianta
    t.Nodes.Clear
End Sub

Private Sub tvwAreePianta_Collapse(ByVal Node As MSComctlLib.Node)
    nessunElementoSelezionato = True
End Sub

Private Sub tvwAreePianta_Expand(ByVal Node As MSComctlLib.Node)
    nessunElementoSelezionato = True
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Private Sub Aggiungi()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call AggiungiRecordsetARiepilogo(VB_FALSO)
    Call EliminaStrutturaDatiCache
    Set areaCommit = New clsAssocArea
    Call PulisciListeAssociazioni
    idTariffaSelezionata = idNessunElementoSelezionato
    idTipoTerminaleSelezionato = idNessunElementoSelezionato
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    idLayoutSupportoSelezionato = idNessunElementoSelezionato
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaStrutturaDatiCache()
    Set areaCommit = Nothing
End Sub

Private Function isAreaErede(idA As Long, idAreaOrigine As Long) As Boolean
    Dim t As TreeView
    Dim l As ImageList
    Dim nodo As Node
    Dim area As clsAssocArea
    Dim idAreaCache As Long
    Dim idAreaPadre As Long
    Dim trovato As Boolean
    
    Set t = tvwAreePianta
    Set l = imlCompletezza
    
    trovato = False
    For Each area In utilizzoLayoutSupporto.collAree
        idAreaCache = area.idArea
        Set nodo = t.Nodes(ChiaveId(idA)).Parent
        If nodo Is Nothing Then
            trovato = False
            isAreaErede = False
            Exit Function
        Else
            idAreaPadre = IdChiave(nodo.Key)
        End If
        If idAreaPadre = idAreaCache Then
            trovato = True
        End If
        While idAreaPadre <> idAreaCache And idAreaPadre <> idTuttiGliElementiSelezionati And Not trovato
            Set nodo = t.Nodes(ChiaveId(idAreaPadre)).Parent
            If Not nodo Is Nothing Then
                idAreaPadre = IdChiave(nodo.Key)
                If idAreaPadre = idAreaCache Then
                    trovato = True
                End If
            Else
                idAreaPadre = idTuttiGliElementiSelezionati
            End If
        Wend
    Next area
    If trovato Then
        idAreaOrigine = idAreaPadre
    End If
    isAreaErede = trovato
End Function

Private Sub adcRiepilogo_Init()
    Dim internalEventOld As Boolean
    Dim f As Fields
    
    internalEventOld = internalEvent
    internalEvent = True
            
    Set r = New ADODB.Recordset
    Set f = r.Fields
    
    Call f.Append("ISEREDITATA", adInteger, 4)
    Call f.Append("EREDITATA", adVarChar, 2)
    Call f.Append("IDTARIFFA", adInteger, 4)
    Call f.Append("TARIFFA", adVarChar, 80)
    Call f.Append("IDTIPOTERMINALE", adInteger, 4)
    Call f.Append("TIPOTERMINALE", adVarChar, 30)
    Call f.Append("IDTIPOSUPPORTO", adInteger, 4)
    Call f.Append("TIPOSUPPORTO", adVarChar, 40)
    Call f.Append("IDLAYOUTSUPPORTO", adInteger, 4)
    Call f.Append("LAYOUTSUPPORTO", adVarChar, 40)
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        Call f.Append("IDTIPOSTATORECORD", adInteger, 4)
'    End If
    Call r.Open
    Set dgrRiepilogo.dataSource = r
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrRiepilogo_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrRiepilogo
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 5
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Visible = False
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Visible = False
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
    g.Columns(8).Visible = False
    g.Columns(9).Width = (dimensioneGrid / numeroCampi)
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        g.Columns(10).Visible = False
'    End If
    
    g.MarqueeStyle = dbgHighlightRow
    
End Sub

Private Sub AggiungiRecordsetARiepilogo(isRecordsetEreditato As ValoreBooleanoEnum)
    Dim ta As clsAssocTariffa
    Dim tt As clsAssocTipoTerm
    Dim ts As clsAssocSupporto
    
    For Each ta In areaCommit.collTariffe
        For Each tt In areaCommit.collTipoTerm
            For Each ts In areaCommit.collTipiSupporto
                r.AddNew
                r("ISEREDITATA") = isRecordsetEreditato
                r("EREDITATA") = IIf(isRecordsetEreditato = VB_FALSO, "NO", "SI")
                r("IDTARIFFA") = ta.idTariffa
                r("TARIFFA") = ta.labelTariffa
                r("IDTIPOTERMINALE") = tt.idTipoTerm
                r("TIPOTERMINALE") = tt.labelTipoTerm
                r("IDTIPOSUPPORTO") = ts.idTipoSupporto
                r("TIPOSUPPORTO") = ts.labelTipoSupporto
                r("IDLAYOUTSUPPORTO") = ts.idLayoutSupporto
                r("LAYOUTSUPPORTO") = ts.labelLayoutSupporto
            Next ts
        Next tt
    Next ta
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    If Not (r.BOF And r.EOF) Then
        r.MoveFirst
        Do While Not r.EOF
            If id = r("IDTARIFFA") Then
                Exit Do
            End If
            r.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub CaricaCaratteristicheAreaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim listaId As String
    Dim isRecordsetEreditato As ValoreBooleanoEnum
    Dim internalEventOld As Boolean
    
    Call ApriConnessioneBD
    
    Call CreaListaIdAreeGenitori
    
    internalEventOld = internalEvent
    internalEvent = True
    listaId = CStr(idRecordSelezionato)
    For i = 1 To listaIdAreeGenitori.count
        listaId = listaId & ", " & listaIdAreeGenitori(i)
    Next i
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT LIVELLO, ULS.*," & _
'            " T.CODICE ||' - '|| T.NOME TLABEL, LS.CODICE ||' - '|| LS.NOME LSLABEL," & _
'            " TS.CODICE ||' - '|| TS.NOME TSLABEL, TT.NOME TTLABEL" & _
'            " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T, TIPOTERMINALE TT, LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, " & _
'            " (SELECT LEVEL AS LIVELLO, IDAREA, NOME, IDAREA_PADRE" & _
'            " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & " START WITH IDAREA_PADRE IS NULL" & _
'            " CONNECT BY PRIOR IDAREA = IDAREA_PADRE ORDER BY LEVEL) LIVELLI" & _
'            " WHERE LIVELLI.IDAREA = ULS.IDAREA AND ULS.IDAREA IN (" & listaId & ")" & _
'            " AND T.IDPRODOTTO = " & idProdottoSelezionato & " AND T.IDTARIFFA = ULS.IDTARIFFA AND" & _
'            " (ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO) AND" & _
'            " (ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO) AND" & _
'            " (ULS.IDTIPOTERMINALE = TT.IDTIPOTERMINALE) AND" & _
'            " LIVELLO >= (SELECT MAX(LIVELLO) FROM UTILIZZOLAYOUTSUPPORTO ULS2, TARIFFA T2," & _
'            " (SELECT LEVEL AS LIVELLO, IDAREA, NOME, IDAREA_PADRE FROM AREA" & _
'            " WHERE IDPIANTA = " & idPiantaSelezionata & " START WITH IDAREA_PADRE IS NULL" & _
'            " CONNECT BY PRIOR IDAREA = IDAREA_PADRE ORDER BY LEVEL) LIVELLI2" & _
'            " WHERE LIVELLI2.IDAREA = ULS2.IDAREA AND ULS.IDTARIFFA = ULS2.IDTARIFFA" & _
'            " AND ULS.IDTIPOSUPPORTO = ULS2.IDTIPOSUPPORTO AND ULS.IDTIPOTERMINALE = ULS2.IDTIPOTERMINALE" & _
'            " AND T2.IDPRODOTTO = " & idProdottoSelezionato & " AND ULS2.IDTARIFFA = T2.IDTARIFFA" & _
'            " AND ULS2.IDAREA IN (" & listaId & "))" & _
'            " AND (ULS.IDTIPOSTATORECORD IS NULL OR ULS.IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")" & _
'            " ORDER BY TLABEL, TTLABEL, TSLABEL"
'    Else
        sql = "SELECT LIVELLO, ULS.*, T.CODICE ||' - '|| T.NOME TLABEL, LS.CODICE ||' - '|| LS.NOME LSLABEL," & _
            " TS.CODICE ||' - '|| TS.NOME TSLABEL, TT.NOME TTLABEL" & _
            " FROM UTILIZZOLAYOUTSUPPORTO ULS, TARIFFA T, TIPOTERMINALE TT, LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, " & _
            " (SELECT LEVEL AS LIVELLO, IDAREA, NOME, IDAREA_PADRE" & _
            " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & " START WITH IDAREA_PADRE IS NULL" & _
            " CONNECT BY PRIOR IDAREA = IDAREA_PADRE ORDER BY LEVEL) LIVELLI" & _
            " WHERE LIVELLI.IDAREA = ULS.IDAREA AND ULS.IDAREA IN (" & listaId & ")" & _
            " AND T.IDPRODOTTO = " & idProdottoSelezionato & " AND T.IDTARIFFA = ULS.IDTARIFFA AND" & _
            " (ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO) AND" & _
            " (ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO) AND" & _
            " (ULS.IDTIPOTERMINALE = TT.IDTIPOTERMINALE) AND" & _
            " LIVELLO >= (SELECT MAX(LIVELLO) FROM UTILIZZOLAYOUTSUPPORTO ULS2, TARIFFA T2," & _
            " (SELECT LEVEL AS LIVELLO, IDAREA, NOME, IDAREA_PADRE FROM AREA" & _
            " WHERE IDPIANTA = " & idPiantaSelezionata & " START WITH IDAREA_PADRE IS NULL" & _
            " CONNECT BY PRIOR IDAREA = IDAREA_PADRE ORDER BY LEVEL) LIVELLI2" & _
            " WHERE LIVELLI2.IDAREA = ULS2.IDAREA AND ULS.IDTARIFFA = ULS2.IDTARIFFA" & _
            " AND ULS.IDTIPOSUPPORTO = ULS2.IDTIPOSUPPORTO AND ULS.IDTIPOTERMINALE = ULS2.IDTIPOTERMINALE" & _
            " AND T2.IDPRODOTTO = " & idProdottoSelezionato & " AND ULS2.IDTARIFFA = T2.IDTARIFFA" & _
            " AND ULS2.IDAREA IN (" & listaId & "))" & _
            " ORDER BY TLABEL, TTLABEL, TSLABEL"
'    End If
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    DoEvents
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            r.AddNew
            isRecordsetEreditato = IIf(rec("IDAREA").Value = idRecordSelezionato, VB_FALSO, VB_VERO)
            r("ISEREDITATA") = isRecordsetEreditato
            r("EREDITATA") = IIf(isRecordsetEreditato = VB_FALSO, "NO", "SI")
            r("IDTARIFFA") = rec("IDTARIFFA")
            r("TARIFFA") = rec("TLABEL")
            r("IDTIPOTERMINALE") = rec("IDTIPOTERMINALE")
            r("TIPOTERMINALE") = rec("TTLABEL")
            r("IDTIPOSUPPORTO") = rec("IDTIPOSUPPORTO")
            r("TIPOSUPPORTO") = rec("TSLABEL")
            r("IDLAYOUTSUPPORTO") = rec("IDLAYOUTSUPPORTO")
            r("LAYOUTSUPPORTO") = rec("LSLABEL")
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                If IsNull(rec("IDTIPOSTATORECORD")) Then
'                    r("IDTIPOSTATORECORD") = idNessunElementoSelezionato
'                Else
'                    r("IDTIPOSTATORECORD") = rec("IDTIPOSTATORECORD")
'                End If
'            End If
            rec.MoveNext
        Wend
    End If
    rec.Close
    internalEvent = internalEventOld
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CreaListaIdAreeGenitori()
    Dim idAreaOrigine As Long
    Dim idA As Long
    
    Set listaIdAreeGenitori = New Collection
    idA = idRecordSelezionato
    idAreaOrigine = idNessunElementoSelezionato
    While Not idAreaOrigine = idTuttiGliElementiSelezionati
        If isAreaErede(idA, idAreaOrigine) Then
            Call listaIdAreeGenitori.Add(idAreaOrigine)
            idA = idAreaOrigine
            idAreaOrigine = idNessunElementoSelezionato
        Else
            idAreaOrigine = idTuttiGliElementiSelezionati
        End If
    Wend
End Sub

Private Sub ElementoSelezionatoSuTreeView()
    Dim sql As String
    Dim newArea As New clsAssocArea
    Dim idAreaOrigine As Long
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call GetIdRecordSelezionato
    'LA CONDIZIONE idRecordSelezionato <> idTuttiGliElementiSelezionati
    'E' STATA AGGIUNTA RECENTEMENTE; SE SI VOLESSE TORNARE ALLA
    'ASSOCIAZIONE A TUTTA LA PIANTA BISOGNA TOGLIERLA E RIPRISTINARE
    'LA INSERISCINELLABASEDATI_OLD
    If idRecordSelezionato <> idTuttiGliElementiSelezionati Then
        Call SetGestioneRecordGriglia(ASG_MODIFICA)
        Call SetGestioneExitCode(EC_NON_SPECIFICATO)
        Call AggiornaAbilitazioneControlli
        Call PulisciControlliAssociazioni
        Call CaricaValoriLstTariffe
        Call CaricaValoriLstTipoTerminale
        Call CaricaValoriLstTipiSupporto
        Call CaricaValoriLayoutSupporto
        idTariffaSelezionata = idNessunElementoSelezionato
        idTipoTerminaleSelezionato = idNessunElementoSelezionato
        idTipoSupportoSelezionato = idNessunElementoSelezionato
        idLayoutSupportoSelezionato = idNessunElementoSelezionato
        Call CaricaCaratteristicheAreaDallaBaseDati
        Call SelezionaElementoSuGriglia(idTariffaSelezionata)
        Set areaCommit = New clsAssocArea
    End If
    
    MousePointer = mousePointerOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AssociaIconaANodi()
    Dim t As TreeView
    Dim nodo As Node
    Dim l As ImageList
    Dim areaDB As clsAssocArea
    
    Set t = tvwAreePianta
    Set l = imlCompletezza
    
    For Each areaDB In utilizzoLayoutSupporto.collAree
        t.Nodes(ChiaveId(areaDB.idArea)).Image = IAT_PRESENTE
    Next areaDB
        
End Sub

Private Sub PulisciComboBox()
    Call cmbTariffe.Clear
    Call cmbTipiTerminale.Clear
    Call cmbTipiSupporto.Clear
    Call cmbLayoutSupporto.Clear
End Sub

Private Sub CreaListaSuperareeFiglieDiPrimiLivello(idA As Long, listaId As Collection)
    Dim t As TreeView
    Dim nodoPadre As Node
    Dim nodoFiglio As Node
    Dim chiaveArea As String
    Dim i As Integer
    Dim chiaveSuperareaServizio As String
    
    chiaveSuperareaServizio = ChiaveId(idSuperareaServizio)
    chiaveArea = ChiaveId(idA)
    Set t = tvwAreePianta
    Set nodoPadre = t.Nodes(chiaveArea)
    Set nodoFiglio = nodoPadre.Child
    If nodoFiglio Is Nothing Then
        If nodoPadre.Key = chiaveSuperareaServizio Then
            Call listaId.Add(IdChiave(nodoPadre.Key))
        Else
            Call listaId.Add(IdChiave(nodoPadre.Parent.Key))
        End If
    Else
        If (nodoFiglio.Children = 0 And nodoFiglio.Key <> chiaveSuperareaServizio) Then
            Call listaId.Add(IdChiave(nodoPadre.Key))
        Else
            For i = 1 To nodoPadre.Children
                Call CreaListaSuperareeFiglieDiPrimiLivello(IdChiave(nodoFiglio.Key), listaId)
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    End If
End Sub

Private Sub InserisciNellaBaseDati()
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim sql2 As String
    Dim rec2 As New ADODB.Recordset
    Dim i As Integer
    Dim idTar As Long
    Dim idTer As TipoTerminaleEnum
    Dim idSup As Long
    Dim idLay As Long
    Dim isEreditata As ValoreBooleanoEnum
    Dim idSA As Long
    Dim internalEventOld As Boolean
    Dim idA As Long
    Dim n As Long
    Dim condizioniSQL As String
    Dim idTariffaOmaggio As Long
    
    Call ApriConnessioneBD
    
    SETAConnection.BeginTrans
    
'   A: ELIMINAZIONE DI TUTTE LE ASSOCIAZIONI PRECEDENTI
    sql1 = "SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSelezionato
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.EOF And rec1.BOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idTar = rec1("IDTARIFFA")
            sql2 = "DELETE FROM UTILIZZOLAYOUTSUPPORTO ULS WHERE" & _
                " ULS.IDTARIFFA = " & idTar & " AND ULS.IDAREA = " & idRecordSelezionato
            SETAConnection.Execute sql2, n, adCmdText
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
'   B: INSERIMENTO DELLE ASSOCIAZIONI CONFIGURATE
    internalEventOld = internalEvent
    internalEvent = True
    If Not (r.EOF And r.BOF) Then
    r.MoveFirst
        While Not r.EOF
            isEreditata = r("ISEREDITATA").Value
            idTar = r("IDTARIFFA").Value
            idTer = r("IDTIPOTERMINALE").Value
            idSup = r("IDTIPOSUPPORTO").Value
            idLay = r("IDLAYOUTSUPPORTO").Value
            If isEreditata = VB_FALSO Then
                If IsTipoTerminaleAssociabileATariffa(idTar, idTer) Then
                    sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTO (" & _
                        "IDAREA, IDTARIFFA, IDTIPOTERMINALE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO) " & _
                        "VALUES (" & _
                        idRecordSelezionato & ", " & _
                        idTar & ", " & _
                        idTer & ", " & _
                        idSup & ", " & _
                        idLay & ")"
                    On Error Resume Next
                    SETAConnection.Execute sql1, n, adCmdText
                    If Err.Number <> 0 Then Err.Clear
                    
                    idTariffaOmaggio = IdTariffaOmaggioAssociata(idTar)
                    If idTariffaOmaggio <> idNessunElementoSelezionato Then
                        sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTO (" & _
                            "IDAREA, IDTARIFFA, IDTIPOTERMINALE, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO) " & _
                            "VALUES (" & _
                            idRecordSelezionato & ", " & _
                            idTariffaOmaggio & ", " & _
                            idTer & ", " & _
                            idSup & ", " & _
                            idLay & ")"
                        On Error Resume Next
                        SETAConnection.Execute sql1, n, adCmdText
                    End If
                End If
            End If
            r.MoveNext
        Wend
    End If
    internalEvent = internalEventOld
    
    SETAConnection.CommitTrans
    Call ChiudiConnessioneBD
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Function IsTipoTerminaleAssociabileATariffa(idTariffa As Long, idTipoTerminale As Long) As Boolean
    Dim tariffaCorrente As clsElementoLista
    Dim terminaleCorrente As clsElementoLista
    Dim i As Integer
    Dim trovato As Boolean
    
    trovato = False
    Set tariffaCorrente = listaTariffe.Item(ChiaveId(idTariffa))
    For Each terminaleCorrente In tariffaCorrente.listaSottoElementiLista
        If idTipoTerminale = terminaleCorrente.idElementoLista Then
            trovato = True
        End If
    Next terminaleCorrente
    IsTipoTerminaleAssociabileATariffa = trovato
End Function

Private Sub CaricaValoriLstTariffe()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim nome As String
    Dim codice As String
    Dim idTariffa As Long
    Dim chiaveTariffa As String
    Dim listaSuperareeFiglieDiPrimiLivello As Collection
    Dim i As Integer
    Dim terminaliAttivi As String
    Dim codTTA As String
    Dim idTTA As TipoTerminaleEnum
    Dim tariffaCorrente As clsElementoLista
    Dim tipoTerminaleCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSuperareeFiglieDiPrimiLivello = New Collection
    Call CreaListaSuperareeFiglieDiPrimiLivello(idRecordSelezionato, listaSuperareeFiglieDiPrimiLivello)
    
    Set listaTariffe = New Collection
    
    For i = 1 To listaSuperareeFiglieDiPrimiLivello.count
        sql = "SELECT T.NOME, T.CODICE, T.IDTARIFFA" & _
            " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP WHERE" & _
            " T.IDTARIFFA = PTP.IDTARIFFA AND" & _
            " T.IDTARIFFA_RIFERIMENTO IS NULL AND" & _
            " T.IDPRODOTTO = " & idProdottoSelezionato & " AND" & _
            " PTP.IDAREA = " & listaSuperareeFiglieDiPrimiLivello(i) & _
            " ORDER BY CODICE"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set tariffaCorrente = New clsElementoLista
                Call tariffaCorrente.InizializzaListaSottoElementiLista
                tariffaCorrente.nomeElementoLista = rec("NOME")
                tariffaCorrente.codiceElementoLista = rec("CODICE")
                tariffaCorrente.idElementoLista = rec("IDTARIFFA").Value
                terminaliAttivi = ""
                sql1 = "SELECT IDTIPOTERMINALE" & _
                    " FROM TARIFFA_TIPOTERMINALE" & _
                    " WHERE IDTARIFFA = " & tariffaCorrente.idElementoLista
                rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
                If Not (rec1.EOF And rec1.BOF) Then
                    rec1.MoveFirst
                    While Not rec1.EOF
                        Set tipoTerminaleCorrente = New clsElementoLista
                        idTTA = rec1("IDTIPOTERMINALE").Value
                        Select Case idTTA
                            Case TT_TERMINALE_AVANZATO
                                codTTA = "TA"
                            Case TT_TERMINALE_LOTTO
                                codTTA = "TL"
                            Case TT_TERMINALE_WEB
                                codTTA = "TW"
                            Case TT_TERMINALE_POS
                                codTTA = "TPOS"
                            Case TT_TERMINALE_TOTEM
                                codTTA = "TTOTEM"
                        End Select
                        tipoTerminaleCorrente.idElementoLista = idTTA
                        tipoTerminaleCorrente.codiceElementoLista = codTTA
                        Call tariffaCorrente.listaSottoElementiLista.Add(tipoTerminaleCorrente)
                        rec1.MoveNext
                        terminaliAttivi = IIf(terminaliAttivi = "", codTTA, terminaliAttivi & "; " & codTTA)
                    Wend
                End If
                rec1.Close
                If terminaliAttivi <> "" Then
                    terminaliAttivi = "(" & terminaliAttivi & ")"
                    tariffaCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME") & terminaliAttivi
                End If
                If Not TariffaGiaPresenteInLista(tariffaCorrente.idElementoLista) Then
                    chiaveTariffa = ChiaveId(tariffaCorrente.idElementoLista)
                    Call listaTariffe.Add(tariffaCorrente, chiaveTariffa)
                End If
                rec.MoveNext
            Wend
        End If
        rec.Close
    Next i
    
    Call ChiudiConnessioneBD
    
    Call lstTariffe_Init
    
End Sub

Private Sub lstTariffe_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tar As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTariffe.Clear

    If Not (listaTariffe Is Nothing) Then
        i = 1
        For Each tar In listaTariffe
            lstTariffe.AddItem tar.descrizioneElementoLista
            lstTariffe.ItemData(i - 1) = tar.idElementoLista
            i = i + 1
        Next tar
    End If
           
    internalEvent = internalEventOld

End Sub

Private Function TariffaGiaPresenteInLista(idT As Long) As Boolean
    Dim trovato As Boolean
    Dim tar As clsElementoLista
    
    trovato = False
    For Each tar In listaTariffe
        If tar.idElementoLista = idT Then
            trovato = True
        End If
    Next tar
    TariffaGiaPresenteInLista = trovato
End Function

Private Sub SelezionaDeselezionaTariffa()
    Dim newTariffa As clsAssocTariffa
    Dim tariffaSelezionata As clsElementoLista
    
    idTariffaSelezionata = lstTariffe.ItemData(lstTariffe.ListIndex)
    Set tariffaSelezionata = listaTariffe.Item(ChiaveId(idTariffaSelezionata))
    If lstTariffe.Selected(lstTariffe.ListIndex) Then
        Set newTariffa = New clsAssocTariffa
        newTariffa.idTariffa = idTariffaSelezionata
        newTariffa.labelTariffa = tariffaSelezionata.descrizioneElementoLista
        Call areaCommit.AggiungiTariffaAdArea(newTariffa)
    Else
        Call areaCommit.OttieniTariffaDaIdTariffa(idTariffaSelezionata, newTariffa)
        Call areaCommit.EliminaTariffaDaArea(idTariffaSelezionata)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriLstTipoTerminale()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoTerminale As String
    Dim tipoTerminaleCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTipiTerminale = New Collection
    
    sql = "SELECT IDTIPOTERMINALE, NOME FROM TIPOTERMINALE" & _
        " WHERE IDTIPOTERMINALE > 0 AND IDTIPOTERMINALE <> " & TT_TERMINALE_CONTROLLO_ACCESSI & _
        " ORDER BY NOME"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoTerminaleCorrente = New clsElementoLista
            tipoTerminaleCorrente.nomeElementoLista = rec("NOME")
            tipoTerminaleCorrente.descrizioneElementoLista = rec("NOME")
            tipoTerminaleCorrente.idElementoLista = rec("IDTIPOTERMINALE").Value
            chiaveTipoTerminale = ChiaveId(tipoTerminaleCorrente.idElementoLista)
            Call listaTipiTerminale.Add(tipoTerminaleCorrente, chiaveTipoTerminale)
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    Call ChiudiConnessioneBD
    
    Call lstTipoTerminale_Init
    
End Sub

Private Sub lstTipoTerminale_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim ter As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTipoTerminale.Clear

    If Not (listaTipiTerminale Is Nothing) Then
        i = 1
        For Each ter In listaTipiTerminale
            lstTipoTerminale.AddItem ter.descrizioneElementoLista
            lstTipoTerminale.ItemData(i - 1) = ter.idElementoLista
            i = i + 1
        Next ter
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SelezionaDeselezionaTipoTerminale()
    Dim newTipoTerminale As clsAssocTipoTerm
    Dim TipoTerminaleSelezionato As clsElementoLista
    
    idTipoTerminaleSelezionato = lstTipoTerminale.ItemData(lstTipoTerminale.ListIndex)
    Set TipoTerminaleSelezionato = listaTipiTerminale.Item(ChiaveId(idTipoTerminaleSelezionato))
    If lstTipoTerminale.Selected(lstTipoTerminale.ListIndex) Then
        Set newTipoTerminale = New clsAssocTipoTerm
        newTipoTerminale.idTipoTerm = idTipoTerminaleSelezionato
        newTipoTerminale.labelTipoTerm = TipoTerminaleSelezionato.nomeElementoLista
        Call areaCommit.AggiungiTipoTerminaleAdArea(newTipoTerminale)
    Else
        Call areaCommit.OttieniTipoTerminaleDaIdTipoTerminale(idTipoTerminaleSelezionato, newTipoTerminale)
        Call areaCommit.EliminaTipoTerminaleDaArea(idTipoTerminaleSelezionato)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriLstTipiSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim tipoSupportoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTipiSupporto = New Collection
    
    sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.NOME, TS.CODICE" & _
        " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
        " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
        " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoSupportoCorrente = New clsElementoLista
            tipoSupportoCorrente.nomeElementoLista = rec("NOME")
            tipoSupportoCorrente.codiceElementoLista = rec("CODICE")
            tipoSupportoCorrente.idElementoLista = rec("IDTIPOSUPPORTO").Value
            tipoSupportoCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
            chiaveTipoSupporto = ChiaveId(tipoSupportoCorrente.idElementoLista)
            Call listaTipiSupporto.Add(tipoSupportoCorrente, chiaveTipoSupporto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstTipiSupporto_Init
    
End Sub

Private Sub lstTipiSupporto_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTipiSupporto.Clear

    If Not (listaTipiSupporto Is Nothing) Then
        i = 1
        For Each tip In listaTipiSupporto
            lstTipiSupporto.AddItem tip.descrizioneElementoLista
            lstTipiSupporto.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SelezionaDeselezionaTipoSupporto()
    Dim newTipoSupporto As clsAssocSupporto
    Dim tipoSupportoSelezionato As clsElementoLista
    
    idTipoSupportoSelezionato = lstTipiSupporto.ItemData(lstTipiSupporto.ListIndex)
    Set tipoSupportoSelezionato = listaTipiSupporto.Item(ChiaveId(idTipoSupportoSelezionato))
    If lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) Then
        If RilevaLayoutAssociatoATipo = True Then
            Set newTipoSupporto = New clsAssocSupporto
            newTipoSupporto.idTipoSupporto = idTipoSupportoSelezionato
            newTipoSupporto.labelTipoSupporto = tipoSupportoSelezionato.descrizioneElementoLista
            newTipoSupporto.idLayoutSupporto = frmDettagliLayoutSupporto.GetIdLayoutSupportoSelezionato
            newTipoSupporto.labelLayoutSupporto = frmDettagliLayoutSupporto.GetLabelLayoutSupportoSelezionato
            Call areaCommit.AggiungiTipoSupportoAdArea(newTipoSupporto)
'            Call AggiungiLabelLayoutATipoSupporto
            lstTipiSupporto.List(lstTipiSupporto.ListIndex) = newTipoSupporto.labelTipoSupporto & " / " & newTipoSupporto.labelLayoutSupporto
'            lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) = True
        Else
            lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) = False
        End If
    Else
'        Call RimuoviLabelLayoutATipoSupporto
        Call areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipoSupportoSelezionato, newTipoSupporto)
        lstTipiSupporto.List(lstTipiSupporto.ListIndex) = newTipoSupporto.labelTipoSupporto
'        lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) = False
'        Call areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipoSupportoSelezionato, newTipoSupporto)
        Call areaCommit.EliminaTipoSupportoDaArea(idTipoSupportoSelezionato)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

'Private Sub AggiungiLabelLayoutATipoSupporto() 'da usare quando si conferma il layout da combo
'    Dim i As Integer
'    Dim idTipo As Long
'    Dim tipoSupportoCorrente As clsAssocSupporto
'    Dim t As clsElementoLista
'
'    i = 1
'    For Each t In listaTipiSupporto
'        idTipo = t.idElementoLista
'        If areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipo, tipoSupportoCorrente) Then
'            Call lstTipiSupporto.RemoveItem(i - 1)
'            Call lstTipiSupporto.AddItem(tipoSupportoCorrente.labelTipoSupporto & " / " & tipoSupportoCorrente.labelLayoutSupporto, i - 1)
'            lstTipiSupporto.ItemData(i - 1) = tipoSupportoCorrente.idTipoSupporto
'            lstTipiSupporto.Selected(i - 1) = True
''            i = i + 1
'        End If
'        i = i + 1
'    Next t
'End Sub

'Private Sub RimuoviLabelLayoutATipoSupporto() 'da usare quando si deseleziona un tiposupporto
'    Dim i As Integer
'    Dim idTipo As Long
'    Dim tipoSupportoCorrente As clsAssocSupporto
'    Dim t As clsElementoLista
'
'    i = 1
'    For Each t In listaTipiSupporto
'        idTipo = t.idElementoLista
'        If areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipo, tipoSupportoCorrente) Then
'            Call lstTipiSupporto.RemoveItem(i - 1)
'            Call lstTipiSupporto.AddItem(tipoSupportoCorrente.labelTipoSupporto, i - 1)
'            lstTipiSupporto.ItemData(i - 1) = tipoSupportoCorrente.idTipoSupporto
'            lstTipiSupporto.Selected(i - 1) = False
''            i = i + 1
'        End If
'        i = i + 1
'    Next t
'End Sub

Private Sub CaricaValoriLayoutSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveLayoutSupporto As String
    Dim layoutSupportoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaLayoutSupporto = New Collection
    
    sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.NOME, LS.CODICE " & _
        " FROM LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
        " WHERE LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO" & _
        " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
        " ORDER BY CODICE"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set layoutSupportoCorrente = New clsElementoLista
            layoutSupportoCorrente.nomeElementoLista = rec("NOME")
            layoutSupportoCorrente.codiceElementoLista = rec("CODICE")
            layoutSupportoCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO").Value
            layoutSupportoCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
            chiaveLayoutSupporto = ChiaveId(layoutSupportoCorrente.idElementoLista)
            Call listaLayoutSupporto.Add(layoutSupportoCorrente, chiaveLayoutSupporto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub CaricaValoriComboDaLista(cmb As ComboBox, listaElementi As Collection)
    Dim i As Integer
    Dim e As clsElementoLista
    
    i = 1
    For Each e In listaElementi
        cmb.AddItem e.descrizioneElementoLista
        cmb.ItemData(i - 1) = e.idElementoLista
        i = i + 1
    Next e
            
End Sub

Private Sub EliminaAssociazioniInBD(idA As Long)
    Dim t As TreeView
    Dim nodoCorrente As Node
    Dim nodoFiglio As Node
    Dim chiaveArea As String
    Dim i As Integer
    Dim sql As String
    Dim elencoIdTariffe As String
    Dim tar As clsElementoLista
    Dim n As Long
    Dim condizioniSQL As String
    
    elencoIdTariffe = ""
    For Each tar In listaTariffe
        elencoIdTariffe = IIf(elencoIdTariffe = "", tar.idElementoLista, elencoIdTariffe & ", " & tar.idElementoLista)
    Next tar
    chiaveArea = ChiaveId(idA)
    Set t = tvwAreePianta
    Set nodoCorrente = t.Nodes(chiaveArea)
    Set nodoFiglio = nodoCorrente.Child
    If listaTariffe.count > 0 Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''            condizioniSql = " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
''                " AND IDTIPOSTATORECORD <> " & TSR_NUOVO
''            Call AggiornaParametriSessioneSuRecord("UTILIZZOLAYOUTSUPPORTO", "IDAREA", idA, TSR_ELIMINATO, condizioniSql)
''            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTO WHERE IDAREA = " & idA & _
''                " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
''                " AND IDTIPOSTATORECORD <> " & TSR_NUOVO
''            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTO WHERE IDAREA = " & idA & _
'                " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
'                " AND IDTIPOSTATORECORD = " & TSR_NUOVO
'            SETAConnection.Execute sql, n, adCmdText
'            condizioniSQL = " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
'                " AND IDTIPOSTATORECORD <> " & TSR_NUOVO
'            Call AggiornaParametriSessioneSuRecord("UTILIZZOLAYOUTSUPPORTO", "IDAREA", idA, TSR_ELIMINATO, condizioniSQL)
'        Else
            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTO WHERE IDAREA = " & idA & _
                " AND IDTARIFFA IN (" & elencoIdTariffe & ")"
            SETAConnection.Execute sql, n, adCmdText
'       End If
        nodoCorrente.Image = IAT_MANCANTE
        DoEvents
        If Not nodoFiglio Is Nothing Then
            For i = 1 To nodoCorrente.Children
                Call EliminaAssociazioniInBD(IdChiave(nodoFiglio.Key))
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    End If
End Sub







