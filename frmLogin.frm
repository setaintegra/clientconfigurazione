VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Login"
   ClientHeight    =   2820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4245
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2820
   ScaleWidth      =   4245
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdUltimoAccesso 
      Caption         =   "Ultimo accesso"
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   2400
      Width           =   3735
   End
   Begin VB.TextBox txtNomeSchema 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1620
      TabIndex        =   3
      Text            =   "SETA_INTEGRA"
      Top             =   1380
      Width           =   2415
   End
   Begin VB.TextBox txtUserId 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1620
      TabIndex        =   0
      Text            =   "SETA_CC"
      Top             =   120
      Width           =   2415
   End
   Begin VB.TextBox txtDataSource 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1620
      TabIndex        =   2
      Text            =   "DBT1LIS"
      Top             =   960
      Width           =   2415
   End
   Begin VB.CommandButton cmdAbbandona 
      Caption         =   "Annulla"
      Height          =   375
      Left            =   2880
      TabIndex        =   5
      Top             =   1920
      Width           =   1095
   End
   Begin VB.CommandButton cmdConnetti 
      Caption         =   "Connetti"
      Default         =   -1  'True
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   1920
      Width           =   1095
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1620
      PasswordChar    =   "*"
      TabIndex        =   1
      Text            =   "T1LISSETA_CC"
      Top             =   540
      Width           =   2415
   End
   Begin VB.Label lblNomeSchema 
      Alignment       =   1  'Right Justify
      Caption         =   "Nome Schema"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   1395
   End
   Begin VB.Label lblDataSource 
      Alignment       =   1  'Right Justify
      Caption         =   "Data Source"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1020
      Width           =   1395
   End
   Begin VB.Label lblPassword 
      Alignment       =   1  'Right Justify
      Caption         =   "Password"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   600
      Width           =   1395
   End
   Begin VB.Label lblUserID 
      Alignment       =   1  'Right Justify
      Caption         =   "User ID"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   180
      Width           =   1395
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAbbandona_Click()
    Unload Me
    Call frmPrincipale.Esci
End Sub

Private Sub cmdConnetti_Click()
    Call Connetti(False)
End Sub

Private Sub Connetti(ultimoAccesso As Boolean)
    Dim mousePointerOld As Integer
    Dim stringaNota As String
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass

    If Not ultimoAccesso Then
        Call RilevaValoriCampi
    End If
    tipoModalitąConfigurazione = TMC_DIRETTA
    stringaNota = "USERID = " & userID
    
    Call ApriConnessioneBD
    If numeroConnessioniVirtualmenteAperte = 0 Then
        'Do Nothing
    Else
        SaveSetting App.EXEName, "Credenziali", "userID", userID
        SaveSetting App.EXEName, "Credenziali", "passWord", passWord
        SaveSetting App.EXEName, "Credenziali", "dataSource", dataSource
        SaveSetting App.EXEName, "Credenziali", "nomeSchema", nomeSchema
    
        Call ScriviLog(CCTA_LOGIN, CCDA_NON_SPECIFICATO, , stringaNota)
        Unload Me
        
'        If UCase(getNomeMacchina()) = "SCARFINA" Then
'            Call frmSaluti.Show(vbModal)
'        End If

'        If esistonoProdottiTDLDaPrendereInCarico Then
'            Call frmPresaInCaricoProdottiTDL.Init
'        End If
        
        Call frmPrincipale.Init
    End If

    MousePointer = mousePointerOld
End Sub

Private Sub AggiornaAbilitazioneControlli()
    cmdConnetti.Enabled = (txtUserId.Text <> "" And _
                           txtDataSource.Text <> "" And _
                           txtNomeSchema.Text <> "")
End Sub

Public Sub Init()
    Call RilevaValoriUltimoAccesso
    cmdUltimoAccesso.Caption = "Ultimo accesso: " & userID & " - " & dataSource

    Call AggiornaAbilitazioneControlli
End Sub

Private Sub RilevaValoriCampi()
    userID = Trim(txtUserId.Text)
    passWord = Trim(txtPassWord.Text)
    dataSource = Trim(txtDataSource.Text)
    nomeSchema = Trim(txtNomeSchema.Text) & "."
End Sub

Private Sub RilevaValoriUltimoAccesso()
    userID = GetSetting(App.EXEName, "Credenziali", "userID", "")
    passWord = GetSetting(App.EXEName, "Credenziali", "passWord", "")
    dataSource = GetSetting(App.EXEName, "Credenziali", "dataSource", "")
    nomeSchema = GetSetting(App.EXEName, "Credenziali", "nomeSchema", "")
End Sub

Private Sub cmdUltimoAccesso_Click()
    Call Connetti(True)
End Sub

Private Sub txtDataSource_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtNomeSchema_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtPassWord_Change()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtUserID_Change()
    Call AggiornaAbilitazioneControlli
    Call ValorizzaNomeSchemaAutomaticamente
End Sub

Private Sub ValorizzaNomeSchemaAutomaticamente()
    txtNomeSchema.Text = Left$(txtUserId.Text, 30)
End Sub

Public Function esistonoProdottiTDLDaPrendereInCarico() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim numeroProdottiTDLDaPrendereInCarico As Long

    esistonoProdottiTDLDaPrendereInCarico = False

    sql = "SELECT COUNT(*) CONT" & _
            " FROM TDLPRODOTTO T, PRODOTTO P" & _
            " WHERE T.IDPRODOTTO = P.IDPRODOTTO" & _
            " AND P.IDTIPOSTATOPRODOTTO = 1" & _
            " AND DATAORAPRESAINCARICO IS NULL"
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    numeroProdottiTDLDaPrendereInCarico = rec("CONT")
    rec.Close

    If numeroProdottiTDLDaPrendereInCarico > 0 Then
        esistonoProdottiTDLDaPrendereInCarico = True
    End If
End Function

