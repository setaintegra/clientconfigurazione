VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmLogAttività 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Log Attività"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraImpostaFiltri 
      Caption         =   "Imposta filtri "
      Height          =   1695
      Left            =   120
      TabIndex        =   14
      Top             =   180
      Width           =   11715
      Begin VB.CommandButton cmdVisualizza 
         Caption         =   "Visualizza"
         Default         =   -1  'True
         Height          =   315
         Left            =   180
         TabIndex        =   11
         Top             =   1200
         Width           =   1935
      End
      Begin VB.ComboBox cmbAttività 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7440
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1200
         Width           =   3585
      End
      Begin VB.ComboBox cmbDominio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7440
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   540
         Width           =   3585
      End
      Begin VB.Frame fraIntervallo 
         Caption         =   "Intervallo "
         Height          =   1275
         Left            =   2220
         TabIndex        =   16
         Top             =   240
         Width           =   5055
         Begin VB.CheckBox chkIntervalloDefault 
            Caption         =   "Intervallo default (giorno corrente)"
            Height          =   195
            Left            =   470
            TabIndex        =   2
            Top             =   240
            Width           =   3375
         End
         Begin VB.TextBox txtOraTermine 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   3840
            MaxLength       =   2
            TabIndex        =   7
            Top             =   840
            Width           =   435
         End
         Begin VB.TextBox txtMinutiTermine 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   8
            Top             =   840
            Width           =   435
         End
         Begin VB.TextBox txtOraInizio 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   3840
            MaxLength       =   2
            TabIndex        =   4
            Top             =   480
            Width           =   435
         End
         Begin VB.TextBox txtMinutiInizio 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   5
            Top             =   480
            Width           =   435
         End
         Begin MSComCtl2.DTPicker dtpDataInizio 
            Height          =   315
            Left            =   420
            TabIndex        =   3
            Top             =   480
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   20250625
            CurrentDate     =   37684.7534259259
         End
         Begin MSComCtl2.DTPicker dtpDataTermine 
            Height          =   315
            Left            =   420
            TabIndex        =   6
            Top             =   840
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   20250625
            CurrentDate     =   37684.7534259259
         End
         Begin VB.Label lblDataTermine 
            Alignment       =   1  'Right Justify
            Caption         =   "a"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   900
            Width           =   195
         End
         Begin VB.Label lblSeparatoreOreMinutiTermine 
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4320
            TabIndex        =   21
            Top             =   840
            Width           =   75
         End
         Begin VB.Label lblOraTermine 
            Alignment       =   1  'Right Justify
            Caption         =   "ora (HH:MM)"
            Height          =   255
            Left            =   2820
            TabIndex        =   20
            Top             =   900
            Width           =   915
         End
         Begin VB.Label lblDataInizio 
            Alignment       =   1  'Right Justify
            Caption         =   "da"
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   540
            Width           =   195
         End
         Begin VB.Label lblSeparatoreOreMinutiInizio 
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4320
            TabIndex        =   18
            Top             =   480
            Width           =   75
         End
         Begin VB.Label lblOraInizio 
            Alignment       =   1  'Right Justify
            Caption         =   "ora (HH:MM)"
            Height          =   255
            Left            =   2820
            TabIndex        =   17
            Top             =   540
            Width           =   915
         End
      End
      Begin VB.Frame fraUtenti 
         Caption         =   "Utenti "
         Height          =   915
         Left            =   180
         TabIndex        =   15
         Top             =   240
         Width           =   1935
         Begin VB.OptionButton optTuttiUtenti 
            Caption         =   "tutti"
            Height          =   195
            Left            =   120
            TabIndex        =   1
            Top             =   600
            Width           =   1755
         End
         Begin VB.OptionButton optUtenteCorrente 
            Caption         =   "solo utente corrente"
            Height          =   195
            Left            =   120
            TabIndex        =   0
            Top             =   300
            Width           =   1755
         End
      End
      Begin VB.Label lblAttività 
         Caption         =   "Attività"
         Height          =   195
         Left            =   7440
         TabIndex        =   24
         Top             =   960
         Width           =   1755
      End
      Begin VB.Label lblDominio 
         Caption         =   "Dominio"
         Height          =   195
         Left            =   7440
         TabIndex        =   23
         Top             =   300
         Width           =   1755
      End
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "Esci"
      Height          =   435
      Left            =   10620
      TabIndex        =   12
      Top             =   8100
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adcLogAttività 
      Height          =   375
      Left            =   240
      Top             =   7260
      Visible         =   0   'False
      Width           =   2355
      _ExtentX        =   4154
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adcLogAttività"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrLogAttività 
      Height          =   5835
      Left            =   120
      TabIndex        =   13
      Top             =   1980
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   10292
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblNota 
      Caption         =   "NOTA: le date/ore sono relative alla data/ora di sistema, non a quella della macchina corrente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   195
      Left            =   120
      TabIndex        =   25
      Top             =   7920
      Width           =   10395
   End
End
Attribute VB_Name = "frmLogAttività"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private dataOraInizio As Date
Private dataOraTermine As Date
Private idAttività As Long
Private idDominio As Long
Private isIntervalloDefault As ValoreBooleanoEnum
Private internalEvent As Boolean

Private tipoFiltroUtenti As CCFiltroUtentiEnum

Public Sub Init()
    Dim sqlDominio As String
    Dim sqlAttività As String
    
    Call Variabili_Init
    sqlDominio = "SELECT IDDOMINIOATTIVITA ID, NOME FROM CC_DOMINIOATTIVITA" & _
        " WHERE IDDOMINIOATTIVITA > 0 ORDER BY NOME"
    sqlAttività = "SELECT IDTIPOATTIVITA ID, NOME FROM CC_TIPOATTIVITA" & _
        " WHERE IDTIPOATTIVITA > 0 ORDER BY NOME"
    Call CaricaValoriCombo(cmbDominio, sqlDominio, "NOME")
    Call CaricaValoriCombo(cmbAttività, sqlAttività, "NOME")
    Call AssegnaValoriCampi
'    Call dtpDataInizio_Init
'    Call dtpDataTermine_Init
    Call adcLogAttività_Init
    Call dgrLogAttività_Init
    Call Me.Show(vbModal)
End Sub

Private Function RilevaValoriCampi() As Boolean
    Dim dataI As Date
    Dim oraI As Integer
    Dim minutiI As Integer
    Dim dataF As Date
    Dim oraF As Integer
    Dim minutiF As Integer

    RilevaValoriCampi = True

    If IsNull(dtpDataInizio.Value) Then
        dataOraInizio = dataNulla
    Else
        dataI = FormatDateTime(dtpDataInizio.Value, vbShortDate)
        If txtOraInizio.Text <> "" Then
            If IsCampoOraCorretto(txtOraInizio) Then
                oraI = CInt(Trim(txtOraInizio.Text))
            Else
                Call frmMessaggio.Visualizza("ErroreFormatoOra", "Ora iniziale")
                RilevaValoriCampi = False
                Exit Function
            End If
        End If
        If txtMinutiInizio.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiInizio) Then
                minutiI = CInt(Trim(txtMinutiInizio.Text))
            Else
                Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Minuti iniziale")
                RilevaValoriCampi = False
                Exit Function
            End If
        End If
        dataOraInizio = FormatDateTime(dataI & " " & oraI & ":" & minutiI, vbGeneralDate)
    End If

    If IsNull(dtpDataTermine.Value) Then
        dataOraTermine = FormatDateTime(Now, vbGeneralDate)
    Else
        dataF = FormatDateTime(dtpDataTermine.Value, vbShortDate)
        If txtOraTermine.Text <> "" Then
            If IsCampoOraCorretto(txtOraTermine) Then
                oraF = CInt(Trim(txtOraTermine.Text))
            Else
                Call frmMessaggio.Visualizza("ErroreFormatoOra", "Ora finale")
                RilevaValoriCampi = False
                Exit Function
            End If
        End If
        If txtMinutiTermine.Text <> "" Then
            If IsCampoMinutiCorretto(txtMinutiTermine) Then
                minutiF = CInt(Trim(txtMinutiTermine.Text))
            Else
                Call frmMessaggio.Visualizza("ErroreFormatoMinuti", "Minuti finale")
                RilevaValoriCampi = False
                Exit Function
            End If
        End If
        dataOraTermine = FormatDateTime(dataF & " " & oraF & ":" & minutiF, vbGeneralDate)
    End If
    
    If optUtenteCorrente.Value = True Then
        tipoFiltroUtenti = CCFU_UTENTE_CORRENTE
    Else
        tipoFiltroUtenti = CCFU_TUTTI
    End If
    
    isIntervalloDefault = chkIntervalloDefault.Value
    
End Function

Private Sub AggiornaAbilitazioneControlli()
    chkIntervalloDefault.Value = isIntervalloDefault
    If isIntervalloDefault Then
        dtpDataInizio.Value = Null
        dtpDataTermine.Value = Null
        txtOraInizio.Text = ""
        txtMinutiInizio.Text = ""
        txtOraTermine.Text = ""
        txtMinutiTermine.Text = ""
    End If
    If IsNull(dtpDataInizio.Value) And IsNull(dtpDataTermine.Value) Then
        cmdVisualizza.Enabled = (isIntervalloDefault = VB_VERO)
    ElseIf Not IsNull(dtpDataInizio.Value) And Not IsNull(dtpDataTermine.Value) Then
'        cmdVisualizza.Enabled = (dataOraTermine > dataOraInizio)
        cmdVisualizza.Enabled = True
    Else
        cmdVisualizza.Enabled = False
    End If
End Sub

Private Sub adcLogAttività_Init()
    Dim internalEventOld As Boolean
    Dim filtroUtenti As String
    Dim filtroAttività As String
    Dim filtroDominio As String
    Dim filtroIntervallo As String
    Dim dataOraInizialeDefault As Date
    Dim dataInizialeDefault As Date
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True

    Set d = adcLogAttività
    
    filtroUtenti = IIf(tipoFiltroUtenti = CCFU_UTENTE_CORRENTE, " AND L.UTENTE = '" & userID & "'", "")
    filtroDominio = IIf(idDominio = idTuttiGliElementiSelezionati, "", " AND L.IDDOMINIOATTIVITA = " & idDominio)
    filtroAttività = IIf(idAttività = idTuttiGliElementiSelezionati, "", " AND L.IDTIPOATTIVITA = " & idAttività)
    If isIntervalloDefault = VB_VERO Then
        dataInizialeDefault = FormatDateTime(Now, vbShortDate)
        dataOraInizialeDefault = FormatDateTime(dataInizialeDefault & " 00:01")
        filtroIntervallo = " AND DATAORA BETWEEN " & SqlDateTimeValue(dataOraInizialeDefault) & _
            " AND SYSDATE"
    Else
        filtroIntervallo = " AND DATAORA BETWEEN " & SqlDateTimeValue(dataOraInizio) & _
            " AND " & SqlDateTimeValue(dataOraTermine)
    End If
    sql = "SELECT L.DATAORA, L.UTENTE, TA.NOME ATTIVITA, DA1.NOME DOMINIO, DA2.NOME SUBDOMINIO, L.NOTE " & _
       " FROM CC_LOG L," & _
       " CC_TIPOATTIVITA TA," & _
       " CC_DOMINIOATTIVITA DA1," & _
       " CC_DOMINIOATTIVITA DA2" & _
       " WHERE L.IDTIPOATTIVITA = TA.IDTIPOATTIVITA" & _
       " AND L.IDDOMINIOATTIVITA = DA1.IDDOMINIOATTIVITA" & _
       " AND L.IDSOTTODOMINIOATTIVITA = DA2.IDDOMINIOATTIVITA" & _
       filtroUtenti & filtroAttività & filtroDominio & filtroIntervallo & _
       " ORDER BY DATAORA DESC"
                
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrLogAttività.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrLogAttività_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrLogAttività
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 6
    g.Columns(0).Width = (dimensioneGrid / numeroCampi)
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Width = (dimensioneGrid / numeroCampi)
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Public Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call cmb.Clear
    Call ApriConnessioneBD

    sql = strSQL
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("NOME")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If i <= 0 Then
        i = 1
    Else
        'Do Nothing
    End If
    cmb.AddItem "<tutti>"
    cmb.ItemData(i - 1) = idTuttiGliElementiSelezionati
'    cmb.AddItem "<nessuno>"
'    cmb.ItemData(i) = idNessunElementoSelezionato
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    For i = 1 To cmb.ListCount
        If id = cmb.ItemData(i - 1) Then
            cmb.ListIndex = i - 1
        End If
    Next i
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    chkIntervalloDefault.Value = isIntervalloDefault
    If (isIntervalloDefault = VB_VERO) Then
        dtpDataInizio.Value = Null
        dtpDataTermine.Value = Null
        txtOraInizio.Text = ""
        txtMinutiInizio.Text = ""
        txtOraTermine.Text = ""
        txtMinutiTermine.Text = ""
    Else
        dtpDataInizio.Value = dataOraInizio
        txtOraInizio.Text = StringaOraMinuti(CStr(Hour(dataOraInizio)))
        txtMinutiInizio.Text = StringaOraMinuti(CStr(Minute(dataOraInizio)))
        dtpDataTermine.Value = dataOraTermine
        txtOraTermine.Text = StringaOraMinuti(CStr(Hour(dataOraTermine)))
        txtMinutiTermine.Text = StringaOraMinuti(CStr(Minute(dataOraTermine)))
    End If
    If tipoFiltroUtenti = CCFU_UTENTE_CORRENTE Then
        optUtenteCorrente.Value = True
    Else
        optTuttiUtenti.Value = True
    End If
    Call SelezionaElementoSuCombo(cmbDominio, idDominio)
    Call SelezionaElementoSuCombo(cmbAttività, idAttività)
    
    internalEvent = internalEventOld
End Sub

Private Sub Variabili_Init()
    isIntervalloDefault = VB_VERO
    tipoFiltroUtenti = CCFU_TUTTI
    idDominio = idTuttiGliElementiSelezionati
    idAttività = idTuttiGliElementiSelezionati
    dataOraInizio = Now
    dataOraTermine = Now
End Sub

Private Sub chkIntervalloDefault_Click()
    isIntervalloDefault = chkIntervalloDefault.Value
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmbDominio_Click()
    idDominio = cmbDominio.ItemData(cmbDominio.ListIndex)
End Sub

Private Sub cmbAttività_Click()
    idAttività = cmbAttività.ItemData(cmbAttività.ListIndex)
End Sub

Private Sub cmdEsci_Click()
    Call Esci
End Sub

Private Sub Esci()
    Unload Me
End Sub

Private Sub cmdVisualizza_Click()
    Call Visualizza
End Sub

Private Sub Visualizza()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If RilevaValoriCampi Then
        Call adcLogAttività_Init
        Call dgrLogAttività_Init
'        Call a.Refresh
'        Call dgrLogAttività.Refresh
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub dtpDataTermine_Init()
    dtpDataTermine.MinDate = dataNulla
    dtpDataTermine.Enabled = False
End Sub

Private Sub dtpDataTermine_CHANGE()
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraTermine_Change()
    If Not internalEvent Then
        If Len(txtOraTermine.Text) = 2 Then
            txtMinutiTermine.SetFocus
        End If
    End If
End Sub

Private Sub txtMinutiTermine_Change()
    isIntervalloDefault = VB_FALSO
'    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dtpDataInizio_Init()
    dtpDataInizio.MinDate = dataNulla
    dtpDataInizio.Enabled = False
End Sub

Private Sub dtpDataInizio_Change()
    isIntervalloDefault = VB_FALSO
'    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub txtOraInizio_Change()
    If Not internalEvent Then
        If Len(txtOraInizio.Text) = 2 Then
            txtMinutiInizio.SetFocus
        End If
    End If
End Sub

Private Sub txtMinutiInizio_Change()
    Call AggiornaAbilitazioneControlli
End Sub

