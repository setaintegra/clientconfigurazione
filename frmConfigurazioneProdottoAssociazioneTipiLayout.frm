VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   11700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClonaConfigurazioneAree 
      Caption         =   "Clona configurazione area selezionata su ..."
      Height          =   435
      Left            =   9720
      TabIndex        =   32
      Top             =   2520
      Width           =   1995
   End
   Begin VB.ListBox lstAreeDaClonare 
      Height          =   2085
      Left            =   4800
      Style           =   1  'Checkbox
      TabIndex        =   31
      Top             =   2520
      Width           =   4695
   End
   Begin VB.CommandButton cmdConfiguraAreeSelezionate 
      Caption         =   "Configura area selezionata"
      Height          =   435
      Left            =   10440
      TabIndex        =   30
      Top             =   960
      Width           =   1275
   End
   Begin VB.ListBox lstAreeSelezionate 
      Height          =   510
      Left            =   4800
      Style           =   1  'Checkbox
      TabIndex        =   28
      Top             =   960
      Width           =   4695
   End
   Begin VB.CommandButton cmdElimina 
      Caption         =   "Elimina associazioni"
      Height          =   435
      Left            =   10560
      TabIndex        =   27
      Top             =   7080
      Width           =   1155
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   14
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   13
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   9
      Top             =   10680
      Width           =   4035
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.ListBox lstTariffe 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   8
      Top             =   5160
      Width           =   5595
   End
   Begin VB.ListBox lstTipiSupporto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   5880
      Style           =   1  'Checkbox
      TabIndex        =   7
      Top             =   5160
      Width           =   5835
   End
   Begin VB.CommandButton cmdAggiungi 
      Caption         =   "Aggiungi"
      Height          =   435
      Left            =   9240
      TabIndex        =   6
      Top             =   7080
      Width           =   1155
   End
   Begin VB.CommandButton cmdDeselezionaArea 
      Caption         =   "Deseleziona area"
      Height          =   435
      Left            =   10440
      TabIndex        =   5
      Top             =   1440
      Width           =   1275
   End
   Begin VB.ComboBox cmbTariffe 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8160
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   7980
      Width           =   3705
   End
   Begin VB.ComboBox cmbTipiSupporto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8160
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   8820
      Width           =   3705
   End
   Begin VB.ComboBox cmbLayoutSupporto 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8160
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   9660
      Width           =   3705
   End
   Begin VB.CommandButton cmdConfermaModifica 
      Caption         =   "Conferma"
      Height          =   315
      Left            =   8700
      TabIndex        =   1
      Top             =   10200
      Width           =   1155
   End
   Begin VB.CommandButton cmdAnnullaModifica 
      Caption         =   "Annulla"
      Height          =   315
      Left            =   10260
      TabIndex        =   0
      Top             =   10200
      Width           =   1155
   End
   Begin MSComctlLib.ImageList imlCompletezza 
      Left            =   3900
      Top             =   3840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigurazioneProdottoAssociazioneTipiLayout.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigurazioneProdottoAssociazioneTipiLayout.frx":015A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigurazioneProdottoAssociazioneTipiLayout.frx":02B4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwAreePianta 
      Height          =   4035
      Left            =   120
      TabIndex        =   15
      Top             =   600
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   7117
      _Version        =   393217
      Indentation     =   706
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin MSDataGridLib.DataGrid dgrRiepilogo 
      Height          =   2535
      Left            =   120
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   7980
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   4471
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblAreeSelezionate 
      Caption         =   "Aree selezionate"
      Height          =   195
      Left            =   5160
      TabIndex        =   29
      Top             =   720
      Width           =   3255
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   26
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   25
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Associazione a Tipi supporto e Layout supporto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   24
      Top             =   120
      Width           =   7395
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso: associazione a tutte le aree di"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   23
      Top             =   7680
      Width           =   4575
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   4800
      TabIndex        =   22
      Top             =   7680
      Width           =   2535
   End
   Begin VB.Label lblTariffeLista 
      Caption         =   "Tariffe del prodotto"
      Height          =   195
      Left            =   120
      TabIndex        =   21
      Top             =   4920
      Width           =   2955
   End
   Begin VB.Label lblTipiSupportoLista 
      Caption         =   "Tipi supporto / Layout supporto"
      Height          =   195
      Left            =   5880
      TabIndex        =   20
      Top             =   4920
      Width           =   3255
   End
   Begin VB.Label lblTariffeCombo 
      Caption         =   "Tariffe dell'associazione prodotto/superarea"
      Height          =   195
      Left            =   8160
      TabIndex        =   19
      Top             =   7740
      Width           =   3135
   End
   Begin VB.Label lblTipiSupportoCombo 
      Caption         =   "Tipi supporto"
      Height          =   195
      Left            =   8160
      TabIndex        =   18
      Top             =   8580
      Width           =   1695
   End
   Begin VB.Label lblLayoutSupportoCombo 
      Caption         =   "Layout supporto"
      Height          =   195
      Left            =   8160
      TabIndex        =   17
      Top             =   9420
      Width           =   1695
   End
   Begin VB.Menu mnuAzioneSuRecord 
      Caption         =   "Azione su Record"
      Visible         =   0   'False
      Begin VB.Menu mnuModifica 
         Caption         =   "&Modifica"
      End
      Begin VB.Menu mnuElimina 
         Caption         =   "&Elimina"
      End
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoAssociazioneTipiLayoutSupporto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idAreaSelezionata As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private idTipoSupportoSelezionato As Long
Private idLayoutSupportoSelezionato As Long
Private idTariffaSelezionata As Long
Private nomeLayoutSupportoSelezionato As String
Private nomeTariffaSelezionata As String
Private nomeTipoSupportoSelezionato As String
Private nessunElementoSelezionato As Boolean
Private isProdottoAttivoSuTL As Boolean
Private idStagioneSelezionata As Long
Private idClasseProdottoSelezionata As Long
Private rateo As Long

Private utilizzoLayoutSupporto As clsUsoLayout
Private areaCommit As clsAssocArea
Private r As ADODB.Recordset
Private isCaratteristicaEreditata As ValoreBooleanoEnum
Private listaIdAreeGenitori As Collection
Private isElementoSuGrigliaSelezionato As Boolean
Private tipoTariffaLotto As String
Private idSuperareaServizio As Long
Private listaTariffe As Collection
Private listaTipiSupporto As Collection
Private listaLayoutSupporto As Collection
'Private tipoStatoRecord As TipoStatoRecordEnum
Private listaAree As Collection

Private gestioneExitCode As ExitCodeEnum
Private tipoImmagine As ImmagineAssociazioneTreeViewEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Declare Function Beep Lib "kernel32" (ByVal dwFreq As Long, ByVal dwDuration As Long) As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    dgrRiepilogo.Caption = "RIEPILOGO DETTAGLIATO ASSOCIAZIONI"
        
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        tvwAreePianta.Enabled = True
        dgrRiepilogo.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        cmdAggiungi.Enabled = False
        cmdDeselezionaArea.Enabled = False
        cmdElimina.Enabled = False
        cmdConfermaModifica.Enabled = False
        cmdAnnullaModifica.Enabled = False
        lstTariffe.Enabled = False
        lstTipiSupporto.Enabled = False
        cmbTariffe.Enabled = False
        cmbTipiSupporto.Enabled = False
        cmbLayoutSupporto.Enabled = False
        lblTariffeCombo.Enabled = False
        lblTipiSupportoCombo.Enabled = False
        lblLayoutSupportoCombo.Enabled = False
        lblTariffeLista.Enabled = False
        lblTipiSupportoLista.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        tvwAreePianta.Enabled = False
        dgrRiepilogo.Enabled = (idAreaSelezionata <> idNessunElementoSelezionato)
        If areaCommit Is Nothing Then
            cmdAggiungi.Enabled = False
        Else
            cmdAggiungi.Enabled = (areaCommit.collTariffe.count > 0 And areaCommit.collTipiSupporto.count > 0)
        End If
        cmdDeselezionaArea.Enabled = True
        cmdElimina.Enabled = True
        cmdConfermaModifica.Enabled = isElementoSuGrigliaSelezionato
        cmdAnnullaModifica.Enabled = isElementoSuGrigliaSelezionato
        lstTariffe.Enabled = (idAreaSelezionata <> idNessunElementoSelezionato)
        lstTipiSupporto.Enabled = (idAreaSelezionata <> idNessunElementoSelezionato)
        cmbTariffe.Enabled = isElementoSuGrigliaSelezionato
        cmbTipiSupporto.Enabled = isElementoSuGrigliaSelezionato
        cmbLayoutSupporto.Enabled = isElementoSuGrigliaSelezionato
        lblTariffeCombo.Enabled = isElementoSuGrigliaSelezionato
        lblTipiSupportoCombo.Enabled = isElementoSuGrigliaSelezionato
        lblLayoutSupportoCombo.Enabled = isElementoSuGrigliaSelezionato
        lblTariffeLista.Enabled = (idAreaSelezionata <> idNessunElementoSelezionato)
        lblTipiSupportoLista.Enabled = (idAreaSelezionata <> idNessunElementoSelezionato)
        If idAreaSelezionata = idTuttiGliElementiSelezionati Then
            lblOperazioneInCorso.Caption = "Operazione in corso: " & _
                "associazione a tutte le Aree di "
            lblOperazione.Caption = tvwAreePianta.Nodes(ChiaveId(idAreaSelezionata)).Text
        Else
            If listaAree.count = 1 Then
                lblOperazioneInCorso.Caption = "Operazione in corso: associazione ad Area "
                lblOperazione.Caption = tvwAreePianta.Nodes(ChiaveId(idAreaSelezionata)).Text
            Else
                lblOperazioneInCorso.Caption = "Operazione in corso: aggiunta configurazioni a "
                lblOperazione.Caption = "tutte le aree selezionate"
            End If
        End If
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        tvwAreePianta.Enabled = True
        dgrRiepilogo.Enabled = False
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        cmdAggiungi.Enabled = False
        cmdDeselezionaArea.Enabled = False
        cmdElimina.Enabled = False
        cmdConfermaModifica.Enabled = False
        cmdAnnullaModifica.Enabled = False
        lstTariffe.Enabled = False
        lstTipiSupporto.Enabled = False
        cmbTariffe.Enabled = False
        cmbTipiSupporto.Enabled = False
        cmbLayoutSupporto.Enabled = False
        lblTariffeCombo.Enabled = False
        lblTipiSupportoCombo.Enabled = False
        lblLayoutSupportoCombo.Enabled = False
        lblTariffeLista.Enabled = False
        lblTipiSupportoLista.Enabled = False
        
    Else
        'Do Nothing
    End If
        
    ' sempre abilitato altrimenti non funziona la scrollbar
    dgrRiepilogo.Enabled = True
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)
            cmdPrecedente.Enabled = gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO)
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Public Sub SetIdStagioneSelezionata(id As Long)
    idStagioneSelezionata = id
End Sub

Public Sub SetIdClasseProdottoSelezionata(id As Long)
    idClasseProdottoSelezionata = id
End Sub

Public Sub SetRateo(r As Long)
    rateo = r
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Private Sub Successivo()
    Call InserisciNellaBaseDati
    Call CaricaFormConfigurazioneAssociazioneLayoutRicevute
End Sub
'
'Private Sub CaricaFormConfigurazioneChiaviProdotto()
'    Call frmConfigurazioneProdottoChiaveProdotto.SetIdProdottoSelezionato(idProdottoSelezionato)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetIdPiantaSelezionata(idPiantaSelezionata)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetIdStagioneSelezionata(idStagioneSelezionata)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetRateo(rateo)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
'
'    Call frmConfigurazioneProdottoChiaveProdotto.SetModalitāForm(modalitāFormCorrente)
'    Call frmConfigurazioneProdottoChiaveProdotto.SetGestioneExitCode(EC_NON_SPECIFICATO)
'    Call frmConfigurazioneProdottoChiaveProdotto.Init
'End Sub

Private Sub CaricaFormConfigurazioneAssociazioneLayoutRicevute()
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetRateo(rateo)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetModalitāForm(modalitāFormCorrente)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoAssociazioneLayoutRicevute.Init
End Sub

Private Sub cmbTariffe_Click()
    If Not internalEvent Then
        idTariffaSelezionata = cmbTariffe.ItemData(cmbTariffe.ListIndex)
        nomeTariffaSelezionata = cmbTariffe.List(cmbTariffe.ListIndex)
    End If
End Sub

Private Sub cmbTipiSupporto_Click()
    If Not internalEvent Then
        idTipoSupportoSelezionato = cmbTipiSupporto.ItemData(cmbTipiSupporto.ListIndex)
        nomeTipoSupportoSelezionato = cmbTipiSupporto.List(cmbTipiSupporto.ListIndex)
        Call CaricaValoriLayoutSupporto
        Call CaricaValoriComboDaLista(cmbLayoutSupporto, listaLayoutSupporto)
    End If
End Sub

Private Sub cmbLayoutSupporto_Click()
    If Not internalEvent Then
        idLayoutSupportoSelezionato = cmbLayoutSupporto.ItemData(cmbLayoutSupporto.ListIndex)
        nomeLayoutSupportoSelezionato = cmbLayoutSupporto.List(cmbLayoutSupporto.ListIndex)
    End If
End Sub

Private Sub cmdAggiungi_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Aggiungi
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdClonaConfigurazioneAree_Click()
    Call ClonaAreeSelezionate
End Sub

Private Sub ClonaAreeSelezionate()
    Dim causaNonEditabilita As String
    Dim listaIdArea As String
    Dim i As Long
    Dim esegui As Boolean
    Dim sql As String
    Dim nD As Long
    Dim nI As Long

    cmdConfiguraAreeSelezionate.Enabled = False
    
    causaNonEditabilita = ""
    esegui = False
    listaIdArea = "-1"
    For i = 0 To lstAreeDaClonare.ListCount - 1
        If lstAreeDaClonare.Selected(i) Then
            listaIdArea = listaIdArea & ", " & lstAreeDaClonare.ItemData(i)
            esegui = True
        End If
    Next i

    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        If esegui Then
            Call ApriConnessioneBD
            SETAConnection.BeginTrans
            
            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV" & _
                " WHERE IDTARIFFA IN (SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSelezionato & ")" & _
                " AND (IDAREA IN (SELECT IDAREA FROM AREA WHERE IDAREA IN (" & listaIdArea & ") OR IDAREA_PADRE IN (" & listaIdArea & ")))"
            SETAConnection.Execute sql, nD, adCmdText
            
            sql = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO)" & _
                " SELECT DISTINCT A.IDAREA, ULS.IDTARIFFA, ULS.IDTIPOSUPPORTO, ULS.IDLAYOUTSUPPORTO" & _
                " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, AREA A, PREZZOTITOLOPRODOTTO PTP" & _
                " WHERE ULS.IDTARIFFA IN (SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSelezionato & ")" & _
                " AND ULS.IDAREA = " & idAreaSelezionata & _
                " AND A.IDAREA IN (" & listaIdArea & ")" & _
                " AND A.IDAREA = PTP.IDAREA" & _
                " AND PTP.IDTARIFFA = ULS.IDTARIFFA"
            SETAConnection.Execute sql, nI, adCmdText
    
            SETAConnection.CommitTrans
            Call ChiudiConnessioneBD
            
            MsgBox "Cancellate " & nD & " righe esistenti e inserite " & nI & " nuove righe"
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If

    cmdConfiguraAreeSelezionate.Enabled = True
    lstAreeDaClonare.Clear

End Sub

Private Sub cmdConfiguraAreeSelezionate_Click()
    Call ConfiguraAreeSelezionate
End Sub

Private Sub ConfiguraAreeSelezionate()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    
    cmdClonaConfigurazioneAree.Enabled = False
    
    Call PulisciControlliAssociazioni
    Call CaricaValoriLstTariffe
    Call CaricaValoriLstTipiSupporto
    idTariffaSelezionata = idNessunElementoSelezionato
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    idLayoutSupportoSelezionato = idNessunElementoSelezionato
'    Call CaricaCaratteristicheAreaDallaBaseDati(False)
'    Call SelezionaElementoSuGriglia(idTariffaSelezionata)
    Set areaCommit = New clsAssocArea
End Sub

Private Sub cmdDeselezionaArea_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call DeselezionaArea
    
    MousePointer = mousePointerOld
    
End Sub

Private Sub DeselezionaArea()
    Dim causaNonEditabilita As String
    Dim nomeArea As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    If (idAreaSelezionata <> idNessunElementoSelezionato) Then
        nomeArea = tvwAreePianta.Nodes(ChiaveId(idAreaSelezionata)).Text
        Call frmMessaggio.Visualizza("ConfermaSalvataggioAssociazioniArea", nomeArea)
        If frmMessaggio.exitCode = EC_CONFERMA Then
            DoEvents
            Set utilizzoLayoutSupporto = New clsUsoLayout
            If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
                isConfigurabile = True
                If tipoStatoProdotto = TSP_ATTIVO Then
                    Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                End If
                If isConfigurabile Then
                    Call InserisciNellaBaseDati
                End If
            End If
            Call utilizzoLayoutSupporto.inizializza(idProdottoSelezionato)
            Call AssociaIconaANodi
        End If
    End If
    Call EliminaStrutturaDatiCache
    Call PulisciControlliAssociazioni
    idAreaSelezionata = idNessunElementoSelezionato
    Call lstAreeSelezionate.Clear
    Call lstAreeDaClonare.Clear
    Set listaAree = Nothing
    Set listaAree = New Collection
    
    Call AggiornaAbilitazioneControlli
    
    cmdConfiguraAreeSelezionate.Enabled = True
    cmdClonaConfigurazioneAree.Enabled = True
End Sub

Private Sub cmdAnnullaModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call AnnullaModifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub AnnullaModifica()
    Dim internalEventOld As Boolean

    isElementoSuGrigliaSelezionato = False
    idTariffaSelezionata = idNessunElementoSelezionato
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    idLayoutSupportoSelezionato = idNessunElementoSelezionato
    internalEventOld = internalEvent
    internalEvent = True
    Call SelezionaElementoSuCombo(cmbTariffe, idTariffaSelezionata)
    Call SelezionaElementoSuCombo(cmbTipiSupporto, idTipoSupportoSelezionato)
    Call SelezionaElementoSuCombo(cmbLayoutSupporto, idLayoutSupportoSelezionato)
    internalEvent = internalEventOld
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdConfermaModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    If idTipoSupportoSelezionato <> idNessunElementoSelezionato And idLayoutSupportoSelezionato <> idNessunElementoSelezionato Then
        Call ConfermaModificaRecord
    Else
        MsgBox "Tipo o layout supporto non selezionato"
    End If
    
    MousePointer = mousePointerOld
End Sub

Private Sub ConfermaModificaRecord()

    Call SetGestioneExitCode(EC_CONFERMA)
    r.Update
    r("IDTARIFFA") = idTariffaSelezionata
    r("TARIFFA") = nomeTariffaSelezionata
    r("IDTIPOSUPPORTO") = idTipoSupportoSelezionato
    r("TIPOSUPPORTO") = nomeTipoSupportoSelezionato
    r("IDLAYOUTSUPPORTO") = idLayoutSupportoSelezionato
    r("LAYOUTSUPPORTO") = nomeLayoutSupportoSelezionato
    Call PulisciComboBox
End Sub

Private Sub cmdElimina_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call elimina
    
    MousePointer = mousePointerOld
End Sub

Private Sub elimina()
    Dim causaNonEditabilita As String
    Dim nomeArea As String
    Dim isConfigurabile As Boolean
    
    nomeArea = tvwAreePianta.Nodes(ChiaveId(idAreaSelezionata)).Text
    causaNonEditabilita = ""
    Call frmMessaggio.Visualizza("ConfermaEliminazioneAssociazioni", nomeArea)
    If frmMessaggio.exitCode = EC_CONFERMA Then
        DoEvents
        If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
            isConfigurabile = True
            If tipoStatoProdotto = TSP_ATTIVO Then
                Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                If frmMessaggio.exitCode <> EC_CONFERMA Then
                    isConfigurabile = False
                End If
            End If
            If isConfigurabile Then
                Call EliminaAssociazioniInBD(idAreaSelezionata)
    '            Set areaCommit = New clsAssocArea
                Call adcRiepilogo_Init
                Call dgrRiepilogo_Init
            End If
        End If
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean
    
    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
                isConfigurabile = True
                If tipoStatoProdotto = TSP_ATTIVO Then
                    Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
                    If frmMessaggio.exitCode <> EC_CONFERMA Then
                        isConfigurabile = False
                    End If
                End If
                If isConfigurabile Then
                    Call InserisciNellaBaseDati
                    Call ScriviLog(CCTA_INSERIMENTO, CCDA_PRODOTTO, CCDA_ASSOC_TIPI_LAYOUT_SUPPORTI, stringaNota, idProdottoSelezionato)
                End If
            Else
                Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
            End If
'            Call Esci
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Public Sub Init()
    Dim sql As String

    Set utilizzoLayoutSupporto = New clsUsoLayout
    isElementoSuGrigliaSelezionato = False
    Call SetIdAreaSelezionata(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call utilizzoLayoutSupporto.inizializza(idProdottoSelezionato)
    Call tvwAreePianta_Init
    Call adcRiepilogo_Init
    Call dgrRiepilogo_Init
    Call CaricaGerarchiaAree
    Call AssociaIconaANodi
    Call AggiornaAbilitazioneControlli
    nessunElementoSelezionato = False
    Set listaAree = New Collection

    Call Me.Show(vbModal)

End Sub

Public Sub SetIdAreaSelezionata(id As Long)
    idAreaSelezionata = id
End Sub

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoPrezzi.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoPrezzi.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec("CODICE")
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    If idAreaSelezionata <> idTuttiGliElementiSelezionati Then
        If i <= 0 Then
            i = 1
        Else
            'Do Nothing
        End If
        cmb.AddItem "<nessuno>"
        cmb.ItemData(i - 1) = idNessunElementoSelezionato
    End If
    
    Call ChiudiConnessioneBD
            
End Sub

Private Sub CaricaGerarchiaAree()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim level As Integer
    Dim idArea As Long
    Dim idPadre As Long
    Dim chiaveArea As String
    Dim chiavePadre As String
    Dim chiavePianta As String
    Dim nomeArea As String
    Dim t As TreeView
    Dim l As ImageList
    Dim nodo As Node
    Dim tipoSA As TipoAreaEnum
    
    Set t = tvwAreePianta
    Set l = imlCompletezza
    
    chiavePianta = ChiaveId(idTuttiGliElementiSelezionati)
    Set nodo = t.Nodes.Add(, , chiavePianta, "Pianta")
    t.Nodes(chiavePianta).Image = IAT_PIANTA
    Call ApriConnessioneBD
    
    sql = "SELECT LEVEL, IDAREA, NOME, CODICE, IDAREA_PADRE, IDTIPOAREA" & _
        " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & _
        " START WITH IDAREA_PADRE IS NULL" & _
        " CONNECT BY PRIOR IDAREA = IDAREA_PADRE" & _
        " ORDER BY LEVEL, NOME"
    rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
    DoEvents
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        While Not (rec.EOF)
            level = rec("LEVEL").Value
            idArea = rec("IDAREA")
            nomeArea = rec("CODICE") & " - " & rec("NOME")
            tipoSA = rec("IDTIPOAREA")
            If tipoSA = TA_SUPERAREA_SERVIZIO Then
                idSuperareaServizio = idArea
            End If
            idPadre = IIf(IsNull(rec("IDAREA_PADRE")), idTuttiGliElementiSelezionati, rec("IDAREA_PADRE"))
            chiaveArea = ChiaveId(idArea)
            chiavePadre = ChiaveId(idPadre)
            If (idPadre = idTuttiGliElementiSelezionati) Then
                Set nodo = t.Nodes.Add(chiavePianta, tvwChild, chiaveArea, nomeArea)
            Else
                Set nodo = t.Nodes.Add(chiavePadre, tvwChild, chiaveArea, nomeArea)
            End If
            t.Nodes(chiaveArea).Image = IAT_MANCANTE
            Call nodo.EnsureVisible
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub tvwAreePianta_Init()
    Dim t As TreeView
    
    Set t = tvwAreePianta
    t.Style = tvwTreelinesPlusMinusPictureText
    t.ImageList = imlCompletezza
End Sub

Private Sub dgrRiepilogo_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call ImpostaAzioneSuDataGrid(Button, Shift, x, y)
End Sub

Private Sub ImpostaAzioneSuDataGrid(Button As Integer, Shift As Integer, x As Single, y As Single)
'    If Button = vbRightButton Then
'        If Not (r.EOF And r.BOF) Then
'            If r("ISEREDITATA").Value = VB_FALSO Then
'                Call PopupMenu(mnuAzioneSuRecord)
'            End If
'        End If
'    End If
'    If Button = vbRightButton Then
    If Not r.EOF Then
        If Not (r.EOF And r.BOF) Then
            If x < 240 Then
                If r("ISEREDITATA").Value = VB_FALSO Then
                    Call PopupMenu(mnuAzioneSuRecord)
                End If
            End If
        End If
    End If
    
End Sub

Private Sub dgrRiepilogo_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call PulisciComboBox
    End If
End Sub

Private Sub mnuElimina_Click()
    Call EliminaRecord
End Sub

Private Sub EliminaRecord()
    Dim l As Long
    
    If gestioneRecordGriglia = ASG_MODIFICA Then
        Call r.Delete(adAffectCurrent)
'    Else
'        l = Beep(700, 1)
    End If
End Sub

Private Sub mnuModifica_Click()
    Call ModificaRecord
End Sub

Private Sub ModificaRecord()
    Dim l As Long
    
    If gestioneRecordGriglia = ASG_MODIFICA Then
        GetListaIdAreaSelezionata
        isElementoSuGrigliaSelezionato = True
        Call AggiornaAbilitazioneControlli
        Call CaricaValoriComboDaLista(cmbTariffe, listaTariffe)
        Call CaricaValoriComboDaLista(cmbTipiSupporto, listaTipiSupporto)
        Call SelezionaElementoSuCombo(cmbTariffe, idTariffaSelezionata)
        Call SelezionaElementoSuCombo(cmbTipiSupporto, idTipoSupportoSelezionato)
        Call SelezionaElementoSuCombo(cmbLayoutSupporto, idLayoutSupportoSelezionato)
'    Else
'        l = Beep(700, 1000)
    End If
End Sub

Private Sub GetListaIdAreaSelezionata()
    If Not (r.BOF) Then
        If r.EOF Then
            r.MoveFirst
        End If
        isCaratteristicaEreditata = r("ISEREDITATA").Value
        idTariffaSelezionata = r("IDTARIFFA").Value
        idTipoSupportoSelezionato = r("IDTIPOSUPPORTO").Value
        idLayoutSupportoSelezionato = r("IDLAYOUTSUPPORTO").Value
    Else
        isCaratteristicaEreditata = VB_VERO
        idTariffaSelezionata = idNessunElementoSelezionato
        idTipoSupportoSelezionato = idNessunElementoSelezionato
        idLayoutSupportoSelezionato = idNessunElementoSelezionato
    End If
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstTipiSupporto_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaTipoSupporto
    End If
End Sub


Private Sub lstTariffe_ItemCheck(Item As Integer)
    If Not internalEvent Then
        Call SelezionaDeselezionaTariffa
    End If
End Sub

Private Function RilevaLayoutAssociatoATipo() As Boolean
    Call frmDettagliLayoutSupporto.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmDettagliLayoutSupporto.Init(idTipoSupportoSelezionato)
    If frmDettagliLayoutSupporto.GetExitCode = EC_CONFERMA Then
        RilevaLayoutAssociatoATipo = True
    Else
        RilevaLayoutAssociatoATipo = False
    End If
End Function

Private Sub tvwAreePianta_Click()
    If Not internalEvent Then
        If Not nessunElementoSelezionato Then
            Call ElementoSelezionatoSuTreeView
        End If
    End If
    nessunElementoSelezionato = False
End Sub

Private Sub PulisciControlliAssociazioni()
'    Call adcRiepilogo_Init
'    Call dgrRiepilogo_Init
    Call lstTariffe_Init
    Call lstTipiSupporto_Init
End Sub

Private Sub PulisciListeAssociazioni()
    Call lstTariffe_Init
    Call lstTipiSupporto_Init
End Sub

Private Sub GetIdAreaSelezionata()
    idAreaSelezionata = IdChiave(tvwAreePianta.selectedItem.Key)
End Sub

Private Sub AggiornaTreeView()
    Dim t As TreeView
    
    Set t = tvwAreePianta
    t.Refresh
End Sub

Private Sub PulisciTreeView()
    Dim t As TreeView
    
    Set t = tvwAreePianta
    t.Nodes.Clear
End Sub

Private Sub tvwAreePianta_Collapse(ByVal Node As MSComctlLib.Node)
    nessunElementoSelezionato = True
End Sub

Private Sub tvwAreePianta_Expand(ByVal Node As MSComctlLib.Node)
    nessunElementoSelezionato = True
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

Private Sub Aggiungi()
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_INSERISCI_DA_SELEZIONE)
    Call AggiungiRecordsetARiepilogo(VB_FALSO)
    Call EliminaStrutturaDatiCache
    Set areaCommit = New clsAssocArea
    Call PulisciListeAssociazioni
    idTariffaSelezionata = idNessunElementoSelezionato
    idTipoSupportoSelezionato = idNessunElementoSelezionato
    idLayoutSupportoSelezionato = idNessunElementoSelezionato
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub EliminaStrutturaDatiCache()
    Set areaCommit = Nothing
End Sub

Private Function isAreaErede(idA As Long, idAreaOrigine As Long) As Boolean
    Dim t As TreeView
    Dim l As ImageList
    Dim nodo As Node
    Dim area As clsAssocArea
    Dim idAreaCache As Long
    Dim idAreaPadre As Long
    Dim trovato As Boolean
    
    Set t = tvwAreePianta
    Set l = imlCompletezza
    
    trovato = False
    For Each area In utilizzoLayoutSupporto.collAree
        idAreaCache = area.idArea
        Set nodo = t.Nodes(ChiaveId(idA)).Parent
        If nodo Is Nothing Then
            trovato = False
            isAreaErede = False
            Exit Function
        Else
            idAreaPadre = IdChiave(nodo.Key)
        End If
        If idAreaPadre = idAreaCache Then
            trovato = True
        End If
        While idAreaPadre <> idAreaCache And idAreaPadre <> idTuttiGliElementiSelezionati And Not trovato
            Set nodo = t.Nodes(ChiaveId(idAreaPadre)).Parent
            If Not nodo Is Nothing Then
                idAreaPadre = IdChiave(nodo.Key)
                If idAreaPadre = idAreaCache Then
                    trovato = True
                End If
            Else
                idAreaPadre = idTuttiGliElementiSelezionati
            End If
        Wend
    Next area
    If trovato Then
        idAreaOrigine = idAreaPadre
    End If
    isAreaErede = trovato
End Function

Private Sub adcRiepilogo_Init()
    Dim internalEventOld As Boolean
    Dim f As Fields
    
    internalEventOld = internalEvent
    internalEvent = True
            
    Set r = New ADODB.Recordset
    Set f = r.Fields
    
    Call f.Append("ISEREDITATA", adInteger, 4)
    Call f.Append("EREDITATA", adVarChar, 2)
    Call f.Append("IDTARIFFA", adInteger, 4)
    Call f.Append("TARIFFA", adVarChar, 80)
    Call f.Append("IDTIPOSUPPORTO", adInteger, 4)
    Call f.Append("TIPOSUPPORTO", adVarChar, 40)
    Call f.Append("IDLAYOUTSUPPORTO", adInteger, 4)
    Call f.Append("LAYOUTSUPPORTO", adVarChar, 40)
    Call r.Open
    Set dgrRiepilogo.dataSource = r

    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub dgrRiepilogo_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrRiepilogo
    g.ScrollBars = dbgVertical
    dimensioneGrid = g.Width - 100
    numeroCampi = 4
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Visible = False
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
    g.Columns(4).Visible = False
    g.Columns(5).Width = (dimensioneGrid / numeroCampi)
    g.Columns(6).Visible = False
    g.Columns(7).Width = (dimensioneGrid / numeroCampi)
'    g.Columns(8).Visible = False
'    g.Columns(9).Width = (dimensioneGrid / numeroCampi)
    
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub AggiungiRecordsetARiepilogo(isRecordsetEreditato As ValoreBooleanoEnum)
    Dim ta As clsAssocTariffa
    Dim tt As clsAssocTipoTerm
    Dim ts As clsAssocSupporto
    
    For Each ta In areaCommit.collTariffe
        For Each ts In areaCommit.collTipiSupporto
            r.AddNew
            r("ISEREDITATA") = isRecordsetEreditato
            r("EREDITATA") = IIf(isRecordsetEreditato = VB_FALSO, "NO", "SI")
            r("IDTARIFFA") = ta.idTariffa
            r("TARIFFA") = ta.labelTariffa
            r("IDTIPOSUPPORTO") = ts.idTipoSupporto
            r("TIPOSUPPORTO") = ts.labelTipoSupporto
            r("IDLAYOUTSUPPORTO") = ts.idLayoutSupporto
            r("LAYOUTSUPPORTO") = ts.labelLayoutSupporto
        Next ts
    Next ta
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    If Not (r.BOF And r.EOF) Then
        r.MoveFirst
        Do While Not r.EOF
            If id = r("IDTARIFFA") Then
                Exit Do
            End If
            r.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub CaricaCaratteristicheAreaDallaBaseDati(selezioneDaTreeView As Boolean)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    Dim listaId As String
    Dim isRecordsetEreditato As ValoreBooleanoEnum
    Dim internalEventOld As Boolean
    
    ' si abilita la griglia di modifica solo se se si č selezionata una sola area/superarea
'    If selezioneDaTreeView Or listaAree.count = 1 Then
    If selezioneDaTreeView Then
        Call adcRiepilogo_Init
        Call dgrRiepilogo_Init
    
        Call ApriConnessioneBD
        
        Call CreaListaIdAreeGenitori
        
        internalEventOld = internalEvent
        internalEvent = True
        listaId = CStr(idAreaSelezionata)
        
        For i = 1 To listaIdAreeGenitori.count
            listaId = listaId & ", " & listaIdAreeGenitori(i)
        Next i
    
        sql = "SELECT LIVELLO, ULS.*, T.CODICE ||' - '|| T.NOME TLABEL, LS.CODICE ||' - '|| LS.NOME LSLABEL," & _
            " TS.CODICE ||' - '|| TS.NOME TSLABEL" & _
            " FROM UTILIZZOLAYOUTSUPPORTOCPV ULS, TARIFFA T, LAYOUTSUPPORTO LS, TIPOSUPPORTO TS, " & _
            " (SELECT LEVEL AS LIVELLO, IDAREA, NOME, IDAREA_PADRE" & _
            " FROM AREA WHERE IDPIANTA = " & idPiantaSelezionata & " START WITH IDAREA_PADRE IS NULL" & _
            " CONNECT BY PRIOR IDAREA = IDAREA_PADRE ORDER BY LEVEL) LIVELLI" & _
            " WHERE LIVELLI.IDAREA = ULS.IDAREA AND ULS.IDAREA IN (" & listaId & ")" & _
            " AND T.IDPRODOTTO = " & idProdottoSelezionato & " AND T.IDTARIFFA = ULS.IDTARIFFA AND" & _
            " (ULS.IDLAYOUTSUPPORTO = LS.IDLAYOUTSUPPORTO) AND" & _
            " (ULS.IDTIPOSUPPORTO = TS.IDTIPOSUPPORTO) AND" & _
            " LIVELLO >= (SELECT MAX(LIVELLO) FROM UTILIZZOLAYOUTSUPPORTOCPV ULS2, TARIFFA T2," & _
            " (SELECT LEVEL AS LIVELLO, IDAREA, NOME, IDAREA_PADRE FROM AREA" & _
            " WHERE IDPIANTA = " & idPiantaSelezionata & " START WITH IDAREA_PADRE IS NULL" & _
            " CONNECT BY PRIOR IDAREA = IDAREA_PADRE ORDER BY LEVEL) LIVELLI2" & _
            " WHERE LIVELLI2.IDAREA = ULS2.IDAREA AND ULS.IDTARIFFA = ULS2.IDTARIFFA" & _
            " AND ULS.IDTIPOSUPPORTO = ULS2.IDTIPOSUPPORTO" & _
            " AND T2.IDPRODOTTO = " & idProdottoSelezionato & " AND ULS2.IDTARIFFA = T2.IDTARIFFA" & _
            " AND ULS2.IDAREA IN (" & listaId & "))" & _
            " ORDER BY TLABEL, TSLABEL"
        rec.Open sql, SETAConnection, adOpenForwardOnly, adLockOptimistic
        DoEvents
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                r.AddNew
                isRecordsetEreditato = IIf(rec("IDAREA").Value = idAreaSelezionata, VB_FALSO, VB_VERO)
                r("ISEREDITATA") = isRecordsetEreditato
                r("EREDITATA") = IIf(isRecordsetEreditato = VB_FALSO, "NO", "SI")
                r("IDTARIFFA") = rec("IDTARIFFA")
                r("TARIFFA") = rec("TLABEL")
                r("IDTIPOSUPPORTO") = rec("IDTIPOSUPPORTO")
                r("TIPOSUPPORTO") = rec("TSLABEL")
                r("IDLAYOUTSUPPORTO") = rec("IDLAYOUTSUPPORTO")
                r("LAYOUTSUPPORTO") = rec("LSLABEL")
                rec.MoveNext
            Wend
        End If
        rec.Close
        internalEvent = internalEventOld
        
        Call ChiudiConnessioneBD
    End If
    
End Sub

Private Sub CreaListaIdAreeGenitori()
    Dim idAreaOrigine As Long
    Dim idA As Long
    Dim i As Long
    
    Set listaIdAreeGenitori = New Collection
    For i = 1 To listaAree.count
        idA = listaAree(i)
        idAreaOrigine = idNessunElementoSelezionato
        While Not idAreaOrigine = idTuttiGliElementiSelezionati
            If isAreaErede(idA, idAreaOrigine) Then
                Call listaIdAreeGenitori.Add(idAreaOrigine)
                idA = idAreaOrigine
                idAreaOrigine = idNessunElementoSelezionato
            Else
                idAreaOrigine = idTuttiGliElementiSelezionati
            End If
        Wend
    Next i
End Sub

Private Sub ElementoSelezionatoSuTreeView()
    Dim sql As String
    Dim newArea As New clsAssocArea
    Dim idAreaOrigine As Long
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call GetIdAreaSelezionata
    
'    'LA CONDIZIONE idRecordSelezionato <> idTuttiGliElementiSelezionati
'    'E' STATA AGGIUNTA RECENTEMENTE; SE SI VOLESSE TORNARE ALLA
'    'ASSOCIAZIONE A TUTTA LA PIANTA BISOGNA TOGLIERLA E RIPRISTINARE
'    'LA INSERISCINELLABASEDATI_OLD
'    If idRecordSelezionato <> idTuttiGliElementiSelezionati Then
'        Call SetGestioneRecordGriglia(ASG_MODIFICA)
'        Call SetGestioneExitCode(EC_NON_SPECIFICATO)
'        Call AggiornaAbilitazioneControlli
'        Call PulisciControlliAssociazioni
'        Call CaricaValoriLstTariffe
'        Call CaricaValoriLstTipiSupporto
'        idTariffaSelezionata = idNessunElementoSelezionato
'        idTipoSupportoSelezionato = idNessunElementoSelezionato
'        idLayoutSupportoSelezionato = idNessunElementoSelezionato
'        Call CaricaCaratteristicheAreaDallaBaseDati
'        Call SelezionaElementoSuGriglia(idTariffaSelezionata)
'        Set areaCommit = New clsAssocArea
'    End If
    
    Call AggiungiAreaAListaAreeSelezionate(idAreaSelezionata)
    Call CaricaListaSuperAreeDaClonare(idAreaSelezionata)
    Call CaricaCaratteristicheAreaDallaBaseDati(True)
    
    MousePointer = mousePointerOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiungiAreaAListaAreeSelezionate(idArea As Long)
    Dim sql As String
    Dim rec As OraDynaset
    Dim listaIdAreeSelezionate As String
    Dim i As Long

    Call lstAreeSelezionate.Clear
    sql = "SELECT IDAREA, CODICE, NOME" & _
        " FROM AREA" & _
        " WHERE IDAREA = " & idArea
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstAreeSelezionate.AddItem(rec("NOME"))
            lstAreeSelezionate.ItemData(i) = rec("IDAREA")
            lstAreeSelezionate.Selected(i) = True
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close

''''    Call listaAree.Add(idArea)
''''
''''    Call lstAreeSelezionate.Clear
''''
''''    If listaAree.count > 0 Then
''''        listaIdAreeSelezionate = "(" & listaAree(1)
''''        For i = 2 To listaAree.count
''''            listaIdAreeSelezionate = listaIdAreeSelezionate & "," & listaAree(i)
''''        Next i
''''        listaIdAreeSelezionate = listaIdAreeSelezionate & ")"
''''
''''        sql = "SELECT IDAREA ID_SA, CODICE CODICE_SA, NOME NOME_SA, NULL ID_A, NULL CODICE_A, NULL NOME_A, 1 SELEZIONATO" & _
''''            " FROM AREA" & _
''''            " WHERE IDAREA IN " & listaIdAreeSelezionate & _
''''            " AND IDTIPOAREA IN (4,5,6)"
''''        sql = sql & " UNION" & _
''''            " SELECT SA.IDAREA ID_SA, SA.CODICE CODICE_SA, SA.NOME NOME_SA, NULL ID_A, NULL CODICE_A, ' ' NOME_A, 0 SELEZIONATO" & _
''''            " FROM AREA A, AREA SA" & _
''''            " WHERE A.IDAREA IN " & listaIdAreeSelezionate & _
''''            " AND A.IDTIPOAREA IN (2,3)" & _
''''            " AND A.IDAREA_PADRE = SA.IDAREA" & _
''''            " AND SA.IDAREA NOT IN (" & _
''''            " SELECT IDAREA" & _
''''            " FROM AREA" & _
''''            " WHERE IDAREA IN " & listaIdAreeSelezionate & _
''''            " AND IDTIPOAREA IN (4,5,6)" & _
''''            " )"
''''        sql = sql & " UNION" & _
''''            " SELECT SA.IDAREA ID_SA, SA.CODICE CODICE_SA, SA.NOME NOME_SA, A.IDAREA ID_A, A.CODICE CODICE_A, A.NOME NOME_A, 1 SELEZIONATO" & _
''''            " FROM AREA A, AREA SA" & _
''''            " WHERE A.IDAREA IN " & listaIdAreeSelezionate & _
''''            " AND A.IDTIPOAREA IN (2,3)" & _
''''            " AND A.IDAREA_PADRE = SA.IDAREA" & _
''''            " AND SA.IDAREA NOT IN (" & _
''''            " SELECT IDAREA" & _
''''            " FROM AREA" & _
''''            " WHERE IDAREA IN " & listaIdAreeSelezionate & _
''''            " AND IDTIPOAREA IN (4,5,6)" & _
''''            " )" & _
''''            " ORDER BY NOME_SA, NOME_A"
''''        Set rec = ORADB.CreateDynaset(sql, 0&)
''''        If Not (rec.BOF And rec.EOF) Then
''''            rec.MoveFirst
''''            i = 0
''''            While Not rec.EOF
''''                If IsNull(rec("ID_A")) Then
''''                    Call lstAreeSelezionate.AddItem(rec("NOME_SA"))
''''                    lstAreeSelezionate.ItemData(i) = rec("ID_SA")
''''                Else
''''                    Call lstAreeSelezionate.AddItem("    " & rec("NOME_A"))
''''                    lstAreeSelezionate.ItemData(i) = rec("ID_A")
''''                End If
''''                If rec("SELEZIONATO") = 1 Then
''''                    lstAreeSelezionate.Selected(i) = True
''''                Else
''''                    lstAreeSelezionate.Selected(i) = False
''''                End If
''''                i = i + 1
''''                rec.MoveNext
''''            Wend
''''        End If
''''        rec.Close
''''    End If
    
End Sub

Private Sub CaricaListaSuperAreeDaClonare(idArea As Long)
    Dim sql As String
    Dim rec As OraDynaset
    Dim listaIdAreeSelezionate As String
    Dim i As Long

    Call lstAreeDaClonare.Clear
    sql = "SELECT A2.IDAREA, A2.NOME" & _
        " FROM AREA A1, AREA A2" & _
        " where A1.idarea = " & idArea & _
        " AND A1.IDTIPOAREA = 5" & _
        " AND A1.IDPIANTA = A2.IDPIANTA" & _
        " AND A2.IDTIPOAREA = 5" & _
        " AND A1.IDAREA <> A2.IDAREA" & _
        " ORDER BY A2.NOME"
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        i = 0
        While Not rec.EOF
            Call lstAreeDaClonare.AddItem(rec("NOME"))
            lstAreeDaClonare.ItemData(i) = rec("IDAREA")
            lstAreeDaClonare.Selected(i) = False
            
            i = i + 1
            rec.MoveNext
        Wend
    End If
    rec.Close
    
End Sub

Private Sub AssociaIconaANodi()
    Dim t As TreeView
    Dim nodo As Node
    Dim l As ImageList
    Dim areaDB As clsAssocArea
    
    Set t = tvwAreePianta
    Set l = imlCompletezza
    
    For Each areaDB In utilizzoLayoutSupporto.collAree
        t.Nodes(ChiaveId(areaDB.idArea)).Image = IAT_PRESENTE
    Next areaDB
        
End Sub

Private Sub PulisciComboBox()
    Call cmbTariffe.Clear
    Call cmbTipiSupporto.Clear
    Call cmbLayoutSupporto.Clear
End Sub

Private Sub CreaListaSuperareeFiglieDiPrimiLivello(idA As Long, listaId As Collection)
    Dim t As TreeView
    Dim nodoPadre As Node
    Dim nodoFiglio As Node
    Dim chiaveArea As String
    Dim i As Integer
    Dim chiaveSuperareaServizio As String
    
    chiaveSuperareaServizio = ChiaveId(idSuperareaServizio)
    chiaveArea = ChiaveId(idA)
    Set t = tvwAreePianta
    Set nodoPadre = t.Nodes(chiaveArea)
    Set nodoFiglio = nodoPadre.Child
    If nodoFiglio Is Nothing Then
        If nodoPadre.Key = chiaveSuperareaServizio Then
            Call listaId.Add(IdChiave(nodoPadre.Key))
        Else
            Call listaId.Add(IdChiave(nodoPadre.Parent.Key))
        End If
    Else
        If (nodoFiglio.Children = 0 And nodoFiglio.Key <> chiaveSuperareaServizio) Then
            Call listaId.Add(IdChiave(nodoPadre.Key))
        Else
            For i = 1 To nodoPadre.Children
                Call CreaListaSuperareeFiglieDiPrimiLivello(IdChiave(nodoFiglio.Key), listaId)
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    End If
End Sub

Private Function GetRecordSelezionatoDaLista() As Long
    Dim i As Long
    
    If lstAreeSelezionate.SelCount = 1 Then
        For i = 0 To lstAreeSelezionate.ListCount - 1
            If lstAreeSelezionate.Selected(i) Then
                GetRecordSelezionatoDaLista = lstAreeSelezionate.ItemData(i)
            End If
        Next i
    Else
        GetRecordSelezionatoDaLista = idNessunElementoSelezionato
    End If
    
End Function

Private Sub InserisciNellaBaseDati()
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim sql2 As String
    Dim rec2 As New ADODB.Recordset
    Dim i As Integer
    Dim idTar As Long
    Dim idSup As Long
    Dim idLay As Long
    Dim isEreditata As ValoreBooleanoEnum
    Dim idSA As Long
    Dim internalEventOld As Boolean
    Dim idA As Long
    Dim n As Long
    Dim condizioniSQL As String
    Dim idTariffaOmaggio As Long
    
    If idAreaSelezionata <> idNessunElementoSelezionato Then
    
        Call ApriConnessioneBD
        
        SETAConnection.BeginTrans
        
'   A: ELIMINAZIONE DI TUTTE LE ASSOCIAZIONI PRECEDENTI
        sql1 = "SELECT IDTARIFFA FROM TARIFFA WHERE IDPRODOTTO = " & idProdottoSelezionato
        rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec1.EOF And rec1.BOF) Then
            rec1.MoveFirst
            While Not rec1.EOF
                idTar = rec1("IDTARIFFA")
                sql2 = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV ULS WHERE" & _
                    " ULS.IDTARIFFA = " & idTar & " AND ULS.IDAREA = " & idAreaSelezionata
                SETAConnection.Execute sql2, n, adCmdText
                rec1.MoveNext
            Wend
        End If
        rec1.Close
    
'   B: INSERIMENTO DELLE ASSOCIAZIONI CONFIGURATE
        internalEventOld = internalEvent
        internalEvent = True
        If Not (r.EOF And r.BOF) Then
            r.MoveFirst
            While Not r.EOF
                isEreditata = r("ISEREDITATA").Value
                idTar = r("IDTARIFFA").Value
                idSup = r("IDTIPOSUPPORTO").Value
                idLay = r("IDLAYOUTSUPPORTO").Value
                If isEreditata = VB_FALSO Then
                    sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (" & _
                        "IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO) " & _
                        "VALUES (" & _
                        idAreaSelezionata & ", " & _
                        idTar & ", " & _
                        idSup & ", " & _
                        idLay & ")"
                    On Error Resume Next
                    SETAConnection.Execute sql1, n, adCmdText
                    If Err.Number <> 0 Then Err.Clear
                    
                    idTariffaOmaggio = IdTariffaOmaggioAssociata(idTar)
                    If idTariffaOmaggio <> idNessunElementoSelezionato Then
                        sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (" & _
                            "IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO) " & _
                            "VALUES (" & _
                            idAreaSelezionata & ", " & _
                            idTariffaOmaggio & ", " & _
                            idSup & ", " & _
                            idLay & ")"
                        On Error Resume Next
                        SETAConnection.Execute sql1, n, adCmdText
                    End If
                End If
                r.MoveNext
            Wend
        End If
'''''''        Else
'''''''    '   A: AGGIUNTA DELLE ASSOCIAZIONI CONFIGURATE per tutte le aree selezionate
'''''''            internalEventOld = internalEvent
'''''''            internalEvent = True
'''''''            If Not (r.EOF And r.BOF) Then
'''''''                r.MoveFirst
'''''''                While Not r.EOF
'''''''                    isEreditata = r("ISEREDITATA").Value
'''''''                    idTar = r("IDTARIFFA").Value
'''''''                    idSup = r("IDTIPOSUPPORTO").Value
'''''''                    idLay = r("IDLAYOUTSUPPORTO").Value
'''''''                    If isEreditata = VB_FALSO Then
'''''''                        For i = 1 To listaAree.count
'''''''                            sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (" & _
'''''''                                "IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO) " & _
'''''''                                "VALUES (" & _
'''''''                                listaAree(i) & ", " & _
'''''''                                idTar & ", " & _
'''''''                                idSup & ", " & _
'''''''                                idLay & ")"
'''''''                            On Error Resume Next
'''''''                            SETAConnection.Execute sql1, n, adCmdText
'''''''                            If Err.Number <> 0 Then Err.Clear
'''''''
'''''''                            idTariffaOmaggio = IdTariffaOmaggioAssociata(idTar)
'''''''                            If idTariffaOmaggio <> idNessunElementoSelezionato Then
'''''''                                sql1 = "INSERT INTO UTILIZZOLAYOUTSUPPORTOCPV (" & _
'''''''                                    "IDAREA, IDTARIFFA, IDTIPOSUPPORTO, IDLAYOUTSUPPORTO) " & _
'''''''                                    "VALUES (" & _
'''''''                                    listaAree(i) & ", " & _
'''''''                                    idTariffaOmaggio & ", " & _
'''''''                                    idSup & ", " & _
'''''''                                    idLay & ")"
'''''''                                On Error Resume Next
'''''''                                SETAConnection.Execute sql1, n, adCmdText
'''''''                            End If
'''''''                        Next i
'''''''                    End If
'''''''                    r.MoveNext
'''''''                Wend
'''''''            End If
        internalEvent = internalEventOld
        
        SETAConnection.CommitTrans
        Call ChiudiConnessioneBD
    End If
        
    Call AggiornaAbilitazioneControlli
    
End Sub
'
'Private Function IsTipoTerminaleAssociabileATariffa(idTariffa As Long, idTipoTerminale As Long) As Boolean
'    Dim tariffaCorrente As clsElementoLista
'    Dim terminaleCorrente As clsElementoLista
'    Dim i As Integer
'    Dim trovato As Boolean
'
'On Error GoTo erroreIsTipoTerminaleAssociabileATariffa
'    trovato = False
'    Set tariffaCorrente = listaTariffe.Item(ChiaveId(idTariffa))
'    For Each terminaleCorrente In tariffaCorrente.listaSottoElementiLista
'        If idTipoTerminale = terminaleCorrente.idElementoLista Then
'            trovato = True
'        End If
'    Next terminaleCorrente
'    IsTipoTerminaleAssociabileATariffa = trovato
'    Exit Function
'
'erroreIsTipoTerminaleAssociabileATariffa:
'    IsTipoTerminaleAssociabileATariffa = False
'
'End Function

Private Sub CaricaValoriLstTariffe()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim sql1 As String
    Dim rec1 As New ADODB.Recordset
    Dim nome As String
    Dim codice As String
    Dim idTariffa As Long
    Dim chiaveTariffa As String
    Dim listaSuperareeFiglieDiPrimiLivello As Collection
    Dim listaSuperaree As String
    Dim i As Integer
    Dim terminaliAttivi As String
    Dim codTTA As String
    Dim tariffaCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaSuperareeFiglieDiPrimiLivello = New Collection
'    Call CreaListaSuperareeFiglieDiPrimiLivello(idRecordSelezionato, listaSuperareeFiglieDiPrimiLivello)
    listaSuperaree = CreaListaAreeSelezionate()
    
    Set listaTariffe = New Collection
    
''    For i = 1 To listaSuperareeFiglieDiPrimiLivello.count
''        sql = "SELECT T.NOME, T.CODICE, T.IDTARIFFA" & _
''            " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP WHERE" & _
''            " T.IDTARIFFA = PTP.IDTARIFFA AND" & _
''            " T.IDTARIFFA_RIFERIMENTO IS NULL AND" & _
''            " T.IDPRODOTTO = " & idProdottoSelezionato & " AND" & _
''            " PTP.IDAREA = " & listaSuperareeFiglieDiPrimiLivello(i) & _
''            " ORDER BY CODICE"
'        sql = "SELECT T.NOME, T.CODICE, T.IDTARIFFA" & _
'            " FROM TARIFFA T" & _
'            " WHERE T.IDTARIFFA_RIFERIMENTO IS NULL AND" & _
'            " T.IDPRODOTTO = " & idProdottoSelezionato & _
'            " ORDER BY CODICE"

'    bisogna prendere solo le tariffe che hanno prezzi nelle superaree selezionate (lstAreeSelezionate)
        sql = "SELECT T.NOME, T.CODICE, T.IDTARIFFA" & _
            " FROM TARIFFA T, PREZZOTITOLOPRODOTTO PTP," & _
            "(" & _
            " SELECT IDAREA FROM AREA WHERE IDAREA IN (" & listaSuperaree & ")" & _
            " UNION" & _
            " SELECT DISTINCT IDAREA_PADRE FROM AREA WHERE IDAREA IN (" & listaSuperaree & ") AND IDAREA_PADRE IS NOT NULL" & _
            " ) A" & _
            " WHERE T.IDTARIFFA_RIFERIMENTO IS NULL AND" & _
            " T.IDPRODOTTO = " & idProdottoSelezionato & _
            " AND T.IDTARIFFA = PTP.IDTARIFFA" & _
            " AND PTP.IDAREA = A.IDAREA" & _
            " ORDER BY CODICE"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set tariffaCorrente = New clsElementoLista
                Call tariffaCorrente.InizializzaListaSottoElementiLista
                tariffaCorrente.nomeElementoLista = rec("NOME")
                tariffaCorrente.codiceElementoLista = rec("CODICE")
                tariffaCorrente.idElementoLista = rec("IDTARIFFA").Value
'                terminaliAttivi = ""
'                sql1 = "SELECT IDTIPOTERMINALE" & _
'                    " FROM TARIFFA_TIPOTERMINALE" & _
'                    " WHERE IDTARIFFA = " & tariffaCorrente.idElementoLista
'                rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
'                If Not (rec1.EOF And rec1.BOF) Then
'                    rec1.MoveFirst
'                    While Not rec1.EOF
'                        Set tipoTerminaleCorrente = New clsElementoLista
'                        idTTA = rec1("IDTIPOTERMINALE").Value
'                        Select Case idTTA
'                            Case TT_TERMINALE_AVANZATO
'                                codTTA = "TA"
'                            Case TT_TERMINALE_LOTTO
'                                codTTA = "TL"
'                            Case TT_TERMINALE_WEB
'                                codTTA = "TW"
'                            Case TT_TERMINALE_POS
'                                codTTA = "TPOS"
'                            Case TT_TERMINALE_TOTEM
'                                codTTA = "TTOTEM"
'                        End Select
'                        tipoTerminaleCorrente.idElementoLista = idTTA
'                        tipoTerminaleCorrente.codiceElementoLista = codTTA
'                        Call tariffaCorrente.listaSottoElementiLista.Add(tipoTerminaleCorrente)
'                        rec1.MoveNext
'                        terminaliAttivi = IIf(terminaliAttivi = "", codTTA, terminaliAttivi & "; " & codTTA)
'                    Wend
'                End If
'                rec1.Close
'                If terminaliAttivi <> "" Then
'                    terminaliAttivi = "(" & terminaliAttivi & ")"
'                End If
                tariffaCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
                If Not TariffaGiaPresenteInLista(tariffaCorrente.idElementoLista) Then
                    chiaveTariffa = ChiaveId(tariffaCorrente.idElementoLista)
                    Call listaTariffe.Add(tariffaCorrente, chiaveTariffa)
                End If
                rec.MoveNext
            Wend
        End If
        rec.Close
'    Next i
    
    Call ChiudiConnessioneBD
    
    Call lstTariffe_Init
    
End Sub
    
Public Function CreaListaAreeSelezionate() As String
    Dim i As Long
    
    CreaListaAreeSelezionate = "0"
    For i = 0 To lstAreeSelezionate.ListCount - 1
        If lstAreeSelezionate.Selected(i) Then
            CreaListaAreeSelezionate = CreaListaAreeSelezionate & ", " & lstAreeSelezionate.ItemData(i)
        End If
    Next i
End Function

Private Sub lstTariffe_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tar As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTariffe.Clear

    If Not (listaTariffe Is Nothing) Then
        i = 1
        For Each tar In listaTariffe
            lstTariffe.AddItem tar.descrizioneElementoLista
            lstTariffe.ItemData(i - 1) = tar.idElementoLista
            i = i + 1
        Next tar
    End If
           
    internalEvent = internalEventOld

End Sub

Private Function TariffaGiaPresenteInLista(idT As Long) As Boolean
    Dim trovato As Boolean
    Dim tar As clsElementoLista
    
    trovato = False
    For Each tar In listaTariffe
        If tar.idElementoLista = idT Then
            trovato = True
        End If
    Next tar
    TariffaGiaPresenteInLista = trovato
End Function

Private Sub SelezionaDeselezionaTariffa()
    Dim newTariffa As clsAssocTariffa
    Dim tariffaSelezionata As clsElementoLista
    
    idTariffaSelezionata = lstTariffe.ItemData(lstTariffe.ListIndex)
    Set tariffaSelezionata = listaTariffe.Item(ChiaveId(idTariffaSelezionata))
    If lstTariffe.Selected(lstTariffe.ListIndex) Then
        Set newTariffa = New clsAssocTariffa
        newTariffa.idTariffa = idTariffaSelezionata
        newTariffa.labelTariffa = tariffaSelezionata.descrizioneElementoLista
        Call areaCommit.AggiungiTariffaAdArea(newTariffa)
    Else
        Call areaCommit.OttieniTariffaDaIdTariffa(idTariffaSelezionata, newTariffa)
        Call areaCommit.EliminaTariffaDaArea(idTariffaSelezionata)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaValoriLstTipiSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveTipoSupporto As String
    Dim tipoSupportoCorrente As clsElementoLista
    
    Call ApriConnessioneBD
    
    Set listaTipiSupporto = New Collection
    
    If idClasseProdottoSelezionata = CPR_MEDIUM Then
        sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.NOME, TS.CODICE" & _
            " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
            " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND TS.ATTIVO = 1" & _
            " AND IDTIPOSUPPORTOSIAE IN (6,8)" & _
            " ORDER BY CODICE"
    Else
        sql = "SELECT DISTINCT TS.IDTIPOSUPPORTO, TS.NOME, TS.CODICE" & _
            " FROM TIPOSUPPORTO TS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE TS.IDTIPOSUPPORTO = OTL.IDTIPOSUPPORTO" & _
            " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND TS.ATTIVO = 1" & _
            " AND IDTIPOSUPPORTOSIAE NOT IN (6,8)" & _
            " ORDER BY CODICE"
    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            Set tipoSupportoCorrente = New clsElementoLista
            tipoSupportoCorrente.nomeElementoLista = rec("NOME")
            tipoSupportoCorrente.codiceElementoLista = rec("CODICE")
            tipoSupportoCorrente.idElementoLista = rec("IDTIPOSUPPORTO").Value
            tipoSupportoCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
            chiaveTipoSupporto = ChiaveId(tipoSupportoCorrente.idElementoLista)
            Call listaTipiSupporto.Add(tipoSupportoCorrente, chiaveTipoSupporto)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
    Call lstTipiSupporto_Init
    
End Sub

Private Sub lstTipiSupporto_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim tip As clsElementoLista

    internalEventOld = internalEvent
    internalEvent = True
    
    lstTipiSupporto.Clear

    If Not (listaTipiSupporto Is Nothing) Then
        i = 1
        For Each tip In listaTipiSupporto
            lstTipiSupporto.AddItem tip.descrizioneElementoLista
            lstTipiSupporto.ItemData(i - 1) = tip.idElementoLista
            i = i + 1
        Next tip
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub SelezionaDeselezionaTipoSupporto()
    Dim newTipoSupporto As clsAssocSupporto
    Dim tipoSupportoSelezionato As clsElementoLista
    
    idTipoSupportoSelezionato = lstTipiSupporto.ItemData(lstTipiSupporto.ListIndex)
    Set tipoSupportoSelezionato = listaTipiSupporto.Item(ChiaveId(idTipoSupportoSelezionato))
    If lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) Then
        If RilevaLayoutAssociatoATipo = True Then
            Set newTipoSupporto = New clsAssocSupporto
            newTipoSupporto.idTipoSupporto = idTipoSupportoSelezionato
            newTipoSupporto.labelTipoSupporto = tipoSupportoSelezionato.descrizioneElementoLista
            newTipoSupporto.idLayoutSupporto = frmDettagliLayoutSupporto.GetIdLayoutSupportoSelezionato
            newTipoSupporto.labelLayoutSupporto = frmDettagliLayoutSupporto.GetLabelLayoutSupportoSelezionato
            Call areaCommit.AggiungiTipoSupportoAdArea(newTipoSupporto)
'            Call AggiungiLabelLayoutATipoSupporto
            lstTipiSupporto.List(lstTipiSupporto.ListIndex) = newTipoSupporto.labelTipoSupporto & " / " & newTipoSupporto.labelLayoutSupporto
'            lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) = True
        Else
            lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) = False
        End If
    Else
'        Call RimuoviLabelLayoutATipoSupporto
        Call areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipoSupportoSelezionato, newTipoSupporto)
        lstTipiSupporto.List(lstTipiSupporto.ListIndex) = newTipoSupporto.labelTipoSupporto
'        lstTipiSupporto.Selected(lstTipiSupporto.ListIndex) = False
'        Call areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipoSupportoSelezionato, newTipoSupporto)
        Call areaCommit.EliminaTipoSupportoDaArea(idTipoSupportoSelezionato)
    End If
    Call AggiornaAbilitazioneControlli
End Sub

'Private Sub AggiungiLabelLayoutATipoSupporto() 'da usare quando si conferma il layout da combo
'    Dim i As Integer
'    Dim idTipo As Long
'    Dim tipoSupportoCorrente As clsAssocSupporto
'    Dim t As clsElementoLista
'
'    i = 1
'    For Each t In listaTipiSupporto
'        idTipo = t.idElementoLista
'        If areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipo, tipoSupportoCorrente) Then
'            Call lstTipiSupporto.RemoveItem(i - 1)
'            Call lstTipiSupporto.AddItem(tipoSupportoCorrente.labelTipoSupporto & " / " & tipoSupportoCorrente.labelLayoutSupporto, i - 1)
'            lstTipiSupporto.ItemData(i - 1) = tipoSupportoCorrente.idTipoSupporto
'            lstTipiSupporto.Selected(i - 1) = True
''            i = i + 1
'        End If
'        i = i + 1
'    Next t
'End Sub

'Private Sub RimuoviLabelLayoutATipoSupporto() 'da usare quando si deseleziona un tiposupporto
'    Dim i As Integer
'    Dim idTipo As Long
'    Dim tipoSupportoCorrente As clsAssocSupporto
'    Dim t As clsElementoLista
'
'    i = 1
'    For Each t In listaTipiSupporto
'        idTipo = t.idElementoLista
'        If areaCommit.OttieniTipoSupportoDaIdTipoSupporto(idTipo, tipoSupportoCorrente) Then
'            Call lstTipiSupporto.RemoveItem(i - 1)
'            Call lstTipiSupporto.AddItem(tipoSupportoCorrente.labelTipoSupporto, i - 1)
'            lstTipiSupporto.ItemData(i - 1) = tipoSupportoCorrente.idTipoSupporto
'            lstTipiSupporto.Selected(i - 1) = False
''            i = i + 1
'        End If
'        i = i + 1
'    Next t
'End Sub

Private Sub CaricaValoriLayoutSupporto()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim chiaveLayoutSupporto As String
    Dim layoutSupportoCorrente As clsElementoLista
    
    Set listaLayoutSupporto = New Collection
    
    If idTipoSupportoSelezionato <> idNessunElementoSelezionato Then
    
        Call ApriConnessioneBD
        sql = "SELECT DISTINCT LS.IDLAYOUTSUPPORTO, LS.NOME, LS.CODICE " & _
            " FROM LAYOUTSUPPORTO LS, ORGANIZ_TIPOSUP_LAYOUTSUP OTL" & _
            " WHERE LS.IDLAYOUTSUPPORTO = OTL.IDLAYOUTSUPPORTO" & _
            " AND OTL.IDORGANIZZAZIONE = " & idOrganizzazioneSelezionata & _
            " AND OTL.IDTIPOSUPPORTO = " & idTipoSupportoSelezionato & _
            " ORDER BY CODICE"
        rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
        If Not (rec.BOF And rec.EOF) Then
            rec.MoveFirst
            While Not rec.EOF
                Set layoutSupportoCorrente = New clsElementoLista
                layoutSupportoCorrente.nomeElementoLista = rec("NOME")
                layoutSupportoCorrente.codiceElementoLista = rec("CODICE")
                layoutSupportoCorrente.idElementoLista = rec("IDLAYOUTSUPPORTO").Value
                layoutSupportoCorrente.descrizioneElementoLista = rec("CODICE") & " - " & rec("NOME")
                chiaveLayoutSupporto = ChiaveId(layoutSupportoCorrente.idElementoLista)
                Call listaLayoutSupporto.Add(layoutSupportoCorrente, chiaveLayoutSupporto)
                rec.MoveNext
            Wend
        End If
        rec.Close
        
        Call ChiudiConnessioneBD
    End If
    
End Sub

Private Sub CaricaValoriComboDaLista(cmb As ComboBox, listaElementi As Collection)
    Dim i As Integer
    Dim e As clsElementoLista
    
    i = 1
    For Each e In listaElementi
        cmb.AddItem e.descrizioneElementoLista
        cmb.ItemData(i - 1) = e.idElementoLista
        i = i + 1
    Next e
            
End Sub

Private Sub EliminaAssociazioniInBD(idA As Long)
    Dim t As TreeView
    Dim nodoCorrente As Node
    Dim nodoFiglio As Node
    Dim chiaveArea As String
    Dim i As Integer
    Dim sql As String
    Dim elencoIdTariffe As String
    Dim tar As clsElementoLista
    Dim n As Long
    Dim condizioniSQL As String
    
    elencoIdTariffe = ""
    For Each tar In listaTariffe
        elencoIdTariffe = IIf(elencoIdTariffe = "", tar.idElementoLista, elencoIdTariffe & ", " & tar.idElementoLista)
    Next tar
    chiaveArea = ChiaveId(idA)
    Set t = tvwAreePianta
    Set nodoCorrente = t.Nodes(chiaveArea)
    Set nodoFiglio = nodoCorrente.Child
    If listaTariffe.count > 0 Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
''            condizioniSql = " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
''                " AND IDTIPOSTATORECORD <> " & TSR_NUOVO
''            Call AggiornaParametriSessioneSuRecord("UTILIZZOLAYOUTSUPPORTOCPV", "IDAREA", idA, TSR_ELIMINATO, condizioniSql)
''            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDAREA = " & idA & _
''                " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
''                " AND IDTIPOSTATORECORD <> " & TSR_NUOVO
''            SETAConnection.Execute sql, n, adCmdText
'            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDAREA = " & idA & _
'                " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
'                " AND IDTIPOSTATORECORD = " & TSR_NUOVO
'            SETAConnection.Execute sql, n, adCmdText
'            condizioniSQL = " AND IDTARIFFA IN (" & elencoIdTariffe & ")" & _
'                " AND IDTIPOSTATORECORD <> " & TSR_NUOVO
'            Call AggiornaParametriSessioneSuRecord("UTILIZZOLAYOUTSUPPORTOCPV", "IDAREA", idA, TSR_ELIMINATO, condizioniSQL)
'        Else
            sql = "DELETE FROM UTILIZZOLAYOUTSUPPORTOCPV WHERE IDAREA = " & idA & _
                " AND IDTARIFFA IN (" & elencoIdTariffe & ")"
            SETAConnection.Execute sql, n, adCmdText
'       End If
        nodoCorrente.Image = IAT_MANCANTE
        DoEvents
        If Not nodoFiglio Is Nothing Then
            For i = 1 To nodoCorrente.Children
                Call EliminaAssociazioniInBD(IdChiave(nodoFiglio.Key))
                Set nodoFiglio = nodoFiglio.Next
            Next i
        End If
    End If
End Sub







