VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmConfigurazioneProdottoSistemiDiEmissione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prodotto"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11955
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8700
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkUsataDalProdotto 
      Caption         =   "Usata dal prodotto"
      Height          =   255
      Left            =   8280
      TabIndex        =   21
      Top             =   5160
      Width           =   2475
   End
   Begin VB.TextBox txtInfo2 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   8460
      TabIndex        =   12
      Top             =   240
      Width           =   1635
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   10200
      TabIndex        =   11
      Top             =   240
      Width           =   1635
   End
   Begin VB.Frame fraExitCode 
      Height          =   915
      Left            =   120
      TabIndex        =   8
      Top             =   7620
      Width           =   2775
      Begin VB.CommandButton cmdAnnulla 
         Caption         =   "Annulla"
         Height          =   435
         Left            =   1440
         TabIndex        =   10
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdConferma 
         Caption         =   "Conferma"
         Height          =   435
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraNavigazioneProdotto 
      Height          =   915
      Left            =   7800
      TabIndex        =   4
      Top             =   7620
      Width           =   4035
      Begin VB.CommandButton cmdEsci 
         Caption         =   "Abbandona"
         Height          =   435
         Left            =   2700
         TabIndex        =   7
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrecedente 
         Caption         =   "< Precedente"
         Height          =   435
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1155
      End
      Begin VB.CommandButton cmdSuccessivo 
         Caption         =   "Successivo >"
         Height          =   435
         Left            =   1440
         TabIndex        =   5
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame fraAzioniSuGriglia 
      Height          =   915
      Left            =   120
      TabIndex        =   2
      Top             =   3840
      Width           =   1515
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Modifica"
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.TextBox txtNome 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      MaxLength       =   30
      TabIndex        =   1
      Top             =   5160
      Width           =   3315
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   3540
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   5160
      Width           =   4635
   End
   Begin MSAdodcLib.Adodc adcConfigurazioneProdottoSistemiDiEmissione 
      Height          =   330
      Left            =   6360
      Top             =   180
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dgrConfigurazioneProdottoSistemiDiEmissione 
      Height          =   2895
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   5106
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Titolo DataGrid"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo2 
      Caption         =   "lblInfo2"
      Height          =   255
      Left            =   8460
      TabIndex        =   20
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblInfo1 
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   10200
      TabIndex        =   19
      Top             =   0
      Width           =   1635
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione delle Modalitā di Emissione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Width           =   5835
   End
   Begin VB.Label lblOperazioneInCorso 
      Caption         =   "Operazione in corso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   3600
      Width           =   1815
   End
   Begin VB.Label lblOperazione 
      Caption         =   "acme"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   16
      Top             =   3600
      Width           =   2775
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   4920
      Width           =   1695
   End
   Begin VB.Label lblDescrizione 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   3540
      TabIndex        =   14
      Top             =   4920
      Width           =   1575
   End
End
Attribute VB_Name = "frmConfigurazioneProdottoSistemiDiEmissione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private internalEvent As Boolean

Private idRecordSelezionato As Long
Private idProdottoSelezionato As Long
Private idOrganizzazioneSelezionata As Long
Private idPiantaSelezionata As Long
Private descrizioneRecordSelezionato As String
Private nomeRecordSelezionato As String
Private nomeProdottoSelezionato As String
Private nomeOrganizzazioneSelezionata As String
Private usatoDalProdotto As ValoreBooleanoEnum
Private idStagioneSelezionata As Long
Private isProdottoAttivoSuTL As Boolean
Private isRecordEditabile As Boolean
Private idClasseProdottoSelezionata As ClasseProdottoEnum
Private rateo As Long
Private esisteUnaModalitāEmissione As Boolean

Private tipoStatoRecordSelezionato As TipoStatoRecordEnum
Private gestioneExitCode As ExitCodeEnum
Private tipoTariffa As TipoOrigineTariffaEnum
Private modalitāFormCorrente As AzioneEnum
Private gestioneRecordGriglia As AzioneSuGrigliaEnum
Private statoNavigazione As TastiNavigazioneConfigurazioneProdottoEnum

Private rientraInDecretoSicurezza As ValoreBooleanoEnum
Private numeroMaxTitoliPerAcquirente As Long

Private Sub AggiornaAbilitazioneControlli()
    
    lblInfo1.Caption = "Prodotto"
    txtInfo1.Text = nomeProdottoSelezionato
    txtInfo1.Enabled = False
    lblInfo2.Caption = "Organizzazione"
    txtInfo2.Text = nomeOrganizzazioneSelezionata
    txtInfo2.Enabled = False
    
    dgrConfigurazioneProdottoSistemiDiEmissione.Caption = "MODALITA' DI EMISSIONE CONFIGURATE"
    
    txtNome.Enabled = False
    txtDescrizione.Enabled = False
    lblNome.Enabled = False
    lblDescrizione.Enabled = False
    
    If (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoSistemiDiEmissione.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        chkUsataDalProdotto.Value = vbUnchecked
        chkUsataDalProdotto.Enabled = False
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    ElseIf (gestioneExitCode = EC_NON_SPECIFICATO And _
            gestioneRecordGriglia <> ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoSistemiDiEmissione.Enabled = False
        chkUsataDalProdotto.Enabled = (gestioneRecordGriglia <> ASG_ELIMINA)
        cmdModifica.Enabled = False
        cmdConferma.Enabled = (Trim(txtNome.Text) <> "")
        cmdAnnulla.Enabled = True
        lblOperazioneInCorso.Caption = "Operazione in corso:"
        Select Case gestioneRecordGriglia
            Case ASG_INSERISCI_NUOVO
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_INSERISCI_DA_SELEZIONE
                lblOperazione.Caption = "inserimento nuovo record"
            Case ASG_MODIFICA
                lblOperazione.Caption = "modifica record selezionato"
            Case ASG_ELIMINA
                lblOperazione.Caption = "eliminazione record selezionato"
            Case Else
                'Do Nothing
        End Select
        
    ElseIf (gestioneExitCode <> EC_NON_SPECIFICATO And _
            gestioneRecordGriglia = ASG_NON_SPECIFICATO) Then
        dgrConfigurazioneProdottoSistemiDiEmissione.Enabled = True
        lblOperazioneInCorso.Caption = ""
        lblOperazione.Caption = ""
        txtNome.Text = ""
        txtDescrizione.Text = ""
        chkUsataDalProdotto = vbUnchecked
        chkUsataDalProdotto.Enabled = False
        cmdModifica.Enabled = (idRecordSelezionato <> idNessunElementoSelezionato)
        cmdConferma.Enabled = False
        cmdAnnulla.Enabled = False
        
    Else
        'Do Nothing
    End If
    
    Select Case modalitāFormCorrente
        Case A_NUOVO
            cmdEsci.Caption = "Abbandona"
            cmdSuccessivo.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
            cmdPrecedente.Enabled = (gestioneExitCode <> EC_NON_SPECIFICATO Or _
                                    (gestioneExitCode = EC_NON_SPECIFICATO And _
                                    gestioneRecordGriglia = ASG_NON_SPECIFICATO))
        Case A_MODIFICA
            cmdPrecedente.Visible = False
            cmdSuccessivo.Visible = False
            cmdEsci.Caption = "Esci"
        Case A_CLONA
            'Do Nothing
        Case A_ELIMINA
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
    
End Sub

Public Sub SetIdProdottoSelezionato(id As Long)
    idProdottoSelezionato = id
End Sub

Public Sub SetIdStagioneSelezionata(idS As Long)
    idStagioneSelezionata = idS
End Sub

Public Sub SetRateo(rt As Long)
    rateo = rt
End Sub

Public Sub SetIdClasseProdottoSelezionata(idC As ClasseProdottoEnum)
    idClasseProdottoSelezionata = idC
End Sub

Public Sub SetNomeProdottoSelezionato(nome As String)
    nomeProdottoSelezionato = nome
End Sub

Public Sub SetIdOrganizzazioneSelezionata(id As Long)
    idOrganizzazioneSelezionata = id
End Sub

Public Sub SetNomeOrganizzazioneSelezionata(nome As String)
    nomeOrganizzazioneSelezionata = nome
End Sub

Private Sub Successivo()
    If NumeroModalitāEmissioneDefinite > 0 Then
        If idClasseProdottoSelezionata = CPR_ABBONAMENTO_A_TURNO_LIBERO_E_POSTO_LIBERO Then
            Call CaricaFormConfigurazioneProdottoSceltaRappresentazione
        Else
            Call CaricaFormConfigurazioneProdottoRappresentazioni
        End If
    Else
        Call frmMessaggio.Visualizza("AvvertimentoModalitāEmissioneMancante")
    End If
End Sub

Private Sub CaricaFormConfigurazioneProdottoRappresentazioni()
    Call frmConfigurazioneProdottoRappresentazioni.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoRappresentazioni.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoRappresentazioni.SetRateo(rateo)
    Call frmConfigurazioneProdottoRappresentazioni.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoRappresentazioni.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoRappresentazioni.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoRappresentazioni.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoRappresentazioni.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoRappresentazioni.Init
End Sub

Private Sub CaricaFormConfigurazioneProdottoSceltaRappresentazione()
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdProdottoSelezionato(idProdottoSelezionato)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetNomeProdottoSelezionato(nomeProdottoSelezionato)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdOrganizzazioneSelezionata(idOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetNomeOrganizzazioneSelezionata(nomeOrganizzazioneSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdPiantaSelezionata(idPiantaSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdStagioneSelezionata(idStagioneSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIdClasseProdottoSelezionata(idClasseProdottoSelezionata)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetRateo(rateo)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetIsProdottoAttivoSuTL(isProdottoAttivoSuTL)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetProdottoRientraDecretoSicurezza(rientraInDecretoSicurezza)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetNumeroMassimoTitoliPerAcqProdotto(numeroMaxTitoliPerAcquirente)
    
    Call frmConfigurazioneProdottoScelteRappresentazione.SetModalitāForm(A_NUOVO)
    Call frmConfigurazioneProdottoScelteRappresentazione.SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call frmConfigurazioneProdottoScelteRappresentazione.Init
End Sub

Private Sub Abbandona()
    Call frmMessaggio.Visualizza("ConfermaAbbandonoPercorsoGuidato")
    If frmMessaggio.exitCode = EC_CONFERMA Then
        Call EliminaProdottoDallaBaseDati(idProdottoSelezionato)
        Call AzionePercorsoGuidato(TNCP_ABBANDONA)
    End If
End Sub

Private Sub Precedente()
    Unload Me
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Annulla
    
    MousePointer = mousePointerOld
End Sub

Private Sub Annulla()
    Call SetGestioneExitCode(EC_ANNULLA)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdConferma_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Conferma
    
    MousePointer = mousePointerOld
End Sub

Private Sub Conferma()
    Dim causaNonEditabilita As String
    Dim stringaNota As String
    Dim isConfigurabile As Boolean

    causaNonEditabilita = ""
    stringaNota = "IDPRODOTTO = " & idProdottoSelezionato & _
        "; IDMODALITAEMISSIONE = " & idRecordSelezionato
    If IsProdottoEditabile(idProdottoSelezionato, causaNonEditabilita) Then
        isConfigurabile = True
        If tipoStatoProdotto = TSP_ATTIVO Then
            Call frmMessaggio.Visualizza("ConfermaEditabilitāProdottoAttivo")
            If frmMessaggio.exitCode <> EC_CONFERMA Then
                isConfigurabile = False
            End If
        End If
        If isConfigurabile Then
            Call SetGestioneExitCode(EC_CONFERMA)
            Call AggiornaAbilitazioneControlli
            
            If valoriCampiOK Then
                If isRecordEditabile Then
                    Select Case gestioneRecordGriglia
                        Case ASG_MODIFICA
                            Call AggiornaNellaBaseDati
                            Call ScriviLog(CCTA_MODIFICA, CCDA_PRODOTTO, CCDA_MODALITA_EMISSIONE, stringaNota, idProdottoSelezionato)
                            Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
                            Call adcConfigurazioneProdottoSistemiDiEmissione_Init
                            Call SelezionaElementoSuGriglia(idRecordSelezionato)
                            Call dgrConfigurazioneProdottoSistemiDiEmissione_Init
                    End Select
                End If
                Call AggiornaAbilitazioneControlli
            Else
                Call frmMessaggio.Visualizza("ErroreEliminazioneModalitāEmissione")
            End If
        End If
    Else
        Call frmMessaggio.Visualizza("NotificaNonEditabilitāCampi", causaNonEditabilita)
    End If
End Sub

Public Sub SetGestioneExitCode(ec As ExitCodeEnum)
    gestioneExitCode = ec
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub Esci()
    Select Case modalitāFormCorrente
        Case A_NUOVO
            Call Abbandona
        Case A_MODIFICA
            Unload Me
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetGestioneRecordGriglia(asg As AzioneSuGrigliaEnum)
    gestioneRecordGriglia = asg
End Sub

Public Sub SetTipoTariffa(tipo As TipoOrigineTariffaEnum)
    tipoTariffa = tipo
End Sub

Private Sub SelezionaElementoSuGriglia(id As Long)
    Dim rec As ADODB.Recordset
    Dim internalEventOld As Boolean
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Set rec = adcConfigurazioneProdottoSistemiDiEmissione.Recordset
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            If id = rec("ID") Then
                Exit Do
            End If
            rec.MoveNext
        Loop
    End If
    internalEvent = internalEventOld
    
End Sub

Private Sub cmdModifica_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Modifica
    
    MousePointer = mousePointerOld
End Sub

Private Sub Modifica()
    Call SetGestioneRecordGriglia(ASG_MODIFICA)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call AggiornaAbilitazioneControlli
    Call GetIdRecordSelezionato
    Call CaricaDallaBaseDati
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdPrecedente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Precedente
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSuccessivo_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Successivo
    
    MousePointer = mousePointerOld
End Sub

Private Sub dgrConfigurazioneProdottoSistemiDiEmissione_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    If Not internalEvent Then
        Call GetIdRecordSelezionato
    End If
End Sub

Public Sub Init()

    Call SetIdRecordSelezionato(idNessunElementoSelezionato)
    Call SetGestioneExitCode(EC_NON_SPECIFICATO)
    Call SetGestioneRecordGriglia(ASG_NON_SPECIFICATO)
    Call adcConfigurazioneProdottoSistemiDiEmissione_Init
    Call SelezionaElementoSuGriglia(idNessunElementoSelezionato)
    Call dgrConfigurazioneProdottoSistemiDiEmissione_Init
    Call Me.Show(vbModal)

End Sub

Private Sub GetIdRecordSelezionato()
    Dim rec As ADODB.Recordset
    
    Set rec = adcConfigurazioneProdottoSistemiDiEmissione.Recordset
    If Not (rec.BOF) Then
        If rec.EOF Then
            rec.MoveFirst
        End If
        idRecordSelezionato = rec("ID").Value
    Else
        idRecordSelezionato = idNessunElementoSelezionato
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Public Sub SetIdRecordSelezionato(id As Long)
    idRecordSelezionato = id
End Sub

Private Sub adcConfigurazioneProdottoSistemiDiEmissione_Init()
    Dim internalEventOld As Boolean
    Dim d As Adodc
    Dim sql As String
    
    internalEventOld = internalEvent
    internalEvent = True
        
    Set d = adcConfigurazioneProdottoSistemiDiEmissione
    
    sql = "SELECT M.IDMODALITAEMISSIONE ID," & _
        " M.NOME ""Nome""," & _
        " M.DESCRIZIONE ""Descrizione""," & _
        " DECODE(P.IDPRODOTTO, " & idProdottoSelezionato & ", 'SI', 'NO') ""Usata dal Prodotto""" & _
        " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P, CLASSEPRODOTTO_MODALITAEMISS C" & _
        " WHERE M.IDMODALITAEMISSIONE = P.IDMODALITAEMISSIONE(+)" & _
        " AND P.IDPRODOTTO(+) = " & idProdottoSelezionato & _
        " AND M.IDMODALITAEMISSIONE > 0" & _
        " AND M.IDMODALITAEMISSIONE = C.IDMODALITAEMISSIONE" & _
        " AND C.IDCLASSEPRODOTTO = " & idClasseProdottoSelezionata & _
        " ORDER BY ""Nome"""
    d.ConnectionString = StringaDiConnessione
    d.RecordSource = sql
    d.Refresh
    
    Set dgrConfigurazioneProdottoSistemiDiEmissione.dataSource = d
        
    internalEvent = internalEventOld
    
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CaricaDallaBaseDati()
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim idSessione As Long
    
    Call ApriConnessioneBD

    isRecordEditabile = True
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT M.IDMODALITAEMISSIONE ID," & _
'            " M.NOME," & _
'            " M.DESCRIZIONE," & _
'            " IDTIPOSTATORECORD, IDSESSIONECONFIGURAZIONE," & _
'            " DECODE(P.IDPRODOTTO, " & idProdottoSelezionato & ", 1, 0) USATO" & _
'            " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P" & _
'            " WHERE M.IDMODALITAEMISSIONE = P.IDMODALITAEMISSIONE(+)" & _
'            " AND P.IDPRODOTTO(+) = " & idProdottoSelezionato & _
'            " AND M.IDMODALITAEMISSIONE = " & idRecordSelezionato
'    Else
        sql = "SELECT M.IDMODALITAEMISSIONE ID," & _
            " M.NOME," & _
            " M.DESCRIZIONE," & _
            " DECODE(P.IDPRODOTTO, " & idProdottoSelezionato & ", 1, 0) USATO" & _
            " FROM MODALITAEMISSIONE M, PRODOTTO_MODALITAEMISSIONE P" & _
            " WHERE M.IDMODALITAEMISSIONE = P.IDMODALITAEMISSIONE(+)" & _
            " AND P.IDPRODOTTO(+) = " & idProdottoSelezionato & _
            " AND M.IDMODALITAEMISSIONE = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        nomeRecordSelezionato = rec("NOME")
        descrizioneRecordSelezionato = IIf(IsNull(rec("DESCRIZIONE")), "", rec("DESCRIZIONE"))
        usatoDalProdotto = rec("USATO")
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            tipoStatoRecordSelezionato = IIf(IsNull(rec("IDTIPOSTATORECORD")), TSR_NON_SPECIFICATO, rec("IDTIPOSTATORECORD").Value)
'            idSessione = IIf(IsNull(rec("IDSESSIONECONFIGURAZIONE")), idNessunElementoSelezionato, rec("IDSESSIONECONFIGURAZIONE").Value)
'            isRecordEditabile = (idSessione = idNessunElementoSelezionato Or _
'                idSessione = idSessioneConfigurazioneCorrente)
'        End If
    End If
    rec.Close
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub AssegnaValoriCampi()
    Dim internalEventOld As Boolean
    Dim i As Integer

    internalEventOld = internalEvent
    internalEvent = True

    txtNome.Text = ""
    txtDescrizione.Text = ""
    txtNome.Text = nomeRecordSelezionato
    txtDescrizione.Text = descrizioneRecordSelezionato
    chkUsataDalProdotto.Value = usatoDalProdotto
        
    internalEvent = internalEventOld

End Sub

Private Sub dgrConfigurazioneProdottoSistemiDiEmissione_Init()
    Dim g As DataGrid
    Dim dimensioneGrid As Long
    Dim numeroCampi As Integer
    
    Set g = dgrConfigurazioneProdottoSistemiDiEmissione
    g.ScrollBars = dbgBoth
    dimensioneGrid = g.Width - 100
    numeroCampi = 3
    g.Columns(0).Visible = False
    g.Columns(1).Width = (dimensioneGrid / numeroCampi)
    g.Columns(2).Width = (dimensioneGrid / numeroCampi)
    g.Columns(3).Width = (dimensioneGrid / numeroCampi)
        
    g.MarqueeStyle = dbgHighlightRow
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Function valoriCampiOK() As Boolean

    valoriCampiOK = True
    nomeRecordSelezionato = Trim(txtNome.Text)
    descrizioneRecordSelezionato = Trim(txtDescrizione.Text)
    usatoDalProdotto = chkUsataDalProdotto.Value
    If EsisteTariffaConModalitāEmissioneSelezionata And _
        usatoDalProdotto = VB_FALSO Then
        valoriCampiOK = False
    End If
    
End Function

Private Function EsisteTariffaConModalitāEmissioneSelezionata() As Boolean
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Long
    
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT COUNT(DISTINCT IDTARIFFA) CONT" & _
'            " FROM TARIFFA" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND IDMODALITAEMISSIONE = " & idRecordSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL" & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT COUNT(DISTINCT IDTARIFFA) CONT" & _
            " FROM TARIFFA" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDMODALITAEMISSIONE = " & idRecordSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT").Value
    End If
    rec.Close
    EsisteTariffaConModalitāEmissioneSelezionata = (cont > 0)
End Function

Public Sub SetModalitāForm(mf As AzioneEnum)
    modalitāFormCorrente = mf
End Sub

Public Sub SetStatoNavigazione(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
End Sub

Public Sub AzionePercorsoGuidato(stato As TastiNavigazioneConfigurazioneProdottoEnum)
    statoNavigazione = stato
    Select Case statoNavigazione
        Case TNCP_ABBANDONA
            Unload Me
            Call frmConfigurazioneProdottoDurateRiservazioni.AzionePercorsoGuidato(TNCP_ABBANDONA)
        Case TNCP_FINE
            Unload Me
            Call frmConfigurazioneProdottoDurateRiservazioni.AzionePercorsoGuidato(TNCP_FINE)
        Case TNCP_PRECEDENTE
            'Do Nothing
        Case Else
            'Do Nothing
    End Select
End Sub

Public Sub SetIdPiantaSelezionata(id As Long)
    idPiantaSelezionata = id
End Sub

Public Sub SetIsProdottoAttivoSuTL(tf As Boolean)
    isProdottoAttivoSuTL = tf
End Sub

Public Sub SetProdottoRientraDecretoSicurezza(v As ValoreBooleanoEnum)
    rientraInDecretoSicurezza = v
End Sub

Public Sub SetNumeroMassimoTitoliPerAcqProdotto(n As Long)
    numeroMaxTitoliPerAcquirente = n
End Sub

'Private Sub AggiornaNellaBaseDati_OLD()
'    Dim sql1 As String
'    Dim sql As String
'    Dim rec As New ADODB.Recordset
'    Dim n As Long
'    Dim condizioniSql As String
'    Dim idModalitā As Long
'
'    Call ApriConnessioneBD
'
'On Error GoTo gestioneErrori
'
'    SETAConnection.BeginTrans
''   AGGIORNAMENTO TABELLA TARIFFA_TIPOTERMINALE
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        Call AggiornaParametriSessioneSuRecord("PRODOTTO_MODALITAEMISSIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO)
'    Else
'        sql = "DELETE FROM PRODOTTO_MODALITAEMISSIONE WHERE IDPRODOTTO = " & idProdottoSelezionato
'        SETAConnection.Execute sql, n, adCmdText
'    End If
'    sql = "SELECT M.IDMODALITAEMISSIONE ID, NOME, DESCRIZIONE" & _
'        " FROM MODALITAEMISSIONE M, CLASSEPRODOTTO_MODALITAEMISS C" & _
'        " WHERE M.IDMODALITAEMISSIONE > 0" & _
'        " AND M.IDMODALITAEMISSIONE = C.IDMODALITAEMISSIONE" & _
'        " AND C.IDCLASSEPRODOTTO = " & idClasseProdottoSelezionata
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    If Not (rec.EOF And rec.BOF) Then
'        rec.MoveFirst
'        While Not rec.EOF
'            idModalitā = rec("ID").Value
'            If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'                sql1 = "INSERT INTO PRODOTTO_MODALITAEMISSIONE" & _
'                    " (IDPRODOTTO, IDMODALITAEMISSIONE)" & _
'                    " (SELECT " & _
'                    idProdottoSelezionato & " IDPRODOTTO, " & _
'                    idRecordSelezionato & " IDMODALITAEMISSIONE" & _
'                    " FROM DUAL" & _
'                    " MINUS" & _
'                    " SELECT IDPRODOTTO, IDMODALITAEMISSIONE" & _
'                    " FROM PRODOTTO_MODALITAEMISSIONE" & _
'                    " WHERE IDMODALITAEMISSIONE = " & idRecordSelezionato & _
'                    " AND IDPRODOTTO = " & idProdottoSelezionato & ")"
'                SETAConnection.Execute sql1, n, adCmdText
'                condizioniSql = " AND IDPRODOTTO = " & idProdottoSelezionato
'                Call AggiornaParametriSessioneSuRecord("PRODOTTO_MODALITAEMISSIONE", "IDMODALITAEMISSIONE", idRecordSelezionato, TSR_NUOVO, condizioniSql)
'            Else
'                sql1 = "INSERT INTO PRODOTTO_MODALITAEMISSIONE" & _
'                    " (IDPRODOTTO, IDMODALITAEMISSIONE)" & _
'                    " VALUES " & _
'                    "(" & idProdottoSelezionato & ", " & _
'                    idModalitā & ")"
'                SETAConnection.Execute sql1, n, adCmdText
'            End If
'            rec.MoveNext
'        Wend
'    End If
'    rec.Close
'    SETAConnection.CommitTrans
'
'    Call ChiudiConnessioneBD
'
'    Exit Sub
'
'gestioneErrori:
'    SETAConnection.RollbackTrans
'    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
'
'End Sub
'
Private Function NumeroModalitāEmissioneDefinite() As Integer
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim cont As Integer
    
    cont = 0
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        sql = "SELECT COUNT(DISTINCT IDMODALITAEMISSIONE) CONT" & _
'            " FROM PRODOTTO_MODALITAEMISSIONE" & _
'            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
'            " AND (IDTIPOSTATORECORD IS NULL " & _
'            " OR IDTIPOSTATORECORD <> " & TSR_ELIMINATO & ")"
'    Else
        sql = "SELECT COUNT(DISTINCT IDMODALITAEMISSIONE) CONT" & _
            " FROM PRODOTTO_MODALITAEMISSIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato
'    End If
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.EOF And rec.BOF) Then
        rec.MoveFirst
        cont = rec("CONT")
    End If
    rec.Close
    NumeroModalitāEmissioneDefinite = cont
End Function

Private Sub AggiornaNellaBaseDati()
    Dim sql1 As String
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim n As Long
    Dim condizioniSQL As String
 
    Call ApriConnessioneBD
    
On Error GoTo gestioneErrori

    SETAConnection.BeginTrans
'    If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'        condizioniSQL = " AND IDMODALITAEMISSIONE = " & idRecordSelezionato
'        Call AggiornaParametriSessioneSuRecord("PRODOTTO_MODALITAEMISSIONE", "IDPRODOTTO", idProdottoSelezionato, TSR_ELIMINATO, condizioniSQL)
'    Else
        sql = "DELETE FROM PRODOTTO_MODALITAEMISSIONE" & _
            " WHERE IDPRODOTTO = " & idProdottoSelezionato & _
            " AND IDMODALITAEMISSIONE = " & idRecordSelezionato
        SETAConnection.Execute sql, n, adCmdText
'    End If
    If usatoDalProdotto = VB_VERO Then
'        If tipoModalitāConfigurazione = TMC_TRANSAZIONALE Then
'            sql1 = "INSERT INTO PRODOTTO_MODALITAEMISSIONE" & _
'                " (IDPRODOTTO, IDMODALITAEMISSIONE)" & _
'                " (SELECT " & _
'                idProdottoSelezionato & " IDPRODOTTO, " & _
'                idRecordSelezionato & " IDMODALITAEMISSIONE" & _
'                " FROM DUAL" & _
'                " MINUS" & _
'                " SELECT IDPRODOTTO, IDMODALITAEMISSIONE" & _
'                " FROM PRODOTTO_MODALITAEMISSIONE" & _
'                " WHERE IDMODALITAEMISSIONE = " & idRecordSelezionato & _
'                " AND IDPRODOTTO = " & idProdottoSelezionato & ")"
'            SETAConnection.Execute sql1, n, adCmdText
'            condizioniSQL = " AND IDPRODOTTO = " & idProdottoSelezionato
'            Call AggiornaParametriSessioneSuRecord("PRODOTTO_MODALITAEMISSIONE", "IDMODALITAEMISSIONE", idRecordSelezionato, TSR_NUOVO, condizioniSQL)
'        Else
            sql1 = "INSERT INTO PRODOTTO_MODALITAEMISSIONE" & _
                " (IDPRODOTTO, IDMODALITAEMISSIONE)" & _
                " VALUES " & _
                "(" & idProdottoSelezionato & ", " & _
                idRecordSelezionato & ")"
            SETAConnection.Execute sql1, n, adCmdText
'        End If
    End If
    SETAConnection.CommitTrans
    
    Call ChiudiConnessioneBD
    
    Exit Sub
    
gestioneErrori:
    SETAConnection.RollbackTrans
    Call frmMessaggio.Visualizza("ErroreGenericoDB", Err.Description)
    
End Sub



