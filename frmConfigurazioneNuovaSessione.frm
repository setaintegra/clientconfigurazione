VERSION 5.00
Begin VB.Form frmConfigurazioneNuovaSessione 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nuova sessione"
   ClientHeight    =   2730
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5250
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2730
   ScaleWidth      =   5250
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Default         =   -1  'True
      Height          =   315
      Left            =   1080
      TabIndex        =   3
      Top             =   2220
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Annulla"
      Height          =   315
      Left            =   4080
      TabIndex        =   4
      Top             =   2220
      Width           =   1035
   End
   Begin VB.ComboBox cmbTipoSessione 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1680
      Width           =   4035
   End
   Begin VB.TextBox txtDescrizione 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   1080
      MaxLength       =   255
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   1020
      Width           =   4035
   End
   Begin VB.TextBox txtNome 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      MaxLength       =   30
      TabIndex        =   0
      Top             =   600
      Width           =   4035
   End
   Begin VB.Label lblTipoSessione 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo"
      Height          =   195
      Left            =   120
      TabIndex        =   8
      Top             =   1740
      Width           =   855
   End
   Begin VB.Label lblTitolo 
      Caption         =   "Crea una nuova sessione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   4995
   End
   Begin VB.Label lblDescrizione 
      Alignment       =   1  'Right Justify
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label lblNome 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Nome"
      Height          =   195
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   780
   End
End
Attribute VB_Name = "frmConfigurazioneNuovaSessione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Dim nomeSessione As String
'Dim descrizioneSessione As String
'Dim idTipoSessione As Long
'Dim tipoSessione As String
'Dim idSessioneCorrente As Long


Private sessioneCorrente As clsSessione 'Oggetto locale del form
Private internalEvent As Boolean

Private Sub AggiornaAbilitazioneControlli()
    cmdConferma.Enabled = Trim(txtNome.Text) <> VALORE_NULLO_TESTO And sessioneCorrente.idTipoSessione <> 0
End Sub

Private Sub Annulla()
    Unload Me
End Sub

Private Sub Conferma()
    If Not sessioneCorrente.EsisteInBaseDati Then
        InserisciSessioneInBaseDati
        idSessioneConfigurazioneCorrente = sessioneCorrente.id
        tipoSessioneConfigurazione = sessioneCorrente.idTipoSessione
        Call ScriviLog(CCTA_NUOVA_SESSIONE, CCDA_SESSIONE_CONFIGURAZIONE, , sessioneCorrente.tipoSessione)
        Unload Me
    Else
        Call frmMessaggio.Visualizza("SessioneDuplicata")
    End If
End Sub

Public Sub Init()
    Set sessioneCorrente = New clsSessione
    Call CaricaComboTipoSessione
    Call AggiornaAbilitazioneControlli
    Call Me.Show(vbModal)
End Sub

Private Sub InserisciSessioneInBaseDati()
 ''devo controllare se la sessione esiste gi�??
        
    Dim mousePointerOld As Integer
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
        Call sessioneCorrente.SalvaNellaBaseDati
        
    MousePointer = mousePointerOld

End Sub

Private Sub tipoSessioneSelezionata_Update()
    If cmbTipoSessione.ListIndex <> -1 Then
        sessioneCorrente.idTipoSessione = cmbTipoSessione.ItemData(cmbTipoSessione.ListIndex)
        sessioneCorrente.tipoSessione = cmbTipoSessione.Text
    End If
End Sub

Private Sub cmbTipoSessione_Click()
    If Not internalEvent Then
        tipoSessioneSelezionata_Update
        AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
        Call Annulla
    
    MousePointer = mousePointerOld

End Sub

Private Sub cmdConferma_Click()
    If Not internalEvent Then
        Conferma
    End If
End Sub

Private Sub txtDescrizione_Change()
    If Not internalEvent Then
        descrizioneSessione_Update
    End If
End Sub

Private Sub txtNome_Change()
    If Not internalEvent Then
        nomeSessione_Update
        AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub nomeSessione_Update()
    sessioneCorrente.nome = Trim(txtNome.Text)
End Sub

Private Sub descrizioneSessione_Update()
    sessioneCorrente.descrizione = Trim(txtDescrizione.Text)
End Sub

Private Sub CaricaComboTipoSessione()
    Dim sql As String
    
    Call cmbTipoSessione.Clear
        
    sql = " SELECT IDTIPOSESSIONECONFIGURAZIONE  ID, NOME FROM CC_TIPOSESSIONECONFIGURAZIONE " & _
          " WHERE IDTIPOSESSIONECONFIGURAZIONE>0 " & _
          " ORDER BY NOME"
        
    Call CaricaValoriCombo(cmbTipoSessione, sql, "NOME")
    Call tipoSessioneSelezionata_Update
    
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox, strSQL As String, NomeCampo As String)
    Dim sql As String
    Dim rec As New ADODB.Recordset
    Dim i As Integer
    
    Call ApriConnessioneBD

    sql = strSQL
    
    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec.BOF And rec.EOF) Then
        i = 1
        rec.MoveFirst
        While Not rec.EOF
            cmb.AddItem rec(NomeCampo)
            cmb.ItemData(i - 1) = rec("ID").Value
            i = i + 1
            rec.MoveNext
        Wend
    End If
    
    rec.Close
    
    Call ChiudiConnessioneBD
            
End Sub


