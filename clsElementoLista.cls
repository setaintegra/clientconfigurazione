VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsElementoLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public idElementoLista As Long
Public nomeElementoLista As String
Public codiceElementoLista As String
Public descrizioneElementoLista As String
'Public idSessioneConfigurazioneElementoLista As Long
'Public tipoStatoRecordElementoLista As TipoStatoRecordEnum
Public idAttributoElementoLista As Long
Public nomeAttributoElementoLista As String
Public codiceAttributoElementoLista As String
Public descrizioneAttributoElementoLista As String
Public listaSottoElementiLista As Collection

Public Sub InizializzaListaSottoElementiLista()
    Set listaSottoElementiLista = New Collection
End Sub
