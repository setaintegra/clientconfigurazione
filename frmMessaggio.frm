VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMessaggio 
   Caption         =   "Messaggio"
   ClientHeight    =   2730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7890
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   2730
   ScaleWidth      =   7890
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdStampa 
      Caption         =   "Stampa"
      Height          =   315
      Left            =   60
      TabIndex        =   4
      Top             =   1980
      Width           =   975
   End
   Begin MSComDlg.CommonDialog cdlSalvaFile 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox txtMessaggio 
      BackColor       =   &H00C0C0C0&
      Height          =   2595
      Left            =   1140
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   60
      Width           =   6675
   End
   Begin VB.CommandButton cmdChiudiAnnulla 
      Caption         =   "Chiudi"
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   1500
      Width           =   975
   End
   Begin VB.CommandButton cmdSalva 
      Caption         =   "Salva"
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   2340
      Width           =   975
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Conferma"
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   1140
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Image imgIconaMessaggio 
      Height          =   735
      Left            =   240
      Top             =   300
      Width           =   795
   End
End
Attribute VB_Name = "frmMessaggio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private messaggio As String
Public exitCode As ExitCodeEnum

Public Sub Visualizza(codiceMessaggio As String, _
                      Optional arg1 As Variant, _
                      Optional arg2 As Variant, _
                      Optional arg3 As Variant, _
                      Optional arg4 As Variant, _
                      Optional arg5 As Variant, _
                      Optional arg6 As Variant, _
                      Optional arg7 As Variant, _
                      Optional arg8 As Variant, _
                      Optional arg9 As Variant) 'CodiceSezione

    Dim arg As Variant
    Dim riga As String
    
    Dim tagMessaggio As String
    Dim tagInizio As String
    Dim tagFine As String
    
    Dim lenTagMessaggio As Byte
    Dim lenTagInizio As Byte
    Dim lenTagFine As Byte
    Dim lenMessaggio As Long
    Dim lenMessaggioLeft As Long
    Dim lenMessaggioRight As Long
    
    Dim messaggioNonTrovato As Boolean
    Dim raggiuntoMessaggioSuggessivo As Boolean
    
    Dim esisteArgomento As Boolean
    Dim posizioneArgomento As Long
    Dim numeroArgomento As Byte
    Dim valoreArgomento As String
    
    Dim tipoMessaggio As String
    
On Error GoTo gestioneErrori

    Me.Height = 3135
    Me.Width = 8010

    tagMessaggio = "[MESSAGGIO]"
    tagInizio = tagMessaggio & " " & codiceMessaggio
    tagFine = tagMessaggio
    
    lenTagMessaggio = Len(tagMessaggio)
    lenTagInizio = Len(tagInizio)
    lenTagFine = Len(tagFine)
        
    messaggio = ""
    
    ' -------------------
    ' CARICA IL MESSAGGIO
    ' -------------------
    Open pathApplicazione & "MESSAGGI.RSC" For Input Access Read Shared As #FILE_MESSAGGI
        While Not (EOF(FILE_MESSAGGI) Or Left(riga, lenTagInizio) = tagInizio)
            Line Input #FILE_MESSAGGI, riga
        Wend
        messaggioNonTrovato = EOF(FILE_MESSAGGI)
        If Not messaggioNonTrovato Then
            tipoMessaggio = Trim(Mid(riga, lenTagInizio + 1, Len(riga)))
            raggiuntoMessaggioSuggessivo = False
            While Not (EOF(FILE_MESSAGGI) Or raggiuntoMessaggioSuggessivo)
                Line Input #FILE_MESSAGGI, riga
                raggiuntoMessaggioSuggessivo = (Left(riga, lenTagFine) = tagFine)
                If Not raggiuntoMessaggioSuggessivo Then messaggio = messaggio & riga & Chr(13) & Chr(10)
            Wend
        End If
    Close #FILE_MESSAGGI
    
    If messaggioNonTrovato Then
    
        messaggio = "Messaggio non disponibile."
    
    Else
        
        ' -----------------------
        ' SOSTITUISCE I PARAMETRI
        ' -----------------------
        esisteArgomento = True
        While esisteArgomento
            posizioneArgomento = InStr(messaggio, "�")
            esisteArgomento = (posizioneArgomento > 0)
            If esisteArgomento Then
                valoreArgomento = ""
                numeroArgomento = Mid(messaggio, posizioneArgomento + 1, 1)
                Select Case numeroArgomento
                    Case 1: If Not IsMissing(arg1) Then valoreArgomento = arg1
                    Case 2: If Not IsMissing(arg2) Then valoreArgomento = arg2
                    Case 3: If Not IsMissing(arg3) Then valoreArgomento = arg3
                    Case 4: If Not IsMissing(arg4) Then valoreArgomento = arg4
                    Case 5: If Not IsMissing(arg5) Then valoreArgomento = arg5
                    Case 6: If Not IsMissing(arg6) Then valoreArgomento = arg6
                    Case 7: If Not IsMissing(arg7) Then valoreArgomento = arg7
                    Case 8: If Not IsMissing(arg8) Then valoreArgomento = arg8
                    Case 9: If Not IsMissing(arg9) Then valoreArgomento = arg9
                End Select
                lenMessaggio = Len(messaggio)
                lenMessaggioLeft = posizioneArgomento - 1
                lenMessaggioRight = lenMessaggio - posizioneArgomento - 1
                messaggio = Left(messaggio, lenMessaggioLeft) & valoreArgomento & Right(messaggio, lenMessaggioRight)
            End If
        Wend
        
        ' ----------------------
        ' ADATTA IL FORM AL TIPO
        ' ----------------------
        Select Case tipoMessaggio
            Case "ERRORE"
                Me.Caption = "Errore"
                cmdSalva.Visible = True
                cmdConferma.Visible = False
                cmdChiudiAnnulla.Caption = "Chiudi"
                imgIconaMessaggio.Picture = LoadPicture(pathApplicazione & "\MSGBOX01.ICO")
            Case "AVVERTIMENTO"
                Me.Caption = "Avvertimento"
                cmdSalva.Visible = True
                cmdConferma.Visible = False
                cmdChiudiAnnulla.Caption = "Chiudi"
                imgIconaMessaggio.Picture = LoadPicture(pathApplicazione & "\MSGBOX03.ICO")
            Case "NOTIFICA"
                Me.Caption = "Notifica"
                cmdSalva.Visible = True
                cmdConferma.Visible = False
                cmdChiudiAnnulla.Caption = "Chiudi"
                imgIconaMessaggio.Picture = LoadPicture(pathApplicazione & "\MSGBOX04.ICO")
            Case "RICHIESTACONFERMA"
                Me.Caption = "Richiesta di CONFERMA"
                cmdSalva.Visible = False
                cmdConferma.Visible = True
                cmdConferma.Caption = "Conferma"
                cmdChiudiAnnulla.Caption = "Annulla"
                imgIconaMessaggio.Picture = LoadPicture(pathApplicazione & "\MSGBOX02.ICO")
            Case "RICHIESTARIPETIZIONE"
                Me.Caption = "Riprova"
                cmdSalva.Visible = False
                cmdConferma.Visible = True
                cmdConferma.Caption = "Riprova"
                cmdChiudiAnnulla.Caption = "Annulla"
                imgIconaMessaggio.Picture = LoadPicture(pathApplicazione & "\MSGBOX02.ICO")
        End Select
     End If
     
    ' -----------------------
    ' VISUALIZZA IL MESSAGGIO
    ' -----------------------
    txtMessaggio = messaggio
    Show 1
    Exit Sub
    
gestioneErrori:
    Call MsgBox("icona non trovata")
End Sub

Private Sub cmdChiudiAnnulla_Click()
    exitCode = EC_ANNULLA
    Hide
End Sub

Private Sub cmdConferma_Click()
    exitCode = EC_CONFERMA
    Hide
End Sub

Private Sub cmdStampa_Click()
    StampaTestoFormattato messaggio, 50, 50
End Sub

Private Sub cmdSalva_Click()
    Call SalvaFileLog
End Sub

Private Sub Form_Resize()
   txtMessaggio.Move 1140, 60, ScaleWidth - 1140, ScaleHeight - 60
End Sub

Private Sub SalvaFileLog()
    Dim nome As String
    Dim fileNum As Integer
    
On Error GoTo gestioneErrori
    
    cdlSalvaFile.InitDir = App.Path
    cdlSalvaFile.Filter = "*.txt"
'    cdlSalvaFile.FilterIndex = 2
    cdlSalvaFile.DefaultExt = "txt"
    cdlSalvaFile.Flags = cdlOFNHideReadOnly Or cdlOFNPathMustExist Or _
        cdlOFNOverwritePrompt Or cdlOFNNoReadOnlyReturn
    cdlSalvaFile.FileName = NomeFileLog
    cdlSalvaFile.CancelError = True
    cdlSalvaFile.ShowSave
    nome = cdlSalvaFile.FileName
    
    fileNum = FreeFile
    Open nome For Output As #fileNum
    Print #fileNum, messaggio
    Close #fileNum
    
    Exit Sub
    
gestioneErrori:
    'do nothing
End Sub




