VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmConfigurazionePiantaFasceSequenze 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pianta"
   ClientHeight    =   10515
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14085
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10515
   ScaleWidth      =   14085
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstPostiOrdinati 
      Height          =   840
      Left            =   13020
      Sorted          =   -1  'True
      TabIndex        =   41
      Top             =   2280
      Width           =   915
   End
   Begin VB.TextBox txtInfo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10680
      TabIndex        =   39
      Top             =   120
      Width           =   3255
   End
   Begin VB.CommandButton cmdSalvaInBD 
      Caption         =   "Salva in base dati"
      Height          =   315
      Left            =   11865
      TabIndex        =   38
      Top             =   8700
      Width           =   1515
   End
   Begin VB.CommandButton cmdCreaSequenzeAutomaticamente 
      Caption         =   "Crea seq. autom."
      Height          =   315
      Left            =   11820
      TabIndex        =   37
      Top             =   5940
      Width           =   1575
   End
   Begin VB.ComboBox cmbArea 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   11460
      Style           =   2  'Dropdown List
      TabIndex        =   31
      Top             =   1920
      Width           =   2475
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "&Annulla"
      Height          =   315
      Left            =   12105
      TabIndex        =   30
      Top             =   9180
      Width           =   1035
   End
   Begin VB.ListBox lstSequenzeConfigurate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1530
      Left            =   11640
      TabIndex        =   24
      Top             =   6300
      Width           =   1935
   End
   Begin VB.CommandButton cmdEliminaSequenza 
      Caption         =   "Elimina sequenza"
      Height          =   315
      Left            =   11820
      TabIndex        =   23
      Top             =   7920
      Width           =   1575
   End
   Begin VB.CommandButton cmdModificaFascia 
      Caption         =   "Modifica fascia"
      Height          =   315
      Left            =   11820
      TabIndex        =   22
      Top             =   5580
      Width           =   1575
   End
   Begin VB.CommandButton cmdEliminaFascia 
      Caption         =   "Elimina fascia"
      Height          =   315
      Left            =   11820
      TabIndex        =   21
      Top             =   5220
      Width           =   1575
   End
   Begin VB.ListBox lstFasceConfigurate 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1530
      Left            =   11640
      TabIndex        =   20
      Top             =   3600
      Width           =   1935
   End
   Begin VB.CommandButton cmdNuovaFascia 
      Caption         =   "Nuova fascia"
      Height          =   315
      Left            =   11820
      TabIndex        =   19
      Top             =   3240
      Width           =   1575
   End
   Begin VB.OptionButton optArea 
      Caption         =   "Area"
      Height          =   195
      Left            =   11460
      TabIndex        =   18
      Top             =   2400
      Width           =   735
   End
   Begin VB.OptionButton optSequenza 
      Caption         =   "Sequenza"
      Height          =   195
      Left            =   11460
      TabIndex        =   17
      Top             =   2880
      Width           =   1035
   End
   Begin VB.OptionButton optFascia 
      Caption         =   "Fascia"
      Height          =   195
      Left            =   11460
      TabIndex        =   16
      Top             =   2640
      Width           =   855
   End
   Begin VB.PictureBox pctGriglia 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   7935
      Left            =   60
      ScaleHeight     =   7875
      ScaleWidth      =   10875
      TabIndex        =   5
      Top             =   600
      Width           =   10935
   End
   Begin VB.CommandButton cmdEsci 
      Caption         =   "&Esci"
      Height          =   315
      Left            =   12105
      TabIndex        =   4
      Top             =   9660
      Width           =   1035
   End
   Begin VB.HScrollBar scbOrizzontale 
      Height          =   255
      Left            =   60
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   8520
      Width           =   10935
   End
   Begin VB.VScrollBar scbVerticale 
      Height          =   7935
      Left            =   10980
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   600
      Width           =   255
   End
   Begin VB.HScrollBar scbZoom 
      Height          =   255
      Left            =   11460
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1260
      Width           =   1875
   End
   Begin MSComctlLib.StatusBar sbrAttributiPosto 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   10200
      Width           =   14085
      _ExtentX        =   24844
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2999
            MinWidth        =   2999
            Key             =   "coordXY"
            Object.ToolTipText     =   "coordinate (x, y)"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Key             =   "nomeFila"
            Object.ToolTipText     =   "nome Fila"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Key             =   "nomePosto"
            Object.ToolTipText     =   "nome Posto"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "indiceDiPreferibilita"
            Object.ToolTipText     =   "indice di Preferibilit�"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "area"
            Object.ToolTipText     =   "area"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
            Key             =   "postiInArea"
            Object.ToolTipText     =   "posti in area:"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
            Key             =   "postiInPianta"
            Object.ToolTipText     =   "posti in pianta:"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Alignment       =   1  'Right Justify
      Caption         =   "lblInfo1"
      Height          =   255
      Left            =   8940
      TabIndex        =   40
      Top             =   180
      Width           =   1635
   End
   Begin VB.Line linSelezionato 
      BorderColor     =   &H0000FF00&
      X1              =   8760
      X2              =   9060
      Y1              =   9765
      Y2              =   9765
   End
   Begin VB.Shape shpInizialeSelezionato 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H0000FF00&
      FillStyle       =   0  'Solid
      Height          =   135
      Left            =   8700
      Shape           =   3  'Circle
      Top             =   9705
      Width           =   135
   End
   Begin VB.Shape shpSequenzaSelezionata01 
      FillColor       =   &H00FF0000&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   8670
      Top             =   9660
      Width           =   195
   End
   Begin VB.Line linSequenza 
      BorderColor     =   &H000000FF&
      X1              =   8670
      X2              =   8880
      Y1              =   9510
      Y2              =   9510
   End
   Begin VB.Shape shpIniziale 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H000000FF&
      FillStyle       =   0  'Solid
      Height          =   135
      Left            =   8700
      Shape           =   3  'Circle
      Top             =   9225
      Width           =   135
   End
   Begin VB.Shape shpPostoInizialeSequenza 
      FillColor       =   &H00FF0000&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   8670
      Top             =   9180
      Width           =   195
   End
   Begin VB.Shape shpPostoInattivo 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00808080&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   8670
      Top             =   9900
      Width           =   195
   End
   Begin VB.Label lblPostoInattivo 
      Caption         =   "Posto inattivo"
      Height          =   195
      Left            =   8940
      TabIndex        =   36
      Top             =   9900
      Width           =   2715
   End
   Begin VB.Shape shpSequenzaSelezionata02 
      FillColor       =   &H00FF0000&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   8925
      Top             =   9660
      Width           =   195
   End
   Begin VB.Label lblSequenzaSelezionata 
      Caption         =   "Sequenza selezionata"
      Height          =   195
      Left            =   9180
      TabIndex        =   35
      Top             =   9660
      Width           =   1695
   End
   Begin VB.Shape shpPostoSequenza 
      FillColor       =   &H00FF0000&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   8670
      Top             =   9420
      Width           =   195
   End
   Begin VB.Label lblPostoSequenza 
      Caption         =   "Posto sequenza"
      Height          =   195
      Left            =   8940
      TabIndex        =   34
      Top             =   9420
      Width           =   2715
   End
   Begin VB.Label lblPostoInizialeSequenza 
      Caption         =   "Posto iniziale sequenza"
      Height          =   195
      Left            =   8940
      TabIndex        =   33
      Top             =   9180
      Width           =   2715
   End
   Begin VB.Label lblArea 
      Caption         =   "Area"
      Height          =   255
      Left            =   11460
      TabIndex        =   32
      Top             =   1680
      Width           =   1635
   End
   Begin VB.Label lblPostoSelezionato 
      Caption         =   "Posto selezionato"
      Height          =   195
      Left            =   5820
      TabIndex        =   29
      Top             =   9900
      Width           =   2715
   End
   Begin VB.Label lblPostoNonAssegnatoAFascia 
      Caption         =   "Posto non assegnato a fascia"
      Height          =   195
      Left            =   5820
      TabIndex        =   28
      Top             =   9660
      Width           =   2715
   End
   Begin VB.Label lblPostoConFascia 
      Caption         =   "Posto assegnato a fascia"
      Height          =   195
      Left            =   5820
      TabIndex        =   27
      Top             =   9420
      Width           =   2715
   End
   Begin VB.Label lblPostoConFasciaSequenza 
      Caption         =   "Posto assegnato a fascia e sequenza"
      Height          =   195
      Left            =   5820
      TabIndex        =   26
      Top             =   9180
      Width           =   2715
   End
   Begin VB.Label lblLegendaColori 
      Caption         =   "Legenda colori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5520
      TabIndex        =   25
      Top             =   8880
      Width           =   1455
   End
   Begin VB.Shape shpPostoSelezionato 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H0000FF00&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   5520
      Top             =   9900
      Width           =   195
   End
   Begin VB.Shape shpPostoNonAssegnatoAFascia 
      FillColor       =   &H000000FF&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   5520
      Top             =   9660
      Width           =   195
   End
   Begin VB.Shape shpPostoConFascia 
      FillColor       =   &H0000FFFF&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   5520
      Top             =   9420
      Width           =   195
   End
   Begin VB.Shape shpPostoConFasciaSequenza 
      FillColor       =   &H00FF0000&
      FillStyle       =   0  'Solid
      Height          =   195
      Left            =   5520
      Top             =   9180
      Width           =   195
   End
   Begin VB.Label lblSelezionePosti 
      Caption         =   "Selezione Posti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   15
      Top             =   8880
      Width           =   2535
   End
   Begin VB.Label lblSelezioneSingoloPosto01 
      Caption         =   "Singolo Posto :"
      Height          =   195
      Left            =   120
      TabIndex        =   14
      Top             =   9180
      Width           =   1095
   End
   Begin VB.Label lblSelezioneSingoloPosto02 
      Caption         =   "SX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   2040
      TabIndex        =   13
      Top             =   9180
      Width           =   1155
   End
   Begin VB.Label lblSelezioneRettangoloPosti01 
      Caption         =   "Rettangolo Posti :"
      Height          =   195
      Left            =   120
      TabIndex        =   12
      Top             =   9660
      Width           =   1275
   End
   Begin VB.Label lblSelezioneRettangoloPosti02 
      Caption         =   "SX + SHIFT + trascinamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   2040
      TabIndex        =   11
      Top             =   9660
      Width           =   2715
   End
   Begin VB.Label lblSelezioneSingoliPosti01 
      Caption         =   "Singoli Posti :"
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   9420
      Width           =   1095
   End
   Begin VB.Label lblSelezioneSingoliPosti02 
      Caption         =   "SX + CTRL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   2040
      TabIndex        =   9
      Top             =   9420
      Width           =   1155
   End
   Begin VB.Label lblZoom01 
      Caption         =   "Zoom :"
      Height          =   195
      Left            =   11520
      TabIndex        =   8
      Top             =   1020
      Width           =   495
   End
   Begin VB.Label lblZoom02 
      Caption         =   "9 X"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   12060
      TabIndex        =   7
      Top             =   1020
      Width           =   495
   End
   Begin VB.Label lblIntestazioneForm 
      Caption         =   "Configurazione grafica delle fasce / sequenze"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   7695
   End
End
Attribute VB_Name = "frmConfigurazionePiantaFasceSequenze"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private maxCoordOrizzontale As Long
Private maxCoordVerticale As Long
Private dimOrizzontalePosto As Long
Private dimVerticalePosto As Long
Private passo As Long
Private xStartRetta As Long
Private xEndRetta As Long
Private yStartRetta As Long
Private yEndRetta As Long
Private larghezzaGriglia As Long
Private altezzaGriglia As Long
Private numeroMaxPostiXInRettangolo As Integer
Private numeroMaxPostiYInRettangolo As Integer
Private idAreaSelezionata As Long
Private idPostoSelezionato As Long
Private idPiantaSelezionata As Long
Private idFasciaSelezionata As Long
Private zoom As Double
Private x0Schermo As Integer
Private y0Schermo As Integer
Private matriceGrigliaPosti() As clsPosto
Private listaPostiSelezionati As Collection
Private listaPostiInGriglia As Collection
Private internalEvent As Boolean
Private nomeAreaSelezionata As String
Private nomePiantaSelezionata As String
Private indiceDiPreferibilitaFasciaSelezionata As Long
Private ordineSequenzaInFascia As Long
Private sequenzaTemporaneaPosti As Collection
Private xPrec As Long
Private yPrec As Long
Private numeroPostiInPianta As Long
Private numeroPostiPerArea As Collection

Private operazioneCorrente As OperazioneCorrenteSuFasciaSequenzaEnum
Private caratteristicaAttualmenteConfigurabile As CaratteristicaAttualmenteConfigurabileEnum
Private tipoGriglia As TipoGrigliaEnum
Private tipoSequenza As TipoSequenzaEnum
Private exitCodeDettagliFascia As ExitCodeDettagliEnum
Private exitCodeDettagliSequenza As ExitCodeDettagliEnum
Private area As classeArea
Private pianta As clsPianta

Public Sub Init()
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    
    internalEvent = True
    pctGriglia.Enabled = False
    sbrAttributiPosto.Enabled = False
    Set listaPostiSelezionati = New Collection
    zoom = 1
    x0Schermo = 1
    y0Schermo = 1
    Call SetCaratteristicaAttualmenteConfigurabile(CAC_NON_SPECIFICATO)
    Call CalcolaPassoReticolo
    Call ScrollBars_Init
    Call MatricePostiBD_Init
    Call CreaMatricePostiBD
    Call ContaPosti
    Call pctGriglia_Init
    Call AssegnaValoriCampi
    Call AggiornaAbilitazioneControlli
    internalEvent = False

    Call CaricaValoriCombo(cmbArea)
    
    Set pianta = New clsPianta
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            Call pianta.inizializza(idPiantaSelezionata, TG_GRANDI_IMPIANTI, idAreaSelezionata)
            Set area = pianta.ottieniAreaDaIdArea(idAreaSelezionata)
            Call SelezionaElementoSuCombo(cmbArea, idAreaSelezionata)
            Call AggiornaAbilitazioneControlli
            Call LstFasceConfigurate_Init
            Call DisegnaGriglia
        Case TG_PICCOLI_IMPIANTI
            Call pianta.inizializza(idPiantaSelezionata, TG_PICCOLI_IMPIANTI)
            Call DisegnaGriglia
    End Select
    
    pctGriglia.Enabled = True
    sbrAttributiPosto.Enabled = True
    Call Me.Show(vbModal)
    
End Sub

Private Sub ImpostaAreaCorrente()

    Set area = New classeArea
    Set area = pianta.ottieniAreaDaIdArea(idAreaSelezionata)
    Call LstFasceConfigurate_Init
    Call SetCaratteristicaAttualmenteConfigurabile(CAC_AREA)
    Call Area_Refresh
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub CreaMatricePostiBD()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim x As Integer
    Dim Y As Integer
    Dim id As Long
    Dim idA As Long
    Dim i As Integer
    Dim j As Integer
    Dim nomeF As String
    Dim nomeP As String
    Dim nomeA As String
    Dim indice As Long
    Dim posto As clsPosto
    
    Set listaPostiInGriglia = New Collection 'SERVONO 3 DIVERSE LISTE
'    Set listaPostiConFascia = New Collection
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
        
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            sql = "SELECT P.IDPOSTO, P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE, P.IDAREA,"
            sql = sql & " P.IDSEQUENZAPOSTI, P.NOMEFILA, P.NOMEPOSTO, A.INDICEDIPREFERIBILITA"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDAREA = " & idAreaSelezionata
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT DISTINCT P.IDPOSTO, P.COORDINATAORIZZONTALE, P.COORDINATAVERTICALE,"
            sql = sql & " P.IDSEQUENZAPOSTI, A.NOME NOMEAREA, A.INDICEDIPREFERIBILITA, P.IDAREA,"
            sql = sql & " P.NOMEFILA, P.NOMEPOSTO"
            sql = sql & " FROM POSTO P, AREA A"
            sql = sql & " WHERE P.IDAREA = A.IDAREA"
            sql = sql & " AND A.IDPIANTA = " & idPiantaSelezionata
        Case Else
    End Select
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        Do While Not rec.EOF
            Set posto = New clsPosto
            posto.idPosto = rec("IDPOSTO").Value
            posto.xPosto = rec("COORDINATAORIZZONTALE").Value
            posto.yPosto = rec("COORDINATAVERTICALE").Value
            posto.idSequenzaPosti = IIf(IsNull(rec("IDSEQUENZAPOSTI")), idNessunElementoSelezionato, rec("IDSEQUENZAPOSTI"))
            Call listaPostiInGriglia.Add(posto)
            x = rec("COORDINATAORIZZONTALE").Value
            Y = rec("COORDINATAVERTICALE").Value
            posto.nomeFila = rec("NOMEFILA")
            posto.nomePosto = rec("NOMEPOSTO")
            posto.indiceDiPreferibilit�Area = rec("INDICEDIPREFERIBILITA").Value
            posto.idArea = rec("IDAREA").Value
            If tipoGriglia = TG_PICCOLI_IMPIANTI Then
                posto.nomeArea = rec("NOMEAREA")
            End If
            For i = 0 To dimOrizzontalePosto - 1
                For j = 0 To dimVerticalePosto - 1
                    Set matriceGrigliaPosti(x + i, Y + j) = posto
                Next j
            Next i
            rec.MoveNext
        Loop
    End If
    rec.Close
    
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub DisegnaMatricePostiBD()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim id As Long
    Dim posto As clsPosto
    
    For Each posto In listaPostiInGriglia
        Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(3), True)
    Next posto
    
End Sub

Private Sub MatricePostiBD_Init()
    Dim i As Integer
    Dim j As Integer
    Dim postoNullo As clsPosto
    
    Set postoNullo = New clsPosto
    ReDim matriceGrigliaPosti(1 To maxCoordOrizzontale, 1 To maxCoordVerticale)
    
    postoNullo.idPosto = idNessunElementoSelezionato
    postoNullo.nomeFila = ""
    postoNullo.nomePosto = ""
    postoNullo.indiceDiPreferibilit�Area = 0
    postoNullo.idArea = idNessunElementoSelezionato
    postoNullo.idSequenzaPosti = idNessunElementoSelezionato
    For i = 1 To maxCoordOrizzontale
        For j = 1 To maxCoordVerticale
            Set matriceGrigliaPosti(i, j) = postoNullo
        Next j
    Next i
End Sub

Private Sub AggiornaAbilitazioneControlli()

    lstPostiOrdinati.Visible = False
    Select Case tipoGriglia
        Case TG_PICCOLI_IMPIANTI
            lblInfo1.Caption = "Pianta"
            txtInfo1.Text = nomePiantaSelezionata
        Case TG_GRANDI_IMPIANTI
            lblInfo1.Caption = "Area"
            txtInfo1.Text = nomeAreaSelezionata
            cmbArea.Enabled = False
    End Select
    txtInfo1.Enabled = False
    lblZoom02.Caption = zoom & " X"
    Select Case caratteristicaAttualmenteConfigurabile
        Case CAC_AREA
            optArea.Enabled = True
            optArea.Value = True
            optFascia.Value = False
            optFascia.Enabled = False
            optSequenza.Value = False
            optSequenza.Enabled = False
            If listaPostiSelezionati Is Nothing Then
                cmdNuovaFascia.Enabled = False
            Else
                cmdNuovaFascia.Enabled = (listaPostiSelezionati.count > 0)
            End If
            lstFasceConfigurate.Enabled = True
            cmdEliminaFascia.Enabled = False
            cmdModificaFascia.Enabled = False
            cmdCreaSequenzeAutomaticamente.Enabled = False
            lstSequenzeConfigurate.Enabled = False
            cmdEliminaSequenza.Enabled = False
        Case CAC_FASCE
            optArea.Enabled = True
            optFascia.Value = True
            optFascia.Enabled = True
            optSequenza.Value = False
            optSequenza.Enabled = False
            cmdNuovaFascia.Enabled = False
            lstFasceConfigurate.Enabled = True
            cmdEliminaFascia.Enabled = lstFasceConfigurate.ListIndex <> idNessunElementoSelezionato
            cmdModificaFascia.Enabled = lstFasceConfigurate.ListIndex <> idNessunElementoSelezionato
            cmdCreaSequenzeAutomaticamente.Enabled = (lstFasceConfigurate.ListIndex <> idNessunElementoSelezionato) And _
                (lstSequenzeConfigurate.ListCount = 0)
            lstSequenzeConfigurate.Enabled = True
            cmdEliminaSequenza.Enabled = lstSequenzeConfigurate.ListIndex <> idNessunElementoSelezionato
        Case CAC_SEQUENZE
            cmdCreaSequenzeAutomaticamente.Enabled = False
            cmdEliminaSequenza.Enabled = lstSequenzeConfigurate.ListIndex <> idNessunElementoSelezionato
        Case Else
            optArea.Enabled = False
            optFascia.Value = False
            optFascia.Enabled = False
            optSequenza.Value = False
            optSequenza.Enabled = False
            cmdNuovaFascia.Enabled = False
            lstFasceConfigurate.Enabled = False
            cmdEliminaFascia.Enabled = False
            cmdModificaFascia.Enabled = False
            cmdCreaSequenzeAutomaticamente.Enabled = False
            lstSequenzeConfigurate.Enabled = False
            cmdEliminaSequenza.Enabled = False
    End Select
End Sub

Private Sub DisegnaGriglia()
    Dim internalEventOld As Boolean
    Dim k As Integer
    
    internalEventOld = internalEvent
    internalEvent = True
    
    Call DisegnaReticoloSuGriglia
    Call Area_Refresh
    internalEvent = internalEventOld
End Sub

Public Sub EliminaPosti()
    Dim sql As String
    Dim i As Integer
    Dim id As Long
    Dim n As Long
    Dim posto As clsPosto

    Call ApriConnessioneBD_ORA
    
    id = idNessunElementoSelezionato
    For Each posto In listaPostiSelezionati
        id = posto.idPosto
        sql = "DELETE FROM POSTO WHERE IDPOSTO = " & id
'        SETAConnection.Execute sql, n, adCmdText
        n = ORADB.ExecuteSQL(sql)
    Next posto
    
    Call ChiudiConnessioneBD_ORA
    
    'cancella i posti dalla griglia
    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, ASP_CANCELLA)
    'aggiorna matrice
    Call MatricePostiBD_Init
    Call CreaMatricePostiBD
    'disegna nuova griglia
    Call DisegnaGriglia
    Set listaPostiSelezionati = Nothing
    Set listaPostiSelezionati = New Collection
    Call AggiornaAbilitazioneControlli

End Sub

Private Sub cmbArea_Click()
    If Not internalEvent Then
        idAreaSelezionata = cmbArea.ItemData(cmbArea.ListIndex)
        Set listaPostiSelezionati = Nothing
        Set listaPostiSelezionati = New Collection
        Call ImpostaAreaCorrente
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdAnnulla_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call Esci
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdEliminaSequenza_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    operazioneCorrente = OCFS_ELIMINA
    Call EliminaSequenza
    
    MousePointer = mousePointerOld
End Sub

Private Sub EliminaSequenza()
    Dim fascia As classeFascia
    
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    Call CaricaFormDettagliSequenza(MFDS_ELIMINA_SEQUENZA)
    If exitCodeDettagliSequenza = EC_DP_CONFERMA Then
        Call fascia.EliminaSequenzaDaIndiceDiPreferibilita(ordineSequenzaInFascia)
'        '''''''''''''''''
        Set listaPostiSelezionati = Nothing
        Set listaPostiSelezionati = fascia.collPosti
'        '''''''''''''''''
        Call LstSequenzeConfigurate_Init(indiceDiPreferibilitaFasciaSelezionata)
        Call Area_Refresh
    Else
        'Do Nothing
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEsci_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call EsciESalvaInBaseDati
    
    MousePointer = mousePointerOld
End Sub

Private Sub EsciESalvaInBaseDati()
    Dim dom As CCDominioAttivit�Enum
    Dim stringaNota As String
    
    Dim count As Integer
    Dim areaCorrente As classeArea
    
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            dom = CCDA_AREA
            stringaNota = "IDPIANTA = " & idPiantaSelezionata & "; IDAREA = " & idAreaSelezionata
        Case TG_PICCOLI_IMPIANTI
            dom = CCDA_PIANTA
            stringaNota = "IDPIANTA = " & idPiantaSelezionata
    End Select
    
    For Each areaCorrente In pianta.collAree
        If areaCorrente Is Nothing Then
'            Call Esci
        Else
            'count = NumeroFasceDaCompletare
            count = areaCorrente.NumeroFasceDaCompletare
            If count = 0 Then
                Call EliminaDallaBaseDati(areaCorrente)
                Call InserisciNellaBaseDati(areaCorrente)
            Else
                Call frmMessaggio.Visualizza("ConfermaUscitaConFascieIncomplete", count)
                If frmMessaggio.exitCode = EC_CONFERMA Then
                    Call EliminaDallaBaseDati(areaCorrente)
                    Call InserisciNellaBaseDati(areaCorrente)
                End If
            End If
        End If
    Next areaCorrente
    Call ScriviLog(CCTA_MODIFICA, dom, CCDA_FASCIA_SEQUENZA, stringaNota)
    Call Esci
End Sub

'Private Function NumeroFasceDaCompletare() As Integer
'    Dim FASCIA As classeFascia
'    Dim count As Integer
'
'    count = 0
'    For Each FASCIA In area.collFasce
'        If FASCIA.collPosti.count > 0 Then
'            count = count + 1
'        End If
'    Next FASCIA
'    NumeroFasceDaCompletare = count
'End Function

Private Sub InserisciNellaBaseDati_old(areaC As classeArea)
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    Dim idNuovaFascia As Long
    Dim idNuovaSequenza As Long
    Dim sql As String
    Dim i As Integer
    Dim rec As New ADODB.Recordset
    
    Call ApriConnessioneBD
    
    For Each fascia In areaC.collFasce
        If fascia.collSequenze.count > 0 Then
            idNuovaFascia = OttieniIdentificatoreDaSequenza("SQ_FASCIAPOSTI")
            sql = "INSERT INTO FASCIAPOSTI (IDFASCIAPOSTI, IDAREA, INDICEDIPREFERIBILITA)" & _
                " VALUES (" & _
                idNuovaFascia & ", " & _
                areaC.idArea & ", " & _
                fascia.indiceDiPreferibilita & ")"
            SETAConnection.Execute sql, , adCmdText
            For Each sequenza In fascia.collSequenze
                idNuovaSequenza = OttieniIdentificatoreDaSequenza("SQ_SEQUENZAPOSTI")
                sql = "INSERT INTO SEQUENZAPOSTI (IDSEQUENZAPOSTI, IDFASCIAPOSTI, ORDINEINFASCIA)" & _
                    " VALUES (" & _
                    idNuovaSequenza & ", " & _
                    idNuovaFascia & ", " & _
                    sequenza.ordineInFascia & ")"
                SETAConnection.Execute sql, , adCmdText
                For i = 1 To sequenza.collPosti.count
                    sql = "UPDATE POSTO SET IDSEQUENZAPOSTI = " & idNuovaSequenza & "," & _
                        " ORDINEINSEQUENZA = " & i & " WHERE IDPOSTO = " & sequenza.collPosti(i).idPosto
                    SETAConnection.Execute sql, , adCmdText
                Next i
            Next sequenza
        End If
    Next fascia
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub EliminaDallaBaseDati_old(areaC As classeArea)
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    Dim rec1 As New ADODB.Recordset
    Dim sql1 As String
    Dim rec2 As New ADODB.Recordset
    Dim sql2 As String
    Dim i As Integer
    Dim idFP As Long
    Dim n As Integer
    
    Call ApriConnessioneBD
    
    n = 0
    sql1 = "UPDATE POSTO SET IDSEQUENZAPOSTI = NULL,"
    sql1 = sql1 & " ORDINEINSEQUENZA = NULL WHERE IDAREA = " & areaC.idArea
    SETAConnection.Execute sql1, n, adCmdText
    
    sql1 = "SELECT IDFASCIAPOSTI FROM FASCIAPOSTI WHERE IDAREA = " & areaC.idArea
    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
    If Not (rec1.BOF And rec1.EOF) Then
        rec1.MoveFirst
        While Not rec1.EOF
            idFP = rec1("IDFASCIAPOSTI")
            sql2 = "DELETE FROM SEQUENZAPOSTI WHERE IDFASCIAPOSTI = " & idFP
            SETAConnection.Execute sql2, n, adCmdText
            rec1.MoveNext
        Wend
    End If
    rec1.Close
    
    sql1 = "DELETE FROM FASCIAPOSTI WHERE IDAREA = " & areaC.idArea
    SETAConnection.Execute sql1, n, adCmdText
    
    Call ChiudiConnessioneBD
    
End Sub

Private Sub Esci()
'    Call EliminaTabellaAppoggioPostiInFascia
    Unload Me
End Sub

Public Sub SetTipoGriglia(tipo As TipoGrigliaEnum)
    tipoGriglia = tipo
End Sub

Public Sub SetMaxCoordOrizzontale(xMax As Long)
    maxCoordOrizzontale = xMax
End Sub

Public Sub SetMaxCoordVerticale(yMax As Long)
    maxCoordVerticale = yMax
End Sub

Public Sub SetDimOrizzontalePosto(xDim As Long)
    dimOrizzontalePosto = xDim
End Sub

Public Sub SetDimVerticalePosto(yDim As Long)
    dimVerticalePosto = yDim
End Sub

Private Sub CalcolaPassoReticolo()
    Dim passoVerticale As Integer
    Dim passoOrizzontale As Integer
    
    larghezzaGriglia = CLng(pctGriglia.Width)
    altezzaGriglia = CLng(pctGriglia.Height)
    passoOrizzontale = CLng(pctGriglia.Width / maxCoordOrizzontale)
    passoVerticale = CLng(pctGriglia.Height / maxCoordVerticale)
    
    passo = IIf(passoOrizzontale < passoVerticale, passoOrizzontale, passoVerticale)
End Sub

Private Sub DisegnaReticoloSuGriglia()
    Dim i As Integer
    Dim j As Integer
    Dim col As Long
    Dim dxSchermo As Long
    Dim dySchermo As Long

    For i = x0Schermo To maxCoordOrizzontale
        For j = y0Schermo To maxCoordVerticale
            If (i Mod 10 = 0) Or (j Mod 10 = 0) Then
                col = QBColor(12)
            Else
                col = QBColor(0)
            End If
            pctGriglia.PSet (zoom * passo * (i - x0Schermo), zoom * passo * (j - y0Schermo)), col
        Next j
    Next i
    
    pctGriglia.Line (zoom * passo * (maxCoordOrizzontale - x0Schermo), 0)- _
        (zoom * passo * (maxCoordOrizzontale - x0Schermo), zoom * passo * (maxCoordVerticale - y0Schermo)), _
        QBColor(0)
    pctGriglia.Line (0, zoom * passo * (maxCoordVerticale - y0Schermo))- _
        (zoom * passo * (maxCoordOrizzontale - x0Schermo), zoom * passo * (maxCoordVerticale - y0Schermo)), _
        QBColor(0)
                    
End Sub

Private Sub cmdNuovaFascia_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    caratteristicaAttualmenteConfigurabile = CAC_FASCE
    operazioneCorrente = OCFS_NUOVA
    Call AggiungiNuovaFascia
    
    MousePointer = mousePointerOld
End Sub

Private Sub cmdSalvaInBD_Click()
    Dim mousePointerOld As Integer
'    Dim oraInizio As Date
'    Dim oraFine As Date
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
'    oraInizio = Now
    Call SalvaInBaseDati
'    oraFine = Now
'
'    Call MsgBox("tempo impiegato: da " & oraInizio & " a " & oraFine)
    
    MousePointer = mousePointerOld
End Sub

Private Sub SalvaInBaseDati()
    Dim count As Integer
    Dim areaCorrente As classeArea
    
    For Each areaCorrente In pianta.collAree
        If Not (areaCorrente Is Nothing) Then
            Call EliminaDallaBaseDati(areaCorrente)
            Call InserisciNellaBaseDati(areaCorrente)
        End If
    Next areaCorrente
End Sub

Private Sub lstFasceConfigurate_Click()
    Call VisualizzaListBoxToolTip(lstFasceConfigurate, lstFasceConfigurate.Text)
    
    If Not internalEvent Then
        indiceDiPreferibilitaFasciaSelezionata = lstFasceConfigurate.ItemData(lstFasceConfigurate.ListIndex)
        caratteristicaAttualmenteConfigurabile = CAC_FASCE
        ordineSequenzaInFascia = idNessunElementoSelezionato
        Call LstSequenzeConfigurate_Init(indiceDiPreferibilitaFasciaSelezionata)
        Call Area_Refresh
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub lstSequenzeConfigurate_Click()
    Call VisualizzaListBoxToolTip(lstSequenzeConfigurate, lstSequenzeConfigurate.Text)
    
    If Not internalEvent Then
        ordineSequenzaInFascia = lstSequenzeConfigurate.ItemData(lstSequenzeConfigurate.ListIndex)
        Call Area_Refresh
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optArea_Click()
    If Not internalEvent Then
        Dim i As Integer
        Dim j As Integer
        Dim colore As ColorePostoEnum
        
        caratteristicaAttualmenteConfigurabile = CAC_AREA
        Call SelezionaElementoSuLista(lstFasceConfigurate, 0)
        Call lstSequenzeConfigurate.Clear
        Set listaPostiSelezionati = Nothing
        Set listaPostiSelezionati = New Collection
        Call Area_Refresh
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optFascia_Click()
    If Not internalEvent Then
        caratteristicaAttualmenteConfigurabile = CAC_FASCE
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub optSequenza_Click()
    If Not internalEvent Then
        caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub pctGriglia_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call AzioneSuGriglia_Update(Button, Shift, x, Y)
End Sub

Private Sub AzioneSuGriglia_Update(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim xSch As Long
    Dim ySch As Long
    Dim idA As Long
    
    If xBD(CInt(x)) > maxCoordOrizzontale Or xBD(CInt(x)) <= 0 Or _
       yBD(CInt(Y)) > maxCoordVerticale Or yBD(CInt(Y)) <= 0 Then
    Else
        xSch = Intero(x)
        ySch = Intero(Y)
        
        Select Case Button
            Case vbLeftButton
                If Shift = vbShiftMask Then
                    pctGriglia.AutoRedraw = False
                    pctGriglia.DrawStyle = vbDot
                    pctGriglia.Refresh
                    pctGriglia.Line (xStartRetta, yStartRetta)-(xSch, ySch), QBColor(0), B
                End If
                pctGriglia.AutoRedraw = True
                pctGriglia.DrawStyle = vbSolid
            Case vbRightButton
                Dim idP As Long
                Dim posto As clsPosto
                Dim fascia As classeFascia
                If caratteristicaAttualmenteConfigurabile = CAC_FASCE Or _
                   caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE Then
                    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
                    Set posto = New clsPosto
                    Set posto = matriceGrigliaPosti(xBD(xSch), yBD(ySch))
                    idP = posto.idPosto
                    If Not fascia.IsPostoAssociatoAFascia(idP) Then
                        idP = idNessunElementoSelezionato
                    Else
                        If idP <> idNessunElementoSelezionato Then
                            If AggiungiPostoAllaSequenzaTemporanea(posto) Then
                                If sequenzaTemporaneaPosti.count > 1 Then
                                    pctGriglia.Refresh
                                    pctGriglia.Line (xPrec, yPrec)- _
                                                    (xSchermo(matriceGrigliaPosti(xBD(xSch), yBD(ySch)).xPosto) + CLng(dimOrizzontalePosto / 2 * passo * zoom), _
                                                    ySchermo(matriceGrigliaPosti(xBD(xSch), yBD(ySch)).yPosto) + CLng(dimVerticalePosto / 2 * passo * zoom)), _
                                                    QBColor(CP_ROSSO)
                                    DoEvents
                                End If
                                xPrec = xSchermo(matriceGrigliaPosti(xBD(xSch), yBD(ySch)).xPosto) + CLng(dimOrizzontalePosto / 2 * passo * zoom)
                                yPrec = ySchermo(matriceGrigliaPosti(xBD(xSch), yBD(ySch)).yPosto) + CLng(dimVerticalePosto / 2 * passo * zoom)
                            End If
                        End If
                    End If
                End If
        End Select
        
        If xBD(xSch) > maxCoordOrizzontale Or xBD(xSch) < 0 Or _
           yBD(ySch) > maxCoordVerticale Or yBD(ySch) < 0 Then
            sbrAttributiPosto.Panels(1).Text = ""
            sbrAttributiPosto.Panels(2).Text = ""
            sbrAttributiPosto.Panels(3).Text = ""
            sbrAttributiPosto.Panels(4).Text = ""
            sbrAttributiPosto.Panels(5).Text = ""
            sbrAttributiPosto.Panels(6).Text = ""
            sbrAttributiPosto.Panels(7).Text = ""
        Else
            sbrAttributiPosto.Panels(1).Text = "X = " & CStr(xBD(xSch)) & "; " & "Y = " & CStr(yBD(ySch))
            sbrAttributiPosto.Panels(2).Text = "Fila: " & matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomeFila
            sbrAttributiPosto.Panels(3).Text = "Posto: " & matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomePosto
            sbrAttributiPosto.Panels(4).Text = "Indice di preferibilit� Area: " & IIf(matriceGrigliaPosti(xBD(xSch), yBD(ySch)).indiceDiPreferibilit�Area = _
                        idNessunElementoSelezionato, "", matriceGrigliaPosti(xBD(xSch), yBD(ySch)).indiceDiPreferibilit�Area)
            sbrAttributiPosto.Panels(5).Text = "Area: " & IIf(tipoGriglia = TG_GRANDI_IMPIANTI, nomeAreaSelezionata, _
                        matriceGrigliaPosti(xBD(xSch), yBD(ySch)).nomeArea)
            idA = matriceGrigliaPosti(xBD(xSch), yBD(ySch)).idArea
            If idA = idNessunElementoSelezionato Then
                If tipoGriglia = TG_GRANDI_IMPIANTI Then
                    sbrAttributiPosto.Panels(6).Text = "Posti in Area: " & numeroPostiPerArea.Item(ChiaveId(idAreaSelezionata))
                Else
                    sbrAttributiPosto.Panels(6).Text = "Posti in Area: " & "0"
                End If
            Else
                sbrAttributiPosto.Panels(6).Text = "Posti in Area: " & numeroPostiPerArea.Item(ChiaveId(idA))
            End If
            sbrAttributiPosto.Panels(7).Text = "Posti in Pianta: " & numeroPostiInPianta
        End If
    End If
    
End Sub

Private Sub pctGriglia_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call AzioneSuGriglia_Start(Button, Shift, x, Y)
End Sub

Private Sub AzioneSuGriglia_Start(Button As Integer, Shift As Integer, x As Single, Y As Single)
    xStartRetta = x
    yStartRetta = Y
    If Button = vbRightButton Then
        Set sequenzaTemporaneaPosti = New Collection
    End If
End Sub

Private Sub DisegnaPosto(XCoord As Integer, YCoord As Integer, col As Long, bordoNero As Boolean) 'GLI ARGOMENTI SONO DEL TIPO xBD, yBD
    Dim x0 As Long
    Dim y0 As Long
    Dim x1 As Long
    Dim y1 As Long
    Dim deltaX As Double
    Dim deltaY As Double

    deltaX = dimOrizzontalePosto / 10
    deltaY = dimVerticalePosto / 10

    x0 = CLng((XCoord - x0Schermo + deltaX) * passo * zoom)
    y0 = CLng((YCoord - y0Schermo + deltaY) * passo * zoom)
    x1 = CLng((XCoord + dimOrizzontalePosto - x0Schermo - deltaX) * _
        passo * zoom)
    y1 = CLng((YCoord + dimVerticalePosto - y0Schermo - deltaY) * _
        passo * zoom)

    x0 = IIf(x0 < 0, 0, x0): x0 = IIf(x0 > larghezzaGriglia, larghezzaGriglia, x0)
    y0 = IIf(y0 < 0, 0, y0): y0 = IIf(y0 > altezzaGriglia, altezzaGriglia, y0)
    x1 = IIf(x1 < 0, 0, x1): x1 = IIf(x1 > larghezzaGriglia, larghezzaGriglia, x1)
    y1 = IIf(y1 < 0, 0, y1): y1 = IIf(y1 > altezzaGriglia, altezzaGriglia, y1)

    pctGriglia.Line (x0, y0)-(x1, y1), col, BF
    If bordoNero Then
        pctGriglia.Line (x0, y0)-(x1, y1), QBColor(CP_NERO), B
    End If
            
End Sub

Private Function xBD(xGriglia As Long) As Integer
    Dim valore As Integer
'   CONVERTE DA X(PICTURE BOX) A X CHE SARA' SCRITTO IN BD
    valore = Int(xGriglia / (passo * zoom)) + x0Schermo
    If valore > 0 Then
        If valore < maxCoordOrizzontale Then
            xBD = valore
        Else
            xBD = maxCoordOrizzontale
        End If
    Else
        xBD = 1
    End If
End Function

Private Function yBD(yGriglia As Long) As Integer
    Dim valore As Integer
'   CONVERTE DA Y(PICTURE BOX) A Y CHE SARA' SCRITTO IN BD
    valore = Int(yGriglia / (passo * zoom)) + y0Schermo
    If valore > 0 Then
        If valore < maxCoordVerticale Then
            yBD = valore
        Else
            yBD = maxCoordVerticale
        End If
    Else
        yBD = 1
    End If
End Function

Private Function xSchermo(xInt As Integer) As Long
'   CONVERTE DA X(DATA BASE) A X DEL CONTROLLO PCTBOX
    xSchermo = CLng(xInt - x0Schermo) * passo * zoom
End Function

Private Function ySchermo(yInt As Integer) As Long
'   CONVERTE DA X(DATA BASE) A X DEL CONTROLLO PCTBOX
    ySchermo = CLng(yInt - y0Schermo) * passo * zoom
End Function

'Public Sub SetNumeroPostiInFila(num As Integer)
'    numeroPostiInFila = num
'End Sub

Private Sub pctGriglia_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Call AzioneSuGriglia_End(Button, Shift, x, Y)
End Sub

Private Sub AzioneSuGriglia_End(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim postoSelezionato As clsPosto
    Dim chiavePosto As String
    Dim mousePointerOld As Integer

    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    DoEvents
    DoEvents
    
    If xBD(CInt(x)) > maxCoordOrizzontale Or xBD(CInt(x)) < 0 Or _
       yBD(CInt(Y)) > maxCoordVerticale Or yBD(CInt(Y)) < 0 Then
        'Do Nothing
    Else
        xEndRetta = x
        yEndRetta = Y
        Select Case Button
            Case vbRightButton
                Dim fascia As classeFascia
                If caratteristicaAttualmenteConfigurabile = CAC_FASCE Or _
                    caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE Then
                    idPostoSelezionato = matriceGrigliaPosti(xBD(xEndRetta), yBD(yEndRetta)).idPosto
                    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
                    If fascia.IsPostoAssociatoAFascia(idPostoSelezionato) And Not (sequenzaTemporaneaPosti Is Nothing) Then
                        Call AggiungiNuovaSequenza
                    End If
                End If
            Case vbLeftButton
                If caratteristicaAttualmenteConfigurabile = CAC_AREA Then
                    If Shift <> vbShiftMask And Shift <> vbCtrlMask And Shift <> vbAltMask Then
                        Select Case caratteristicaAttualmenteConfigurabile
                            Case CAC_AREA
                                Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_NON_IN_FASCIA)
                            Case CAC_FASCE
                                Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_CON_FASCIA)
                            Case CAC_SEQUENZE
                                'NIENTE da fare
                        End Select
                        Set postoSelezionato = RilevaPostoSelezionato(xBD(xEndRetta), yBD(yEndRetta))
                        If postoSelezionato.idPosto <> idNessunElementoSelezionato Then
                            Set listaPostiSelezionati = Nothing
                            Set listaPostiSelezionati = New Collection
                            chiavePosto = ChiaveId(postoSelezionato.idPosto)
                            Call listaPostiSelezionati.Add(postoSelezionato, chiavePosto)
                            Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_SELEZIONATO)
                        Else
                            Select Case caratteristicaAttualmenteConfigurabile
                                Case CAC_AREA
                                    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_NON_IN_FASCIA)
                                Case CAC_FASCE
                                    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_CON_FASCIA)
                                Case CAC_SEQUENZE
                                    'da fare
                            End Select
                            Set listaPostiSelezionati = Nothing
                            Set listaPostiSelezionati = New Collection
                        End If
                        Call AggiornaAbilitazioneControlli
                    Else
                        Select Case Shift
                            Case vbShiftMask
                                pctGriglia.Refresh
                                Select Case caratteristicaAttualmenteConfigurabile
                                    Case CAC_AREA
                                        Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_NON_IN_FASCIA)
                                    Case CAC_FASCE
                                        Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_CON_FASCIA)
                                    Case CAC_SEQUENZE
                                        'da fare
                                End Select
                                Set listaPostiSelezionati = Nothing
                                Set listaPostiSelezionati = New Collection
                                Call RilevaPostiRettangolo
                                Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_SELEZIONATO)
                                Call AggiornaAbilitazioneControlli
                            Case vbCtrlMask
                                Set postoSelezionato = RilevaPostoSelezionato(xBD(xEndRetta), yBD(yEndRetta))
                                If postoSelezionato.idPosto <> idNessunElementoSelezionato Then
                                    chiavePosto = ChiaveId(postoSelezionato.idPosto)
                                    Call listaPostiSelezionati.Add(postoSelezionato, chiavePosto)
                                    Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_SELEZIONATO)
                                    Call AggiornaAbilitazioneControlli
                                End If
                            Case Else
                            Set listaPostiSelezionati = Nothing
                            Set listaPostiSelezionati = New Collection
                        End Select
                    End If
                End If
            Case Else
                    'Do Nothing
        End Select
    End If
    MousePointer = mousePointerOld
    DoEvents
End Sub

Private Sub RilevaPostiRettangolo()
    Dim i As Integer
    Dim j As Integer
    Dim id As Long
    Dim idOld As Long
    Dim segnoX As Integer
    Dim segnoY As Integer
    Dim posto As clsPosto
    Dim chiavePosto As String
    
    If (xBD(xEndRetta) - xBD(xStartRetta)) >= 0 Then
        segnoX = 1
    Else
        segnoX = -1
    End If
    If (yBD(yEndRetta) - yBD(yStartRetta)) >= 0 Then
        segnoY = 1
    Else
        segnoY = -1
    End If
    
    idOld = idNessunElementoSelezionato
    For i = xBD(xStartRetta) To xBD(xEndRetta) Step segnoX * dimOrizzontalePosto
        For j = yBD(yStartRetta) To yBD(yEndRetta) Step segnoY * dimVerticalePosto
            id = matriceGrigliaPosti(i, j).idPosto
            Select Case caratteristicaAttualmenteConfigurabile
                Case CAC_AREA
                    If area.VerificaSePostoInAreaCorrente(id) Then
                        If area.VerificaSePostoInFasciaSequenzaArea(id) Then
                            'Do Nothing
                        Else
                            If id <> idNessunElementoSelezionato And id <> idOld Then
                                idOld = id
'                                Set posto = New clsPosto
                                Set posto = matriceGrigliaPosti(i, j)
                                chiavePosto = ChiaveId(id)
                                Call listaPostiSelezionati.Add(posto, chiavePosto)
                            End If
                        End If
                    End If
                Case CAC_FASCE
                Case CAC_SEQUENZE
                    'da fare
            End Select
        Next j
    Next i
    
End Sub

Private Sub ImpostaColoreInsiemePosti(lista As Collection, asp As AspettoPostoEnum)
    Dim posto As clsPosto
    
    If Not (lista Is Nothing) Then
        For Each posto In lista
            Select Case asp
                Case AP_CON_FASCIA_SEQUENZA
                    Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(CP_AZZURRO), True)
                Case AP_CON_FASCIA
                    Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(CP_GIALLO), True)
                Case AP_NON_IN_FASCIA
                    Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(CP_ROSSO), True)
                Case AP_SELEZIONATO
                    Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(CP_VERDE_SELEZIONE), False)
                Case AP_NON_ATTIVO
                    Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(CP_GRIGIO), False)
                Case AP_DESELEZIONATO
'                    RIOTTIENE IL COLORE PRECEDENTE
                    If operazioneCorrente = OCFS_NUOVA And _
                       caratteristicaAttualmenteConfigurabile = CAC_FASCE Then
                        Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(6), True)
                    Else
                        Call DisegnaPosto(posto.xPosto, posto.yPosto, QBColor(3), True)
                    End If
                Case Else
            End Select

        Next posto
    End If
    
End Sub

Private Function RilevaPostoSelezionato(x As Integer, Y As Integer) As clsPosto
    Set RilevaPostoSelezionato = matriceGrigliaPosti(x, Y)
End Function
'
'Private Sub CaricaFormDettagliRettangoloPosti(moda As ModalitaFormDettagliEnum)
'    Call frmDettagliRettangoloPosti.SetModalitaForm(moda)
'    Call frmDettagliRettangoloPosti.SetNumeroMaxPostiInFila(numeroMaxPostiXInRettangolo)
'    Call frmDettagliRettangoloPosti.SetNumeroMaxFileInRettangolo(numeroMaxPostiYInRettangolo)
'    Call frmDettagliRettangoloPosti.SetTipoGriglia(tipoGriglia)
'    If tipoGriglia = TG_GRANDI_IMPIANTI Then
'        Call frmDettagliRettangoloPosti.SetIdAreaSelezionata(idAreaSelezionata)
'        Call frmDettagliRettangoloPosti.SetNomeAreaSelezionata(nomeAreaSelezionata)
'    End If
'    Call frmDettagliRettangoloPosti.Init
'End Sub

'Public Sub SetNumeroFileInRettangolo(numF As Integer)
'    numeroFileInRettangolo = numF
'End Sub

Public Sub SetIdAreaSelezionata(idA As Long)
    idAreaSelezionata = idA
End Sub

Public Sub SetIdPiantaSelezionata(idP As Long)
    idPiantaSelezionata = idP
End Sub

'Public Sub SetNomePrimaFila(nomePF As String)
'    nomePrimaFila = nomePF
'End Sub
'
'Public Sub SetNomeUltimaFila(nomeUF As String)
'    nomeUltimaFila = nomeUF
'End Sub
'
'Public Sub SetNomePrimoPosto(nomePP As String)
'    nomePrimoPosto = nomePP
'End Sub
'
'Public Sub SetNomeUltimoPosto(nomeUP As String)
'    nomeUltimoPosto = nomeUP
'End Sub
'
'Public Sub SetStepPosti(stP As Long)
'    stepPosti = stP
'End Sub
'
'Public Sub SetStepFile(stF As Long)
'    stepFile = stF
'End Sub
'
'Public Sub SetMetodoCreazionePosti(m As MetodoCreazionePostiEnum)
'    metodoCreazionePosti = m
'End Sub

Public Sub SetCaratteristicaAttualmenteConfigurabile(c As CaratteristicaAttualmenteConfigurabileEnum)
    caratteristicaAttualmenteConfigurabile = c
End Sub

Public Sub SetExitCodeDettagliFascia(ec As ExitCodeDettagliEnum)
    exitCodeDettagliFascia = ec
End Sub

Public Sub SetExitCodeDettagliSequenza(ec As ExitCodeDettagliEnum)
    exitCodeDettagliSequenza = ec
End Sub

Private Sub pctGriglia_Init()
    pctGriglia.MousePointer = vbCrosshair
    pctGriglia.DrawWidth = 1
    pctGriglia.Cls
End Sub

Private Sub ScrollBars_Init()
    
    scbOrizzontale.SmallChange = 1
    scbOrizzontale.LargeChange = 10
    scbOrizzontale.Min = 1
    If x0Schermo + Int(larghezzaGriglia / (passo * zoom)) <= maxCoordOrizzontale Then
        scbOrizzontale.max = maxCoordOrizzontale - Int(larghezzaGriglia / (passo * zoom))
    Else
        scbOrizzontale.max = 1
    End If
    
    scbVerticale.SmallChange = 1
    scbVerticale.LargeChange = 10
    scbVerticale.Min = 1
    If y0Schermo + Int(altezzaGriglia / (passo * zoom)) <= maxCoordVerticale Then
        scbVerticale.max = maxCoordVerticale - Int(altezzaGriglia / (passo * zoom))
    Else
        scbVerticale.max = 1
    End If
    
    scbZoom.Min = 1
    scbZoom.max = 5
    scbZoom.Value = 1
End Sub

Private Sub scbZoom_Change()
    If Not internalEvent Then
        Call ApplicaZoom
    End If
End Sub

Private Sub ApplicaZoom()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    zoom = scbZoom.Value
    If x0Schermo + Int(larghezzaGriglia / (passo * zoom)) <= maxCoordOrizzontale Then
        scbOrizzontale.max = maxCoordOrizzontale - Int(larghezzaGriglia / (passo * zoom))
    Else
        scbOrizzontale.max = 1
    End If
    If y0Schermo + Int(altezzaGriglia / (passo * zoom)) <= maxCoordVerticale Then
        scbVerticale.max = maxCoordVerticale - Int(altezzaGriglia / (passo * zoom))
    Else
        scbVerticale.max = 1
    End If
    Call pctGriglia_Init
    Call DisegnaGriglia
    Call AggiornaAbilitazioneControlli
    MousePointer = mousePointerOld
End Sub

Private Sub scbOrizzontale_Change()
    If Not internalEvent Then
        Call ApplicaScrollOrizzontale
    End If
End Sub

Private Sub ApplicaScrollOrizzontale()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    x0Schermo = scbOrizzontale.Value
    If x0Schermo + Int(larghezzaGriglia / (passo * zoom)) <= maxCoordOrizzontale Then
        scbOrizzontale.max = maxCoordOrizzontale - Int(larghezzaGriglia / (passo * zoom))
    Else
        scbOrizzontale.max = 1
    End If
    Call pctGriglia_Init
    Call DisegnaGriglia
    
    MousePointer = mousePointerOld
End Sub

Private Sub scbVerticale_Change()
    If Not internalEvent Then
        Call ApplicaScrollVerticale
    End If
End Sub

Private Sub ApplicaScrollVerticale()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    y0Schermo = scbVerticale.Value
    If y0Schermo + Int(altezzaGriglia / (passo * zoom)) <= maxCoordVerticale Then
        scbVerticale.max = maxCoordVerticale - Int(altezzaGriglia / (passo * zoom))
    Else
        scbVerticale.max = 1
    End If
    Call pctGriglia_Init
    Call DisegnaGriglia
    
    MousePointer = mousePointerOld
End Sub

Public Sub SetNomeAreaSelezionata(nomeA As String)
    nomeAreaSelezionata = nomeA
End Sub

Public Sub SetNomePiantaSelezionata(nomeP As String)
    nomePiantaSelezionata = nomeP
End Sub

Private Sub CaricaValoriCombo(cmb As ComboBox)
    Dim i As Integer
    Dim area As classeArea
    
'    For i = 1 To UBound(elencoAreeAssociateAPianta)
'        cmb.AddItem (elencoAreeAssociateAPianta(i).nomeArea)
'        cmb.ItemData(i - 1) = elencoAreeAssociateAPianta(i).idArea
'    Next i
    i = 1
    For Each area In listaAreeAssociateAPianta
        cmb.AddItem area.nomeArea
        cmb.ItemData(i - 1) = area.idArea
        i = i + 1
    Next area
End Sub

Public Sub AggiornaMatricePostiBD(id As Long, idA As Long, x As Integer, Y As Integer, _
                                  nomeF As String, nomeP As String, nomeA As String, _
                                  indice As Long)
    Dim i As Integer
    Dim j As Integer
    
    For i = 0 To dimOrizzontalePosto - 1
        For j = 0 To dimVerticalePosto - 1
            matriceGrigliaPosti(x + i, Y + j).idPosto = id
            matriceGrigliaPosti(x + i, Y + j).nomeFila = ProssimoNomeFila(nomeF, 0)
            matriceGrigliaPosti(x + i, Y + j).nomePosto = ProssimoNomePosto(nomeP, 0)
            matriceGrigliaPosti(x + i, Y + j).idArea = idA
            matriceGrigliaPosti(x + i, Y + j).nomeArea = nomeA
        Next j
    Next i
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Public Sub SetIdFasciaSelezionata(idF As Long)
    idFasciaSelezionata = idF
End Sub

Public Sub SetIndiceDiPreferibilitaFascia(ind As Long)
    indiceDiPreferibilitaFasciaSelezionata = ind
End Sub

Public Sub SetOrdineSequenzaInFascia(ind As Long)
    ordineSequenzaInFascia = ind
End Sub

Private Sub CaricaFormDettagliFascia(moda As ModalitaFormDettagliFasciaEnum)
    nomeAreaSelezionata = area.nomeArea
    Call frmDettagliFascia.SetModalitaForm(moda)
    Call frmDettagliFascia.SetIdFascia(idFasciaSelezionata)
    Call frmDettagliFascia.SetIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    Call frmDettagliFascia.SetTipoGriglia(tipoGriglia)
    Call frmDettagliFascia.SetIdAreaSelezionata(idAreaSelezionata)
    Call frmDettagliFascia.SetListaFasceConfigurate(area.collFasce)
    Call frmDettagliFascia.SetNomeAreaSelezionata(nomeAreaSelezionata)
    Call frmDettagliFascia.Init
End Sub

Private Sub CaricaFormCreaSequenzeAutomaticamente()
    Call frmCreaSequenzeAutomaticamente.Init
End Sub

Private Sub LstFasceConfigurate_Init()
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim f As classeFascia

    internalEventOld = internalEvent
    internalEvent = True
    
    lstFasceConfigurate.Clear

    If Not (area.collFasce Is Nothing) Then
        i = 1
        For Each f In area.collFasce
            lstFasceConfigurate.AddItem "fascia " & f.indiceDiPreferibilita
            lstFasceConfigurate.ItemData(i - 1) = f.indiceDiPreferibilita
            i = i + 1
        Next f
    End If
           
    internalEvent = internalEventOld

End Sub

Private Sub AssegnaValoriCampi()
    optFascia.Value = (caratteristicaAttualmenteConfigurabile = CAC_FASCE)
    optSequenza.Value = (caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE)
End Sub

Private Sub cmdModificaFascia_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    caratteristicaAttualmenteConfigurabile = CAC_FASCE
    operazioneCorrente = OCFS_MODIFICA
    Call ModificaFascia
    
    MousePointer = mousePointerOld
End Sub

Private Sub ModificaFascia()
    Dim fascia As classeFascia
    Dim indiceOld As Long
    
    Call GetIndiceDiPreferibilitaDaFasciaSelezionataSuLista
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    indiceOld = indiceDiPreferibilitaFasciaSelezionata
    Call CaricaFormDettagliFascia(MFDF_MODIFICA_ATTRIBUTI_FASCIA)
    If exitCodeDettagliFascia = EC_DP_CONFERMA Then
        If area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata) Is Nothing Then
            Call area.sostituisciFasciaDaIndiceDiPreferibilita(indiceOld, indiceDiPreferibilitaFasciaSelezionata)
            Call LstFasceConfigurate_Init
        Else
            Call frmMessaggio.Visualizza("AvvertimentoIndicePreferibilitaGiaPresentePerArea", _
                                          indiceDiPreferibilitaFasciaSelezionata, _
                                          nomeAreaSelezionata)
        End If
    Else
        'Do Nothing
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub cmdEliminaFascia_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    caratteristicaAttualmenteConfigurabile = CAC_FASCE
    operazioneCorrente = OCFS_ELIMINA
    Call EliminaFascia
    
    MousePointer = mousePointerOld
End Sub

Private Sub EliminaFascia()
    Dim fascia As classeFascia
    
    Call GetIndiceDiPreferibilitaDaFasciaSelezionataSuLista
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    Call CaricaFormDettagliFascia(MFDF_ELIMINA_FASCIA)
    If exitCodeDettagliFascia = EC_DP_CONFERMA Then
        Call area.EliminaFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
        Call LstFasceConfigurate_Init
        caratteristicaAttualmenteConfigurabile = CAC_AREA
        Call Area_Refresh
    Else
        'Do Nothing
    End If
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub GetIndiceDiPreferibilitaDaFasciaSelezionataSuLista()
    If lstFasceConfigurate.ListCount > 0 Then
        indiceDiPreferibilitaFasciaSelezionata = lstFasceConfigurate.ItemData(lstFasceConfigurate.ListIndex)
    Else
        indiceDiPreferibilitaFasciaSelezionata = 0
    End If
End Sub

Private Sub SelezionaElementoSuLista(lst As ListBox, indice As Long)
    Dim i As Integer
    Dim trovato As Boolean
    Dim internalEventOld As Boolean
    
    i = 1
    trovato = False
    If indice = 0 Then
        If lst.ListIndex = idNessunElementoSelezionato Then
            'Do Nothing
        Else
            internalEventOld = internalEvent
            internalEvent = True
            lst.Selected(lst.ListIndex) = False
            internalEvent = internalEventOld
        End If
    Else
        While i <= lst.ListCount And Not trovato
            If lst.ItemData(i - 1) = indice Then
                trovato = True
                internalEventOld = internalEvent
                internalEvent = True
                lst.Selected(i - 1) = True
                internalEvent = internalEventOld
            End If
            i = i + 1
        Wend
    End If
End Sub

Private Sub LstSequenzeConfigurate_Init(indF As Long)
    Dim internalEventOld As Boolean
    Dim i As Integer
    Dim f As classeFascia
    Dim s As classeSequenza

    internalEventOld = internalEvent
    internalEvent = True
    
    lstSequenzeConfigurate.Clear

    Set f = area.ottieniFasciaDaIndiceDiPreferibilita(indF)
    If Not (f.collSequenze Is Nothing) Then
        i = 1
        For Each s In f.collSequenze
            lstSequenzeConfigurate.AddItem "sequenza " & s.ordineInFascia
            lstSequenzeConfigurate.ItemData(i - 1) = s.ordineInFascia
            i = i + 1
        Next s
    End If
           
    internalEvent = internalEventOld

End Sub

Private Function AggiungiPostoAllaSequenzaTemporanea(posto As clsPosto) As Boolean
    Dim iPosto As Long
    Dim i As Integer
    Dim gi�Presente As Boolean
    
    AggiungiPostoAllaSequenzaTemporanea = False
    i = 1
    gi�Presente = False
    While i <= sequenzaTemporaneaPosti.count
        gi�Presente = (sequenzaTemporaneaPosti(i).idPosto = posto.idPosto)
        i = i + 1
    Wend
    If Not gi�Presente Then
        Call sequenzaTemporaneaPosti.Add(posto)
        AggiungiPostoAllaSequenzaTemporanea = True
    End If
End Function

Private Sub CaricaFormDettagliSequenza(moda As ModalitaFormDettagliSequenzaEnum)
    Dim fascia As classeFascia
    nomeAreaSelezionata = area.nomeArea
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    Call frmDettagliSequenza.SetModalitaForm(moda)
    Call frmDettagliSequenza.SetIdSequenza(idFasciaSelezionata)
    Call frmDettagliSequenza.SetOrdineSequenzaInFascia(ordineSequenzaInFascia)
    Call frmDettagliSequenza.SetTipoGriglia(tipoGriglia)
    Call frmDettagliSequenza.SetIdAreaSelezionata(idAreaSelezionata)
    Call frmDettagliSequenza.SetListaSequenzeConfigurate(fascia.collSequenze)
    Call frmDettagliSequenza.SetNomeAreaSelezionata(nomeAreaSelezionata)
    Call frmDettagliSequenza.Init
End Sub

Private Sub DisegnaLineaUnionePosti(xOld As Integer, yOld As Integer, xCorr As Integer, yCorr As Integer, col As ColorePostoEnum)
    pctGriglia.Line (xSchermo(xOld) + CLng(dimOrizzontalePosto * passo * zoom / 2), _
                    ySchermo(yOld) + CLng(dimVerticalePosto * passo * zoom / 2))- _
                    (xSchermo(xCorr) + CLng(dimOrizzontalePosto * passo * zoom / 2), _
                    ySchermo(yCorr) + CLng(dimVerticalePosto * passo * zoom / 2)), _
                    QBColor(col)
End Sub
    
Private Sub DisegnaPuntoIniziale(x As Integer, Y As Integer, col As ColorePostoEnum)
    Dim fill_old As Integer
    Dim color_old
    
    fill_old = pctGriglia.FillStyle
    color_old = pctGriglia.FillColor
    pctGriglia.FillStyle = 0
    pctGriglia.FillColor = QBColor(col)
    pctGriglia.Circle (xSchermo(x) + CLng(dimOrizzontalePosto / 2 * passo * zoom), _
                    ySchermo(Y) + CLng(dimVerticalePosto / 2 * passo * zoom)), _
                    dimOrizzontalePosto / 5 * passo * zoom, QBColor(col)
    pctGriglia.FillStyle = fill_old
    pctGriglia.FillColor = color_old
End Sub

Private Sub SelezionaElementoSuCombo(cmb As ComboBox, id As Long)
    Dim i As Integer
    
    If id = idNessunElementoSelezionato Then
        cmb.ListIndex = idNessunElementoSelezionato
    Else
        For i = 1 To cmb.ListCount
            If id = cmb.ItemData(i - 1) Then
                cmb.ListIndex = i - 1
            End If
        Next i
    End If
    
End Sub

Private Sub Area_Refresh()
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    
    Select Case caratteristicaAttualmenteConfigurabile
        Case CAC_AREA
            If tipoGriglia = TG_GRANDI_IMPIANTI Then
                Call ImpostaColoreInsiemePosti(listaPostiInGriglia, AP_NON_IN_FASCIA)
            Else
                Call ImpostaColoreInsiemePosti(listaPostiInGriglia, AP_NON_ATTIVO)
                Call ImpostaColoreInsiemePosti(area.collPosti, AP_NON_IN_FASCIA)
            End If
            Call ImpostaColoreInsiemePosti(listaPostiSelezionati, AP_SELEZIONATO)
            For Each fascia In area.collFasce
                Call ImpostaColoreInsiemePosti(fascia.collPosti, AP_CON_FASCIA)
                For Each sequenza In fascia.collSequenze
                    Call ImpostaColoreInsiemePosti(sequenza.collPosti, AP_CON_FASCIA_SEQUENZA)
                Next sequenza
            Next fascia
        Case CAC_FASCE
            Call ImpostaColoreInsiemePosti(listaPostiInGriglia, AP_NON_ATTIVO)
            Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
            If Not (fascia Is Nothing) Then
                Call ImpostaColoreInsiemePosti(fascia.collPosti, AP_CON_FASCIA)
                For Each sequenza In fascia.collSequenze
                    Call ImpostaColoreInsiemePosti(sequenza.collPosti, AP_CON_FASCIA_SEQUENZA)
                    If (sequenza.ordineInFascia = ordineSequenzaInFascia) Then
                        Call DisegnaSequenzaPosti(sequenza.collPosti, CP_VERDE_SELEZIONE)
                    Else
                        Call DisegnaSequenzaPosti(sequenza.collPosti, CP_ROSSO)
                    End If
                Next sequenza
            End If
        Case CAC_SEQUENZE
            Call ImpostaColoreInsiemePosti(listaPostiInGriglia, AP_NON_ATTIVO)
            Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
            If Not (fascia Is Nothing) Then
                Call ImpostaColoreInsiemePosti(fascia.collPosti, AP_CON_FASCIA)
                For Each sequenza In fascia.collSequenze
                    Call ImpostaColoreInsiemePosti(sequenza.collPosti, AP_CON_FASCIA_SEQUENZA)
                    ' Questo � stato aggiunto da LUIGI ma cos� � uguale a CAC_FASCE!
                    If (sequenza.ordineInFascia = ordineSequenzaInFascia) Then
                        Call DisegnaSequenzaPosti(sequenza.collPosti, CP_VERDE_SELEZIONE)
                    Else
                        Call DisegnaSequenzaPosti(sequenza.collPosti, CP_ROSSO)
                    End If
                Next sequenza
            End If
        Case Else
            Call ImpostaColoreInsiemePosti(listaPostiInGriglia, AP_NON_ATTIVO)
    End Select
End Sub

Private Sub AggiungiNuovaFascia()
    Dim fascia As classeFascia
    
    Call CaricaFormDettagliFascia(MFDF_CREA_FASCIA)
    If exitCodeDettagliFascia = EC_DP_CONFERMA Then
        Set fascia = New classeFascia
        Call area.creaFascia(indiceDiPreferibilitaFasciaSelezionata, listaPostiSelezionati)
        Call LstFasceConfigurate_Init
        Call SelezionaElementoSuLista(lstFasceConfigurate, indiceDiPreferibilitaFasciaSelezionata)
    Else
        Call SetCaratteristicaAttualmenteConfigurabile(CAC_AREA)
    End If
    Call Area_Refresh
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub AggiungiNuovaSequenza()
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    
    If sequenzaTemporaneaPosti.count > 0 Then
        caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE
        Call CaricaFormDettagliSequenza(MFDS_CREA_SEQUENZA)
        If exitCodeDettagliSequenza = EC_DP_CONFERMA Then
            Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
            Call CreaSequenzaInFascia(fascia)
        Else
            Call Area_Refresh
        End If
        Call AggiornaAbilitazioneControlli
    End If
End Sub

Private Sub DisegnaSequenzaPosti_OLD(listaPostiInSequenza As Collection, col As ColorePostoEnum)
    Dim xOld As Integer
    Dim yOld As Integer
    Dim xNew As Integer
    Dim yNew As Integer
    Dim k As Integer
    
    If listaPostiInSequenza.count > 0 Then
        xOld = listaPostiInSequenza.Item(1).xPosto
        yOld = listaPostiInSequenza.Item(1).yPosto
        Call DisegnaPuntoIniziale(xOld, yOld, col)

        For k = 2 To listaPostiInSequenza.count
            xNew = listaPostiInSequenza.Item(k).xPosto
            yNew = listaPostiInSequenza.Item(k).yPosto
            Call DisegnaLineaUnionePosti(xOld, yOld, xNew, yNew, col)
            xOld = xNew
            yOld = yNew
            DoEvents
        Next k
    End If
    
End Sub

Private Sub DisegnaSequenzaPosti(listaPostiInSequenza As Collection, col As ColorePostoEnum)
    Dim xOld As Integer
    Dim yOld As Integer
    Dim p As clsPosto
    Dim primoPosto As Boolean
    
    primoPosto = True
    For Each p In listaPostiInSequenza
        If primoPosto Then
            Call DisegnaPuntoIniziale(p.xPosto, p.yPosto, col)
            xOld = p.xPosto
            yOld = p.yPosto
            primoPosto = False
        Else
            Call DisegnaLineaUnionePosti(xOld, yOld, p.xPosto, p.yPosto, col)
            xOld = p.xPosto
            yOld = p.yPosto
        End If
    Next p
End Sub

Private Sub ContaPosti()
    Dim sql As String
'    Dim rec As New ADODB.Recordset
    Dim rec As OraDynaset
    Dim sql1 As String
'    Dim rec1 As New ADODB.Recordset
    Dim rec1 As OraDynaset
    Dim idA As Long
    Dim numero As Long
    
    Call ApriConnessioneBD_ORA
        
    Set numeroPostiPerArea = New Collection
    
    ORADB.BeginTrans
    sql = " SELECT COUNT(P.IDPOSTO) NUMERO"
    sql = sql & " FROM POSTO P, AREA A"
    sql = sql & " WHERE P.IDAREA = A.IDAREA"
    sql = sql & " AND A.IDPIANTA = " & idPiantaSelezionata
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
    Set rec = ORADB.CreateDynaset(sql, 0&)
    rec.MoveFirst
    numeroPostiInPianta = rec("NUMERO")
    rec.Close
    Select Case tipoGriglia
        Case TG_GRANDI_IMPIANTI
            Call numeroPostiPerArea.Add(listaPostiInGriglia.count, ChiaveId(idAreaSelezionata))
        Case TG_PICCOLI_IMPIANTI
            sql = "SELECT DISTINCT IDAREA"
            sql = sql & " FROM AREA"
            sql = sql & " WHERE IDPIANTA = " & idPiantaSelezionata
'            rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
            Set rec = ORADB.CreateDynaset(sql, 0&)
            If Not (rec.BOF And rec.EOF) Then
                rec.MoveFirst
                While Not rec.EOF
                    idA = rec("IDAREA")
                    sql1 = "SELECT COUNT(IDPOSTO) NUMERO FROM POSTO WHERE IDAREA = " & idA
'                    rec1.Open sql1, SETAConnection, adOpenDynamic, adLockOptimistic
                    Set rec1 = ORADB.CreateDynaset(sql1, 0&)
                    rec1.MoveFirst
                    numero = rec1("NUMERO")
                    Call numeroPostiPerArea.Add(numero, ChiaveId(idA))
                    rec1.Close
                    rec.MoveNext
                Wend
            End If
            rec.Close
    End Select
    ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
    Call AggiornaAbilitazioneControlli
    
End Sub

Private Sub cmdCreaSequenzeAutomaticamente_Click()
    Dim mousePointerOld As Integer
    
    mousePointerOld = MousePointer
    MousePointer = vbHourglass
    
    Call CreaSequenzeAutomaticamente
    
    MousePointer = mousePointerOld
End Sub

Private Sub CreaSequenzeAutomaticamente()
    caratteristicaAttualmenteConfigurabile = CAC_FASCE
    operazioneCorrente = OCFS_NUOVA
    Call CaricaFormCreaSequenzeAutomaticamente
    If frmCreaSequenzeAutomaticamente.GetExitCode = EC_CONFERMA Then
        tipoSequenza = frmCreaSequenzeAutomaticamente.GetTipoSequenza
        Select Case tipoSequenza
            Case TS_ABC123L, TS_ABC123S, TS_ABC321L, TS_ABC321S, TS_CBA123L, TS_CBA123S, TS_CBA321L, TS_CBA321S
                Call CreaSequenzeLogiche
            Case Else
                Call CreaSequenzeGeometriche
        End Select
    End If
    Call Area_Refresh
End Sub

Private Sub CreaSequenzeLogiche()
    Select Case tipoSequenza
        Case TS_ABC123S
            Call CreaSequenzaLogicaUnica(1, 1)
        Case TS_ABC321S
            Call CreaSequenzaLogicaUnica(-1, 1)
        Case TS_CBA123S
            Call CreaSequenzaLogicaUnica(1, -1)
        Case TS_CBA321S
            Call CreaSequenzaLogicaUnica(-1, -1)
        Case TS_ABC123L
            Call CreaSequenzeLogicheLineari(1, 1)
        Case TS_ABC321L
            Call CreaSequenzeLogicheLineari(-1, 1)
        Case TS_CBA123L
            Call CreaSequenzeLogicheLineari(1, -1)
        Case TS_CBA321L
            Call CreaSequenzeLogicheLineari(-1, -1)
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Private Sub CreaSequenzaLogicaUnica(versoPosti As Integer, versoFile As Integer)
    Dim i As Integer
    Dim nomeFila As String
    Dim nomeFilaOld As String
    Dim chiaveIdPosto As String
    Dim finito As Boolean
    Dim indiceNew As Integer
    Dim posto As clsPosto
    Dim n As Integer
    Dim fascia As classeFascia
    
    Call CaricaListaPostiOrdinata
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE
    n = listaPostiSelezionati.count
    Set sequenzaTemporaneaPosti = New Collection
    i = 0
    If versoFile = 1 Then
        nomeFila = CStr(Mid(lstPostiOrdinati.List(i), 1, 2))
    Else
        nomeFila = CStr(Mid(lstPostiOrdinati.List(n - i - 1), 1, 2))
    End If
    nomeFilaOld = nomeFila
    finito = False
    While Not finito
        nomeFilaOld = nomeFila
        If versoFile = 1 Then
            nomeFila = CStr(Mid(lstPostiOrdinati.List(i), 1, 2))
        Else
            nomeFila = CStr(Mid(lstPostiOrdinati.List(n - i - 1), 1, 2))
        End If
        indiceNew = IndiceInizialeFila(nomeFila, versoPosti)
        While nomeFila = nomeFilaOld
            chiaveIdPosto = ChiaveIdPostoOrdinatoInLista(CStr(lstPostiOrdinati.List(indiceNew)))
            Set posto = listaPostiSelezionati(chiaveIdPosto)
            Call sequenzaTemporaneaPosti.Add(posto, chiaveIdPosto)
            indiceNew = indiceNew + versoPosti
            i = i + 1
            If versoFile = 1 Then
                nomeFila = CStr(Mid(lstPostiOrdinati.List(i), 1, 2))
            Else
                nomeFila = CStr(Mid(lstPostiOrdinati.List(n - i - 1), 1, 2))
            End If
        Wend
        versoPosti = -1 * versoPosti
        If i >= n Then finito = True
    Wend
    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
    Call CreaSequenzaInFascia(fascia)
End Sub

Private Sub CreaSequenzeLogicheLineari(versoPosti As Integer, versoFile As Integer)
    Dim i As Integer
    Dim nomeFila As String
    Dim nomeFilaOld As String
    Dim chiaveIdPosto As String
    Dim finito As Boolean
    Dim indiceNew As Integer
    Dim posto As clsPosto
    Dim n As Integer
    Dim fascia As classeFascia

    Call CaricaListaPostiOrdinata
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE
    n = listaPostiSelezionati.count
    i = 0
    If versoFile = 1 Then
        nomeFila = CStr(Mid(lstPostiOrdinati.List(i), 1, 2))
    Else
        nomeFila = CStr(Mid(lstPostiOrdinati.List(n - i - 1), 1, 2))
    End If
    nomeFilaOld = nomeFila
    finito = False
    While Not finito
        nomeFilaOld = nomeFila
        If versoFile = 1 Then
            nomeFila = CStr(Mid(lstPostiOrdinati.List(i), 1, 2))
        Else
            nomeFila = CStr(Mid(lstPostiOrdinati.List(n - i - 1), 1, 2))
        End If
        indiceNew = IndiceInizialeFila(nomeFila, versoPosti)
        Set sequenzaTemporaneaPosti = New Collection
        While nomeFila = nomeFilaOld
            chiaveIdPosto = ChiaveIdPostoOrdinatoInLista(CStr(lstPostiOrdinati.List(indiceNew)))
            Set posto = listaPostiSelezionati(chiaveIdPosto)
            Call sequenzaTemporaneaPosti.Add(posto, chiaveIdPosto)
            indiceNew = indiceNew + versoPosti
            i = i + 1
        If versoFile = 1 Then
            nomeFila = CStr(Mid(lstPostiOrdinati.List(i), 1, 2))
        Else
            nomeFila = CStr(Mid(lstPostiOrdinati.List(n - i - 1), 1, 2))
        End If
        Wend
        ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
        Call CreaSequenzaInFascia(fascia)
        If i >= n Then finito = True
    Wend
End Sub

Private Function IndiceInizialeFila(nomeNuovaFila As String, verso As Integer) As Integer
    Dim i As Integer
    Dim inizio As Integer
    Dim fine As Integer
    
'    metodo scandaloso; rifare comunque tutto
    If verso = 1 Then
        i = 0
    Else
        i = lstPostiOrdinati.ListCount - 1
    End If
    While nomeNuovaFila <> Left(lstPostiOrdinati.List(i), 2)
        i = i + verso
    Wend
    IndiceInizialeFila = i
End Function

Private Function ChiaveIdPostoOrdinatoInLista(sIn As String) As String
    Dim l As Integer
    
    l = Len(sIn)
    ChiaveIdPostoOrdinatoInLista = CStr(Right(sIn, l - 5))
End Function

Private Sub CaricaListaPostiOrdinata()
    Dim posto As clsPosto
    Dim nomeOrdinePosto As String
    Dim id As Long
    
    Call lstPostiOrdinati.Clear
    For Each posto In listaPostiSelezionati
        nomeOrdinePosto = StringaLunghezzaMaxFissataAllineataADestra("  " & posto.nomeFila, 2) & _
            StringaLunghezzaMaxFissataAllineataADestra("   " & posto.nomePosto, 3) & _
            ChiaveId(posto.idPosto)
        Call lstPostiOrdinati.AddItem(nomeOrdinePosto)
    Next posto
End Sub
'
'Private Sub CreaTabellaAppoggioPostiInFascia()
'    Dim sql As String
'
'    nomeTabellaTemporanea = SqlStringTableName("TMP_IDPOSTIFASCIA_" & getNomeMacchina)
'    sql = "CREATE TABLE " & nomeTabellaTemporanea
'    sql = sql & " (IDPOSTO NUMBER(10))"
'
'    Call EliminaTabellaAppoggioPostiInFascia
'    Call ApriConnessioneBD_ORA
''    SETAConnection.Execute (sql)
'    Call ORADB.ExecuteSQL(sql)
'    Call ChiudiConnessioneBD_ORA
'
'End Sub
'
'Private Sub EliminaTabellaAppoggioPostiInFascia()
'    Dim sql As String
'
'On Error GoTo gestioneErrori
'
'    sql = "DROP TABLE " & nomeTabellaTemporanea
'
'    Call ApriConnessioneBD_ORA
''    SETAConnection.Execute (sql)
'    Call ORADB.ExecuteSQL(sql)
'
'gestioneErrori:
'
'    Call ChiudiConnessioneBD_ORA
'
'End Sub
'
'Private Sub PopolaTabellaAppoggioPostiInFascia(fasciaCorrente As classeFascia)
'    Dim i As Integer
'    Dim rec As New ADODB.Recordset
'    Dim sql As String
'
'    sql = "SELECT * FROM " & nomeTabellaTemporanea
'    rec.Open sql, SETAConnection, adOpenDynamic, adLockOptimistic
'    For i = 1 To fasciaCorrente.collPosti.count
'        rec.AddNew
'        rec("IDPOSTO") = fasciaCorrente.collPosti(i).idPosto
'    Next i
'    rec.UpdateBatch
'    rec.Close
'End Sub

Private Sub GetParametriRettangolo(fascia As classeFascia, xMin As Long, xMax As Long, yMin As Long, yMax As Long)
    Dim i As Integer
    For i = 1 To fascia.collPosti.count
        xMin = IIf(fascia.collPosti(i).xPosto < xMin, fascia.collPosti(i).xPosto, xMin)
        xMax = IIf(fascia.collPosti(i).xPosto > xMax, fascia.collPosti(i).xPosto, xMax)
        yMin = IIf(fascia.collPosti(i).yPosto < yMin, fascia.collPosti(i).yPosto, yMin)
        yMax = IIf(fascia.collPosti(i).yPosto > yMax, fascia.collPosti(i).yPosto, yMax)
    Next i
End Sub

Private Sub CreaSequenzaInFascia(fascia As classeFascia)
    Dim sequenza As classeSequenza
    
    Set sequenza = New classeSequenza
    Call fascia.creaSequenza(ordineSequenzaInFascia, sequenzaTemporaneaPosti)
    Call LstSequenzeConfigurate_Init(indiceDiPreferibilitaFasciaSelezionata)
    Call SelezionaElementoSuLista(lstSequenzeConfigurate, ordineSequenzaInFascia)
    Call Area_Refresh
End Sub

Private Sub CreaSequenzeGeometriche()
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    Dim xIniz As Long
    Dim yIniz As Long
    Dim xFine As Long
    Dim yFine As Long
    Dim xMin As Long
    Dim xMax As Long
    Dim yMin As Long
    Dim yMax As Long
    Dim i As Integer
    Dim j As Integer
    Dim verso As Integer
    Dim idP As Long
    Dim cambiaVerso As Boolean
    
    Set fascia = area.ottieniFasciaDaIndiceDiPreferibilita(indiceDiPreferibilitaFasciaSelezionata)
    
    xMin = maxCoordOrizzontale
    xMax = 0
    yMin = maxCoordVerticale
    yMax = 0
    Call GetParametriRettangolo(fascia, xMin, xMax, yMin, yMax)
    caratteristicaAttualmenteConfigurabile = CAC_SEQUENZE
    Select Case tipoSequenza
        Case TS_NWO
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMin
            yIniz = yMin
            xFine = xMax
            yFine = yMax
            verso = 1
            For j = yMin To (yMax + dimVerticalePosto) Step dimVerticalePosto
                cambiaVerso = False
                ' aggiunto +1 e -1 da luigi per tranquillit�...
                For i = xIniz - verso * dimOrizzontalePosto To xFine + verso * dimOrizzontalePosto Step verso * dimOrizzontalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next i
                If cambiaVerso Then
                    verso = verso * (-1)
                    xIniz = xFine - xIniz
                    xFine = xFine - xIniz
                    xIniz = xFine + xIniz
                End If
            Next j
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_NEO
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMax
            yIniz = yMin
            xFine = xMin
            yFine = yMax
            verso = -1
            For j = yMin To (yMax + dimVerticalePosto) Step dimVerticalePosto
                cambiaVerso = False
                For i = xIniz - verso * dimOrizzontalePosto To xFine + verso * dimOrizzontalePosto Step verso * dimOrizzontalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next i
                If cambiaVerso Then
                    verso = verso * (-1)
                    xIniz = xFine - xIniz
                    xFine = xFine - xIniz
                    xIniz = xFine + xIniz
                End If
            Next j
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_SWO
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMin
            yIniz = yMax
            xFine = xMax
            yFine = yMin
            verso = 1
            For j = (yMax + dimVerticalePosto) To yMin Step -dimVerticalePosto
                cambiaVerso = False
                For i = xIniz - verso * dimOrizzontalePosto To xFine + verso * dimOrizzontalePosto Step verso * dimOrizzontalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next i
                If cambiaVerso Then
                    verso = verso * (-1)
                    xIniz = xFine - xIniz
                    xFine = xFine - xIniz
                    xIniz = xFine + xIniz
                End If
            Next j
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_SEO
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMax
            yIniz = yMax
            xFine = xMin
            yFine = yMin
            verso = -1
            For j = (yMax + dimVerticalePosto) To yMin Step -dimVerticalePosto
                cambiaVerso = False
                For i = xIniz - verso * dimOrizzontalePosto To xFine + verso * dimOrizzontalePosto Step verso * dimOrizzontalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next i
                If cambiaVerso Then
                    verso = verso * (-1)
                    xIniz = xFine - xIniz
                    xFine = xFine - xIniz
                    xIniz = xFine + xIniz
                End If
            Next j
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_NWV
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMin
            yIniz = yMin
            xFine = xMax
            yFine = yMax
            verso = 1
            For i = xMin To (xMax + dimOrizzontalePosto) Step dimOrizzontalePosto
                cambiaVerso = False
                For j = yIniz To yFine Step verso * dimVerticalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next j
                If cambiaVerso Then
                    verso = verso * (-1)
                    yIniz = yFine - yIniz
                    yFine = yFine - yIniz
                    yIniz = yFine + yIniz
                End If
            Next i
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_NEV
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMax
            yIniz = yMin
            xFine = xMin
            yFine = yMax
            verso = 1
            For i = (xMax + dimOrizzontalePosto) To xMin Step -dimOrizzontalePosto
                cambiaVerso = False
                For j = yIniz To yFine Step verso * dimVerticalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next j
                If cambiaVerso Then
                    verso = verso * (-1)
                    yIniz = yFine - yIniz
                    yFine = yFine - yIniz
                    yIniz = yFine + yIniz
                End If
            Next i
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_SWV
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMin
            yIniz = yMax
            xFine = xMax
            yFine = yMin
            verso = -1
            For i = xMin To (xMax + dimOrizzontalePosto) Step dimOrizzontalePosto
                cambiaVerso = False
                For j = yIniz To yFine Step verso * dimVerticalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next j
                If cambiaVerso Then
                    verso = verso * (-1)
                    yIniz = yFine - yIniz
                    yFine = yFine - yIniz
                    yIniz = yFine + yIniz
                End If
            Next i
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_SEV
            Set sequenzaTemporaneaPosti = New Collection
            xIniz = xMax
            yIniz = yMax
            xFine = xMin
            yFine = yMin
            verso = -1
            For i = (xMax + dimOrizzontalePosto) To xMin Step -dimOrizzontalePosto
                cambiaVerso = False
                For j = yIniz To yFine Step verso * dimVerticalePosto
                    If IsPuntoInGriglia(i, j) Then
                        idP = matriceGrigliaPosti(i, j).idPosto
                        If fascia.IsPostoAssociatoAFascia(idP) Then
                            cambiaVerso = True
                            Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                        End If
                    End If
                Next j
                If cambiaVerso Then
                    verso = verso * (-1)
                    yIniz = yFine - yIniz
                    yFine = yFine - yIniz
                    yIniz = yFine + yIniz
                End If
            Next i
            ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
            Call CreaSequenzaInFascia(fascia)
        Case TS_W1
            For j = yMin To (yMax + dimVerticalePosto) Step dimVerticalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For i = xMin To (xMax + dimOrizzontalePosto) Step dimOrizzontalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next i
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next j
        Case TS_E1
            For j = yMin To (yMax + dimVerticalePosto) Step dimVerticalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For i = (xMax + dimOrizzontalePosto) To xMin Step -dimOrizzontalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next i
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next j
        Case TS_N1
            For i = xMin To (xMax + dimOrizzontalePosto) Step dimOrizzontalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For j = yMin To (yMax + dimVerticalePosto) Step dimVerticalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next j
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next i
        Case TS_S1
            For i = xMin To (xMax + dimOrizzontalePosto) Step dimOrizzontalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For j = (yMax + dimVerticalePosto) To yMin Step -dimVerticalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next j
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next i
        Case TS_W2
            For j = (yMax + dimVerticalePosto) To yMin Step -dimVerticalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For i = xMin To (xMax + dimOrizzontalePosto) Step dimOrizzontalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next i
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next j
        Case TS_E2
            For j = (yMax + dimVerticalePosto) To yMin Step -dimVerticalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For i = (xMax + dimOrizzontalePosto) To xMin Step -dimOrizzontalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next i
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next j
        Case TS_N2
            For i = (xMax + dimOrizzontalePosto) To xMin Step -dimOrizzontalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For j = yMin To (yMax + dimVerticalePosto) Step dimVerticalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next j
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next i
        Case TS_S2
            For i = (xMax + dimOrizzontalePosto) To xMin Step -dimOrizzontalePosto
                Set sequenzaTemporaneaPosti = New Collection
                For j = (yMax + dimVerticalePosto) To yMin Step -dimVerticalePosto
                    idP = matriceGrigliaPosti(i, j).idPosto
                    If fascia.IsPostoAssociatoAFascia(idP) Then
                        Call sequenzaTemporaneaPosti.Add(matriceGrigliaPosti(i, j), ChiaveId(matriceGrigliaPosti(i, j).idPosto))
                    End If
                Next j
                If sequenzaTemporaneaPosti.count > 0 Then
                    ordineSequenzaInFascia = fascia.OrdineInFasciaSuccessivo
                    Call CreaSequenzaInFascia(fascia)
                End If
            Next i
        Case Else
    End Select
    Call AggiornaAbilitazioneControlli
End Sub

Private Function IsPuntoInGriglia(x As Integer, Y As Integer) As Boolean
    If x < 1 Or x > maxCoordOrizzontale Or Y < 1 Or Y > maxCoordVerticale Then
        IsPuntoInGriglia = False
    Else
        IsPuntoInGriglia = True
    End If
End Function

Private Sub EliminaDallaBaseDati(areaC As classeArea)
    Dim sql1 As String
    Dim sql2 As String
    Dim idFP As Long
    Dim n As Integer
    Dim rec As OraDynaset
    
    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    
    sql1 = "UPDATE POSTO SET IDSEQUENZAPOSTI = NULL,"
    sql1 = sql1 & " ORDINEINSEQUENZA = NULL WHERE IDAREA = " & areaC.idArea
    n = ORADB.ExecuteSQL(sql1)
    
    sql1 = "SELECT IDFASCIAPOSTI FROM FASCIAPOSTI WHERE IDAREA = " & areaC.idArea
    Set rec = ORADB.CreateDynaset(sql1, 0&)
    If Not (rec.BOF And rec.EOF) Then
        rec.MoveFirst
        While Not rec.EOF
            idFP = rec("IDFASCIAPOSTI")
            sql2 = "DELETE FROM SEQUENZAPOSTI WHERE IDFASCIAPOSTI = " & idFP
            n = ORADB.ExecuteSQL(sql2)
            rec.MoveNext
        Wend
    End If
    rec.Close
    
    sql1 = "DELETE FROM FASCIAPOSTI WHERE IDAREA = " & areaC.idArea
    n = ORADB.ExecuteSQL(sql1)
    
    Call ORADB.CommitTrans

    Call ChiudiConnessioneBD_ORA
    
End Sub

Private Sub InserisciNellaBaseDati(areaC As classeArea)
    Dim fascia As classeFascia
    Dim sequenza As classeSequenza
    Dim idNuovaFascia As Long
    Dim idNuovaSequenza As Long
    Dim sql As String
    Dim i As Integer
    Dim n As Long

    Call ApriConnessioneBD_ORA
    
    Call ORADB.BeginTrans
    For Each fascia In areaC.collFasce
        If fascia.collSequenze.count > 0 Then
            idNuovaFascia = OttieniIdentificatoreDaSequenza("SQ_FASCIAPOSTI")
            sql = "INSERT INTO FASCIAPOSTI (IDFASCIAPOSTI, IDAREA, INDICEDIPREFERIBILITA)"
            sql = sql & " VALUES ("
            sql = sql & idNuovaFascia & ", "
            sql = sql & areaC.idArea & ", "
            sql = sql & fascia.indiceDiPreferibilita & ")"
            n = ORADB.ExecuteSQL(sql)
            For Each sequenza In fascia.collSequenze
                idNuovaSequenza = OttieniIdentificatoreDaSequenza("SQ_SEQUENZAPOSTI")
                sql = "INSERT INTO SEQUENZAPOSTI (IDSEQUENZAPOSTI, IDFASCIAPOSTI, ORDINEINFASCIA)"
                sql = sql & " VALUES ("
                sql = sql & idNuovaSequenza & ", "
                sql = sql & idNuovaFascia & ", "
                sql = sql & sequenza.ordineInFascia & ")"
                n = ORADB.ExecuteSQL(sql)
                For i = 1 To sequenza.collPosti.count
                    sql = "UPDATE POSTO SET IDSEQUENZAPOSTI = " & idNuovaSequenza & ","
                    sql = sql & " ORDINEINSEQUENZA = " & i & " WHERE IDPOSTO = " & sequenza.collPosti(i).idPosto
                    n = ORADB.ExecuteSQL(sql)
                Next i
            Next sequenza
        End If
    Next fascia
    Call ORADB.CommitTrans
    
    Call ChiudiConnessioneBD_ORA
    
End Sub


